class faculty{
String FacultyName;
List<Student> student;
private static List<faculty> facultylist;
String type = null;
String path = null;

private static List<faculty> load_data(){
facultylist = new ArrayList();
try(FileReader fileReader = new FileReader(null)){
Gson gson = new Gson();
            JsonReader reader = new JsonReader(fileReader);
 JsonObject jsonObject  = gson.fromJson(reader,JsonObject.class);

            JsonArray jsonArray =  jsonObject.getAsJsonArray("faculty");
for(int i =0 ;i<jsonArray.size();i++)
            {
                JsonElement jsonElement = jsonArray.get(i);
faculty faculty_obj = gson.fromJson(jsonElement,faculty.class);
facultylist.add(faculty_obj);
            }}
catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

return facultylist;
}
public static List<faculty> getData(){
if(facultylist == null){
facultylist = load_data();
}
return facultylist;
}}