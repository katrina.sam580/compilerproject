import java.util.List;

public class CommonAggregations {

    public double Count(List<Double> numbers)
    {
        return numbers.
                stream().
                mapToDouble(Double::doubleValue)
                .count();
    }

    public double Sum(List<Double> numbers){
        return numbers.
                stream().
                mapToDouble(Double::doubleValue)
                .sum();
    }

    public double Avg(List<Double> numbers)
    {
        return numbers.
                stream().
                mapToDouble(Double::doubleValue).average().getAsDouble();
    }

    public double Max(List<Double> numbers)
    {
        return numbers.
                stream().
                mapToDouble(Double::doubleValue).max().getAsDouble();
    }
    public double Min(List<Double> numbers)
    {
        return numbers.
                stream().
                mapToDouble(Double::doubleValue).min().getAsDouble();
    }

    public double Std(List<Double> numbers)
    {
        double sum = Sum(numbers);
        double mean = 0;
        mean = sum / (numbers.size()*1.0);
        double res = 0;
        for(Double i : numbers)
        {
            res +=Math.pow((i-mean),2);
        }
        return Math.sqrt(sum/(numbers.size()-1));
       // return Math.sqrt(res);
    }

    private static volatile CommonAggregations myCommonAggregator;

    private CommonAggregations(){
        if (myCommonAggregator != null) {
            throw new IllegalStateException("CommonAggregations object is already created!");
        }
    }

    public static CommonAggregations getCommonAggregations() {
        if(myCommonAggregator == null){
            synchronized(CommonAggregations.class) {
                if (myCommonAggregator == null) {
                    myCommonAggregator = new CommonAggregations();
                }
            }
        }
        return myCommonAggregator;
    }

}
