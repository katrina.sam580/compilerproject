// Generated from C:/Users/Sumayya Mufleh/Desktop/Compiler (3)/src\SQL.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SQLParser}.
 */
public interface SQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SQLParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(SQLParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(SQLParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#error}.
	 * @param ctx the parse tree
	 */
	void enterError(SQLParser.ErrorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#error}.
	 * @param ctx the parse tree
	 */
	void exitError(SQLParser.ErrorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt_list(SQLParser.Sql_stmt_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt_list(SQLParser.Sql_stmt_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt(SQLParser.Sql_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt(SQLParser.Sql_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alter_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterAlter_table_stmt(SQLParser.Alter_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alter_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitAlter_table_stmt(SQLParser.Alter_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alter_table_add_constraint}.
	 * @param ctx the parse tree
	 */
	void enterAlter_table_add_constraint(SQLParser.Alter_table_add_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alter_table_add_constraint}.
	 * @param ctx the parse tree
	 */
	void exitAlter_table_add_constraint(SQLParser.Alter_table_add_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#alter_table_add}.
	 * @param ctx the parse tree
	 */
	void enterAlter_table_add(SQLParser.Alter_table_addContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#alter_table_add}.
	 * @param ctx the parse tree
	 */
	void exitAlter_table_add(SQLParser.Alter_table_addContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#create_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_table_stmt(SQLParser.Create_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#create_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_table_stmt(SQLParser.Create_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#type_create_table}.
	 * @param ctx the parse tree
	 */
	void enterType_create_table(SQLParser.Type_create_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#type_create_table}.
	 * @param ctx the parse tree
	 */
	void exitType_create_table(SQLParser.Type_create_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#path_create_table}.
	 * @param ctx the parse tree
	 */
	void enterPath_create_table(SQLParser.Path_create_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#path_create_table}.
	 * @param ctx the parse tree
	 */
	void exitPath_create_table(SQLParser.Path_create_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 */
	void enterCreate_aggregation_function(SQLParser.Create_aggregation_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 */
	void exitCreate_aggregation_function(SQLParser.Create_aggregation_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#jar_path}.
	 * @param ctx the parse tree
	 */
	void enterJar_path(SQLParser.Jar_pathContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#jar_path}.
	 * @param ctx the parse tree
	 */
	void exitJar_path(SQLParser.Jar_pathContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#class_name}.
	 * @param ctx the parse tree
	 */
	void enterClass_name(SQLParser.Class_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#class_name}.
	 * @param ctx the parse tree
	 */
	void exitClass_name(SQLParser.Class_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#return_type}.
	 * @param ctx the parse tree
	 */
	void enterReturn_type(SQLParser.Return_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#return_type}.
	 * @param ctx the parse tree
	 */
	void exitReturn_type(SQLParser.Return_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#method_name}.
	 * @param ctx the parse tree
	 */
	void enterMethod_name(SQLParser.Method_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#method_name}.
	 * @param ctx the parse tree
	 */
	void exitMethod_name(SQLParser.Method_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#delete_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDelete_stmt(SQLParser.Delete_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#delete_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDelete_stmt(SQLParser.Delete_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#drop_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDrop_table_stmt(SQLParser.Drop_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#drop_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDrop_table_stmt(SQLParser.Drop_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFactored_select_stmt(SQLParser.Factored_select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFactored_select_stmt(SQLParser.Factored_select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#assigne_select}.
	 * @param ctx the parse tree
	 */
	void enterAssigne_select(SQLParser.Assigne_selectContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#assigne_select}.
	 * @param ctx the parse tree
	 */
	void exitAssigne_select(SQLParser.Assigne_selectContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#insert_stmt}.
	 * @param ctx the parse tree
	 */
	void enterInsert_stmt(SQLParser.Insert_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#insert_stmt}.
	 * @param ctx the parse tree
	 */
	void exitInsert_stmt(SQLParser.Insert_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSelect_stmt(SQLParser.Select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSelect_stmt(SQLParser.Select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#select_or_values}.
	 * @param ctx the parse tree
	 */
	void enterSelect_or_values(SQLParser.Select_or_valuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#select_or_values}.
	 * @param ctx the parse tree
	 */
	void exitSelect_or_values(SQLParser.Select_or_valuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#update_stmt}.
	 * @param ctx the parse tree
	 */
	void enterUpdate_stmt(SQLParser.Update_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#update_stmt}.
	 * @param ctx the parse tree
	 */
	void exitUpdate_stmt(SQLParser.Update_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#update_first_parameter}.
	 * @param ctx the parse tree
	 */
	void enterUpdate_first_parameter(SQLParser.Update_first_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#update_first_parameter}.
	 * @param ctx the parse tree
	 */
	void exitUpdate_first_parameter(SQLParser.Update_first_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#update_second_parameter}.
	 * @param ctx the parse tree
	 */
	void enterUpdate_second_parameter(SQLParser.Update_second_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#update_second_parameter}.
	 * @param ctx the parse tree
	 */
	void exitUpdate_second_parameter(SQLParser.Update_second_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_def}.
	 * @param ctx the parse tree
	 */
	void enterColumn_def(SQLParser.Column_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_def}.
	 * @param ctx the parse tree
	 */
	void exitColumn_def(SQLParser.Column_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(SQLParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(SQLParser.Type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_constraint}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint(SQLParser.Column_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_constraint}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint(SQLParser.Column_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_primary_key(SQLParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_primary_key(SQLParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_foreign_key(SQLParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_foreign_key(SQLParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_not_null(SQLParser.Column_constraint_not_nullContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_not_null(SQLParser.Column_constraint_not_nullContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_constraint_null}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_null(SQLParser.Column_constraint_nullContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_constraint_null}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_null(SQLParser.Column_constraint_nullContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_default}.
	 * @param ctx the parse tree
	 */
	void enterColumn_default(SQLParser.Column_defaultContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_default}.
	 * @param ctx the parse tree
	 */
	void exitColumn_default(SQLParser.Column_defaultContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_default_value}.
	 * @param ctx the parse tree
	 */
	void enterColumn_default_value(SQLParser.Column_default_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_default_value}.
	 * @param ctx the parse tree
	 */
	void exitColumn_default_value(SQLParser.Column_default_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(SQLParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(SQLParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expr_test}.
	 * @param ctx the parse tree
	 */
	void enterExpr_test(SQLParser.Expr_testContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expr_test}.
	 * @param ctx the parse tree
	 */
	void exitExpr_test(SQLParser.Expr_testContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fun_name}.
	 * @param ctx the parse tree
	 */
	void enterFun_name(SQLParser.Fun_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fun_name}.
	 * @param ctx the parse tree
	 */
	void exitFun_name(SQLParser.Fun_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void enterParameter_list(SQLParser.Parameter_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void exitParameter_list(SQLParser.Parameter_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#define_var}.
	 * @param ctx the parse tree
	 */
	void enterDefine_var(SQLParser.Define_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#define_var}.
	 * @param ctx the parse tree
	 */
	void exitDefine_var(SQLParser.Define_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#define_var_assigne}.
	 * @param ctx the parse tree
	 */
	void enterDefine_var_assigne(SQLParser.Define_var_assigneContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#define_var_assigne}.
	 * @param ctx the parse tree
	 */
	void exitDefine_var_assigne(SQLParser.Define_var_assigneContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(SQLParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(SQLParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(SQLParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(SQLParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(SQLParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(SQLParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#body_without_bracketa}.
	 * @param ctx the parse tree
	 */
	void enterBody_without_bracketa(SQLParser.Body_without_bracketaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#body_without_bracketa}.
	 * @param ctx the parse tree
	 */
	void exitBody_without_bracketa(SQLParser.Body_without_bracketaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#json_object_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJson_object_stmt(SQLParser.Json_object_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#json_object_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJson_object_stmt(SQLParser.Json_object_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#json_body}.
	 * @param ctx the parse tree
	 */
	void enterJson_body(SQLParser.Json_bodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#json_body}.
	 * @param ctx the parse tree
	 */
	void exitJson_body(SQLParser.Json_bodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#property_json}.
	 * @param ctx the parse tree
	 */
	void enterProperty_json(SQLParser.Property_jsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#property_json}.
	 * @param ctx the parse tree
	 */
	void exitProperty_json(SQLParser.Property_jsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#array_json}.
	 * @param ctx the parse tree
	 */
	void enterArray_json(SQLParser.Array_jsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#array_json}.
	 * @param ctx the parse tree
	 */
	void exitArray_json(SQLParser.Array_jsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#get_property_json}.
	 * @param ctx the parse tree
	 */
	void enterGet_property_json(SQLParser.Get_property_jsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#get_property_json}.
	 * @param ctx the parse tree
	 */
	void exitGet_property_json(SQLParser.Get_property_jsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#set_prorerty_json}.
	 * @param ctx the parse tree
	 */
	void enterSet_prorerty_json(SQLParser.Set_prorerty_jsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#set_prorerty_json}.
	 * @param ctx the parse tree
	 */
	void exitSet_prorerty_json(SQLParser.Set_prorerty_jsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#json_object_name}.
	 * @param ctx the parse tree
	 */
	void enterJson_object_name(SQLParser.Json_object_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#json_object_name}.
	 * @param ctx the parse tree
	 */
	void exitJson_object_name(SQLParser.Json_object_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#return_stm}.
	 * @param ctx the parse tree
	 */
	void enterReturn_stm(SQLParser.Return_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#return_stm}.
	 * @param ctx the parse tree
	 */
	void exitReturn_stm(SQLParser.Return_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expr_condition}.
	 * @param ctx the parse tree
	 */
	void enterExpr_condition(SQLParser.Expr_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expr_condition}.
	 * @param ctx the parse tree
	 */
	void exitExpr_condition(SQLParser.Expr_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#condition_header}.
	 * @param ctx the parse tree
	 */
	void enterCondition_header(SQLParser.Condition_headerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#condition_header}.
	 * @param ctx the parse tree
	 */
	void exitCondition_header(SQLParser.Condition_headerContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIf_stmt(SQLParser.If_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#if_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIf_stmt(SQLParser.If_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#else_if}.
	 * @param ctx the parse tree
	 */
	void enterElse_if(SQLParser.Else_ifContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#else_if}.
	 * @param ctx the parse tree
	 */
	void exitElse_if(SQLParser.Else_ifContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#else_stmt}.
	 * @param ctx the parse tree
	 */
	void enterElse_stmt(SQLParser.Else_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#else_stmt}.
	 * @param ctx the parse tree
	 */
	void exitElse_stmt(SQLParser.Else_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#switch_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_stmt(SQLParser.Switch_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#switch_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_stmt(SQLParser.Switch_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#case_stm}.
	 * @param ctx the parse tree
	 */
	void enterCase_stm(SQLParser.Case_stmContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#case_stm}.
	 * @param ctx the parse tree
	 */
	void exitCase_stm(SQLParser.Case_stmContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#default_stm_switch}.
	 * @param ctx the parse tree
	 */
	void enterDefault_stm_switch(SQLParser.Default_stm_switchContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#default_stm_switch}.
	 * @param ctx the parse tree
	 */
	void exitDefault_stm_switch(SQLParser.Default_stm_switchContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#body_switch}.
	 * @param ctx the parse tree
	 */
	void enterBody_switch(SQLParser.Body_switchContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#body_switch}.
	 * @param ctx the parse tree
	 */
	void exitBody_switch(SQLParser.Body_switchContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#exp_for}.
	 * @param ctx the parse tree
	 */
	void enterExp_for(SQLParser.Exp_forContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#exp_for}.
	 * @param ctx the parse tree
	 */
	void exitExp_for(SQLParser.Exp_forContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#exp_i}.
	 * @param ctx the parse tree
	 */
	void enterExp_i(SQLParser.Exp_iContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#exp_i}.
	 * @param ctx the parse tree
	 */
	void exitExp_i(SQLParser.Exp_iContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#incr_decr_without_comma}.
	 * @param ctx the parse tree
	 */
	void enterIncr_decr_without_comma(SQLParser.Incr_decr_without_commaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#incr_decr_without_comma}.
	 * @param ctx the parse tree
	 */
	void exitIncr_decr_without_comma(SQLParser.Incr_decr_without_commaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFor_stmt(SQLParser.For_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFor_stmt(SQLParser.For_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#declare_var_for}.
	 * @param ctx the parse tree
	 */
	void enterDeclare_var_for(SQLParser.Declare_var_forContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#declare_var_for}.
	 * @param ctx the parse tree
	 */
	void exitDeclare_var_for(SQLParser.Declare_var_forContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#for_condition}.
	 * @param ctx the parse tree
	 */
	void enterFor_condition(SQLParser.For_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#for_condition}.
	 * @param ctx the parse tree
	 */
	void exitFor_condition(SQLParser.For_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#for_process_var}.
	 * @param ctx the parse tree
	 */
	void enterFor_process_var(SQLParser.For_process_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#for_process_var}.
	 * @param ctx the parse tree
	 */
	void exitFor_process_var(SQLParser.For_process_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#min_var}.
	 * @param ctx the parse tree
	 */
	void enterMin_var(SQLParser.Min_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#min_var}.
	 * @param ctx the parse tree
	 */
	void exitMin_var(SQLParser.Min_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#plus_var}.
	 * @param ctx the parse tree
	 */
	void enterPlus_var(SQLParser.Plus_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#plus_var}.
	 * @param ctx the parse tree
	 */
	void exitPlus_var(SQLParser.Plus_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#foreach}.
	 * @param ctx the parse tree
	 */
	void enterForeach(SQLParser.ForeachContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#foreach}.
	 * @param ctx the parse tree
	 */
	void exitForeach(SQLParser.ForeachContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#foreach_header}.
	 * @param ctx the parse tree
	 */
	void enterForeach_header(SQLParser.Foreach_headerContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#foreach_header}.
	 * @param ctx the parse tree
	 */
	void exitForeach_header(SQLParser.Foreach_headerContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#if_line_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIf_line_stmt(SQLParser.If_line_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#if_line_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIf_line_stmt(SQLParser.If_line_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#if_line_first_parameter}.
	 * @param ctx the parse tree
	 */
	void enterIf_line_first_parameter(SQLParser.If_line_first_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#if_line_first_parameter}.
	 * @param ctx the parse tree
	 */
	void exitIf_line_first_parameter(SQLParser.If_line_first_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#if_line_second_parameter}.
	 * @param ctx the parse tree
	 */
	void enterIf_line_second_parameter(SQLParser.If_line_second_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#if_line_second_parameter}.
	 * @param ctx the parse tree
	 */
	void exitIf_line_second_parameter(SQLParser.If_line_second_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(SQLParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(SQLParser.PrintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expr_var}.
	 * @param ctx the parse tree
	 */
	void enterExpr_var(SQLParser.Expr_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expr_var}.
	 * @param ctx the parse tree
	 */
	void exitExpr_var(SQLParser.Expr_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#declare_variable}.
	 * @param ctx the parse tree
	 */
	void enterDeclare_variable(SQLParser.Declare_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#declare_variable}.
	 * @param ctx the parse tree
	 */
	void exitDeclare_variable(SQLParser.Declare_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#declare_array}.
	 * @param ctx the parse tree
	 */
	void enterDeclare_array(SQLParser.Declare_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#declare_array}.
	 * @param ctx the parse tree
	 */
	void exitDeclare_array(SQLParser.Declare_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#schema_array}.
	 * @param ctx the parse tree
	 */
	void enterSchema_array(SQLParser.Schema_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#schema_array}.
	 * @param ctx the parse tree
	 */
	void exitSchema_array(SQLParser.Schema_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#array_assigne}.
	 * @param ctx the parse tree
	 */
	void enterArray_assigne(SQLParser.Array_assigneContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#array_assigne}.
	 * @param ctx the parse tree
	 */
	void exitArray_assigne(SQLParser.Array_assigneContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#element_arr_2}.
	 * @param ctx the parse tree
	 */
	void enterElement_arr_2(SQLParser.Element_arr_2Context ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#element_arr_2}.
	 * @param ctx the parse tree
	 */
	void exitElement_arr_2(SQLParser.Element_arr_2Context ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#element_arr}.
	 * @param ctx the parse tree
	 */
	void enterElement_arr(SQLParser.Element_arrContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#element_arr}.
	 * @param ctx the parse tree
	 */
	void exitElement_arr(SQLParser.Element_arrContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#declare_array_vr}.
	 * @param ctx the parse tree
	 */
	void enterDeclare_array_vr(SQLParser.Declare_array_vrContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#declare_array_vr}.
	 * @param ctx the parse tree
	 */
	void exitDeclare_array_vr(SQLParser.Declare_array_vrContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#expr_while}.
	 * @param ctx the parse tree
	 */
	void enterExpr_while(SQLParser.Expr_whileContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#expr_while}.
	 * @param ctx the parse tree
	 */
	void exitExpr_while(SQLParser.Expr_whileContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#op1}.
	 * @param ctx the parse tree
	 */
	void enterOp1(SQLParser.Op1Context ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#op1}.
	 * @param ctx the parse tree
	 */
	void exitOp1(SQLParser.Op1Context ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#while_stmt}.
	 * @param ctx the parse tree
	 */
	void enterWhile_stmt(SQLParser.While_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#while_stmt}.
	 * @param ctx the parse tree
	 */
	void exitWhile_stmt(SQLParser.While_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#do_while_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDo_while_stmt(SQLParser.Do_while_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#do_while_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDo_while_stmt(SQLParser.Do_while_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#call_func}.
	 * @param ctx the parse tree
	 */
	void enterCall_func(SQLParser.Call_funcContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#call_func}.
	 * @param ctx the parse tree
	 */
	void exitCall_func(SQLParser.Call_funcContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#aggregation_func}.
	 * @param ctx the parse tree
	 */
	void enterAggregation_func(SQLParser.Aggregation_funcContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#aggregation_func}.
	 * @param ctx the parse tree
	 */
	void exitAggregation_func(SQLParser.Aggregation_funcContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#heigher_order_function}.
	 * @param ctx the parse tree
	 */
	void enterHeigher_order_function(SQLParser.Heigher_order_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#heigher_order_function}.
	 * @param ctx the parse tree
	 */
	void exitHeigher_order_function(SQLParser.Heigher_order_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#args_list_higher}.
	 * @param ctx the parse tree
	 */
	void enterArgs_list_higher(SQLParser.Args_list_higherContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#args_list_higher}.
	 * @param ctx the parse tree
	 */
	void exitArgs_list_higher(SQLParser.Args_list_higherContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void enterArgument_list(SQLParser.Argument_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#argument_list}.
	 * @param ctx the parse tree
	 */
	void exitArgument_list(SQLParser.Argument_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#aggr_argument_list}.
	 * @param ctx the parse tree
	 */
	void enterAggr_argument_list(SQLParser.Aggr_argument_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#aggr_argument_list}.
	 * @param ctx the parse tree
	 */
	void exitAggr_argument_list(SQLParser.Aggr_argument_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#bool_ex}.
	 * @param ctx the parse tree
	 */
	void enterBool_ex(SQLParser.Bool_exContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#bool_ex}.
	 * @param ctx the parse tree
	 */
	void exitBool_ex(SQLParser.Bool_exContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 */
	void enterForeign_key_clause(SQLParser.Foreign_key_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 */
	void exitForeign_key_clause(SQLParser.Foreign_key_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 */
	void enterFk_target_column_name(SQLParser.Fk_target_column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 */
	void exitFk_target_column_name(SQLParser.Fk_target_column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#indexed_column}.
	 * @param ctx the parse tree
	 */
	void enterIndexed_column(SQLParser.Indexed_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#indexed_column}.
	 * @param ctx the parse tree
	 */
	void exitIndexed_column(SQLParser.Indexed_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint(SQLParser.Table_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint(SQLParser.Table_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_primary_key(SQLParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_primary_key(SQLParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_foreign_key(SQLParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_foreign_key(SQLParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_unique(SQLParser.Table_constraint_uniqueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_unique(SQLParser.Table_constraint_uniqueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_constraint_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_key(SQLParser.Table_constraint_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_constraint_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_key(SQLParser.Table_constraint_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 */
	void enterFk_origin_column_name(SQLParser.Fk_origin_column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 */
	void exitFk_origin_column_name(SQLParser.Fk_origin_column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#qualified_table_name}.
	 * @param ctx the parse tree
	 */
	void enterQualified_table_name(SQLParser.Qualified_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#qualified_table_name}.
	 * @param ctx the parse tree
	 */
	void exitQualified_table_name(SQLParser.Qualified_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void enterOrdering_term(SQLParser.Ordering_termContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void exitOrdering_term(SQLParser.Ordering_termContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#pragma_value}.
	 * @param ctx the parse tree
	 */
	void enterPragma_value(SQLParser.Pragma_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#pragma_value}.
	 * @param ctx the parse tree
	 */
	void exitPragma_value(SQLParser.Pragma_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#common_table_expression}.
	 * @param ctx the parse tree
	 */
	void enterCommon_table_expression(SQLParser.Common_table_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#common_table_expression}.
	 * @param ctx the parse tree
	 */
	void exitCommon_table_expression(SQLParser.Common_table_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#result_column}.
	 * @param ctx the parse tree
	 */
	void enterResult_column(SQLParser.Result_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#result_column}.
	 * @param ctx the parse tree
	 */
	void exitResult_column(SQLParser.Result_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_or_subquery}.
	 * @param ctx the parse tree
	 */
	void enterTable_or_subquery(SQLParser.Table_or_subqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_or_subquery}.
	 * @param ctx the parse tree
	 */
	void exitTable_or_subquery(SQLParser.Table_or_subqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#join_clause}.
	 * @param ctx the parse tree
	 */
	void enterJoin_clause(SQLParser.Join_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#join_clause}.
	 * @param ctx the parse tree
	 */
	void exitJoin_clause(SQLParser.Join_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#join_operator}.
	 * @param ctx the parse tree
	 */
	void enterJoin_operator(SQLParser.Join_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#join_operator}.
	 * @param ctx the parse tree
	 */
	void exitJoin_operator(SQLParser.Join_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#join_constraint}.
	 * @param ctx the parse tree
	 */
	void enterJoin_constraint(SQLParser.Join_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#join_constraint}.
	 * @param ctx the parse tree
	 */
	void exitJoin_constraint(SQLParser.Join_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#select_core}.
	 * @param ctx the parse tree
	 */
	void enterSelect_core(SQLParser.Select_coreContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#select_core}.
	 * @param ctx the parse tree
	 */
	void exitSelect_core(SQLParser.Select_coreContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#create_type_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_type_stmt(SQLParser.Create_type_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#create_type_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_type_stmt(SQLParser.Create_type_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#create_type_name}.
	 * @param ctx the parse tree
	 */
	void enterCreate_type_name(SQLParser.Create_type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#create_type_name}.
	 * @param ctx the parse tree
	 */
	void exitCreate_type_name(SQLParser.Create_type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_def_type}.
	 * @param ctx the parse tree
	 */
	void enterColumn_def_type(SQLParser.Column_def_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_def_type}.
	 * @param ctx the parse tree
	 */
	void exitColumn_def_type(SQLParser.Column_def_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#cte_table_name}.
	 * @param ctx the parse tree
	 */
	void enterCte_table_name(SQLParser.Cte_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#cte_table_name}.
	 * @param ctx the parse tree
	 */
	void exitCte_table_name(SQLParser.Cte_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#signed_number}.
	 * @param ctx the parse tree
	 */
	void enterSigned_number(SQLParser.Signed_numberContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#signed_number}.
	 * @param ctx the parse tree
	 */
	void exitSigned_number(SQLParser.Signed_numberContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void enterLiteral_value(SQLParser.Literal_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void exitLiteral_value(SQLParser.Literal_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#literal_value_json}.
	 * @param ctx the parse tree
	 */
	void enterLiteral_value_json(SQLParser.Literal_value_jsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#literal_value_json}.
	 * @param ctx the parse tree
	 */
	void exitLiteral_value_json(SQLParser.Literal_value_jsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(SQLParser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(SQLParser.Unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#error_message}.
	 * @param ctx the parse tree
	 */
	void enterError_message(SQLParser.Error_messageContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#error_message}.
	 * @param ctx the parse tree
	 */
	void exitError_message(SQLParser.Error_messageContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#module_argument}.
	 * @param ctx the parse tree
	 */
	void enterModule_argument(SQLParser.Module_argumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#module_argument}.
	 * @param ctx the parse tree
	 */
	void exitModule_argument(SQLParser.Module_argumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void enterColumn_alias(SQLParser.Column_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void exitColumn_alias(SQLParser.Column_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterKeyword(SQLParser.KeywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitKeyword(SQLParser.KeywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#unknown}.
	 * @param ctx the parse tree
	 */
	void enterUnknown(SQLParser.UnknownContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#unknown}.
	 * @param ctx the parse tree
	 */
	void exitUnknown(SQLParser.UnknownContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(SQLParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(SQLParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#function_name}.
	 * @param ctx the parse tree
	 */
	void enterFunction_name(SQLParser.Function_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#function_name}.
	 * @param ctx the parse tree
	 */
	void exitFunction_name(SQLParser.Function_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#database_name}.
	 * @param ctx the parse tree
	 */
	void enterDatabase_name(SQLParser.Database_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#database_name}.
	 * @param ctx the parse tree
	 */
	void exitDatabase_name(SQLParser.Database_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#source_table_name}.
	 * @param ctx the parse tree
	 */
	void enterSource_table_name(SQLParser.Source_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#source_table_name}.
	 * @param ctx the parse tree
	 */
	void exitSource_table_name(SQLParser.Source_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_name(SQLParser.Table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_name(SQLParser.Table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_or_index_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_or_index_name(SQLParser.Table_or_index_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_or_index_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_or_index_name(SQLParser.Table_or_index_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#new_table_name}.
	 * @param ctx the parse tree
	 */
	void enterNew_table_name(SQLParser.New_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#new_table_name}.
	 * @param ctx the parse tree
	 */
	void exitNew_table_name(SQLParser.New_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#column_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name(SQLParser.Column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#column_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name(SQLParser.Column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void enterCollation_name(SQLParser.Collation_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void exitCollation_name(SQLParser.Collation_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#foreign_table}.
	 * @param ctx the parse tree
	 */
	void enterForeign_table(SQLParser.Foreign_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#foreign_table}.
	 * @param ctx the parse tree
	 */
	void exitForeign_table(SQLParser.Foreign_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#index_name}.
	 * @param ctx the parse tree
	 */
	void enterIndex_name(SQLParser.Index_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#index_name}.
	 * @param ctx the parse tree
	 */
	void exitIndex_name(SQLParser.Index_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#trigger_name}.
	 * @param ctx the parse tree
	 */
	void enterTrigger_name(SQLParser.Trigger_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#trigger_name}.
	 * @param ctx the parse tree
	 */
	void exitTrigger_name(SQLParser.Trigger_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#view_name}.
	 * @param ctx the parse tree
	 */
	void enterView_name(SQLParser.View_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#view_name}.
	 * @param ctx the parse tree
	 */
	void exitView_name(SQLParser.View_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#module_name}.
	 * @param ctx the parse tree
	 */
	void enterModule_name(SQLParser.Module_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#module_name}.
	 * @param ctx the parse tree
	 */
	void exitModule_name(SQLParser.Module_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#pragma_name}.
	 * @param ctx the parse tree
	 */
	void enterPragma_name(SQLParser.Pragma_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#pragma_name}.
	 * @param ctx the parse tree
	 */
	void exitPragma_name(SQLParser.Pragma_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#savepoint_name}.
	 * @param ctx the parse tree
	 */
	void enterSavepoint_name(SQLParser.Savepoint_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#savepoint_name}.
	 * @param ctx the parse tree
	 */
	void exitSavepoint_name(SQLParser.Savepoint_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#table_alias}.
	 * @param ctx the parse tree
	 */
	void enterTable_alias(SQLParser.Table_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#table_alias}.
	 * @param ctx the parse tree
	 */
	void exitTable_alias(SQLParser.Table_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#transaction_name}.
	 * @param ctx the parse tree
	 */
	void enterTransaction_name(SQLParser.Transaction_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#transaction_name}.
	 * @param ctx the parse tree
	 */
	void exitTransaction_name(SQLParser.Transaction_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link SQLParser#any_name}.
	 * @param ctx the parse tree
	 */
	void enterAny_name(SQLParser.Any_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link SQLParser#any_name}.
	 * @param ctx the parse tree
	 */
	void exitAny_name(SQLParser.Any_nameContext ctx);
}