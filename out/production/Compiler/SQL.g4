/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 by Bart Kiers
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Project      : sqlite-parser; an ANTLR4 grammar for SQLite
 *                https://github.com/bkiers/sqlite-parser
 * Developed by : Bart Kiers, bart@big-o.nl
 */
grammar SQL;

parse
 : ( function | sql_stmt_list | error )* EOF
 ;

error
 : UNEXPECTED_CHAR
   {
     throw new RuntimeException("UNEXPECTED_CHAR=" + $UNEXPECTED_CHAR.text);
   }
 ;

sql_stmt_list
 : ';'* sql_stmt ( ';'+ sql_stmt )* ';'*
 ;

sql_stmt
 //: ( K_EXPLAIN ( K_QUERY K_PLAN )? )? ( alter_table_stmt
 :                                     ( alter_table_stmt
//                                      | analyze_stmt
//                                      | attach_stmt
//                                      | begin_stmt
//                                      | commit_stmt
//                                      | compound_select_stmt
//                                      | create_index_stmt
                                      | create_table_stmt
  //                                    | create_trigger_stmt
  //                                    | create_view_stmt
//                                      | create_virtual_table_stmt
                                      | delete_stmt
//                                      | delete_stmt_limited
//                                      | detach_stmt
//                                      | drop_index_stmt
                                      | drop_table_stmt
//                                      | drop_trigger_stmt
//                                      | drop_view_stmt
                                      | factored_select_stmt
                                      | insert_stmt
//                                      | pragma_stmt
//                                      | reindex_stmt
//                                      | release_stmt
//                                      | rollback_stmt
//                                      | savepoint_stmt
//                                      | simple_select_stmt
//                                      | select_stmt
                                      | update_stmt
//                                      | update_stmt_limited
//                                      | vacuum_stmt )
                                      |create_aggregation_function

                                      |create_type_stmt

                                      )
 ;

alter_table_stmt
 //: K_ALTER K_TABLE K_ONLY? ( database_name '.' )? source_table_name
   : K_ALTER K_TABLE  ( database_name '.' )? source_table_name
   ( K_RENAME K_TO new_table_name
   | alter_table_add
   | alter_table_add_constraint
   | K_ADD K_COLUMN? column_def
   )
 //  K_ENABLE? (unknown)?
 ;

alter_table_add_constraint
 : K_ADD K_CONSTRAINT any_name table_constraint
 ;

alter_table_add
 : K_ADD table_constraint
 ;

//analyze_stmt
// : K_ANALYZE ( database_name | table_or_index_name | database_name '.' table_or_index_name )?
// ;

//attach_stmt
// : K_ATTACH K_DATABASE? expr K_AS database_name
// ;

//begin_stmt
// : K_BEGIN ( K_DEFERRED | K_IMMEDIATE | K_EXCLUSIVE )? ( K_TRANSACTION transaction_name? )?
// ;

//commit_stmt
// : ( K_COMMIT | K_END ) ( K_TRANSACTION transaction_name? )?
// ;
//
//compound_select_stmt
// : ( K_WITH K_RECURSIVE? common_table_expression ( ',' common_table_expression )* )?
//   select_core ( ( K_UNION K_ALL? | K_INTERSECT | K_EXCEPT ) select_core )+
//   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
//   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
// ;
//
//create_index_stmt
// : K_CREATE K_UNIQUE? K_INDEX ( K_IF K_NOT K_EXISTS )?
//   ( database_name '.' )? index_name K_ON table_name '(' indexed_column ( ',' indexed_column )* ')'
//   ( K_WHERE expr )?
// ;

create_table_stmt
// : K_CREATE ( K_TEMP | K_TEMPORARY )? K_TABLE ( K_IF K_NOT K_EXISTS )?
   : K_CREATE  K_TABLE ( K_IF K_NOT K_EXISTS )?
   ( database_name '.' )? table_name
 //  ( '(' column_def ( ',' table_constraint | ',' column_def )* ')' ( K_WITHOUT IDENTIFIER )?
   ( '(' column_def ( ',' table_constraint | ',' column_def )* ')' (type_create_table)? (path_create_table)?
   | K_AS select_stmt
//   ) (unknown)?
   )
 ;

 type_create_table:
 K_TYPE '=' IDENTIFIER
 ;

 path_create_table:
  K_PATH '=' IDENTIFIER
 ;

//create aggregation function
create_aggregation_function:

K_CREATE K_AGGREGATION_FUNC function_name '('(jar_path ',' class_name ',' method_name ',' return_type ',' schema_array)?')'

;

jar_path:
 IDENTIFIER
;

class_name:
  IDENTIFIER
;

return_type:
IDENTIFIER
;

method_name:
  IDENTIFIER
;
//end


//create_trigger_stmt
// : K_CREATE ( K_TEMP | K_TEMPORARY )? K_TRIGGER ( K_IF K_NOT K_EXISTS )?
//   ( database_name '.' )? trigger_name ( K_BEFORE  | K_AFTER | K_INSTEAD K_OF )?
//   ( K_DELETE | K_INSERT | K_UPDATE ( K_OF column_name ( ',' column_name )* )? ) K_ON ( database_name '.' )? table_name
//   ( K_FOR K_EACH K_ROW )? ( K_WHEN expr )?
//   K_BEGIN ( ( update_stmt | insert_stmt | delete_stmt | select_stmt ) ';' )+ K_END
// ;
//
//create_view_stmt
// : K_CREATE ( K_TEMP | K_TEMPORARY )? K_VIEW ( K_IF K_NOT K_EXISTS )?
//   ( database_name '.' )? view_name K_AS select_stmt
// ;
//
//create_virtual_table_stmt
// : K_CREATE K_VIRTUAL K_TABLE ( K_IF K_NOT K_EXISTS )?
//   ( database_name '.' )? table_name
//   K_USING module_name ( '(' module_argument ( ',' module_argument )* ')' )?
// ;

delete_stmt
// : with_clause? K_DELETE K_FROM qualified_table_name
 :  K_DELETE K_FROM qualified_table_name
   ( K_WHERE expr )?
 ;

//delete_stmt_limited
// : with_clause? K_DELETE K_FROM qualified_table_name
//   ( K_WHERE expr )?
//   ( ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
//     K_LIMIT expr ( ( K_OFFSET | ',' ) expr )?
//   )?
// ;
//
//detach_stmt
// : K_DETACH K_DATABASE? database_name
// ;
//
//drop_index_stmt
// : K_DROP K_INDEX ( K_IF K_EXISTS )? ( database_name '.' )? index_name
// ;

drop_table_stmt
 : K_DROP K_TABLE ( K_IF K_EXISTS )? ( database_name '.' )? table_name
 ;

//drop_trigger_stmt
// : K_DROP K_TRIGGER ( K_IF K_EXISTS )? ( database_name '.' )? trigger_name
// ;
//
//drop_view_stmt
// : K_DROP K_VIEW ( K_IF K_EXISTS )? ( database_name '.' )? view_name
// ;

factored_select_stmt
 :
// ( K_WITH K_RECURSIVE? common_table_expression ( ',' common_table_expression )* )?
    select_core
   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
 ;


assigne_select:
(((K_VAR )? IDENTIFIER)|declare_array
) '=' factored_select_stmt
;


insert_stmt
// : with_clause? ( K_INSERT
//                | K_REPLACE
//                | K_INSERT K_OR K_REPLACE
//                | K_INSERT K_OR K_ROLLBACK
//                | K_INSERT K_OR K_ABORT
//                | K_INSERT K_OR K_FAIL
//                | K_INSERT K_OR K_IGNORE ) K_INTO
                :   K_INSERT  K_INTO
   ( database_name '.' )? table_name ( '(' column_name ( ',' column_name )* ')' )?
   ( K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
   | factored_select_stmt
   | K_DEFAULT K_VALUES
   )
 ;

//pragma_stmt
// : K_PRAGMA ( database_name '.' )? pragma_name ( '=' pragma_value
//                                               | '(' pragma_value ')' )?
// ;
//
//reindex_stmt
// : K_REINDEX ( collation_name
//             | ( database_name '.' )? ( table_name | index_name )
//             )?
// ;
//
//release_stmt
// : K_RELEASE K_SAVEPOINT? savepoint_name
// ;
//
//rollback_stmt
// : K_ROLLBACK ( K_TRANSACTION transaction_name? )? ( K_TO K_SAVEPOINT? savepoint_name )?
// ;
//
//savepoint_stmt
// : K_SAVEPOINT savepoint_name
// ;
//
//simple_select_stmt
// : ( K_WITH K_RECURSIVE? common_table_expression ( ',' common_table_expression )* )?
//   select_core ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
//   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
// ;

select_stmt
 //: ( K_WITH K_RECURSIVE? common_table_expression ( ',' common_table_expression )* )?
//   select_or_values ( compound_operator select_or_values )*
 :  select_or_values
   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
 ;

select_or_values
 : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
   ( K_FROM ( table_or_subquery ( ',' table_or_subquery )* | join_clause ) )?
   ( K_WHERE expr )?
   ( K_GROUP K_BY expr ( ',' expr )* ( K_HAVING expr )? )?
 | K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
 ;

update_stmt
// : with_clause? K_UPDATE ( K_OR K_ROLLBACK
//                         | K_OR K_ABORT
//                         | K_OR K_REPLACE
//                         | K_OR K_FAIL
//                         | K_OR K_IGNORE )? qualified_table_name
                         :  K_UPDATE  qualified_table_name
   K_SET   update_first_parameter  update_second_parameter  ( K_WHERE expr )?
 ;


update_first_parameter:
column_name '=' expr
;


update_second_parameter :
( ',' column_name '=' expr )*
;

//update_stmt_limited
// : with_clause? K_UPDATE ( K_OR K_ROLLBACK
//                         | K_OR K_ABORT
//                         | K_OR K_REPLACE
//                         | K_OR K_FAIL
//                         | K_OR K_IGNORE )? qualified_table_name
//   K_SET column_name '=' expr ( ',' column_name '=' expr )* ( K_WHERE expr )?
//   ( ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
//     K_LIMIT expr ( ( K_OFFSET | ',' ) expr )?
//   )?
// ;
//
//vacuum_stmt
// : K_VACUUM
// ;

column_def
 : column_name ( column_constraint | type_name )*
 ;

type_name
 : name ( '(' signed_number (any_name)? ')'
         | '(' signed_number (any_name)? ',' signed_number (any_name)? ')' )?
 ;

column_constraint
 : ( K_CONSTRAINT name )?
   ( column_constraint_primary_key
   | column_constraint_foreign_key
   | column_constraint_not_null
   | column_constraint_null
  // | K_UNIQUE conflict_clause
    | K_UNIQUE
   | K_CHECK '(' expr ')'
   | column_default
   | K_COLLATE collation_name
   )
 ;

column_constraint_primary_key
 //: K_PRIMARY K_KEY ( K_ASC | K_DESC )? conflict_clause K_AUTOINCREMENT?
 : K_PRIMARY K_KEY ( K_ASC | K_DESC )?  K_AUTOINCREMENT?
 ;

column_constraint_foreign_key
 : foreign_key_clause
 ;

column_constraint_not_null
 //: K_NOT K_NULL conflict_clause
  : K_NOT K_NULL
 ;

column_constraint_null
// : K_NULL conflict_clause
: K_NULL
 ;

column_default
 : K_DEFAULT (column_default_value | '(' expr ')' | K_NEXTVAL '(' expr ')' | any_name )  ( '::' any_name+ )?
 ;

column_default_value
 : ( signed_number | literal_value )
 ;

//conflict_clause
// : ( K_ON K_CONFLICT ( K_ROLLBACK
//                     | K_ABORT
//                     | K_FAIL
//                     | K_IGNORE
//                     | K_REPLACE
//                     )
//   )?
// ;

/*
    SQLite understands the following binary operators, in order from highest to
    lowest precedence:

    ||
    *    /    %
    +    -
    <<   >>   &    |
    <    <=   >    >=
    =    ==   !=   <>   IS   IS NOT   IN   LIKE   GLOB   MATCH   REGEXP
    AND
    OR
*/
expr
 : literal_value
 |array_assigne
 | ( ( database_name '.' )? table_name '.' )? column_name
 | unary_operator expr
 |call_func
 |bool_ex
 | expr '||' expr
 | expr ('+=' | '-=' | '*=' | '/='|'%=') expr
 | expr ( '*' | '/' | '%' ) expr
 | expr ( '+' | '-' ) expr
 | expr ( '<<' | '>>' | '&' | '|' ) expr
 | expr ( '<' | '<=' | '>' | '>=' ) expr
 | expr ( '=' | '==' | '!=' | '<>' | K_IS | K_IS K_NOT  | K_LIKE | K_GLOB | K_MATCH | K_REGEXP ) expr
 | expr K_AND expr
 | expr K_OR expr
 | function_name '(' ( K_DISTINCT? expr ( ',' expr )* | '*' )? ')'
 | '(' expr ')'
 |incr_decr_without_comma
 | expr K_NOT? K_IN ( '(' ( factored_select_stmt
                          | expr ( ',' expr )*
                          )?
                      ')'
                    | ( database_name '.' )? table_name
                     )
 | ( ( K_NOT )? K_EXISTS )? '(' factored_select_stmt ')'

 ;

 expr_test
  : literal_value
  |IDENTIFIER
 // | BIND_PARAMETER
  |array_assigne
  |get_property_json
  | unary_operator expr
  | call_func
  |bool_ex
  | expr_test '||' expr_test
  | expr_test ('+=' | '-=' | '*=' | '/='|'%=') expr_test
  | expr_test ( '*' | '/' | '%' ) expr_test
  | expr_test ( '+' | '-' ) expr_test
  | expr_test ( '<<' | '>>' | '&' | '|' ) expr_test
  | expr_test ( '<' | '<=' | '>' | '>=' ) expr_test
  | expr_test ( '=' | '==' | '!=' | '<>' | K_IS | K_IS K_NOT | K_IN | K_LIKE | K_GLOB | K_MATCH | K_REGEXP ) expr_test
  | expr_test K_AND expr_test
  | expr_test K_OR expr_test

  | function_name '(' ( K_DISTINCT? expr_test ( ',' expr_test )* | '*' )? ')'
  | '(' expr_test ')'
  |incr_decr_without_comma
 // | K_CAST '(' expr K_AS type_name ')'
 // | expr K_COLLATE collation_name
 // | expr K_NOT? ( K_LIKE | K_GLOB | K_REGEXP | K_MATCH ) expr ( K_ESCAPE expr )?
 // | expr ( K_ISNULL | K_NOTNULL | K_NOT K_NULL )
 // | expr K_IS K_NOT? expr
 // | expr K_NOT? K_BETWEEN expr K_AND expr
  | expr_test K_NOT? K_IN ( '(' ( factored_select_stmt
                           | expr_test ( ',' expr_test )*
                           )?
                       ')'
                     | ( database_name '.' )? table_name )
  | ( ( K_NOT )? K_EXISTS )? '(' factored_select_stmt ')'
 // | K_CASE expr? ( K_WHEN expr K_THEN expr )+ ( K_ELSE expr )? K_END
 // | raise_function

  ;
 /////////////////////////////////////////////// Start of Added ////////////////////////////////////////////////
 ///
  fun_name:
  IDENTIFIER
  ;



 parameter_list:
(( define_var_assigne(',' define_var_assigne)*)?| define_var )? | ( define_var (','  define_var )*(','define_var_assigne)* )?

 //(( K_VAR IDENTIFIER '='(IDENTIFIER|DIGIT|NUMERIC_LITERAL|literal_value|K_TRUE|K_FALSE|expr_var)(',' K_VAR IDENTIFIER '='(IDENTIFIER|DIGIT|NUMERIC_LITERAL|literal_value|K_TRUE|K_FALSE|expr_var))*)?| K_VAR IDENTIFIER )? | ( K_VAR IDENTIFIER (','  K_VAR IDENTIFIER )*(','K_VAR IDENTIFIER '='(IDENTIFIER|DIGIT|NUMERIC_LITERAL|literal_value|K_TRUE|K_FALSE||expr_var))* )?
 ;
define_var:

K_VAR IDENTIFIER

;
define_var_assigne:
((K_VAR)? IDENTIFIER '='(IDENTIFIER|bool_ex|expr_var)|assigne_select)
;
/// Function
function:
  fun_name '(' parameter_list ')'
  '{'
      body
      //(return_stm)?
  '}'
;
block:
'{'
  body?
'}'
;

body:
( if_stmt|if_line_stmt';'|json_object_stmt ';'|set_prorerty_json';'|switch_stmt | min_var | plus_var | for_stmt | foreach |print';' | define_var ';'|call_func ';'|define_var_assigne ';'| while_stmt |do_while_stmt|block |expr';'|declare_array';'|declare_array_vr';'|sql_stmt_list|assigne_select';')*
(return_stm)?
;

body_without_bracketa:
( if_stmt|if_line_stmt';'|json_object_stmt ';'|set_prorerty_json';'| switch_stmt | min_var | plus_var | for_stmt | foreach | print';'| define_var ';'|call_func ';'|define_var_assigne ';'| while_stmt |do_while_stmt|expr';'|declare_array';'|declare_array_vr';'|return_stm|sql_stmt_list|assigne_select';')
;




//json opject
json_object_stmt:

(K_VAR)? IDENTIFIER '='

json_body

;

json_body:
  '{'

 //(property_json)?

 //(property_json ',' property_json)*(',' property_json)*

 ((property_json)(','property_json)*)?

  '}'
;

property_json:
 (IDENTIFIER) ':'  expr_var    //(literal_value |IDENTIFIER)
 |(IDENTIFIER) ':' array_json
 |(IDENTIFIER) ':' json_body
;

array_json:
// '('literal_value_json? (literal_value_json','literal_value_json)* (','literal_value_json)* ')'|
//'('json_body? (json_body','json_body)* (','json_body)* ')'
('['((literal_value_json)(','literal_value_json)*)?']')|
('['((json_body)(','json_body)*)?']')
;

get_property_json:
json_object_name '.' json_object_name ('.' json_object_name)*
;

set_prorerty_json:
get_property_json '=' expr_var
;

json_object_name:
IDENTIFIER
;
/////////////






////return
return_stm:
 K_RETURN ( if_line_stmt| bool_ex | expr_test )? ';'    //replace expr with expr_test 2020
 ;
/*
block:
('{' block '}')*
;*/


expr_condition
:NUMERIC_LITERAL
 |literal_value
 |get_property_json
 | IDENTIFIER
 |array_assigne
 | DIGIT
 |'(' expr_condition ')'
 | unary_operator NUMERIC_LITERAL
 | expr_condition ( '*' | '/' | '%' ) expr_condition
 | expr_condition ( '+' | '-' ) expr_condition
 |incr_decr_without_comma
 |call_func
;
condition_header:
 '('condition_header')'
 |IDENTIFIER
 |expr_var( '<' | '<=' | '>' | '>=' | '==' | '=' | '!=' ) expr_var
 |condition_header ( K_AND | K_OR ) condition_header
 |condition_header ( AMP2 | PIPE2 ) condition_header
 |bool_ex
;


///if
 if_stmt:
 K_IF '(' (condition_header) ')'
 (
 '{' (body)'}'
 | (body_without_bracketa)
)
(else_if)*
 (else_stmt)?
 ;
 else_if:
  ( K_ELSE K_IF '('  (condition_header) ')'
   ( '{' (body)'}'
   | (body_without_bracketa))
   )
 ;
 else_stmt:
 (K_ELSE  ('{' (body)'}'
           | (body_without_bracketa))
           )
 ;



 //Switch
switch_stmt :
K_SWITCH '(' (expr_var |call_func|if_line_stmt|define_var_assigne) ')'
'{'
    body_switch
'}'
;

case_stm:
K_CASE (literal_value |any_name)  ':'
     body
    (( K_BREAK ';')?)
;
default_stm_switch:
 K_DEFAULT ':'
     body
     (( K_BREAK ';')?)
;
body_switch:
 ( case_stm
  )*

 (
default_stm_switch
  )?
;

/*exp_for:
   IDENTIFIER '='  (DIGIT|literal_value|incr_decr_without_comma | exp_i )
 | IDENTIFIER '+=' (DIGIT|literal_value|incr_decr_without_comma | exp_i )
 | IDENTIFIER '-=' (DIGIT|literal_value|incr_decr_without_comma | exp_i )
 | IDENTIFIER '*=' (DIGIT|literal_value|incr_decr_without_comma | exp_i )
 | IDENTIFIER '/=' (DIGIT|literal_value|incr_decr_without_comma | exp_i )
;



exp_i:
( (literal_value|incr_decr_without_comma)  ('+' | '-' | '/' |'*')    )* ( (IDENTIFIER|literal_value|incr_decr_without_comma) ('+' | '-' | '/' |'*') (IDENTIFIER|literal_value|incr_decr_without_comma) )+

;*/


exp_for:
   IDENTIFIER '='  (DIGIT|literal_value|incr_decr_without_comma | exp_i | '(' exp_i ')' | '(' incr_decr_without_comma ')' | '(' literal_value ')')
 | IDENTIFIER '+=' (DIGIT|literal_value|incr_decr_without_comma | exp_i  | '(' exp_i ')' | '(' incr_decr_without_comma ')' | '(' literal_value ')')
 | IDENTIFIER '-=' (DIGIT|literal_value|incr_decr_without_comma | exp_i | '(' exp_i ')' | '(' incr_decr_without_comma ')' | '(' literal_value ')')
 | IDENTIFIER '*=' (DIGIT|literal_value|incr_decr_without_comma | exp_i | '(' exp_i ')' | '(' incr_decr_without_comma ')' | '(' literal_value ')')
 | IDENTIFIER '/=' (DIGIT|literal_value|incr_decr_without_comma | exp_i | '(' exp_i ')' | '(' incr_decr_without_comma ')' | '(' literal_value ')')

;

exp_i:
( (literal_value|incr_decr_without_comma | '(' literal_value ')'| '('incr_decr_without_comma ')')  ('+' | '-' | '/' |'*')  )*
 ( (IDENTIFIER|literal_value|incr_decr_without_comma | '(' IDENTIFIER ')'|'('literal_value')'|'('incr_decr_without_comma')' | '(' exp_i ')') ('+' | '-' | '/' |'*') (IDENTIFIER|literal_value|incr_decr_without_comma |'('IDENTIFIER')'|'('literal_value')'|'('incr_decr_without_comma')' |'(' exp_i ')') )+


;

//PLUS_PLUS : '++';
//MINUS_MINUS : '--';
// i++  ++i i-- --i
 incr_decr_without_comma:
  (IDENTIFIER|array_assigne|get_property_json) MINUS_MINUSl|(IDENTIFIER|array_assigne|get_property_json) PLUS_PLUSl| MINUS_MINUSr (IDENTIFIER|array_assigne|get_property_json) |PLUS_PLUSr (IDENTIFIER|array_assigne|get_property_json)
  ;

/*
//plus as i= x + ++i; | i+=i++; | i=i++; | i+=i+i+i
plus_or_min:
IDENTIFIER '=' ( (IDENTIFIER ( '+' | '-'| '*' | '/' ) incr_decr_without_comma) | (incr_decr_without_comma)  )
| (incr_decr_without_comma) ('+' | '-' | '/' |'*') (incr_decr_without_comma | IDENTIFIER) ';'
;*/

//for
  for_stmt:
     K_FOR '('( declare_var_for  ';'
                 for_condition';'
                for_process_var ) ')'
     ('{'

        body

     '}'
     |body_without_bracketa
     )
  ;

declare_var_for:
((K_VAR)? IDENTIFIER ('=')(expr_var) )?
;

for_condition:
( (expr_var) ('<' | '<=' | '>' | '>=' | '==' | '=' | '!=') (expr_var) )?
;


for_process_var:
(incr_decr_without_comma|exp_for)?
;


//PLUS_PLUS : '++';
//MINUS_MINUS : '--';
//minus variable
min_var:
((IDENTIFIER|NUMERIC_LITERAL|array_assigne|get_property_json) MINUS_MINUSl';'
| (IDENTIFIER|NUMERIC_LITERAL|array_assigne|get_property_json) MINUS_MINUSl|  MINUS_MINUSr (IDENTIFIER|NUMERIC_LITERAL|array_assigne|get_property_json) (';')?)
;


//plus variable
plus_var:
((IDENTIFIER|NUMERIC_LITERAL|array_assigne|get_property_json) PLUS_PLUSl  (';')?
|  PLUS_PLUSr (IDENTIFIER|NUMERIC_LITERAL|array_assigne|get_property_json) (';')? )
;



//foreach
 foreach:
  K_FOR '(' foreach_header ')'
  ('{'
     body
  '}'
  | body_without_bracketa
  )
 ;

foreach_header:
  (K_VAR)? IDENTIFIER ':' IDENTIFIER
;

//if_line_stmt:
//  (K_VAR IDENTIFIER '=')? condition_header '?' (expr_condition|print|if_line_stmt) ':' (expr_condition|print|if_line_stmt)|
//  '('if_line_stmt')'
//;

if_line_stmt:
  (define_var '=')? condition_header '?' if_line_first_parameter ':' if_line_second_parameter
;

if_line_first_parameter:
 (expr_var|print|if_line_stmt)
 |'('if_line_stmt')'
;

if_line_second_parameter:
   (expr_var|print|if_line_stmt)
   |'('if_line_stmt')'
;


//print
 print:
K_PRINT '('  (get_property_json|expr_test ) ')'  //replace expr with expr_test 2020
 ;

/////
expr_var     //arithmatic
: IDENTIFIER
 |array_assigne
// |( ( database_name '.' )? table_name '.' )? column_name  //my new add 2020
 |get_property_json
 |literal_value
 |call_func
 |'(' expr_var ')'
 | unary_operator expr_var
 | expr_var ( '*' | '/' | '%' ) expr_var
 | expr_var ( '+' | '-' ) expr_var
 |incr_decr_without_comma
 /*| K_TRUE
 | K_FALSE*/
;


///declar var
 declare_variable:
    K_VAR IDENTIFIER  ('=' (expr_var|sql_stmt| bool_ex)  )?
 ;

//declare arr
declare_array:
 K_VAR IDENTIFIER  ( ('['(expr_var)']')|('[]'))
| K_VAR IDENTIFIER ( ('['(expr_var)']''['(expr_var)']')|('[]''[]'))
;
schema_array:
'[' (IDENTIFIER ',')*IDENTIFIER ']'
;
array_assigne:
IDENTIFIER (('['(expr_var)']')|('[]')) | IDENTIFIER (('['(expr_var)']''['(expr_var)']'))
;
///array
 element_arr_2:
 ( element_arr (','  element_arr )* )?
 ;
element_arr
 :
 bool_ex
 |expr_var
 ;

declare_array_vr:
K_VAR IDENTIFIER '[]' ('=' '{'( element_arr (','  element_arr )* )? '}')
|K_VAR IDENTIFIER '[]' '[]' ('=' '{'
                                       ('{'element_arr_2'}')(',''{'element_arr_2'}')*
                                   '}'
                                   )
;





expr_while
:NUMERIC_LITERAL
 | DIGIT
 |'(' expr_while ')'
/* | unary_operator NUMERIC_LITERAL
 | expr_while '||' expr_while
 | expr_while '&&' expr_while*/
  | (op1)  ('<' | '<=' | '>' | '>=' | '==' | '!=') (op1 | IDENTIFIER | DIGIT |IDENTIFIER '['(expr_var)']'|get_property_json|literal_value)
 /* | expr_while ( '<' | '<=' | '>' | '>=' | '==' *//*| '='*//* | '!=' ) expr_while
 | expr_while ( '*' | '/' | '%') expr_while ('<' | '<=' | '>' | '>=' | '==' | '!=') expr_while
 | expr_while ( '+' | '-' ) expr_while  ('<' | '<=' | '>' | '>=' | '==' | '!=') expr_while*/


;

op1: // for while(x+3%2/x*k > 5+3*8/3%m){
 '(' (IDENTIFIER| DIGIT |literal_value|IDENTIFIER '['(expr_var)']'|get_property_json)   ( '+=' | '-=' |'*=' | '/=' | '%=' )  (op1) ')'
|(  (IDENTIFIER| DIGIT |literal_value|IDENTIFIER '['(expr_var)']'|get_property_json|incr_decr_without_comma)   ( '+' | '-' |'*' | '/' | '%' )      )* ( (IDENTIFIER| DIGIT|literal_value|IDENTIFIER '['(expr_var)']'|get_property_json|incr_decr_without_comma)  ( '+' | '-' |'*' | '/' | '%' ) (IDENTIFIER| DIGIT|literal_value|IDENTIFIER '['(expr_var)']'|get_property_json|incr_decr_without_comma)  )+
| (IDENTIFIER| DIGIT|literal_value |IDENTIFIER '['(expr_var)']'|get_property_json|incr_decr_without_comma)
| '(' op1')'
;

////while
while_stmt:
K_WHILE '(' (condition_header) ')'
(
'{'
      body
'}'
|
      body_without_bracketa
|';'
)
;

//////do_while
do_while_stmt:
K_DO (
      '{'
         body
       '}' K_WHILE '(' (condition_header)  ')'';'

 |

     body_without_bracketa
      K_WHILE '(' (condition_header)  ')'';'
      )
;

///cal_function
call_func:
(K_VAR IDENTIFIER '=')? fun_name '(' argument_list ')'
;

aggregation_func:
(K_VAR IDENTIFIER '=')? fun_name '(' aggr_argument_list ')'   //My Add 2020
;

heigher_order_function:
 K_FUNCTION '(' args_list_higher ')'

 body
;

args_list_higher:
((bool_ex|expr_var)(','(bool_ex|expr_var))*)?
;

argument_list:
(
   (
     expr_var)
      (','(expr_var)
    )* | '*'
   (','heigher_order_function)?
    /* (','heigher_order_function)?*/
)?
  /* |(heigher_order_function)?*/
;

aggr_argument_list:     //My Add 2020
(
   K_DISTINCT?(
     expr)
      (','(expr)
    )* | '*'
    /* (','heigher_order_function)?*/
)?
  /* |(heigher_order_function)?*/
;

bool_ex:
K_TRUE|K_FALSE|bool_ex('&'|'|') bool_ex
;






 /////////////////////////////////////////////// End of Added ////////////////////////////////////////////////

foreign_key_clause
 : K_REFERENCES ( database_name '.' )? foreign_table ( '(' fk_target_column_name ( ',' fk_target_column_name )* ')' )?
   ( ( K_ON ( K_DELETE | K_UPDATE ) ( K_SET K_NULL
                                    | K_SET K_DEFAULT
                                    | K_CASCADE
                                    | K_RESTRICT
                                    | K_NO K_ACTION )
     | K_MATCH name
     )
   )*
   ( K_NOT? K_DEFERRABLE ( K_INITIALLY K_DEFERRED | K_INITIALLY K_IMMEDIATE )? K_ENABLE? )?
 ;

fk_target_column_name
 : name
 ;

//raise_function
// : K_RAISE '(' ( K_IGNORE
//               | ( K_ROLLBACK | K_ABORT | K_FAIL ) ',' error_message )
//           ')'
// ;

indexed_column
 : column_name ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
 ;

table_constraint
 : ( K_CONSTRAINT name )?
   ( table_constraint_primary_key
   | table_constraint_key
   | table_constraint_unique
   | K_CHECK '(' expr ')'
   | table_constraint_foreign_key
   )
 ;

table_constraint_primary_key
 //: K_PRIMARY K_KEY '(' indexed_column ( ',' indexed_column )* ')' conflict_clause
 : K_PRIMARY K_KEY '(' indexed_column ( ',' indexed_column )* ')'
 ;

table_constraint_foreign_key
 : K_FOREIGN K_KEY '(' fk_origin_column_name ( ',' fk_origin_column_name )* ')' foreign_key_clause
 ;

table_constraint_unique
// : K_UNIQUE K_KEY? name? '(' indexed_column ( ',' indexed_column )* ')' conflict_clause
 : K_UNIQUE K_KEY? name? '(' indexed_column ( ',' indexed_column )* ')'
 ;

table_constraint_key
 //: K_KEY name? '(' indexed_column ( ',' indexed_column )* ')' conflict_clause
: K_KEY name? '(' indexed_column ( ',' indexed_column )* ')'
 ;

fk_origin_column_name
 : column_name
 ;

//with_clause
// : K_WITH K_RECURSIVE? cte_table_name K_AS '(' select_stmt ')' ( ',' cte_table_name K_AS '(' select_stmt ')' )*
// ;

qualified_table_name
 : ( database_name '.' )? table_name ( K_INDEXED K_BY index_name
                                     | K_NOT K_INDEXED )?
 ;

ordering_term
 : expr ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
 ;

pragma_value
 : signed_number
 | name
 | STRING_LITERAL
 ;

common_table_expression
 : table_name ( '(' column_name ( ',' column_name )* ')' )? K_AS '(' select_stmt ')'
 ;

result_column
 : '*'
 | table_name '.' '*'
 | expr ( K_AS? column_alias )?
 ;

table_or_subquery
 : ( database_name '.' )? table_name ( K_AS? table_alias )?
   ( K_INDEXED K_BY index_name
   | K_NOT K_INDEXED )?
 | '(' ( table_or_subquery ( ',' table_or_subquery )*
       | join_clause )
   ')' ( K_AS? table_alias )?
 | '(' factored_select_stmt ')' ( K_AS? table_alias )?    //My Add : select_stmt -> factored_select_stmt
 ;

join_clause
: table_or_subquery ( join_operator table_or_subquery join_constraint )*
// : table_or_subquery ( join_operator table_or_subquery join_constraint )*
 ;

join_operator
 : ','
// | K_NATURAL? ( K_LEFT K_OUTER? | K_INNER | K_CROSS )? K_JOIN
 |  ( K_LEFT K_OUTER? | K_INNER  )? K_JOIN
 ;

join_constraint
// : ( K_ON expr
//   | K_USING '(' column_name ( ',' column_name )* ')' )?
 : ( K_ON expr)?
 ;

select_core
 : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
   ( K_FROM ( table_or_subquery ( ',' table_or_subquery )* | join_clause ) )?
   ( K_WHERE expr )?
   ( K_GROUP K_BY expr ( ',' expr )* ( K_HAVING expr )? )?
 | K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
 ;

//compound_operator
// : K_UNION
// | K_UNION K_ALL
// | K_INTERSECT
// | K_EXCEPT
// ;
create_type_stmt
   : K_CREATE  K_TYPE ( K_IF K_NOT K_EXISTS )?
    create_type_name
   ( '(' column_def_type ( ',' column_def_type )* ')'
  // | K_AS select_stmt
   )
 ;


create_type_name
 : any_name
 ;

column_def_type
 : column_name (type_name)*
 ;

cte_table_name
 : table_name ( '(' column_name ( ',' column_name )* ')' )?
 ;

signed_number
 : ( ( '+' | '-' )? NUMERIC_LITERAL | '*' )
 ;

literal_value
 : NUMERIC_LITERAL
 | STRING_LITERAL
 | BLOB_LITERAL
 | K_NULL
 | K_CURRENT_TIME
 | K_CURRENT_DATE
 | K_CURRENT_TIMESTAMP
 ;

literal_value_json
 : NUMERIC_LITERAL
 |IDENTIFIER
 | STRING_LITERAL
 | BLOB_LITERAL
 | K_NULL
 | K_TRUE
 | K_FALSE
 |array_json
 ;
unary_operator
 : '-'
 | '+'
 | '~'
 | K_NOT
 ;

error_message
 : STRING_LITERAL
 ;

module_argument // TODO check what exactly is permitted here
 : expr
 | column_def
 ;

column_alias
 : IDENTIFIER
 | STRING_LITERAL
 ;


keyword
 : K_ABORT
 | K_ACTION
 | K_AGGREGATION
 | K_AGGREGATION_FUNC
 | K_ADD
 | K_AFTER
 | K_ALL
 | K_ALTER
 | K_ANALYZE
 | K_AND
 | K_AS
 | K_ASC
 | K_ATTACH
 | K_AUTOINCREMENT
 | K_BEFORE
 | K_BEGIN
 | K_BETWEEN
 | K_BY
 | K_CASCADE
 | K_CASE
 | K_CAST
 | K_CHECK
 | K_COLLATE
 | K_COLUMN
 | K_COMMIT
 | K_CONFLICT
 | K_CONSTRAINT
 | K_CREATE
 | K_CROSS
 | K_CURRENT_DATE
 | K_CURRENT_TIME
 | K_CURRENT_TIMESTAMP
 | K_DATABASE
 | K_DEFAULT
 | K_DEFERRABLE
 | K_DEFERRED
 | K_DELETE
 | K_DESC
 | K_DETACH
 | K_DISTINCT
 | K_DROP
 | K_EACH
 | K_ELSE
 | K_END
 | K_ENABLE
 | K_ESCAPE
 | K_EXCEPT
 | K_EXCLUSIVE
 | K_EXISTS
 | K_EXPLAIN
 | K_FAIL
 | K_FOR
 | K_FOREIGN
 | K_FROM
 | K_FULL
 | K_GLOB
 | K_GROUP
 | K_HAVING
 | K_IF
 | K_IGNORE
 | K_IMMEDIATE
 | K_IN
 | K_INDEX
 | K_INDEXED
 | K_INITIALLY
 | K_INNER
 | K_INSERT
 | K_INSTEAD
 | K_INTERSECT
 | K_INTO
 | K_IS
 | K_ISNULL
 | K_JOIN
 | K_KEY
 | K_LEFT
 | K_LIKE
 | K_LIMIT
 | K_MATCH
 | K_NATURAL
 | K_NO
 | K_NOT
 | K_NOTNULL
 | K_NULL
 | K_OF
 | K_OFFSET
 | K_ON
 | K_OR
 | K_ORDER
 | K_OUTER
 | K_PLAN
 | K_PRAGMA
 | K_PRIMARY
 | K_QUERY
 | K_RAISE
 | K_RECURSIVE
 | K_REFERENCES
 | K_REGEXP
 | K_REINDEX
 | K_RELEASE
 | K_RENAME
 | K_REPLACE
 | K_RESTRICT
 | K_RIGHT
 | K_ROLLBACK
 | K_ROW
 | K_SAVEPOINT
 | K_SELECT
 | K_SET
 | K_TABLE
 | K_TYPE
 | K_PATH
 | K_TEMP
 | K_TEMPORARY
 | K_THEN
 | K_TO
 | K_TRANSACTION
 | K_TRIGGER
 | K_UNION
 | K_UNIQUE
 | K_UPDATE
 | K_USING
 | K_VACUUM
 | K_VALUES
 | K_VIEW
 | K_VIRTUAL
 | K_WHEN
 | K_WHERE
 | K_WITH
 | K_WITHOUT
 | K_NEXTVAL
 | K_SWITCH ///Added
 | K_RETURN
 | K_VAR
 | K_PRINT
 | K_TRUE
 | K_FALSE
 | K_WHILE
 | K_DO
 | K_BREAK
 |K_FUNCTION
 ;

// TODO check all names below

//[a-zA-Z_0-9\t \-\[\]\=]+
unknown
 : .+
 ;

name
 : any_name
 ;

function_name
 : any_name
 ;

database_name
 : any_name
 ;

source_table_name
 : any_name
 ;

table_name
 : any_name
 ;

table_or_index_name
 : any_name
 ;

new_table_name
 : any_name
 ;

column_name
 : any_name
 ;

collation_name
 : any_name
 ;

foreign_table
 : any_name
 ;

index_name
 : any_name
 ;

trigger_name
 : any_name
 ;

view_name
 : any_name
 ;

module_name
 : any_name
 ;

pragma_name
 : any_name
 ;

savepoint_name
 : any_name
 ;

table_alias
 : any_name
 ;

transaction_name
 : any_name
 ;

any_name
 : IDENTIFIER
 //keyword
 | STRING_LITERAL
 | '(' any_name ')'
 ;
PLUS_PLUSl : '++';
MINUS_MINUSl : '--';
PLUS_PLUSr : '++';
MINUS_MINUSr : '--';
SCOL : ';';
DOT : '.';
OPEN_PAR : '(';
CLOSE_PAR : ')';
COMMA : ',';
ASSIGN : '=';
STAR : '*';
PLUS : '+';
MINUS : '-';
TILDE : '~';
PIPE2 : '||';
DIV : '/';
MOD : '%';
LT2 : '<<';
GT2 : '>>';
AMP : '&';
AMP2 : '&&';

PIPE : '|';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';
PLUS_EQ : '+=';
MINUS_EQ : '-=';
STAR_EQ : '*=';
DIV_EQ : '/=';
MOD_EQ : '%=';

// http://www.sqlite.org/lang_keywords.html
K_ABORT : A B O R T;
K_ACTION : A C T I O N;
K_ADD : A D D;
K_AGGREGATION_FUNC: A G G R E G A T I O N'_'F U N C T I O N;
K_AGGREGATION : A G G R E G A T I O N;
K_AFTER : A F T E R;
K_ALL : A L L;
K_ALTER : A L T E R;
K_ANALYZE : A N A L Y Z E;
K_AND : A N D;
K_AS : A S;
K_ASC : A S C;
K_ATTACH : A T T A C H;
K_AUTOINCREMENT : A U T O I N C R E M E N T;
K_BEFORE : B E F O R E;
K_BEGIN : B E G I N;
K_BETWEEN : B E T W E E N;
K_BY : B Y;
K_CASCADE : C A S C A D E;
K_CASE : C A S E;
K_CAST : C A S T;
K_CHECK : C H E C K;
K_COLLATE : C O L L A T E;
K_COLUMN : C O L U M N;
K_COMMIT : C O M M I T;
K_CONFLICT : C O N F L I C T;
K_CONSTRAINT : C O N S T R A I N T;
K_CREATE : C R E A T E;
K_CROSS : C R O S S;
K_CURRENT_DATE : C U R R E N T '_' D A T E;
K_CURRENT_TIME : C U R R E N T '_' T I M E;
K_CURRENT_TIMESTAMP : C U R R E N T '_' T I M E S T A M P;
K_DATABASE : D A T A B A S E;
K_DEFAULT : D E F A U L T;
K_DEFERRABLE : D E F E R R A B L E;
K_DEFERRED : D E F E R R E D;
K_DELETE : D E L E T E;
K_DESC : D E S C;
K_DETACH : D E T A C H;
K_DISTINCT : D I S T I N C T;
K_DROP : D R O P;
K_EACH : E A C H;
K_ELSE : E L S E;
K_END : E N D;
K_ENABLE : E N A B L E;
K_ESCAPE : E S C A P E;
K_EXCEPT : E X C E P T;
K_EXCLUSIVE : E X C L U S I V E;
K_EXISTS : E X I S T S;
K_EXPLAIN : E X P L A I N;
K_FAIL : F A I L;
K_FOR : F O R;
K_FOREIGN : F O R E I G N;
K_FROM : F R O M;
K_FULL : F U L L;
K_GLOB : G L O B;
K_GROUP : G R O U P;
K_HAVING : H A V I N G;
K_IF : I F;
K_IGNORE : I G N O R E;
K_IMMEDIATE : I M M E D I A T E;
K_IN : I N;
K_INDEX : I N D E X;
K_INDEXED : I N D E X E D;
K_INITIALLY : I N I T I A L L Y;
K_INNER : I N N E R;
K_INSERT : I N S E R T;
K_INSTEAD : I N S T E A D;
K_INTERSECT : I N T E R S E C T;
K_INTO : I N T O;
K_IS : I S;
K_ISNULL : I S N U L L;
K_JOIN : J O I N;
K_KEY : K E Y;
K_LEFT : L E F T;
K_LIKE : L I K E;
K_LIMIT : L I M I T;
K_MATCH : M A T C H;
K_NATURAL : N A T U R A L;
K_NEXTVAL : N E X T V A L;
K_NO : N O;
K_NOT : N O T;
K_NOTNULL : N O T N U L L;
K_NULL : N U L L;
K_OF : O F;
K_OFFSET : O F F S E T;
K_ON : O N;
K_ONLY : O N L Y;
K_OR : O R;
K_ORDER : O R D E R;
K_OUTER : O U T E R;
K_PLAN : P L A N;
K_PRAGMA : P R A G M A;
K_PRIMARY : P R I M A R Y;
K_QUERY : Q U E R Y;
K_RAISE : R A I S E;
K_RECURSIVE : R E C U R S I V E;
K_REFERENCES : R E F E R E N C E S;
K_REGEXP : R E G E X P;
K_REINDEX : R E I N D E X;
K_RELEASE : R E L E A S E;
K_RENAME : R E N A M E;
K_REPLACE : R E P L A C E;
K_RESTRICT : R E S T R I C T;
K_RIGHT : R I G H T;
K_ROLLBACK : R O L L B A C K;
K_ROW : R O W;
K_SAVEPOINT : S A V E P O I N T;
K_SELECT : S E L E C T;
K_SET : S E T;
K_TABLE : T A B L E;
K_TYPE  : T Y P E;
K_PATH  : P A T H;
K_TEMP : T E M P;
K_TEMPORARY : T E M P O R A R Y;
K_THEN : T H E N;
K_TO : T O;
K_TRANSACTION : T R A N S A C T I O N;
K_TRIGGER : T R I G G E R;
K_UNION : U N I O N;
K_UNIQUE : U N I Q U E;
K_UPDATE : U P D A T E;
K_USING : U S I N G;
K_VACUUM : V A C U U M;
K_VALUES : V A L U E S;
K_VIEW : V I E W;
K_VIRTUAL : V I R T U A L;
K_WHEN : W H E N;
K_WHERE : W H E R E;
K_WITH : W I T H;
K_WITHOUT : W I T H O U T;
K_SWITCH :S W I T C H ; /// Added
K_RETURN :R E T U R N ;
K_VAR :V A R ;
K_PRINT :P R I N T;
K_TRUE : T R U E;
K_FALSE: F A L S E;
K_WHILE: W H I L E;
K_DO : D O;
K_BREAK: B R E A K ;
K_FUNCTION: F U N C T I O N;
IDENTIFIER
 : '"' (~'"' | '""')* '"'
 | '`' (~'`' | '``')* '`'
 //| '[' ~']'* ']'
 | [a-zA-Z_] [a-zA-Z_0-9]* // TODO check: needs more chars in set
 ;
IDENTIFIER2
 : //'"' (~'"' | '""')* '"'
 //| '`' (~'`' | '``')* '`'
 //| '[' ~']'* ']'
 | [a-zA-Z_] [a-zA-Z_0-9]* // TODO check: needs more chars in set
 ;
NUMERIC_LITERAL
 : DIGIT+ ( '.' DIGIT* )? ( E [-+]? DIGIT+ )?
 | '.' DIGIT+ ( E [-+]? DIGIT+ )?
 ;


BIND_PARAMETER
 : '?' DIGIT*
 | [:@$] IDENTIFIER
 ;

STRING_LITERAL
 : '\'' ( ~'\'' | '\'\'' )* '\''
 ;

BLOB_LITERAL
 : X STRING_LITERAL
 ;

SINGLE_LINE_COMMENT
 : //'--' ~[\r\n]* -> channel(HIDDEN)
 ;

MULTILINE_COMMENT
 : '/*' .*? ( '*/' | EOF ) -> channel(HIDDEN)
 ;

SPACES
 : [ \u000B\t\r\n] -> channel(HIDDEN)
 ;

UNEXPECTED_CHAR
 : .
 ;

fragment DIGIT : [0-9];

fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
