package tree.Base;

//import com.sun.org.apache.xpath.internal.operations.Bool;
import generated.SQLBaseVisitor;
import generated.SQLParser;
import tree.AST.Parse;
import tree.AST.QueryStmt.*;
import tree.AST.javaStmt.*;
import tree.Main;
import tree.SymbolTable.Scope;
import tree.SymbolTable.Symbol;
import tree.SymbolTable.Type;
import tree.SymbolTable.declaredFunction;

import java.util.*;

public class BaseVisitor extends SQLBaseVisitor {

    String expr_type;
    String type;

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public Parse visitParse(SQLParser.ParseContext ctx) {
      System.out.println("visitParse");
        Parse p = new Parse();

        if (ctx.sql_stmt_list().size() != 0) {
            List<List<SQL_Statement>> sqlStmts = new ArrayList<>();
            for (int i = 0; i < ctx.sql_stmt_list().size(); i++) {

                sqlStmts.add(visitSql_stmt_list(ctx.sql_stmt_list(i)));
            }

            p.setSqlStmts(sqlStmts);
        }

        if (ctx.function().size() != 0) {
            List<FunctionDeclaration> functionDeclarations = new ArrayList<>();

            for (int i = 0; i < ctx.function().size(); i++) {
                functionDeclarations.add(visitFunction(ctx.function(i)));
            }

            p.setFunctions(functionDeclarations);
        }


        p.setLine(ctx.getStart().getLine()); //get line number
        p.setCol(ctx.getStart().getCharPositionInLine()); // get col number

        return p;
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public Object visitError(SQLParser.ErrorContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public List<SQL_Statement> visitSql_stmt_list(SQLParser.Sql_stmt_listContext ctx) {

        //System.out.println("visitSql_stmt_list");

        List<SQL_Statement> sqlStmt = new ArrayList<SQL_Statement>();

        for (int i = 0; i < ctx.sql_stmt().size(); i++) {
            sqlStmt.add(visitSql_stmt(ctx.sql_stmt(i)));
        }

        return sqlStmt;
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public SelectStmt visitSelect_stmt(SQLParser.Select_stmtContext ctx) {
       // System.out.println("visitSelect_stmt");

        //SelectStmt selectStmt=new SelectStmt();
        //System.out.println("visitSelect_stmt");

        return null;
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public SelectStmt visitFactored_select_stmt(SQLParser.Factored_select_stmtContext ctx) {
        //System.out.println("visitFactored_select_stmt");

        SelectStmt select = new SelectStmt();
        List<ResultColumn> col_name = new ArrayList<>();
        List<table_or_subquery> table_or_subqueries = new ArrayList<>();
        Expr ex = new Expr();

        int h = 0;
        if (ctx.select_core().result_column() != null) {
            for (int i = 0; i < ctx.select_core().result_column().size(); i++) {
                col_name.add(visitResult_column(ctx.select_core().result_column(i)));
            }

            select.setColumnItem(col_name);
        }


        if (ctx.select_core().table_or_subquery() != null) {
            for (int i = 0; i < ctx.select_core().table_or_subquery().size(); i++) {
                table_or_subqueries.add(visitTable_or_subquery(ctx.select_core().table_or_subquery(i)));
            }
            select.setTable_or_subqueries(table_or_subqueries);

        }

        if (ctx.select_core().join_clause() != null) {
           // System.out.println("VisitJOINStmt");
            select.setJoinclaus(visitJoin_clause(ctx.select_core().join_clause()));
        }

//        select.setFromItem( visitTable_or_subquery(ctx.select_core().table_or_subquery(0)));
//        select.setColumnItem(visitResult_column(ctx.select_core().result_column());


        if (ctx.select_core().K_WHERE() != null) {

            ex = visitExpr(ctx.select_core().expr(0));
            select.setWhereItem(ex);
        }

        if (ctx.ordering_term() != null) {
            List<Order_Tearm> order_tearms = new ArrayList<>();
            for (int i = 0; i < ctx.ordering_term().size(); i++) {
                order_tearms.add(visitOrdering_term(ctx.ordering_term(i)));
            }
            select.setOrder_tearms(order_tearms);
        }
        if (ctx.K_LIMIT() != null) {
            //select.setL
            select.setLimits(visitExpr(ctx.expr(0)));
        }

        if ((ctx.K_OFFSET() != null)) {
            //select.setLimits(visitExpr(ctx.expr(1)));
            select.setOffset(visitExpr(ctx.expr(1)));
        }
        if (ctx.select_core().K_GROUP() != null && ctx.select_core().K_BY() != null) {

            List<Expr> exprs = new ArrayList<>();
           // System.out.println("000000000000000000000000000");
//            if(ctx.select_core().expr(1)!=null)
//            {
//                exprs.add(visitExpr(ctx.select_core().expr(1)));
//            }

            //exprs.add(visitExpr(ctx.select_core().expr(2)));

            for (int i = 1; i < ctx.select_core().expr().size(); i++) {
                exprs.add(visitExpr(ctx.select_core().expr(i)));
                h = i;
            }

            select.setGroupby(exprs);
        }
//        if(ctx.select_core().K_HAVING()!=null)
//        {
//            select.setHaving_expr(visitExpr(ctx.select_core().expr(h)));
//        }
        if (ctx.select_core().K_VALUES() != null) {
            List<Expr> values = new ArrayList<>();
            for (int i = 0; i < ctx.select_core().expr().size(); i++) {
                values.add(visitExpr(ctx.select_core().expr(i)));
            }
            select.setValues_exprs(values);
        }

        // if(ctx.select_core().table_or_subquery()!=null)

        select.setName("Select");


        return select;
//        return visitChildren(ctx);

    }


    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public Object visitCommon_table_expression(SQLParser.Common_table_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public Object visitSelect_or_values(SQLParser.Select_or_valuesContext ctx) {
        return visitChildren(ctx);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public Order_Tearm visitOrdering_term(SQLParser.Ordering_termContext ctx) {
       // System.out.println("visit order_term");
        Order_Tearm order_tearm = new Order_Tearm();
        if (ctx.expr() != null) {
            order_tearm.setTearm(visitExpr(ctx.expr()));
        }
        if (ctx.collation_name() != null) {
            order_tearm.setCollation_name(ctx.collation_name().getText());
        }
        return order_tearm;
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */


    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public ResultColumn visitResult_column(SQLParser.Result_columnContext ctx) {
       // System.out.println("visitResultcolumn");

        ResultColumn resultColumn = new ResultColumn();

        if (ctx.table_name() != null) {
            resultColumn.setTb_name(visitTable_name(ctx.table_name()));
        }

        if (ctx.expr() != null) {
            resultColumn.setExpr(visitExpr(ctx.expr()));
            if(ctx.expr().column_name()!=null)
            {
                Main.list_joins.add(ctx.expr().column_name().any_name().IDENTIFIER().getText());
            }
        }

        if (ctx.K_AS() != null && ctx.column_alias() != null) {
            resultColumn.setAlias_name(ctx.column_alias().getText());
        }

        if(ctx.STAR()!=null)
        {
            resultColumn.setStar(ctx.STAR().getText());
        }

        return resultColumn;

    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public table_or_subquery visitTable_or_subquery(SQLParser.Table_or_subqueryContext ctx) {

       // System.out.println("Visit Table_or_Subquery");
        table_or_subquery table_or_subquery = new table_or_subquery();
        List<tree.AST.QueryStmt.table_or_subquery> table_or_subqueries = new ArrayList<>();
        if (ctx.database_name() != null) {
            table_or_subquery.setDb_name(visitDatabase_name(ctx.database_name()));
        }
        if (ctx.table_name() != null) {
            if(Main.table_name.contains(ctx.table_name().any_name().IDENTIFIER().getText())) {
                table_or_subquery.setTb_name(visitTable_name(ctx.table_name()));
            }
            else {
                System.err.println("undefine table");
                System.exit(1);
            }
        }
        if (ctx.index_name() != null) {
            table_or_subquery.setIndex_name(ctx.index_name().getText());
        }
        if (ctx.K_AS() != null) {
            if (ctx.table_alias() != null) {
                table_or_subquery.setTable_alias_1(ctx.table_alias().getText());
            }
        }
        if (ctx.table_or_subquery() != null) {
            for (int i = 0; i < ctx.table_or_subquery().size(); i++) {
                table_or_subqueries.add(visitTable_or_subquery(ctx.table_or_subquery(i)));
            }
            table_or_subquery.setTable_or_subqueries(table_or_subqueries);

        }

        if (ctx.join_clause() != null) {
           // System.out.println("VisitJOINStmt");
            table_or_subquery.setJoinclaus(visitJoin_clause(ctx.join_clause()));
        }

        if (ctx.factored_select_stmt() != null) {
            //table_or_subquery.setSelectStmt(visitFactored_select_stmt(ctx.));
            //System.out.println("VisitSelectStmt");
            table_or_subquery.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
        }


        return table_or_subquery;
    }

    @Override
    public joinclaus visitJoin_clause(SQLParser.Join_clauseContext ctx) {
        //System.out.println("Visitjoinclause");
        joinclaus joinclaus = new joinclaus();
        List<table_or_subquery> table_or_subqueries = new ArrayList<>();
//        join_clause
//        : table_or_subquery ( join_operator table_or_subquery join_constraint )*
        if (ctx.table_or_subquery(0) != null) {
            table_or_subqueries.add(visitTable_or_subquery(ctx.table_or_subquery(0)));
            joinclaus.setTable_or_subqueries(table_or_subqueries);


        }
        if (ctx.table_or_subquery(1) != null) {
            for (int i = 1; i < ctx.table_or_subquery().size(); i++) {
                table_or_subqueries.add(visitTable_or_subquery(ctx.table_or_subquery(i)));

            }

            joinclaus.setTable_or_subqueries(table_or_subqueries);

        }
        if (ctx.join_constraint().size() > 0) {
            if (ctx.join_constraint(0).expr() != null) {
               // System.out.println("join constrain");
                List<Expr> exprs = new ArrayList<>();
                for (int i = 0; i < ctx.join_constraint().size(); i++) {
                    exprs.add(visitExpr(ctx.join_constraint(i).expr()));
                }

                joinclaus.setJoin_constrain(exprs);
            }
        }
        return joinclaus;
    }

    public void check_where_on (SQLParser.Assigne_selectContext ctx,AssigneSelect assigneSelect){

        List<String> ch_join = new ArrayList<>();

        //////K_WHERE

        for(int b=0 ; b<Main.table_list.size();b++) {
            for (int a = 0; a < Main.table_list.get(b).getColumn_defs().size(); a++) {
                for (int c = 0; c < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); c++) {
                    if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(c).table_name().any_name().IDENTIFIER().getText()
                            .equals(Main.table_list.get(b).getTb_name().getAny_name().getName())) {
                        ch_join.add(Main.table_list.get(b).getColumn_defs().get(a).getCol_name().getAny_name().getName());
                        //System.out.println("nnnnnnnnn : " + Main.table_list.get(b).getColumn_defs().get(a).getCol_name().getAny_name().getName());
                    }
                }
            }
        }

        //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
        //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
        if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
        {
            if(ctx.factored_select_stmt().select_core().expr().size()>0)
            {
                for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                    if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                        if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                        {
                            for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                            {
                                if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                {
                                    for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                    {
                                        try{
                                            if (!(ch_join.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                System.exit(1);
                                            }
                                            if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().IDENTIFIER().getText()))){
                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().getStart().getLine());
                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().getStart().getCharPositionInLine());
                                                System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                //System.exit(1);
                                            }
                                        }
                                        catch (NullPointerException e){}
                                    }
                                }
                            }
                        }

                    } else {
                        if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                            for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                try {
                                    //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                    if (!(ch_join.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                        System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                        System.exit(1);
                                    }
                                    if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().IDENTIFIER().getText()))){
                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().getStart().getLine());
                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().getStart().getCharPositionInLine());
                                        System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                        //System.exit(1);
                                    }
                                } catch (NullPointerException e) {
                                }

                            }
                        }
                    }
                }
            }
        }
        //////K_WHERE

        /////K_ON//////

        if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().size()>0)
        {
            for(int v =0; v<ctx.factored_select_stmt().select_core().join_clause().join_constraint().size();v++){

                if(ctx.factored_select_stmt().select_core().join_clause().join_constraint(v).K_ON()!=null)
                {
                    if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr()!=null)
                    {
                        if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr().expr().size()>0)
                        {
                            for(int a = 0 ;a<ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr().expr().size();a++){

                                try {
                                    if (!(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr().expr().get(a).table_name().any_name().IDENTIFIER().getText()))) {
                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr().expr().get(a).table_name().any_name().getStart().getLine());
                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr().expr().get(a).table_name().any_name().getStart().getCharPositionInLine());
                                        System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                        //System.exit(1);
                                    }


                                if (!(ch_join.contains(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr().expr().get(a).column_name().any_name().IDENTIFIER().getText()))) {
                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr().expr().get(a).column_name().any_name().getStart().getLine());
                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(v).expr().expr().get(a).column_name().any_name().getStart().getCharPositionInLine());
                                    System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                    System.exit(1);
                                }
                                }
                                catch (NullPointerException e){}

                            }
                        }
//
                    }
                }
            }
        }


        ////K_ON///////


    }

    public void flat_star_column(SQLParser.Assigne_selectContext ctx,AssigneSelect assigneSelect,Map<String,Type> columns,int j,int k,int l) {
        //for (int j = 0; j < Main.table_list.size(); j++) {
        // for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {
        //for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {
        Type type1 = new Type();

//        try {
//            if (Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
//
//                for (int m = 0; m < Main.type_list.size(); m++) {
//
//                    if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
//                        System.out.println("flaaaaaaaaaaaaaaaaaaaaaaaaaaaat_tyyyyyyyyyyyyype000000000");
//                        for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
//                            Type type2 = new Type();
//                            for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {
//                                // System.out.println("flaaaaaaaaaaaaaaaaaaaaaaaaaaaat_tyyyyyyyyyyyyype000000000");
//                                type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
//                                columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
//                                System.out.println(" cols : "+ Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName());
//
//                                //System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//                            }
//
//
//                            if (Main.type_name_list.contains(columns.get(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName()).getName())) {
//                                System.out.println("flaaaaaaaaaaaaaaaaaaaaaaaaaaaat_tyyyyyyyyyyyyype000000000");
//                                flat_star_column(ctx, assigneSelect, columns, m, n, l);
//                            }
//                        }
//
//                    }
//                }
//            }
//
//        }
//        catch (IndexOutOfBoundsException e){}

        if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {

            for (int m = 0; m < Main.table_list.size(); m++) {

                if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                    for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                        Type type2 = new Type();
                        for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                            type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                            columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                            //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                            System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                        }


                        if (Main.table_name.contains(columns.get(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName()).getName())) {
                            System.out.println("flaaaaaaaaaaaaaaaaaaaaaaaaaaaat");
                            flat_star_column(ctx, assigneSelect, columns, m, n, l);
                        }
                        if (Main.type_name_list.contains(columns.get(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName()).getName())) {
                            System.out.println("flaaaaaaaaaaaaaaaaaaaaaaaaaaaattype");
                            flat_type_star_column(ctx, assigneSelect, columns, m, n, l);
                        }

                    }

                }
            }
        } else {
//                                                        //normal column
            //  System.out.println("pppppppppppppppppppppppppppppppp");
            type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());
            columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+"_"+Main.table_list.get(j).getTb_name().getAny_name().getName(), type1);
            // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
            //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

        }
    }
    public void flat_type_star_column(SQLParser.Assigne_selectContext ctx,AssigneSelect assigneSelect,Map<String,Type> columns,int j,int k,int l) {
        if (Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
            System.out.println("tyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyp create");
            for (int m = 0; m < Main.type_list.size(); m++) {
                if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                    for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                        Type type2 = new Type();
                        for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                            type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                            columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                            //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                            // System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());

                            if(Main.type_name_list.contains(type2.getName()))
                            {
                                flat_type_star_column(ctx,assigneSelect,columns,m,n,b);
                            }
                        }



                    }
                }



            }

        }

        }


    public void flat_result_column(SQLParser.Assigne_selectContext ctx,AssigneSelect assigneSelect,Map<String,Type> columns,List<String> column_add,List<String> check_type,int i,int u,int v) {

        //System.out.println("cols : " + ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText());
        for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
            System.out.println("in fffffffff");
            Type type1 = new Type();

            if(Main.type_name_list.contains(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
            {
                System.out.println("ccccckkcbcncncyu888888");
                check_type.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
            }

            type1.setName(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
//                                                                 //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);
//
            columns.put(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName(), type1);
            column_add.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());

        }

            if (Main.table_name.contains(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName())) {

                System.out.println("8888888888888888888888888 " + columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName());
                for (int m = 0; m < Main.table_list.size(); m++) {

                    if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName())) {

                        // System.out.println("888888888888888888888888877777");
                        for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                            Type type2 = new Type();
                            for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

//                            type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//                            columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
//                            column_add.add(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//
//                                                                                      //s added
//                            System.out.print(" column_name : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");
//
//                            System.out.println(" var_column_type : "+Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                flat_result_column(ctx, assigneSelect, columns, column_add,check_type,i, m, n);

                            }


                        }


                    }


                }


            }
        for(int y=0;y<check_type.size();y++) {
            if (Main.type_name_list.contains(check_type.get(y)))
            {
                System.out.println("vvvvvvvvvvvvvv " + check_type.get(y));

                for (int m = 0; m < Main.type_list.size(); m++) {
                    if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type.get(y))) {

                        // System.out.println("888888888888888888888888877777");
                        for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                            Type type2 = new Type();
                            for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                check_type.add(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());

                            }

                            if(Main.type_list.contains(columns.get(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName()).getName())){
                                flat_result_column(ctx,assigneSelect,columns,column_add,check_type,i,m,n);

                            }

                        }


                    }

                }
            }
        }

    }

    public void flat_type_result_column(SQLParser.Assigne_selectContext ctx,AssigneSelect assigneSelect,Map<String,Type> columns,List<String> check_type,int i,int u,int v) {

        //System.out.println("cols : " + ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText());
        for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
            System.out.println("in fffffffff");
            Type type1 = new Type();
            if(Main.type_name_list.contains(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
            {
                System.out.println("ccccckkcbcncncyu888888");
                check_type.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
            }

        }

        for(int y=0;y<check_type.size();y++) {
            if (Main.type_name_list.contains(check_type.get(y)))
            {
                System.out.println("vvvvvvvvvvvvvv " + check_type.get(y));

                for (int m = 0; m < Main.type_list.size(); m++) {
                    if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type.get(y))) {

                        // System.out.println("888888888888888888888888877777");
                        for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                            Type type2 = new Type();
                            for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                check_type.add(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());

                            }

                            if(Main.type_list.contains(columns.get(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName()).getName())){
                                flat_type_result_column(ctx,assigneSelect,columns,check_type,i,m,n);

                            }

                        }


                    }

                }
            }
        }

    }



    @Override
    public AssigneSelect visitAssigne_select(SQLParser.Assigne_selectContext ctx)  {

        System.out.println("visitAssigneSelect");
        AssigneSelect assigneSelect = new AssigneSelect();

        assigneSelect.setLine(ctx.getStart().getLine());
        assigneSelect.setCol(ctx.getStart().getCharPositionInLine());
        String type_name = " ";
        boolean test=false;
        boolean table =false;
        Map<String, Type> columns = new HashMap<String, Type>();
        List<String> list=new ArrayList<>();
        List<String> list_joins=new ArrayList<>();
        List<String> agg_list = new ArrayList<>();
        Scope currentScope =   Main.parent_stack.peek();
        Type type = new Type();
        String var_name=ctx.IDENTIFIER().getText();
        assigneSelect.setVar(ctx.IDENTIFIER().getSymbol().getText());
        if(ctx.factored_select_stmt().select_core()!=null)
        {
            assigneSelect.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
        }
        //type.setName(this.expr_type);
        /************/
        boolean check_join=false;
        String join = " ";
        Map<String, Type> columns_flat = new HashMap<String, Type>();
        List<String> list_dublicate=new ArrayList<>();
        List<String> column_add = new ArrayList<>();
        List<String> check_type = new ArrayList<>();
        List<String> check_type_join = new ArrayList<>();
        HashSet unique = new HashSet();




        // s added
        Map<String, Type> columns2 = new HashMap<String, Type>();
        List<String> all_cols_name=new ArrayList<>();
        List<String> all_cols_name_in_subquery_table=new ArrayList<>();
        List<String> all_table_in_subquery = new ArrayList<>();
      ///  Map<String, Type> current_columns = new HashMap<String, Type>();


        /*************/
        if(ctx.K_VAR()!= null){
            if(currentScope.getSymbolMap().get(var_name)!=null)
            {
                System.err.println("Error in multiple declaration : a variable "+var_name+" should be declared in the same scope at most once "+"the colume is : "+ assigneSelect.getCol()+"   the line is :  " +assigneSelect.getLine());
            }
            else{
                Symbol varSymbol = new Symbol();
                String varName = ctx.IDENTIFIER().getText();
                varSymbol.setName(varName);
                varSymbol.setIsParam(false);
                assigneSelect.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));

                //for join clause
                if (ctx.factored_select_stmt().select_core().join_clause() != null) {
                    //for table_or_subquery
                    for (int i = 0; i < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); i++) {

                        if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).K_AS() != null) {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).table_alias().any_name().getText();
                        } else {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).table_name().any_name().getText();
                        }
                    }

                    if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null) {
                        System.out.println("Staaaaaar");
                    }

                    type.setName(type_name);
                    type.setColumns(columns);
                    varSymbol.setType(type);
                    varSymbol.setScope(currentScope);
                    currentScope.addSymbol(varName, varSymbol);
                    System.out.println("current Scope { "+currentScope.getId() + " }");
                    System.out.println("variable type : " + varSymbol.getType().getName());
                    System.out.println("var scope : " + varSymbol.getScope().getId());
                }

                //for table_or_subquery
                if (ctx.factored_select_stmt().select_core().table_or_subquery().size() > 0) {

                    System.err.println("first table_or_subquery ");
                    String tb_name_out_subquery = " ";

                    //to get all table name in out select
               /*     for(int m =0;m<ctx.factored_select_stmt().select_core().table_or_subquery().size();m++){

                        // if aliases in out select
                        if (ctx.factored_select_stmt().select_core().table_or_subquery(m).K_AS() != null) {
                            type_name += "_" + ctx.factored_select_stmt().select_core().table_or_subquery(m).table_alias().any_name().getText();
                        }else{
                            type_name += "_" + ctx.factored_select_stmt().select_core().table_or_subquery(m).table_name().any_name().getText();
                        }
                    }


                    System.err.println("sooooooooooooooooooooooooooooooooooooooooooo => "+type_name);
*/

                    //= ctx.factored_select_stmt().select_core().table_or_subquery(0).table_name().any_name().getText();
                    //subquery
                    if(ctx.factored_select_stmt().select_core().K_WHERE() != null ){



                        for(int i =0;i<ctx.factored_select_stmt().select_core().expr().size();i++){


                            for(int j =0;j<ctx.factored_select_stmt().select_core().expr(i).expr().size();j++) {

                                try {
                                    if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt() != null) {


                                        //check if column in subquery is one
                                        if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().size() == 1) {


                                            if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size() > 0) {


                                                for (int e = 0; e < ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size(); e++) {
                                                    //System.out.println("table name : " + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery(e).table_name().any_name().getText());
                                                    all_table_in_subquery.add(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery(e).table_name().any_name().getText());
                                                }

                                                System.out.println("all_table_in_subquery  => " + all_table_in_subquery);
                                                System.out.println("---------------------------------- ");

                                            }


                                            ////////////here
                                            for (int g = 0; g < Main.table_list.size(); g++) {

                                                if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size() > 0) {


                                                    //to get table with cols with type of each col
                                                    for (int n = 0; n < all_table_in_subquery.size(); n++) {
                                                        if (Main.table_list.get(g).getTb_name().getAny_name().getName().equals(all_table_in_subquery.get(n))) {

                                                            System.out.println("table name :" + Main.table_list.get(g).getTb_name().getAny_name().getName());
////77777777777777777777
                                                            for (int cc = 0; cc < Main.table_list.get(g).getColumn_defs().size(); cc++) {


                                                                System.out.println("Column => " + Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName());
                                                                all_cols_name_in_subquery_table.add(Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName());


                                                                //to get type the column
                                                                Type type2 = new Type();
                                                                for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(cc).getType_names().size(); l++) {
                                                                    type2.setName(Main.table_list.get(g).getColumn_defs().get(cc).getType_names().get(l).getNames().getName().getName());

                                                                    //to store name and type of col
                                                                    columns.put(Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName(), type2);
                                                                    System.out.println("Type : " + type2.getName());
                                                                    //  System.out.println("coooooooooooooooooooool : " +  columns);
                                                                }

                                                            }
                                                        }


                                                    }


                                                }


                                            }

                                            //System.err.println("all_cols_name in sub : " +all_cols_name_in_subquery_table);
                                            //System.err.println("this col => " + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());


                                            //check if col in subquery is found in tables

                                            //check of column in subquery is ( * )
                                            if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).STAR() != null) {
                                                System.err.println("columns in subquery can't be start ( * )");
                                            } else if (all_cols_name_in_subquery_table.contains(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText())) {

                                                for (int a = 0; a < columns.size(); a++) {


                                                    //  System.err.println("type column IDENTEFIR in subquery : " + columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName());
                                                    //  System.err.println("yyyyyyyyyyyyyyyyyyyyyyyyyyiiiiiiiii : " + ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText());


                                                    for (int g = 0; g < Main.table_list.size(); g++) {
                                                        for (int k = 0; k < Main.table_list.get(g).getColumn_defs().size(); k++) {


                                                            //to get type the column
                                                   /*     Type type2= new Type();
                                                        for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(k).getType_names().size(); l++) {
                                                            type2.setName(Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                            //to store name and type of col
                                                            columns.put( Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type2);
                                                            //  System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyiiiiiiiii type : " +  type2.getName());
                                                        }*/

                                                        }

                                                    }


                                                    for (int g = 0; g < Main.table_list.size(); g++) {
                                                        for (int k = 0; k < Main.table_list.get(g).getColumn_defs().size(); k++) {


                                                            //to get type the column
                                                            Type type2 = new Type();
                                                            for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(k).getType_names().size(); l++) {
                                                                type2.setName(Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());
                                                                //System.err.println("tttttttttttttttttttttttt :=> "+ Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " => " + type2.getName());
                                                                //to store name and type of col
                                                                columns2.put(Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type2);
                                                                // System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb : " + Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                                                //  System.err.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy :=> "+ columns);


                                                                if (Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())) {

                                                                    // System.err.println("howwwwwwwwwwwwwwwwwwwwwwwwww ????");
                                                                    //   System.err.println("columnsssssssssss //////"+columns.get("id").getName());
                                                                    //    System.err.println("type of col : "+ Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                    //  System.out.println("type column IDENTEFIR in subquery kookokokokokoooooooooooooooooooooooo: " + columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName());


                                                                    //System.err.print("id :  "+ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());
                                                                    if (Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName().equals(columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName())) {

                                                                        // System.err.println("alllllllllllllllllllllllllllllllllllll is trueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
                                                                    } else {
                                                                        System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText() + " and " + Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " must be the same type ... ");

                                                                    }


                                                                }


                                                            }
                                                        }
                                                    }


                                                }

                                            } else {
                                                System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText() + " not found in tables subquery .... ");
                                            }

                                            /////end here



                                  /*
                                   all_cols_name.add(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                   if (Main.column_name.contains(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText())) {
                                        System.err.println("columns in subquery found in table subquery" + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText());
                                    }else{
                                        System.err.println("columns in subquery noooooooooooooooooot found in table subquery" + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText());
                                    }*/


                                        } else {
                                            System.err.println("columns in subquery must be one ...");
                                        }


                                    }
                                }
                                catch (NullPointerException e){}

//                                else{
//                                    System.err.println("subquery must be select");
//                                }

                               // break;

                            }

                          //  System.err.println("lll" + ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText() );

                            // System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).factored_select_stmt().select_core());




                            //Where In (expr IN SelectExpr )
                            if(ctx.factored_select_stmt().select_core().expr(i).K_IN() != null) {


                                /////////addesd********************
                                if (ctx.factored_select_stmt().select_core().expr(i).factored_select_stmt() != null) {


                                    if (ctx.factored_select_stmt().select_core().expr(i).expr(i) != null) { //check IDENTIFIER is write


                                                                        /* System.err.println("subquery");
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().IDENTIFIER());
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());
                                    */





                                                                           /* for(int c=0;c<ctx.factored_select_stmt().select_core().table_or_subquery().size();c++){
                                                                                System.err.println("koko:"+ctx.factored_select_stmt().select_core().table_or_subquery(c).table_name().any_name().getText());

                                                                            }*/


                                        ///check IDENTIFIER is found in cols
                                        for (int j = 0; j < Main.table_list.size(); j++) {

                                            //to get all cols in all table (select * from test1,test2)
                                            for (int p = 0; p < ctx.factored_select_stmt().select_core().table_or_subquery().size(); p++) {

                                                //check if table (in out select) is found in table_list
                                                if (ctx.factored_select_stmt().select_core().table_or_subquery(p).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {

                                                    //table flat


                                                    for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                                        // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                                        for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {

                                                            Type type1 = new Type();


                                                            //flat (table)
                                                            if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                System.err.println("================  //flat (table) ======================== ");

                                                                type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                                columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                                System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                                //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                for (int m = 0; m < Main.table_list.size(); m++) {
                                                                    if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {

                                                                        for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                            Type type2 = new Type();
                                                                            for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName(), type2);
                                                                                //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                            }

                                                                        }

                                                                    }
                                                                }

                                                                //
                                                            }   //end flat


                                                            //flat (type)
                                                            else if (Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                System.err.println("================  //flat (type) ======================== ");

                                                                type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                                columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                                System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                                //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                for (int m = 0; m < Main.type_name_list.size(); m++) {
                                                                    if (Main.type_name_list.get(m).equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {

                                                                        System.err.println("================ kk ======================== ");

                                                                        for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                            Type type2 = new Type();


                                                                            for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                System.err.println("================ hhhhhhhhhhhhhhhhhhh ======================== " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().size());

                                                                                //    if((Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName().equals())

                                                                                type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName(), type2);
                                                                                //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                System.out.println(" var_column_type : " + Main.table_list.get(m).getTb_name().getAny_name().getName());
                                                                            }

                                                                        }
                                                                    }
                                                                }

                                                            }//end flat type


                                                            else {    //normal column
                                                                //   System.err.println("col=>");

                                                                type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                // System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
                                                                // System.out.println("var_column_type : " + Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                                                all_cols_name.add(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());




                                                                                               /*     for(int y=0;y<columns.size();y++){
                                                                                                        if(columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).equals(ctx
                                                                                                        .factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())){
                                                                                                                    System.err.println("gogogogogogoooooooooooooooggggggggggg"+columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()));
                                                                                                        }else{

                                                                                                            System.err.println("noooooooooooooooooooooooooooo"+columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()));
                                                                                                        }



                                                                                                    }*/


                                                            }    //end of normal column


                                                        }


                                                        //}
                                                    }


                                                }


                                            }


                                        }


                                        ////////*********************************************

                                                                           /* System.err.println("all_cols_name in outer select : " +all_cols_name);
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText());
                                    */

                                        if (all_cols_name.contains(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())) {
                                            //  System.err.println(" column  found " );

                                        } else {
                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText() + " column not found ");
                                        }


                                    }


                                    //  System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());


                                }
                            }///g
                            else{// where stmt without IN
                                //  System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());
                                System.err.println("check where stmt");
                            }


                        }






                    }
                    else{
                        if (ctx.factored_select_stmt().select_core().table_or_subquery(0).K_AS() != null) {
                            type_name = ctx.factored_select_stmt().select_core().table_or_subquery(0).table_alias().any_name().getText();

                        } else {
                            type_name = ctx.factored_select_stmt().select_core().table_or_subquery(0).table_name().any_name().getText();
                        }
                    }
                    //}
                /* else {
                    for (int i = 0; i < ctx.factored_select_stmt().select_core().table_or_subquery().size(); i++) {

                        if (ctx.factored_select_stmt().select_core().table_or_subquery(i).K_AS() != null) {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().table_or_subquery(i).table_alias().any_name().getText();
                        } else {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().table_or_subquery(i).table_name().any_name().getText();
                        }
                    }
                }*/
                    type.setName(type_name);
                    type.setColumns(columns);
                    varSymbol.setType(type);
                    varSymbol.setScope(currentScope);
                    currentScope.addSymbol(varName, varSymbol);
                    System.out.println("current Scope { "+currentScope.getId() + " }");
                    System.out.println("variable type : " + varSymbol.getType().getName());
                    System.out.println("var scope : " + varSymbol.getScope().getId());
                }
                //for result_column
                if (ctx.factored_select_stmt().select_core().result_column().size() > 0) {

                    System.out.println("++++++++++++++++++++++++++++0");
                    // if (ctx.factored_select_stmt().select_core().result_column(0).!=null){}   agg fumc
                    if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null&&ctx.factored_select_stmt().select_core().join_clause()==null) {
                        System.out.println("++++++++++++++++++++++++++++0");


                      List<String> checks = new ArrayList<>();

                        for (int i = 0; i < ctx.factored_select_stmt().select_core().result_column().size(); i++) {

                            for (int j = 0; j < Main.table_list.size(); j++) {


                                //to get all cols in all table (select * from test1,test2)
                                for (int p = 0; p < ctx.factored_select_stmt().select_core().table_or_subquery().size(); p++) {

                                    //check if table (in out select) is found in table_list


                                        if (ctx.factored_select_stmt().select_core().table_or_subquery(p).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {
                                            System.out.println("-----------------------------");
                                            for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                               //////K_WHERE
                                                for (int r = 0; r < Main.table_list.get(j).getColumn_defs().size(); r++) {
                                                    checks.add( Main.table_list.get(j).getColumn_defs().get(r).getCol_name().getAny_name().getName());
                                                    //System.out.println("nmnmnmnmnmnm "+Main.table_list.get(j).getColumn_defs().get(r).getCol_name().getAny_name().getName());
                                                }
                                                //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                                //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                                if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                                {
                                                    if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                    {
                                                        for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                            if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                                if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                                {
                                                                    for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                    {
                                                                        if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                        {
                                                                            for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                            {
                                                                                try{
                                                                                    if (!(checks.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                        System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                      //  System.exit(1);
                                                                                    }
                                                                                }
                                                                                catch (NullPointerException e){}
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            } else {
                                                                if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                    for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                        try {
                                                                            //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                            if (!(checks.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                                System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                            //    System.exit(1);
                                                                            }
                                                                        } catch (NullPointerException e) {
                                                                        }

                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                //////K_WHERE

                                                //List<String> ch = new ArrayList<>();
                                                // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                                for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {




                                                    Type type1 = new Type();

                                                    //****** type flat  ******
                                                //    System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkl " + Main.type_name_list.get(0));
                                                    flat_type_star_column(ctx,assigneSelect,columns,j,k,l);

                                                    //flat_type_result_column(ctx,assigneSelect,columns,check_type,k,j,l);
//                                                    if (Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
//                                                        System.out.println("tyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyp create");
//                                                        for (int m = 0; m < Main.type_list.size(); m++) {
//                                                            if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
//                                                                for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
//                                                                    Type type2 = new Type();
//                                                                    for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {
//
//                                                                        type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
//                                                                        columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
//                                                                        //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");
//
//                                                                        // System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//                                                                    }
//
//                                                                }
//                                                            }
//                                                        }
//
//                                                    }

                                                    //****** end of type flat *****

                                                    //table flat
                                                    flat_star_column(ctx,assigneSelect,columns,j,k,l);
//                                                    else if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
//                                                        type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
//                                                        columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
//                                                        //System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
//                                                        //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
//
//                                                        //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());
//
//                                                        for (int m = 0; m < Main.table_list.size(); m++) {
//                                                            if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
//                                                                for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
//                                                                    Type type2 = new Type();
//                                                                    for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {
//
//                                                                        type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//                                                                        columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
//                                                                        //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");
//
//                                                                        System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//                                                                    }
//
//                                                                }
//                                                            }
//                                                        }
//
////
//                                                    }   //end table flat
//                                                    else {
//                                                        //normal column
//                                                      //  System.out.println("pppppppppppppppppppppppppppppppp");
//                                                        type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());
//
//                                                        columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
//                                                        // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
//                                                        System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
//                                                    }    //end of normal column


                                                }
                                                //}
                                            }
                                        }


                                }

                            }

                        }


                    }
                    else {

                        //System.out.println("-------------------");

                        //for join  join_claus
                        if (ctx.factored_select_stmt().select_core().join_clause() != null){
                            if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size() > 0) {


                                if (ctx.factored_select_stmt().select_core().result_column().size() > 0) {
                                    for (int i = 0; i < ctx.factored_select_stmt().select_core().result_column().size(); i++) {


                                        //System.out.println(ctx.factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());
                                        //*********star***********
                                        if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null) {
                                            System.out.println("++++++++++++++++++++++++++++0");

                                            //List<String> ch_join = new ArrayList<>();

                                            for (int g = 0; g < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); g++) {

                                                for (int j = 0; j < Main.table_list.size(); j++) {

                                                    if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(g).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {

                                                        for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                                            ///////
                                                            check_where_on(ctx,assigneSelect);
                                                            ///////

                                                            // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                                            for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {

                                                                Type type1 = new Type();

                                                                if(Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()))
                                                                {
                                                                    System.out.println("tyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyp create");
                                                                    for (int m = 0; m < Main.type_list.size(); m++) {
                                                                        if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                            for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                                                                                Type type2 = new Type();
                                                                                for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                                                                    type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                    columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                    //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                    // System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                }

                                                                            }
                                                                        }
                                                                    }

                                                                }

                                                                //flat
                                                                else if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                    //System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                                    //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                                    //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                    for (int m = 0; m < Main.table_list.size(); m++) {
                                                                        if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                            for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                Type type2 = new Type();
                                                                                for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                    type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                    columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                                    System.out.println(" cols : " + Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + " ");

                                                                                    System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                }

                                                                            }
                                                                        }
                                                                    }

//
                                                                }   //end flat
                                                                else {
                                                                    //normal column
                                                                    System.out.println("pppppppppppppppppppppppppppppppp");
                                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+"_"+Main.table_list.get(j).getTb_name().getAny_name().getName(), type1);
                                                                    //System.out.print("var_column_name : " + Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " , ");
                                                                    //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
                                                                }    //end of normal column
                                                            }
                                                            //}
                                                        }
                                                    }

                                                }

                                            }

                                        }

                                        //***********end star*******
                                        //aggregation function
                                        if (ctx.factored_select_stmt().select_core().result_column().get(i).expr() != null) {
                                            if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func() != null) {

                                                for (int v = 0; v < Main.symbolTable.getDeclaredAggregationFunction().size(); v++) {

                                                    agg_list.add(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName());

                                                    if (Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())) {

                                                        Type type1 = new Type();
//
                                                        type1.setName(Main.symbolTable.getDeclaredAggregationFunction().get(v).getReturn_type());
                                                        columns.put(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName(), type1);
                                                        System.out.print("column_name : " + Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName() + " , ");
                                                        System.out.println("var_column_type : " + columns.get(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName()).getName());
                                                    }

                                                }

                                                if (!(agg_list.contains(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText()))) {
                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getLine());
                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getCharPositionInLine());
                                                    System.err.println("undeclared function : " + "at line : "+assigneSelect.getLine()+" at col : "+ assigneSelect.getCol());
                                                   // System.exit(1);
                                                }

                                            }



                                            //check column when using join
                                            if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name() != null) {

                                                if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name() != null) {
                                                    System.out.println("llllllllllllllllllooooopp");
                                                    //List<String> ch_join = new ArrayList<>();
                                                    if (!(Main.table_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText()))) {
                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().getStart().getLine());
                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().getStart().getCharPositionInLine());
                                                        System.err.println("undeclared table : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                    //    System.exit(1);
                                                    }

                                                    for (int n = 0; n < Main.table_list.size(); n++) {
                                                        if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getTb_name().getAny_name().getName())) {
                                                            System.out.println("kkkkkkkkkkkkkkkkkkkk" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                            if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText())) {


                                                                for (int v = 0; v < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); v++) {

                                                                    //if(Main.column_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())){
                                                                    //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())) {
                                                                    if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getTb_name().getAny_name().getName())) {

                                                                        for (int c = 0; c < Main.table_list.get(n).getColumn_defs().size(); c++) {
                                                                            list_joins.add(Main.table_list.get(n).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                            System.out.println("xxxx " + Main.table_list.get(n).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                        }

                                                                    }
                                                                    //}
                                                                }
                                                                ///////
                                                                check_where_on (ctx,assigneSelect);
                                                               ///////

                                                                //******check join*****

                                                                for (int v = 0; v < Main.table_list.get(n).getColumn_defs().size(); v++) {

                                                                    if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {

                                                                        System.out.println("cols : " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                        //System.out.println("dddddddddddddddddddddd "+Main.table_list.get(n).getColumn_defs().size());
                                                                        try {
                                                                            for (int l = 0; l < Main.table_list.get(n).getColumn_defs().get(v).getType_names().size(); l++) {
                                                                                System.out.println("in fffffffff");
                                                                                Type type1 = new Type();
                                                                                type1.setName(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                //columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                //System.out.println("tyyyyyyyyyyype " + Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                //System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());

//                                                              //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);
                                                                                String col_name = " ";

                                                                                if(Main.type_name_list.contains(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
                                                                                {
                                                                                    check_type_join.add(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                }
                                                                                if (!unique.add(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {
                                                                                    System.out.println("+++++mnmnmn++++" + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                                    columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                    System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                                                    //System.out.println("typppppppppe name " + columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());

                                                                                } else {
                                                                                    if (!(Main.type_name_list.contains(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))){
                                                                                        columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                    }
                                                                                    //System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                                                    //System.out.println("typppppppppe name " + columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());
                                                                                }


                                                                            }
                                                                            /////////////////////////
                                                                            //////////////////////////////////////
                                                                            //flat type
                                                                            for(int y=0;y<check_type_join.size();y++) {
                                                                                if (Main.type_name_list.contains(check_type_join.get(y)))
                                                                                {
                                                                                    System.out.println("vvvvvvvvvvvvvv " + check_type_join.get(y));

                                                                                    for (int m = 0; m < Main.type_list.size(); m++) {
                                                                                        if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type_join.get(y))) {

                                                                                            // System.out.println("888888888888888888888888877777");
                                                                                            for (int c = 0; c < Main.type_list.get(m).getColumn_def_type().size(); c++) {
                                                                                                Type type2 = new Type();
                                                                                                for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(c).getType_names().size(); b++) {

                                                                                                    type2.setName(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());
                                                                                                    columns.put(Main.type_list.get(m).getColumn_def_type().get(c).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                                    column_add.add(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());

                                                                                                }



                                                                                            }


                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            //end flat type
                                                                            //flat
                                                                            try {
                                                                                if (Main.table_name.contains(columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName()).getName())) {
                                                                                    System.out.println("8888888888888888888888888");
                                                                                    for (int m = 0; m < Main.table_list.size(); m++) {

                                                                                        if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName()).getName())) {

                                                                                            System.out.println("888888888888888888888888877777");
                                                                                            for (int l = 0; l < Main.table_list.get(m).getColumn_defs().size(); l++) {
                                                                                                Type type2 = new Type();
                                                                                                for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(l).getType_names().size(); b++) {

                                                                                                    type2.setName(Main.table_list.get(m).getColumn_defs().get(l).getType_names().get(b).getNames().getName().getName());
                                                                                                    columns.put(Main.table_list.get(m).getColumn_defs().get(l).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName() + "_" + "flat", type2);
                                                                                                    System.out.print(" column_name : " + Main.table_list.get(m).getColumn_defs().get(l).getCol_name().getAny_name().getName() + " ");

                                                                                                    System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(l).getType_names().get(b).getNames().getName().getName());
                                                                                                }

                                                                                            }
                                                                                        }
                                                                                    }

                                                                                }
                                                                            }
                                                                            catch (NullPointerException e){}
                                                                            //end flat
                                                                            /////////////////////////////////////


                                                                            ///////////////////////
                                                                        } catch (IndexOutOfBoundsException e) {

                                                                        }


                                                                    }


                                                                }


                                                            } else {
                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                                System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                              //  System.exit(1);
                                                            }
                                                        }
                                                    }

                                                } else {
                                                    List<String> ch_join = new ArrayList<>();
                                                    //System.out.println(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText());
                                                    if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText())) {


                                                        for (int u = 0; u < Main.table_list.size(); u++) {

                                                            for (int v = 0; v < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); v++) {

                                                                //if(Main.column_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())){
                                                                //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())) {
                                                                if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(u).getTb_name().getAny_name().getName())) {

                                                                    for (int c = 0; c < Main.table_list.get(u).getColumn_defs().size(); c++) {
                                                                        list_joins.add(Main.table_list.get(u).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                        System.out.println("xxxx " + Main.table_list.get(u).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                    }

                                                                }
                                                                //////K_WHERE

                                                                for(int b=0 ; b<Main.table_list.size();b++) {
                                                                    for (int a = 0; a < Main.table_list.get(b).getColumn_defs().size(); a++) {
                                                                        for (int c = 0; c < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); c++) {
                                                                            if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(c).table_name().any_name().IDENTIFIER().getText()
                                                                                    .equals(Main.table_list.get(b).getTb_name().getAny_name().getName())) {
                                                                                ch_join.add(Main.table_list.get(b).getColumn_defs().get(a).getCol_name().getAny_name().getName());
                                                                                System.out.println("nnnnnnnnn : " + Main.table_list.get(b).getColumn_defs().get(a).getCol_name().getAny_name().getName());
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                                //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                                                //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                                                if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                                                {
                                                                    if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                                    {
                                                                        for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                                            if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                                                if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                                                {
                                                                                    for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                                    {
                                                                                        if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                                        {
                                                                                            for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                                            {
                                                                                                try{
                                                                                                    if (!(ch_join.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                        System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                      //  System.exit(1);
                                                                                                    }
                                                                                                    if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().IDENTIFIER().getText()))){
                                                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().getStart().getLine());
                                                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().getStart().getCharPositionInLine());
                                                                                                        System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                      //  System.exit(1);
                                                                                                    }
                                                                                                }
                                                                                                catch (NullPointerException e){}
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                            } else {
                                                                                if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                                    for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                                        try {
                                                                                            //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                                            if (!(ch_join.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                             //   System.exit(1);
                                                                                            }
                                                                                            if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().IDENTIFIER().getText()))){
                                                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().getStart().getLine());
                                                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().getStart().getCharPositionInLine());
                                                                                                System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                             //   System.exit(1);
                                                                                            }
                                                                                        } catch (NullPointerException e) {
                                                                                        }

                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                //////K_WHERE

                                                                /////K_ON//////

                                                                if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().size()>0)
                                                                {
                                                                    for(int h =0; h<ctx.factored_select_stmt().select_core().join_clause().join_constraint().size();h++){

                                                                        if(ctx.factored_select_stmt().select_core().join_clause().join_constraint(h).K_ON()!=null)
                                                                        {
                                                                            if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr()!=null)
                                                                            {
                                                                                if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().size()>0)
                                                                                {
                                                                                    for(int a = 0 ;a<ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().size();a++){

                                                                                        if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().IDENTIFIER().getText()))){
                                                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().getStart().getLine());
                                                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().getStart().getCharPositionInLine());
                                                                                            System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                        //    System.exit(1);
                                                                                        }

                                                                                        if (!(ch_join.contains(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().IDENTIFIER().getText()))) {
                                                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().getStart().getLine());
                                                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().getStart().getCharPositionInLine());
                                                                                            System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                       //     System.exit(1);
                                                                                        }

                                                                                    }
                                                                                }
//
                                                                            }
                                                                        }
                                                                    }
                                                                }


                                                                ////K_ON///////

                                                                //}
                                                            }

                                                            //***********check join***********

                                                            for (int v = 0; v < Main.table_list.get(u).getColumn_defs().size(); v++) {

                                                                if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText().equals(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {


                                                                    //System.out.println("cols : " + ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText());
                                                                    try {
                                                                        for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
                                                                            System.out.println("in fffffffff");
                                                                            Type type1 = new Type();
                                                                            type1.setName(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
//                                                              //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);

                                                                            //if (!unique.add(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()))
                                                                            if (!unique.add(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())){

                                                                                System.out.println("OOOOOOOOOOOOOOOOO " + Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                                System.err.println("ambiguous column : ");
                                                                             //   System.exit(1);
                                                                            }
                                                                            if(Main.type_name_list.contains(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
                                                                            {
                                                                                check_type_join.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                            }
                                                                            else {
                                                                                columns.put(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName(), type1);
                                                                            }

                                                                        }

                                                                        //////////////////////////////////////
                                                                        //flat type
                                                                        for(int y=0;y<check_type_join.size();y++) {
                                                                            if (Main.type_name_list.contains(check_type_join.get(y)))
                                                                            {
                                                                                System.out.println("vvvvvvvvvvvvvv " + check_type_join.get(y));

                                                                                for (int m = 0; m < Main.type_list.size(); m++) {
                                                                                    if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type_join.get(y))) {

                                                                                        // System.out.println("888888888888888888888888877777");
                                                                                        for (int c = 0; c < Main.type_list.get(m).getColumn_def_type().size(); c++) {
                                                                                            Type type2 = new Type();
                                                                                            for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(c).getType_names().size(); b++) {

                                                                                                type2.setName(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());
                                                                                                columns.put(Main.type_list.get(m).getColumn_def_type().get(c).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                                column_add.add(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());

                                                                                            }



                                                                                        }


                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        //end flat type

                                                                        //flat
                                                                        try {
                                                                            if (Main.table_name.contains(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName())) {
                                                                                //System.out.println("8888888888888888888888888");
                                                                                for (int m = 0; m < Main.table_list.size(); m++) {

                                                                                    if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName())) {

                                                                                        // System.out.println("888888888888888888888888877777");
                                                                                        for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                            Type type2 = new Type();
                                                                                            for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                                type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                                columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                                                System.out.print(" column_name : " + Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + " ");

                                                                                                System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                            }

                                                                                        }
                                                                                    }
                                                                                }

                                                                            }
                                                                        }
                                                                        catch (NullPointerException e)
                                                                        {

                                                                        }
                                                                        //end flat

                                                                        /////////////////////////////////////
                                                                    } catch (IndexOutOfBoundsException e) {
                                                                    }


                                                                }


                                                            }


                                                        }

                                                    } else {
                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                        System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                     //   System.exit(1);
                                                    }
                                                }
                                                //

                                                if (!(list_joins.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText()))) {

                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                    System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                   // System.exit(1);
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                        }

                        if(ctx.factored_select_stmt().select_core().table_or_subquery().size()>0) {
                            ///**************//
                            for(int j=0;j<ctx.factored_select_stmt().select_core().table_or_subquery().size();j++){

                                //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText())){



                                // System.out.println("Test_1 : "+ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText());



                                if(ctx.factored_select_stmt().select_core().result_column().size()>0)
                                {
                                    List<String> checks = new ArrayList<>();

                                    for(int i=0;i<ctx.factored_select_stmt().select_core().result_column().size();i++) {




                                        //aggregation function
                                        if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func() != null) {

                                            for (int v = 0; v < Main.symbolTable.getDeclaredAggregationFunction().size(); v++) {

                                                agg_list.add(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName());

                                                if (Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())) {

                                                    Type type1 = new Type();
//
                                                    type1.setName(Main.symbolTable.getDeclaredAggregationFunction().get(v).getReturn_type());
                                                    columns.put(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName(), type1);
                                                    System.out.print("column_name : " + Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName() + " , ");
                                                    System.out.println("var_column_type : " + columns.get(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName()).getName());
                                                }


                                            }

                                            if(!(agg_list.contains(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())))
                                            {
                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getLine());
                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getCharPositionInLine());
                                                System.err.println("undeclared function : " + "at line : "+assigneSelect.getLine()+" at col : "+ assigneSelect.getCol());
                                           //     System.exit(1);
                                            }

                                        }




                                        if(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name()!=null) {


                                            if(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name()!=null)
                                            {
                                                System.out.println("111111111111111111111111111122");
                                                if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText())))
                                                {
                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().getStart().getLine());
                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().getStart().getCharPositionInLine());
                                                    System.err.println("undeclared table : "+ " at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                  //  System.exit(1);
                                                }
                                            }

                                            if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getText())) {
                                                //System.out.println("cols : "+ ctx.factored_select_stmt().select_core().result_column().get(i).getText());
                                                for (int u = 0; u < Main.table_list.size(); u++) {
                                                    if (ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText().equals(Main.table_list.get(u).getTb_name().getAny_name().getName())) {



                                                        for(int g =0;g<Main.table_list.get(u).getColumn_defs().size();g++){
                                                            list.add(Main.table_list.get(u).getColumn_defs().get(g).getCol_name().getAny_name().getName());
                                                        }

                                                        //////K_WHERE
                                                        //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                                        //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                                        if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                                        {
                                                            if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                            {
                                                                for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                                    if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                                        if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                                        {
                                                                            for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                            {
                                                                                if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                                {
                                                                                    for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                                    {
                                                                                        try{
                                                                                            if (!(list.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                               // System.exit(1);
                                                                                            }
                                                                                        }
                                                                                        catch (NullPointerException e){}
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                    } else {
                                                                        if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                            for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                                try {
                                                                                    //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                                    if (!(list.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                                        System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                      //  System.exit(1);
                                                                                    }
                                                                                } catch (NullPointerException e) {
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        //////K_WHERE

                                                        //list.add(Main.table_list.get(u).getColumn_defs().get(1).getCol_name().getAny_name().getName());

                                                        for (int v = 0; v < Main.table_list.get(u).getColumn_defs().size(); v++) {


                                                            if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText().equals(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {



                                                               /**********/
                                                                flat_result_column(ctx, assigneSelect, columns, column_add,check_type, i,u,v);
//                                                                for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
//                                                                    flat_result_column(ctx, assigneSelect, columns, column_add, u, i, v,l);
//                                                                }
                                                                //System.out.println("cols : " + ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText());
//                                                                for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
//                                                                    System.out.println("in fffffffff");
//                                                                    Type type1 = new Type();
//
//                                                                    if(Main.type_name_list.contains(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
//                                                                    {
//                                                                        System.out.println("ccccckkcbcncncyu888888");
//                                                                        check_type.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
//                                                                    }
//                                                                    else {
//                                                                        type1.setName(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
////                                                                  //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);
////
//                                                                        columns.put(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName(), type1);
//                                                                        column_add.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
//                                                                    }
//                                                                }


                                                                //flat type
                                                                flat_type_result_column(ctx,assigneSelect,columns,check_type,i,u,v);
//                                                                for(int y=0;y<check_type.size();y++) {
//                                                                    if (Main.type_name_list.contains(check_type.get(y)))
//                                                                    {
//                                                                        System.out.println("vvvvvvvvvvvvvv " + check_type.get(y));
//
//                                                                        for (int m = 0; m < Main.type_list.size(); m++) {
//                                                                            if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type.get(y))) {
//
//                                                                                // System.out.println("888888888888888888888888877777");
//                                                                                for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
//                                                                                    Type type2 = new Type();
//                                                                                    for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {
//
//                                                                                        type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
//                                                                                        columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
//                                                                                        column_add.add(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
//
//                                                                                    }
//
//
//
//                                                                                }
//
//
//                                                                            }
//                                                                        }
//                                                                    }
//                                                                }
                                                                //end flat type

                                                                //flat
//                                                                try {
//                                                                    if (Main.table_name.contains(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName())) {
//
//                                                                        System.out.println("8888888888888888888888888 " + columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName());
//                                                                        for (int m = 0; m < Main.table_list.size(); m++) {
//
//                                                                            if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName())) {
//
//                                                                                // System.out.println("888888888888888888888888877777");
//                                                                                for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
//                                                                                    Type type2 = new Type();
//                                                                                    for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {
//
//                                                                                        type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//                                                                                        columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
//                                                                                        column_add.add(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//
//                                                                                      //s added
//                                                                                        System.out.print(" column_name : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");
//
//                                                                                        System.out.println(" var_column_type : "+Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
//
//
//                                                                                    }
//
//
//                                                                                }
//
//
//                                                                            }
//
//
//                                                                        }
//
//
//                                                                    }
//                                                                }
//                                                                catch (NullPointerException e){}





                                                                //end flat
                                                            //s added
                                                                /*System.out.print("column_name : " + Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + " , ");
                                                                System.out.println("var_column_type : " + columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());
*/


//
                                                                test = true;


                                                            }

                                                            if (!(list.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText())))  {

                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getLine());
                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getCharPositionInLine());
                                                                System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                               // System.exit(1);

                                                                //System.err.println("ttttest");

                                                            }

                                                        }



                                                    }



                                                }



                                            } else {


                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getLine());
                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getCharPositionInLine());
                                                System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                            //    System.exit(1);
                                            }
                                        }
                                    }
                                }
                                //}
//                                else
//                                {
////
//                                    System.err.println("undefine table");
//                                    System.exit(1);
//                                }
                            }


                        }





                    }
                    if(ctx.factored_select_stmt().select_core().join_clause()==null) {
                        type_name = " ";
                        //to get all table name in out select
                        for (int m = 0; m < ctx.factored_select_stmt().select_core().table_or_subquery().size(); m++) {

                            // if aliases in out select
                            if (ctx.factored_select_stmt().select_core().table_or_subquery(m).K_AS() != null) {
                                type_name += "_" + ctx.factored_select_stmt().select_core().table_or_subquery(m).table_alias().any_name().getText();
                            } else {
                                type_name += "_" + ctx.factored_select_stmt().select_core().table_or_subquery(m).table_name().any_name().getText();
                            }
                        }
                    }
                    System.err.println("ttttttooooooooooooooooooooooooooooooooooooooooooo first => "+type_name);
                 //   Main.symbolTable.addScope(currentScope);
                    //System.out.println("----" + type_name);
                    type.setName(type_name);
                    type.setColumns(columns);
                    varSymbol.setType(type);
                    varSymbol.setScope(currentScope);
                    currentScope.addSymbol(varName, varSymbol);
                    System.out.println("current Scope { "+currentScope.getId() + " }");
                    System.out.println("variable type : " + varSymbol.getType().getName());
                    System.out.println("var scope : " + varSymbol.getScope().getId());

                 //   Main.symbolTable.addScope(currentScope);

                }
            }
        }
        else {
            Symbol symbol =  currentScope.getSymbolMap().get(var_name);
            if(symbol!=null)
            {
                Symbol varSymbol = new Symbol();
                String varName = ctx.IDENTIFIER().getText();
                varSymbol.setName(varName);
                varSymbol.setIsParam(false);
                assigneSelect.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));

                //for join clause
                if (ctx.factored_select_stmt().select_core().join_clause() != null) {
                    //for table_or_subquery
                    for (int i = 0; i < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); i++) {

                        if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).K_AS() != null) {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).table_alias().any_name().getText();
                        } else {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).table_name().any_name().getText();
                        }
                    }

                    if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null) {
                        System.out.println("Staaaaaar");
                    }

                    type.setName(type_name);
                    type.setColumns(columns);
                    if(symbol.getType()!=null)
                    {
                        if(symbol.getType().getName()!=type.getName())
                        {
                            System.err.println("Error for assigning another type declared variable "+symbol.getName()+"the colume is : "+ assigneSelect.getCol()+"   the line is :  " +assigneSelect.getLine());
                        }
                        else
                        {
                            currentScope.getSymbolMap().get(var_name).setType(type);
                            System.out.println("var type : "+symbol.getType().getName());
                            System.out.println("scop var : "+symbol.getScope().getId());
                            // assigneSelect.setSymbol(symbol);
                        }
                    }
                    else {
                        currentScope.getSymbolMap().get(var_name).setType(type);
                        System.out.println("var type : "+symbol.getType().getName());
                        System.out.println("scop var : "+symbol.getScope().getId());
                        // assigneSelect.setSymbol(symbol);
                    }
                }

                //for table_or_subquery
                if (ctx.factored_select_stmt().select_core().table_or_subquery().size() > 0) {


                    System.err.println("second table_or_subquery ");
                    String tb_name_out_subquery = " ";

                    //to get all table name in out select
               /*     for(int m =0;m<ctx.factored_select_stmt().select_core().table_or_subquery().size();m++){

                        // if aliases in out select
                        if (ctx.factored_select_stmt().select_core().table_or_subquery(m).K_AS() != null) {
                            type_name += "_" + ctx.factored_select_stmt().select_core().table_or_subquery(m).table_alias().any_name().getText();
                        }else{
                            type_name += "_" + ctx.factored_select_stmt().select_core().table_or_subquery(m).table_name().any_name().getText();
                        }
                    }


                    System.err.println("sooooooooooooooooooooooooooooooooooooooooooo => "+type_name);
*/

                    //= ctx.factored_select_stmt().select_core().table_or_subquery(0).table_name().any_name().getText();


                    //subquery
                    if(ctx.factored_select_stmt().select_core().K_WHERE() != null ){



                        for(int i =0;i<ctx.factored_select_stmt().select_core().expr().size();i++){


                            for(int j =0;j<ctx.factored_select_stmt().select_core().expr(i).expr().size();j++) {

                                 try {
                                     if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt() != null) {


                                         //check if column in subquery is one
                                         if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().size() == 1) {


                                             if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size() > 0) {


                                                 for (int e = 0; e < ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size(); e++) {
                                                     //System.out.println("table name : " + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery(e).table_name().any_name().getText());
                                                     all_table_in_subquery.add(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery(e).table_name().any_name().getText());
                                                 }

                                                 System.out.println("all_table_in_subquery  => " + all_table_in_subquery);
                                                 System.out.println("---------------------------------- ");

                                             }


                                             ////////////here
                                             for (int g = 0; g < Main.table_list.size(); g++) {

                                                 if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size() > 0) {


                                                     //to get table with cols with type of each col
                                                     for (int n = 0; n < all_table_in_subquery.size(); n++) {
                                                         if (Main.table_list.get(g).getTb_name().getAny_name().getName().equals(all_table_in_subquery.get(n))) {

                                                             System.out.println("table name :" + Main.table_list.get(g).getTb_name().getAny_name().getName());
////77777777777777777777
                                                             for (int cc = 0; cc < Main.table_list.get(g).getColumn_defs().size(); cc++) {


                                                                 System.out.println("Column => " + Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName());
                                                                 all_cols_name_in_subquery_table.add(Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName());


                                                                 //to get type the column
                                                                 Type type2 = new Type();
                                                                 for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(cc).getType_names().size(); l++) {
                                                                     type2.setName(Main.table_list.get(g).getColumn_defs().get(cc).getType_names().get(l).getNames().getName().getName());

                                                                     //to store name and type of col
                                                                     columns.put(Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName(), type2);
                                                                     System.out.println("Type : " + type2.getName());
                                                                     //  System.out.println("coooooooooooooooooooool : " +  columns);
                                                                 }

                                                             }
                                                         }


                                                     }


                                                 }


                                             }

                                             //System.err.println("all_cols_name in sub : " +all_cols_name_in_subquery_table);
                                             //System.err.println("this col => " + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());


                                             //check if col in subquery is found in tables

                                             //check of column in subquery is ( * )
                                             if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).STAR() != null) {
                                                 System.err.println("columns in subquery can't be start ( * )");
                                             } else if (all_cols_name_in_subquery_table.contains(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText())) {

                                                 for (int a = 0; a < columns.size(); a++) {


                                                     //  System.err.println("type column IDENTEFIR in subquery : " + columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName());
                                                     //  System.err.println("yyyyyyyyyyyyyyyyyyyyyyyyyyiiiiiiiii : " + ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText());


                                                     for (int g = 0; g < Main.table_list.size(); g++) {
                                                         for (int k = 0; k < Main.table_list.get(g).getColumn_defs().size(); k++) {


                                                             //to get type the column
                                                   /*     Type type2= new Type();
                                                        for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(k).getType_names().size(); l++) {
                                                            type2.setName(Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                            //to store name and type of col
                                                            columns.put( Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type2);
                                                            //  System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyiiiiiiiii type : " +  type2.getName());
                                                        }*/

                                                         }

                                                     }


                                                     for (int g = 0; g < Main.table_list.size(); g++) {
                                                         for (int k = 0; k < Main.table_list.get(g).getColumn_defs().size(); k++) {


                                                             //to get type the column
                                                             Type type2 = new Type();
                                                             for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(k).getType_names().size(); l++) {
                                                                 type2.setName(Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());
                                                                 //System.err.println("tttttttttttttttttttttttt :=> "+ Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " => " + type2.getName());
                                                                 //to store name and type of col
                                                                 columns2.put(Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type2);
                                                                 // System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb : " + Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                                                 //  System.err.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy :=> "+ columns);


                                                                 if (Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())) {

                                                                     // System.err.println("howwwwwwwwwwwwwwwwwwwwwwwwww ????");
                                                                     //   System.err.println("columnsssssssssss //////"+columns.get("id").getName());
                                                                     //    System.err.println("type of col : "+ Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                     //  System.out.println("type column IDENTEFIR in subquery kookokokokokoooooooooooooooooooooooo: " + columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName());


                                                                     //System.err.print("id :  "+ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());
                                                                     if (Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName().equals(columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName())) {

                                                                         // System.err.println("alllllllllllllllllllllllllllllllllllll is trueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
                                                                     } else {
                                                                         System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText() + " and " + Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " must be the same type ... ");

                                                                     }


                                                                 }


                                                             }
                                                         }
                                                     }


                                                 }

                                             } else {
                                                 System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText() + " not found in tables subquery .... ");
                                             }

                                             /////end here



                                  /*
                                   all_cols_name.add(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                   if (Main.column_name.contains(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText())) {
                                        System.err.println("columns in subquery found in table subquery" + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText());
                                    }else{
                                        System.err.println("columns in subquery noooooooooooooooooot found in table subquery" + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText());
                                    }*/


                                         } else {
                                             System.err.println("columns in subquery must be one ...");
                                         }


                                     }
                                 }
                                 catch (NullPointerException e){}
//                                 else{
//                                    System.err.println("subquery must be select");
//                                }

                                // break;

                            }

                            //  System.err.println("lll" + ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText() );

                            // System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).factored_select_stmt().select_core());




                            //Where In (expr IN SelectExpr )
                            if(ctx.factored_select_stmt().select_core().expr(i).K_IN() != null){



                                if(ctx.factored_select_stmt().select_core().expr(i).expr(i) != null){ //check IDENTIFIER is write


                                                                        /* System.err.println("subquery");
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().IDENTIFIER());
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());
                                    */





                                                                           /* for(int c=0;c<ctx.factored_select_stmt().select_core().table_or_subquery().size();c++){
                                                                                System.err.println("koko:"+ctx.factored_select_stmt().select_core().table_or_subquery(c).table_name().any_name().getText());

                                                                            }*/




                                    ///check IDENTIFIER is found in cols
                                    for (int j = 0; j < Main.table_list.size(); j++) {

                                        //to get all cols in all table (select * from test1,test2)
                                        for (int p = 0; p < ctx.factored_select_stmt().select_core().table_or_subquery().size(); p++) {

                                            //check if table (in out select) is found in table_list
                                            if (ctx.factored_select_stmt().select_core().table_or_subquery(p).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {

                                                //table flat


                                                for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                                    // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                                    for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {

                                                        Type type1 = new Type();




                                                        //flat (table)
                                                        if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                            System.err.println("================  //flat (table) ======================== ");

                                                            type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                            columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                            // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                            System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                            //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                            for (int m = 0; m < Main.table_list.size(); m++) {
                                                                if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {

                                                                    for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                        Type type2 = new Type();
                                                                        for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                            type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                            columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName(), type2);
                                                                            //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                            System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                        }

                                                                    }

                                                                }
                                                            }

                                                            //
                                                        }   //end flat


                                                        //flat (type)
                                                        else if (Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                            System.err.println("================  //flat (type) ======================== ");

                                                            type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                            columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                            // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                            System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                            //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                            for (int m = 0; m < Main.type_name_list.size(); m++) {
                                                                if (Main.type_name_list.get(m).equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {

                                                                    System.err.println("================ kk ======================== ");

                                                                    for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                        Type type2 = new Type();


                                                                        for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                            System.err.println("================ hhhhhhhhhhhhhhhhhhh ======================== " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().size());

                                                                            //    if((Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName().equals())

                                                                            type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                            columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName(), type2);
                                                                            //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                            System.out.println(" var_column_type : " + Main.table_list.get(m).getTb_name().getAny_name().getName());
                                                                        }

                                                                    }
                                                                }
                                                            }

                                                        }//end flat type


                                                        else {    //normal column
                                                            //   System.err.println("col=>");

                                                            type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                            columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                            // System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
                                                            // System.out.println("var_column_type : " + Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                                            all_cols_name.add(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());




                                                                                               /*     for(int y=0;y<columns.size();y++){
                                                                                                        if(columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).equals(ctx
                                                                                                        .factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())){
                                                                                                                    System.err.println("gogogogogogoooooooooooooooggggggggggg"+columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()));
                                                                                                        }else{

                                                                                                            System.err.println("noooooooooooooooooooooooooooo"+columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()));
                                                                                                        }



                                                                                                    }*/




                                                        }    //end of normal column


                                                    }


                                                    //}
                                                }


                                            }


                                        }


                                    }






                                    ////////*********************************************

                                                                           /* System.err.println("all_cols_name in outer select : " +all_cols_name);
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText());
                                    */

                                    if(all_cols_name.contains(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())){
                                        //  System.err.println(" column  found " );

                                    }else{
                                        System.err.println( ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText() + " column not found " );
                                    }





                                }




                                //  System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());


                            }else{// where stmt without IN
                                //  System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());
                                System.err.println("check where stmt");
                            }


                        }






                    }
                    else{
                        if (ctx.factored_select_stmt().select_core().table_or_subquery(0).K_AS() != null) {
                            type_name = ctx.factored_select_stmt().select_core().table_or_subquery(0).table_alias().any_name().getText();

                        } else {
                            type_name = ctx.factored_select_stmt().select_core().table_or_subquery(0).table_name().any_name().getText();
                        }
                    }






                    //}

                /* else {
                    for (int i = 0; i < ctx.factored_select_stmt().select_core().table_or_subquery().size(); i++) {

                        if (ctx.factored_select_stmt().select_core().table_or_subquery(i).K_AS() != null) {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().table_or_subquery(i).table_alias().any_name().getText();
                        } else {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().table_or_subquery(i).table_name().any_name().getText();
                        }
                    }
                }*/


                    type.setName(type_name);
                    type.setColumns(columns);
                    if(symbol.getType()!=null)
                    {
                        if(symbol.getType().getName()!=type.getName())
                        {
                            System.err.println("Error for assigning another type declared variable "+symbol.getName()+"the colume is : "+ assigneSelect.getCol()+"   the line is :  " +assigneSelect.getLine());
                        }
                        else
                        {
                            currentScope.getSymbolMap().get(var_name).setType(type);
                            System.out.println("var type : "+symbol.getType().getName());
                            System.out.println("scop var : "+symbol.getScope().getId());
                            // assigneSelect.setSymbol(symbol);
                        }
                    }
                    else {
                        currentScope.getSymbolMap().get(var_name).setType(type);
                        System.out.println("var type : "+symbol.getType().getName());
                        System.out.println("scop var : "+symbol.getScope().getId());
                        // assigneSelect.setSymbol(symbol);
                    }
                }

                //for result_column
                if (ctx.factored_select_stmt().select_core().result_column().size() > 0) {

                    System.out.println("++++++++++++++++++++++++++++0");
                    // if (ctx.factored_select_stmt().select_core().result_column(0).!=null){}   agg fumc
                    if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null&&ctx.factored_select_stmt().select_core().join_clause()==null) {
                        System.out.println("++++++++++++++++++++++++++++0");
                        List<String> checks = new ArrayList<>();
                        for (int i = 0; i < ctx.factored_select_stmt().select_core().result_column().size(); i++) {

                            for (int j = 0; j < Main.table_list.size(); j++) {


                                //to get all cols in all table (select * from test1,test2)
                                for (int p = 0; p < ctx.factored_select_stmt().select_core().table_or_subquery().size(); p++) {

                                    //check if table (in out select) is found in table_list


                                    if (ctx.factored_select_stmt().select_core().table_or_subquery(p).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {
                                        System.out.println("-----------------------------");
                                        for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                             //////K_WHERE
                                            for (int r = 0; r < Main.table_list.get(j).getColumn_defs().size(); r++) {
                                                checks.add( Main.table_list.get(j).getColumn_defs().get(r).getCol_name().getAny_name().getName());
                                                //System.out.println("nmnmnmnmnmnm "+Main.table_list.get(j).getColumn_defs().get(r).getCol_name().getAny_name().getName());
                                            }
                                            //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                            //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                            if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                            {
                                                if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                {
                                                    for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                        if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                            if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                            {
                                                                for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                {
                                                                    if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                    {
                                                                        for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                        {
                                                                            try{
                                                                                if (!(checks.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                    System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                 //   System.exit(1);
                                                                                }
                                                                            }
                                                                            catch (NullPointerException e){}
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                        } else {
                                                            if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                    try {
                                                                        //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                        if (!(checks.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                            System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                         //   System.exit(1);
                                                                        }
                                                                    } catch (NullPointerException e) {
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            //////K_WHERE

                                            // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                            for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {

                                                Type type1 = new Type();

                                                //****** type flat  ******
                                                //    System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkl " + Main.type_name_list.get(0));

                                                if (Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                    System.out.println("tyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyp create");
                                                    for (int m = 0; m < Main.type_list.size(); m++) {
                                                        if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                            for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                                                                Type type2 = new Type();
                                                                for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                                                    type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                                                    columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                    //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                    // System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                }

                                                            }
                                                        }
                                                    }

                                                }

                                                //****** end of type flat *****

                                                //table flat
                                                else if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                    //System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                    //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                    //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                    for (int m = 0; m < Main.table_list.size(); m++) {
                                                        if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                            for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                Type type2 = new Type();
                                                                for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                    type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                    columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                    //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                    System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                }

                                                            }
                                                        }
                                                    }

//
                                                }   //end table flat
                                                else {
                                                    //normal column
                                                    //  System.out.println("pppppppppppppppppppppppppppppppp");
                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                    // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                    System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
                                                }    //end of normal column


                                            }
                                            //}
                                        }
                                    }


                                }

                            }

                        }
                    }

                    else {

                        //System.out.println("-------------------");

                        //for join  join_claus
                        if (ctx.factored_select_stmt().select_core().join_clause() != null){
                            if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size() > 0) {


                                if (ctx.factored_select_stmt().select_core().result_column().size() > 0) {
                                    for (int i = 0; i < ctx.factored_select_stmt().select_core().result_column().size(); i++) {


                                        //System.out.println(ctx.factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());
                                        //*********star***********
                                        if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null) {
                                            System.out.println("++++++++++++++++++++++++++++0");

                                           // List<String> ch_join = new ArrayList<>();

                                            for (int g = 0; g < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); g++) {

                                                for (int j = 0; j < Main.table_list.size(); j++) {

                                                    if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(g).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {

                                                        for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                                            ///////
                                                            check_where_on(ctx,assigneSelect);
                                                            ///////

                                                            // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                                            for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {

                                                                Type type1 = new Type();

                                                                if(Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()))
                                                                {
                                                                    System.out.println("tyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyp create");
                                                                    for (int m = 0; m < Main.type_list.size(); m++) {
                                                                        if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                            for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                                                                                Type type2 = new Type();
                                                                                for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                                                                    type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                    columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                    //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                    // System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                }

                                                                            }
                                                                        }
                                                                    }

                                                                }

                                                                //flat
                                                                else if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                    //System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                                    //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                                    //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                    for (int m = 0; m < Main.table_list.size(); m++) {
                                                                        if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                            for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                Type type2 = new Type();
                                                                                for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                    type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                    columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                                    System.out.println(" cols : " + Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + " ");

                                                                                    System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                }

                                                                            }
                                                                        }
                                                                    }

//
                                                                }   //end flat
                                                                else {
                                                                    //normal column
                                                                    System.out.println("pppppppppppppppppppppppppppppppp");
                                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+"_"+Main.table_list.get(j).getTb_name().getAny_name().getName(), type1);
                                                                    //System.out.print("var_column_name : " + Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " , ");
                                                                    //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
                                                                }    //end of normal column
                                                            }
                                                            //}
                                                        }
                                                    }

                                                }

                                            }

                                        }

                                        //***********end star*******
                                        //aggregation function
                                        if (ctx.factored_select_stmt().select_core().result_column().get(i).expr() != null) {
                                            if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func() != null) {

                                                for (int v = 0; v < Main.symbolTable.getDeclaredAggregationFunction().size(); v++) {

                                                    agg_list.add(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName());

                                                    if (Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())) {

                                                        Type type1 = new Type();
//
                                                        type1.setName(Main.symbolTable.getDeclaredAggregationFunction().get(v).getReturn_type());
                                                        columns.put(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName(), type1);
                                                        System.out.print("column_name : " + Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName() + " , ");
                                                        System.out.println("var_column_type : " + columns.get(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName()).getName());
                                                    }

                                                }

                                                if (!(agg_list.contains(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText()))) {
                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getLine());
                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getCharPositionInLine());
                                                    System.err.println("undeclared function : " + "at line : "+assigneSelect.getLine()+" at col : "+ assigneSelect.getCol());
                                                 //   System.exit(1);
                                                }

                                            }



                                            //check column when using join
                                            if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name() != null) {

                                                if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name() != null) {

                                                   // List<String> ch_join = new ArrayList<>();
                                                    if (!(Main.table_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText()))) {
                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().getStart().getLine());
                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().getStart().getCharPositionInLine());
                                                        System.err.println("undeclared table : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                     //   System.exit(1);
                                                    }

                                                    for (int n = 0; n < Main.table_list.size(); n++) {
                                                        if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getTb_name().getAny_name().getName())) {
                                                            System.out.println("kkkkkkkkkkkkkkkkkkkk" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                            if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText())) {


                                                                for (int v = 0; v < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); v++) {

                                                                    //if(Main.column_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())){
                                                                    //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())) {
                                                                    if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getTb_name().getAny_name().getName())) {

                                                                        for (int c = 0; c < Main.table_list.get(n).getColumn_defs().size(); c++) {
                                                                            list_joins.add(Main.table_list.get(n).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                            System.out.println("xxxx " + Main.table_list.get(n).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                        }

                                                                    }
                                                                    //}
                                                                }

                                                                ///////
                                                                check_where_on (ctx,assigneSelect);
                                                                ///////

                                                                //******check join*****

                                                                for (int v = 0; v < Main.table_list.get(n).getColumn_defs().size(); v++) {

                                                                    if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {

                                                                        System.out.println("cols : " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                        //System.out.println("dddddddddddddddddddddd "+Main.table_list.get(n).getColumn_defs().size());
                                                                        try {
                                                                            for (int l = 0; l < Main.table_list.get(n).getColumn_defs().get(v).getType_names().size(); l++) {
                                                                                System.out.println("in fffffffff");
                                                                                Type type1 = new Type();
                                                                                type1.setName(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                //columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                //System.out.println("tyyyyyyyyyyype " + Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                //System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());

//                                                              //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);
                                                                                String col_name = " ";

                                                                                if(Main.type_name_list.contains(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
                                                                                {
                                                                                    check_type_join.add(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                }
                                                                                if (!unique.add(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {
                                                                                    System.out.println("+++++mnmnmn++++" + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                                    columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                    System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                                                    //System.out.println("typppppppppe name " + columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());

                                                                                } else {
                                                                                    if (!(Main.type_name_list.contains(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))){
                                                                                        columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                    }
                                                                                    //System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                                                    //System.out.println("typppppppppe name " + columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());
                                                                                }


                                                                            }
                                                                            /////////////////////////
                                                                            //////////////////////////////////////
                                                                            //flat type
                                                                            for(int y=0;y<check_type_join.size();y++) {
                                                                                if (Main.type_name_list.contains(check_type_join.get(y)))
                                                                                {
                                                                                    System.out.println("vvvvvvvvvvvvvv " + check_type_join.get(y));

                                                                                    for (int m = 0; m < Main.type_list.size(); m++) {
                                                                                        if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type_join.get(y))) {

                                                                                            // System.out.println("888888888888888888888888877777");
                                                                                            for (int c = 0; c < Main.type_list.get(m).getColumn_def_type().size(); c++) {
                                                                                                Type type2 = new Type();
                                                                                                for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(c).getType_names().size(); b++) {

                                                                                                    type2.setName(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());
                                                                                                    columns.put(Main.type_list.get(m).getColumn_def_type().get(c).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                                    column_add.add(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());

                                                                                                }



                                                                                            }


                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            //end flat type
                                                                            //flat
                                                                            try {
                                                                                if (Main.table_name.contains(columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName()).getName())) {
                                                                                    System.out.println("8888888888888888888888888");
                                                                                    for (int m = 0; m < Main.table_list.size(); m++) {

                                                                                        if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName()).getName())) {

                                                                                            System.out.println("888888888888888888888888877777");
                                                                                            for (int l = 0; l < Main.table_list.get(m).getColumn_defs().size(); l++) {
                                                                                                Type type2 = new Type();
                                                                                                for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(l).getType_names().size(); b++) {

                                                                                                    type2.setName(Main.table_list.get(m).getColumn_defs().get(l).getType_names().get(b).getNames().getName().getName());
                                                                                                    columns.put(Main.table_list.get(m).getColumn_defs().get(l).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName() + "_" + "flat", type2);
                                                                                                    System.out.print(" column_name : " + Main.table_list.get(m).getColumn_defs().get(l).getCol_name().getAny_name().getName() + " ");

                                                                                                    System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(l).getType_names().get(b).getNames().getName().getName());
                                                                                                }

                                                                                            }
                                                                                        }
                                                                                    }

                                                                                }
                                                                            }
                                                                            catch (NullPointerException e){}
                                                                            //end flat
                                                                            /////////////////////////////////////


                                                                            ///////////////////////
                                                                        } catch (IndexOutOfBoundsException e) {

                                                                        }


                                                                    }


                                                                }


                                                            } else {
                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                                System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                              //  System.exit(1);
                                                            }
                                                        }
                                                    }

                                                } else {
                                                    List<String> ch_join = new ArrayList<>();
                                                    //System.out.println(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText());
                                                    if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText())) {


                                                        for (int u = 0; u < Main.table_list.size(); u++) {

                                                            for (int v = 0; v < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); v++) {

                                                                //if(Main.column_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())){
                                                                //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())) {
                                                                if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(u).getTb_name().getAny_name().getName())) {

                                                                    for (int c = 0; c < Main.table_list.get(u).getColumn_defs().size(); c++) {
                                                                        list_joins.add(Main.table_list.get(u).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                        System.out.println("xxxx " + Main.table_list.get(u).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                    }

                                                                }
                                                                //}
                                                            }


                                                            //////K_WHERE

                                                            for(int b=0 ; b<Main.table_list.size();b++) {
                                                                for (int a = 0; a < Main.table_list.get(b).getColumn_defs().size(); a++) {
                                                                    for (int c = 0; c < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); c++) {
                                                                        if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(c).table_name().any_name().IDENTIFIER().getText()
                                                                                .equals(Main.table_list.get(b).getTb_name().getAny_name().getName())) {
                                                                            ch_join.add(Main.table_list.get(b).getColumn_defs().get(a).getCol_name().getAny_name().getName());
                                                                            System.out.println("nnnnnnnnn : " + Main.table_list.get(b).getColumn_defs().get(a).getCol_name().getAny_name().getName());
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                                            //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                                            if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                                            {
                                                                if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                                {
                                                                    for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                                        if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                                            if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                                            {
                                                                                for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                                {
                                                                                    if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                                    {
                                                                                        for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                                        {
                                                                                            try{
                                                                                                if (!(ch_join.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                    System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                //    System.exit(1);
                                                                                                }
                                                                                                if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().IDENTIFIER().getText()))){
                                                                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().getStart().getLine());
                                                                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().getStart().getCharPositionInLine());
                                                                                                    System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                 //   System.exit(1);
                                                                                                }
                                                                                            }
                                                                                            catch (NullPointerException e){}
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                        } else {
                                                                            if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                                for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                                    try {
                                                                                        //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                                        if (!(ch_join.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                                            System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                       //     System.exit(1);
                                                                                        }
                                                                                        if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().IDENTIFIER().getText()))){
                                                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().getStart().getLine());
                                                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().getStart().getCharPositionInLine());
                                                                                            System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                        //    System.exit(1);
                                                                                        }
                                                                                    } catch (NullPointerException e) {
                                                                                    }

                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            //////K_WHERE

                                                            /////K_ON//////

                                                            if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().size()>0)
                                                            {
                                                                for(int h =0; h<ctx.factored_select_stmt().select_core().join_clause().join_constraint().size();h++){

                                                                    if(ctx.factored_select_stmt().select_core().join_clause().join_constraint(h).K_ON()!=null)
                                                                    {
                                                                        if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr()!=null)
                                                                        {
                                                                            if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().size()>0)
                                                                            {
                                                                                for(int a = 0 ;a<ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().size();a++){

                                                                                    if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().IDENTIFIER().getText()))){
                                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().getStart().getLine());
                                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().getStart().getCharPositionInLine());
                                                                                        System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                   //     System.exit(1);
                                                                                    }

                                                                                    if (!(ch_join.contains(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().IDENTIFIER().getText()))) {
                                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().getStart().getLine());
                                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().getStart().getCharPositionInLine());
                                                                                        System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                     //   System.exit(1);
                                                                                    }

                                                                                }
                                                                            }
//
                                                                        }
                                                                    }
                                                                }
                                                            }


                                                            ////K_ON///////

                                                            //***********check join***********

                                                            for (int v = 0; v < Main.table_list.get(u).getColumn_defs().size(); v++) {

                                                                if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText().equals(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {


                                                                    //System.out.println("cols : " + ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText());
                                                                    try {
                                                                        for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
                                                                            System.out.println("in fffffffff");
                                                                            Type type1 = new Type();
                                                                            type1.setName(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
//                                                              //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);

                                                                            //if (!unique.add(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()))
                                                                            if (!unique.add(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())){

                                                                                System.out.println("OOOOOOOOOOOOOOOOO " + Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                                System.err.println("ambiguous column : ");
                                                                             //   System.exit(1);
                                                                            }
                                                                            if(Main.type_name_list.contains(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
                                                                            {
                                                                                check_type_join.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                            }
                                                                            else {
                                                                                columns.put(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName(), type1);
                                                                            }

                                                                        }

                                                                        //////////////////////////////////////
                                                                        //flat type
                                                                        for(int y=0;y<check_type_join.size();y++) {
                                                                            if (Main.type_name_list.contains(check_type_join.get(y)))
                                                                            {
                                                                                System.out.println("vvvvvvvvvvvvvv " + check_type_join.get(y));

                                                                                for (int m = 0; m < Main.type_list.size(); m++) {
                                                                                    if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type_join.get(y))) {

                                                                                        // System.out.println("888888888888888888888888877777");
                                                                                        for (int c = 0; c < Main.type_list.get(m).getColumn_def_type().size(); c++) {
                                                                                            Type type2 = new Type();
                                                                                            for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(c).getType_names().size(); b++) {

                                                                                                type2.setName(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());
                                                                                                columns.put(Main.type_list.get(m).getColumn_def_type().get(c).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                                column_add.add(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());

                                                                                            }



                                                                                        }


                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        //end flat type

                                                                        //flat
                                                                        try {
                                                                            if (Main.table_name.contains(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName())) {
                                                                                //System.out.println("8888888888888888888888888");
                                                                                for (int m = 0; m < Main.table_list.size(); m++) {

                                                                                    if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName())) {

                                                                                        // System.out.println("888888888888888888888888877777");
                                                                                        for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                            Type type2 = new Type();
                                                                                            for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                                type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                                columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                                                System.out.print(" column_name : " + Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + " ");

                                                                                                System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                            }

                                                                                        }
                                                                                    }
                                                                                }

                                                                            }
                                                                        }
                                                                        catch (NullPointerException e)
                                                                        {

                                                                        }
                                                                        //end flat

                                                                        /////////////////////////////////////
                                                                    } catch (IndexOutOfBoundsException e) {
                                                                    }


                                                                }


                                                            }


                                                        }

                                                    } else {
                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                        System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                     //   System.exit(1);
                                                    }
                                                }
                                                //

                                                if (!(list_joins.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText()))) {

                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                    System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                 //   System.exit(1);
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                        }

                        if(ctx.factored_select_stmt().select_core().table_or_subquery().size()>0) {
                            ///**************//
                            for(int j=0;j<ctx.factored_select_stmt().select_core().table_or_subquery().size();j++){

                                //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText())){



                                // System.out.println("Test_1 : "+ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText());



                                if(ctx.factored_select_stmt().select_core().result_column().size()>0)
                                {
                                    for(int i=0;i<ctx.factored_select_stmt().select_core().result_column().size();i++) {


                                        //aggregation function
                                        if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func() != null) {

                                            for (int v = 0; v < Main.symbolTable.getDeclaredAggregationFunction().size(); v++) {

                                                agg_list.add(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName());

                                                if (Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())) {

                                                    Type type1 = new Type();
//
                                                    type1.setName(Main.symbolTable.getDeclaredAggregationFunction().get(v).getReturn_type());
                                                    columns.put(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName(), type1);
                                                    System.out.print("column_name : " + Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName() + " , ");
                                                    System.out.println("var_column_type : " + columns.get(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName()).getName());
                                                }


                                            }

                                            if(!(agg_list.contains(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())))
                                            {
                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getLine());
                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getCharPositionInLine());
                                                System.err.println("undeclared function : " + "at line : "+assigneSelect.getLine()+" at col : "+ assigneSelect.getCol());
                                            //    System.exit(1);
                                            }

                                        }




                                        if(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name()!=null) {


                                            if(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name()!=null)
                                            {
                                                System.out.println("111111111111111111111111111122");
                                                if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText())))
                                                {
                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().getStart().getLine());
                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().getStart().getCharPositionInLine());
                                                    System.err.println("undeclared table : "+ " at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                               //     System.exit(1);
                                                }
                                            }

                                            if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getText())) {
                                                //System.out.println("cols : "+ ctx.factored_select_stmt().select_core().result_column().get(i).getText());
                                                for (int u = 0; u < Main.table_list.size(); u++) {
                                                    if (ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText().equals(Main.table_list.get(u).getTb_name().getAny_name().getName())) {


                                                        for(int g =0;g<Main.table_list.get(u).getColumn_defs().size();g++){
                                                            list.add(Main.table_list.get(u).getColumn_defs().get(g).getCol_name().getAny_name().getName());
                                                        }

                                                        //////K_WHERE
                                                        //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                                        //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                                        if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                                        {
                                                            if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                            {
                                                                for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                                    if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                                        if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                                        {
                                                                            for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                            {
                                                                                if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                                {
                                                                                    for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                                    {
                                                                                        try{
                                                                                            if (!(list.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                            //    System.exit(1);
                                                                                            }
                                                                                        }
                                                                                        catch (NullPointerException e){}
                                                                                    }
                                                                                }
                                                                            }
                                                                        }

                                                                    } else {
                                                                        if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                            for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                                try {
                                                                                    //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                                    if (!(list.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                                        System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                   //     System.exit(1);
                                                                                    }
                                                                                } catch (NullPointerException e) {
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        //////K_WHERE

                                                        //list.add(Main.table_list.get(u).getColumn_defs().get(1).getCol_name().getAny_name().getName());

                                                        for (int v = 0; v < Main.table_list.get(u).getColumn_defs().size(); v++) {


                                                            if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText().equals(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {




                                                                //System.out.println("cols : " + ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText());
                                                                for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
                                                                    System.out.println("in fffffffff");
                                                                    Type type1 = new Type();

                                                                    if(Main.type_name_list.contains(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
                                                                    {
                                                                        System.out.println("ccccckkcbcncncyu888888");
                                                                        check_type.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                    }
                                                                    else {
                                                                        type1.setName(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
//                                                                  //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);
//
                                                                        columns.put(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName(), type1);
                                                                        column_add.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                    }
                                                                }


                                                                //flat type
                                                                for(int y=0;y<check_type.size();y++) {
                                                                    if (Main.type_name_list.contains(check_type.get(y)))
                                                                    {
                                                                        System.out.println("vvvvvvvvvvvvvv " + check_type.get(y));

                                                                        for (int m = 0; m < Main.type_list.size(); m++) {
                                                                            if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type.get(y))) {

                                                                                // System.out.println("888888888888888888888888877777");
                                                                                for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                                                                                    Type type2 = new Type();
                                                                                    for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                                                                        type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                        columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                        column_add.add(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());

                                                                                    }



                                                                                }


                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                //end flat type

                                                                //flat
                                                                try {
                                                                    if (Main.table_name.contains(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName())) {

                                                                        System.out.println("8888888888888888888888888 " + columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName());
                                                                        for (int m = 0; m < Main.table_list.size(); m++) {

                                                                            if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName())) {

                                                                                // System.out.println("888888888888888888888888877777");
                                                                                for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                    Type type2 = new Type();
                                                                                    for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                        type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                        columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                                        column_add.add(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());

                                                                                        //s added
                                                                                        System.out.print(" column_name : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                        System.out.println(" var_column_type : "+Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());


                                                                                    }


                                                                                }


                                                                            }


                                                                        }


                                                                    }
                                                                }
                                                                catch (NullPointerException e){}





                                                                //end flat
                                                                //s added
                                                                /*System.out.print("column_name : " + Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + " , ");
                                                                System.out.println("var_column_type : " + columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());
*/


//
                                                                test = true;


                                                            }

                                                            if (!(list.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText())))  {

                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getLine());
                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getCharPositionInLine());
                                                                System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                        //        System.exit(1);

                                                                //System.err.println("ttttest");

                                                            }

                                                        }



                                                    }



                                                }



                                            } else {


                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getLine());
                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getCharPositionInLine());
                                                System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                             //   System.exit(1);
                                            }
                                        }
                                    }
                                }
                                //}
//                                else
//                                {
////
//                                    System.err.println("undefine table");
//                                    System.exit(1);
//                                }
                            }


                        }





                    }

                    type.setName(type_name);
                    type.setColumns(columns);
                    if(symbol.getType()!=null)
                    {
                        if(symbol.getType().getName()!=type.getName())
                        {
                            System.err.println("Error for assigning another type declared variable "+symbol.getName()+"the colume is : "+ assigneSelect.getCol()+"   the line is :  " +assigneSelect.getLine());
                        }
                        else
                        {
                            currentScope.getSymbolMap().get(var_name).setType(type);
                            System.out.println("var type : "+symbol.getType().getName());
                            System.out.println("scop var : "+symbol.getScope().getId());
                            // assigneSelect.setSymbol(symbol);
                        }
                    }
                    else {
                        currentScope.getSymbolMap().get(var_name).setType(type);
                        System.out.println("var type : "+symbol.getType().getName());
                        System.out.println("scop var : "+symbol.getScope().getId());
                        // assigneSelect.setSymbol(symbol);
                    }

                    //   Main.symbolTable.addScope(currentScope);

                }

            }
            else
            {
                Scope currentparent = Main.parent_stack.peek();
                System.out.println("currentscope "+ currentparent.getId());
                boolean found= false;
                Scope parent =currentparent.getParent();
                while(parent!=null){
                    Symbol s = parent.getSymbolMap().get(var_name);
                    if(s!=null){
                        found= true;
                        assigneSelect.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
                        //for join clause
                        if (ctx.factored_select_stmt().select_core().join_clause() != null) {
                            //for table_or_subquery
                            for (int i = 0; i < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); i++) {

                                if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).K_AS() != null) {
                                    type_name = type_name + "_" + ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).table_alias().any_name().getText();
                                } else {
                                    type_name = type_name + "_" + ctx.factored_select_stmt().select_core().join_clause().table_or_subquery(i).table_name().any_name().getText();
                                }
                            }

                            if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null) {
                                System.out.println("Staaaaaar");
                            }
                            if(s.getType()!=null)
                            {
                                if(s.getType().getName()!=type.getName())
                                {
                                    System.err.println("Error for assigning another type declared variable "+s.getName()+" the colume is : "+ assigneSelect.getCol()+"   the line is :  " +assigneSelect.getLine());
                                }
                                else {
                                    type.setName(type_name);
                                    type.setColumns(columns);
                                    s.setType(type);
                                    System.out.println("varSymbol type :  "+s.getType().getName());
                                    System.out.println("scop var : "+s.getScope().getId());
                                }
                            }
                            else {
                                type.setName(type_name);
                                type.setColumns(columns);
                                s.setType(type);

                                System.out.println("varSymbol type :  "+s.getType().getName());
                                System.out.println("scop var : "+s.getScope().getId());

                            }

                        }

                        //for table_or_subquery
                        if (ctx.factored_select_stmt().select_core().table_or_subquery().size() > 0) {


                            System.err.println("third table_or_subquery ");
                            String tb_name_out_subquery = " ";

                            //to get all table name in out select
               /*     for(int m =0;m<ctx.factored_select_stmt().select_core().table_or_subquery().size();m++){

                        // if aliases in out select
                        if (ctx.factored_select_stmt().select_core().table_or_subquery(m).K_AS() != null) {
                            type_name += "_" + ctx.factored_select_stmt().select_core().table_or_subquery(m).table_alias().any_name().getText();
                        }else{
                            type_name += "_" + ctx.factored_select_stmt().select_core().table_or_subquery(m).table_name().any_name().getText();
                        }
                    }


                    System.err.println("sooooooooooooooooooooooooooooooooooooooooooo => "+type_name);
*/

                            //= ctx.factored_select_stmt().select_core().table_or_subquery(0).table_name().any_name().getText();


                            //subquery
                            if(ctx.factored_select_stmt().select_core().K_WHERE() != null ){



                                for(int i =0;i<ctx.factored_select_stmt().select_core().expr().size();i++){


                                    for(int j =0;j<ctx.factored_select_stmt().select_core().expr(i).expr().size();j++) {

                                        try {
                                            if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt() != null) {


                                                //check if column in subquery is one
                                                if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().size() == 1) {


                                                    if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size() > 0) {


                                                        for (int e = 0; e < ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size(); e++) {
                                                            //System.out.println("table name : " + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery(e).table_name().any_name().getText());
                                                            all_table_in_subquery.add(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery(e).table_name().any_name().getText());
                                                        }

                                                        System.out.println("all_table_in_subquery  => " + all_table_in_subquery);
                                                        System.out.println("---------------------------------- ");

                                                    }


                                                    ////////////here
                                                    for (int g = 0; g < Main.table_list.size(); g++) {

                                                        if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().table_or_subquery().size() > 0) {


                                                            //to get table with cols with type of each col
                                                            for (int n = 0; n < all_table_in_subquery.size(); n++) {
                                                                if (Main.table_list.get(g).getTb_name().getAny_name().getName().equals(all_table_in_subquery.get(n))) {

                                                                    System.out.println("table name :" + Main.table_list.get(g).getTb_name().getAny_name().getName());
////77777777777777777777
                                                                    for (int cc = 0; cc < Main.table_list.get(g).getColumn_defs().size(); cc++) {


                                                                        System.out.println("Column => " + Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName());
                                                                        all_cols_name_in_subquery_table.add(Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName());


                                                                        //to get type the column
                                                                        Type type2 = new Type();
                                                                        for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(cc).getType_names().size(); l++) {
                                                                            type2.setName(Main.table_list.get(g).getColumn_defs().get(cc).getType_names().get(l).getNames().getName().getName());

                                                                            //to store name and type of col
                                                                            columns.put(Main.table_list.get(g).getColumn_defs().get(cc).getCol_name().getAny_name().getName(), type2);
                                                                            System.out.println("Type : " + type2.getName());
                                                                            //  System.out.println("coooooooooooooooooooool : " +  columns);
                                                                        }

                                                                    }
                                                                }


                                                            }


                                                        }


                                                    }

                                                    //System.err.println("all_cols_name in sub : " +all_cols_name_in_subquery_table);
                                                    //System.err.println("this col => " + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());


                                                    //check if col in subquery is found in tables

                                                    //check of column in subquery is ( * )
                                                    if (ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).STAR() != null) {
                                                        System.err.println("columns in subquery can't be start ( * )");
                                                    } else if (all_cols_name_in_subquery_table.contains(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText())) {

                                                        for (int a = 0; a < columns.size(); a++) {


                                                            //  System.err.println("type column IDENTEFIR in subquery : " + columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName());
                                                            //  System.err.println("yyyyyyyyyyyyyyyyyyyyyyyyyyiiiiiiiii : " + ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText());


                                                            for (int g = 0; g < Main.table_list.size(); g++) {
                                                                for (int k = 0; k < Main.table_list.get(g).getColumn_defs().size(); k++) {


                                                                    //to get type the column
                                                   /*     Type type2= new Type();
                                                        for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(k).getType_names().size(); l++) {
                                                            type2.setName(Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                            //to store name and type of col
                                                            columns.put( Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type2);
                                                            //  System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyiiiiiiiii type : " +  type2.getName());
                                                        }*/

                                                                }

                                                            }


                                                            for (int g = 0; g < Main.table_list.size(); g++) {
                                                                for (int k = 0; k < Main.table_list.get(g).getColumn_defs().size(); k++) {


                                                                    //to get type the column
                                                                    Type type2 = new Type();
                                                                    for (int l = 0; l < Main.table_list.get(g).getColumn_defs().get(k).getType_names().size(); l++) {
                                                                        type2.setName(Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());
                                                                        //System.err.println("tttttttttttttttttttttttt :=> "+ Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " => " + type2.getName());
                                                                        //to store name and type of col
                                                                        columns2.put(Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type2);
                                                                        // System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb : " + Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                                                        //  System.err.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy :=> "+ columns);


                                                                        if (Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())) {

                                                                            // System.err.println("howwwwwwwwwwwwwwwwwwwwwwwwww ????");
                                                                            //   System.err.println("columnsssssssssss //////"+columns.get("id").getName());
                                                                            //    System.err.println("type of col : "+ Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                            //  System.out.println("type column IDENTEFIR in subquery kookokokokokoooooooooooooooooooooooo: " + columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName());


                                                                            //System.err.print("id :  "+ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());
                                                                            if (Main.table_list.get(g).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName().equals(columns.get(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText()).getName())) {

                                                                                // System.err.println("alllllllllllllllllllllllllllllllllllll is trueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee ");
                                                                            } else {
                                                                                System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText() + " and " + Main.table_list.get(g).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " must be the same type ... ");

                                                                            }


                                                                        }


                                                                    }
                                                                }
                                                            }


                                                        }

                                                    } else {
                                                        System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText() + " not found in tables subquery .... ");
                                                    }

                                                    /////end here



                                  /*
                                   all_cols_name.add(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                   if (Main.column_name.contains(ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText())) {
                                        System.err.println("columns in subquery found in table subquery" + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText());
                                    }else{
                                        System.err.println("columns in subquery noooooooooooooooooot found in table subquery" + ctx.factored_select_stmt().select_core().expr(i).expr(1).factored_select_stmt().select_core().result_column().get(0).expr().column_name().getText());
                                    }*/


                                                } else {
                                                    System.err.println("columns in subquery must be one ...");
                                                }


                                            }
                                        }
                                        catch (NullPointerException e){}
//                                        else{
//                                            System.err.println("subquery must be select");
//                                        }

                                        // break;

                                    }

                                    //  System.err.println("lll" + ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText() );

                                    // System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).factored_select_stmt().select_core());




                                    //Where In (expr IN SelectExpr )
                                    if(ctx.factored_select_stmt().select_core().expr(i).K_IN() != null){



                                        if(ctx.factored_select_stmt().select_core().expr(i).expr(i) != null){ //check IDENTIFIER is write


                                                                        /* System.err.println("subquery");
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().IDENTIFIER());
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());
                                    */





                                                                           /* for(int c=0;c<ctx.factored_select_stmt().select_core().table_or_subquery().size();c++){
                                                                                System.err.println("koko:"+ctx.factored_select_stmt().select_core().table_or_subquery(c).table_name().any_name().getText());

                                                                            }*/




                                            ///check IDENTIFIER is found in cols
                                            for (int j = 0; j < Main.table_list.size(); j++) {

                                                //to get all cols in all table (select * from test1,test2)
                                                for (int p = 0; p < ctx.factored_select_stmt().select_core().table_or_subquery().size(); p++) {

                                                    //check if table (in out select) is found in table_list
                                                    if (ctx.factored_select_stmt().select_core().table_or_subquery(p).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {

                                                        //table flat


                                                        for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                                            // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                                            for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {

                                                                Type type1 = new Type();




                                                                //flat (table)
                                                                if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                    System.err.println("================  //flat (table) ======================== ");

                                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                    // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                                    System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                                    //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                    for (int m = 0; m < Main.table_list.size(); m++) {
                                                                        if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {

                                                                            for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                Type type2 = new Type();
                                                                                for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                    type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                    columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName(), type2);
                                                                                    //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                    System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                }

                                                                            }

                                                                        }
                                                                    }

                                                                    //
                                                                }   //end flat


                                                                //flat (type)
                                                                else if (Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                    System.err.println("================  //flat (type) ======================== ");

                                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                    // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                                    System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                                    //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                    for (int m = 0; m < Main.type_name_list.size(); m++) {
                                                                        if (Main.type_name_list.get(m).equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {

                                                                            System.err.println("================ kk ======================== ");

                                                                            for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                Type type2 = new Type();


                                                                                for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                    System.err.println("================ hhhhhhhhhhhhhhhhhhh ======================== " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().size());

                                                                                    //    if((Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName().equals())

                                                                                    type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                    columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName(), type2);
                                                                                    //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                    System.out.println(" var_column_type : " + Main.table_list.get(m).getTb_name().getAny_name().getName());
                                                                                }

                                                                            }
                                                                        }
                                                                    }

                                                                }//end flat type


                                                                else {    //normal column
                                                                    //   System.err.println("col=>");

                                                                    type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                    columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                    // System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
                                                                    // System.out.println("var_column_type : " + Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());

                                                                    all_cols_name.add(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());




                                                                                               /*     for(int y=0;y<columns.size();y++){
                                                                                                        if(columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).equals(ctx
                                                                                                        .factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())){
                                                                                                                    System.err.println("gogogogogogoooooooooooooooggggggggggg"+columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()));
                                                                                                        }else{

                                                                                                            System.err.println("noooooooooooooooooooooooooooo"+columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()));
                                                                                                        }



                                                                                                    }*/




                                                                }    //end of normal column


                                                            }


                                                            //}
                                                        }


                                                    }


                                                }


                                            }






                                            ////////*********************************************

                                                                           /* System.err.println("all_cols_name in outer select : " +all_cols_name);
                                                                            System.err.println(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText());
                                    */

                                            if(all_cols_name.contains(ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText())){
                                                //  System.err.println(" column  found " );

                                            }else{
                                                System.err.println( ctx.factored_select_stmt().select_core().expr(i).expr(i).column_name().any_name().getText() + " column not found " );
                                            }





                                        }




                                        //  System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());


                                    }else{// where stmt without IN
                                        //  System.err.println(ctx.factored_select_stmt().select_core().expr(i).K_IN());
                                        System.err.println("check where stmt");
                                    }


                                }






                            }
                            else{
                                if (ctx.factored_select_stmt().select_core().table_or_subquery(0).K_AS() != null) {
                                    type_name = ctx.factored_select_stmt().select_core().table_or_subquery(0).table_alias().any_name().getText();

                                } else {
                                    type_name = ctx.factored_select_stmt().select_core().table_or_subquery(0).table_name().any_name().getText();
                                }
                            }






                            //}

                /* else {
                    for (int i = 0; i < ctx.factored_select_stmt().select_core().table_or_subquery().size(); i++) {

                        if (ctx.factored_select_stmt().select_core().table_or_subquery(i).K_AS() != null) {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().table_or_subquery(i).table_alias().any_name().getText();
                        } else {
                            type_name = type_name + "_" + ctx.factored_select_stmt().select_core().table_or_subquery(i).table_name().any_name().getText();
                        }
                    }
                }*/
                            if(s.getType()!=null)
                            {
                                if(s.getType().getName()!=type.getName())
                                {
                                    System.err.println("Error for assigning another type declared variable "+s.getName()+" the colume is : "+ assigneSelect.getCol()+"   the line is :  " +assigneSelect.getLine());
                                }
                                else {
                                    type.setName(type_name);
                                    type.setColumns(columns);
                                    s.setType(type);
                                    System.out.println("varSymbol type :  "+s.getType().getName());
                                    System.out.println("scop var : "+s.getScope().getId());
                                }
                            }
                            else {
                                type.setName(type_name);
                                type.setColumns(columns);
                                s.setType(type);

                                System.out.println("varSymbol type :  "+s.getType().getName());
                                System.out.println("scop var : "+s.getScope().getId());

                            }

                        }

                        //for result_column
                        if (ctx.factored_select_stmt().select_core().result_column().size() > 0) {

                            System.out.println("++++++++++++++++++++++++++++0");
                            // if (ctx.factored_select_stmt().select_core().result_column(0).!=null){}   agg fumc


                            if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null&&ctx.factored_select_stmt().select_core().join_clause()==null) {
                                System.out.println("++++++++++++++++++++++++++++0");


                                  List<String> checks=new ArrayList<>();

                                for (int i = 0; i < ctx.factored_select_stmt().select_core().result_column().size(); i++) {

                                    for (int j = 0; j < Main.table_list.size(); j++) {


                                        //to get all cols in all table (select * from test1,test2)
                                        for (int p = 0; p < ctx.factored_select_stmt().select_core().table_or_subquery().size(); p++) {

                                            //check if table (in out select) is found in table_list


                                            if (ctx.factored_select_stmt().select_core().table_or_subquery(p).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {
                                                System.out.println("-----------------------------");
                                                for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                                    //////K_WHERE
                                                    for (int r = 0; r < Main.table_list.get(j).getColumn_defs().size(); r++) {
                                                        checks.add( Main.table_list.get(j).getColumn_defs().get(r).getCol_name().getAny_name().getName());
                                                        //System.out.println("nmnmnmnmnmnm "+Main.table_list.get(j).getColumn_defs().get(r).getCol_name().getAny_name().getName());
                                                    }
                                                    //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                                    //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                                    if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                                    {
                                                        if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                        {
                                                            for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                                if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                                    if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                                    {
                                                                        for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                        {
                                                                            if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                            {
                                                                                for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                                {
                                                                                    try{
                                                                                        if (!(checks.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                            System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                            System.exit(1);
                                                                                        }
                                                                                    }
                                                                                    catch (NullPointerException e){}
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                } else {
                                                                    if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                        for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                            try {
                                                                                //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                                if (!(checks.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                                    System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                    System.exit(1);
                                                                                }
                                                                            } catch (NullPointerException e) {
                                                                            }

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    //////K_WHERE

                                                    // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                                    for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {

                                                        Type type1 = new Type();

                                                        //****** type flat  ******
                                                        //    System.out.println("kkkkkkkkkkkkkkkkkkkkkkkkl " + Main.type_name_list.get(0));

                                                        if (Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                            System.out.println("tyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyp create");
                                                            for (int m = 0; m < Main.type_list.size(); m++) {
                                                                if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                    for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                                                                        Type type2 = new Type();
                                                                        for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                                                            type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                                                            columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                            //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                            // System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                        }

                                                                    }
                                                                }
                                                            }

                                                        }

                                                        //****** end of type flat *****

                                                        //table flat
                                                        else if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                            type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                            columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                            //System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                            //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                            //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                            for (int m = 0; m < Main.table_list.size(); m++) {
                                                                if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                    for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                        Type type2 = new Type();
                                                                        for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                            type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                            columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                            //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                            System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                        }

                                                                    }
                                                                }
                                                            }

//
                                                        }   //end table flat
                                                        else {
                                                            //normal column
                                                            //  System.out.println("pppppppppppppppppppppppppppppppp");
                                                            type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                            columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                            // System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                            System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
                                                        }    //end of normal column


                                                    }
                                                    //}
                                                }
                                            }


                                        }

                                    }

                                }


                            }



                            else {

                                //System.out.println("-------------------");

                                //for join  join_claus
                                if (ctx.factored_select_stmt().select_core().join_clause() != null){
                                    if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size() > 0) {


                                        if (ctx.factored_select_stmt().select_core().result_column().size() > 0) {
                                            for (int i = 0; i < ctx.factored_select_stmt().select_core().result_column().size(); i++) {


                                                //System.out.println(ctx.factored_select_stmt().select_core().result_column(0).expr().column_name().any_name().getText());
                                                //*********star***********
                                                if (ctx.factored_select_stmt().select_core().result_column(0).STAR() != null) {
                                                    System.out.println("++++++++++++++++++++++++++++0");

                                                    //List<String> ch_join = new ArrayList<>();

                                                    for (int g = 0; g < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); g++) {

                                                        for (int j = 0; j < Main.table_list.size(); j++) {

                                                            if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(g).table_name().any_name().getText().equals(Main.table_list.get(j).getTb_name().getAny_name().getName())) {

                                                                for (int k = 0; k < Main.table_list.get(j).getColumn_defs().size(); k++) {

                                                                    ///////
                                                                    check_where_on(ctx,assigneSelect);
                                                                    ///////

                                                                    // if (Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText())) {
                                                                    for (int l = 0; l < Main.table_list.get(j).getColumn_defs().get(k).getType_names().size(); l++) {

                                                                        Type type1 = new Type();

                                                                        if(Main.type_name_list.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()))
                                                                        {
                                                                            System.out.println("tyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyp create");
                                                                            for (int m = 0; m < Main.type_list.size(); m++) {
                                                                                if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                                    for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                                                                                        Type type2 = new Type();
                                                                                        for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                                                                            type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                            columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                            //System.out.println(" cols : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                            // System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                        }

                                                                                    }
                                                                                }
                                                                            }

                                                                        }

                                                                        //flat
                                                                        else if (Main.table_name.contains(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                            type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName()); //table name type
                                                                            columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName(), type1);
                                                                            //System.out.print("var_column_name : "+ Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+" , ");
                                                                            //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());

                                                                            //System.out.println("_____----"+Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                            for (int m = 0; m < Main.table_list.size(); m++) {
                                                                                if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName())) {
                                                                                    for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                        Type type2 = new Type();
                                                                                        for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                            type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                            columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                                            System.out.println(" cols : " + Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + " ");

                                                                                            System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                        }

                                                                                    }
                                                                                }
                                                                            }

//
                                                                        }   //end flat
                                                                        else {
                                                                            //normal column
                                                                            System.out.println("pppppppppppppppppppppppppppppppp");
                                                                            type1.setName(Main.table_list.get(j).getColumn_defs().get(k).getType_names().get(l).getNames().getName().getName());

                                                                            columns.put(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()+"_"+Main.table_list.get(j).getTb_name().getAny_name().getName(), type1);
                                                                            //System.out.print("var_column_name : " + Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName() + " , ");
                                                                            //System.out.println("var_column_type : " + columns.get(Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName()).getName());
                                                                        }    //end of normal column
                                                                    }
                                                                    //}
                                                                }
                                                            }

                                                        }

                                                    }

                                                }

                                                //***********end star*******
                                                //aggregation function
                                                if (ctx.factored_select_stmt().select_core().result_column().get(i).expr() != null) {
                                                    if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func() != null) {

                                                        for (int v = 0; v < Main.symbolTable.getDeclaredAggregationFunction().size(); v++) {

                                                            agg_list.add(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName());

                                                            if (Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())) {

                                                                Type type1 = new Type();
//
                                                                type1.setName(Main.symbolTable.getDeclaredAggregationFunction().get(v).getReturn_type());
                                                                columns.put(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName(), type1);
                                                                System.out.print("column_name : " + Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName() + " , ");
                                                                System.out.println("var_column_type : " + columns.get(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName()).getName());
                                                            }

                                                        }

                                                        if (!(agg_list.contains(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText()))) {
                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getLine());
                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getCharPositionInLine());
                                                            System.err.println("undeclared function : " + "at line : "+assigneSelect.getLine()+" at col : "+ assigneSelect.getCol());
                                                            System.exit(1);
                                                        }

                                                    }



                                                    //check column when using join
                                                    if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name() != null) {

                                                        if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name() != null) {
                                                           // List <String> ch_join = new ArrayList<>();
                                                            if (!(Main.table_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText()))) {
                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().getStart().getLine());
                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().getStart().getCharPositionInLine());
                                                                System.err.println("undeclared table : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                          //      System.exit(1);
                                                            }

                                                            for (int n = 0; n < Main.table_list.size(); n++) {
                                                                if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getTb_name().getAny_name().getName())) {
                                                                    System.out.println("kkkkkkkkkkkkkkkkkkkk" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                                    if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText())) {


                                                                        for (int v = 0; v < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); v++) {

                                                                            //if(Main.column_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())){
                                                                            //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())) {
                                                                            if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getTb_name().getAny_name().getName())) {

                                                                                for (int c = 0; c < Main.table_list.get(n).getColumn_defs().size(); c++) {
                                                                                    list_joins.add(Main.table_list.get(n).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                                    System.out.println("xxxx " + Main.table_list.get(n).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                                }

                                                                            }
                                                                            //}
                                                                        }


                                                                        ///////
                                                                        check_where_on (ctx,assigneSelect);
                                                                        ///////

                                                                        //******check join*****

                                                                        for (int v = 0; v < Main.table_list.get(n).getColumn_defs().size(); v++) {

                                                                            if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {

                                                                                System.out.println("cols : " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                                //System.out.println("dddddddddddddddddddddd "+Main.table_list.get(n).getColumn_defs().size());
                                                                                try {
                                                                                    for (int l = 0; l < Main.table_list.get(n).getColumn_defs().get(v).getType_names().size(); l++) {
                                                                                        System.out.println("in fffffffff");
                                                                                        Type type1 = new Type();
                                                                                        type1.setName(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                        //columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                        //System.out.println("tyyyyyyyyyyype " + Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                        //System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());

//                                                              //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);
                                                                                        String col_name = " ";

                                                                                        if(Main.type_name_list.contains(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
                                                                                        {
                                                                                            check_type_join.add(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                        }
                                                                                        if (!unique.add(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {
                                                                                            System.out.println("+++++mnmnmn++++" + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                                            columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                            System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                                                            //System.out.println("typppppppppe name " + columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());

                                                                                        } else {
                                                                                            if (!(Main.type_name_list.contains(Main.table_list.get(n).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))){
                                                                                                columns.put(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName(), type1);
                                                                                            }
                                                                                            //System.out.println("cooooooool name " + Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()+ "_" + Main.table_list.get(n).getTb_name().getAny_name().getName());
                                                                                            //System.out.println("typppppppppe name " + columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());
                                                                                        }


                                                                                    }
                                                                                    /////////////////////////
                                                                                    //////////////////////////////////////
                                                                                    //flat type
                                                                                    for(int y=0;y<check_type_join.size();y++) {
                                                                                        if (Main.type_name_list.contains(check_type_join.get(y)))
                                                                                        {
                                                                                            System.out.println("vvvvvvvvvvvvvv " + check_type_join.get(y));

                                                                                            for (int m = 0; m < Main.type_list.size(); m++) {
                                                                                                if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type_join.get(y))) {

                                                                                                    // System.out.println("888888888888888888888888877777");
                                                                                                    for (int c = 0; c < Main.type_list.get(m).getColumn_def_type().size(); c++) {
                                                                                                        Type type2 = new Type();
                                                                                                        for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(c).getType_names().size(); b++) {

                                                                                                            type2.setName(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());
                                                                                                            columns.put(Main.type_list.get(m).getColumn_def_type().get(c).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                                            column_add.add(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());

                                                                                                        }



                                                                                                    }


                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    //end flat type
                                                                                    //flat
                                                                                    try {
                                                                                        if (Main.table_name.contains(columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName()).getName())) {
                                                                                            System.out.println("8888888888888888888888888");
                                                                                            for (int m = 0; m < Main.table_list.size(); m++) {

                                                                                                if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(n).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(n).getTb_name().getAny_name().getName()).getName())) {

                                                                                                    System.out.println("888888888888888888888888877777");
                                                                                                    for (int l = 0; l < Main.table_list.get(m).getColumn_defs().size(); l++) {
                                                                                                        Type type2 = new Type();
                                                                                                        for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(l).getType_names().size(); b++) {

                                                                                                            type2.setName(Main.table_list.get(m).getColumn_defs().get(l).getType_names().get(b).getNames().getName().getName());
                                                                                                            columns.put(Main.table_list.get(m).getColumn_defs().get(l).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName() + "_" + "flat", type2);
                                                                                                            System.out.print(" column_name : " + Main.table_list.get(m).getColumn_defs().get(l).getCol_name().getAny_name().getName() + " ");

                                                                                                            System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(l).getType_names().get(b).getNames().getName().getName());
                                                                                                        }

                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                        }
                                                                                    }
                                                                                    catch (NullPointerException e){}
                                                                                    //end flat
                                                                                    /////////////////////////////////////


                                                                                    ///////////////////////
                                                                                } catch (IndexOutOfBoundsException e) {

                                                                                }


                                                                            }


                                                                        }


                                                                    } else {
                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                                        System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                                       // System.exit(1);
                                                                    }
                                                                }
                                                            }

                                                        } else {
                                                            List<String> ch_join = new ArrayList<>();
                                                            //System.out.println(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText());
                                                            if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().IDENTIFIER().getText())) {


                                                                for (int u = 0; u < Main.table_list.size(); u++) {

                                                                    for (int v = 0; v < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); v++) {

                                                                        //if(Main.column_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())){
                                                                        //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText())) {
                                                                        if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(v).table_name().any_name().IDENTIFIER().getText().equals(Main.table_list.get(u).getTb_name().getAny_name().getName())) {

                                                                            for (int c = 0; c < Main.table_list.get(u).getColumn_defs().size(); c++) {
                                                                                list_joins.add(Main.table_list.get(u).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                                System.out.println("xxxx " + Main.table_list.get(u).getColumn_defs().get(c).getCol_name().getAny_name().getName());
                                                                            }

                                                                        }
                                                                        //}
                                                                    }

                                                                    //////K_WHERE

                                                                    for(int b=0 ; b<Main.table_list.size();b++) {
                                                                        for (int a = 0; a < Main.table_list.get(b).getColumn_defs().size(); a++) {
                                                                            for (int c = 0; c < ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().size(); c++) {
                                                                                if (ctx.factored_select_stmt().select_core().join_clause().table_or_subquery().get(c).table_name().any_name().IDENTIFIER().getText()
                                                                                        .equals(Main.table_list.get(b).getTb_name().getAny_name().getName())) {
                                                                                    ch_join.add(Main.table_list.get(b).getColumn_defs().get(a).getCol_name().getAny_name().getName());
                                                                                    System.out.println("nnnnnnnnn : " + Main.table_list.get(b).getColumn_defs().get(a).getCol_name().getAny_name().getName());
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                                                    //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                                                    if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                                                    {
                                                                        if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                                        {
                                                                            for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                                                if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                                                    if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                                                    {
                                                                                        for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                                        {
                                                                                            if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                                            {
                                                                                                for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                                                {
                                                                                                    try{
                                                                                                        if (!(ch_join.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                            System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                          //  System.exit(1);
                                                                                                        }
                                                                                                        if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().IDENTIFIER().getText()))){
                                                                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().getStart().getLine());
                                                                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).table_name().any_name().getStart().getCharPositionInLine());
                                                                                                            System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                         //   System.exit(1);
                                                                                                        }
                                                                                                    }
                                                                                                    catch (NullPointerException e){}
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                } else {
                                                                                    if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                                        for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                                            try {
                                                                                                //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                                                if (!(ch_join.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                    System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                   // System.exit(1);
                                                                                                }
                                                                                                if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().IDENTIFIER().getText()))){
                                                                                                    assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().getStart().getLine());
                                                                                                    assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).table_name().any_name().getStart().getCharPositionInLine());
                                                                                                    System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                 //   System.exit(1);
                                                                                                }
                                                                                            } catch (NullPointerException e) {
                                                                                            }

                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    //////K_WHERE

                                                                    /////K_ON//////

                                                                    if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().size()>0)
                                                                    {
                                                                        for(int h =0; h<ctx.factored_select_stmt().select_core().join_clause().join_constraint().size();h++){

                                                                            if(ctx.factored_select_stmt().select_core().join_clause().join_constraint(h).K_ON()!=null)
                                                                            {
                                                                                if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr()!=null)
                                                                                {
                                                                                    if(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().size()>0)
                                                                                    {
                                                                                        for(int a = 0 ;a<ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().size();a++){

                                                                                            if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().IDENTIFIER().getText()))){
                                                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().getStart().getLine());
                                                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).table_name().any_name().getStart().getCharPositionInLine());
                                                                                                System.err.println("undefine table in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                             //   System.exit(1);
                                                                                            }

                                                                                            if (!(ch_join.contains(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().IDENTIFIER().getText()))) {
                                                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().getStart().getLine());
                                                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().join_clause().join_constraint().get(h).expr().expr().get(a).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                             //   System.exit(1);
                                                                                            }

                                                                                        }
                                                                                    }
//
                                                                                }
                                                                            }
                                                                        }
                                                                    }


                                                                    ////K_ON///////

                                                                    //***********check join***********

                                                                    for (int v = 0; v < Main.table_list.get(u).getColumn_defs().size(); v++) {

                                                                        if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText().equals(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {


                                                                            //System.out.println("cols : " + ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText());
                                                                            try {
                                                                                for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
                                                                                    System.out.println("in fffffffff");
                                                                                    Type type1 = new Type();
                                                                                    type1.setName(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
//                                                              //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);

                                                                                    //if (!unique.add(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()))
                                                                                    if (!unique.add(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())){

                                                                                        System.out.println("OOOOOOOOOOOOOOOOO " + Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName());
                                                                                        System.err.println("ambiguous column : ");
                                                                                     //   System.exit(1);
                                                                                    }
                                                                                    if(Main.type_name_list.contains(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
                                                                                    {
                                                                                        check_type_join.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                                    }
                                                                                    else {
                                                                                        columns.put(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName(), type1);
                                                                                    }

                                                                                }

                                                                                //////////////////////////////////////
                                                                                //flat type
                                                                                for(int y=0;y<check_type_join.size();y++) {
                                                                                    if (Main.type_name_list.contains(check_type_join.get(y)))
                                                                                    {
                                                                                        System.out.println("vvvvvvvvvvvvvv " + check_type_join.get(y));

                                                                                        for (int m = 0; m < Main.type_list.size(); m++) {
                                                                                            if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type_join.get(y))) {

                                                                                                // System.out.println("888888888888888888888888877777");
                                                                                                for (int c = 0; c < Main.type_list.get(m).getColumn_def_type().size(); c++) {
                                                                                                    Type type2 = new Type();
                                                                                                    for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(c).getType_names().size(); b++) {

                                                                                                        type2.setName(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());
                                                                                                        columns.put(Main.type_list.get(m).getColumn_def_type().get(c).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                                        column_add.add(Main.type_list.get(m).getColumn_def_type().get(c).getType_names().get(b).getNames().getName().getName());

                                                                                                    }



                                                                                                }


                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                                //end flat type

                                                                                //flat
                                                                                try {
                                                                                    if (Main.table_name.contains(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName())) {
                                                                                        //System.out.println("8888888888888888888888888");
                                                                                        for (int m = 0; m < Main.table_list.size(); m++) {

                                                                                            if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName())) {

                                                                                                // System.out.println("888888888888888888888888877777");
                                                                                                for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                                    Type type2 = new Type();
                                                                                                    for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                                        type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                                        columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                                                        System.out.print(" column_name : " + Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + " ");

                                                                                                        System.out.println(" var_column_type : " + Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                                    }

                                                                                                }
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                }
                                                                                catch (NullPointerException e)
                                                                                {

                                                                                }
                                                                                //end flat

                                                                                /////////////////////////////////////
                                                                            } catch (IndexOutOfBoundsException e) {
                                                                            }


                                                                        }


                                                                    }


                                                                }

                                                            } else {
                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                                System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                           //     System.exit(1);
                                                            }
                                                        }
                                                        //

                                                        if (!(list_joins.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText()))) {

                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getLine());
                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getStart().getCharPositionInLine());
                                                            System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                         //   System.exit(1);
                                                        }
                                                    }
                                                }
                                            }

                                        }

                                    }
                                }

                                if(ctx.factored_select_stmt().select_core().table_or_subquery().size()>0) {
                                    ///**************//
                                    for(int j=0;j<ctx.factored_select_stmt().select_core().table_or_subquery().size();j++){

                                        //if(Main.table_name.contains(ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText())){



                                        // System.out.println("Test_1 : "+ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText());



                                        if(ctx.factored_select_stmt().select_core().result_column().size()>0)
                                        {
                                            for(int i=0;i<ctx.factored_select_stmt().select_core().result_column().size();i++) {


                                                //aggregation function
                                                if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func() != null) {

                                                    for (int v = 0; v < Main.symbolTable.getDeclaredAggregationFunction().size(); v++) {

                                                        agg_list.add(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName());

                                                        if (Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName().equals(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())) {

                                                            Type type1 = new Type();
//
                                                            type1.setName(Main.symbolTable.getDeclaredAggregationFunction().get(v).getReturn_type());
                                                            columns.put(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName(), type1);
                                                            System.out.print("column_name : " + Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName() + " , ");
                                                            System.out.println("var_column_type : " + columns.get(Main.symbolTable.getDeclaredAggregationFunction().get(v).getFunction_name().getAny_name().getName()).getName());
                                                        }


                                                    }

                                                    if(!(agg_list.contains(ctx.factored_select_stmt().select_core().result_column(i).expr().call_func().fun_name().IDENTIFIER().getText())))
                                                    {
                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getLine());
                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().call_func().getStart().getCharPositionInLine());
                                                        System.err.println("undeclared function : " + "at line : "+assigneSelect.getLine()+" at col : "+ assigneSelect.getCol());
                                                   //     System.exit(1);
                                                    }

                                                }




                                                if(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name()!=null) {


                                                    if(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name()!=null)
                                                    {
                                                        System.out.println("111111111111111111111111111122");
                                                        if(!(Main.table_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().IDENTIFIER().getText())))
                                                        {
                                                            assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().getStart().getLine());
                                                            assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().table_name().any_name().getStart().getCharPositionInLine());
                                                            System.err.println("undeclared table : "+ " at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                         //   System.exit(1);
                                                        }
                                                    }

                                                    if (Main.column_name.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().getText())) {
                                                        //System.out.println("cols : "+ ctx.factored_select_stmt().select_core().result_column().get(i).getText());
                                                        for (int u = 0; u < Main.table_list.size(); u++) {
                                                            if (ctx.factored_select_stmt().select_core().table_or_subquery().get(j).table_name().any_name().getText().equals(Main.table_list.get(u).getTb_name().getAny_name().getName())) {


                                                                for(int g =0;g<Main.table_list.get(u).getColumn_defs().size();g++){
                                                                    list.add(Main.table_list.get(u).getColumn_defs().get(g).getCol_name().getAny_name().getName());
                                                                }

                                                                //////K_WHERE
                                                                //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getTb_name().getAny_name().getName());
                                                                //System.out.println("nnnnnnnnnnnnnnnnnnnnnnnn : "+Main.table_list.get(j).getColumn_defs().get(k).getCol_name().getAny_name().getName());
                                                                if(ctx.factored_select_stmt().select_core().K_WHERE()!=null)
                                                                {
                                                                    if(ctx.factored_select_stmt().select_core().expr().size()>0)
                                                                    {
                                                                        for(int a = 0;a<ctx.factored_select_stmt().select_core().expr().size();a++) {
                                                                            if (ctx.factored_select_stmt().select_core().expr(a).OPEN_PAR() != null) {

                                                                                if(ctx.factored_select_stmt().select_core().expr(a).expr().size()>0)
                                                                                {
                                                                                    for(int c=0;c<ctx.factored_select_stmt().select_core().expr(a).expr().size();c++)
                                                                                    {
                                                                                        if(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size()>0)
                                                                                        {
                                                                                            for(int d=0;d<ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().size();d++)
                                                                                            {
                                                                                                try{
                                                                                                    if (!(list.contains(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getLine());
                                                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr(a).expr().get(c).expr().get(d).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                        System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                                    //    System.exit(1);
                                                                                                    }
                                                                                                }
                                                                                                catch (NullPointerException e){

                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                            } else {
                                                                                if (ctx.factored_select_stmt().select_core().expr().get(a).expr().size() > 0) {
                                                                                    for (int b = 0; b < ctx.factored_select_stmt().select_core().expr().get(a).expr().size(); b++) {
                                                                                        try {
                                                                                            //System.out.println("mbbbbbbbbbbbbb : " + ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText());

                                                                                            if (!(list.contains(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().IDENTIFIER().getText()))) {

                                                                                                assigneSelect.setLine(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getLine());
                                                                                                assigneSelect.setCol(ctx.factored_select_stmt().select_core().expr().get(a).expr().get(b).column_name().any_name().getStart().getCharPositionInLine());
                                                                                                System.err.println("undefine column in where : " + " at line : " + assigneSelect.getLine() + " at col : " + assigneSelect.getCol());
                                                                                             //   System.exit(1);
                                                                                            }
                                                                                        } catch (NullPointerException e) {
                                                                                        }

                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                //////K_WHERE

                                                                //list.add(Main.table_list.get(u).getColumn_defs().get(1).getCol_name().getAny_name().getName());

                                                                for (int v = 0; v < Main.table_list.get(u).getColumn_defs().size(); v++) {


                                                                    if (ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText().equals(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName())) {




                                                                        //System.out.println("cols : " + ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText());
                                                                        for (int l = 0; l < Main.table_list.get(u).getColumn_defs().get(i).getType_names().size(); l++) {
                                                                            System.out.println("in fffffffff");
                                                                            Type type1 = new Type();

                                                                            if(Main.type_name_list.contains(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName()))
                                                                            {
                                                                                System.out.println("ccccckkcbcncncyu888888");
                                                                                check_type.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                            }
                                                                            else {
                                                                                type1.setName(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
//                                                                  //columns.put(ctx.factored_select_stmt().select_core().result_column(i).expr().column_name().any_name().getText(), type1);
//
                                                                                columns.put(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName(), type1);
                                                                                column_add.add(Main.table_list.get(u).getColumn_defs().get(v).getType_names().get(l).getNames().getName().getName());
                                                                            }
                                                                        }


                                                                        //flat type
                                                                        for(int y=0;y<check_type.size();y++) {
                                                                            if (Main.type_name_list.contains(check_type.get(y)))
                                                                            {
                                                                                System.out.println("vvvvvvvvvvvvvv " + check_type.get(y));

                                                                                for (int m = 0; m < Main.type_list.size(); m++) {
                                                                                    if (Main.type_list.get(m).getCreate_type_name().getAny_name().getName().equals(check_type.get(y))) {

                                                                                        // System.out.println("888888888888888888888888877777");
                                                                                        for (int n = 0; n < Main.type_list.get(m).getColumn_def_type().size(); n++) {
                                                                                            Type type2 = new Type();
                                                                                            for (int b = 0; b < Main.type_list.get(m).getColumn_def_type().get(n).getType_names().size(); b++) {

                                                                                                type2.setName(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                                columns.put(Main.type_list.get(m).getColumn_def_type().get(n).getCol_name().getAny_name().getName() + "_" + Main.type_list.get(m).getCreate_type_name().getAny_name().getName(), type2);
                                                                                                column_add.add(Main.type_list.get(m).getColumn_def_type().get(n).getType_names().get(b).getNames().getName().getName());

                                                                                            }



                                                                                        }


                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                        //end flat type

                                                                        //flat
                                                                        try {
                                                                            if (Main.table_name.contains(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName())) {

                                                                                System.out.println("8888888888888888888888888 " + columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName());
                                                                                for (int m = 0; m < Main.table_list.size(); m++) {

                                                                                    if (Main.table_list.get(m).getTb_name().getAny_name().getName().equals(columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(u).getTb_name().getAny_name().getName()).getName())) {

                                                                                        // System.out.println("888888888888888888888888877777");
                                                                                        for (int n = 0; n < Main.table_list.get(m).getColumn_defs().size(); n++) {
                                                                                            Type type2 = new Type();
                                                                                            for (int b = 0; b < Main.table_list.get(m).getColumn_defs().get(n).getType_names().size(); b++) {

                                                                                                type2.setName(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());
                                                                                                columns.put(Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "_" + Main.table_list.get(m).getTb_name().getAny_name().getName(), type2);
                                                                                                column_add.add(Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());

                                                                                                //s added
                                                                                                System.out.print(" column_name : "+ Main.table_list.get(m).getColumn_defs().get(n).getCol_name().getAny_name().getName()+" ");

                                                                                                System.out.println(" var_column_type : "+Main.table_list.get(m).getColumn_defs().get(n).getType_names().get(b).getNames().getName().getName());


                                                                                            }


                                                                                        }


                                                                                    }


                                                                                }


                                                                            }
                                                                        }
                                                                        catch (NullPointerException e){}





                                                                        //end flat
                                                                        //s added
                                                                /*System.out.print("column_name : " + Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName() + " , ");
                                                                System.out.println("var_column_type : " + columns.get(Main.table_list.get(u).getColumn_defs().get(v).getCol_name().getAny_name().getName()).getName());
*/


//
                                                                        test = true;


                                                                    }

                                                                    if (!(list.contains(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getText())))  {

                                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getLine());
                                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getCharPositionInLine());
                                                                        System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                                      //  System.exit(1);

                                                                        //System.err.println("ttttest");

                                                                    }

                                                                }



                                                            }



                                                        }



                                                    } else {


                                                        assigneSelect.setLine(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getLine());
                                                        assigneSelect.setCol(ctx.factored_select_stmt().select_core().result_column().get(i).expr().column_name().any_name().getStart().getCharPositionInLine());
                                                        System.err.println("undefine column : "+" at line : "+assigneSelect.getLine()+" at col : "+assigneSelect.getCol());
                                                     //   System.exit(1);
                                                    }
                                                }
                                            }
                                        }
                                        //}
//                                else
//                                {
////
//                                    System.err.println("undefine table");
//                                    System.exit(1);
//                                }
                                    }


                                }





                            }

                            //System.out.println("----" + type_name);
                            if(s.getType()!=null)
                            {
                                if(s.getType().getName()!=type.getName())
                                {
                                    System.err.println("Error for assigning another type declared variable "+s.getName()+" the colume is : "+ assigneSelect.getCol()+"   the line is :  " +assigneSelect.getLine());
                                }
                                else {
                                    type.setName(type_name);
                                    type.setColumns(columns);
                                    s.setType(type);
                                    System.out.println("varSymbol type :  "+s.getType().getName());
                                    System.out.println("scop var : "+s.getScope().getId());
                                }
                            }
                            else {
                                type.setName(type_name);
                                type.setColumns(columns);
                                s.setType(type);

                                System.out.println("varSymbol type :  "+s.getType().getName());
                                System.out.println("scop var : "+s.getScope().getId());

                            }


                            //   Main.symbolTable.addScope(currentScope);

                        }


                        break;
                    }
                    parent=parent.getParent();
                }
                if(found==false)
                {
                    System.out.println("Error for using undeclared variable "+var_name +"the colume is : "+ assigneSelect.getCol()+"   the line is :  " +assigneSelect.getLine());

                }
            }

        }


        if (ctx.declare_array() != null) {

            assigneSelect.setDeclareArray(visitDeclare_array(ctx.declare_array()));
        }

        return assigneSelect;
    }

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public Object visitColumn_alias(SQLParser.Column_aliasContext ctx) {
        return visitChildren(ctx);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    //@Override public  Object visitLiteral_value(SQLParser.Literal_valueContext ctx) { return visitChildren(ctx); }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */

    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public Object visitKeyword(SQLParser.KeywordContext ctx) {
        return visitChildren(ctx);
    }


    //////////////////////////////////// Our Function ////////////////////////////////////////

    @Override
    public FunctionDeclaration visitFunction(SQLParser.FunctionContext ctx) {
        declaredFunction declaredFunctions= new declaredFunction();
        declaredFunctions.setFunctionName(ctx.fun_name().getText());
        Scope funScope = new Scope();
        funScope.setId("fun_" + ctx.fun_name().getText());
        Main.symbolTable.addScope(funScope);
        Main.parent_stack.push(funScope);
        declaredFunctions.setScopefunction(funScope);
        Main.symbolTable.addFunction(declaredFunctions);
        //System.out.println("Visitfunction");
        FunctionDeclaration func = new FunctionDeclaration();
        func.setLine(ctx.getStart().getLine());
        func.setCol(ctx.getStart().getCharPositionInLine());
        func.setFuncname(visitFun_name(ctx.fun_name()));
        System.out.println(func.getFuncname().getFuncname());

        func.setHeader(visitParameter_list(ctx.parameter_list()));

        func.setBody(visitBody(ctx.body()));
        func.setScope_function(funScope);
        System.out.println("Scope name : " + Main.parent_stack.peek().getId());
        for (String key : funScope.getSymbolMap().keySet()) {
            System.out.println(key);
        }


        Main.parent_stack.pop();
        return func;

    }

    @Override
    public FunctionName visitFun_name(SQLParser.Fun_nameContext ctx) {
        //System.out.println("Visit function_name");
        FunctionName name = new FunctionName();
        name.setFuncname(ctx.getText());
        name.setLine(ctx.getStart().getLine());
        name.setCol(ctx.getStart().getCharPositionInLine());
        return name;
    }

    @Override
    public HeaderFunction visitParameter_list(SQLParser.Parameter_listContext ctx) {
        System.out.println("Visit header_function");
        HeaderFunction header = new HeaderFunction();
        header.setLine(ctx.getStart().getLine());
        header.setCol(ctx.getStart().getCharPositionInLine());
        List<Variable> vars=new ArrayList<>();
        List<Symbol>symbols=new ArrayList<>();
      //  Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1 );
        Scope currentScope = Main.parent_stack.peek();
        if(ctx.define_var()!=null)
        {
            for(int i=0;i<ctx.define_var().size();i++)
            {
                vars.add(visitDefine_var(ctx.define_var(i)));
            }
            header.setParameters(vars);

            for (int i=0;i<ctx.define_var().size();i++) {

                Symbol symbol = new Symbol();
                symbol.setName(ctx.define_var(i).IDENTIFIER().getSymbol().getText());
                symbol.setIsParam(true);
                symbol.setScope(currentScope);
                currentScope.addSymbol(symbol.getName(), symbol);
                symbols.add(symbol);
                header.getSymbols().add(symbol);
            }

        }
        if(ctx.define_var_assigne()!=null)
        {
            for(int i=0;i<ctx.define_var_assigne().size();i++)
            {
                vars.add(visitDefine_var_assigne(ctx.define_var_assigne(i)));
            }
            header.setParameters(vars);

            for (int i=0;i<ctx.define_var_assigne().size();i++) {

                Symbol symbol = new Symbol();
                symbol.setName(ctx.define_var_assigne(i).IDENTIFIER(0).getText());
                symbol.setIsParam(true);
                symbol.setScope(currentScope);
                Type type = new Type();
                type.setName(this.expr_type);
                symbol.setType(type);
                currentScope.addSymbol(symbol.getName(), symbol);
                symbols.add(symbol);
                header.getSymbols().add(symbol);

            }

        }


        return header;

    }

    @Override
    public BodyFunction visitBody(SQLParser.BodyContext ctx) {
       // System.out.println("Visit_body");
        BodyFunction body = new BodyFunction();
        body.setLine(ctx.getStart().getLine());
        body.setCol(ctx.getStart().getCharPositionInLine());
        List<AssigneVar> assigneVars = new ArrayList<>();
        List<Expr> ex = new ArrayList<>();
        ///**********add majd****************/
        List<While> whiles = new ArrayList<>();
        List<Do_While> do_whiles = new ArrayList<>();
        List<Call_func> call_funcs = new ArrayList<>();
        List<switch_case> switch_cases = new ArrayList<>();
        List<Block> blocks = new ArrayList<>();
        /************end add**********************/

        List<ForEach> foreachlist = new ArrayList<>();
        List<DeclareArray> declareArrays = new ArrayList<>();
        List<AssigneArray> assigneArrays = new ArrayList<>();
        List<DeclareVar> declareVars = new ArrayList<>();
        List<AssigneSelect> assigneSelects = new ArrayList<>();
        List<JsonObjectStmt> jsonObjectStmts = new ArrayList<>();
        List<forDecleration> for_stmt = new ArrayList<>();
        List<IfDecleration> if_stmt = new ArrayList<>();
        List<print> pr = new ArrayList<>();

        ///**********add majd****************/
        if (ctx.define_var() != null) {
            for (int i = 0; i < ctx.define_var().size(); i++) {
                declareVars.add(visitDefine_var(ctx.define_var(i)));
            }
            body.setDeclareVars(declareVars);
        }
        if (ctx.call_func() != null) {
            for (int i = 0; i < ctx.call_func().size(); i++) {
                call_funcs.add(visitCall_func(ctx.call_func().get(i)));
                body.setCall_funcs(call_funcs);
            }
        }
        if (ctx.define_var_assigne() != null) {
            for (int i = 0; i < ctx.define_var_assigne().size(); i++) {
                assigneVars.add(visitDefine_var_assigne(ctx.define_var_assigne(i)));
            }
            body.setAssigneVars(assigneVars);
        }
        if (ctx.assigne_select() != null) {

            for (int i = 0; i < ctx.assigne_select().size(); i++) {
                assigneSelects.add(visitAssigne_select(ctx.assigne_select(i)));
            }
            body.setAssigneSelects(assigneSelects);
        }
        if (ctx.while_stmt() != null) {
            for (int i = 0; i < ctx.while_stmt().size(); i++) {
                whiles.add(visitWhile_stmt(ctx.while_stmt().get(i)));
            }
            body.setWhiles(whiles);
        }
        if (ctx.do_while_stmt() != null) {
            for (int i = 0; i < ctx.do_while_stmt().size(); i++) {
                do_whiles.add(visitDo_while_stmt(ctx.do_while_stmt().get(i)));
                body.setDo_whiles(do_whiles);
            }
        }
        if (ctx.switch_stmt() != null) {
            for (int i = 0; i < ctx.switch_stmt().size(); i++) {
                switch_cases.add(visitSwitch_stmt(ctx.switch_stmt().get(i)));
                for (int j = 0; j < ctx.switch_stmt().get(i).body_switch().case_stm().size(); j++) {
                    if (ctx.switch_stmt().get(i).body_switch().case_stm().get(j).any_name() != null) {
                        System.out.println("1111111111111111111");
                        if (ctx.switch_stmt().get(i).body_switch().case_stm().get(j).any_name() != null) {
                            System.out.println("222222222222222");
                            if (ctx.switch_stmt().get(i).body_switch().case_stm().get(j).any_name().IDENTIFIER() != null) {
                                System.out.println("33333");
                                //  Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
                                Scope currentScope =   Main.parent_stack.peek();
                                Symbol symbol = currentScope.getSymbolMap().get(ctx.switch_stmt().get(i).body_switch().case_stm().get(j).any_name().IDENTIFIER().getText());
                                System.out.println("currentscope " + currentScope.getId());
                                if (symbol == null) {

                                    Scope currentparent = Main.parent_stack.peek();
                                    System.out.println("currentscope " + currentparent.getId());
                                    boolean found = false;
                                    Scope parent = currentparent.getParent();
                                    while (parent != null) {
                                        Symbol s = parent.getSymbolMap().get(ctx.switch_stmt().get(i).body_switch().case_stm().get(j).any_name().IDENTIFIER().getText());
                                        if (s != null) {
                                            if(s.getType()==null)
                                            {
                                                System.err.println("Warring for using anassigned variable "+ ctx.switch_stmt().get(i).body_switch().case_stm().get(j).any_name().IDENTIFIER().getText()+"the colume is "+"the colume is : "+ switch_cases.get(i).getBody_switch().getCases().get(j).getCol()+"   the line is :  " +switch_cases.get(i).getBody_switch().getCases().get(j).getLine());

                                            }
                                            found = true;
                                            break;
                                        }
                                        parent = parent.getParent();
                                    }
                                    if (found == false) {

                                        System.err.println("Error for using undeclared variable " +ctx.switch_stmt().get(i).body_switch().case_stm().get(j).any_name().IDENTIFIER().getText()+"the colume is : "+ switch_cases.get(i).getBody_switch().getCases().get(j).getCol()+"   the line is :  " +switch_cases.get(i).getBody_switch().getCases().get(j).getLine());

                                    }
                                }else{
                                    if(symbol.getType()==null)
                                    {
                                        System.err.println("Warring for using anassigned variable "+ ctx.switch_stmt().get(i).body_switch().case_stm().get(j).any_name().IDENTIFIER().getText()+"the colume is "+"the colume is : "+ switch_cases.get(i).getBody_switch().getCases().get(j).getCol()+"   the line is :  " +switch_cases.get(i).getBody_switch().getCases().get(j).getLine());

                                    }
                                }

                            }
                        }
                    } else {
                        System.out.println("00000000000");
                    }

                }
            }
            body.setSwitch_cases(switch_cases);
        }
        if (ctx.block() != null) {
            for (int i = 0; i < ctx.block().size(); i++) {
                blocks.add(visitBlock(ctx.block().get(i)));
            }
            body.setBlocks(blocks);
        }
        /************end add**********************/
        if(ctx.expr()!=null)
        {
            for (int i = 0; i < ctx.expr().size(); i++) {
                ex.add(visitExpr(ctx.expr(i)));
                for (int j = 0; j < ctx.expr().get(i).expr().size(); j++) {
                    if (ctx.expr().get(i).expr().get(j).column_name() != null) {
                        System.out.println("1111111111111111111");
                        if (ctx.expr().get(i).expr().get(j).column_name().any_name() != null) {
                            System.out.println("222222222222222");
                            if (ctx.expr().get(i).expr().get(j).column_name().any_name().IDENTIFIER() != null) {
                                System.out.println("33333");
                              //  Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
                                Scope currentScope =   Main.parent_stack.peek();
                                Symbol symbol = currentScope.getSymbolMap().get(ctx.expr().get(i).expr().get(j).column_name().any_name().IDENTIFIER().getText());
                                System.out.println("currentscope " + currentScope.getId());
                                if (symbol == null) {
                                    Scope currentparent = Main.parent_stack.peek();
                                    System.out.println("currentscope " + currentparent.getId());
                                    boolean found = false;
                                    Scope parent = currentparent.getParent();
                                    while (parent != null) {
                                        Symbol s = parent.getSymbolMap().get(ctx.expr().get(i).expr().get(j).column_name().any_name().IDENTIFIER().getText());
                                        if (s != null) {
                                            found = true;
                                            if(s.getType()==null)
                                            {
                                                System.err.println("Warring for using anassigned variable "+  ctx.expr().get(i).expr().get(j).column_name().any_name().IDENTIFIER().getText()+"the colume is : "+ ex.get(i).getCol()+"   the line is :  " +ex.get(i).getLine());

                                            }
                                            break;
                                        }
                                        parent = parent.getParent();
                                    }
                                    if (found == false) {

                                        System.err.println("Error for using undeclared variable " + ctx.expr().get(i).expr().get(j).column_name().any_name().IDENTIFIER().getText()+"the colume is : "+ ex.get(i).getCol()+"   the line is :  " +ex.get(i).getLine());

                                    }
                                }
                                else {
                                    if(symbol.getType()==null) {
                                        System.err.println("Warring for using anassigned variable " + ctx.expr().get(i).expr().get(j).column_name().any_name().IDENTIFIER().getText() + "the colume is : " + ex.get(i).getCol() + "   the line is :  " + ex.get(i).getLine());
                                    }
                                }

                            }
                        }
                    } else {
                        System.out.println("00000000000");
                    }

                }

                body.setExpr(ex);
            }
        }
        if (ctx.foreach() != null) {
            for (int i = 0; i < ctx.foreach().size(); i++) {
                foreachlist.add(visitForeach(ctx.foreach(i)));
            }
            body.setForeach_stmt(foreachlist);
        }
        if (ctx.declare_array() != null) {
            for (int i = 0; i < ctx.declare_array().size(); i++) {
                declareArrays.add(visitDeclare_array(ctx.declare_array(i)));
            }
            body.setDeclareArrays(declareArrays);
        }
        if (ctx.declare_array_vr() != null) {
            for (int i = 0; i < ctx.declare_array_vr().size(); i++) {
                assigneArrays.add(visitDeclare_array_vr(ctx.declare_array_vr(i)));
            }
            body.setAssigneArrays(assigneArrays);
        }
        if (ctx.json_object_stmt() != null) {
            for (int i = 0; i < ctx.json_object_stmt().size(); i++) {
                jsonObjectStmts.add(visitJson_object_stmt(ctx.json_object_stmt(i)));
            }
            body.setJsonObjectStmts(jsonObjectStmts);
        }
        if (ctx.for_stmt() != null) {
            //For Stmt
            for (int i = 0; i < ctx.for_stmt().size(); i++) {
                System.out.println("for_stmt:" + ctx.for_stmt().get(i).getText());
                for_stmt.add(visitFor_stmt(ctx.for_stmt(i)));
            }
            body.setFor_stmt(for_stmt);

        }
        if (ctx.if_stmt() != null) {
            for (int i = 0; i < ctx.if_stmt().size(); i++) {
                System.out.println("if_stmt:" + ctx.if_stmt().get(i).getText());
                if_stmt.add(visitIf_stmt(ctx.if_stmt().get(i)));
            }
            body.setIf_stmt(if_stmt);
        }
        if (ctx.print() != null) {
            for (int i = 0; i < ctx.print().size(); i++) {
                pr.add(visitPrint(ctx.print().get(i)));
            }
            body.setPrints(pr);

        }
        if (ctx.if_line_stmt() != null) {
            //If Line
            List<ifLine> if_line = new ArrayList<>();
            for (int i = 0; i < ctx.if_line_stmt().size(); i++) {
                System.out.println("if Line :" + ctx.if_line_stmt().get(i).getText());
                if_line.add(visitIf_line_stmt(ctx.if_line_stmt().get(i)));
            }
            body.setIf_line(if_line);
        }
        if (ctx.set_prorerty_json() != null) {
            List<SetPropertyJson> setPropertyJsons = new ArrayList<>();
            for (int i = 0; i < ctx.set_prorerty_json().size(); i++) {
                setPropertyJsons.add(visitSet_prorerty_json(ctx.set_prorerty_json(i)));
            }
            body.setPropertyJsons(setPropertyJsons);
        }
        if (ctx.return_stm() != null) {
            body.setReturn_stm(visitReturn_stm(ctx.return_stm()));
        }
        //Plus Var
        if (ctx.plus_var() != null) {
            List<plusVar> plus_var = new ArrayList<>();
            for (int i = 0; i < ctx.plus_var().size(); i++) {
                plus_var.add(visitPlus_var(ctx.plus_var(i)));
            }
            body.setPlus_var(plus_var);
        }
        //Min Var
        if (ctx.min_var() != null) {
            List<minVar> min_var = new ArrayList<>();
            for (int i = 0; i < ctx.min_var().size(); i++) {
                min_var.add(visitMin_var(ctx.min_var(i)));
            }
            body.setMin_var(min_var);
        }
        if (ctx.sql_stmt_list().size() > 0) {
            body.setSql_statements(visitSql_stmt_list(ctx.sql_stmt_list(0)));
        }
        return body;
    }

    /***************add majd*********************/
    @Override
    public BodyFunctionWilthoutBracket visitBody_without_bracketa(SQLParser.Body_without_bracketaContext ctx) {
        //System.out.println("Visit body Wilthout Bracket ");
        BodyFunctionWilthoutBracket body = new BodyFunctionWilthoutBracket();
        body.setLine(ctx.getStart().getLine());
        body.setCol(ctx.getStart().getCharPositionInLine());
        List<ForEach> foreachlist = new ArrayList<>();
        List<DeclareArray> declareArrays = new ArrayList<>();
        List<AssigneArray> assigneArrays = new ArrayList<>();
        List<DeclareVar> declareVars = new ArrayList<>();
        List<AssigneVar> assigneVars = new ArrayList<>();
        List<JsonObjectStmt> jsonObjectStmts = new ArrayList<>();
        List<forDecleration> for_stmt = new ArrayList<>();
        List<IfDecleration> if_stmt = new ArrayList<>();

        List<Expr> ex = new ArrayList<>();
        List<While> whiles = new ArrayList<>();
        List<Do_While> do_whiles = new ArrayList<>();
        List<Call_func> call_funcs = new ArrayList<>();
        List<switch_case> switch_cases = new ArrayList<>();
        if (ctx.define_var() != null) {
            declareVars.add(visitDefine_var(ctx.define_var()));
            body.setDeclareVars(declareVars);
        }
        if (ctx.define_var_assigne() != null) {
            assigneVars.add(visitDefine_var_assigne(ctx.define_var_assigne()));
            body.setAssigneVars(assigneVars);
        }
        if (ctx.assigne_select() != null) {
            List<AssigneSelect> assigneSelects = new ArrayList<>();
            assigneSelects.add(visitAssigne_select(ctx.assigne_select()));
            body.setAssigneSelects(assigneSelects);
        }
        if (ctx.expr() != null) {
            ex.add(visitExpr(ctx.expr()));
            body.setExpr(ex);
        }
        if (ctx.foreach() != null) {
            foreachlist.add(visitForeach(ctx.foreach()));
            body.setForeach_stmt(foreachlist);
        }
        if (ctx.declare_array() != null) {
            declareArrays.add(visitDeclare_array(ctx.declare_array()));
            body.setDeclareArrays(declareArrays);
        }
        if (ctx.declare_array_vr() != null) {

            assigneArrays.add(visitDeclare_array_vr(ctx.declare_array_vr()));
            body.setAssigneArrays(assigneArrays);
        }

        if (ctx.json_object_stmt() != null) {
            jsonObjectStmts.add(visitJson_object_stmt(ctx.json_object_stmt()));
            body.setJsonObjectStmts(jsonObjectStmts);
        }
        if (ctx.for_stmt() != null) {
            //For Stmt
            //  System.out.println("for_stmt:"+ctx.for_stmt().get().getText());
            for_stmt.add(visitFor_stmt(ctx.for_stmt()));
            body.setFor_stmt(for_stmt);

        }
        if (ctx.if_stmt() != null) {
            //System.out.println("if_stmt:"+ctx.if_stmt().get(i).getText());
            if_stmt.add(visitIf_stmt(ctx.if_stmt()));
            body.setIf_stmt(if_stmt);
        }
        if (ctx.print() != null) {
            List<print> pr = new ArrayList<>();
            // System.out.println("print :"+ctx.print().get(i).getText());
            pr.add(visitPrint(ctx.print()));
            body.setPrintt(pr);
        }
        if (ctx.if_line_stmt() != null) {
            //If Line
            List<ifLine> if_line = new ArrayList<>();
            //System.out.println("if Line :"+ctx.if_line_stmt().get(i).getText());
            if_line.add(visitIf_line_stmt(ctx.if_line_stmt()));
            body.setIf_line(if_line);
        }

        if (ctx.while_stmt() != null) {
            whiles.add(visitWhile_stmt(ctx.while_stmt()));
            body.setWhiles(whiles);
        }
        if (ctx.do_while_stmt() != null) {
            do_whiles.add(visitDo_while_stmt(ctx.do_while_stmt()));
            body.setDo_whiles(do_whiles);
        }
        if (ctx.call_func() != null) {
            call_funcs.add(visitCall_func(ctx.call_func()));
            body.setCall_funcs(call_funcs);
        }
        if (ctx.switch_stmt() != null) {
            switch_cases.add(visitSwitch_stmt(ctx.switch_stmt()));
            body.setSwitch_cases(switch_cases);
        }


        //Plus Var
        if (ctx.plus_var() != null) {
            List<plusVar> plus_var = new ArrayList<>();

            plus_var.add(visitPlus_var(ctx.plus_var()));

            body.setPlus_var(plus_var);
        }

        //Min Var
        if (ctx.min_var() != null) {
            List<minVar> min_var = new ArrayList<>();

            min_var.add(visitMin_var(ctx.min_var()));

            body.setMin_var(min_var);
        }

        if (ctx.return_stm() != null) {
            body.setReturn_stm(visitReturn_stm(ctx.return_stm()));
        }

        return body;
    }

    @Override
    public While visitWhile_stmt(SQLParser.While_stmtContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("visit while statement");
        Scope whileScope = new Scope();
        whileScope.setId("while_scope");
        whileScope.setParent(parent);
        Main.symbolTable.addScope(whileScope);
        Main.parent_stack.push(whileScope);
        While whil = new While();
        whil.setLine(ctx.getStart().getLine());
        whil.setCol(ctx.getStart().getCharPositionInLine());
        if(ctx.condition_header()!=null)
        {
            Header_while header_while= new Header_while();
            header_while.setCondition_header(visitCondition_header(ctx.condition_header()));
            whil.setHeader_while(header_while);
        }
        if(ctx.body()!=null) {
            whil.setBodyFunction(visitBody(ctx.body()));
        }
        if(ctx.body_without_bracketa()!=null) {

            whil.setBODY(visitBody_without_bracketa(ctx.body_without_bracketa()));

        }
        whil.setScope(whileScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +whileScope.getParent().getId() );
        for (String key : whileScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return whil;

    }

    @Override
    public Do_While visitDo_while_stmt(SQLParser.Do_while_stmtContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("visit do while statement");
        Scope do_whileScope = new Scope();
        do_whileScope.setId("do_whileScope");
        do_whileScope.setParent(parent);
        Main.symbolTable.addScope(do_whileScope);
        Main.parent_stack.push(do_whileScope);

        Do_While do_while = new Do_While();
        do_while.setLine(ctx.getStart().getLine());
        do_while.setCol(ctx.getStart().getCharPositionInLine());
        if(ctx.body()!=null)
        {
            do_while.setBodyFunction(visitBody(ctx.body()));
        }
        if(ctx.body_without_bracketa()!= null)
        {
            do_while.setBODY(visitBody_without_bracketa(ctx.body_without_bracketa()));
        }
        if(ctx.condition_header()!=null)
        {
            Header_while header_while= new Header_while();
            header_while.setCondition_header(visitCondition_header(ctx.condition_header()));

            do_while.setHeader_while(header_while);
        }
        do_while.setScope(do_whileScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +do_whileScope.getParent().getId() );
        for (String key : do_whileScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return do_while;

    }

//    call_func:
//            (K_VAR IDENTIFIER '=')? fun_name '(' argument_list ')'
//    ;
    @Override
    public Call_func visitCall_func(SQLParser.Call_funcContext ctx) {
        System.out.println("visit call funcation");
        Call_func call_func = new Call_func();
        call_func.setLine(ctx.getStart().getLine());
        call_func.setCol(ctx.getStart().getCharPositionInLine());
        Scope currentScope =   Main.parent_stack.peek();
        Type type = new Type();
        boolean founds =false;
        boolean foundes =false;
        if (ctx.ASSIGN() != null) {
            String var_name=ctx.IDENTIFIER().getText();
            if(ctx.K_VAR()!= null){
                if(currentScope.getSymbolMap().get(var_name)!=null)
                {
                    System.err.println("Error in multiple declaration : a variable "+var_name+" should be declared in the same scope at most once "+"the colume is : "+ call_func.getCol()+"   the line is :  " +call_func.getLine());
                }
                else{
                    Symbol varSymbol = new Symbol();
                    //set Identefier
                    varSymbol.setIsParam(false);
                    varSymbol.setName(var_name);
                    varSymbol.setScope(currentScope);

                    boolean found =false;
                    for(int i=0;i<Main.symbolTable.getDeclaredFunction().size();i++)
                    {
                        if(Main.symbolTable.getDeclaredFunction().get(i).getFunctionName().equals(ctx.fun_name().IDENTIFIER().getText()))
                        {
                            found=true;
                            if(Main.symbolTable.getDeclaredFunction().get(i).getReturnType()!=null)
                            {
                                varSymbol.setType(Main.symbolTable.getDeclaredFunction().get(i).getReturnType());
                                System.out.println("varSymbol type :  "+varSymbol.getType().getName());
                            }
                           break;
                        }
                    }
                    if(found==false)
                    {
                        System.err.println("Error foe calling undeclared method " +ctx.fun_name().IDENTIFIER().getText()+"the colume is : "+ call_func.getCol()+"   the line is :  " +call_func.getLine());
                    }
                    currentScope.addSymbol(var_name,varSymbol);
                    System.out.println("scop var : "+varSymbol.getScope().getId());
                    call_func.setSymbol(varSymbol);
                    System.out.println("tabel symbol : "+ call_func.getSymbol().getName());

                }

            }
            else {
                Symbol symbol =  currentScope.getSymbolMap().get(var_name);
                if(symbol!=null) {
                    boolean founde = false;
                    for (int i = 0; i < Main.symbolTable.getDeclaredFunction().size(); i++) {
                        if (Main.symbolTable.getDeclaredFunction().get(i).getFunctionName().equals(ctx.fun_name().IDENTIFIER().getText())) {
                            founde = true;
                            if (Main.symbolTable.getDeclaredFunction().get(i).getReturnType() != null) {
                                symbol.setType(Main.symbolTable.getDeclaredFunction().get(i).getReturnType());
                                System.out.println("varSymbol type :  " + symbol.getType().getName());
                                System.out.println("scop var : " + symbol.getScope().getId());
                            }
                            break;

                        }
                    }
                    if (founde == false) {
                        System.err.println("Error foe calling undeclared method " + ctx.fun_name().IDENTIFIER().getText() + "the colume is : " + call_func.getCol() + "   the line is :  " + call_func.getLine());
                    }
            //    }
                }
                else
                {
                    Scope currentparent = Main.parent_stack.peek();
                    System.out.println("currentscope "+ currentparent.getId());
                    boolean found= false;
                    Scope parent =currentparent.getParent();
                    while(parent!=null){
                        Symbol s = parent.getSymbolMap().get(var_name);
                        if(s!=null){
                            found= true;

                            for(int i=0;i<Main.symbolTable.getDeclaredFunction().size();i++)
                            {
                                if(Main.symbolTable.getDeclaredFunction().get(i).getFunctionName().equals(ctx.fun_name().IDENTIFIER().getText()))
                                {
                                    foundes=true;
                                    if(Main.symbolTable.getDeclaredFunction().get(i).getReturnType()!=null)
                                    {
                                        s.setType(Main.symbolTable.getDeclaredFunction().get(i).getReturnType());
                                        System.out.println("varSymbol type :  "+s.getType().getName());
                                    }
                                      break;
                                }
                            }
                            if(foundes==false)
                            {
                                System.err.println("Error foe calling undeclared method " +ctx.fun_name().IDENTIFIER().getText()+"the colume is : "+ call_func.getCol()+"   the line is :  " +call_func.getLine());
                            }
                          //  s.setType(type);
                            System.out.println("varSymbol type :  "+s.getType().getName());
                            break;
                        }
                        parent=parent.getParent();
                    }
                    if(found==false)
                    {
                        System.out.println("Error for using undeclared variable "+var_name +"the colume is : "+ call_func.getCol()+"   the line is :  " +call_func.getLine());

                    }
                }
            }
            call_func.setVar_assin(ctx.IDENTIFIER().getText());
            System.out.println(ctx.IDENTIFIER().getText());
        }
        else {
            boolean ok = false;
            for (int j = 0; j < Main.symbolTable.getDeclaredAggregationFunction().size(); j++) {
                System.out.println("1 " + Main.symbolTable.getDeclaredAggregationFunction().get(j).getFunction_name().getAny_name().getName() + "2 " + ctx.fun_name().IDENTIFIER().getText());
                if (Main.symbolTable.getDeclaredAggregationFunction().get(j).getFunction_name().getAny_name().getName().equals(ctx.fun_name().IDENTIFIER().getText())) {
                    ok = true;
                    System.out.println("okkkkkkkkkkkkkkkkkkkkkkk  " + ok);
                }

            }
            if(ok==false) {
////////////////////////////
                for (int i = 0; i < Main.symbolTable.getDeclaredFunction().size(); i++) {

                    if (Main.symbolTable.getDeclaredFunction().get(i).getFunctionName().equals(ctx.fun_name().IDENTIFIER().getText())) {
                        founds = true;

                        break;
                    }
                }

                if (founds == false) {
                    System.err.println("Error foe calling undeclared method " + ctx.fun_name().IDENTIFIER().getText() + "the colume is : " + call_func.getCol() + "   the line is :  " + call_func.getLine());
                }
            }
        }
        call_func.setFun_name(visitFun_name(ctx.fun_name()));
        if (ctx.argument_list() != null) {
            call_func.setArgument_list(visitArgument_list(ctx.argument_list()));
        }


        return call_func;
    }

    @Override
    public Argument_list visitArgument_list(SQLParser.Argument_listContext ctx) {
        System.out.println(" visit argument list ");
        Argument_list argument_list = new Argument_list();
        argument_list.setLine(ctx.getStart().getLine());
        argument_list.setCol(ctx.getStart().getCharPositionInLine());
        List<Expr> exprs = new ArrayList<>();
        if (ctx.expr_var() != null) {
            for (int i = 0; i < ctx.expr_var().size(); i++) {
                exprs.add(visitExpr_var(ctx.expr_var(i)));
            }
            argument_list.setExpr(exprs);
        }
        if (ctx.heigher_order_function() != null) {
            argument_list.setHeigher(visitHeigher_order_function(ctx.heigher_order_function()));
        }

        return argument_list;
    }

    @Override
    public heigher_order_function visitHeigher_order_function(SQLParser.Heigher_order_functionContext ctx) {
        System.out.println("visit heigher order function ");
        heigher_order_function heigher = new heigher_order_function();
        heigher.setLine(ctx.getStart().getLine());
        heigher.setCol(ctx.getStart().getCharPositionInLine());
        if (ctx.args_list_higher() != null) {
            heigher.setArgument_list_higher(visitArgs_list_higher(ctx.args_list_higher()));
        }
        if (ctx.body() != null) {
            heigher.setBodyFunction(visitBody(ctx.body()));
        }

        return heigher;
    }

    @Override
    public Argument_list_higher visitArgs_list_higher(SQLParser.Args_list_higherContext ctx) {
        System.out.println("visit args list higher");
        Argument_list_higher argument = new Argument_list_higher();
        argument.setLine(ctx.getStart().getLine());
        argument.setCol(ctx.getStart().getCharPositionInLine());
        List<Expr> expr = new ArrayList<>();
        List<Bollean> bool_exp = new ArrayList<>();
        if (ctx.expr_var() != null) {
            for (int i = 0; i < ctx.expr_var().size(); i++) {
                expr.add(visitExpr_var(ctx.expr_var(i)));
            }
            argument.setExpr(expr);
        }
        if (ctx.bool_ex() != null) {
            for (int i = 0; i < ctx.bool_ex().size(); i++) {
                bool_exp.add(visitBool_ex(ctx.bool_ex(i)));
            }
            argument.setBool_exor(bool_exp);
        }

        return argument;
    }

    @Override
    public switch_case visitSwitch_stmt(SQLParser.Switch_stmtContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("visit switch ");
        Scope switchScope = new Scope();
        switchScope.setId("switchScope");
        switchScope.setParent(parent);
        Main.symbolTable.addScope(switchScope);
        Main.parent_stack.push(switchScope);

        switch_case aswitch = new switch_case();
        aswitch.setLine(ctx.getStart().getLine());
        aswitch.setCol(ctx.getStart().getCharPositionInLine());
        Header_switch header_switch= new Header_switch();
        if(ctx.call_func()!=null)
        {
            header_switch.setCall_func(visitCall_func(ctx.call_func()));
        }
        if(ctx.expr_var()!=null)
        {
            header_switch.setExpr(visitExpr_var(ctx.expr_var()));
        }
        if(ctx.if_line_stmt()!=null)
        {
            header_switch.setIfLine(visitIf_line_stmt(ctx.if_line_stmt()));
        }
        if(ctx.define_var_assigne()!=null)
        {
            header_switch.setAssigneVar(visitDefine_var_assigne(ctx.define_var_assigne()));
        }
        aswitch.setHeader_switch(header_switch);
        if(ctx.body_switch()!=null)
        {
            aswitch.setBody_switch(visitBody_switch(ctx.body_switch()));
        }
        aswitch.setScope(switchScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +switchScope.getParent().getId() );
        for (String key : switchScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return aswitch;



    }

    @Override
    public Body_switch visitBody_switch(SQLParser.Body_switchContext ctx) {
        System.out.println("visit body switch");
        Body_switch body_switch =new Body_switch();
        body_switch.setLine(ctx.getStart().getLine());
        body_switch.setCol(ctx.getStart().getCharPositionInLine());
        List<Case> cases= new ArrayList<>();
        if(ctx.case_stm()!=null)
        {
            for(int i=0; i<ctx.case_stm().size();i++)
            {
                cases.add(visitCase_stm(ctx.case_stm().get(i)));
            }
            body_switch.setCases(cases);
        }
        if(ctx.default_stm_switch()!=null)
        {
            body_switch.setaDefault(visitDefault_stm_switch(ctx.default_stm_switch()));
        }

        return body_switch;
    }

    @Override
    public Case visitCase_stm(SQLParser.Case_stmContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println(" visit case stm ");
        Scope caseScope = new Scope();
        caseScope.setId("caseScope");
        caseScope.setParent(parent);
        Main.symbolTable.addScope(caseScope);
        Main.parent_stack.push(caseScope);

        Case acase= new Case();
        acase.setLine(ctx.getStart().getLine());
        acase.setCol(ctx.getStart().getCharPositionInLine());
        if (ctx.literal_value()!=null)
        {
            acase.setCase_of_switch(visitLiteral_value(ctx.literal_value()));
            System.out.println(ctx.literal_value().getText());
        }
        if(ctx.any_name()!=null)
        {
            acase.setAny_name(visitAny_name(ctx.any_name()));
        }
        if(ctx.body()!=null)
        {
            acase.setBody(visitBody(ctx.body()));
        }
        acase.setScope(caseScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +caseScope.getParent().getId() );
        for (String key : caseScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return acase;
    }

    @Override
    public Default visitDefault_stm_switch(SQLParser.Default_stm_switchContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("visit default stm switch");
        Scope  defaultcaseScope = new Scope();
        defaultcaseScope.setId("defaultcaseScope");
        defaultcaseScope.setParent(parent);
        Main.symbolTable.addScope(defaultcaseScope);
        Main.parent_stack.push(defaultcaseScope);

        Default adefault = new Default();
        adefault.setLine(ctx.getStart().getLine());
        adefault.setCol(ctx.getStart().getCharPositionInLine());
        adefault.setBodyFunction(visitBody(ctx.body()));
        adefault.setScope(defaultcaseScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +defaultcaseScope.getParent().getId() );
        for (String key : defaultcaseScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return adefault;
    }

    @Override
    public Block visitBlock(SQLParser.BlockContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println(" visit block ");
        Scope blockScope = new Scope();
        blockScope.setId("blockScope");
        blockScope.setParent(parent);
        Main.symbolTable.addScope(blockScope);
        Main.parent_stack.push(blockScope);

        Block block =new Block();
        block.setLine(ctx.getStart().getLine());
        block.setCol(ctx.getStart().getCharPositionInLine());

        block.setBodyFunction(visitBody(ctx.body()));
        block.setScope(blockScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +blockScope.getParent().getId() );
        for (String key : blockScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return block;
    }

    @Override
    public Return_stm visitReturn_stm(SQLParser.Return_stmContext ctx) {
     Type types=new Type();
        System.out.println(" visit return ");
        Return_stm return_stm = new Return_stm();
        return_stm.setLine(ctx.getStart().getLine());
        return_stm.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.bool_ex() != null) {
            return_stm.setBool_exor(visitBool_ex(ctx.bool_ex()));
        }
        if (ctx.expr_test() != null) {
            return_stm.setExpr(visitExpr_test(ctx.expr_test()));
        }
        if (ctx.if_line_stmt() != null) {
            return_stm.setIfLine(visitIf_line_stmt(ctx.if_line_stmt()));
        }

        types.setName(  this.expr_type);
         Main.symbolTable.getDeclaredFunction().get(Main.symbolTable.getDeclaredFunction().size() - 1).setReturnType(types);
         System.out.println("type return function "+ Main.symbolTable.getDeclaredFunction().get(Main.symbolTable.getDeclaredFunction().size() - 1).getReturnType().getName());
        return return_stm;
    }

    /******************end******************/
    @Override
    public JsonObjectStmt visitJson_object_stmt(SQLParser.Json_object_stmtContext ctx) {
        System.out.println("VisitJsonObjectStatedment");
        JsonObjectStmt json = new JsonObjectStmt();
        json.setLine(ctx.getStart().getLine());
        json.setCol(ctx.getStart().getCharPositionInLine());

        json.setObjectName(ctx.IDENTIFIER().getText());
        System.out.println("json object name : " + json.getObjectName());
        if (ctx.json_body() != null) {
            //visit json body
            json.setJsonBody(visitJson_body(ctx.json_body()));
        }

        return json;
    }

    @Override
    public JsonBody visitJson_body(SQLParser.Json_bodyContext ctx) {
        System.out.println("VisitJsonbody");
        JsonBody jsonBody = new JsonBody();
        jsonBody.setLine(ctx.getStart().getLine());
        jsonBody.setCol(ctx.getStart().getCharPositionInLine());

        List<PropertyJson> propertyJsons = new ArrayList<>();
        //visit list of property json

        for (int i = 0; i < ctx.property_json().size(); i++) {
            propertyJsons.add(visitProperty_json(ctx.property_json(i)));
        }
        jsonBody.setPropertyJsons(propertyJsons);

        return jsonBody;
    }

    @Override
    public PropertyJson visitProperty_json(SQLParser.Property_jsonContext ctx) {
        System.out.println("VisitPropertyjson");
        PropertyJson propertyJson = new PropertyJson();
        propertyJson.setLine(ctx.getStart().getLine());
        propertyJson.setCol(ctx.getStart().getCharPositionInLine());

        propertyJson.setPropertyName(ctx.IDENTIFIER().getText());
        System.out.println("PropertyName : " + propertyJson.getPropertyName());

        if (ctx.expr_var() != null) {
            propertyJson.setExpr(visitExpr_var(ctx.expr_var()));
        }
        if (ctx.array_json() != null) {
            //visit arrayjson
            propertyJson.setArrayJson(visitArray_json(ctx.array_json()));
        }
        if (ctx.json_body() != null) {
            //visitjsonbody nested object
            propertyJson.setJsonBody(visitJson_body(ctx.json_body()));
        }
        return propertyJson;
    }

    @Override
    public ArrayJson visitArray_json(SQLParser.Array_jsonContext ctx) {
        System.out.println("VisiteArrayJson");

        ArrayJson arrayJson = new ArrayJson();
        arrayJson.setLine(ctx.getStart().getLine());
        arrayJson.setCol(ctx.getStart().getCharPositionInLine());

        List<LvalueJson> lvalueJsons = new ArrayList<>();
        List<JsonBody> jsonBodies = new ArrayList<>();

        if (ctx.literal_value_json() != null) {
            for (int i = 0; i < ctx.literal_value_json().size(); i++) {
                lvalueJsons.add(visitLiteral_value_json(ctx.literal_value_json(i)));
            }
            arrayJson.setLvalueJsons(lvalueJsons);
        }
        if (ctx.json_body() != null) {
            for (int i = 0; i < ctx.json_body().size(); i++) {
                jsonBodies.add(visitJson_body(ctx.json_body(i)));
            }
            arrayJson.setJsonBodies(jsonBodies);
        }

        return arrayJson;
    }

    @Override
    public LvalueJson visitLiteral_value_json(SQLParser.Literal_value_jsonContext ctx) {
        System.out.println("visiteLiteralvalue");
        LvalueJson lvalueJson = new LvalueJson();
        lvalueJson.setLine(ctx.getStart().getLine());
        lvalueJson.setCol(ctx.getStart().getCharPositionInLine());

        lvalueJson.setValue(ctx.getText());
        if (ctx.array_json() != null) {
            System.out.println("visitarrayjson");
            lvalueJson.setArrayJson(visitArray_json(ctx.array_json()));
            System.out.println(lvalueJson.getArrayJson().getLvalueJsons().get(0).getValue());
            System.out.println(lvalueJson.getArrayJson().getLvalueJsons().get(1).getValue());
        }


        return lvalueJson;
    }

    @Override
    public DeclareVar visitDefine_var(SQLParser.Define_varContext ctx) {
        System.out.println("Visitdeclar");
        DeclareVar vars=new DeclareVar();
        vars.setLine(ctx.getStart().getLine());
        vars.setCol(ctx.getStart().getCharPositionInLine());

      //  Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
        Scope currentScope =   Main.parent_stack.peek();
        String var_name=ctx.IDENTIFIER().getText();
        if(currentScope.getSymbolMap().get(var_name)!=null)
        {
            System.err.println("Error in multiple declaration : a variable "+var_name+" should be declared in the same scope at most once "+"the colume is : "+ vars.getCol()+"   the line is :  " +vars.getLine());
        }
         else {
            Symbol varSymbol = new Symbol();
            //set Identefier
            varSymbol.setIsParam(false);
            varSymbol.setName(var_name);
            varSymbol.setScope(currentScope);
            currentScope.addSymbol(var_name,varSymbol);
            vars.setSymbol(varSymbol);
        }
        vars.setVarname(ctx.IDENTIFIER().getText());

        return vars;

    }

    @Override
    public AssigneVar visitDefine_var_assigne(SQLParser.Define_var_assigneContext ctx) {
        System.out.println("Visitassigne");
        AssigneVar svar=new AssigneVar();
        svar.setLine(ctx.getStart().getLine());
        svar.setCol(ctx.getStart().getCharPositionInLine());

        if(ctx.assigne_select()!=null)
        {
            System.out.println("vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv");
            svar.setAssigneSelect(visitAssigne_select(ctx.assigne_select()));
        }
        else {
            //    Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
            Scope currentScope = Main.parent_stack.peek();
            Type type = new Type();
            String var_name = ctx.IDENTIFIER(0).getText();
            svar.setVar(ctx.IDENTIFIER(0).getText());
            if (ctx.bool_ex() != null) {
                System.out.println("9999999999999999");
                svar.setBoll_expr(visitBool_ex(ctx.bool_ex()));
            }
            if (ctx.expr_var() != null) {

                svar.setExp(visitExpr_var(ctx.expr_var()));
            }
            if (ctx.IDENTIFIER(1) != null && ctx.IDENTIFIER(1).getText() != null) {
                svar.setValue_var(ctx.IDENTIFIER(1).getText());
            }

            type.setName(this.expr_type);
            if (ctx.K_VAR() != null) {
                if (currentScope.getSymbolMap().get(var_name) != null)
                {
                    System.err.println("Error in multiple declaration : a variable " + var_name + " should be declared in the same scope at most once " + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                }
                else {
                    Symbol varSymbol = new Symbol();
                    //set Identefier
                    varSymbol.setIsParam(false);
                    varSymbol.setName(var_name);
                    varSymbol.setScope(currentScope);
                    if (ctx.IDENTIFIER(1) != null && ctx.IDENTIFIER(1).getText() != null)
                    {
                        Symbol v_symbol = currentScope.getSymbolMap().get(ctx.IDENTIFIER(1).getText());
                        if (v_symbol != null) {
                            if (v_symbol.getType() != null) {
                                varSymbol.setType(v_symbol.getType());
                                System.out.println("var type1 : " + varSymbol.getType().getName() + "  var type2 : " + v_symbol.getType().getName());
                            } else {
                                System.out.println("Warring for using unassigned variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                            }
                        } else {
                            Scope c_parent_v = Main.parent_stack.peek();
                            System.out.println("currentscope " + c_parent_v.getId());
                            boolean found = false;
                            Scope parent_v = c_parent_v.getParent();
                            while (parent_v != null) {
                                Symbol v_s = parent_v.getSymbolMap().get(ctx.IDENTIFIER(1).getText());
                                if (v_s != null) {
                                    found = true;

                                    if (v_s.getType() != null) {
                                        varSymbol.setType(v_s.getType());
                                        System.out.println("var type1 : " + varSymbol.getType().getName() + "  var type2 : " + v_s.getType().getName());
                                    } else {
                                        System.out.println("Warring for using unassigned variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                    }
                                    break;
                                }
                                parent_v = parent_v.getParent();
                            }
                            if (found == false) {
                                System.out.println("Error for using undeclared variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                            }
                        }

                    }
                    else if(ctx.expr_var()!=null&&ctx.expr_var().call_func()!=null) {
                     if(ctx.expr_var().call_func()!=null){
                        boolean founds = false;
                        for (int i = 0; i < Main.symbolTable.getDeclaredFunction().size(); i++) {
                            if (Main.symbolTable.getDeclaredFunction().get(i).getFunctionName().equals(ctx.expr_var().call_func().fun_name().IDENTIFIER().getText())) {
                                founds = true;
                                if (Main.symbolTable.getDeclaredFunction().get(i).getReturnType() != null) {
                                    varSymbol.setType(Main.symbolTable.getDeclaredFunction().get(i).getReturnType());
                                    System.out.println("varSymbol type :  " + varSymbol.getType().getName());
                                }

                            }
                        }
                        if (founds == false) {
                            // System.err.println("Error foe calling undeclared method " +ctx.fun_name().IDENTIFIER().getText()+"the colume is : "+ call_func.getCol()+"   the line is :  " +call_func.getLine());
                        }
                    }
                    }
                    else {
                    //   System.out.println("11111111111111111111111ttttttttttttttttt"+type.getName());
                        varSymbol.setType(type);
                        System.out.println("varSymbol type :  " + varSymbol.getType().getName());
                    }
                    currentScope.addSymbol(var_name, varSymbol);
                    System.out.println("scop var : " + varSymbol.getScope().getId());
                    svar.setSymbol(varSymbol);
                    System.out.println("tabel symbol : " + svar.getSymbol().getName());
                }


            }
            else {
                Symbol symbol = currentScope.getSymbolMap().get(var_name);
                if (symbol != null) {
                    if (ctx.IDENTIFIER(1) != null && ctx.IDENTIFIER(1).getText() != null)
                    {
                        Symbol v_symbol1 = currentScope.getSymbolMap().get(ctx.IDENTIFIER(1).getText());
                        if (v_symbol1 != null) {
                            if (v_symbol1.getType() != null) {
                                if (symbol.getType() != null) {
                                    if (symbol.getType().getName() != v_symbol1.getType().getName()) {
                                        System.err.println("Error for assigning another type declared variable " + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                    } else {
                                        symbol.setType(v_symbol1.getType());
                                        System.out.println("var type1 : " + symbol.getType().getName() + "  var type2 : " + v_symbol1.getType().getName());

                                    }
                                } else {
                                    symbol.setType(v_symbol1.getType());
                                    System.out.println("var type1 : " + symbol.getType().getName() + "  var type2 : " + v_symbol1.getType().getName());

                                }
                            } else {
                                System.err.println("Warring for using unassigned variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                            }
                        } else {
                            Scope c_parent_v1 = Main.parent_stack.peek();
                            System.out.println("currentscope " + c_parent_v1.getId());
                            boolean found = false;
                            Scope parent_v1 = c_parent_v1.getParent();
                            while (parent_v1 != null) {
                                Symbol v_s = parent_v1.getSymbolMap().get(ctx.IDENTIFIER(1).getText());
                                if (v_s != null) {
                                    found = true;
                                    if (v_s.getType() != null) {
                                        if (symbol.getType() != null) {
                                            if (symbol.getType().getName() != v_s.getType().getName()) {
                                                System.err.println("Error for assigning another type declared variable " + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());

                                            } else {
                                                symbol.setType(v_s.getType());
                                                System.out.println("var type1 : " + symbol.getType().getName() + "  var type2 : " + v_s.getType().getName());

                                            }
                                        } else {
                                            symbol.setType(v_s.getType());
                                            System.out.println("var type1 : " + symbol.getType().getName() + "  var type2 : " + v_s.getType().getName());

                                        }
                                    } else {
                                        System.err.println("Warring for using unassigned variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                    }
                                    break;
                                }
                                parent_v1 = parent_v1.getParent();
                            }
                            if (found == false) {
                                System.err.println("Error for using undeclared variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                            }
                        }

                    }
                    else if(ctx.expr_var()!=null&&ctx.expr_var().call_func()!=null) {
                        if(ctx.expr_var().call_func()!=null){
                            boolean founds = false;
                            for (int i = 0; i < Main.symbolTable.getDeclaredFunction().size(); i++) {
                                if (Main.symbolTable.getDeclaredFunction().get(i).getFunctionName().equals(ctx.expr_var().call_func().fun_name().IDENTIFIER().getText())) {
                                    founds = true;
                                    if (Main.symbolTable.getDeclaredFunction().get(i).getReturnType() != null) {
//                                        if(symbol.getType()==null)
//                                        {
//                                            System.err.println("Warring for using anassigned variable "+var_name +"the colume is : "+ svar.getCol()+"   the line is :  " +svar.getLine());
//                                        }
//                                        else
//                                        {
                                        if(symbol.getType()!=null) {
                                            if (symbol.getType().getName().equals(Main.symbolTable.getDeclaredFunction().get(i).getReturnType().getName())) {
                                                symbol.setType(Main.symbolTable.getDeclaredFunction().get(i).getReturnType());
                                                System.out.println("varSymbol type :  " + symbol.getType().getName());
                                            } else {
                                                System.err.println("Error for assigne another type for declared variable " + var_name + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                            }
                                        }
                                        else {
                                            symbol.setType(Main.symbolTable.getDeclaredFunction().get(i).getReturnType());
                                            System.out.println("varSymbol type :  " + symbol.getType().getName());
                                        }
                                      //  }

                                    }

                                }
                            }
                            if (founds == false) {
                                // System.err.println("Error foe calling undeclared method " +ctx.fun_name().IDENTIFIER().getText()+"the colume is : "+ call_func.getCol()+"   the line is :  " +call_func.getLine());
                            }
                        }
                    }
                    else
                        {
                           // System.out.println("22222222222222222ttttttttttttttttt"+type.getName());
                        if (symbol.getType() != null) {
                            if (symbol.getType().getName() != type.getName())
                            {
                                System.err.println("Error for assigning another type declared variable " + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                            }
                            else
                                {
                                currentScope.getSymbolMap().get(var_name).setType(type);
                                System.out.println("var type : " + symbol.getType().getName());
                            }
                        } else {
                            currentScope.getSymbolMap().get(var_name).setType(type);
                            System.out.println("var type : " + symbol.getType().getName());
                        }

                    }
                    System.out.println("scop var : " + symbol.getScope().getId());
                    svar.setSymbol(symbol);
                    System.out.println("tabel symbol : " + svar.getSymbol().getName());
                }
                else {
                    Scope currentparent = Main.parent_stack.peek();
                    System.out.println("currentscope " + currentparent.getId());
                    boolean found = false;
                    Scope parent = currentparent.getParent();
                    while (parent != null) {
                        Symbol s = parent.getSymbolMap().get(var_name);
                        if (s != null) {
                            found = true;
                            if (ctx.IDENTIFIER(1) != null && ctx.IDENTIFIER(1).getText() != null) {
                                Symbol v_symbol2 = currentScope.getSymbolMap().get(ctx.IDENTIFIER(1).getText());
                                if (v_symbol2 != null) {
                                    if (v_symbol2.getType() != null) {
                                        if (s.getType() != null) {
                                            if (s.getType().getName() != v_symbol2.getType().getName()) {
                                                System.err.println("Error for assigning another type declared variable " + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                            } else {
                                                s.setType(v_symbol2.getType());
                                                System.out.println("var type1 : " + s.getType().getName() + "  var type2 : " + v_symbol2.getType().getName());

                                            }
                                        } else {
                                            s.setType(v_symbol2.getType());
                                            System.out.println("var type1 : " + s.getType().getName() + "  var type2 : " + v_symbol2.getType().getName());

                                        }
                                    } else {
                                        System.out.println("Warring for using unassigned variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                    }
                                } else {
                                    Scope c_parent_v2 = Main.parent_stack.peek();
                                    System.out.println("currentscope " + c_parent_v2.getId());
                                    boolean found2 = false;
                                    Scope parent_v2 = c_parent_v2.getParent();
                                    while (parent_v2 != null) {
                                        Symbol v_s2 = parent_v2.getSymbolMap().get(ctx.IDENTIFIER(1).getText());
                                        if (v_s2 != null) {
                                            found2 = true;

                                            if (v_s2.getType() != null) {
                                                if (s.getType() != null) {
                                                    if (s.getType().getName() != v_s2.getType().getName()) {
                                                        System.err.println("Error for assigning another type declared variable " + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                                    } else {
                                                        s.setType(v_s2.getType());
                                                        System.out.println("var type1 : " + s.getType().getName() + "  var type2 : " + v_s2.getType().getName());
                                                    }
                                                } else {
                                                    s.setType(v_s2.getType());
                                                    System.out.println("var type1 : " + s.getType().getName() + "  var type2 : " + v_s2.getType().getName());

                                                }

                                            } else {
                                                System.out.println("Warring for using unassigned variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                            }
                                            break;
                                        }
                                        parent_v2 = parent_v2.getParent();
                                    }
                                    if (found2 == false) {
                                        System.out.println("Error for using undeclared variable " + ctx.IDENTIFIER(1).getText() + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                    }
                                }

                            }
                            else if(ctx.expr_var()!=null&&ctx.expr_var().call_func()!=null) {
                                if(ctx.expr_var().call_func()!=null){
                                    boolean founds = false;
                                    for (int i = 0; i < Main.symbolTable.getDeclaredFunction().size(); i++) {
                                        if (Main.symbolTable.getDeclaredFunction().get(i).getFunctionName().equals(ctx.expr_var().call_func().fun_name().IDENTIFIER().getText())) {
                                            founds = true;
                                            if (Main.symbolTable.getDeclaredFunction().get(i).getReturnType() != null) {
//                                                if(s.getType()==null)
//                                                {
//                                                    System.err.println("Warring for using anassigned variable "+var_name +"the colume is : "+ svar.getCol()+"   the line is :  " +svar.getLine());
//                                                }
//                                                else
//                                                {
                                                if(s.getType()!=null) {
                                                    if (s.getType().getName().equals(Main.symbolTable.getDeclaredFunction().get(i).getReturnType().getName())) {
                                                        s.setType(Main.symbolTable.getDeclaredFunction().get(i).getReturnType());
                                                        System.out.println("varSymbol type :  " + s.getType().getName());
                                                    } else {
                                                        System.err.println("Error for assigne another type for declared variable " + var_name + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                                    }
                                                }
                                                else {
                                                    s.setType(Main.symbolTable.getDeclaredFunction().get(i).getReturnType());
                                                    System.out.println("varSymbol type :  " + s.getType().getName());
                                                }
                                               // }

                                            }

                                        }
                                    }
                                    if (founds == false) {
                                        // System.err.println("Error foe calling undeclared method " +ctx.fun_name().IDENTIFIER().getText()+"the colume is : "+ call_func.getCol()+"   the line is :  " +call_func.getLine());
                                    }
                                }
                            }
                            else {
                                if (s.getType() != null) {
                                    if (s.getType().getName() != type.getName()) {
                                        System.err.println("Error for assigning another type declared variable " + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());
                                    } else {
                                        s.setType(type);
                                        System.out.println("varSymbol type :  " + s.getType().getName());

                                    }
                                } else {
                                    s.setType(type);
                                    System.out.println("varSymbol type :  " + s.getType().getName());

                                }


                            }

                            break;
                        }
                        parent = parent.getParent();
                    }
                    if (found == false) {
                        System.err.println("Error for using undeclared variable " + var_name + "the colume is : " + svar.getCol() + "   the line is :  " + svar.getLine());

                    }
                }

            }

        }
        return svar;
    }

    @Override
    public Expr visitExpr_var(SQLParser.Expr_varContext ctx) {

        System.out.println("visitExpr");
        MathimaticalExpr me = new MathimaticalExpr();
        UnaryExpr unaryExpr=new UnaryExpr();
        Expr ex_res = new Expr();
        ex_res.setLine(ctx.getStart().getLine());
        ex_res.setCol(ctx.getStart().getCharPositionInLine());

        //  Type type = new Type();
        List<Call_func>call_funcs= new ArrayList<>();
        Scope currentscope = Main.parent_stack.peek();
        System.out.println("currentscope "+currentscope.getId());
        if(ctx.literal_value()!=null)
        {
            ex_res.setValue(ctx.literal_value().getText());
            if (ctx.literal_value().NUMERIC_LITERAL()!=null)
            {
                this.expr_type=Type.NUMBER_CONST;
            }
            if (ctx.literal_value().STRING_LITERAL()!=null)
            {
                this.expr_type=Type.STRING_CONST;
            }
        }
      //  type.setName(this.expr_type);
        if(ctx.IDENTIFIER()!=null)
        {
            Symbol symbol =  currentscope.getSymbolMap().get(ctx.IDENTIFIER().getText());
            if(symbol!=null)
            {
                if(symbol.getType()!=null)
                {
                    this.expr_type=symbol.getType().getName();

                   // System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\"+this.expr_type+"+++++++"+type);
                    System.out.println("scop var : "+symbol.getScope().getId());
                    ex_res.setSymbol(symbol);
                    System.out.println("tabel symbol : "+ ex_res.getSymbol().getName());
                }
                else {
                    System.err.println("Warring for using anassigned variable "+ ctx.IDENTIFIER().getText()+"the colume is "+ ex_res.getCol() + "   the line is :  " + ex_res.getLine());


                }

            }else {
                boolean found = false;
                Scope parent = currentscope.getParent();
                while (parent != null) {
                    Symbol s = parent.getSymbolMap().get(ctx.IDENTIFIER().getText());
                    if (s != null) {
                        found = true;
                        if(s.getType()==null)
                        {
                           // this.expr_type=s.getType().getName();

                          //  System.out.println("\\\\\\\\\\\\\\\\\\\\\\\\\\\\"+this.expr_type+"+++++++"+type);
                            System.err.println("Warring for using anassigned variable "+ ctx.IDENTIFIER().getText()+"the colume is "+ ex_res.getCol() + "   the line is :  " + ex_res.getLine());

                        }
                        else {
                            this.expr_type=s.getType().getName();
                        }
                        break;
                    }
                    parent = parent.getParent();
                }
                if (found == false) {
                    System.err.println("Error for using undeclared variable " + ctx.IDENTIFIER().getText() + "  the colume is : " + ex_res.getCol() + "   the line is :  " + ex_res.getLine());
                }
            }
         //   this.expr_type=Type.STRING_CONST;
            ex_res.setValue(ctx.IDENTIFIER().getText());
        }
        if(ctx.OPEN_PAR()!=null&&ctx.CLOSE_PAR()!=null)
        {
            ex_res.setExprwithbrackets(visitExpr_var(ctx.expr_var(0)));
        }
        if(ctx.call_func()!=null)
        {
            call_funcs.add(visitCall_func(ctx.call_func()));
            ex_res.setCall_funcs(call_funcs);
        }
        if(ctx.array_assigne()!=null)
        {
            ex_res.setDeclareArray(visitArray_assigne(ctx.array_assigne()));
        }
        if(ctx.expr_var(0)!=null&&ctx.expr_var(0).array_assigne()!=null)
        {
            ex_res.setDeclareArray(visitArray_assigne(ctx.expr_var(0).array_assigne()));
        }
        if(ctx.expr_var(1)!=null&&ctx.expr_var(1).array_assigne()!=null)
        {
            ex_res.setDeclareArray(visitArray_assigne(ctx.expr_var(1).array_assigne()));
        }
        if(ctx.get_property_json()!=null)
        {
            ex_res.setGetPropertyJsons(visitGet_property_json(ctx.get_property_json()));
        }
        if(ctx.expr_var(0)!=null&&ctx.expr_var(0).get_property_json()!=null)
        {
            ex_res.setGetPropertyJsons(visitGet_property_json(ctx.expr_var(0).get_property_json()));
        }
        if(ctx.expr_var(1)!=null&&ctx.expr_var(1).get_property_json()!=null)
        {
            ex_res.setGetPropertyJsons(visitGet_property_json(ctx.expr_var(1).get_property_json()));
        }
        if (ctx.incr_decr_without_comma()!=null)
        {
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
        }
        if(ctx.expr_var(0)!=null&&ctx.expr_var(0).incr_decr_without_comma()!=null)
        {
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.expr_var(0).incr_decr_without_comma()));
        }
        if(ctx.expr_var(1)!=null&&ctx.expr_var(1).incr_decr_without_comma()!=null)
        {
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.expr_var(1).incr_decr_without_comma()));
        }
        if(ctx.unary_operator()!=null)
        {
            System.out.println("unaryoperator");
            if(ctx.unary_operator().PLUS()!=null)
            {
                unaryExpr.setTerm(visitExpr_var(ctx.expr_var(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().PLUS);
            }
            if(ctx.unary_operator().MINUS()!=null)
            {
                unaryExpr.setTerm(visitExpr_var(ctx.expr_var(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().MINUS);
            }

            ex_res.setUnaryExpr(unaryExpr);
        }

        if(ctx.STAR() != null) {
            System.out.println("Star");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.STAR().getText());
            System.out.println(ctx.expr_var(1).getText());
            if(ctx.expr_var(0)!=null)
            {
                me.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }
            if(ctx.expr_var(1)!=null)
            {
                me.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            me.setMath(me.getMath().STAR);
            ex_res = me;

        }
        if (ctx.PLUS() != null) {
            System.out.println("Plus");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.PLUS() .getText());
            System.out.println(ctx.expr_var(1).getText());
            if(ctx.expr_var(0)!=null)
            {
                me.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }

            if(ctx.expr_var(1)!=null)
            {
                me.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            me.setMath(me.getMath().PLUS);
            ex_res=me;
        }
        if (ctx.MINUS()!= null) {
            System.out.println("Minus");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.MINUS() .getText());
            System.out.println(ctx.expr_var(1).getText());
            if(ctx.expr_var(0)!=null)
            {
                me.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }
            if(ctx.expr_var(1)!=null)
            {
                me.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            me.setMath(me.getMath().MINUS);
            ex_res=me;
        }
        if (ctx.DIV()!= null) {
            System.out.println("DIV");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.DIV() .getText());
            System.out.println(ctx.expr_var(1).getText());
            if(ctx.expr_var(0)!=null)
            {
                me.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }
            if(ctx.expr_var(1)!=null)
            {
                me.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            me.setMath(me.getMath().DIV);
            ex_res=me;
        }
        if (ctx.MOD()!= null) {
            System.out.println("MOD");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.MOD() .getText());
            System.out.println(ctx.expr_var(1).getText());
            if(ctx.expr_var(0)!=null)
            {
                me.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }
            if(ctx.expr_var(1)!=null)
            {
                me.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            me.setMath(me.getMath().MOD);
            ex_res=me;

        }
       // this.expr_type=Type.NUMBER_CONST;
        return ex_res;
    }

    @Override
    public ForEach visitForeach(SQLParser.ForeachContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("visitForeach");
        Scope ForeachScope = new Scope();
        ForeachScope.setId("ForeachScope");
        ForeachScope.setParent(parent);
        Main.symbolTable.addScope(ForeachScope);
        Main.parent_stack.push(ForeachScope);

        ForEach forEach = new ForEach();
        forEach.setLine(ctx.getStart().getLine());
        forEach.setCol(ctx.getStart().getCharPositionInLine());

        forEach.setForEachHeader(visitForeach_header(ctx.foreach_header()));
        if(ctx.body()!=null) {
            // System.out.println("visiteBodyforeach");
            forEach.setBody(visitBody(ctx.body()));
        }
        if(ctx.body_without_bracketa()!=null)
        {
            forEach.setBodyFunctionWilthoutBracket(visitBody_without_bracketa(ctx.body_without_bracketa()));
        }
        forEach.setScope(ForeachScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +ForeachScope.getParent().getId() );
        for (String key : ForeachScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return forEach;



    }

    @Override
    public ForEachHeader visitForeach_header(SQLParser.Foreach_headerContext ctx) {

        System.out.println("visitForeach_header");
        ForEachHeader forEachHeader= new ForEachHeader();
        forEachHeader.setLine(ctx.getStart().getLine());
        forEachHeader.setCol(ctx.getStart().getCharPositionInLine());

        //    Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
        Scope currentScope =   Main.parent_stack.peek();
        Type type = new Type();
        type.setName(this.expr_type);
        System.out.println("var currentScope : "+currentScope.getId());
        if(ctx.K_VAR()!= null){
            if(currentScope.getSymbolMap().get(ctx.IDENTIFIER(0).getText())!=null)
            {
                System.err.println("Error in multiple declaration : a variable "+ctx.IDENTIFIER(0).getText()+" should be declared in the same scope at most once "+"the colume is : "+ forEachHeader.getCol()+"   the line is :  " +forEachHeader.getLine());
            }
            else{
                Symbol varSymbol = new Symbol();
                //set Identefier
                varSymbol.setIsParam(false);
                varSymbol.setName(ctx.IDENTIFIER(0).getText());
                varSymbol.setScope(currentScope);
                Scope currentparent = Main.parent_stack.peek();
                System.out.println("currentscope "+ currentparent.getId());
                boolean found2= false;
                Scope parent =currentparent.getParent();
                while(parent!=null){
                    Symbol s = parent.getSymbolMap().get(ctx.IDENTIFIER(1).getText());
                    if(s!=null)
                    {
                        found2= true;
                        if(s.getType()==null)
                        {
                            System.err.println("Warring for using anassigned variable "+ctx.IDENTIFIER(1).getText() +"the colume is : "+ forEachHeader.getCol()+"   the line is :  " +forEachHeader.getLine());
                        }
                        else {
                            varSymbol.setType(s.getType());
                            // varSymbol.setType(type);
                            System.out.println("var type : "+varSymbol.getType().getName());
                        }
                        break;
                    }
                    parent=parent.getParent();
                }
                if(found2==false)
                {
                    System.out.println("Error for using undeclared variable "+ctx.IDENTIFIER(1).getText() +" the colume is : "+ forEachHeader.getCol()+"   the line is :  " +forEachHeader.getLine());
                }
                currentScope.addSymbol(ctx.IDENTIFIER(0).getText(),varSymbol);
                System.out.println("scop var : "+varSymbol.getScope().getId());
                forEachHeader.setSymbol(varSymbol);
                System.out.println("tabel symbol : "+ forEachHeader.getSymbol().getName());
            }
        }
        else {
            Scope currentparent = Main.parent_stack.peek();
            System.out.println("currentscope "+ currentparent.getId());
            boolean found1= false;
            Scope parent =currentparent.getParent();
            while(parent!=null){
                Symbol s1 = parent.getSymbolMap().get(ctx.IDENTIFIER(0).getText());
                if(s1!=null){
                    found1= true;
                    if(s1.getType()==null)
                    {
                        System.err.println("Warring for using anassigned variable "+ctx.IDENTIFIER(0).getText() +"the colume is : "+ forEachHeader.getCol()+"   the line is :  " +forEachHeader.getLine());
                    }
                    else {
                        boolean found2= false;
                        Scope parent2= currentparent.getParent();
                        while(parent2!=null)
                         {
                             Symbol s2 =parent2.getSymbolMap().get(ctx.IDENTIFIER(1).getText());
                             if(s2!=null)
                             {
                                 found2=true;
                                 if(s2.getType()==null)
                                 {
                                     System.err.println("Warring for using anassigned variable "+ctx.IDENTIFIER(1).getText() +"the colume is : "+ forEachHeader.getCol()+"   the line is :  " +forEachHeader.getLine());
                                 }
                                 else {
                                     if(s1.getType().getName()!=s2.getType().getName())
                                     {
                                         System.err.println("Error for assigne another type for declar variable "+ctx.IDENTIFIER(0).getText() +"the colume is : "+ forEachHeader.getCol()+"   the line is :  " +forEachHeader.getLine());
                                     }
                                     else {
                                         s1.setType(s2.getType());
                                     }
                                 }
                                 break;
                             }
                             parent2=parent2.getParent();
                         }
                        if(found2==false)
                        {
                            System.out.println("Error for using undeclared variable "+ctx.IDENTIFIER(1).getText() +"the colume is : "+ forEachHeader.getCol()+"   the line is :  " +forEachHeader.getLine());

                        }
                    }
                    break;
                }
                parent=parent.getParent();
            }
            if(found1==false)
            {
                System.out.println("Error for using undeclared variable "+ctx.IDENTIFIER(0).getText() +"the colume is : "+ forEachHeader.getCol()+"   the line is :  " +forEachHeader.getLine());

            }
        }

        forEachHeader.setLeftforeachstmt(ctx.IDENTIFIER(0).getText());
        forEachHeader.setRightforeachstmt(ctx.IDENTIFIER(1).getText());
        return forEachHeader;


    }

    @Override
    public SetPropertyJson visitSet_prorerty_json(SQLParser.Set_prorerty_jsonContext ctx) {
        System.out.println("Visitesetpropertyjson");
        SetPropertyJson setPropertyJson = new SetPropertyJson();
        setPropertyJson.setLine(ctx.getStart().getLine());
        setPropertyJson.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.get_property_json() != null) {
            setPropertyJson.setGetPropertyJson(visitGet_property_json(ctx.get_property_json()));
        }
        if (ctx.expr_var() != null) {
            setPropertyJson.setExpr(visitExpr_var(ctx.expr_var()));
        }
        return setPropertyJson;
    }

    @Override
    public List<DeclareArray> visitArray_assigne(SQLParser.Array_assigneContext ctx) {
        System.out.println("visitDeclareArrayinexp");
        List<DeclareArray> arr = new ArrayList<>();

        DeclareArray declareArray = new DeclareArray();

        //System.out.println(arr.get(0));

        declareArray.setArrayName(ctx.IDENTIFIER().getText());
        declareArray.setArrayindex1(visitExpr_var(ctx.expr_var(0)));
        System.out.println("the ArrayName : " + declareArray.getArrayName());
        if (ctx.expr_var(1) != null) {
            declareArray.setArrayindex2(visitExpr_var(ctx.expr_var(1)));
        }
        //
        arr.add(declareArray);

        return arr;
    }
    int m=1;
    @Override
    public Expr visitExpr(SQLParser.ExprContext ctx) {
        String NUMBER_CONST = "number";
        String STRING_CONST = "string";
        String BOOLEAN_CONST = "boolean";
        System.out.println("visitExpr");
        MathimaticalExpr me = new MathimaticalExpr();
        LogicalExpr le = new LogicalExpr();
        Expr ex_res = new Expr();
        ex_res.setLine(ctx.getStart().getLine());
        ex_res.setCol(ctx.getStart().getCharPositionInLine());

        Binary_Expr bi= new Binary_Expr();
        UnaryExpr unaryExpr=new UnaryExpr();
        expr_sql expr_sql=new expr_sql();
        List<Call_func>call_funcs= new ArrayList<>();
        Bollean boll = new Bollean();

        if (ctx.literal_value() != null) {
            // System.out.println("llllll");
            ex_res.setValue(ctx.literal_value().getText());
            if(ctx.literal_value().NUMERIC_LITERAL()!=null)
            {
                ex_res.setValueType(NUMBER_CONST);
            }
            if(ctx.literal_value().STRING_LITERAL()!=null)
            {
                ex_res.setValueType(STRING_CONST);
            }

        }


        if (ctx.call_func() != null) {
            call_funcs.add(visitCall_func(ctx.call_func()));
            ex_res.setCall_funcs(call_funcs);
        }
        if(ctx.column_name()!=null)
        {
            if(ctx.column_name().any_name()!=null)
            {
                ex_res.setCol_name(visitColumn_name(ctx.column_name()));
                // ex_res.setValue(ctx.column_name().any_name().getText());
                System.out.println("****************************************************");
                System.out.println("**************************************************** "+ex_res.getCol_name().getAny_name().getName());
            }
        }
        if(ctx.table_name()!=null)
        {
            ex_res.setTablename(visitTable_name(ctx.table_name()));
        }
        if(ctx.database_name()!=null)
        {
            ex_res.setDatabasename(visitDatabase_name(ctx.database_name()));
        }
        if(ctx.bool_ex()!=null)
        {
            ex_res.setBool_expr(visitBool_ex(ctx.bool_ex()));
            ex_res.setValueType(BOOLEAN_CONST);
        }
        if(ctx.OPEN_PAR()!=null&&ctx.CLOSE_PAR()!=null)
        {
            if(ctx.factored_select_stmt()!=null)
            {
                ex_res.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
            }
            else {
                ex_res.setExprwithbrackets(visitExpr(ctx.expr(0)));
            }
        }
//        if(ctx.aggregation_func()!=null)
//        {
////            call_funcs.add(visitCall_func(ctx.call_func()));
////            ex_res.setCall_funcs(call_funcs);
//        }
        if(ctx.array_assigne()!=null)
        {
            ex_res.setDeclareArray(visitArray_assigne(ctx.array_assigne()));
        }
        if(ctx.expr(0)!=null&&ctx.expr(0).array_assigne()!=null)
        {
            ex_res.setDeclareArray(visitArray_assigne(ctx.expr(0).array_assigne()));
        }
        if(ctx.expr(1)!=null&&ctx.expr(1).array_assigne()!=null)
        {
            ex_res.setDeclareArray(visitArray_assigne(ctx.expr(1).array_assigne()));
        }
        if(ctx.unary_operator()!=null)
        {
            System.out.println("unaryoperator");
            if(ctx.unary_operator().PLUS()!=null)
            {
                unaryExpr.setTerm(visitExpr(ctx.expr(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().PLUS);
            }
            if(ctx.unary_operator().MINUS()!=null)
            {
                unaryExpr.setTerm(visitExpr(ctx.expr(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().MINUS);

            }

            ex_res.setUnaryExpr(unaryExpr);
        }
        if(ctx.expr(0)!=null&&ctx.expr(0).unary_operator()!=null)
        {
            System.out.println("unaryoperator");
            if(ctx.expr(0).unary_operator().PLUS()!=null)
            {
                unaryExpr.setTerm(visitExpr(ctx.expr(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().PLUS);
            }
            if(ctx.expr(0).unary_operator().MINUS()!=null)
            {
                unaryExpr.setTerm(visitExpr(ctx.expr(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().MINUS);

            }

            ex_res.setUnaryExpr(unaryExpr);
        }
        if(ctx.expr(1)!=null&&ctx.expr(1).unary_operator()!=null)
        {
            System.out.println("unaryoperator");
            if(ctx.expr(1).unary_operator().PLUS()!=null)
            {
                unaryExpr.setTerm(visitExpr(ctx.expr(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().PLUS);
            }
            if(ctx.expr(1).unary_operator().MINUS()!=null)
            {
                unaryExpr.setTerm(visitExpr(ctx.expr(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().MINUS);

            }

            ex_res.setUnaryExpr(unaryExpr);
        }
        if(ctx.incr_decr_without_comma()!=null)
        {

            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
        }
        if(ctx.expr(0)!=null&&ctx.expr(0).incr_decr_without_comma()!=null)
        {
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.expr(0).incr_decr_without_comma()));
        }
        if(ctx.expr(1)!=null&&ctx.expr(1).incr_decr_without_comma()!=null)
        {
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.expr(1).incr_decr_without_comma()));
        }
        if(ctx.ASSIGN()!=null)
        {
            System.out.println("ASSIGN");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.ASSIGN().getText());
            System.out.println(ctx.expr(1).getText());
            if(ctx.expr(0)!=null)
            {
                System.out.println("left");
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                System.out.println("right");
                me.setRight_expr(visitExpr(ctx.expr(1)));

            }
            me.setMath(me.getMath().ASSIGN);
            System.out.println("=== "+ me.getMath());
            ex_res.setMathimaticalExpr(me);
            // ex_res = me;

        }
        if (ctx.STAR() != null) {
            System.out.println("Star");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.STAR().getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {
                me.setLeft_expr(visitExpr(ctx.expr(0)));
                System.out.println("valye----"+me.getLeft_expr().getValue());
            }
            if(ctx.expr(1)!=null)
            {
                me.setRight_expr(visitExpr(ctx.expr(1)));
                System.out.println("valye----"+me.getRight_expr().getValue());
            }
            me.setMath(me.getMath().STAR);
            // ex_res = me;
            ex_res.setMathimaticalExpr(me);

        }
        if (ctx.PLUS() != null) {
            System.out.println("Plus");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.PLUS() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }
            if(ctx.expr(1)!=null)
            {
                me.setRight_expr(visitExpr(ctx.expr(1)));
            }
            me.setMath(me.getMath().PLUS);
            //ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if (ctx.MINUS()!= null) {
            System.out.println("Minus");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.MINUS() .getText());
            System.out.println(ctx.expr(1).getText());
            if(ctx.expr(0)!=null)
            {
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }
            if(ctx.expr(1)!=null)
            {
                me.setRight_expr(visitExpr(ctx.expr(1)));
            }
            me.setMath(me.getMath().MINUS);
            //ex_res=me;
            ex_res.setMathimaticalExpr(me);

        }

        if (ctx.DIV()!= null) {
            System.out.println("DIV");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.DIV() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().DIV);
            //  ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if (ctx.MOD()!= null) {
            System.out.println("MOD");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.MOD() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }


            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MOD);
            //  ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if(ctx.EQ()!=null)
        {
            System.out.println("EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }


            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().EQ);
            // ex_res=le;
            ex_res.setLogicalExpr(le);

        }
        if(ctx.GT()!=null)
        {
            System.out.println("GT");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.GT() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }


            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().GT);
            //   ex_res=le;
            ex_res.setLogicalExpr(le);
        }
        if(ctx.GT_EQ()!=null)
        {
            System.out.println("GT_EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.GT_EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                // System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }


            if(ctx.expr(1)!=null)
            {

                // System.out.println(ctx.expr(1).getChildCount());
                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().GT_EQ);
            // ex_res=le;
            ex_res.setLogicalExpr(le);
        }
        if(ctx.LT()!=null)
        {
            System.out.println("LT");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.LT() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().LT);
            // ex_res=le;
            ex_res.setLogicalExpr(le);

        }
        if(ctx.LT_EQ()!=null)
        {
            System.out.println("LT_EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.LT_EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().LT_EQ);
            // ex_res=le;
            ex_res.setLogicalExpr(le);
        }
        if(ctx.NOT_EQ1()!=null)
        {
            System.out.println("NOT_EQ1");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.NOT_EQ1() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().NOT_EQ1);
            // ex_res=le;
            ex_res.setLogicalExpr(le);
        }
        if(ctx.NOT_EQ2()!=null)
        {
            System.out.println("NOT_EQ1");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.NOT_EQ1() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().NOT_EQ2);
            // ex_res=le;
            ex_res.setLogicalExpr(le);
        }
        if(ctx.K_AND()!=null)
        {
            System.out.println("K_AND");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.K_AND() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().K_AND);
            //  ex_res=le;
            ex_res.setLogicalExpr(le);
        }
        if(ctx.K_OR()!=null)
        {
            System.out.println("K_OR");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.K_OR() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().K_OR);
            //  ex_res=le;
            ex_res.setLogicalExpr(le);

        }
        if(ctx.PLUS_EQ()!=null)
        {
            System.out.println("PLUS_EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.PLUS_EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                me.setRight_expr(visitExpr(ctx.expr(1)));
            }
            me.setMath(me.getMath().PLUS_EQ);
            // ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if(ctx.MINUS_EQ()!=null)
        {
            System.out.println("MINUS_EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.MINUS_EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MINUS_EQ);
            //   ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if(ctx.STAR_EQ()!=null)
        {
            System.out.println("STAR_EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.STAR_EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().STAR_EQ);
            // ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if(ctx.DIV_EQ()!=null)
        {
            System.out.println("DIV_EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.DIV_EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().DIV_EQ);

            // ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if(ctx.MOD_EQ()!=null)
        {
            System.out.println("MOD_EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.MOD_EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MOD_EQ);
            //  ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if(ctx.MOD_EQ()!=null)
        {
            System.out.println("MOD_EQ");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.MOD_EQ() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr(ctx.expr(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MOD_EQ);
            // ex_res=me;
            ex_res.setMathimaticalExpr(me);
        }
        if(ctx.AMP()!=null)
        {
            System.out.println("AMP");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.AMP() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                bi.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                bi.setRight_expr(visitExpr(ctx.expr(1)));
            }
            bi.setLogic(bi.getLogic().AMP);
            // ex_res=bi;
            ex_res.setBinary_expr(bi);
        }
        if(ctx.LT2()!=null)
        {
            System.out.println("LT2");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.LT2() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                bi.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                bi.setRight_expr(visitExpr(ctx.expr(1)));
            }
            bi.setLogic(bi.getLogic().LT2);
            // ex_res=bi;
            ex_res.setBinary_expr(bi);
        }
        if(ctx.GT2()!=null)
        {
            System.out.println("GT2");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.GT2() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                bi.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                bi.setRight_expr(visitExpr(ctx.expr(1)));
            }
            bi.setLogic(bi.getLogic().GT2);
            // ex_res=bi;
            ex_res.setBinary_expr(bi);
        }
        if(ctx.PIPE()!=null)
        {
            System.out.println("PIPE");
            System.out.println(ctx.expr(0).getText());
            System.out.println(ctx.PIPE() .getText());
            System.out.println(ctx.expr(1).getText());

            if(ctx.expr(0)!=null)
            {

                bi.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                bi.setRight_expr(visitExpr(ctx.expr(1)));
            }
            bi.setLogic(bi.getLogic().PIPE);
            // ex_res=bi;
            ex_res.setBinary_expr(bi);
        }

        if(ctx.K_IS()!=null)
        {

            if(ctx.expr(0)!=null)
            {

                expr_sql.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                expr_sql.setRight_expr(visitExpr(ctx.expr(1)));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_IS);
            // ex_res=expr_sql;
            ex_res.setExpr_sql(expr_sql);
        }

//| K_IS | K_IS K_NOT | K_IN | K_LIKE | K_GLOB | K_MATCH | K_REGEXP

        if(ctx.K_IN()!=null)
        {
            System.out.println("visit IN");
            List<Expr> list_expr = new ArrayList<>();
            //  System.out.println("visit IN");
            if(ctx.expr(0)!=null)
            {
                expr_sql.setLeft_expr(visitExpr(ctx.expr(0)));
            }
//            else if(ctx.expr(1)!=null)
//            {
//                list_expr.add(visitExpr(ctx.expr(1)));
//              //  expr_sql.setRight_expr(visitExpr(ctx.expr(1)));
//            }
            if(ctx.expr(1)!=null)
            {
                System.out.println("visit IN11111111111111111111111111111111111111111111111111111111111111111111");
            }
             m=1;
            while(ctx.expr(m)!=null)
            {
                list_expr.add(visitExpr(ctx.expr(m)));
                m++;
            }
            expr_sql.setListexpr(list_expr);
            System.out.println("visit IN11111111111111111111111111111111111111111111111111111111111111111111 "+list_expr.size());
            ///fac
            if(ctx.factored_select_stmt()!=null)
            {
                System.out.println("select***************");
                expr_sql.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
                System.out.println("select*************** "+expr_sql.getSelectStmt());
            }


            // System.out.println("visit IN");
            if(ctx.K_NOT()!=null)
            {
                expr_sql.setOp_sql(expr_sql.getOp_sql().K_NOT_K_IN);
            }
            else {
                expr_sql.setOp_sql(expr_sql.getOp_sql().K_IN);
            }
            ex_res.setExpr_sql(expr_sql);
        }

        if(ctx.K_LIKE()!=null)
        {
            if(ctx.expr(0)!=null)
            {

                expr_sql.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                expr_sql.setRight_expr(visitExpr(ctx.expr(1)));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_LIKE);
            // ex_res=expr_sql;
            ex_res.setExpr_sql(expr_sql);
        }

        if(ctx.K_GLOB()!=null)
        {
            if(ctx.expr(0)!=null)
            {

                expr_sql.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                expr_sql.setRight_expr(visitExpr(ctx.expr(1)));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_GLOB);
            // ex_res=expr_sql;
            ex_res.setExpr_sql(expr_sql);
        }

        if(ctx.K_MATCH()!=null)
        {
            if(ctx.expr(0)!=null)
            {

                expr_sql.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                expr_sql.setRight_expr(visitExpr(ctx.expr(1)));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_MATCH);
            // ex_res=expr_sql;
            ex_res.setExpr_sql(expr_sql);
        }
        if(ctx.K_REGEXP()!=null)
        {
            if(ctx.expr(0)!=null)
            {

                expr_sql.setLeft_expr(visitExpr(ctx.expr(0)));
            }

            if(ctx.expr(1)!=null)
            {
                expr_sql.setRight_expr(visitExpr(ctx.expr(1)));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_REGEXP);
            //  ex_res=expr_sql;
            ex_res.setExpr_sql(expr_sql);
        }

        //System.out.println(ex.getLeft_exp());
        //System.out.println(ex.getRight_exp());
        //System.out.println(ex.getOP());
        System.out.println("****************************************************");
        System.out.println("**************************************************** "+ex_res.getValue());
        return ex_res;
    }

    @Override
    public Expr visitExpr_test(SQLParser.Expr_testContext ctx) {
        System.out.println("visitExpr_test");
        MathimaticalExpr me = new MathimaticalExpr();
        LogicalExpr le = new LogicalExpr();
        Expr ex_res = new Expr();
        ex_res.setLine(ctx.getStart().getLine());
        ex_res.setCol(ctx.getStart().getCharPositionInLine());

        Binary_Expr bi = new Binary_Expr();
        UnaryExpr unaryExpr = new UnaryExpr();
        expr_sql expr_sql = new expr_sql();
        List<Call_func> call_funcs = new ArrayList<>();
        Bollean boll = new Bollean();
        if (ctx.literal_value() != null) {
            ex_res.setValue(ctx.literal_value().getText());
            if (ctx.literal_value().NUMERIC_LITERAL() != null) {
                this.expr_type = Type.NUMBER_CONST;
            }
            if (ctx.literal_value().STRING_LITERAL() != null) {
                this.expr_type = Type.STRING_CONST;
            }

        }
        Scope currentScope = Main.parent_stack.peek();

        if(ctx.IDENTIFIER()!=null)
        {
            Symbol symbol =  currentScope.getSymbolMap().get(ctx.IDENTIFIER().getText());
            if(symbol!=null)
            {
               if(symbol.getType()!=null)
               {
                   System.out.println("scop var : "+symbol.getScope().getId());
                   ex_res.setSymbol(symbol);
                   System.out.println("tabel symbol : "+ ex_res.getSymbol().getName());
               }
               else {
                   System.err.println("Warring for using anassigned variable "+ ctx.IDENTIFIER().getText()+"the colume is "+ ex_res.getCol() + "   the line is :  " + ex_res.getLine());

               }

            }else {
                boolean found = false;
                Scope parent = currentScope.getParent();
                while (parent != null) {
                    Symbol s = parent.getSymbolMap().get(ctx.IDENTIFIER().getText());
                    if (s != null) {
                        if(s.getType()==null)
                        {
                            System.err.println("Warring for using anassigned variable "+ ctx.IDENTIFIER().getText()+"the colume is "+ ex_res.getCol() + "   the line is :  " + ex_res.getLine());

                        }
                        found = true;
                        break;
                    }
                    parent = parent.getParent();
                }
                if (found == false) {
                    System.err.println("Error for using undeclared variable " + ctx.IDENTIFIER().getText() + "  the colume is : " + ex_res.getCol() + "   the line is :  " + ex_res.getLine());
                }
            }
            ex_res.setValue(ctx.IDENTIFIER().getText());
        }
        if (ctx.call_func() != null) {
            call_funcs.add(visitCall_func(ctx.call_func()));
            ex_res.setCall_funcs(call_funcs);
        }
        if (ctx.get_property_json() != null) {
            ex_res.setGetPropertyJsons(visitGet_property_json(ctx.get_property_json()));
        }
        if (ctx.table_name() != null) {
            ex_res.setTablename(visitTable_name(ctx.table_name()));
        }
        if (ctx.database_name() != null) {
            ex_res.setDatabasename(visitDatabase_name(ctx.database_name()));
        }
        if (ctx.bool_ex() != null) {
            ex_res.setBool_expr(visitBool_ex(ctx.bool_ex()));
        }
        if (ctx.OPEN_PAR() != null && ctx.CLOSE_PAR() != null) {
            if (ctx.factored_select_stmt() != null) {
                ex_res.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
            } else {
                ex_res.setExprwithbrackets(visitExpr_test(ctx.expr_test(0)));
            }
        }

        if (ctx.array_assigne() != null) {
            ex_res.setDeclareArray(visitArray_assigne(ctx.array_assigne()));
        }
        if (ctx.expr_test(0) != null && ctx.expr_test(0).array_assigne() != null) {
            ex_res.setDeclareArray(visitArray_assigne(ctx.expr_test(0).array_assigne()));
        }
        if (ctx.expr_test(1) != null && ctx.expr_test(1).array_assigne() != null) {
            ex_res.setDeclareArray(visitArray_assigne(ctx.expr_test(1).array_assigne()));
        }
        if (ctx.unary_operator() != null) {
            System.out.println("unaryoperator");
            if (ctx.unary_operator().PLUS() != null) {
                unaryExpr.setTerm(visitExpr_test(ctx.expr_test(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().PLUS);
            }
            if (ctx.unary_operator().MINUS() != null) {
                unaryExpr.setTerm(visitExpr_test(ctx.expr_test(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().MINUS);

            }

            ex_res.setUnaryExpr(unaryExpr);
        }
        if (ctx.expr_test(0) != null && ctx.expr_test(0).unary_operator() != null) {
            System.out.println("unaryoperator");
            if (ctx.expr_test(0).unary_operator().PLUS() != null) {
                unaryExpr.setTerm(visitExpr_test(ctx.expr_test(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().PLUS);
            }
            if (ctx.expr_test(0).unary_operator().MINUS() != null) {
                unaryExpr.setTerm(visitExpr_test(ctx.expr_test(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().MINUS);

            }

            ex_res.setUnaryExpr(unaryExpr);
        }
        if (ctx.expr_test(1) != null && ctx.expr_test(1).unary_operator() != null) {
            System.out.println("unaryoperator");
            if (ctx.expr_test(1).unary_operator().PLUS() != null) {
                unaryExpr.setTerm(visitExpr_test(ctx.expr_test(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().PLUS);
            }
            if (ctx.expr_test(1).unary_operator().MINUS() != null) {
                unaryExpr.setTerm(visitExpr_test(ctx.expr_test(0)));
                unaryExpr.setUnary(unaryExpr.getUnary().MINUS);

            }

            ex_res.setUnaryExpr(unaryExpr);
        }
        if (ctx.incr_decr_without_comma() != null) {
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
        }
        if (ctx.expr_test(0) != null && ctx.expr_test(0).incr_decr_without_comma() != null) {
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.expr_test(0).incr_decr_without_comma()));
        }
        if (ctx.expr_test(1) != null && ctx.expr_test(1).incr_decr_without_comma() != null) {
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.expr_test(1).incr_decr_without_comma()));
        }
        if (ctx.ASSIGN() != null) {
            System.out.println("ASSIGN");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.ASSIGN().getText());
            System.out.println(ctx.expr_test(1).getText());
            if (ctx.expr_test(0) != null) {
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }
            if (ctx.expr_test(1) != null) {
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));

            }
            me.setMath(me.getMath().ASSIGN);
            ex_res = me;

        }
        if (ctx.STAR() != null) {
            System.out.println("Star");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.STAR().getText());
            System.out.println(ctx.expr_test(1).getText());
            if (ctx.expr_test(0) != null) {
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
                System.out.println("valye----" + me.getLeft_expr().getValue());
            }
            if (ctx.expr_test(1) != null) {
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                System.out.println("valye----" + me.getRight_expr().getValue());
            }
            me.setMath(me.getMath().STAR);
            ex_res = me;

        }
        if (ctx.PLUS() != null) {
            System.out.println("Plus");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.PLUS().getText());
            System.out.println(ctx.expr_test(1).getText());
            if (ctx.expr_test(0) != null) {
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }
            if (ctx.expr_test(1) != null) {
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
            }
            me.setMath(me.getMath().PLUS);
            ex_res = me;
        }
        if (ctx.MINUS() != null) {
            System.out.println("Minus");

            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.MINUS().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }


            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MINUS);
            ex_res = me;


        }
        if (ctx.DIV() != null) {
            System.out.println("DIV");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.DIV().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().DIV);
            ex_res = me;
        }
        if (ctx.MOD() != null) {
            System.out.println("MOD");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.MOD().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }


            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MOD);
            ex_res = me;

        }
        if (ctx.EQ() != null) {
            System.out.println("EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }


            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().EQ);
            ex_res = le;
        }
        if (ctx.GT() != null) {
            System.out.println("GT");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.GT().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }


            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().GT);
            ex_res = le;

        }
        if (ctx.GT_EQ() != null) {
            System.out.println("GT_EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.GT_EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                // System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }


            if (ctx.expr_test(1) != null) {

                // System.out.println(ctx.expr(1).getChildCount());
                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().GT_EQ);
            ex_res = le;

        }
        if (ctx.LT() != null) {
            System.out.println("LT");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.LT().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().LT);
            ex_res = le;


        }
        if (ctx.LT_EQ() != null) {
            System.out.println("LT_EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.LT_EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().LT_EQ);
            ex_res = le;
        }
        if (ctx.NOT_EQ1() != null) {
            System.out.println("NOT_EQ1");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.NOT_EQ1().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().NOT_EQ1);
            ex_res = le;
        }
        if (ctx.NOT_EQ2() != null) {
            System.out.println("NOT_EQ1");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.NOT_EQ1().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().NOT_EQ2);
            ex_res = le;

        }
        if (ctx.K_AND() != null) {
            System.out.println("K_AND");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.K_AND().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().K_AND);
            ex_res = le;

        }
        if (ctx.K_OR() != null) {
            System.out.println("K_OR");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.K_OR().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                le.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                le.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            le.setlogic(le.getlogic().K_OR);
            ex_res = le;

        }
        if (ctx.PLUS_EQ() != null) {
            System.out.println("PLUS_EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.PLUS_EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
            }
            me.setMath(me.getMath().PLUS_EQ);
            ex_res = me;
        }
        if (ctx.MINUS_EQ() != null) {
            System.out.println("MINUS_EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.MINUS_EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MINUS_EQ);
            ex_res = me;
        }
        if (ctx.STAR_EQ() != null) {
            System.out.println("STAR_EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.STAR_EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().STAR_EQ);
            ex_res = me;
        }
        if (ctx.DIV_EQ() != null) {
            System.out.println("DIV_EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.DIV_EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().DIV_EQ);

            ex_res = me;
        }
        if (ctx.MOD_EQ() != null) {
            System.out.println("MOD_EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.MOD_EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {
                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MOD_EQ);
            ex_res = me;
        }
        if (ctx.MOD_EQ() != null) {
            System.out.println("MOD_EQ");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.MOD_EQ().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {
                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_test(ctx.expr_test(1)));
                //visitExpr(ctx.expr(1));
            }
            me.setMath(me.getMath().MOD_EQ);
            ex_res = me;
        }
        if (ctx.AMP() != null) {
            System.out.println("AMP");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.AMP().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                bi.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {
                bi.setRight_expr(visitExpr_test(ctx.expr_test(1)));
            }
            bi.setLogic(bi.getLogic().AMP);
            ex_res = bi;
        }
        if (ctx.LT2() != null) {
            System.out.println("LT2");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.LT2().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                bi.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {
                bi.setRight_expr(visitExpr_test(ctx.expr_test(1)));
            }
            bi.setLogic(bi.getLogic().LT2);
            ex_res = bi;
        }
        if (ctx.GT2() != null) {
            System.out.println("GT2");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.GT2().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                bi.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {
                bi.setRight_expr(visitExpr_test(ctx.expr_test(1)));
            }
            bi.setLogic(bi.getLogic().GT2);
            ex_res = bi;
        }
        if (ctx.PIPE() != null) {
            System.out.println("PIPE");
            System.out.println(ctx.expr_test(0).getText());
            System.out.println(ctx.PIPE().getText());
            System.out.println(ctx.expr_test(1).getText());

            if (ctx.expr_test(0) != null) {

                bi.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {
                bi.setRight_expr(visitExpr_test(ctx.expr_test(1)));
            }
            bi.setLogic(bi.getLogic().PIPE);
            ex_res = bi;
        }
        if (ctx.K_IS() != null) {

            if (ctx.expr_test(0) != null) {

                expr_sql.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {
                expr_sql.setRight_expr(visitExpr_test(ctx.expr_test(1)));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_IS);
            ex_res = bi;
        }
//| K_IS | K_IS K_NOT | K_IN | K_LIKE | K_GLOB | K_MATCH | K_REGEXP
        if (ctx.K_IN() != null) {
            if (ctx.expr_test(0) != null) {

                expr_sql.setLeft_expr(visitExpr_test((ctx.expr_test(0))));
            }

            if (ctx.expr_test(1) != null) {
                expr_sql.setRight_expr(visitExpr_test((ctx.expr_test(1))));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_IN);
            ex_res = expr_sql;
        }
        if (ctx.K_LIKE() != null) {
            if (ctx.expr_test(0) != null) {

                expr_sql.setLeft_expr(visitExpr_test((ctx.expr_test(0))));
            }

            if (ctx.expr_test(1) != null) {
                expr_sql.setRight_expr(visitExpr_test((ctx.expr_test(1))));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_LIKE);
            ex_res = expr_sql;
        }
        if (ctx.K_GLOB() != null) {
            if (ctx.expr_test(0) != null) {

                expr_sql.setLeft_expr(visitExpr_test((ctx.expr_test(0))));
            }

            if (ctx.expr_test(1) != null) {
                expr_sql.setRight_expr(visitExpr_test((ctx.expr_test(1))));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_GLOB);
            ex_res = expr_sql;
        }
        if (ctx.K_MATCH() != null) {
            if (ctx.expr_test(0) != null) {

                expr_sql.setLeft_expr(visitExpr_test((ctx.expr_test(0))));
            }

            if (ctx.expr_test(1) != null) {
                expr_sql.setRight_expr(visitExpr_test((ctx.expr_test(1))));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_MATCH);
            ex_res = expr_sql;
        }
        if (ctx.K_REGEXP() != null) {
            if (ctx.expr_test(0) != null) {

                expr_sql.setLeft_expr(visitExpr_test(ctx.expr_test(0)));
            }

            if (ctx.expr_test(1) != null) {
                expr_sql.setRight_expr(visitExpr_test((ctx.expr_test(1))));
            }
            expr_sql.setOp_sql(expr_sql.getOp_sql().K_REGEXP);
            ex_res = expr_sql;
        }

        return ex_res;
    }


//    @Override public bodyWithoutBrackets visitBody_without_bracketa(SQLParser.Body_without_bracketaContext ctx) {
//        bodyWithoutBrackets body = new bodyWithoutBrackets();
//        ForEach forEach = new ForEach();
//
//        if(ctx.foreach()!=null)
//        {
//            body.setForEach(visitForeach(ctx.foreach()));
//        }
//
//        return body;
//
//    }

    @Override
    public DeclareArray visitDeclare_array(SQLParser.Declare_arrayContext ctx) {
        System.out.println("visitDeclareArray");
        DeclareArray arr = new DeclareArray();
        arr.setLine(ctx.getStart().getLine());
        arr.setCol(ctx.getStart().getCharPositionInLine());

        arr.setArrayName(ctx.IDENTIFIER().getText());
        if (ctx.expr_var(0) != null) {
            arr.setArrayindex1(visitExpr_var(ctx.expr_var(0)));
        }
        System.out.println("the ArrayName : " + arr.getArrayName());
        if (ctx.expr_var(1) != null) {
            arr.setArrayindex2(visitExpr_var(ctx.expr_var(1)));
        }
        //

        return arr;

    }

    @Override
    public GetPropertyJson visitGet_property_json(SQLParser.Get_property_jsonContext ctx) {
        GetPropertyJson getPropertyJson = new GetPropertyJson();
        getPropertyJson.setLine(ctx.getStart().getLine());
        getPropertyJson.setCol(ctx.getStart().getCharPositionInLine());

        System.out.println("visitGetPropertyJson");
        for (int i = 0; i < ctx.json_object_name().size(); i++) {
            getPropertyJson.getVars().add(ctx.json_object_name(i).IDENTIFIER().getText());
        }
        return getPropertyJson;
    }

    @Override
    public AssigneArray visitDeclare_array_vr(SQLParser.Declare_array_vrContext ctx) {
        System.out.println("visitDeclare_array_vr");
        List<ElementArray> elementArrays = new ArrayList<>();
        AssigneArray assigneArray = new AssigneArray();
        assigneArray.setLine(ctx.getStart().getLine());
        assigneArray.setCol(ctx.getStart().getCharPositionInLine());

        assigneArray.setArr(ctx.IDENTIFIER().getText());

        if (ctx.element_arr() != null) {
            for (int i = 0; i < ctx.element_arr().size(); i++) {

                elementArrays.add(visitElement_arr(ctx.element_arr(i)));
            }
            assigneArray.setElementlist(elementArrays);
        }
        if (ctx.element_arr_2() != null) {
            for (int i = 0; i < ctx.element_arr_2().size(); i++) {
                elementArrays = visitElement_arr_2(ctx.element_arr_2(i));
            }
            assigneArray.setElementlist(elementArrays);
        }
        return assigneArray;
    }

    @Override
    public ElementArray visitElement_arr(SQLParser.Element_arrContext ctx) {
        System.out.println("visitElementarray");
        ElementArray elementArray = new ElementArray();
        elementArray.setLine(ctx.getStart().getLine());
        elementArray.setCol(ctx.getStart().getCharPositionInLine());

        elementArray.setElement(visitExpr_var(ctx.expr_var()));
        return elementArray;
    }

    @Override
    public List<ElementArray> visitElement_arr_2(SQLParser.Element_arr_2Context ctx) {
        System.out.println("visitElementarray2");
        List<ElementArray> elementArray = new ArrayList<>();

        for (int i = 0; i < ctx.element_arr().size(); i++) {
            elementArray.add(visitElement_arr(ctx.element_arr().get(i)));
        }

        return elementArray;

    }


    //sumayaa

    //For
    @Override
    public forDecleration visitFor_stmt(SQLParser.For_stmtContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("VisitfFor_stmt");
        Scope ForScope = new Scope();
        ForScope.setId("for_scope");
        ForScope.setParent(parent);
        Main.symbolTable.addScope(ForScope);
        Main.parent_stack.push(ForScope);

        forDecleration for_stmt = new forDecleration();
        for_stmt.setLine(ctx.getStart().getLine());
        for_stmt.setCol(ctx.getStart().getCharPositionInLine());

        if(ctx.declare_var_for() !=null)
        {
            for_stmt.setFor_var(visitDeclare_var_for(ctx.declare_var_for()));
        }
        if(ctx.for_condition()!=null)
        {
            for_stmt.setFor_con(visitFor_condition(ctx.for_condition()));
        }
        if(ctx.for_process_var() !=null)
        {
            for_stmt.setFor_process(visitFor_process_var(ctx.for_process_var()));
        }
        if(ctx.body() !=null)
        {
            for_stmt.setBody(visitBody(ctx.body()));
        }
        if(ctx.body_without_bracketa()!=null)
        {
            for_stmt.setBodyFunctionWilthoutBracket(visitBody_without_bracketa(ctx.body_without_bracketa()));
        }
        for_stmt.setScope(ForScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +ForScope.getParent().getId() );
        for (String key : ForScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return for_stmt;
    }


    @Override
    public Function_name visitFunction_name(SQLParser.Function_nameContext ctx) {
        System.out.println("visit FunctionName");
        Function_name functionName = new Function_name();
        functionName.setLine(ctx.getStart().getLine());
        functionName.setCol(ctx.getStart().getCharPositionInLine());

        functionName.setAny_name(visitAny_name(ctx.any_name()));

        return functionName;
    }
//    declare_var_for:
//            ((K_VAR)? IDENTIFIER ('=')(expr_var) )?
//    ;
    @Override
    public DeclareForVar visitDeclare_var_for(SQLParser.Declare_var_forContext ctx) {
        DeclareForVar var_for = new DeclareForVar();
        var_for.setLine(ctx.getStart().getLine());
        var_for.setCol(ctx.getStart().getCharPositionInLine());

        //  Scope currentScope = Main.symbolTable.getScopes().get(Main.symbolTable.getScopes().size() - 1);
        Scope currentScope =   Main.parent_stack.peek();
        Type type = new Type();
        String var_name=ctx.IDENTIFIER().getText();
        var_for.setForVarname(var_name);
        if(ctx.expr_var()!=null) {
            var_for.setValu(visitExpr_var(ctx.expr_var()));
        }
        type.setName(this.expr_type);
        if(ctx.K_VAR()!= null){
            if(currentScope.getSymbolMap().get(var_name)!=null)
            {
                System.err.println("Error in multiple declaration : a variable "+var_name+" should be declared in the same scope at most once "+"the colume is : "+ var_for.getCol()+"   the line is :  " +var_for.getLine());
            }
            else {
                Symbol varSymbol = new Symbol();
                //set Identefier
                varSymbol.setType(type);
                varSymbol.setIsParam(false);
                varSymbol.setName(var_name);
                varSymbol.setScope(currentScope);
                currentScope.addSymbol(var_name,varSymbol);
                System.out.println("var type : "+varSymbol.getType().getName());
                System.out.println("scop var : "+varSymbol.getScope().getId());
                var_for.setSymbol(varSymbol);
                System.out.println("tabel symbol : "+ var_for.getSymbol().getName());
            }


        }
        else {
            Symbol symbol =  currentScope.getSymbolMap().get(var_name);
            if(symbol!=null)
            {
                currentScope.getSymbolMap().get(var_name).setType(type);
                System.out.println("var type : "+symbol.getType().getName());
                System.out.println("scop var : "+symbol.getScope().getId());
                var_for.setSymbol(symbol);
                System.out.println("tabel symbol : "+ var_for.getSymbol().getName());
            }else
            {
                Scope currentparent = Main.parent_stack.peek();
                System.out.println("currentscope "+ currentparent.getId());
                boolean found= false;
                Scope parent =currentparent.getParent();
                while(parent!=null){
                    Symbol s = parent.getSymbolMap().get(var_name);
                    if(s!=null){
                        found= true;
                        if(s.getType()==null)
                        {
                            type.setName(this.expr_type);
                            s.setType(type);
                            System.out.println("55555555555555555555"+s.getType().getName());
                        }
                        else {
                            if(s.getType().getName()!=type.getName())
                            {
                                System.err.println("Error for assigne another type for declared variable "+var_name+"the colume is : "+ var_for.getCol()+"   the line is :  " +var_for.getLine());

                            }
                        }

                        break;
                    }
                    parent=parent.getParent();
                }
                if(found==false)
                {
                    System.out.println("Error for using undeclared variable "+var_name +"the colume is : "+ var_for.getCol()+"   the line is :  " +var_for.getLine());

                }
                // System.out.println("Error for using undeclared variable the colume is : "+ svar.getCol()+"   the line is :  " +svar.getLine());
            }

        }

        return var_for;
    }

    @Override
    public ForCondition visitFor_condition(SQLParser.For_conditionContext ctx) {
        System.out.println("VisitFor_condition");
        ForCondition for_con = new ForCondition();
        for_con.setLine(ctx.getStart().getLine());
        for_con.setCol(ctx.getStart().getCharPositionInLine());

        LogicalExpr le = new LogicalExpr();
        Expr ex_res = new Expr();


        if (ctx.EQ() != null) {
            System.out.println("EQ");
            if (ctx.expr_var(0) != null) {
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }


            if (ctx.expr_var(1) != null) {
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }


            le.setlogic(le.getlogic().EQ);
            ex_res = le;
        }

        if (ctx.GT() != null) {
            System.out.println("GT");
            if (ctx.expr_var(0) != null) {
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }


            if (ctx.expr_var(1) != null) {
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }

            le.setlogic(le.getlogic().GT);
            ex_res = le;
        }

        if (ctx.GT_EQ() != null) {
            System.out.println("GT_EQ");
            if (ctx.expr_var(0) != null) {
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }


            if (ctx.expr_var(1) != null) {
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            le.setlogic(le.getlogic().GT_EQ);
            ex_res = le;
        }


        if (ctx.LT() != null) {
            System.out.println("LT");
            if (ctx.expr_var(0) != null) {
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }


            if (ctx.expr_var(1) != null) {
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }

            le.setlogic(le.getlogic().LT);
            ex_res = le;
        }

        if (ctx.LT_EQ() != null) {
            System.out.println("LT_EQ");

            if (ctx.expr_var(0) != null) {
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }


            if (ctx.expr_var(1) != null) {
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }


            le.setlogic(le.getlogic().LT_EQ);
            ex_res = le;
        }

        if (ctx.NOT_EQ1() != null) {
            System.out.println("NOT_EQ1");
            System.out.println(ctx.expr_var(0).getText());

            if (ctx.expr_var(0) != null) {
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }


            if (ctx.expr_var(1) != null) {
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }

            le.setlogic(le.getlogic().NOT_EQ1);
            ex_res = le;
        }


        if (ctx.ASSIGN() != null) {
            System.out.println("ASSIGN");
            if (ctx.expr_var(0) != null) {
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }


            if (ctx.expr_var(1) != null) {
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            ex_res = le;
        }


        for_con.setExp(ex_res);

        return for_con;

    }

    @Override
    public forProcessVar visitFor_process_var(SQLParser.For_process_varContext ctx) {
        System.out.println("VisitFor_process_var");
        forProcessVar for_process = new forProcessVar();
        for_process.setLine(ctx.getStart().getLine());
        for_process.setCol(ctx.getStart().getCharPositionInLine());


        if (ctx.exp_for() != null) {
            System.out.println("Process var :" + ctx.exp_for().getText());
            for_process.setExp_for(visitExp_for(ctx.exp_for()));
        }

        if (ctx.incr_decr_without_comma() != null) {
            System.out.println("Process var :" + ctx.incr_decr_without_comma().getText());
            for_process.setInc_dec(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
        }


        return for_process;
    }

    //visit exprfor

    @Override
    public expFor visitExp_for(SQLParser.Exp_forContext ctx) {
        System.out.println("visitExpr_fOR");
        expFor exp_for = new expFor();

        exp_for.setLine(ctx.getStart().getLine());
        exp_for.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.ASSIGN() != null) {
            System.out.println("ASSIGN");
            System.out.println(ctx.IDENTIFIER().getText());
            exp_for.setIdentifier(ctx.IDENTIFIER().getText());
            System.out.println(ctx.ASSIGN().getText());
            exp_for.setOP(ctx.ASSIGN().getText());
            if (ctx.literal_value() != null) {
                System.out.println("literal value :" + ctx.literal_value().getText());
                exp_for.setLiteral_value(visitLiteral_value(ctx.literal_value()));
            }
            if (ctx.incr_decr_without_comma() != null) {
                System.out.println("incr decr without comma :" + ctx.incr_decr_without_comma().getText());
                exp_for.setInc_dec(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
            }
            if (ctx.exp_i() != null) {
                System.out.println("exp i :" + ctx.exp_i().getText());
                exp_for.setExpI(visitExp_i(ctx.exp_i()));

            }
        }

        if (ctx.PLUS_EQ() != null) {
            System.out.println("PLUS_EQ");
            System.out.println(ctx.IDENTIFIER().getText());
            exp_for.setIdentifier(ctx.IDENTIFIER().getText());
            System.out.println(ctx.PLUS_EQ().getText());
            exp_for.setOP(ctx.PLUS_EQ().getText());
            if (ctx.literal_value() != null) {
                System.out.println("literal value :" + ctx.literal_value().getText());
                exp_for.setLiteral_value(visitLiteral_value(ctx.literal_value()));
            }
            if (ctx.incr_decr_without_comma() != null) {
                System.out.println("incr decr without comma :" + ctx.incr_decr_without_comma().getText());
                exp_for.setInc_dec(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
            }
            if (ctx.exp_i() != null) {
                System.out.println("exp i :" + ctx.exp_i().getText());
                exp_for.setExpI(visitExp_i(ctx.exp_i()));

            }
        }

        if (ctx.MINUS_EQ() != null) {
            System.out.println("MINUS_EQ");
            System.out.println(ctx.IDENTIFIER().getText());
            exp_for.setIdentifier(ctx.IDENTIFIER().getText());
            System.out.println(ctx.MINUS_EQ().getText());
            exp_for.setOP(ctx.MINUS_EQ().getText());
            if (ctx.literal_value() != null) {
                System.out.println("literal value :" + ctx.literal_value().getText());
                exp_for.setLiteral_value(visitLiteral_value(ctx.literal_value()));
            }
            if (ctx.incr_decr_without_comma() != null) {
                System.out.println("incr decr without comma :" + ctx.incr_decr_without_comma().getText());
                exp_for.setInc_dec(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
            }
            if (ctx.exp_i() != null) {
                System.out.println("exp i :" + ctx.exp_i().getText());
                exp_for.setExpI(visitExp_i(ctx.exp_i()));

            }
        }

        if (ctx.STAR_EQ() != null) {
            System.out.println("STAR_EQ");
            System.out.println(ctx.IDENTIFIER().getText());
            exp_for.setIdentifier(ctx.IDENTIFIER().getText());
            System.out.println(ctx.STAR_EQ().getText());
            exp_for.setOP(ctx.STAR_EQ().getText());
            if (ctx.literal_value() != null) {
                System.out.println("literal value :" + ctx.literal_value().getText());
                exp_for.setLiteral_value(visitLiteral_value(ctx.literal_value()));
            }
            if (ctx.incr_decr_without_comma() != null) {
                System.out.println("incr decr without comma :" + ctx.incr_decr_without_comma().getText());
                exp_for.setInc_dec(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
            }
            if (ctx.exp_i() != null) {
                System.out.println("exp i :" + ctx.exp_i().getText());
                exp_for.setExpI(visitExp_i(ctx.exp_i()));

            }
        }

        if (ctx.DIV_EQ() != null) {
            System.out.println("DIV_EQ");
            System.out.println(ctx.IDENTIFIER().getText());
            exp_for.setIdentifier(ctx.IDENTIFIER().getText());
            System.out.println(ctx.DIV_EQ().getText());
            exp_for.setOP(ctx.DIV_EQ().getText());
            if (ctx.literal_value() != null) {
                System.out.println("literal value :" + ctx.literal_value().getText());
                exp_for.setLiteral_value(visitLiteral_value(ctx.literal_value()));
            }
            if (ctx.incr_decr_without_comma() != null) {
                System.out.println("incr decr without comma :" + ctx.incr_decr_without_comma().getText());
                exp_for.setInc_dec(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
            }
            if (ctx.exp_i() != null) {
                System.out.println("exp i :" + ctx.exp_i().getText());
                exp_for.setExpI(visitExp_i(ctx.exp_i()));

            }

        }

        return exp_for;
    }

    @Override
    public exp_i visitExp_i(SQLParser.Exp_iContext ctx) {
        System.out.println("visit_exp_i ");
        exp_i expi = new exp_i();
        expi.setLine(ctx.getStart().getLine());
        expi.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.literal_value() != null) {
            for (int i = 0; i < ctx.literal_value().size(); i++) {
                System.out.println("literal_val : " + ctx.literal_value().get(i).getText());
                expi.setLiteral_value(visitLiteral_value(ctx.literal_value().get(i)));
            }
        }
        if (ctx.incr_decr_without_comma() != null) {
            for (int i = 0; i < ctx.incr_decr_without_comma().size(); i++) {
                System.out.println("incr_decr_without_comma : " + ctx.incr_decr_without_comma().get(i).getText());
                expi.setInc_dec(visitIncr_decr_without_comma(ctx.incr_decr_without_comma().get(i)));
            }
        }
        List<exp_i> ex_i = new ArrayList<>();
        if (ctx.exp_i() != null) {
            for (int i = 0; i < ctx.exp_i().size(); i++) {
                System.out.println("exp_i : " + ctx.exp_i().get(i).getText());
                ex_i.add(visitExp_i(ctx.exp_i().get(i)));
            }
            expi.setExpI(ex_i);
        }
        // return ex_res;
        return expi;
    }


    @Override
    public String visitLiteral_value(SQLParser.Literal_valueContext ctx) {
        System.out.println("visit_literal_value ");
        String literal;

        literal = ctx.getText();
        return literal;
    }


    @Override
    public incrDecrWithoutComma visitIncr_decr_without_comma(SQLParser.Incr_decr_without_commaContext ctx) {
        System.out.println("visit_Incr_decr_without_comma ");
        incrDecrWithoutComma i=new incrDecrWithoutComma();
        i.setLine(ctx.getStart().getLine());
        i.setCol(ctx.getStart().getCharPositionInLine());

        List<DeclareArray>declareArrays=new ArrayList<>();
        Scope currentscope = Main.parent_stack.peek();
        System.out.println("currentscope "+ currentscope.getId());
        if(ctx.IDENTIFIER()!=null)
        {
            Symbol symbol =  currentscope.getSymbolMap().get(ctx.IDENTIFIER().getText());
            if(symbol!=null)
            {
                if(symbol.getType()!=null)
                {
                    System.out.println("var type : "+symbol.getType().getName());
                    System.out.println("scop var : "+symbol.getScope().getId());
                    i.setSymbol(symbol);
                    System.out.println("tabel symbol : "+ i.getSymbol().getName());
                }
                else {
                    System.err.println("Warring for using anassigned variable "+ ctx.IDENTIFIER().getText()+"the colume is "+ i.getCol() + "   the line is :  " + i.getLine());

                }

            }else {
                boolean found = false;
                Scope parent = currentscope.getParent();
                while (parent != null) {
                    Symbol s = parent.getSymbolMap().get(ctx.IDENTIFIER().getText());
                    if (s != null) {
                        if(s.getType()==null)
                        {
                            System.err.println("Warring for using anassigned variable "+ ctx.IDENTIFIER().getText()+"the colume is "+ i.getCol() + "   the line is :  " + i.getLine());

                        }
                        found = true;
                        break;
                    }
                    parent = parent.getParent();
                }
                if (found == false) {
                    System.err.println("Error for using undeclared variable " + ctx.IDENTIFIER().getText() + "  the colume is : " + i.getCol() + "   the line is :  " + i.getLine());
                }
            }
            i.setValue(ctx.IDENTIFIER().getText());
        }
        if(ctx.array_assigne()!=null)
        {
            //declareArrays.add(visitArray_assigne(ctx.array_assigne()));
            i.setDeclareArray(visitArray_assigne(ctx.array_assigne()));
        }
        if(ctx.get_property_json()!=null)
        {
            i.setGetPropertyJson(visitGet_property_json(ctx.get_property_json()));
        }
        if(ctx.PLUS_PLUSl()!=null)
        {
            i.setLogic(i.getLogic().PLUS_PLUSl);
        }
        if(ctx.PLUS_PLUSr()!=null)
        {
            i.setLogic(i.getLogic().PLUS_PLUSr);
        }
        if(ctx.MINUS_MINUSl()!=null)
        {
            i.setLogic(i.getLogic().PLUS_PLUSl);
        }
        if(ctx.PLUS_PLUSl()!=null)
        {
            i.setLogic(i.getLogic().PLUS_PLUSl);
        }
        return i;
    }


    //if
    @Override
    public IfDecleration visitIf_stmt(SQLParser.If_stmtContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("visitIf_stmt ");
        Scope ifScope = new Scope();
        ifScope.setId("ifScope");
        ifScope.setParent(parent);
        Main.symbolTable.addScope(ifScope);
        Main.parent_stack.push(ifScope);

        IfDecleration if_stmt = new IfDecleration();
        if_stmt.setLine(ctx.getStart().getLine());
        if_stmt.setCol(ctx.getStart().getCharPositionInLine());

        if(ctx.condition_header() !=null) {
            if_stmt.setIf_header(visitCondition_header(ctx.condition_header()));
        }
        if(ctx.body() !=null)
        {
            System.out.println("body :"+ctx.body().getText());
            if_stmt.setBody(visitBody(ctx.body()));
        }
        if(ctx.body_without_bracketa()!=null)
        {
            if_stmt.setBodyFunctionWilthoutBracket(visitBody_without_bracketa(ctx.body_without_bracketa()));
        }
        if_stmt.setScope(ifScope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +ifScope.getParent().getId() );
        for (String key : ifScope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        //else if
        if(ctx.else_if() !=null)
        {
            List<elseIf> else_if=new ArrayList<>();
            for(int i=0;i<ctx.else_if().size();i++)
            {
                System.out.println("else if:"+ctx.else_if().get(i).getText());
                else_if.add(visitElse_if(ctx.else_if().get(i)));
            }
            if_stmt.setElseIf(else_if);
        }
        //else
        if(ctx.else_stmt() !=null)
        {
            System.out.println("else :"+ctx.else_stmt().getText());
            if_stmt.setEls(visitElse_stmt(ctx.else_stmt()));

        }
        return if_stmt;
    }

    @Override
    public elseIf visitElse_if(SQLParser.Else_ifContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("visitElse_if");
        Scope else_if_Scope = new Scope();
        else_if_Scope.setId("else_if_Scope");
        else_if_Scope.setParent(parent);
        Main.symbolTable.addScope(else_if_Scope);
        Main.parent_stack.push(else_if_Scope);

        elseIf else_if = new elseIf();
        else_if.setLine(ctx.getStart().getLine());
        else_if.setCol(ctx.getStart().getCharPositionInLine());

        if(ctx.condition_header() !=null) {
            else_if.setElse_if_header(visitCondition_header(ctx.condition_header()));
        }
        if(ctx.body() !=null)
        {
            System.out.println("body else if:"+ctx.body().getText());
            else_if.setBody(visitBody(ctx.body()));
        }
        if(ctx.body_without_bracketa()!=null)
        {
            else_if.setBodyFunctionWilthoutBracket(visitBody_without_bracketa(ctx.body_without_bracketa()));
        }
        else_if.setScope(else_if_Scope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +else_if_Scope.getParent().getId() );
        for (String key : else_if_Scope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();
        return else_if;

    }

    @Override
    public Else visitElse_stmt(SQLParser.Else_stmtContext ctx) {
        Scope parent =  Main.parent_stack.peek();
        System.out.println("visit_Else");
        Scope else_Scope = new Scope();
        else_Scope.setId("else_Scope");
        else_Scope.setParent(parent);
        Main.symbolTable.addScope(else_Scope);
        Main.parent_stack.push(else_Scope);

        Else el = new Else();
        el.setLine(ctx.getStart().getLine());
        el.setCol(ctx.getStart().getCharPositionInLine());

        if(ctx.body() !=null)
        {
            System.out.println("body else :"+ctx.body().getText());
            el.setBody(visitBody(ctx.body()));
        }
        if(ctx.body_without_bracketa()!=null)
        {
            el.setBodyFunctionWilthoutBracket(visitBody_without_bracketa(ctx.body_without_bracketa()));
        }
        el.setScope(else_Scope);

        System.out.println("Scope name : " +  Main.parent_stack.peek().getId());
        System.out.println("Scope parent : " +else_Scope.getParent().getId() );
        for (String key : else_Scope.getSymbolMap().keySet() ) {
            System.out.println(key);
        }
        Main.parent_stack.pop();

        return el;


    }

    @Override
    public condition_header visitCondition_header(SQLParser.Condition_headerContext ctx) {
        System.out.println("visit condition header");
        condition_header condition_header =new condition_header();
        condition_header.setLine(ctx.getStart().getLine());
        condition_header.setCol(ctx.getStart().getCharPositionInLine());

        LogicalExpr le = new LogicalExpr();
        Expr ex_res = new Expr();
        Scope currentscope =  Main.parent_stack.peek();
        System.out.println("currentscope  :"+currentscope.getId());
        if(ctx.IDENTIFIER()!=null)
        {
            Symbol symbol =  currentscope.getSymbolMap().get(ctx.IDENTIFIER().getText());
            if(symbol!=null)
            {
                if(symbol.getType()!=null)
                {
                    System.out.println("scop var : "+symbol.getScope().getId());
                    ex_res.setSymbol(symbol);
                    System.out.println("tabel symbol : "+ ex_res.getSymbol().getName());
                }
                else {
                    System.err.println("Warring for using anassigned variable "+ ctx.IDENTIFIER().getText()+"the colume is "+ condition_header.getCol() + "   the line is :  " + condition_header.getLine());
                }


            }else
            {
            System.out.println("IDENTIFIER");
            this.expr_type = Type.STRING_CONST;
            boolean found = false;
            Scope parent = currentscope.getParent();
            while (parent != null) {
                Symbol s = parent.getSymbolMap().get(ctx.IDENTIFIER().getText());
                if (s != null) {
                    found = true;
                    if(s.getType()== null)
                    {
                        System.err.println("Warring for using anassigned variable "+ ctx.IDENTIFIER().getText()+"the colume is "+ condition_header.getCol() + "   the line is :  " + condition_header.getLine());

                    }
                    break;
                }
                parent = parent.getParent();
            }
            if (found == false) {
                System.err.println("Error for using undeclared variable the colume is : " + condition_header.getCol() + "   the line is :  " + condition_header.getLine());
            }
        }
            ex_res.setValue(ctx.getText());
        }

        if(ctx.OPEN_PAR()!=null&&ctx.CLOSE_PAR()!=null)
        {
            condition_header.setCondition_header_brackets(visitCondition_header(ctx.condition_header(0)));
        }

        if(ctx.EQ()!=null)
        {
            System.out.println("EQ");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.EQ() .getText());
            System.out.println(ctx.expr_var(1).getText());
            if(ctx.expr_var(0)!=null)
            {
//                if(ctx.expr_var(0).IDENTIFIER()!=null){
//                    boolean found= false;
//                    Scope parent =currentscope.getParent();
//                    while(parent!=null){
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(0).getText());
//                        if(s!=null){
//                            found= true;
//                            break;
//                        }
//                        parent=parent.getParent();
//                    }
//                    if(found==false)
//                    {
//                        System.out.println("Error for using undeclared variable "+ctx.expr_var(0).getText() +" the colume is : "+ ex_res.getCol()+"   the line is :  " +ex_res.getLine());
//                    }
//                }

                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }
            if(ctx.expr_var(1)!=null)
            {
//                if(ctx.expr_var(1).IDENTIFIER()!=null) {
//                    boolean found = false;
//                    Scope parent = currentscope.getParent();
//                    while (parent != null) {
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(1).getText());
//                        if (s != null) {
//                            found = true;
//                            break;
//                        }
//                        parent = parent.getParent();
//                    }
//                    if (found == false) {
//                        System.out.println("Error for using undeclared variable" + ctx.expr_var(1).getText() + " the colume is : " + ex_res.getCol() + "   the line is :  " + ex_res.getLine());
//                    }
//                }
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            le.setlogic(le.getlogic().EQ);
            ex_res=le;
        }
        if(ctx.GT()!=null)
        {
            System.out.println("GT");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.GT() .getText());
            System.out.println(ctx.expr_var(1).getText());

            if(ctx.expr_var(0)!=null)
            {
//                if(ctx.expr_var(0).IDENTIFIER()!=null){
//                    boolean found= false;
//                    Scope parent =currentscope.getParent();
//                    while(parent!=null){
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(0).getText());
//                        if(s!=null){
//                            found= true;
//                            break;
//                        }
//                        parent=parent.getParent();
//                    }
//                    if(found==false)
//                    {
//                        System.out.println("Error for using undeclared variable "+ctx.expr_var(0).getText() +" the colume is : "+ ex_res.getCol()+"   the line is :  " +ex_res.getLine());
//                    }
//                }
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }

            if(ctx.expr_var(1)!=null)
            {
//                if(ctx.expr_var(1).IDENTIFIER()!=null) {
//                    boolean found = false;
//                    Scope parent = currentscope.getParent();
//                    while (parent != null) {
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(1).getText());
//                        if (s != null) {
//                            found = true;
//                            break;
//                        }
//                        parent = parent.getParent();
//                    }
//                    if (found == false) {
//                        System.out.println("Error for using undeclared variable" + ctx.expr_var(1).getText() + " the colume is : " + ex_res.getCol() + "   the line is :  " + ex_res.getLine());
//                    }
//                }
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            le.setlogic(le.getlogic().GT);
            ex_res=le;

        }
        if(ctx.GT_EQ()!=null)
        {
            System.out.println("GT_EQ");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.GT_EQ() .getText());
            System.out.println(ctx.expr_var(1).getText());

            if(ctx.expr_var(0)!=null)
            {
//                if(ctx.expr_var(0).IDENTIFIER()!=null){
//                    boolean found= false;
//                    Scope parent =currentscope.getParent();
//                    while(parent!=null){
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(0).getText());
//                        if(s!=null){
//                            found= true;
//                            break;
//                        }
//                        parent=parent.getParent();
//                    }
//                    if(found==false)
//                    {
//                        System.out.println("Error for using undeclared variable "+ctx.expr_var(0).getText() +" the colume is : "+ ex_res.getCol()+"   the line is :  " +ex_res.getLine());
//                    }
//                }
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }

            if(ctx.expr_var(1)!=null)
            {
//                if(ctx.expr_var(1).IDENTIFIER()!=null) {
//                    boolean found = false;
//                    Scope parent = currentscope.getParent();
//                    while (parent != null) {
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(1).getText());
//                        if (s != null) {
//                            found = true;
//                            break;
//                        }
//                        parent = parent.getParent();
//                    }
//                    if (found == false) {
//                        System.out.println("Error for using undeclared variable" + ctx.expr_var(1).getText() + " the colume is : " + ex_res.getCol() + "   the line is :  " + ex_res.getLine());
//                    }
//                }
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            le.setlogic(le.getlogic().GT_EQ);
            ex_res=le;

        }
        if(ctx.LT()!=null)
        {
            System.out.println("LT");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.LT() .getText());
            System.out.println(ctx.expr_var(1).getText());
            if(ctx.expr_var(0)!=null)
            {
//                if(ctx.expr_var(0).IDENTIFIER()!=null){
//                    boolean found= false;
//                    Scope parent =currentscope.getParent();
//                    while(parent!=null){
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(0).getText());
//                        if(s!=null){
//                            found= true;
//                            break;
//                        }
//                        parent=parent.getParent();
//                    }
//                    if(found==false)
//                    {
//                        System.out.println("Error for using undeclared variable "+ctx.expr_var(0).getText() +" the colume is : "+ ex_res.getCol()+"   the line is :  " +ex_res.getLine());
//                    }
//                }
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }
            if(ctx.expr_var(1)!=null)
            {
//                if(ctx.expr_var(1).IDENTIFIER()!=null) {
//                    boolean found = false;
//                    Scope parent = currentscope.getParent();
//                    while (parent != null) {
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(1).getText());
//                        if (s != null) {
//                            found = true;
//                            break;
//                        }
//                        parent = parent.getParent();
//                    }
//                    if (found == false) {
//                        System.out.println("Error for using undeclared variable" + ctx.expr_var(1).getText() + " the colume is : " + ex_res.getCol() + "   the line is :  " + ex_res.getLine());
//                    }
//                }
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }
            le.setlogic(le.getlogic().LT);
            ex_res=le;
        }
        if(ctx.LT_EQ()!=null)
        {
            System.out.println("LT_EQ");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.LT_EQ() .getText());
            System.out.println(ctx.expr_var(1).getText());
            if(ctx.expr_var(0)!=null)
            {
//                if(ctx.expr_var(0).IDENTIFIER()!=null){
//                    boolean found= false;
//                    Scope parent =currentscope.getParent();
//                    while(parent!=null){
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(0).getText());
//                        if(s!=null){
//                            found= true;
//                            break;
//                        }
//                        parent=parent.getParent();
//                    }
//                    if(found==false)
//                    {
//                        System.out.println("Error for using undeclared variable "+ctx.expr_var(0).getText() +" the colume is : "+ ex_res.getCol()+"   the line is :  " +ex_res.getLine());
//                    }
//                }
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }

            if(ctx.expr_var(1)!=null)
            {
//                if(ctx.expr_var(1).IDENTIFIER()!=null) {
//                    boolean found = false;
//                    Scope parent = currentscope.getParent();
//                    while (parent != null) {
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(1).getText());
//                        if (s != null) {
//                            found = true;
//                            break;
//                        }
//                        parent = parent.getParent();
//                    }
//                    if (found == false) {
//                        System.out.println("Error for using undeclared variable" + ctx.expr_var(1).getText() + " the colume is : " + ex_res.getCol() + "   the line is :  " + ex_res.getLine());
//                    }
//                }
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }

            le.setlogic(le.getlogic().LT_EQ);
            ex_res=le;
        }
        if(ctx.NOT_EQ1()!=null)
        {
            System.out.println("NOT_EQ1");
            System.out.println(ctx.expr_var(0).getText());
            System.out.println(ctx.NOT_EQ1() .getText());
            System.out.println(ctx.expr_var(1).getText());


            if(ctx.expr_var(0)!=null)
            {
//                if(ctx.expr_var(0).IDENTIFIER()!=null){
//                    boolean found= false;
//                    Scope parent =currentscope.getParent();
//                    while(parent!=null){
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(0).getText());
//                        if(s!=null){
//                            found= true;
//                            break;
//                        }
//                        parent=parent.getParent();
//                    }
//                    if(found==false)
//                    {
//                        System.out.println("Error for using undeclared variable "+ctx.expr_var(0).getText() +" the colume is : "+ ex_res.getCol()+"   the line is :  " +ex_res.getLine());
//                    }
//                }
                le.setLeft_expr(visitExpr_var(ctx.expr_var(0)));
            }

            if(ctx.expr_var(1)!=null)
            {
//                if(ctx.expr_var(1).IDENTIFIER()!=null) {
//                    boolean found = false;
//                    Scope parent = currentscope.getParent();
//                    while (parent != null) {
//                        Symbol s = parent.getSymbolMap().get(ctx.expr_var(1).getText());
//                        if (s != null) {
//                            found = true;
//                            break;
//                        }
//                        parent = parent.getParent();
//                    }
//                    if (found == false) {
//                        System.out.println("Error for using undeclared variable" + ctx.expr_var(1).getText() + " the colume is : " + ex_res.getCol() + "   the line is :  " + ex_res.getLine());
//                    }
//                }
                le.setRight_expr(visitExpr_var(ctx.expr_var(1)));
            }

            le.setlogic(le.getlogic().NOT_EQ1);
            ex_res=le;
        }

        if(ctx.K_AND()!=null)
        {
            System.out.println("K_AND");
            System.out.println(ctx.condition_header(0).getText());
            System.out.println(ctx.K_AND() .getText());
            System.out.println(ctx.condition_header(1).getText());


            if(ctx.condition_header(0)!=null)
            {
                le.setLeft_expr(visitCondition_header(ctx.condition_header(0)));
            }


            if(ctx.condition_header(1)!=null)
            {
                le.setRight_expr(visitCondition_header(ctx.condition_header(1)));

            }
            le.setlogic(le.getlogic().K_AND);
            ex_res=le;

        }
        if(ctx.K_OR()!=null)
        {
            System.out.println("K_OR");
            System.out.println(ctx.condition_header(0).getText());
            System.out.println(ctx.K_OR() .getText());
            System.out.println(ctx.condition_header(1).getText());

            if(ctx.condition_header(0)!=null)
            {
                le.setLeft_expr(visitCondition_header(ctx.condition_header(0)));
            }

            if(ctx.condition_header(1)!=null)
            {
                le.setRight_expr(visitCondition_header(ctx.condition_header(1)));
            }
            le.setlogic(le.getlogic().K_OR);
            ex_res=le;

        }

        if(ctx.AMP2()!=null)
        {
            System.out.println("AMP2");
            System.out.println(ctx.condition_header(0).getText());
            System.out.println(ctx.AMP2() .getText());
            System.out.println(ctx.condition_header(1).getText());


            if(ctx.condition_header(0)!=null)
            {
                le.setLeft_expr(visitCondition_header(ctx.condition_header(0)));
            }


            if(ctx.condition_header(1)!=null)
            {
                le.setRight_expr(visitCondition_header(ctx.condition_header(1)));

            }
            le.setlogic(le.getlogic().AMP2);
            ex_res=le;

        }
        if(ctx.PIPE2()!=null)
        {
            System.out.println("PIPE2");
            System.out.println(ctx.condition_header(0).getText());
            System.out.println(ctx.PIPE2() .getText());
            System.out.println(ctx.condition_header(1).getText());

            if(ctx.condition_header(0)!=null)
            {
                le.setLeft_expr(visitCondition_header(ctx.condition_header(0)));
            }

            if(ctx.condition_header(1)!=null)
            {
                le.setRight_expr(visitCondition_header(ctx.condition_header(1)));
            }
            le.setlogic(le.getlogic().PIPE2);
            ex_res=le;

        }

        if(ctx.bool_ex()!=null)
        {
            condition_header.setBool_expr(visitBool_ex(ctx.bool_ex()));
        }
        condition_header.setExpr(ex_res);
        return condition_header;
    }


    @Override
    public Bollean visitBool_ex(SQLParser.Bool_exContext ctx) {

        Bollean BOOL=new Bollean();
        BOOL.setLine(ctx.getStart().getLine());
        BOOL.setCol(ctx.getStart().getCharPositionInLine());

        System.out.println("visitBool_exp");

        if(ctx.K_TRUE()!=null)
        {
            BOOL.setBool(ctx.K_TRUE().getText());
            //   this.expr_type=Type.BOOLEAN_CONST;
            System.out.println("bool : "+ctx.K_TRUE().getText());
        }

        if(ctx.K_FALSE()!=null)
        {
            BOOL.setBool(ctx.K_FALSE().getText());
            //  this.expr_type=Type.BOOLEAN_CONST;
            System.out.println("bool : "+ctx.K_FALSE().getText());
        }

        if (ctx.AMP() != null) {
            System.out.println("Amp");

            if(ctx.bool_ex(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                BOOL.setLeft_expr(visitBool_ex(ctx.bool_ex(0)));
                //ex_res.setValue(ctx.expr(0).column_name().any_name().getText());
                System.out.println("valye----"+BOOL.getLeft_expr().getBool());
            }


            if(ctx.bool_ex(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                BOOL.setRight_expr(visitBool_ex(ctx.bool_ex(1)));
                //ex_res.setValue(ctx.expr(1).column_name().any_name().getText());
                System.out.println("valye----"+BOOL.getRight_expr().getBool());
                //visitExpr(ctx.expr(1));
            }
            BOOL.setLogic(BOOL.getLogic().AMP);
            System.out.println("gettt "+BOOL.getLogic());
        }

        if (ctx.PIPE() != null) {
            System.out.println("PIPE");

            if(ctx.bool_ex(0)!=null)
            {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                BOOL.setLeft_expr(visitBool_ex(ctx.bool_ex(0)));
                //ex_res.setValue(ctx.expr(0).column_name().any_name().getText());
                System.out.println("valye----"+BOOL.getLeft_expr().getBool());
            }


            if(ctx.bool_ex(1)!=null)
            {

                //System.out.println(ctx.expr(1).getChildCount());
                BOOL.setRight_expr(visitBool_ex(ctx.bool_ex(1)));
                //ex_res.setValue(ctx.expr(1).column_name().any_name().getText());
                System.out.println("valye----"+BOOL.getRight_expr().getBool());
                //visitExpr(ctx.expr(1));
            }
            BOOL.setLogic(BOOL.getLogic().PIPE);
            System.out.println("gettt "+BOOL.getLogic());
        }

        this.expr_type=Type.BOOLEAN_CONST;

        return BOOL;
    }


    @Override
    public Expr visitExpr_condition(SQLParser.Expr_conditionContext ctx) {
        System.out.println("visit expr condition");
        MathimaticalExpr me = new MathimaticalExpr();
        PropertyJson propertyJson = new PropertyJson();
        incrDecrWithoutComma inc = new incrDecrWithoutComma();
        UnaryExpr unaryExpr = new UnaryExpr();
        Expr ex_res = new Expr();
        ex_res.setLine(ctx.getStart().getLine());
        ex_res.setCol(ctx.getStart().getCharPositionInLine());

        List<Call_func> call_funcs = new ArrayList<>();
        if (ctx.call_func() != null) {
            call_funcs.add(visitCall_func(ctx.call_func()));
            ex_res.setCall_funcs(call_funcs);
        }
        if (ctx.NUMERIC_LITERAL() != null) {
            System.out.println("NUMERIC_LITERAL");
            me.setValue(ctx.getText());
            ex_res = me;
        }
        if (ctx.literal_value() != null) {
            System.out.println("literal_value");
            me.setValue(ctx.getText());
            ex_res = me;
        }
        if (ctx.get_property_json() != null)//////////*****************************************************/
        {
            ex_res.setGetPropertyJsons(visitGet_property_json(ctx.get_property_json()));
        }
        if (ctx.IDENTIFIER() != null) {
            System.out.println("IDENTIFIER ");
            me.setValue(ctx.getText());
            ex_res = me;
        }
        if (ctx.expr_condition(0) != null && ctx.expr_condition(0).get_property_json() != null) {
            ex_res.setGetPropertyJsons(visitGet_property_json(ctx.expr_condition(0).get_property_json()));
        }
        if (ctx.expr_condition(1) != null && ctx.expr_condition(1).get_property_json() != null) {
            ex_res.setGetPropertyJsons(visitGet_property_json(ctx.expr_condition(1).get_property_json()));
        }
        if (ctx.expr_condition(0) != null && ctx.expr_condition(0).array_assigne() != null) {
            System.out.println("array_assigne");
            ex_res.setDeclareArray(visitArray_assigne(ctx.expr_condition(0).array_assigne()));
        }
        if (ctx.expr_condition(1) != null && ctx.expr_condition(1).array_assigne() != null) {
            System.out.println("array_assigne");
            ex_res.setDeclareArray(visitArray_assigne(ctx.expr_condition(1).array_assigne()));
        }
        if (ctx.DIGIT() != null) {
            System.out.println("DIGIT ");
            me.setValue(ctx.getText());
            ex_res = me;
        }
        if (ctx.unary_operator() != null) {
            System.out.println("unaryoperator");
            unaryExpr.setTerm(visitExpr_condition(ctx.expr_condition(0)));
            ex_res = unaryExpr;
        }

        if (ctx.STAR() != null) {
            System.out.println("Star");
            System.out.println(ctx.expr_condition(0).getText());
            System.out.println(ctx.STAR().getText());
            System.out.println(ctx.expr_condition(1).getText());
            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() != 1) {
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() == 1) {
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() != 1) {

                //System.out.println(ctx.expr(1).getChildCount());
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
                //visitExpr(ctx.expr(1));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() == 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }
            ex_res = me;

        }
        if (ctx.DIV() != null) {
            System.out.println("DIV");
            System.out.println(ctx.expr_condition(0).getText());
            System.out.println(ctx.DIV().getText());
            System.out.println(ctx.expr_condition(1).getText());

            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() != 1) {
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() == 1) {
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }

            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() != 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() == 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }
            ex_res = me;


        }
        if (ctx.MOD() != null) {
            System.out.println("MOD");
            System.out.println(ctx.expr_condition(0).getText());
            System.out.println(ctx.MOD().getText());
            System.out.println(ctx.expr_condition(1).getText());

            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() != 1) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() == 1) {
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }

            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() != 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() == 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }
            ex_res = me;

        }
        if (ctx.PLUS() != null) {
            System.out.println("Plus");
            System.out.println(ctx.expr_condition(0).getText());
            System.out.println(ctx.PLUS().getText());
            System.out.println(ctx.expr_condition(1).getText());

            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() != 1) {
                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() == 1) {
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }


            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() != 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }

            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() == 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }
            ex_res = me;
        }
        if (ctx.MINUS() != null) {
            System.out.println("Minus");

            System.out.println(ctx.expr_condition(0).getText());
            System.out.println(ctx.MINUS().getText());
            System.out.println(ctx.expr_condition(1).getText());

            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() != 1) {

                //System.out.println(ctx.expr(0).getChildCount());
                //visitExpr(ctx.expr(0));
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(0).getChildCount() == 1) {
                me.setLeft_expr(visitExpr_condition(ctx.expr_condition(0)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() != 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }
            if (ctx.expr_condition(1) != null && ctx.expr_condition(1).getChildCount() == 1) {
                me.setRight_expr(visitExpr_condition(ctx.expr_condition(1)));
            }
            ex_res = me;


        }
        if (ctx.incr_decr_without_comma() != null)//////////*****************************************************/
        {
            System.out.println("incr_decr_without_comma :" + ctx.incr_decr_without_comma().getText());
            ex_res.setIncrDecr(visitIncr_decr_without_comma(ctx.incr_decr_without_comma()));
        }

        return ex_res;

    }

    //Print
    @Override
    public print visitPrint(SQLParser.PrintContext ctx) {

        //System.out.println("visitPrint");
        print pr = new print();
        pr.setLine(ctx.getStart().getLine());
        pr.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.expr_test() != null) {
            pr.setExpr(visitExpr_test(ctx.expr_test()));
            System.out.println(ctx.expr_test().getText());
        }
        if (ctx.get_property_json() != null) {
            pr.setGetPropertyJson(visitGet_property_json(ctx.get_property_json()));
        }

        return pr;
    }


    @Override
    public ifLine visitIf_line_stmt(SQLParser.If_line_stmtContext ctx) {
       // System.out.println("visitIf_line_stmt");
        int v=0;
        ifLine if_line = new ifLine();
        if_line.setLine(ctx.getStart().getLine());
        if_line.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.define_var() != null) {
            if_line.setVar(visitDefine_var(ctx.define_var()));
        }
        if (ctx.condition_header() != null) {
            if_line.setCon_header(visitCondition_header(ctx.condition_header()));
        }

        if (ctx.if_line_first_parameter() != null) {
            if_line.setIf_lin_f_pa(visitIf_line_first_parameter(ctx.if_line_first_parameter()));
        }

        if (ctx.if_line_second_parameter() != null) {
            if_line.setIf_lin_s_pa(visitIf_line_second_parameter(ctx.if_line_second_parameter()));
        }

        return if_line;

    }

    @Override
    public ifLineFirstParameter visitIf_line_first_parameter(SQLParser.If_line_first_parameterContext ctx) {

        //System.out.println("visitIf_line_first_parameter");
        ifLineFirstParameter if_line_f_pa = new ifLineFirstParameter();
        if_line_f_pa.setLine(ctx.getStart().getLine());
        if_line_f_pa.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.print() != null) {
            if_line_f_pa.setPr(visitPrint(ctx.print()));
        }

        if (ctx.expr_var() != null) {
            if_line_f_pa.setExpr(visitExpr_var(ctx.expr_var()));
        }

        if (ctx.if_line_stmt() != null) {
            if_line_f_pa.setIf_lin(visitIf_line_stmt(ctx.if_line_stmt()));
        }

        return if_line_f_pa;
    }

    @Override
    public ifLineSecondParameter visitIf_line_second_parameter(SQLParser.If_line_second_parameterContext ctx) {

       // System.out.println("visitIf_line_second_parameter");
        ifLineSecondParameter if_line_s_pa = new ifLineSecondParameter();
        if_line_s_pa.setLine(ctx.getStart().getLine());
        if_line_s_pa.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.print() != null) {
            if_line_s_pa.setPr(visitPrint(ctx.print()));
        }

        if (ctx.expr_var() != null) {
            if_line_s_pa.setExpr(visitExpr_var(ctx.expr_var()));
        }

        if (ctx.if_line_stmt() != null) {
            if_line_s_pa.setIf_line(visitIf_line_stmt(ctx.if_line_stmt()));
        }

        return if_line_s_pa;
    }


    //Sql Statments

    @Override
    public SQL_Statement visitSql_stmt(SQLParser.Sql_stmtContext ctx) {

        //System.out.println("visitSql_stmt");

        SQL_Statement s = new SQL_Statement();
        s.setLine(ctx.getStart().getLine());
        s.setCol(ctx.getStart().getCharPositionInLine());

        //select

        if (ctx.factored_select_stmt() != null) {
            s.setSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
        }

        //insert
        if (ctx.insert_stmt() != null) {
            s.setInsert(visitInsert_stmt(ctx.insert_stmt()));
        }

        //delete
        if (ctx.delete_stmt() != null) {
            s.setDel_sql(visitDelete_stmt(ctx.delete_stmt()));
        }


        //update
        if (ctx.update_stmt() != null) {

            s.setUpd(visitUpdate_stmt(ctx.update_stmt()));
        }


        //Drop Table
        if (ctx.drop_table_stmt() != null) {
            s.setDrop_table(visitDrop_table_stmt(ctx.drop_table_stmt()));
        }
        //alter table
        if (ctx.alter_table_stmt() != null) {
            s.setAlter_table_stmt(visitAlter_table_stmt(ctx.alter_table_stmt()));

        }

        //create aggregation
        if (ctx.create_aggregation_function() != null) {

            s.setAggregation_function(visitCreate_aggregation_function(ctx.create_aggregation_function()));
        }

        //create table
        if (ctx.create_table_stmt() != null) {
            s.setCreateTable(visitCreate_table_stmt(ctx.create_table_stmt()));
        }

        //create type
        if(ctx.create_type_stmt()!=null){
            s.setCreateType(visitCreate_type_stmt(ctx.create_type_stmt()));
        }



        return s;
    }


    @Override
    public create_aggregation_function visitCreate_aggregation_function(SQLParser.Create_aggregation_functionContext ctx) {
       System.out.println("create aggregation function");
        create_aggregation_function aggregation_function = new create_aggregation_function();
        if (ctx.function_name() != null) {
            aggregation_function.setFunction_name(visitFunction_name(ctx.function_name()));
        }

        if (ctx.jar_path() != null) {

            aggregation_function.setJarParth(ctx.jar_path().IDENTIFIER().getSymbol().getText());

        }

        if (ctx.class_name() != null) {
            aggregation_function.setClassName(ctx.class_name().IDENTIFIER().getText());
        }

        if (ctx.method_name() != null) {
            aggregation_function.setMethodName(ctx.method_name().IDENTIFIER().getText());
        }
        if (ctx.return_type()!=null){
            if(ctx.return_type().IDENTIFIER().getText().equals("string"))
            {
                aggregation_function.setReturn_type(Type.STRING_CONST);
            }
           else if(ctx.return_type().IDENTIFIER().getText().equals("number"))
            {
                aggregation_function.setReturn_type(Type.NUMBER_CONST);
            }
           else if(ctx.return_type().IDENTIFIER().getText().equals("boolean"))
            {
                aggregation_function.setReturn_type(Type.BOOLEAN_CONST);
            }
            else
            {
               System.err.println("unknown this type");
               System.exit(1);
            }
         //aggregation_function.setReturn_type(ctx.return_type().IDENTIFIER().getText());
        }

        if (ctx.schema_array() != null) {
            aggregation_function.setArray(visitSchema_array(ctx.schema_array()));
        }
        Main.symbolTable.getDeclaredAggregationFunction().add(aggregation_function);
        System.out.println("agg_st" + Main.symbolTable.getDeclaredAggregationFunction().size());
//     Main.symbolTable.setDeclaredAggregationFunction();
        return aggregation_function;
    }

    @Override
    public shcemaarray visitSchema_array(SQLParser.Schema_arrayContext ctx) {
        List<String> elements = new ArrayList<>();
        shcemaarray array = new shcemaarray();
        for (int i = 0; i < ctx.IDENTIFIER().size(); i++) {
            elements.add(ctx.IDENTIFIER(i).getText());
        }
        array.setElements(elements);

        return array;
    }

    @Override
    public createType visitCreate_type_stmt(SQLParser.Create_type_stmtContext ctx) {

        createType createType = new createType();

        List<columnDefType> column_def_type = new ArrayList<>();


        Type typeType = new Type();   //to store this type name in list

        if(ctx.create_type_name()!=null){
            createType.setCreate_type_name(visitCreate_type_name(ctx.create_type_name()));
            Main.type_name_list.add(ctx.create_type_name().getText());
        }

        if(ctx.column_def_type()!=null){
            for (int i = 0; i < ctx.column_def_type().size(); i++) {
                column_def_type.add(visitColumn_def_type(ctx.column_def_type(i)));
            }
            createType.setColumn_def_type(column_def_type);
        }



        typeType.setName(ctx.create_type_name().any_name().IDENTIFIER().getText());
        Main.symbolTable.addTypeType(typeType);
        System.out.println("New_Type : " + Main.symbolTable.getDeclaredTypesType().get(0).getName());
        Main.type_list.add(createType);


        //added by katrine
        for(int i =0; i<Main.type_list.size(); i++)
        {
            //System.out.println("+++++++++++++++++++======");
            for(int j=0;j<Main.type_list.get(i).getColumn_def_type().size();j++)
            {
                for(int a=j+1;a<Main.type_list.get(i).getColumn_def_type().size();a++) {

                    try {
                        if (Main.type_list.get(i).getColumn_def_type().get(j).getCol_name().getAny_name().getName().equals(Main.type_list.get(i).getColumn_def_type().get(a).getCol_name().getAny_name().getName())) {
                            createType.setLine(ctx.column_def_type().get(a).getStart().getLine());
                            createType.setCol(ctx.column_def_type().get(a).getStart().getCharPositionInLine());
                            System.err.println("duplicate column : " + "at line = " + createType.getLine() + " at col = " + createType.getCol());
                            System.exit(1);
                        }
                    } catch (IndexOutOfBoundsException e) {
                    }
                }
            }
        }
        //end my add

        return createType;
    }

    @Override
    public columnDefType visitColumn_def_type(SQLParser.Column_def_typeContext ctx) {

        columnDefType columnDefType = new columnDefType();

        List<type_name> type_names = new ArrayList<>();

        if(ctx.column_name()!=null){
            columnDefType.setCol_name(visitColumn_name(ctx.column_name()));
        }

     /*   if(ctx.type_name()!=null){
            columnDefType.setType_names(visitType_name(ctx.type_name()));
        }*/

        if (ctx.type_name() != null) {
            if(ctx.type_name().size()==0){
                System.err.println("please write the type ...");

            }else if(ctx.type_name().size()==1){
                for (int i = 0; i < ctx.type_name().size(); i++) {
                    type_names.add(visitType_name(ctx.type_name().get(i)));
                }
                columnDefType.setType_names(type_names);
            }else{
                System.err.println("type must be one .. you can't add many type");
            }

        }




        return columnDefType;
    }

    @Override
    public create_type_name visitCreate_type_name(SQLParser.Create_type_nameContext ctx) {

        create_type_name createTypeName = new create_type_name();

        if(ctx.any_name()!=null){
            createTypeName.setAny_name(visitAny_name(ctx.any_name()));
        }

        return createTypeName;
    }



    @Override
    public CreateTable visitCreate_table_stmt(SQLParser.Create_table_stmtContext ctx) {
        //System.out.println("visit Create_table_stmt");
        CreateTable createTable = new CreateTable();
        List<column_def> column_defs = new ArrayList<>();
        List<table_constraint> table_constraints = new ArrayList<>();
        Type tableType = new Type();     //table type


        if (ctx.database_name() != null) {
            createTable.setDb_name(visitDatabase_name(ctx.database_name()));
        }
        if (ctx.table_name() != null) {
            createTable.setTb_name(visitTable_name(ctx.table_name()));
            Main.table_name.add(ctx.table_name().getText());
        }
        if (ctx.column_def() != null) {
            for (int i = 0; i < ctx.column_def().size(); i++) {
                column_defs.add(visitColumn_def(ctx.column_def(i)));
                Main.column_name.add(ctx.column_def(i).column_name().any_name().getText());
            }
            createTable.setColumn_defs(column_defs);
        }
        if(ctx.type_create_table()!=null)
        {
            createTable.setType_create_table(ctx.type_create_table().IDENTIFIER().getSymbol().getText());
        }
        if(ctx.path_create_table()!=null)
        {
            createTable.setPath_create_type(ctx.path_create_table().IDENTIFIER().getSymbol().getText());
        }
        if (ctx.table_constraint() != null) {
            for (int i = 0; i < ctx.table_constraint().size(); i++) {
                table_constraints.add(visitTable_constraint(ctx.table_constraint(i)));
            }
            createTable.setTable_constraints(table_constraints);
        }

//        if(ctx.select_stmt()!=null)
//        {
//            createTable.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
//        }


        tableType.setName(ctx.table_name().any_name().IDENTIFIER().getText());
        Main.symbolTable.addType(tableType);
        System.out.println("New_Type : " + Main.symbolTable.getDeclaredTypes().get(0).getName());
        Main.table_list.add(createTable);


        for(int i =0; i<Main.table_list.size(); i++)
        {
            //System.out.println("+++++++++++++++++++======");
            for(int j=0;j<Main.table_list.get(i).getColumn_defs().size();j++)
            {
                for(int a=j+1;a<Main.table_list.get(i).getColumn_defs().size();a++) {

                    try {
                        if (Main.table_list.get(i).getColumn_defs().get(j).getCol_name().getAny_name().getName().equals(Main.table_list.get(i).getColumn_defs().get(a).getCol_name().getAny_name().getName())) {
                            createTable.setLine(ctx.column_def().get(a).getStart().getLine());
                            createTable.setCol(ctx.column_def().get(a).getStart().getCharPositionInLine());
                            System.err.println("duplicate column : " + "at line = " + createTable.getLine() + " at col = " + createTable.getCol());
                            System.exit(1);
                        }
                    } catch (IndexOutOfBoundsException e) {
                    }
                }
            }
        }
        //end my add

        return createTable;
    }

    @Override
    public alter_table_stmt visitAlter_table_stmt(SQLParser.Alter_table_stmtContext ctx) {
//
       // System.out.println("visit Alter_table_stmt");
        alter_table_stmt alter = new alter_table_stmt();
        if (ctx.K_TABLE() != null) {
            if (ctx.K_TABLE() != null) {
                if (ctx.database_name() != null) {
                    alter.setDatabaseName(visitDatabase_name(ctx.database_name()));
                }

                alter.setSource_table_name(visitSource_table_name(ctx.source_table_name()));
                if (ctx.K_RENAME() != null) {
                    if (ctx.K_TO() != null) {
                        if (ctx.new_table_name() != null) {
                            alter.setNew_table_name(visitNew_table_name(ctx.new_table_name()));
                        }
                    }
                }

                if (ctx.alter_table_add() != null) {
                    alter.setAlter_table_add(visitAlter_table_add(ctx.alter_table_add()));
                }
                if (ctx.alter_table_add_constraint() != null) {
                    alter.setAlter_table_add_constraint(visitAlter_table_add_constraint(ctx.alter_table_add_constraint()));
                }
                if (ctx.K_ADD() != null) {
                    if (ctx.column_def() != null) {
                        alter.setColumn_def(visitColumn_def(ctx.column_def()));
                    }
                }
            }
        }

        return alter;
    }

    @Override
    public column_def visitColumn_def(SQLParser.Column_defContext ctx) {
        //System.out.println("visitColumn_def");
        column_def column_defs = new column_def();
        List<column_constraint> column_constraints = new ArrayList<>();
        List<type_name> type_names = new ArrayList<>();
        column_defs.setCol_name(visitColumn_name(ctx.column_name()));
        if (ctx.column_constraint() != null) {
            for (int i = 0; i < ctx.column_constraint().size(); i++) {
                column_constraints.add(visitColumn_constraint(ctx.column_constraint().get(i)));

            }
            column_defs.setColumn_constraints(column_constraints);
        }
        if (ctx.type_name() != null) {
            for (int i = 0; i < ctx.type_name().size(); i++) {
                type_names.add(visitType_name(ctx.type_name().get(i)));

            }
            column_defs.setType_names(type_names);
        }
        return column_defs;
    }

    @Override
    public type_name visitType_name(SQLParser.Type_nameContext ctx) {
        //System.out.println("visitType_name");
        type_name type_names = new type_name();
        List<signed_number> signed_numbers = new ArrayList<>();
        List<anyNam> anyNamss = new ArrayList<>();


//        anyNam name = new anyNam();
//        List<signed_number> signed_numbers= new ArrayList<>();
//        List<anyNam> anyNams= new ArrayList<>();
//

        if(ctx.name().getText().equals("string")) {
            type_names.setNames(visitName(ctx.name()));
        }
        else if(ctx.name().getText().equals("number")){

            type_names.setNames(visitName(ctx.name()));
        }
        else if(ctx.name().getText().equals("boolean")){

            type_names.setNames(visitName(ctx.name()));
        }
        else if(Main.table_name.contains(ctx.name().getText())){

            type_names.setNames(visitName(ctx.name()));
        }
        else if( Main.type_name_list.contains(ctx.name().getText()) ){//to check type is found
            //System.err.print("kkkkkkkkkkkkkkkkkkkkkkkkkkkkookoko type :");
            //System.err.println(ctx.name().getText());

            //System.err.print("kkkkkkkkkkkkkkkkkkkkkkkkkkkkookoko Main.type_name_list : ");
            //System.err.println(Main.type_name_list);
            type_names.setNames(visitName(ctx.name()));
        }
        else {
            type_names.setLine(ctx.name().getStart().getLine());
            type_names.setCol(ctx.name().getStart().getCharPositionInLine());
            System.err.println("undeclared this type : "+"at line : "+type_names.getLine()+" at col : "+type_names.getCol());
            System.exit(1);
        }
        if (ctx.signed_number() != null) {
            for (int i = 0; i < ctx.signed_number().size(); i++) {
                signed_numbers.add(visitSigned_number(ctx.signed_number().get(i)));
            }
            type_names.setSigned_numbers(signed_numbers);
        }
        if (ctx.any_name() != null) {
            for (int i = 0; i < ctx.any_name().size(); i++) {
                anyNamss.add(visitAny_name(ctx.any_name().get(i)));
            }
            type_names.setAnyNams(anyNamss);
        }

        return type_names;
    }

    @Override
    public column_constraint visitColumn_constraint(SQLParser.Column_constraintContext ctx) {
       // System.out.println("visitColumn_constraint");
        column_constraint column_constraints = new column_constraint();
        if (ctx.K_CONSTRAINT() != null) {
            if (ctx.name() != null) {
                //column_constraints.setNam(visitAny_name(ctx.name().any_name()));
                column_constraints.setName(visitName(ctx.name()));
            }
        }
        if (ctx.column_constraint_primary_key() != null) {
            column_constraints.setColumn_constraint_primary_key(visitColumn_constraint_primary_key(ctx.column_constraint_primary_key()));
        }
        if (ctx.column_constraint_foreign_key() != null) {
            column_constraints.setColumn_constraint_foreign_key(visitColumn_constraint_foreign_key(ctx.column_constraint_foreign_key()));
        }
        if (ctx.column_constraint_not_null() != null) {
            column_constraints.setColumn_constraint_not_null(visitColumn_constraint_not_null(ctx.column_constraint_not_null()));
        }
        if (ctx.column_constraint_null() != null) {
            column_constraints.setColumn_constraint_null(visitColumn_constraint_null(ctx.column_constraint_null()));
        }
        if (ctx.K_CHECK() != null) {
            if (ctx.expr() != null) {
                column_constraints.setExpr(visitExpr(ctx.expr()));
            }
        }
        if (ctx.column_default() != null) {
            column_constraints.setColumn_defaults(visitColumn_default(ctx.column_default()));
        }
        if (ctx.K_COLLATE() != null) {
            column_constraints.setCollation_name(visitCollation_name(ctx.collation_name()));
        }

        return column_constraints;
    }

    @Override
    public column_default visitColumn_default(SQLParser.Column_defaultContext ctx) {
       // System.out.println("visitColumn_default");
        column_default column_defs = new column_default();
        List<anyNam> anyNams = new ArrayList<>();
        if (ctx.K_DEFAULT() != null) {

            if (ctx.column_default_value() != null) {
                column_defs.setColumn_default_values(visitColumn_default_value(ctx.column_default_value()));
            }
            if (ctx.K_NEXTVAL() != null) {
                column_defs.setExpr(visitExpr(ctx.expr()));
            }
            if (ctx.expr() != null) {
                column_defs.setExpr(visitExpr(ctx.expr()));
            }
//            if(ctx.any_name()!=null)
//            {
//                column_defs.setAnyNam1(visitAny_name((SQLParser.Any_nameContext) ctx.any_name()));
//
//            }
            if (ctx.any_name() != null) {
                for (int i = 0; i < ctx.any_name().size(); i++) {
                    anyNams.add(visitAny_name(ctx.any_name().get(i)));
                }
                column_defs.setAnyNams(anyNams);
            }

        }

//        if(ctx.any_name()!=null)
//        {
//            column_defs.setAnyNam2(visitAny_name((SQLParser.Any_nameContext) ctx.any_name()));
//        }
        return column_defs;
    }

    @Override
    public column_default_value visitColumn_default_value(SQLParser.Column_default_valueContext ctx) {
       // System.out.println("visitColumn_default_value");
        column_default_value column_default_values = new column_default_value();
        if (ctx.signed_number() != null) {
            column_default_values.setSigned_numbers(visitSigned_number(ctx.signed_number()));
        }
        if (ctx.literal_value() != null) {
            column_default_values.setLiteral(visitLiteral_value(ctx.literal_value()));
        }
        return column_default_values;
    }

    @Override
    public signed_number visitSigned_number(SQLParser.Signed_numberContext ctx) {
       // System.out.println("visitSigned_number");
        signed_number signed_numbers = new signed_number();
        signed_numbers.setLine(ctx.getStart().getLine());
        signed_numbers.setCol(ctx.getStart().getCharPositionInLine());

        if (ctx.NUMERIC_LITERAL() != null) {
            signed_numbers.setNumeric(ctx.getText());
        }
        return signed_numbers;
    }


    @Override
    public column_constraint_null visitColumn_constraint_null(SQLParser.Column_constraint_nullContext ctx) {
       // System.out.println("visitColumn_constraint_null");
        column_constraint_null column_constraint_nulls = new column_constraint_null();

        return column_constraint_nulls;
    }

    @Override
    public column_constraint_not_null visitColumn_constraint_not_null(SQLParser.Column_constraint_not_nullContext ctx) {
       // System.out.println("visitColumn_constraint_not_null");
        column_constraint_not_null column_constraint_not_nulls = new column_constraint_not_null();

        return column_constraint_not_nulls;
    }

    @Override
    public column_constraint_primary_key visitColumn_constraint_primary_key(SQLParser.Column_constraint_primary_keyContext ctx) {
       // System.out.println("visitColumn_constraint_primary_key");
        column_constraint_primary_key column_constraint_primary_keys = new column_constraint_primary_key();


        return column_constraint_primary_keys;
    }

    @Override
    public column_constraint_foreign_key visitColumn_constraint_foreign_key(SQLParser.Column_constraint_foreign_keyContext ctx) {
       // System.out.println("visitColumn_constraint_foreign_key");
        column_constraint_foreign_key column_constraint_foreign_keys = new column_constraint_foreign_key();
        column_constraint_foreign_keys.setForeign_key_clause(visitForeign_key_clause(ctx.foreign_key_clause()));
        return column_constraint_foreign_keys;
    }


    @Override
    public alter_table_add_constraint visitAlter_table_add_constraint(SQLParser.Alter_table_add_constraintContext ctx) {
       // System.out.println("visitAlter_table_add_constraint");
        alter_table_add_constraint alter_table_add_constraint = new alter_table_add_constraint();
        alter_table_add_constraint.setAnyNam(visitAny_name(ctx.any_name()));
        alter_table_add_constraint.setTable_constraint(visitTable_constraint(ctx.table_constraint()));

        return alter_table_add_constraint;
    }

    @Override
    public alter_table_add visitAlter_table_add(SQLParser.Alter_table_addContext ctx) {
       // System.out.println("visitAlter_table_add");
        alter_table_add alter_table_add = new alter_table_add();
        alter_table_add.setTable_constraint(visitTable_constraint(ctx.table_constraint()));
        return alter_table_add;
    }

    @Override
    public table_constraint visitTable_constraint(SQLParser.Table_constraintContext ctx) {
       // System.out.println("visitTable_constraint");
        table_constraint table_constraint = new table_constraint();
        if (ctx.name() != null) {
            // table_constraint.setAnyNam(visitAny_name(ctx.name().any_name()));
            table_constraint.setNam(visitName(ctx.name()));
        }
        if (ctx.table_constraint_primary_key() != null) {
            table_constraint.setTable_constraint_primary_key(visitTable_constraint_primary_key(ctx.table_constraint_primary_key()));
        }
        if (ctx.table_constraint_key() != null) {
            table_constraint.setTable_constraint_key(visitTable_constraint_key(ctx.table_constraint_key()));
        }
        if (ctx.table_constraint_unique() != null) {
            table_constraint.setTable_constraint_unique(visitTable_constraint_unique(ctx.table_constraint_unique()));
        }
        if (ctx.expr() != null) {
            table_constraint.setExpr(visitExpr(ctx.expr()));
        }
        if (ctx.table_constraint_foreign_key() != null) {
            table_constraint.setTable_constraint_foreign_key(visitTable_constraint_foreign_key(ctx.table_constraint_foreign_key()));
        }

        return table_constraint;
    }

    @Override
    public Name visitName(SQLParser.NameContext ctx) {
        //System.out.println("visitName");
        Name name = new Name();
        name.setLine(ctx.getStart().getLine());
        name.setCol(ctx.getStart().getCharPositionInLine());

        name.setName(visitAny_name(ctx.any_name()));
        return name;
    }

    @Override
    public table_constraint_foreign_key visitTable_constraint_foreign_key(SQLParser.Table_constraint_foreign_keyContext ctx) {
       // System.out.println("visitTable_constraint_foreign_key");
        table_constraint_foreign_key table_constraint_foreign_key = new table_constraint_foreign_key();
        List<fk_origin_column_name> fk_origin_column_names = new ArrayList<>();
        if (ctx.fk_origin_column_name() != null) {
            for (int i = 0; i < ctx.fk_origin_column_name().size(); i++) {
                fk_origin_column_names.add(visitFk_origin_column_name(ctx.fk_origin_column_name().get(i)));
            }
            table_constraint_foreign_key.setFk_origin_column_names(fk_origin_column_names);
        }
        if (ctx.foreign_key_clause() != null) {
            table_constraint_foreign_key.setForeign_key_clause(visitForeign_key_clause(ctx.foreign_key_clause()));
        }


        return table_constraint_foreign_key;
    }

    @Override
    public foreign_key_clause visitForeign_key_clause(SQLParser.Foreign_key_clauseContext ctx) {
        //System.out.println("visitForeign_key_clause");
        foreign_key_clause foreign_key_clause = new foreign_key_clause();
        List<fk_target_column_name> fk_target_column_names = new ArrayList<>();
        List<Name> names = new ArrayList<>();
        if (ctx.database_name() != null) {
            foreign_key_clause.setDatabaseName(visitDatabase_name(ctx.database_name()));
        }
        if (ctx.foreign_table() != null) {
            foreign_key_clause.setForeign_table(visitForeign_table(ctx.foreign_table()));
            if (ctx.fk_target_column_name() != null) {

                for (int i = 0; i < ctx.fk_target_column_name().size(); i++) {
                    fk_target_column_names.add(visitFk_target_column_name(ctx.fk_target_column_name().get(i)));
                }
                foreign_key_clause.setFk_target_column_names(fk_target_column_names);

            }


        }
        if (ctx.name() != null) {
            for (int i = 0; i < ctx.name().size(); i++) {
                names.add(visitName(ctx.name().get(i)));
            }
            foreign_key_clause.setName(names);
        }
        return foreign_key_clause;
    }

    @Override
    public fk_target_column_name visitFk_target_column_name(SQLParser.Fk_target_column_nameContext ctx) {
       // System.out.println("visitFk_target_column_name");
        fk_target_column_name fk_target_column_name = new fk_target_column_name();
        //  fk_target_column_name.setNam(visitAny_name(ctx.name().any_name()));
        fk_target_column_name.setName(visitName(ctx.name()));
        return fk_target_column_name;
    }

    @Override
    public foreign_table visitForeign_table(SQLParser.Foreign_tableContext ctx) {
       // System.out.println("visitForeign_table");
        foreign_table foreign_table = new foreign_table();
        foreign_table.setForeign_table(visitAny_name(ctx.any_name()));
        return foreign_table;
    }


    @Override
    public fk_origin_column_name visitFk_origin_column_name(SQLParser.Fk_origin_column_nameContext ctx) {
        //System.out.println("visitFk_origin_column_name");
        fk_origin_column_name fk_origin_column_name = new fk_origin_column_name();
        fk_origin_column_name.setCol_name(visitColumn_name(ctx.column_name()));
        return fk_origin_column_name;
    }


    @Override
    public table_constraint_unique visitTable_constraint_unique(SQLParser.Table_constraint_uniqueContext ctx) {
       // System.out.println("visitTable_constraint_unique");
        table_constraint_unique table_constraint_unique = new table_constraint_unique();
        List<indexed_column> indexed_columns = new ArrayList<>();
        if (ctx.name() != null) {
            table_constraint_unique.setName(visitName(ctx.name()));
        }
        if (ctx.indexed_column() != null) {
            for (int i = 0; i < ctx.indexed_column().size(); i++) {
                indexed_columns.add(visitIndexed_column(ctx.indexed_column().get(i)));
            }
            table_constraint_unique.setIndexed_columns(indexed_columns);
        }
        return table_constraint_unique;
    }


    @Override
    public table_constraint_key visitTable_constraint_key(SQLParser.Table_constraint_keyContext ctx) {
       // System.out.println("visitTable_constraint_key");
        table_constraint_key table_constraint_key = new table_constraint_key();
        List<indexed_column> indexed_columns = new ArrayList<>();
        if (ctx.name() != null) {
            table_constraint_key.setName(visitName(ctx.name()));
        }
        if (ctx.indexed_column() != null) {
            for (int i = 0; i < ctx.indexed_column().size(); i++) {
                indexed_columns.add(visitIndexed_column(ctx.indexed_column().get(i)));
            }
            table_constraint_key.setIndexed_columns(indexed_columns);
        }

        return table_constraint_key;
    }


    @Override
    public table_constraint_primary_key visitTable_constraint_primary_key(SQLParser.Table_constraint_primary_keyContext ctx) {
       // System.out.println("visitTable_constraint_primary_key");
        table_constraint_primary_key table_constraint_primary_key = new table_constraint_primary_key();
        List<indexed_column> indexed_columns = new ArrayList<>();
        if (ctx.indexed_column() != null) {
            for (int i = 0; i < ctx.indexed_column().size(); i++) {
                indexed_columns.add(visitIndexed_column(ctx.indexed_column().get(i)));
            }
            table_constraint_primary_key.setIndexed_columnList(indexed_columns);
        }

        return table_constraint_primary_key;
    }

    @Override
    public indexed_column visitIndexed_column(SQLParser.Indexed_columnContext ctx) {
       // System.out.println("visitIndexed_column");
        indexed_column indexed_column = new indexed_column();
        if (ctx.column_name() != null) {
            indexed_column.setCol_name(visitColumn_name(ctx.column_name()));
        }
        if (ctx.collation_name() != null) {
            indexed_column.setCollation_name(visitCollation_name(ctx.collation_name()));
        }
        return indexed_column;
    }

    @Override
    public collation_name visitCollation_name(SQLParser.Collation_nameContext ctx) {
       // System.out.println("visitCollation_name");
        collation_name collation_name = new collation_name();
        collation_name.setLine(ctx.getStart().getLine());
        collation_name.setCol(ctx.getStart().getCharPositionInLine());

        collation_name.setCollation_name(visitAny_name(ctx.any_name()));
        return collation_name;
    }


    @Override
    public New_table_name visitNew_table_name(SQLParser.New_table_nameContext ctx) {
       // System.out.println("visitNew_table_name");
        New_table_name new_table_name = new New_table_name();
        new_table_name.setAny_name(visitAny_name(ctx.any_name()));
        return new_table_name;
    }


    @Override
    public Source_table_name visitSource_table_name(SQLParser.Source_table_nameContext ctx) {
       // System.out.println("visitSource_table_name");
        Source_table_name source_table_name = new Source_table_name();
        source_table_name.setAny_name(visitAny_name(ctx.any_name()));
        return source_table_name;
    }

    @Override
    public insertSql visitInsert_stmt(SQLParser.Insert_stmtContext ctx) {
       // System.out.println("visitInsert_stmt");

        insertSql ins = new insertSql();

        if (ctx.database_name() != null) {
            ins.setDbName(visitDatabase_name(ctx.database_name()));
        }

        if (ctx.table_name() != null) {
            ins.setTbName(visitTable_name(ctx.table_name()));
        }


        List<col_name> c_name_list = new ArrayList<>();
        if (ctx.column_name() != null) {
            for (int i = 0; i < ctx.column_name().size(); i++) {
                c_name_list.add(visitColumn_name(ctx.column_name(i)));
                // System.out.println("Expr : "+ctx.expr().get(i).getText());
            }
            ins.setC_name(c_name_list);
        }


        List<Expr> expr_list = new ArrayList<>();
        if (ctx.expr() != null) {
            for (int i = 0; i < ctx.expr().size(); i++) {
                expr_list.add(visitExpr(ctx.expr(i)));
                // System.out.println("colu_name  : "+ctx.column_name().get(i).getText());
            }
            ins.setExp(expr_list);
        }


        if (ctx.factored_select_stmt() != null) {
            // ins.setSelect_stmt(visitFactored_select_stmt(ctx.select_stmt()));
            ins.setSelect_stmt(visitFactored_select_stmt(ctx.factored_select_stmt()));
        }


        return ins;
    }


    @Override
    public db_name visitDatabase_name(SQLParser.Database_nameContext ctx) {
        //System.out.println("visitDatabase_name");
        db_name name = new db_name();

        if (ctx.any_name() != null) {
            name.setAny_name(visitAny_name(ctx.any_name()));
        }

        //  System.out.println("Database Name : "+ctx.any_name().getText());
        return name;

    }

    @Override public anyNam visitAny_name(SQLParser.Any_nameContext ctx) {
        System.out.println("visitAny_name");
        anyNam any_name =  new anyNam();
        any_name.setLine(ctx.getStart().getLine());
        any_name.setCol(ctx.getStart().getCharPositionInLine());

//        Scope currentscope = Main.parent_stack.peek();
        // System.out.println("currentscope "+ currentscope);
        if(ctx.IDENTIFIER()!=null)
        {
//            boolean found= false;
//            Scope parent =currentscope.getParent();
//            while(parent!=null){
//                Symbol s = parent.getSymbolMap().get(ctx.IDENTIFIER().getText());
//                if(s!=null){
//                    found= true;
//                    break;
//                }
//                parent=parent.getParent();
//            }
//            if(found==false)
//            {
//                System.out.println("Error for using undeclared variable "+ctx.IDENTIFIER().getText() +"  the colume is : "+ any_name.getCol()+"   the line is :  " +any_name.getLine());
//
//            }
            any_name.setName(ctx.IDENTIFIER().getText());
            System.out.println(ctx.IDENTIFIER().getText());
        }

        if(ctx.STRING_LITERAL()!=null)
        {
            any_name.setName(ctx.STRING_LITERAL().getText());
            System.out.println(ctx.STRING_LITERAL().getText());
        }
        return any_name;
    }

    @Override
    public tb_name visitTable_name(SQLParser.Table_nameContext ctx) {
        //System.out.println("visitTable_name");
        tb_name name = new tb_name();

        if (ctx.any_name() != null) {
            name.setAny_name(visitAny_name(ctx.any_name()));
            this.type = ctx.any_name().getText();
        }
        System.out.println("Table Name : " + ctx.any_name().getText());
        return name;
    }

    @Override
    public col_name visitColumn_name(SQLParser.Column_nameContext ctx) {
       // System.out.println("visitColumn_name");
        col_name name = new col_name();

        if (ctx.any_name() != null) {
            name.setAny_name(visitAny_name(ctx.any_name()));
        }

        return name;

    }

    //delete
    @Override
    public deleteSql visitDelete_stmt(SQLParser.Delete_stmtContext ctx) {
        System.out.println("visitDelete_stmt");
        deleteSql del = new deleteSql();


        if (ctx.qualified_table_name() != null) {
            System.out.println("Qualified Table Name : " + ctx.qualified_table_name().getText());
            del.setQualified_table_name(visitQualified_table_name(ctx.qualified_table_name()));
        }

        if (ctx.expr() != null) {
            System.out.println("Expr : " + ctx.expr().getText());
            del.setExp(visitExpr(ctx.expr()));
        }

        return del;
    }

    @Override
    public qualifiedTableName visitQualified_table_name(SQLParser.Qualified_table_nameContext ctx) {
        System.out.println("visitQualified_table_name");
        qualifiedTableName q_tabel_name = new qualifiedTableName();

        if (ctx.database_name() != null) {
            q_tabel_name.setDbName(visitDatabase_name(ctx.database_name()));
        }

        if (ctx.table_name() != null) {
            q_tabel_name.setTbName(visitTable_name(ctx.table_name()));
        }

        if (ctx.index_name() != null) {
            q_tabel_name.setIn_name(visitIndex_name(ctx.index_name()));
        }


        return q_tabel_name;
    }


    @Override
    public indexNam visitIndex_name(SQLParser.Index_nameContext ctx) {
        System.out.println("visitIndex_name");
        indexNam in_name = new indexNam();

        if (ctx.any_name() != null) {
            System.out.println("Index : " + ctx.any_name().getText());
            in_name.setAny_name(visitAny_name(ctx.any_name()));
        }
        //System.out.println("Index Name : "+ in_name.getAny_name().getName());

        return in_name;
    }

    //update
    @Override
    public updateSql visitUpdate_stmt(SQLParser.Update_stmtContext ctx) {
        System.out.println("visitUpdate_stmt");
        updateSql update = new updateSql();

        if (ctx.qualified_table_name() != null) {
            update.setQualified_table_name(visitQualified_table_name(ctx.qualified_table_name()));
        }


        if (ctx.update_first_parameter() != null) {
            update.setUpdate_f_parameter(visitUpdate_first_parameter(ctx.update_first_parameter()));
        }


        if (ctx.update_second_parameter() != null) {
            update.setUpdate_s_parameter(visitUpdate_second_parameter(ctx.update_second_parameter()));
        }

        if (ctx.expr() != null) {
            update.setExp(visitExpr(ctx.expr()));
        }


        return update;
    }

    @Override
    public UpdateFirstParameter visitUpdate_first_parameter(SQLParser.Update_first_parameterContext ctx) {
       // System.out.println("visitUpdate_first_parameter");

        UpdateFirstParameter u_f_parameters = new UpdateFirstParameter();


        if (ctx.column_name() != null) {
            System.out.println("Column Name : " + ctx.column_name().getText());
            u_f_parameters.setC_name(visitColumn_name(ctx.column_name()));
        }


        if (ctx.expr() != null) {
            System.out.println("Expr : " + ctx.expr().getText());
            u_f_parameters.setExp(visitExpr(ctx.expr()));
        }


        return u_f_parameters;
    }

    @Override
    public UpdateSecondParameter visitUpdate_second_parameter(SQLParser.Update_second_parameterContext ctx) {

        System.out.println("visitUpdate_second_parameter");

        UpdateSecondParameter u_s_parameters = new UpdateSecondParameter();


        List<col_name> c_name_list = new ArrayList<>();

        if (ctx.column_name() != null) {

            for (int i = 0; i < ctx.column_name().size(); i++) {
                c_name_list.add(visitColumn_name(ctx.column_name(i)));
                System.out.println("Column Name : " + ctx.column_name().get(i).getText());
            }

            u_s_parameters.setC_name(c_name_list);
        }


        List<Expr> expr_list = new ArrayList<>();

        if (ctx.expr() != null) {
            for (int i = 0; i < ctx.expr().size(); i++) {
                expr_list.add(visitExpr(ctx.expr(0)));
                System.out.println("Expr : " + ctx.expr().get(i).getText());
            }

            u_s_parameters.setExp(expr_list);

        }


        return u_s_parameters;
    }

    //Drop Table
    @Override
    public dropTable visitDrop_table_stmt(SQLParser.Drop_table_stmtContext ctx) {
        //System.out.println("visitDrop_table_stmt");
        dropTable drop_table = new dropTable();

        if (ctx.database_name() != null) {
            drop_table.setDbName(visitDatabase_name(ctx.database_name()));
        }

        if (ctx.table_name() != null) {
            drop_table.setTbName(visitTable_name(ctx.table_name()));
        }


        return drop_table;
    }

//    min_var:
//            ((IDENTIFIER|NUMERIC_LITERAL|array_assigne|get_property_json) '--;'
//            | (IDENTIFIER|NUMERIC_LITERAL|array_assigne|get_property_json) '--'|  '--' (IDENTIFIER|NUMERIC_LITERAL|array_assigne|get_property_json) (';')?)
//    ;
    //Plus VAR , Min Var
    @Override
    public plusVar visitPlus_var(SQLParser.Plus_varContext ctx) {
        System.out.println("visitPlus_var");

        plusVar plus_var = new plusVar();
        plus_var.setLine(ctx.getStart().getLine());
        plus_var.setCol(ctx.getStart().getCharPositionInLine());

        Scope currentScope = Main.parent_stack.peek();

        List<DeclareArray> declareArrays = new ArrayList<>();

        if (ctx.IDENTIFIER() != null) {
            Symbol symbol =  currentScope.getSymbolMap().get(ctx.IDENTIFIER().getText());
            if(symbol!=null)
            {
                if(symbol.getType()==null)
                {
                    System.err.println("Warring for using anassigned variable "+ctx.IDENTIFIER().getText() +"the colume is : "+ plus_var.getCol()+"   the line is :  " +plus_var.getLine());

                }
                else {
                    System.out.println("scop var : "+symbol.getScope().getId());
                    plus_var.setSymbol(symbol);
                    System.out.println("tabel symbol : "+ plus_var.getSymbol().getName());
                }

            }else {
                boolean found = false;
                Scope parent = currentScope.getParent();
                while (parent != null) {
                    Symbol s = parent.getSymbolMap().get(ctx.IDENTIFIER().getText());
                    if (s != null) {
                        found = true;
                        if(s.getType()==null)
                        {
                            System.err.println("Warring for using anassigned variable "+ctx.IDENTIFIER().getText() +"the colume is : "+ plus_var.getCol()+"   the line is :  " +plus_var.getLine());

                        }
                        break;
                    }
                    parent = parent.getParent();
                }
                if (found == false) {
                    System.out.println("Error for using undeclared variable " + ctx.IDENTIFIER().getText() + "  the colume is : " + plus_var.getCol() + "   the line is :  " + plus_var.getLine());
                }
            }
          //  this.expr_type=Type.STRING_CONST;
            plus_var.setIdentif(ctx.IDENTIFIER().getText());
        }
        if (ctx.NUMERIC_LITERAL() != null) {
            plus_var.setNum_lteral(ctx.NUMERIC_LITERAL().getText());
        }

        if (ctx.array_assigne() != null) {

            declareArrays = visitArray_assigne(ctx.array_assigne());
            plus_var.setAssign_array(declareArrays);
        }


        if (ctx.get_property_json() != null) {
            plus_var.setGet_prop_json(visitGet_property_json(ctx.get_property_json()));
        }

        return plus_var;
    }


    @Override
    public minVar visitMin_var(SQLParser.Min_varContext ctx) {

        System.out.println("visitMin_var");

        minVar min_var = new minVar();
        min_var.setLine(ctx.getStart().getLine());
        min_var.setCol(ctx.getStart().getCharPositionInLine());

        Scope currentScope = Main.parent_stack.peek();

        List<DeclareArray> declareArrays = new ArrayList<>();

        if (ctx.IDENTIFIER() != null) {
            Symbol symbol =  currentScope.getSymbolMap().get(ctx.IDENTIFIER().getText());
            if(symbol!=null)
            {
                if(symbol.getType()==null)
                {
                    System.err.println("Warring for using anassigned variable "+ctx.IDENTIFIER().getText() +"the colume is : "+ min_var.getCol()+"   the line is :  " +min_var.getLine());
                }
                else
                {
                    System.out.println("scop var : "+symbol.getScope().getId());
                    min_var.setSymbol(symbol);
                    System.out.println("tabel symbol : "+ min_var.getSymbol().getName());
                }

            }else {
                boolean found = false;
                Scope parent = currentScope.getParent();
                while (parent != null) {
                    Symbol s = parent.getSymbolMap().get(ctx.IDENTIFIER().getText());
                    if (s != null) {
                        found = true;
                        if(s.getType()==null)
                        {
                            System.err.println("Warring for using anassigned variable "+ctx.IDENTIFIER().getText() +"the colume is : "+ min_var.getCol()+"   the line is :  " +min_var.getLine());
                        }
                        break;
                    }
                    parent = parent.getParent();
                }
                if (found == false) {
                    System.out.println("Error for using undeclared variable " + ctx.IDENTIFIER().getText() + "  the colume is : " + min_var.getCol() + "   the line is :  " + min_var.getLine());
                }
            }
            this.expr_type=Type.STRING_CONST;
            min_var.setIdentif(ctx.IDENTIFIER().getText());
        }
        if (ctx.NUMERIC_LITERAL() != null) {
            min_var.setNum_lteral(ctx.NUMERIC_LITERAL().getText());
        }
        if (ctx.array_assigne() != null) {

            declareArrays = visitArray_assigne(ctx.array_assigne());
            min_var.setAssign_array(declareArrays);
        }

        if (ctx.get_property_json() != null) {
            min_var.setGet_prop_json(visitGet_property_json(ctx.get_property_json()));
        }
        return min_var;
    }

    public void throwException(String message) throws Exception{

        throw new Exception(message);
    }
}


