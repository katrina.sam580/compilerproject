package tree;

import generated.SQLLexer;
import generated.SQLParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import tree.AST.Parse;
import tree.AST.QueryStmt.CreateTable;
import tree.AST.QueryStmt.SelectStmt;
import tree.AST.QueryStmt.createType;
import tree.AST.javaStmt.FunctionDeclaration;
import tree.AST.vis.BaseASTVisitor;
import tree.Base.BaseVisitor;
import tree.CG.GeneratorCode;
import tree.SymbolTable.Scope;
import tree.SymbolTable.Symbol;
import tree.SymbolTable.SymbolTable;
import tree.SymbolTable.Type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

public class Main {
    public static SymbolTable symbolTable = new SymbolTable();
    public static Stack<Scope> parent_stack = new Stack();
    public static List<CreateTable> table_list = new ArrayList<>();
    public static List<String> column_name = new ArrayList<>();
    public static List<String> table_name = new ArrayList<>();
    public static List<String> list_joins = new ArrayList<>();
    public static List<createType> type_list = new ArrayList<>();//to store type object in list
    public static List<String> type_name_list = new ArrayList<>();//to store just type name in list



    public static void main(String[] args) {

        try {
            String source = "samples//samples.txt";
            CharStream cs = fromFileName(source);
            SQLLexer lexer = new SQLLexer(cs);
            CommonTokenStream token  = new CommonTokenStream(lexer);
            SQLParser parser = new SQLParser(token);
            ParseTree tree = parser.parse();

            Parse p = (Parse) new BaseVisitor().visit(tree);
            p.accept(new BaseASTVisitor());
            System.out.println(p.toString());
            System.out.println("***************** symbol tabel ***********************");
            System.out.println("----- scope -----");
            for(int i=0;i< symbolTable.getScopes().size();i++)
            {
                System.out.println("scope id          : "+symbolTable.getScopes().get(i).getId());
                if(symbolTable.getScopes().get(i).getParent()!= null) {
                    System.out.println("scope parent      : " + symbolTable.getScopes().get(i).getParent().getId());
                }
                System.out.println("scope map symbol ");
                for (String key : symbolTable.getScopes().get(i).getSymbolMap().keySet() )
                {
                    Symbol s = symbolTable.getScopes().get(i).getSymbolMap().get(key);

                    System.out.println("name of symbol    : "+s.getName());
                    if(s.getType()!=null){
                        System.out.println("type of symbol    : "+s.getType().getName());
                    }
                    System.out.println("isParam  : "+s.getIsParam());

                }
                System.out.println();
                System.out.println(".............");
                System.out.println();
            }
            System.out.println("----- function -----");
            for(int i=0;i< symbolTable.getDeclaredFunction().size();i++)
            {
                System.out.println("function name         : "+symbolTable.getDeclaredFunction().get(i).getFunctionName());
                if(symbolTable.getDeclaredFunction().get(i).getReturnType()!=null)
                {
                    System.out.println("return function type          : "+symbolTable.getDeclaredFunction().get(i).getReturnType().getName());
                }
                System.out.println("scope id              : "+symbolTable.getDeclaredFunction().get(i).getScopefunction().getId());
                if(symbolTable.getDeclaredFunction().get(i).getScopefunction().getParent()!= null) {
                    System.out.println("scope parent      : " + symbolTable.getDeclaredFunction().get(i).getScopefunction().getParent().getId());
                }
                System.out.println("scope map symbol ");
                for (String key : symbolTable.getDeclaredFunction().get(i).getScopefunction().getSymbolMap().keySet() )
                {
                    Symbol s = symbolTable.getDeclaredFunction().get(i).getScopefunction().getSymbolMap().get(key);

                    System.out.println("name of symbol    : "+s.getName());
                    if(s.getType()!=null){
                        System.out.println("type of symbol    : "+s.getType().getName());

                        //--**added by katrine**--
                        if(s.getType().getColumns().size()>0)
                        {
                            for(String key_col : s.getType().getColumns().keySet() )
                            {

                                System.out.println("->column_name : "+key_col);
                                System.out.println(" ->column_type : "+s.getType().getColumns().get(key_col).getName());

                            }
                        }
                        //--**end my add**--
                    }
                    System.out.println("isParam  : "+s.getIsParam());

                }
                System.out.println();
                System.out.println(".............");
                System.out.println();
            }


            System.out.println("----- Types -----");
            for(int i=0;i< symbolTable.getDeclaredTypesType().size();i++)
            {
                System.out.println("Type name    : "+symbolTable.getDeclaredTypesType().get(i).getName());

                System.out.println("Type map Columns  ");
                for (String key : symbolTable.getDeclaredTypesType().get(i).getColumns().keySet() )
                {
                    Type t = symbolTable.getDeclaredTypesType().get(i).getColumns().get(key);

                    System.out.println("name of Type    : "+t.getName());


                }
                System.out.println();
                System.out.println(".............");
                System.out.println();
            }

             CreateTable createTable = new CreateTable();   //should be list of table
            List<CreateTable> createTableList = new ArrayList<>();
            List<createType> createTypeList = new ArrayList<>();
            List<SelectStmt> selectStmtList = new ArrayList<>();
            if(p.getSqlStmts().size()>0) {
                for (int i = 0; i < p.getSqlStmts().size(); i++) {
                    for(int j=0;j<p.getSqlStmts().get(i).size();j++) {
                       if (p.getSqlStmts().get(i).get(j).getCreateTable()!=null)
                       {
                           createTableList.add(p.getSqlStmts().get(i).get(j).getCreateTable());
                       }
                       if(p.getSqlStmts().get(i).get(j).getCreateType()!=null)
                       {
                           createTypeList.add(p.getSqlStmts().get(i).get(j).getCreateType());
                       }
//                       if(p.getSqlStmts().get(i).get(i).getSelectStmt()!=null)
//                       {
//                           selectStmtList.add(p.getSqlStmts().get(i).get(i).getSelectStmt());
//                       }
                    }
                }
            }


            FunctionDeclaration functionDeclaration = new FunctionDeclaration();
            functionDeclaration=p.getFunctions().get(0);

            GeneratorCode generatorCode = new GeneratorCode(Main.symbolTable,createTableList,createTypeList,functionDeclaration,p,"generatefiles");
            try {
                generatorCode.generate_file();
                generatorCode.generate_file_for_function();

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
