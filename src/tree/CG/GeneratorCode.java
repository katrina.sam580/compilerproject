package tree.CG;

import generated.SQLParser;
import tree.AST.Parse;
import tree.AST.QueryStmt.*;
import tree.AST.javaStmt.Expr;
import tree.AST.javaStmt.FunctionDeclaration;
import tree.AST.javaStmt.print;
import tree.Main;
import tree.SymbolTable.Symbol;
import tree.SymbolTable.SymbolTable;
import tree.SymbolTable.Type;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.*;

public class GeneratorCode {

   private SymbolTable symbolTable;
   private String generatefile_path;
   private CreateTable createTable;
   private String type_table;
   private String path_table;
   private List<CreateTable> createTableList;
   private List<createType> createTypeList;
   private FunctionDeclaration functionDeclaration;
   private Parse p;
    public boolean ok1=false;
    public boolean ok2=false;
    public boolean boo=false;
    public int count1=0;
    public int count2=0;
    public int majd=0;
    Set<String> stringSet = new HashSet<>();
    int table=-1;
    int agg_count=0;
    int main_count=0;
    int join_table_count=0;
    List<Integer> main_list = new ArrayList<>();
    public List<String> list_join = new ArrayList<>();
    public String test="";
    public GeneratorCode(SymbolTable symbolTable,List<CreateTable> createTableList,List<createType> createTypeList, FunctionDeclaration functionDeclaration,Parse p,String generatefile_path){
        this.symbolTable=symbolTable;
        this.generatefile_path = generatefile_path;
        this.createTableList = createTableList;
        this.createTypeList = createTypeList;
        this.functionDeclaration =functionDeclaration;
        this.p=p;
    }

    public void generate_file() throws IOException {

        for(int i=0;i<this.symbolTable.getDeclaredTypes().size();i++)
        {
          FileWriter gen_files = new FileWriter(this.generatefile_path+'/'+'/'+this.symbolTable.getDeclaredTypes().get(i).getName()+".txt");
          BufferedWriter bufferedWriter = new BufferedWriter(gen_files);
           addClassName(bufferedWriter,this.symbolTable.getDeclaredTypes().get(i).getName());
        }
        for (int j=0;j<this.symbolTable.getDeclaredTypesType().size();j++)
        {
            FileWriter gen_files = new FileWriter(this.generatefile_path+'/'+'/'+this.symbolTable.getDeclaredTypesType().get(j).getName()+".txt");
            BufferedWriter bufferedWriter = new BufferedWriter(gen_files);
            addClassName(bufferedWriter,this.symbolTable.getDeclaredTypesType().get(j).getName());
        }
    }

    ///Aggregation_function/////
//    public void add_class_aggfunc(BufferedWriter bufferedWriter,create_aggregation_function aggregation_function) throws IOException
//    {
//        bufferedWriter.write("class " + aggregation_function.getClassName());
//        bufferedWriter.write("{");
//        bufferedWriter.newLine();
//        //add data members and methods here
//        //addDataMembers(bufferedWriter,func_name);
//        //add_datamember_aggfunc(bufferedWriter,aggregation_function);
//        bufferedWriter.newLine();
//        //addMethods(bufferedWriter,func_name);
//        add_methods_aggfunc(bufferedWriter,aggregation_function);
//        bufferedWriter.newLine();
//        bufferedWriter.write("}");
//        bufferedWriter.flush();
//        bufferedWriter.close();
//    }

//    public void add_datamember_aggfunc(BufferedWriter bufferedWriter,create_aggregation_function aggregation_function) throws IOException
//    {
//      bufferedWriter.write("public static String JarPath = "+aggregation_function.getJarParth()+";");
//        bufferedWriter.newLine();
//      bufferedWriter.write("public static String JarName = "+'"'+aggregation_function.getClassName()+".jar"+'"'+";");
//        bufferedWriter.newLine();
//      bufferedWriter.write("public static String ClassName = "+'"'+aggregation_function.getClassName()+'"'+";");
//        bufferedWriter.newLine();
//      bufferedWriter.write("public static String MethodName = "+'"'+aggregation_function.getFunction_name().getAny_name().getName()+'"'+";");
//
//    }

//    public void add_methods_aggfunc(BufferedWriter bufferedWriter,create_aggregation_function aggregation_function) throws IOException
//    {
//
//            bufferedWriter.write(" public double "+aggregation_function.getFunction_name().getAny_name().getName()+"(List<Double> numbers){\n" +
//                    "        return numbers.\n" +
//                    "                stream().\n" +
//                    "                mapToDouble(Double::doubleValue)\n" +
//                    "                ."+aggregation_function.getFunction_name().getAny_name().getName().toLowerCase()+"();\n" +
//                    "    }");
//            bufferedWriter.newLine();
//
//            bufferedWriter.write("private static volatile "+aggregation_function.getClassName()+" my"+aggregation_function.getClassName()+";\n" +
//                    "\n" +
//                    "    private "+aggregation_function.getClassName()+"(){\n" +
//                    "        if ("+"my"+aggregation_function.getClassName()+" != null) {\n" +
//                    "            throw new IllegalStateException(\"function object is already created!\");\n" +
//                    "        }\n" +
//                    "    }");
//           bufferedWriter.newLine();
//
//           bufferedWriter.write("    public static "+aggregation_function.getClassName()+" get"+aggregation_function.getClassName()+"() {\n" +
//                   "        if("+"my"+aggregation_function.getClassName()+" == null){\n" +
//                   "            synchronized("+aggregation_function.getClassName()+".class) {\n" +
//                   "                if ("+"my"+aggregation_function.getClassName()+" == null) {\n" +
//                   "my"+aggregation_function.getClassName() +" = new "+aggregation_function.getClassName()+"();\n" +
//                   "                }\n" +
//                   "            }\n" +
//                   "        }\n" +
//                   "        return "+"my"+aggregation_function.getClassName()+";\n" +
//                   "    }");
//    }

    ////////////////////////////

    public void generate_file_for_function() throws IOException {
        /**generate class for declared types in ST**/
        for(int i=0;i<this.symbolTable.getDeclaredFunction().size();i++)
        {
            FileWriter gen_files = new FileWriter(this.generatefile_path+'/'+'/'+this.symbolTable.getDeclaredFunction().get(i).getFunctionName()+".txt");
            BufferedWriter bufferedWriter = new BufferedWriter(gen_files);
            addFunctionName(bufferedWriter,this.symbolTable.getDeclaredFunction().get(i).getFunctionName());
        }
    }
    public void addFunctionName(BufferedWriter bufferedWriter,String function_name) throws IOException {
        bufferedWriter.write("public static void " + function_name);
        bufferedWriter.write("(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, MalformedURLException ");
        bufferedWriter.write("{");
        bufferedWriter.newLine();
        //add data members and methods here
        generate_code_main(bufferedWriter,createTableList);
        addDataMembersFunction(bufferedWriter,function_name);

        addMethodsPrint(bufferedWriter,function_name);



        bufferedWriter.write("}");
        bufferedWriter.flush();
    }
    public void generate_code_aggfunc()
    {

    }
    public void addDataMembersFunction(BufferedWriter bufferedWriter,String function_name) throws IOException
    {
        int i=0;
        for (String key :  this.functionDeclaration.getScope_function().getSymbolMap().keySet() )
        {
            Symbol s =  this.functionDeclaration.getScope_function().getSymbolMap().get(key);

            if(s.getType()!=null){
                if (s.getType().getName().equals(Type.NUMBER_CONST)) {
                    bufferedWriter.write(double.class.toString() + " " +s.getName()+ " = " +this.functionDeclaration.getBody().getAssigneVars().get(i).getExp().getValue()+ ";");

                    bufferedWriter.newLine();
                }
                if (s.getType().getName().equals(Type.STRING_CONST)) {
                    bufferedWriter.write("String" + " " + s.getName() + " = " +this.functionDeclaration.getBody().getAssigneVars().get(i).getExp().getValue()+ ";");
                    bufferedWriter.newLine();
                }

                if (s.getType().getName().equals(Type.BOOLEAN_CONST)) {
                    bufferedWriter.write(boolean.class.toString() + " " +s.getName()+ " = " +this.functionDeclaration.getBody().getAssigneVars().get(i).getExp().getValue() + ";");
                    bufferedWriter.newLine();
                }
                if(s.getType().getColumns().size()>0)
                {
                    //System.out.println(s.getType().getColumns().get());
//                    for( String key1 : s.getType().getColumns().keySet()) {
//                        if (s.getType().getColumns().get(key1).getName().equals(Type.NUMBER_CONST)) {
//                            bufferedWriter.write("List<" + double.class.toString() + "> " + s.getName() + "  = " + "new ArrayList<>()" + ";");
//                            bufferedWriter.newLine();
//                            bufferedWriter.write(s.getType().getName().substring(2) + ".load_data();");
//                            bufferedWriter.newLine();
//                        }
//                       else if (s.getType().getColumns().get(key1).getName().equals(Type.STRING_CONST)) {
//                            bufferedWriter.write("List<" + "String" + "> " + s.getName() + "  = " + "new ArrayList<>()" + ";");
//                            bufferedWriter.newLine();
//                            bufferedWriter.write(s.getType().getName().substring(2) + ".load_data();");
//                            bufferedWriter.newLine();
//                        }
//                       else if (s.getType().getColumns().get(key1).getName().equals(Type.BOOLEAN_CONST)) {
//                            bufferedWriter.write("List<" + boolean.class.toString() + "> " + s.getName() + "  = " + "new ArrayList<>()" + ";");
//                            bufferedWriter.newLine();
//                            bufferedWriter.write(s.getType().getName().substring(2) + ".load_data();");
//                            bufferedWriter.newLine();
//                        }
//                        else {
//                            bufferedWriter.write("List<" + s.getType().getColumns().get(key1).getName() + "> " + s.getName() + "  = " + "new ArrayList<>()" + ";");
//                            bufferedWriter.newLine();
//                            bufferedWriter.write(s.getName() + " = " + s.getType().getName().substring(2) + ".load_data();");
//                            bufferedWriter.newLine();
//                        }
//                    }
                }

            }
            bufferedWriter.flush();
            i++;
        }




    }

    public void generate_print_stmt(AssigneSelect assigneSelect)
    {
        List<print> printList = new ArrayList<>();
        FunctionDeclaration functionDeclaration = new FunctionDeclaration();
        functionDeclaration=p.getFunctions().get(0);

        for(int i=0;i< functionDeclaration.getBody().getPrints().size();i++)
        {
           printList.add(functionDeclaration.getBody().getPrints().get(i));
        }


    }

    //                List<faculty> list  = new ArrayList<>();
    //                list = faculty.getData();
    // expr ( K_IS | K_IS K_NOT | K_LIKE | K_GLOB | K_MATCH | K_REGEXP ) expr
    public void expr(int ma,String var_name,table_or_subquery table_or_subquery, SelectStmt selectStmt, BufferedWriter bufferedWriter, Expr expr) throws IOException
    {
        System.out.println("exprrrrrrrrrrrrrrr");
        if(expr.getValue()!=null)
        {
            System.out.println("valuuuuuuuuuuuu");
            bufferedWriter.write(expr.getValue());
        }
        else if(expr.getCol_name()!=null)
        {
            System.out.println("!=nullllllllllllllllllllllllllllll");


            if(expr.getTablename()!=null)
            {
                bufferedWriter.write(var_name + ".get(i).");

                bufferedWriter.write(expr.getTablename().getAny_name().getName()+".");
                bufferedWriter.write("get(q).");
                bufferedWriter.write(expr.getCol_name().getAny_name().getName());
            }
            else {
                bufferedWriter.write(var_name + ".get(i).");
                bufferedWriter.write(expr.getCol_name().getAny_name().getName());
            }


        }

        else if(expr.getBool_expr()!=null)
        {
            System.out.println("bol     "+expr.getBool_expr().getBool());
            bufferedWriter.write(expr.getBool_expr().getBool());
        }

        else if(expr.getCall_funcs().size()>0)
        {
            System.out.println("call func  s   "+expr.getCall_funcs().get(0).getArgument_list().getExpr().size());
            System.out.println("call func     "+expr.getCall_funcs().get(0).getFun_name().getFuncname());
            bufferedWriter.write(expr.getCall_funcs().get(0).getFun_name().getFuncname()+"(");
            for(int i=0;i<expr.getCall_funcs().get(0).getArgument_list().getExpr().size();i++)
            {
                if(i!=0)
                {
                    bufferedWriter.write(" , ");
                }
                bufferedWriter.write(expr.getCall_funcs().get(0).getArgument_list().getExpr().get(i).getValue());
            }
            bufferedWriter.write(")");

        }

        else if(expr.getMathimaticalExpr()!=null)
        {
            if(expr.getMathimaticalExpr().getLeft_expr()!=null)
            {
                expr(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getMathimaticalExpr().getLeft_expr());

            }
            String sm=get_symoles(expr.getMathimaticalExpr().getMath().name());
            bufferedWriter.write(" "+sm+" ");
            if(expr.getMathimaticalExpr().getRight_expr()!=null)
            {
                expr(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getMathimaticalExpr().getRight_expr());

            }
        }
        else if(expr.getBinary_expr()!=null)
        {
            if(expr.getBinary_expr().getLeft_expr()!=null)
            {
                expr(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getBinary_expr().getLeft_expr());

            }
            String sb=get_symoles(expr.getBinary_expr().getLogic().name());
            bufferedWriter.write(" "+sb+" ");
            if(expr.getBinary_expr().getRight_expr()!=null)
            {
                expr(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getBinary_expr().getRight_expr());

            }
        }
        else if(expr.getExpr_sql()!=null) {
            String sq = expr.getExpr_sql().getOp_sql().name();
            System.out.println("LLLLLLLLLLLLLLLLL " + sq);
            if (sq.equals("K_IN")||sq.equals("K_NOT_K_IN"))
            {
                if(expr.getExpr_sql().getListexpr()!=null&&expr.getExpr_sql().getListexpr().size()>0) {
                    String TYPE = expr.getExpr_sql().getListexpr().get(0).getValueType();
                    if (TYPE == "number") {
                        TYPE = "Double";
                    }
                    if (TYPE == "string") {
                        TYPE = "String";
                    }
                    if (TYPE == "boolean") {
                        TYPE = "Boolean";
                    }


                    bufferedWriter.write("List<" + TYPE + "> " + "lis_IN"+ma + "  = " + "new ArrayList<>()" + ";");
                    bufferedWriter.newLine();
                    bufferedWriter.write("lis_IN"+ma+".addAll(Arrays.asList( ");
                    int n = 0;
                    for (int i = 0; i < expr.getExpr_sql().getListexpr().size(); i++) {
                        if (expr.getExpr_sql().getListexpr().get(i).getValue() != null) {
                            if (n == 1) {
                                bufferedWriter.write(",");
                            }
                            n = 1;
                            expr(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getListexpr().get(i));
                            //    bufferedWriter.write(expr.getExpr_sql().getListexpr().get(i).getValue());

                        }
                        if (expr.getExpr_sql().getListexpr().get(i).getBool_expr() != null) {
                            if (n == 1) {
                                bufferedWriter.write(",");
                            }
                            n = 1;
                            expr(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getListexpr().get(i));


                        }
                        if (expr.getExpr_sql().getListexpr().get(i).getCol_name() != null) {
                            if (n == 1) {
                                bufferedWriter.write(",");
                            }
                            n = 1;
                            expr(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getListexpr().get(i));
                            //    bufferedWriter.write(expr.getExpr_sql().getListexpr().get(i).getValue());

                        }
                    }
                    bufferedWriter.write(" ));");
                    bufferedWriter.newLine();


///////
                    if (ok1 == false) {
                        bufferedWriter.write("List<" + table_or_subquery.getTb_name().getAny_name().getName() + "> " + "currentlist"+ma + "  = " + "new ArrayList<>()" + ";");
                        bufferedWriter.newLine();
                        bufferedWriter.write("for (int i =0;i<" + var_name+".size()" + ";i++){");
                        bufferedWriter.newLine();
                        if(expr.getExpr_sql().getLeft_expr().getTablename()!=null|| expr.getExpr_sql().getListexpr().get(0).getTablename()!=null)
                        {
                            boo=true;
                            if(expr.getExpr_sql().getLeft_expr().getTablename()!=null)
                            {
                                bufferedWriter.write("for(int q=0;q<"+var_name + ".get(i)."+expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName()+".size()"+";q++){");
                                bufferedWriter.newLine();
                            }
                            else
                            {
                                bufferedWriter.write("for(int q=0;q<"+var_name + ".get(i)."+expr.getExpr_sql().getListexpr().get(0).getTablename().getAny_name().getName()+".size()"+";q++){");
                                bufferedWriter.newLine();
                            }
                        }
                        ok1 = true;
                        if (sq.equals("K_IN")) {
                            bufferedWriter.write("  if( ");
                        }
                        if (sq.equals("K_NOT_K_IN")) {
                            bufferedWriter.write("  if(! ");
                        }
                        bufferedWriter.write("lis_IN"+ma+".contains( ");
                    }
                    if (expr.getExpr_sql().getLeft_expr() != null) {
                        count1++;
                        count2++;
                        expr(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getLeft_expr());

                    }
                    if (ok1 == true && ok2 == false) {
                        bufferedWriter.write(")) {");
                        bufferedWriter.newLine();
                        bufferedWriter.write("    currentlist"+ma+".add("+var_name+".get(i));");
                        bufferedWriter.newLine();
                        bufferedWriter.newLine();
                        bufferedWriter.write("}");
                        bufferedWriter.newLine();
                        bufferedWriter.write("}");
                        bufferedWriter.newLine();
                        if(boo==true)
                        {
                            bufferedWriter.newLine();
                            bufferedWriter.write("}");
                            boo=false;
                        }
                        bufferedWriter.write(var_name+" = currentlist"+ma+" ;");
                        bufferedWriter.newLine();
                        ok2 = true;
                    }
                }
                // ex_res.getSelectStmt()
                if(expr.getExpr_sql().getSelectStmt()!=null)
                {
                    System.out.println("select from in*****************************************");
                }
            }
            if (sq.equals("K_LIKE")) {
                if (ok1 == false) {

                    String TYPE ="";
                    if(expr.getExpr_sql().getLeft_expr().getCol_name()!=null) {
                        for (int t = 0; t < this.createTableList.size(); t++) {
                            if (this.createTableList.get(t).getTb_name().getAny_name().getName().equals(table_or_subquery.getTb_name().getAny_name().getName())) {
                                System.out.println("33333333333333333"+this.createTableList.get(t).getTb_name().getAny_name().getName());
                                if(expr.getExpr_sql().getLeft_expr().getTablename()!=null)
                                {
                                    System.out.println("**************"+expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName());
                                    for (int i = 0; i < this.createTableList.size(); i++) {
                                        for (int j = 0; j < this.createTableList.get(i).getColumn_defs().size(); j++) {
                                            if (this.createTableList.get(i).getColumn_defs().get(j).getCol_name().getAny_name().getName().equals(expr.getExpr_sql().getLeft_expr().getCol_name().getAny_name().getName())) {
                                                System.out.println("col name **** "+expr.getExpr_sql().getLeft_expr().getCol_name().getAny_name().getName());
                                                for (int a = 0; a < this.createTableList.get(i).getColumn_defs().get(j).getType_names().size(); a++) {
                                                    TYPE = this.createTableList.get(i).getColumn_defs().get(j).getType_names().get(a).getNames().getName().getName();
                                                    System.out.println(TYPE + "66666666666666666666666666");

                                                }
                                            }
                                        }
                                    }
                                }
                                else {
                                    for (int c = 0; c < this.createTableList.get(t).getColumn_defs().size(); c++) {
                                        System.out.println("4444444444444444444");
                                        if (this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName().equals(expr.getExpr_sql().getLeft_expr().getCol_name().getAny_name().getName())) {
                                            System.out.println("5555555555555555555555");
                                            for (int y = 0; y < this.createTableList.get(t).getColumn_defs().get(c).getType_names().size(); y++) {
                                                System.out.println("66666666666666666666666666");
                                                TYPE = this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName();
                                                System.out.println(TYPE + "66666666666666666666666666");

                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }

                    else if(expr.getExpr_sql().getLeft_expr().getValue()!=null) {
                        TYPE = expr.getExpr_sql().getLeft_expr().getValueType();
                    }
                    if (TYPE.equals("number")) {
                        TYPE = "Double";
                    }
                    if (TYPE.equals("string")) {
                        TYPE = "String";
                    }
                    if (TYPE.equals("boolean")) {
                        TYPE = "Boolean";
                    }
                    String list_name = "list_";
                    if(expr.getExpr_sql().getLeft_expr().getCol_name()!=null)
                    {
                        list_name= "list_"+expr.getExpr_sql().getLeft_expr().getCol_name().getAny_name().getName();
                    }
                    else if(expr.getExpr_sql().getLeft_expr().getValue()!=null)
                    {
                        list_name="list_"+expr.getExpr_sql().getLeft_expr().getValue();
                    }

                    bufferedWriter.write("List<" + table_or_subquery.getTb_name().getAny_name().getName() + "> " + "currentlist"+ma + "  = " + "new ArrayList<>()" + ";");
                    bufferedWriter.newLine();
                    bufferedWriter.write("List<"+TYPE+">"+list_name+" = new ArrayList<>();");
                    bufferedWriter.newLine();
                    bufferedWriter.write("for (int i =0;i<" + var_name+".size()" + ";i++){");
                    bufferedWriter.newLine();
                    if(expr.getExpr_sql().getLeft_expr().getTablename()!=null|| expr.getExpr_sql().getRight_expr().getTablename()!=null)
                    {
                        boo=true;
                        if(expr.getExpr_sql().getLeft_expr().getTablename()!=null)
                        {
                            bufferedWriter.write("for(int q=0;q<"+var_name + ".get(i)."+expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName()+".size()"+";q++){");
                            bufferedWriter.newLine();
                        }
                        else
                        {
                            bufferedWriter.write("for(int q=0;q<"+var_name + ".get(i)."+expr.getExpr_sql().getRight_expr().getTablename().getAny_name().getName()+".size()"+";q++){");
                            bufferedWriter.newLine();
                        }
                    }
                    bufferedWriter.write(list_name+".add(0,");
                    expr(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getLeft_expr());
                    bufferedWriter.write(");");
                    bufferedWriter.newLine();
                    ok1 = true;
                    bufferedWriter.write("  if( "+list_name);
                }
                if (expr.getExpr_sql().getLeft_expr() != null) {
                    count1++;
                    count2++;
                    System.out.println("left");
                    //  expr(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getLeft_expr());

                }

                //   String sq = expr.getExpr_sql().getOp_sql().name();
                System.out.println("LLLLLLLLLLLLLLLLL " + sq);
                if (sq.equals("K_LIKE")) {
                    bufferedWriter.write(".contains( ");
                }

                if (expr.getExpr_sql().getRight_expr() != null) {
                    System.out.println("right");
                    count1++;
                    expr(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getRight_expr());
                    if (count1 == count2 * 2) {
                        if (ok1 == true && ok2 == false) {
                            bufferedWriter.write(")) {");
                            bufferedWriter.newLine();
                            bufferedWriter.write("    currentlist"+ma+".add("+var_name+".get(i));");
                            bufferedWriter.newLine();
                            bufferedWriter.newLine();
                            bufferedWriter.write("}");
                            bufferedWriter.newLine();
                            bufferedWriter.write("}");
                            bufferedWriter.newLine();
                            if(boo==true)
                            {
                                bufferedWriter.newLine();
                                bufferedWriter.write("}");
                                bufferedWriter.newLine();
                                boo=false;
                            }
                            bufferedWriter.write(var_name+" = currentlist"+ma+" ;");
                            bufferedWriter.newLine();
                            ok2 = true;
                        }
                    }
                }
            }


        }
        else if(expr.getLogicalExpr()!=null)
        {
            if(ok1==false) {
                bufferedWriter.write("List<" + table_or_subquery.getTb_name().getAny_name().getName() + "> " + "currentlist"+ma + "  = " + "new ArrayList<>()" + ";");
                bufferedWriter.newLine();
                bufferedWriter.write("for (int i =0;i<" + var_name+".size()" + ";i++){");
                bufferedWriter.newLine();
                if(expr.getLogicalExpr().getLeft_expr().getTablename()!=null||expr.getLogicalExpr().getRight_expr().getTablename()!=null)
                {
                    boo=true;
                    if(expr.getLogicalExpr().getLeft_expr().getTablename()!=null)
                    {
                        bufferedWriter.write("for(int q=0;q<"+var_name + ".get(i)."+expr.getLogicalExpr().getLeft_expr().getTablename().getAny_name().getName()+".size()"+";q++){");
                        bufferedWriter.newLine();
                    }
                    else
                    {
                        bufferedWriter.write("for(int q=0;q<"+var_name + ".get(i)."+expr.getLogicalExpr().getRight_expr().getTablename().getAny_name().getName()+".size()"+";q++){");
                        bufferedWriter.newLine();
                    }
                }
                ok1=true;
                bufferedWriter.write("  if( ");
            }
            if(expr.getLogicalExpr().getLeft_expr()!=null)
            {
                count1++;
                count2++;
                expr(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getLogicalExpr().getLeft_expr());

            }
            String sl=get_symoles(expr.getLogicalExpr().getlogic().name());
            bufferedWriter.write(" "+sl+" ");
            if(expr.getLogicalExpr().getRight_expr()!=null)
            {
                count1++;
                expr(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getLogicalExpr().getRight_expr());
                if(count1==count2*2)
                {
                    if(ok1==true&& ok2==false)
                    {
                        bufferedWriter.write(") {");
                        bufferedWriter.newLine();
                        bufferedWriter.write("    currentlist"+ma+".add("+var_name+".get(i));");
                        bufferedWriter.newLine();
                        bufferedWriter.newLine();
                        bufferedWriter.write("}");
                        bufferedWriter.newLine();
                        bufferedWriter.write("}");
                        if(boo==true)
                        {
                            bufferedWriter.newLine();
                            bufferedWriter.write("}");
                            boo=false;
                        }
                        bufferedWriter.newLine();
                        bufferedWriter.write(var_name+" = currentlist"+ma+" ;");
                        bufferedWriter.newLine();
                        ok2=true;
                    }
                }
            }

        }
        else if(expr.getIncrDecr()!=null)
        {
            System.out.println("increment                  "+expr.getIncrDecr().getLogic().name());
            if(expr.getIncrDecr().getLogic().name().equals("PLUS_PLUSr"))
            {
                bufferedWriter.write( "++");
            }
            if(expr.getIncrDecr().getLogic().name().equals("MINUS_MINUSr"))
            {
                bufferedWriter.write( "--");
            }
            bufferedWriter.write( expr.getIncrDecr().getValue());
            if(expr.getIncrDecr().getLogic().name().equals("PLUS_PLUSl"))
            {
                System.out.println("increment                  "+expr.getIncrDecr().getLogic().name());
                bufferedWriter.write( "++");
            }
            if(expr.getIncrDecr().getLogic().name().equals("MINUS_MINUSl"))
            {
                bufferedWriter.write( "--");
            }
        }
        else if(expr.getUnaryExpr()!=null)
        {
            if(expr.getUnaryExpr().getUnary().name().equals("MINUS"))
            {
                bufferedWriter.write("- ");
            }
            if(expr.getUnaryExpr().getUnary().name().equals("PLUSE"))
            {
                bufferedWriter.write("+ ");
            }
            if(expr.getUnaryExpr().getTerm()!=null)
            {
                expr(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getUnaryExpr().getTerm());
            }

        }





    }

    List<String> table_name = new ArrayList<>();
    List<String> column_name = new ArrayList<>();

    public void expr_join_where(int ma,String var_name,table_or_subquery table_or_subquery, SelectStmt selectStmt, BufferedWriter bufferedWriter, Expr expr) throws IOException
    {
        System.out.println("exprrrrrrrrrrrrrrr");
        if(expr.getValue()!=null)
        {
            System.out.println("valuuuuuuuuuuuu");
            bufferedWriter.write(expr.getValue());
        }
        else if(expr.getCol_name()!=null)
        {
            System.out.println("!=nullllllllllllllllllllllllllllll");
            table++;

//            bufferedWriter.write("x" + "_list" + ".get(" + "i_" + "x" + ").");
//            bufferedWriter.write(expr.getCol_name().getAny_name().getName());

            if(expr.getDatabasename()!=null)
            {
                System.out.println("dddddddddddddddddddddd");
                bufferedWriter.write(expr.getDatabasename().getAny_name().getName() + "_list_"+join_table_count + ".get(" + "i_" + expr.getDatabasename().getAny_name().getName() + ").");
                bufferedWriter.write(expr.getTablename().getAny_name().getName()+".");
                bufferedWriter.write(expr.getCol_name().getAny_name().getName());
                //bufferedWriter.newLine();

            }
            else  {
                //table_name.add(expr.getTablename().getAny_name().getName());
                //System.out.println("00000000000000000000 : "+expr.getTablename().getAny_name().getName());
                //bufferedWriter.write(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(table).getTb_name().getAny_name().getName()+"_list" + ".get(i"+table+").");
                bufferedWriter.write(expr.getTablename().getAny_name().getName() + "_list_"+join_table_count+ ".get(" + "i_" + expr.getTablename().getAny_name().getName() + ").");
                bufferedWriter.write(expr.getCol_name().getAny_name().getName());
                //bufferedWriter.newLine();

            }



           //column_name.add(expr.getCol_name().getAny_name().getName());

        }

        else if(expr.getBool_expr()!=null)
        {
            System.out.println("bol     "+expr.getBool_expr().getBool());
            bufferedWriter.write(expr.getBool_expr().getBool());
        }

        else if(expr.getCall_funcs().size()>0)
        {
            System.out.println("call func  s   "+expr.getCall_funcs().get(0).getArgument_list().getExpr().size());
            System.out.println("call func     "+expr.getCall_funcs().get(0).getFun_name().getFuncname());
            bufferedWriter.write(expr.getCall_funcs().get(0).getFun_name().getFuncname()+"(");
            for(int i=0;i<expr.getCall_funcs().get(0).getArgument_list().getExpr().size();i++)
            {
                if(i!=0)
                {
                    bufferedWriter.write(" , ");
                }
                bufferedWriter.write(expr.getCall_funcs().get(0).getArgument_list().getExpr().get(i).getValue());
            }
            bufferedWriter.write(")");

        }

        else if(expr.getMathimaticalExpr()!=null)
        {
            if(expr.getMathimaticalExpr().getLeft_expr()!=null)
            {
                expr_join_where(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getMathimaticalExpr().getLeft_expr());

            }
            String sm=get_symoles(expr.getMathimaticalExpr().getMath().name());
            bufferedWriter.write(" "+sm+" ");
            if(expr.getMathimaticalExpr().getRight_expr()!=null)
            {
                expr_join_where(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getMathimaticalExpr().getRight_expr());

            }
        }
        else if(expr.getBinary_expr()!=null)
        {
            if(expr.getBinary_expr().getLeft_expr()!=null)
            {
                expr_join_where(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getBinary_expr().getLeft_expr());

            }
            String sb=get_symoles(expr.getBinary_expr().getLogic().name());
            bufferedWriter.write(" "+sb+" ");
            if(expr.getBinary_expr().getRight_expr()!=null)
            {
                expr_join_where(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getBinary_expr().getRight_expr());

            }
        }
        else if(expr.getExpr_sql()!=null) {
            String sq = expr.getExpr_sql().getOp_sql().name();
            System.out.println("LLLLLLLLLLLLLLLLL " + sq);
            if (sq.equals("K_IN")||sq.equals("K_NOT_K_IN"))
            {
                if(expr.getExpr_sql().getListexpr()!=null&&expr.getExpr_sql().getListexpr().size()>0) {
                    String TYPE = expr.getExpr_sql().getListexpr().get(0).getValueType();
                    if (TYPE == "number") {
                        TYPE = "Double";
                    }
                    if (TYPE == "string") {
                        TYPE = "String";
                    }
                    if (TYPE == "boolean") {
                        TYPE = "Boolean";
                    }


                    bufferedWriter.write("List<" + TYPE + "> " + "lis_IN"+ma + "  = " + "new ArrayList<>()" + ";");
                    bufferedWriter.newLine();
                    bufferedWriter.write("lis_IN"+ma+".addAll(Arrays.asList( ");
                    int n = 0;
                    for (int i = 0; i < expr.getExpr_sql().getListexpr().size(); i++) {
                        if (expr.getExpr_sql().getListexpr().get(i).getValue() != null) {
                            if (n == 1) {
                                bufferedWriter.write(",");
                            }
                            n = 1;
                            expr_join_where(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getListexpr().get(i));
                            //    bufferedWriter.write(expr.getExpr_sql().getListexpr().get(i).getValue());

                        }
                        if (expr.getExpr_sql().getListexpr().get(i).getBool_expr() != null) {
                            if (n == 1) {
                                bufferedWriter.write(",");
                            }
                            n = 1;
                            expr_join_where(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getListexpr().get(i));


                        }
                        if (expr.getExpr_sql().getListexpr().get(i).getCol_name() != null) {
                            if (n == 1) {
                                bufferedWriter.write(",");
                            }
                            n = 1;
                            expr_join_where(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getListexpr().get(i));
                            //    bufferedWriter.write(expr.getExpr_sql().getListexpr().get(i).getValue());

                        }
                    }
                    bufferedWriter.write(" ));");
                    bufferedWriter.newLine();


///////
                    if (ok1 == false) {
//                        bufferedWriter.write("List<" + table_or_subquery.getTb_name().getAny_name().getName()+ "> " + "currentlist"+ma + "  = " + "new ArrayList<>()" + ";");
//                        bufferedWriter.newLine();
//                        bufferedWriter.write("for (int i =0;i<" + table_or_subquery.getTb_name().getAny_name().getName()+"_list"+".size()" + ";i++){");
//                        bufferedWriter.newLine();
                        ok1 = true;
                        if (sq.equals("K_IN")) {
                            bufferedWriter.write("  if( ");
                        }
                        if (sq.equals("K_NOT_K_IN")) {
                            bufferedWriter.write("  if(! ");
                        }
                        bufferedWriter.write("lis_IN"+ma+".contains( ");
                    }
                    if (expr.getExpr_sql().getLeft_expr() != null) {
                        count1++;
                        count2++;
                        expr_join_where(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getLeft_expr());

                    }
                    if (ok1 == true && ok2 == false) {
                        bufferedWriter.write(")) {");
                        bufferedWriter.newLine();
                        //bufferedWriter.write(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(table).getTb_name().getAny_name().getName()+"_list" + ".get(i"+table+").");
                        //bufferedWriter.write(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(table).getTb_name().getAny_name().getName()+"_list" + ".get(i"+table+").");
//                        bufferedWriter.write(expr.getTablename().getAny_name().getName()+"_list" + ".get("+"i_"+expr.getTablename().getAny_name().getName()+").");
//                        bufferedWriter.write(expr.getCol_name().getAny_name().getName());
                        for (int i=0;i<table_name.size();i++)
                        {

                                bufferedWriter.write(table_name.get(i)+"_current_list_"+join_table_count+".add("+table_name.get(i)+"_list_"+join_table_count+".get(i_"+table_name.get(i)+"));");
                                bufferedWriter.newLine();

                        }
                        for (int t = 0; t < selectStmt.getJoinclaus().getTable_or_subqueries().size(); t++) {
                            for (int c = 0; c < selectStmt.getColumnItem().size(); c++) {
                                for (int f = 0; f < selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().size(); f++) {
                                    for (int p = 0; p < selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size(); p++) {
                                        if (selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons() != null) {
                                            System.out.println("0000000000000000000000000000000000");
                                            if (selectStmt.getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName().equals(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0))) {
                                                bufferedWriter.write("list" + "_" + var_name+"_"+main_count) ;
                                                bufferedWriter.write(".add(" + selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+ "_list_"+join_table_count+ ".get(i_"+selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+").");
                                                for(int i=1;i<selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().size();i++) {


                                                    bufferedWriter.write(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(i));

                                                }
                                                bufferedWriter.write(");");
                                                bufferedWriter.newLine();
                                            }


                                        } else{
                                            bufferedWriter.write(selectStmt.getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + var_name + ".add(" + var_name + ".get(i).");
                                            bufferedWriter.write(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
                                            bufferedWriter.newLine();
                                        }
                                    }
                                }
                            }
                        }
                        /////////// bufferedWriter.write(expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName()+"_current_list"+".add("+expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName()+"_list"+".get(i_"+expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName()+"));");
                        //bufferedWriter.write(expr.getTablename().getAny_name().getName()+"_list" + ".get("+"i_"+expr.getTablename().getAny_name().getName()+").");
                        //bufferedWriter.write(expr.getCol_name().getAny_name().getName());
                        bufferedWriter.newLine();
                        bufferedWriter.newLine();
                        bufferedWriter.write("}");
                        bufferedWriter.newLine();
                        //bufferedWriter.write(expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName()+"_list"+" = currentlist"+ma+" ;");
                        bufferedWriter.newLine();
                        ok2 = true;
                    }
                }
                // ex_res.getSelectStmt()
                if(expr.getExpr_sql().getSelectStmt()!=null)
                {
                    System.out.println("select from in*****************************************");
                }
            }
            if (sq.equals("K_LIKE")) {
                if (ok1 == false) {

                    String TYPE ="";
                    if(expr.getExpr_sql().getLeft_expr().getCol_name()!=null) {
                        for (int t = 0; t < this.createTableList.size(); t++) {
                            if (this.createTableList.get(t).getTb_name().getAny_name().getName().equals(table_or_subquery.getTb_name().getAny_name().getName())) {
                                System.out.println("33333333333333333"+this.createTableList.get(t).getTb_name().getAny_name().getName());
                                if(expr.getExpr_sql().getLeft_expr().getTablename()!=null)
                                {
                                    System.out.println("**************"+expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName());
                                    for (int i = 0; i < this.createTableList.size(); i++) {
                                        for (int j = 0; j < this.createTableList.get(i).getColumn_defs().size(); j++) {
                                            if (this.createTableList.get(i).getColumn_defs().get(j).getCol_name().getAny_name().getName().equals(expr.getExpr_sql().getLeft_expr().getCol_name().getAny_name().getName())) {
                                                System.out.println("col name **** "+expr.getExpr_sql().getLeft_expr().getCol_name().getAny_name().getName());
                                                for (int a = 0; a < this.createTableList.get(i).getColumn_defs().get(j).getType_names().size(); a++) {
                                                    TYPE = this.createTableList.get(i).getColumn_defs().get(j).getType_names().get(a).getNames().getName().getName();
                                                    System.out.println(TYPE + "66666666666666666666666666");

                                                }
                                            }
                                        }
                                    }
                                }
                                else {
                                    for (int c = 0; c < this.createTableList.get(t).getColumn_defs().size(); c++) {
                                        System.out.println("4444444444444444444");
                                        if (this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName().equals(expr.getExpr_sql().getLeft_expr().getCol_name().getAny_name().getName())) {
                                            System.out.println("5555555555555555555555");
                                            for (int y = 0; y < this.createTableList.get(t).getColumn_defs().get(c).getType_names().size(); y++) {
                                                System.out.println("66666666666666666666666666");
                                                TYPE = this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName();
                                                System.out.println(TYPE + "66666666666666666666666666");

                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }

                    else if(expr.getExpr_sql().getLeft_expr().getValue()!=null) {
                        TYPE = expr.getExpr_sql().getLeft_expr().getValueType();
                    }
                    if (TYPE.equals("number")) {
                        TYPE = "Double";
                    }
                    if (TYPE.equals("string")) {
                        TYPE = "String";
                    }
                    if (TYPE.equals("boolean")) {
                        TYPE = "Boolean";
                    }
                    String list_name = "list_";
                    if(expr.getExpr_sql().getLeft_expr().getCol_name()!=null)
                    {
                        list_name= "list_"+expr.getExpr_sql().getLeft_expr().getCol_name().getAny_name().getName();
                    }
                    else if(expr.getExpr_sql().getLeft_expr().getValue()!=null)
                    {
                        list_name="list_"+expr.getExpr_sql().getLeft_expr().getValue();
                    }

                    //bufferedWriter.write("List<" + table_or_subquery.getTb_name().getAny_name().getName() + "> " + "currentlist"+ma + "  = " + "new ArrayList<>()" + ";");
                    bufferedWriter.newLine();
                    bufferedWriter.write("List<"+TYPE+">"+list_name+" = new ArrayList<>();");
                    bufferedWriter.newLine();
                   // bufferedWriter.write("for (int i =0;i<" + var_name+".size()" + ";i++){");
                    bufferedWriter.newLine();
//                    if(expr.getExpr_sql().getLeft_expr().getTablename()!=null|| expr.getExpr_sql().getRight_expr().getTablename()!=null)
//                    {
//                        boo=true;
//                        if(expr.getExpr_sql().getLeft_expr().getTablename()!=null)
//                        {
//                            bufferedWriter.write("for(int q=0;q<"+var_name + ".get(i)."+expr.getExpr_sql().getLeft_expr().getTablename().getAny_name().getName()+".size()"+";q++){");
//                            bufferedWriter.newLine();
//                        }
//                        else
//                        {
//                            bufferedWriter.write("for(int q=0;q<"+var_name + ".get(i)."+expr.getExpr_sql().getRight_expr().getTablename().getAny_name().getName()+".size()"+";q++){");
//                            bufferedWriter.newLine();
//                        }
//                    }
                    bufferedWriter.write(list_name+".add(0,");
                    expr_join_where(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getLeft_expr());
                    bufferedWriter.write(");");
                    bufferedWriter.newLine();
                    ok1 = true;
                    bufferedWriter.write("  if( "+list_name);
                }
                if (expr.getExpr_sql().getLeft_expr() != null) {
                    count1++;
                    count2++;
                    System.out.println("left");
                    //  expr(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getLeft_expr());

                }

                //   String sq = expr.getExpr_sql().getOp_sql().name();
                System.out.println("LLLLLLLLLLLLLLLLL " + sq);
                if (sq.equals("K_LIKE")) {
                    bufferedWriter.write(".contains( ");
                }

                if (expr.getExpr_sql().getRight_expr() != null) {
                    System.out.println("right");
                    count1++;
                    expr_join_where(ma,var_name,table_or_subquery, selectStmt, bufferedWriter, expr.getExpr_sql().getRight_expr());
                    if (count1 == count2 * 2) {
                        if (ok1 == true && ok2 == false) {
                            bufferedWriter.write(")) {");
                            bufferedWriter.newLine();
                           // bufferedWriter.write("    currentlist"+ma+".add("+var_name+".get(i));");
                            System.out.println("oooooooooooooooooooooo :"+table_name.size());
                            for (int i=0;i<table_name.size();i++)
                            {
                                System.out.println("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
                                bufferedWriter.write(table_name.get(i)+"_current_list_"+join_table_count+".add("+table_name.get(i)+"_list_"+join_table_count+".get(i_"+table_name.get(i)+"));");
                                bufferedWriter.newLine();

                            }
                            for (int t = 0; t < selectStmt.getJoinclaus().getTable_or_subqueries().size(); t++) {
                                for (int c = 0; c < selectStmt.getColumnItem().size(); c++) {
                                    for (int f = 0; f < selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().size(); f++) {
                                        for (int p = 0; p < selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size(); p++) {
                                            if (selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons() != null) {
                                                System.out.println("0000000000000000000000000000000000");
                                                if (selectStmt.getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName().equals(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0))) {
                                                    bufferedWriter.write("list" + "_" + var_name+"_"+main_count) ;
                                                    bufferedWriter.write(".add(" + selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+ "_list_" +join_table_count+ ".get(i_"+selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+").");
                                                    for(int i=1;i<selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().size();i++) {


                                                        bufferedWriter.write(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(i));

                                                    }
                                                    bufferedWriter.write(");");
                                                    bufferedWriter.newLine();
                                                }


                                            } else{
                                                bufferedWriter.write(selectStmt.getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + var_name + ".add(" + var_name + ".get(i).");
                                                bufferedWriter.write(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
                                                bufferedWriter.newLine();
                                            }
                                        }
                                    }
                                }
                            }
                            bufferedWriter.newLine();
                            bufferedWriter.newLine();
                            bufferedWriter.write("}");
                            bufferedWriter.newLine();
                            //bufferedWriter.write("}");
                            bufferedWriter.newLine();
                            if(boo==true)
                            {
                                bufferedWriter.newLine();
                                bufferedWriter.write("}");
                                bufferedWriter.newLine();
                                boo=false;
                            }
                            //bufferedWriter.write(var_name+" = currentlist"+ma+" ;");
                            bufferedWriter.newLine();
                            ok2 = true;
                        }
                    }
                }
            }


        }
        else if(expr.getLogicalExpr()!=null)
        {
            if(ok1==false) {
//                bufferedWriter.write("List<" + table_or_subquery.getTb_name().getAny_name().getName() + "> " + "currentlist"+ma + "  = " + "new ArrayList<>()" + ";");
//                bufferedWriter.newLine();
//                bufferedWriter.write("for (int i =0;i<" + var_name+".size()" + ";i++){");
//                bufferedWriter.newLine();
                ok1=true;
                bufferedWriter.write("  if( ");
            }
            if(expr.getLogicalExpr().getLeft_expr()!=null)
            {
                count1++;
                count2++;
                expr_join_where(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getLogicalExpr().getLeft_expr());

            }
            String sl=get_symoles(expr.getLogicalExpr().getlogic().name());
            bufferedWriter.write(" "+sl+" ");
            if(expr.getLogicalExpr().getRight_expr()!=null)
            {
                count1++;
                expr_join_where(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getLogicalExpr().getRight_expr());
                if(count1==count2*2)
                {
                    if(ok1==true&& ok2==false)
                    {
                        bufferedWriter.write(") {");
                        bufferedWriter.newLine();
                       // bufferedWriter.write("    currentlist"+ma+".add("+var_name+".get(i));");
                        for (int i = 0; i < selectStmt.getJoinclaus().getTable_or_subqueries().size(); i++) {
                            bufferedWriter.write(selectStmt.getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_current_list_"+join_table_count+".add(" + selectStmt.getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list_"+join_table_count+".get(" + "i_" + selectStmt.getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + ")" + ");");
                            bufferedWriter.newLine();
                        }
                        for (int t = 0; t < selectStmt.getJoinclaus().getTable_or_subqueries().size(); t++) {
                            for (int c = 0; c < selectStmt.getColumnItem().size(); c++) {
                                for (int f = 0; f < selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().size(); f++) {
                                    for (int p = 0; p < selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size(); p++) {
                                        if (selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons() != null) {
                                            System.out.println("0000000000000000000000000000000000");
                                            if (selectStmt.getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName().equals(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0))) {
                                                bufferedWriter.write("list" + "_" + var_name+"_"+main_count) ;
                                                bufferedWriter.write(".add(" + selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+ "_list_"+join_table_count + ".get(i_"+selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+").");
                                                for(int i=1;i<selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().size();i++) {


                                                    bufferedWriter.write(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(i));

                                                }
                                                bufferedWriter.write(");");
                                                bufferedWriter.newLine();
                                            }


                                        } else{
                                            bufferedWriter.write(selectStmt.getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + var_name + ".add(" + var_name + ".get(i).");
                                            bufferedWriter.write(selectStmt.getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
                                            bufferedWriter.newLine();
                                        }
                                    }
                                }
                            }
                        }
                        bufferedWriter.newLine();
                        bufferedWriter.newLine();
                        bufferedWriter.write("}");
                        bufferedWriter.newLine();
                       // bufferedWriter.write(var_name+" = currentlist"+ma+" ;");
                        bufferedWriter.newLine();
                        ok2=true;
                    }
                }
            }

        }
        else if(expr.getIncrDecr()!=null)
        {
            System.out.println("increment                  "+expr.getIncrDecr().getLogic().name());
            if(expr.getIncrDecr().getLogic().name().equals("PLUS_PLUSr"))
            {
                bufferedWriter.write( "++");
            }
            if(expr.getIncrDecr().getLogic().name().equals("MINUS_MINUSr"))
            {
                bufferedWriter.write( "--");
            }
            bufferedWriter.write( expr.getIncrDecr().getValue());
            if(expr.getIncrDecr().getLogic().name().equals("PLUS_PLUSl"))
            {
                System.out.println("increment                  "+expr.getIncrDecr().getLogic().name());
                bufferedWriter.write( "++");
            }
            if(expr.getIncrDecr().getLogic().name().equals("MINUS_MINUSl"))
            {
                bufferedWriter.write( "--");
            }
        }
        else if(expr.getUnaryExpr()!=null)
        {
            if(expr.getUnaryExpr().getUnary().name().equals("MINUS"))
            {
                bufferedWriter.write("- ");
            }
            if(expr.getUnaryExpr().getUnary().name().equals("PLUSE"))
            {
                bufferedWriter.write("+ ");
            }
            if(expr.getUnaryExpr().getTerm()!=null)
            {
                expr_join_where(ma,var_name,table_or_subquery,selectStmt,bufferedWriter,expr.getUnaryExpr().getTerm());
            }

        }


    }

    public String get_symoles(String s)
    {
        String symbol="";
        /////////////ma//////////////////
        if( s=="PLUS")
        {
            symbol= "+";
        }
        if( s=="STAR")
        {
            symbol= "*";
        }
        if( s=="MINUS")
        {
            symbol= "-";
        }
        if( s=="DIV")
        {
            symbol= "/";
        }
        if( s=="MOD")
        {
            symbol= "%";
        }
        if( s=="PLUS_EQ")
        {
            symbol= "+=";
        }
        if( s=="MINUS_EQ")
        {
            symbol= "-=";
        }
        if( s=="STAR_EQ")
        {
            symbol= "*=";
        }
        if( s=="DIV_EQ")
        {
            symbol= "/=";
        }
        if( s=="MOD_EQ")
        {
            symbol= "%=";
        }
        if( s=="ASSIGN")
        {
            symbol= "=";
        }
        /////////////////////lo///////////////////////

        if( s=="GT")
        {
            symbol= ">";
        }
        if( s=="LT")
        {
            symbol= "<";
        }
        if( s=="GT_EQ")
        {
            symbol= ">=";
        }
        if( s=="LT_EQ")
        {
            symbol= "<=";
        }
        if( s=="EQ")
        {
            symbol= "==";
        }
        if( s=="NOT_EQ1")
        {
            symbol= "!=";
        }
        if( s=="NOT_EQ2")
        {
            symbol= "<>";
        }
        if( s=="K_AND")
        {
            symbol= "and";
        }
        if( s=="K_OR")
        {
            symbol= "or";
        }
        if( s=="PIPE2")
        {
            symbol= "||";
        }
        if( s=="AMP2")
        {
            symbol= "&&";
        }
        ////////bi///////////////
        if( s=="GT2")
        {
            symbol= ">>";
        }
        if( s=="LT2")
        {
            symbol= "<<";
        }
        if( s=="AMP")
        {
            symbol= "&";
        }
        if( s=="PIPE")
        {
            symbol= "|";
        }
        //////////////////sql/////////
        if( s=="K_IS")
        {
            symbol= "IS";
        }
        if( s=="K_NOT")
        {
            symbol= "NOT";
        }
        if( s=="K_IN")
        {
            symbol= "IN";
        }
        if( s=="K_NOT_K_IN")
        {
            symbol= "NOT IN";
        }

        if( s=="K_LIKE")
        {
            symbol= "LIKE";
        }
        if( s=="K_GLOB")
        {
            symbol= "GLOB";
        }
        if( s=="K_MATCH")
        {
            symbol= "MATCH";
        }
        if( s=="K_REGEXP")
        {
            symbol= "REGEXP";
        }
        if( s=="K_EXISTS")
        {
            symbol= "EXISTS";
        }

        return symbol;

    }

    public void expr_join(BufferedWriter bufferedWriter,Expr expr,AssigneSelect assigneSelect) throws IOException
    {
       //bufferedWriter.write("if(");

        String tbname="";
        if(expr.getValue()!=null)
        {
            System.out.println("valuuuuuuuuuuuu");
            bufferedWriter.write(expr.getValue());
        }
//        else if(expr.getTablename()!=null)
//        {
//            tbname=expr.getTablename().getAny_name().getName();
//           // bufferedWriter.write(expr.getTablename().getAny_name().getName());
//        }
        else if(expr.getCol_name()!=null)
        {
           System.out.println("!=nullllllllllllllllllllllllllllll");
            table++;


                //bufferedWriter.write(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(table).getTb_name().getAny_name().getName()+"_list" + ".get(i"+table+").");
                bufferedWriter.write(expr.getTablename().getAny_name().getName()+"_list_"+join_table_count + ".get("+"i_"+expr.getTablename().getAny_name().getName()+").");
                bufferedWriter.write(expr.getCol_name().getAny_name().getName());
                table_name.add(expr.getTablename().getAny_name().getName());
                System.out.println("bbbbbbbbbnnnnnnmmmmmmmm : "+table_name.size());

        }
        else if(expr.getBool_expr()!=null)
        {
            System.out.println("bol     "+expr.getBool_expr().getBool());
            bufferedWriter.write(expr.getBool_expr().getBool());
        }

        else if(expr.getMathimaticalExpr()!=null)
        {
            if(expr.getMathimaticalExpr().getLeft_expr()!=null)
            {
                expr_join(bufferedWriter,expr.getMathimaticalExpr().getLeft_expr(),assigneSelect);

            }
            String sm=get_symoles(expr.getMathimaticalExpr().getMath().name());
            if(sm.equals("="))
            {
                sm = "==";
            }
            bufferedWriter.write(" "+sm+" ");
            if(expr.getMathimaticalExpr().getRight_expr()!=null)
            {
                expr_join(bufferedWriter,expr.getMathimaticalExpr().getRight_expr(),assigneSelect);

            }
        }

        else if(expr.getLogicalExpr()!=null)
        {
            System.out.println("expr.getMathimaticalExpr()");
            if(expr.getLogicalExpr().getLeft_expr()!=null)
            {
                expr_join(bufferedWriter,expr.getLogicalExpr().getLeft_expr(),assigneSelect);

            }
            String sm=get_symoles(expr.getLogicalExpr().getlogic().name());
            bufferedWriter.write(" "+sm+" ");
            if(expr.getLogicalExpr().getRight_expr()!=null)
            {
                expr_join(bufferedWriter,expr.getLogicalExpr().getRight_expr(),assigneSelect);

            }
        }




//       bufferedWriter.write("){");
//       bufferedWriter.newLine();

    }

    public void join_constraint(BufferedWriter bufferedWriter,AssigneSelect assigneSelect) throws IOException
    {

        if(assigneSelect.getSelectStmt().getWhereItem()!=null)
        {
            System.out.println("K_WHERE");
            if (assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().size() > 0) {//System.out.println("fffffffffffffffffffffffffffffffffffffexp");


                bufferedWriter.write("if(");
                for (int j = 0; j < assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().size(); j++) {
//                if(assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().get(j).getMathimaticalExpr()!=null)
//                {
//                    System.out.println("getMathimaticalExpr");
//                    //assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().get(j).getMathimaticalExpr().getRight_expr().
//                }
//                if(assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().get(j).getLogicalExpr()!=null)
//                {
//                    System.out.println("getLogicalExpr");
//                }
                    //bufferedWriter.write("if(");
                    //for(int i=0;i<assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++)
                    expr_join(bufferedWriter, assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().get(j), assigneSelect);

                }
            }
            bufferedWriter.write("){");

            bufferedWriter.newLine();

//            bufferedWriter.write("if(");
//
//            expr_join(bufferedWriter, assigneSelect.getSelectStmt().getWhereItem(), assigneSelect);
//
//            bufferedWriter.write("){");
//            bufferedWriter.newLine();
            //if(assigneSelect.getSelectStmt().getWhereItem()!=null)
            //{
                System.out.println("whereeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");


                for (int i = 0; i < assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().size(); i++) {

                    ok1=false;
                    ok2=false;
                    count1=0;
                    count2=0;
                    majd++;
                    expr_join_where(majd, assigneSelect.getVar(), assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i), assigneSelect.getSelectStmt(), bufferedWriter, assigneSelect.getSelectStmt().getWhereItem());
                    break;
                }



            //}
//            for (int i = 0; i < assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().size(); i++) {
//                bufferedWriter.write(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_current_list.add(" + assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list.get(" + "i_" + assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + ")" + ");");
//                bufferedWriter.newLine();
//            }
            bufferedWriter.write("}");

           // bufferedWriter.write("}");
        }
       else {

            System.out.println("K_ON");
            if (assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().size() > 0) {//System.out.println("fffffffffffffffffffffffffffffffffffffexp");


                bufferedWriter.write("if(");
                for (int j = 0; j < assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().size(); j++) {
//                if(assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().get(j).getMathimaticalExpr()!=null)
//                {
//                    System.out.println("getMathimaticalExpr");
//                    //assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().get(j).getMathimaticalExpr().getRight_expr().
//                }
//                if(assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().get(j).getLogicalExpr()!=null)
//                {
//                    System.out.println("getLogicalExpr");
//                }
                    //bufferedWriter.write("if(");
                    //for(int i=0;i<assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++)
                    expr_join(bufferedWriter, assigneSelect.getSelectStmt().getJoinclaus().getJoin_constrain().get(j), assigneSelect);

                }
            }
            bufferedWriter.write("){");
            bufferedWriter.newLine();

            for (int i = 0; i < assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().size(); i++) {
                bufferedWriter.write(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_current_list_"+join_table_count+".add(" + assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list_"+join_table_count+".get(" + "i_" + assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + ")" + ");");
                bufferedWriter.newLine();
            }

               for (int t = 0; t < assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().size(); t++) {
                   for (int c = 0; c < assigneSelect.getSelectStmt().getColumnItem().size(); c++) {
                       for (int f = 0; f < assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().size(); f++) {
                           for (int p = 0; p < assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size(); p++) {
                               if (assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons() != null) {
                                   if (assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName().equals(assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0))) {
                                       bufferedWriter.write("list" + "_" + assigneSelect.getVar()+"_"+main_count) ;
                                       bufferedWriter.write(".add(" + assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+ "_list_"+join_table_count + ".get(i_"+assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+").");
                                       for(int i=1;i<assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().size();i++) {


                                           bufferedWriter.write(assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(i));

                                       }
                                       bufferedWriter.write(");");
                                       bufferedWriter.newLine();
                                   }

                               } else{
                                   bufferedWriter.write("list" + "_" + assigneSelect.getVar()+"_"+main_count + ".add(" + assigneSelect.getVar() + ".get(i).");
                                   bufferedWriter.write(assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
                                   bufferedWriter.newLine();
                               }


                           }

                       }
                   }
               }

            bufferedWriter.write("}");

        }


//                                     for (int t = 0; t < assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().size(); t++) {
//                                         for (int c = 0; c < assigneSelect.getSelectStmt().getColumnItem().size(); c++) {
//                                             for (int f = 0; f < assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().size(); f++) {
//                                                 for (int p = 0; p < assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size(); p++) {
//                                                     if (assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons() != null) {
//                                                            if (assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName().equals(assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0))) {
//                                                                bufferedWriter.write(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelect.getVar() +
//
//                                                                        ".add(" + assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0) + "_current_list" + ".get(i).");
//                                                                bufferedWriter.write(assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(1) + ");");
//
//                                                                bufferedWriter.newLine();
//                                                            }
//
//
//                                                      } else{
//                                                                bufferedWriter.write(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelect.getVar() + ".add(" + assigneSelect.getVar() + ".get(i).");
//                                                                bufferedWriter.write(assigneSelect.getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
//                                                                bufferedWriter.newLine();
//                                                            }
//                                                 }
//                                             }
//                                         }
//                                     }

    }
    public void Aggregation_loader_jar(BufferedWriter bufferedWriter,String func_name,String varname) throws IOException
    {
        agg_count++;

        for(int i=0;i<this.symbolTable.getDeclaredAggregationFunction().size();i++)
        {
            if(this.symbolTable.getDeclaredAggregationFunction().get(i).getFunction_name().getAny_name().getName().equals(func_name))
            {
               //String JarPath = "C://Users//katren//Desktop//java_example_generate//out//artifacts//aggre_jar//";
                //        String JarName = "aggre.jar";
                //        String ClassName = "CommonAggregations";
                //        String MethodName = "Sum";

                bufferedWriter.write("String JarPath_"+varname+"_"+agg_count+" = "+this.symbolTable.getDeclaredAggregationFunction().get(i).getJarParth()+";");
                bufferedWriter.newLine();
                bufferedWriter.write("String JarName_"+varname+"_"+agg_count+" = "+'"'+this.symbolTable.getDeclaredAggregationFunction().get(i).getClassName()+".jar"+'"'+";");
                bufferedWriter.newLine();
                bufferedWriter.write("String ClassName_"+varname+"_"+agg_count+" = "+'"'+this.symbolTable.getDeclaredAggregationFunction().get(i).getClassName()+'"'+";");
                bufferedWriter.newLine();
                bufferedWriter.write("String MethodName_"+varname+"_"+agg_count+" = "+'"'+this.symbolTable.getDeclaredAggregationFunction().get(i).getFunction_name().getAny_name().getName()+'"'+";");
                bufferedWriter.newLine();

                bufferedWriter.write("URLClassLoader myClassLoader_"+varname+"_"+agg_count+" = new URLClassLoader(\n" +
                        "                new URL[]{new File(JarPath_"+varname+"_"+agg_count+"+JarName_"+varname+"_"+agg_count+").toURI().toURL()},\n" +
                        "                Main.class.getClassLoader()\n" +
                        "        );");
                bufferedWriter.newLine();

                bufferedWriter.write("        Class<?> myClass_"+varname+"_"+agg_count+" = Class.forName(ClassName_"+varname+"_"+agg_count+", true, myClassLoader_"+varname+"_"+agg_count+");\n" +
                        "        Method mySingeltonGetterMethod_"+varname+"_"+agg_count+"= myClass_"+varname+"_"+agg_count+".getMethod(\"get\" + ClassName_"+varname+"_"+agg_count+",\n" +
                        "                null);\n" +
                        "        \n" +
                        "        Object myObject_"+varname+"_"+agg_count+" = mySingeltonGetterMethod_"+varname+"_"+agg_count+".invoke(null);");

                bufferedWriter.newLine();

               bufferedWriter.write("var myValue_"+varname+"_"+agg_count+"= myObject_"+varname+"_"+agg_count+".getClass().getMethod(MethodName_"+varname+"_"+agg_count+", List.class)\n" +
                       "                .invoke(myObject_"+varname+"_"+agg_count+", myNumbers_"+varname+"_"+agg_count+");");

                bufferedWriter.newLine();

            }
        }
    }
    public void generate_code_main(BufferedWriter bufferedWriter,List<CreateTable> createTables) throws IOException
    {
        ///////////////////
        System.out.println("pppppppppppppppppp");
        List<AssigneSelect> assigneSelectList = new ArrayList<>();
        List<print> printList = new ArrayList<>();
        FunctionDeclaration functionDeclaration = new FunctionDeclaration();
        boolean check_func=false;
        functionDeclaration=p.getFunctions().get(0);

       for(int i=0;i< functionDeclaration.getBody().getAssigneVars().size();i++)
       {
           if(functionDeclaration.getBody().getAssigneVars().get(i).getAssigneSelect()!=null)
           {
               assigneSelectList.add(functionDeclaration.getBody().getAssigneVars().get(i).getAssigneSelect());


           }
       }
       ///////////////////
        for(int i=0;i< functionDeclaration.getBody().getPrints().size();i++)
        {
            printList.add(functionDeclaration.getBody().getPrints().get(i));
        }
       //////////////////////

       //System.out.println(assigneSelectList.size());
        for(int a=0;a<assigneSelectList.size();a++){


        //System.out.println("***********************");
           if(assigneSelectList.get(a).getSelectStmt()!=null) {
               //for join

               if (assigneSelectList.get(a).getSelectStmt().getJoinclaus() != null) {

//                   //System.out.println("MMMMMMMMMMMMMMMMMM");
//                   join_tables(bufferedWriter,assigneSelectList.get(a));  //create table1 and table2 list
//
//                   for(int i=0;i<assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++) {
//                       bufferedWriter.write("for( int i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "=0;" + "" + "i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "<" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list.size();" + "i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "++ )" + "{");
//                       bufferedWriter.newLine();
//
//
//                   }
//                   //bufferedWriter.newLine();
//
//                   join_constraint(bufferedWriter,assigneSelectList.get(a));
//                   bufferedWriter.newLine();
//
//                   bufferedWriter.write("}");
//
////                   bufferedWriter.newLine();
////                   for(int i=0;i<assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++) {
////                       bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list = "+assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_current_list");
////                       bufferedWriter.newLine();
////                   }
//                   bufferedWriter.newLine();
//                   bufferedWriter.write("}");
                   /**for aggregation function**/
                   if(assigneSelectList.get(a).getSelectStmt().getColumnItem().size()>0)
                   {
                       for(int t=0;t<assigneSelectList.get(a).getSelectStmt().getJoinclaus() .getTable_or_subqueries().size();t++) {
                           System.out.println("agggggregation");
                           for (int c = 0; c < assigneSelectList.get(a).getSelectStmt().getColumnItem().size(); c++) {
                               if (assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr() != null) {
                                   if (assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().size() > 0) {
                                       System.out.println("ffffffffffffffffffffffffffffunc");
                                       main_count++;
                                       main_list.add(main_count);
                                       check_func=true;

                                       for (int f = 0; f < assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().size(); f++) {
                                           for (int p = 0; p < assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size(); p++) {
                                               if (assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons() != null) {
                                                   bufferedWriter.write("List<Double> " + "list" + "_" + assigneSelectList.get(a).getVar() +"_"+main_count+ " = new ArrayList();");
                                                   bufferedWriter.newLine();
                                               }
                                               else {
                                                   bufferedWriter.write("List<Double> " + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelectList.get(a).getVar() +"_"+main_count+ " = new ArrayList();");
                                                   bufferedWriter.newLine();
                                               }
                                           }
                                       }
                                       //System.out.println("MMMMMMMMMMMMMMMMMM");
                                       join_tables(bufferedWriter,assigneSelectList.get(a));  //create table1 and table2 list

                                       for(int i=0;i<assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++) {
                                           bufferedWriter.write("for( int i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "=0;" + "" + "i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "<" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list_"+join_table_count+".size();" + "i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "++ )" + "{");
                                           bufferedWriter.newLine();


                                       }
                                       //bufferedWriter.newLine();

                                       join_constraint(bufferedWriter,assigneSelectList.get(a));

                                       bufferedWriter.newLine();

                                       bufferedWriter.write("}");


                                       bufferedWriter.newLine();
                                       bufferedWriter.write("}");

                                       for (int f = 0; f < assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().size(); f++) {
//
//
//                                           for (int p = 0; p < assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size(); p++) {
//                                               if (assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons() != null) {
//                                                   bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelectList.get(a).getVar() +
//
//                                                           ".add(" + assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(0)+"_current_list" + ".get(i).");
//                                                   bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons().getVars().get(1)+ ");");
//
//                                                   bufferedWriter.newLine();
//                                               } else {
//
//                                                   bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelectList.get(a).getVar() + ".add(" + assigneSelectList.get(a).getVar() + ".get(i).");
//                                                   bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
//                                                   bufferedWriter.newLine();
//                                               }
//                                           }

                                           bufferedWriter.newLine();
                                           //var myNumbers = id;


                                           for (int p = 0; p < assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size(); p++) {
                                               bufferedWriter.write("var myNumbers_" + assigneSelectList.get(a).getVar() +"_"+main_count+ " = ");
                                               bufferedWriter.write("list" + "_" + assigneSelectList.get(a).getVar() +"_"+main_count+ ";");
                                               bufferedWriter.newLine();
                                               Aggregation_loader_jar(bufferedWriter, assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getFun_name().getFuncname(), assigneSelectList.get(a).getVar());
                                               bufferedWriter.newLine();
                                           }

                                       }
                                       // for(int i=0;i<x.size();i++)
                                       //        {
                                       //            id.add(x.get(i).id);
                                       //        }


                                   }
//                                   else
//                                   {
//                                       System.out.println("MMMMMMMMMMMMMMMMMM");
//                                       join_tables(bufferedWriter,assigneSelectList.get(a));  //create table1 and table2 list
//
//                                       for(int i=0;i<assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++) {
//                                           bufferedWriter.write("for( int i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "=0;" + "" + "i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "<" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list.size();" + "i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "++ )" + "{");
//                                           bufferedWriter.newLine();
//
//
//                                       }
//                                       //bufferedWriter.newLine();
//
//                                       join_constraint(bufferedWriter,assigneSelectList.get(a));
//                                       bufferedWriter.newLine();
//
//                                       bufferedWriter.write("}");
//
////                   bufferedWriter.newLine();
////                   for(int i=0;i<assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++) {
////                       bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list = "+assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_current_list");
////                       bufferedWriter.newLine();
////                   }
//                                       bufferedWriter.newLine();
//                                       bufferedWriter.write("}");
//
//                                      // break;
//                                   }
                               }
                           }
                           break;
                       }
                   }
                   /**end of aggregation function**/
                   if(check_func==false)
                   {
                       System.out.println("MMMMMMMMMMMMMMMMMM");
                       join_tables(bufferedWriter,assigneSelectList.get(a));  //create table1 and table2 list

                       for(int i=0;i<assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++) {
                           bufferedWriter.write("for( int i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "=0;" + "" + "i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "<" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list.size();" + "i_" + assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "++ )" + "{");
                           bufferedWriter.newLine();


                       }
                       //bufferedWriter.newLine();

                       join_constraint(bufferedWriter,assigneSelectList.get(a));
                       bufferedWriter.newLine();

                       bufferedWriter.write("}");

//                   bufferedWriter.newLine();
//                   for(int i=0;i<assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++) {
//                       bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_list = "+assigneSelectList.get(a).getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "_current_list");
//                       bufferedWriter.newLine();
//                   }
                       bufferedWriter.newLine();
                       bufferedWriter.write("}");

                       // break;
                   }

               }
               else {
                   for (int t = 0; t < assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().size(); t++) {
                       for (int j = 0; j < assigneSelectList.get(a).getSelectStmt().getColumnItem().size(); j++) {
                           if (assigneSelectList.get(a).getSelectStmt().getColumnItem().get(j).getStar() == null) {



                               bufferedWriter.write("List<" + assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "> " + assigneSelectList.get(a).getVar() + "  = " + "new ArrayList<>()" + ";");
                               bufferedWriter.newLine();
                               bufferedWriter.write(assigneSelectList.get(a).getVar() + " = " + assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + ".getData();");
                               bufferedWriter.newLine();

                               /** start where add by majd **/

                               if(assigneSelectList.get(a).getSelectStmt().getWhereItem()!=null)
                               {
                                   System.out.println("whereeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                                   ok1=false;
                                   ok2=false;
                                   boo=false;
                                   count1=0;
                                   count2=0;
                                   majd++;
                                   expr(majd, assigneSelectList.get(a).getVar(),assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t),assigneSelectList.get(a).getSelectStmt(),bufferedWriter,assigneSelectList.get(a).getSelectStmt().getWhereItem());

                               }
                               /** end where add by majd **/

                               /**for aggregation function**/
                               if(assigneSelectList.get(a).getSelectStmt().getColumnItem().size()>0)
                               {
                                   System.out.println("agggggregation");
                                   for(int c=0;c<assigneSelectList.get(a).getSelectStmt().getColumnItem().size();c++)
                                   {
                                       if(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr()!=null)
                                       {
                                           if(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().size()>0)
                                           {
                                               main_count++;
                                               main_list.add(main_count);

                                               for(int f=0;f<assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().size();f++)
                                               {
                                                   ///count(*)
//                                                   if(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size()==0)
//                                                   {
//                                                       //star element
//
//
//                                                           bufferedWriter.write("List<"+assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName()+"> "+assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName()+"_"+assigneSelectList.get(a).getVar()+"_"+main_count+" = new ArrayList();");
//                                                           bufferedWriter.newLine();
//
//                                                           bufferedWriter.write("for( int i=0; i<"+assigneSelectList.get(a).getVar()+".size();"+" i++ ){");
//                                                           bufferedWriter.newLine();
//
//                                                         bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelectList.get(a).getVar()  +"_"+main_count+ ".add(" + assigneSelectList.get(a).getVar() + ".get(i));");
//                                                         bufferedWriter.newLine();
//                                                        //bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
//
//                                                       bufferedWriter.write("}");
//
//                                                       bufferedWriter.write("var myNumbers_"+assigneSelectList.get(a).getVar()+"_"+main_count+" = ");
//                                                       bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName()+"_"+assigneSelectList.get(a).getVar()+"_"+main_count+";");
//                                                       bufferedWriter.newLine();
//                                                       Aggregation_loader_jar(bufferedWriter,assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getFun_name().getFuncname(),assigneSelectList.get(a).getVar());
//                                                       bufferedWriter.newLine();
//
//
//
//                                                   }

                                                   /////count(*)
                                                   for(int p=0;p<assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size();p++)
                                                   {

                                                       if(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons()!=null)
                                                       {

                                                           bufferedWriter.write("List<Double> "+assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName()
                                                                   +"_"+assigneSelectList.get(a).getVar()+"_"+main_count+" = new ArrayList();");
                                                           bufferedWriter.newLine();
                                                       }
                                                       else {
                                                           bufferedWriter.write("List<Double> " + assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelectList.get(a).getVar() +"_"+main_count+ " = new ArrayList();");
                                                           bufferedWriter.newLine();
                                                       }
                                                   }
                                                   bufferedWriter.write("for( int i=0; i<"+assigneSelectList.get(a).getVar()+"_"+main_count+".size();"+" i++ ){");
                                                   bufferedWriter.newLine();
                                                   for(int p=0;p<assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size();p++)
                                                   {
                                                       if(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getGetPropertyJsons()!=null)
                                                       {
                                                           bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelectList.get(a).getVar()  +"_"+main_count+
                                                                   ".add(" + assigneSelectList.get(a).getVar() + ".get(i).");
                                                           bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
                                                           bufferedWriter.newLine();
                                                       }
                                                       else {

                                                           bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName() + "_" + assigneSelectList.get(a).getVar()  +"_"+main_count+ ".add(" + assigneSelectList.get(a).getVar() + ".get(i).");
                                                           bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().get(p).getValue() + ");");
                                                           bufferedWriter.newLine();
                                                       }
                                                   }
                                                   bufferedWriter.write("}");
                                                   bufferedWriter.newLine();
                                                   //var myNumbers = id;


                                                   for(int p=0;p<assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getArgument_list().getExpr().size();p++)
                                                   {
                                                       bufferedWriter.write("var myNumbers_"+assigneSelectList.get(a).getVar()+"_"+main_count+" = ");
                                                       bufferedWriter.write(assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t).getTb_name().getAny_name().getName()+"_"+assigneSelectList.get(a).getVar()+"_"+main_count+";");
                                                       bufferedWriter.newLine();
                                                       Aggregation_loader_jar(bufferedWriter,assigneSelectList.get(a).getSelectStmt().getColumnItem().get(c).getExpr().getCall_funcs().get(f).getFun_name().getFuncname(),assigneSelectList.get(a).getVar());
                                                       bufferedWriter.newLine();
                                                   }

                                               }
                                               //end count(*)

                                               // for(int i=0;i<x.size();i++)
                                               //        {
                                               //            id.add(x.get(i).id);
                                               //        }


                                           }
                                       }
                                   }
                               }
                               /**end of aggregation function**/

                               /** start where add by majd **/

//                               if(assigneSelectList.get(a).getSelectStmt().getWhereItem()!=null)
//                               {
//                                   System.out.println("whereeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
//                                    ok1=false;
//                                    ok2=false;
//                                     boo=false;
//                                    count1=0;
//                                    count2=0;
//                                    majd++;
//                                   expr(majd, assigneSelectList.get(a).getVar(),assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(t),assigneSelectList.get(a).getSelectStmt(),bufferedWriter,assigneSelectList.get(a).getSelectStmt().getWhereItem());
//
//                               }
                               /** end where add by majd **/
                               break;
                           }
                       }
                   }

                   for (int j = 0; j < assigneSelectList.get(a).getSelectStmt().getColumnItem().size(); j++) {
                       if (assigneSelectList.get(a).getSelectStmt().getColumnItem().get(j).getStar() != null) {
                           for (int i = 0; i < assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().size(); i++) {

                               bufferedWriter.write("List<" + assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + "> " + assigneSelectList.get(a).getVar() + "  = " + "new ArrayList<>()" + ";");
                               bufferedWriter.newLine();
                               bufferedWriter.write(assigneSelectList.get(a).getVar() +" = " + assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName() + ".getData();");
                               bufferedWriter.newLine();
                               System.out.println("select * *********************************************************");
                               /** start where add by majd **/

                               if(assigneSelectList.get(a).getSelectStmt().getWhereItem()!=null)
                               {
                                   System.out.println("whereeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                                   ok1=false;
                                   ok2=false;
                                   boo=false;
                                   count1=0;
                                   count2=0;
                                   majd++;
                                   expr(majd, assigneSelectList.get(a).getVar(),assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(i),assigneSelectList.get(a).getSelectStmt(),bufferedWriter,assigneSelectList.get(a).getSelectStmt().getWhereItem());

                               }
                               /** end where add by majd **/


                           }
                           bufferedWriter.newLine();
                           bufferedWriter.newLine();




//                           if (assigneSelectList.get(a).getVar())


                       } else {
//                           if(assigneSelectList.get(a).getSelectStmt().getWhereItem()!=null)
//                           {
//                              System.out.println( "where ----"+assigneSelectList.get(a).getSelectStmt().getWhereItem().getValue());
//                           }
                           for (int i = 0; i < assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().size(); i++) {



                               for (int v = 0; v < createTables.size(); v++) {
                                   if (assigneSelectList.get(a).getSelectStmt().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName()
                                           .equals(createTables.get(v).getTb_name().getAny_name().getName())) {
                                       if (assigneSelectList.get(a).getSelectStmt().getColumnItem().get(j).getExpr().getCol_name() != null) {
                                           for (int n = 0; n < createTables.get(v).getColumn_defs().size(); n++) {

                                               if (assigneSelectList.get(a).getSelectStmt().getColumnItem().get(j).getExpr().getCol_name().getAny_name().getName()
                                                       .equals(createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName())) {
                                                   System.out.println("vaaaaaaaal : "+assigneSelectList.get(a).getSelectStmt().getColumnItem().get(j).getExpr().getCol_name().getAny_name().getName());
                                                   for (int y = 0; y < createTables.get(v).getColumn_defs().get(n).getType_names().size(); y++) {
//                                                       if (createTables.get(v).getColumn_defs().get(n).getType_names().get(y).getNames().getName().getName().equals(Type.NUMBER_CONST)) {
//                                                           bufferedWriter.write("List<" + "Double" + "> " + createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "  = " + "new ArrayList<>()" + ";");
//                                                           bufferedWriter.newLine();
//                                                           ////        for (int i =0;i<users.userslist.size();i++)
//                                                           ////        {
//                                                           ////            id.add(users.userslist.get(i).id);
//                                                           ////        }
//                                                           bufferedWriter.write("for (int i =0;i<" + assigneSelectList.get(a).getVar()+".size()" + ";i++){");
//                                                           bufferedWriter.newLine();
//                                                           bufferedWriter.write(createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + ".add(" + assigneSelectList.get(a).getVar()+".get(i)." + createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + ");");
//                                                           bufferedWriter.newLine();
//                                                           bufferedWriter.write("}");
//                                                           bufferedWriter.newLine();
//
//                                                       } else if (createTables.get(v).getColumn_defs().get(n).getType_names().get(y).getNames().getName().getName().equals(Type.STRING_CONST)) {
//                                                           bufferedWriter.write("List<" + "String" + "> " + createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "  = " + "new ArrayList<>()" + ";");
//                                                           bufferedWriter.newLine();
//
//                                                           bufferedWriter.write("for (int i =0;i<" + assigneSelectList.get(a).getVar()+".size()" + ";i++){");
//                                                           bufferedWriter.newLine();
//                                                           bufferedWriter.write(createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + ".add(" + assigneSelectList.get(a).getVar()+".get(i)." + createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + ");");
//                                                           bufferedWriter.newLine();
//                                                           bufferedWriter.write("}");
//                                                           bufferedWriter.newLine();
//                                                       } else if (createTables.get(v).getColumn_defs().get(n).getType_names().get(y).getNames().getName().getName().equals(Type.BOOLEAN_CONST)) {
//                                                           bufferedWriter.write("List<" + "Boolean" + "> " + createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "  = " + "new ArrayList<>()" + ";");
//                                                           bufferedWriter.newLine();
//
//                                                           bufferedWriter.write("for (int i =0;i<" + assigneSelectList.get(a).getVar()+".size()" + ";i++){");
//                                                           bufferedWriter.newLine();
//                                                           bufferedWriter.write(createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + ".add(" + assigneSelectList.get(a).getVar()+".get(i)." + createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + ");");
//                                                           bufferedWriter.newLine();
//                                                           bufferedWriter.write("}");
//                                                           bufferedWriter.newLine();
//                                                       } else {
//                                                           bufferedWriter.write("List<" + createTables.get(v).getColumn_defs().get(n).getType_names().get(y).getNames().getName().getName() + "> " + createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + "  = " + "new ArrayList<>()" + ";");
//                                                           bufferedWriter.newLine();
//
//                                                           bufferedWriter.write("for (int i =0;i<" + assigneSelectList.get(a).getVar()+".size()" + ";i++){");
//                                                           bufferedWriter.newLine();
//                                                           bufferedWriter.write(createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + ".add(" + assigneSelectList.get(a).getVar()+".get(i)." + createTables.get(v).getColumn_defs().get(n).getCol_name().getAny_name().getName() + ");");
//                                                           bufferedWriter.newLine();
//                                                           bufferedWriter.write("}");
//                                                           bufferedWriter.newLine();
//                                                       }

                                                   }
                                               }
                                           }
                                       }
                                   }
                               }

                           }

                       }
                   }
               }
           }
           ///
        }
    }

    public void join_tables (BufferedWriter bufferedWriter,AssigneSelect assigneSelect) throws IOException
    {
        join_table_count++;

       for(int i=0;i<assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().size();i++)
       {
           //System.out.println(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName());
           //for (int j = 0; j < assigneSelect.getSelectStmt().getColumnItem().size(); j++) {

              // if(assigneSelect.getSelectStmt().getColumnItem().get(j).getStar()!=null)
               //{
                   bufferedWriter.write(" List<"+assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName()+">"+ assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName()+"_list_"+join_table_count+"  = new ArrayList<>();\n" +
                           assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName()+"_list_"+join_table_count+" = "+assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName()+".getData();\n");
                   bufferedWriter.newLine();

                   bufferedWriter.write(" List<"+assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName()+">"+ assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName()+"_current_list_"+join_table_count+ "  = new ArrayList<>();\n");
                   bufferedWriter.newLine();

                  list_join.add(assigneSelect.getSelectStmt().getJoinclaus().getTable_or_subqueries().get(i).getTb_name().getAny_name().getName()+"_current_list"+"_"+join_table_count);

               //}

           //}

       }
    }

    public void addMethodsPrint(BufferedWriter bufferedWriter,String function_name) throws IOException {

        List<print>printList= new ArrayList<>();
        boolean newtype=false;
        boolean agg_func=false;
        for(int i=0;i< functionDeclaration.getBody().getPrints().size();i++)
        {
            printList.add(functionDeclaration.getBody().getPrints().get(i));
        }
            for (String key :  this.functionDeclaration.getScope_function().getSymbolMap().keySet() ) {

                Symbol s = this.functionDeclaration.getScope_function().getSymbolMap().get(key);
                if (s.getType().getName().equals(Type.NUMBER_CONST) || s.getType().getName().equals(Type.STRING_CONST) || s.getType().getName().equals(Type.BOOLEAN_CONST)) {
                    bufferedWriter.write("System.out.println( ");

                    for (int i = 0; i < this.functionDeclaration.getBody().getPrints().size(); i++) {
                        print print = this.functionDeclaration.getBody().getPrints().get(i);

                        if (print.getExpr().getGetPropertyJsons() != null) {
                            bufferedWriter.write(print.getExpr().getGetPropertyJsons().getVars().toString());
                        }
                        if (print.getExpr().getIncrDecr() != null) {
                            bufferedWriter.write(print.getExpr().getIncrDecr().getValue());
                        }
                        if (print.getExpr().getCall_func() != null) {
                            bufferedWriter.write(print.getExpr().getCall_func().toString());
                        }

                        if (print.getExpr().getExprwithbrackets() != null) {
                            bufferedWriter.write(print.getExpr().getExprwithbrackets().toString());
                        }

                        if (print.getExpr().getValue() != null) {
                            bufferedWriter.write(print.getExpr().getValue());
                        }
                    }


                    bufferedWriter.write(" )" + ";");
                    bufferedWriter.newLine();
                }

                if (printList.size() > 0) {

                    if (s.getType().getColumns().size() > 0) {

                        for (String Key1 : s.getType().getColumns().keySet()) {
                            for(int i=0;i<this.symbolTable.getDeclaredAggregationFunction().size();i++) {
                                if (this.symbolTable.getDeclaredAggregationFunction().get(i).getFunction_name().getAny_name().getName().equals(Key1))
                                {
                                    agg_func=true;
                                    System.out.println("mbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
                                    for(int r=0;r<main_list.size();r++) {
                                        bufferedWriter.write("System.out.println(myValue_" + s.getName() + "_" + main_list.get(r).toString() + ");");
                                        bufferedWriter.newLine();

                                    }
                                    break;
                                }
                            }
                        }

                        if(agg_func==false) {

                            String str1[] = s.getType().getName().split("_");

                            if (str1.length > 2) {

                                for (int i = 0; i < list_join.size(); i++) {
                                    bufferedWriter.write("for(int i=0;i<" + list_join.get(i) + ".size();" + "i++) { ");
                                    bufferedWriter.newLine();

                                    String arr[] = list_join.get(i).split("_");


                                    for (String Key : s.getType().getColumns().keySet()) {

                                        String str_join[] = Key.split("_");

                                        if (str_join[str_join.length - 1].equals(arr[0])) {

                                            //System.out.println("ooookokoko");
                                            bufferedWriter.write("System.out.println(" + list_join.get(i) + ".get(i)." + str_join[0] + ");");
                                            bufferedWriter.newLine();

                                        } else  //another type
                                        {
                                            String add = "";
                                            newtype = true;
                                            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();

                                            if (Main.type_name_list.contains(str_join[str_join.length - 1])) //type
                                            {
                                                System.out.println("............................m_1");
                                                for (int a = 0; a < this.createTableList.size(); a++) {
                                                    if (arr[0].equals(this.createTableList.get(a).getTb_name().getAny_name().getName())) {
                                                        System.out.println("............................m_2");
                                                        for (int j = 0; j < this.createTableList.get(a).getColumn_defs().size(); j++) {
                                                            for (int t = 0; t < this.createTableList.get(a).getColumn_defs().get(j).getType_names().size(); t++) {
                                                                if (str_join[str_join.length - 1].equals(this.createTableList.get(a).getColumn_defs().get(j).getType_names().get(t).getNames().getName().getName())) {
                                                                    test = str_join[str_join.length - 1];
                                                                    System.out.println("............................m_3");
                                                                    //if(arr[0].equals(this.createTableList.get(a).getTb_name().getAny_name().getName())) {
                                                                    //System.out.println("............................m");
                                                                    for (int p = 0; p < this.createTypeList.size(); p++) {
                                                                        if (str_join[str_join.length - 1].equals(this.createTypeList.get(p).getCreate_type_name().getAny_name().getName())) {
                                                                            System.out.println("............................m_4");
                                                                            for (int h = 0; h < this.createTypeList.get(p).getColumn_def_type().size(); h++) {

                                                                                //add="System.out.println(" + list_join.get(i) + ".get(i)." + str_join[str_join.length - 1] + "." + this.createTypeList.get(p).getColumn_def_type().get(h).getCol_name().getAny_name().getName() + ");";

                                                                                stringSet.add(this.createTypeList.get(p).getColumn_def_type().get(h).getCol_name().getAny_name().getName());


                                                                            }
//

                                                                        }
                                                                    }
                                                                    //}

                                                                }
                                                            }

                                                        }

                                                    }
                                                }

                                                //System.out.println("sssssssssssssssssyy "+stringSet);


                                            }

                                            if (Main.table_name.contains(str_join[str_join.length - 1]))  //table
                                            {

                                            }
                                        }

                                    }
                                    if (newtype) {
                                        for (int r = 0; r < stringSet.size(); r++) {
                                            bufferedWriter.write("System.out.println(" + list_join.get(i) + ".get(i)." + test + "." + stringSet.toArray()[r] + ");");
//
                                            bufferedWriter.newLine();
                                        }
                                    }


                                    bufferedWriter.write("}");
                                    bufferedWriter.newLine();

                                }


                            }
                            else {
                                bufferedWriter.write("for(int i=0;i<" + s.getName() + ".size()" + ";i++) { ");
                                bufferedWriter.newLine();
                                for (String Key : s.getType().getColumns().keySet()) {


                                    String str[] = Key.split("_");


                                    System.out.println(Key);

                                    if (!(str[1].equals(s.getType().getName().substring(2))))  //another type
                                    {
//                            bufferedWriter.write("for( ){");
//                            bufferedWriter.newLine();
                                        //System.out.println(str[0]+" "+str[1]);

//                            bufferedWriter.write("for( ){");
//                            bufferedWriter.newLine();
                                        if (Main.table_name.contains(str[1])) {
                                            System.out.println("ssssssssssss : " + str[1]);

                                            for (int t = 0; t < this.createTableList.size(); t++) {
                                                for (int c = 0; c < this.createTableList.get(t).getColumn_defs().size(); c++) {
                                                    for (int y = 0; y < this.createTableList.get(t).getColumn_defs().get(c).getType_names().size(); y++) {


                                                        if (str[1].equals(this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName())) {


                                                            bufferedWriter.write("for(int j=0;j<" + s.getName() + ".get(i)." + this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName() + ".size()" + ";j++){");
                                                            bufferedWriter.newLine();
                                                            bufferedWriter.newLine();
                                                            bufferedWriter.write("System.out.println( ");
                                                            bufferedWriter.write(s.getName() + ".get(i)" + "." + this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName() + ".get(j)" + "." + str[0]);
                                                            bufferedWriter.write(" )" + ";");
                                                            bufferedWriter.newLine();


                                                            //System.out.println("Keeey : "+s.getType().getColumns().get(Key).getName());
                                                            if (Main.type_name_list.contains(s.getType().getColumns().get(Key).getName())) {
                                                                for (int h = 0; h < Main.type_list.size(); h++) {
                                                                    if (Main.type_list.get(h).getCreate_type_name().getAny_name().getName().equals(s.getType().getColumns().get(Key).getName())) {
                                                                        //bufferedWriter.write("for( ){");
                                                                        bufferedWriter.newLine();
                                                                        for (int q = 0; q < Main.type_list.get(h).getColumn_def_type().size(); q++) {
                                                                            bufferedWriter.write("System.out.println( ");
                                                                            bufferedWriter.write(s.getName() + ".get(i)" + "." + this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName() + ".get(j)" + "." + str[0] + "." + Main.type_list.get(h).getColumn_def_type().get(q).getCol_name().getAny_name().getName());
                                                                            bufferedWriter.write(" )" + ";");
                                                                            bufferedWriter.newLine();
                                                                        }
                                                                        //bufferedWriter.write("}");
                                                                        bufferedWriter.newLine();
                                                                    }
                                                                }

                                                            }

                                                            bufferedWriter.write("}");
                                                            bufferedWriter.newLine();

                                                        }

                                                    }
                                                }
                                            }


                                        }
//                            bufferedWriter.write("}");
//                            bufferedWriter.newLine();


//                            if(Main.type_name_list.contains(str[1])&&s.getType().getName().equals(str[1]))
//                            {
//
//                                System.out.println("ooooooooooooooooooooooooooooooooooooooo");
//
//                                for(int t=0;t<this.createTableList.size();t++)
//                                {
//                                    for(int c=0;c<this.createTableList.get(t).getColumn_defs().size();c++)
//                                    {
//                                        for(int y=0;y<this.createTableList.get(t).getColumn_defs().get(c).getType_names().size();y++)
//                                        {
//                                            if(str[1].equals(this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName()))
//                                            {
//
//                                                bufferedWriter.write("System.out.println( ");
//                                                bufferedWriter.write(s.getName() + ".get(i)" +"."+this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName()+"." + str[0]);
//                                                bufferedWriter.write(" )" + ";");
//                                                bufferedWriter.newLine();
//                                                if(Main.type_name_list.contains(s.getType().getColumns().get(Key).getName()))
//                                                {
//                                                    System.out.println("ooooooooooooooooooooooooooooooooooooooo");
//                                                    for(int h=0;h<Main.type_list.size();h++)
//                                                    {
//                                                        if(Main.type_list.get(h).getCreate_type_name().getAny_name().getName().equals(s.getType().getColumns().get(Key).getName()))
//                                                        {
//                                                            for(int q=0;q<Main.type_list.get(h).getColumn_def_type().size();q++) {
//                                                                bufferedWriter.write("System.out.println( ");
//                                                                bufferedWriter.write(s.getName() + ".get(i)" + "." + this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName() + "." + str[0] + "." + Main.type_list.get(h).getColumn_def_type().get(q).getCol_name().getAny_name().getName());
//                                                                bufferedWriter.write(" )" + ";");
//                                                                bufferedWriter.newLine();
//                                                            }
//                                                        }
//                                                    }
//
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                          if(Main.type_name_list.contains(str[1]))
//                          {
//                              for(int t=0;t<this.createTableList.size();t++)
//                              {
//                                  for(int c=0;c<this.createTableList.get(t).getColumn_defs().size();c++)
//                                  {
//                                      for(int y=0;y<this.createTableList.get(t).getColumn_defs().get(c).getType_names().size();y++)
//                                      {
//                                          if(str[1].equals(this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName()))
//                                          {
//
//                                              bufferedWriter.write("System.out.println( ");
//                                              bufferedWriter.write(s.getName() + ".get(i)" +"."+this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName()+"." + str[0]);
//                                              bufferedWriter.write(" )" + ";");
//                                              bufferedWriter.newLine();
//                                          }
//                                      }
//                                  }
//                              }
//                          }

                                    }

                                    if (str[1].equals(s.getType().getName().substring(2))) {
                                        // if(s.getType().getColumns().get(Key).getName().equals(Type.STRING_CONST)){
                                        bufferedWriter.write("System.out.println( ");
                                        bufferedWriter.write(s.getName() + ".get(i)" + "." + str[0]);
                                        bufferedWriter.write(" )" + ";");
                                        bufferedWriter.newLine();


                                        if (Main.type_name_list.contains(s.getType().getColumns().get(Key).getName())) {
                                            System.out.println("llllllllllll = " + s.getType().getColumns().get(Key).getName());
                                            for (int t = 0; t < this.createTableList.size(); t++) {
                                                for (int c = 0; c < this.createTableList.get(t).getColumn_defs().size(); c++) {
                                                    for (int y = 0; y < this.createTableList.get(t).getColumn_defs().get(c).getType_names().size(); y++) {
                                                        if (str[1].equals(this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName())) {
//                                                if(this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName().equals(Type.NUMBER_CONST)
//                                                        ||this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName().equals(Type.STRING_CONST)
//                                                        ||this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName().equals(Type.BOOLEAN_CONST)
//                                                ) {


                                                            bufferedWriter.newLine();
                                                            bufferedWriter.write("System.out.println( ");
                                                            bufferedWriter.write(s.getName() + ".get(i)" + "." + this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName() + "." + str[0]);
                                                            bufferedWriter.write(" )" + ";");
                                                            bufferedWriter.newLine();
                                                            //}
                                                            //System.out.println("Keeey : "+s.getType().getColumns().get(Key).getName());
                                                            if (Main.type_name_list.contains(s.getType().getColumns().get(Key).getName())) {
                                                                for (int h = 0; h < Main.type_list.size(); h++) {
                                                                    if (Main.type_list.get(h).getCreate_type_name().getAny_name().getName().equals(s.getType().getColumns().get(Key).getName())) {
                                                                        for (int q = 0; q < Main.type_list.get(h).getColumn_def_type().size(); q++) {

                                                                            for (int f = 0; f < Main.type_list.get(h).getColumn_def_type().get(q).getType_names().size(); f++) {

//                                                                    if(Main.type_list.get(h).getColumn_def_type().get(q).getType_names().get(f).getNames().getName().getName().equals(Type.NUMBER_CONST)
//                                                                    ||Main.type_list.get(h).getColumn_def_type().get(q).getType_names().get(f).getNames().getName().getName().equals(Type.STRING_CONST)
//                                                                            ||Main.type_list.get(h).getColumn_def_type().get(q).getType_names().get(f).getNames().getName().getName().equals(Type.BOOLEAN_CONST)
//                                                                    ) {
                                                                                bufferedWriter.write("System.out.println( ");
                                                                                bufferedWriter.write(s.getName() + ".get(i)" + "." + str[0] + "." + Main.type_list.get(h).getColumn_def_type().get(q).getCol_name().getAny_name().getName());
                                                                                bufferedWriter.write(" )" + ";");
                                                                                bufferedWriter.newLine();
                                                                                //}
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        }


                                    }


                                }

//                        else
//                        {
//
//                            for(int t=0;t<this.createTableList.size();t++)
//                            {
//                                if(s.getType().getName().substring(2).equals(this.createTableList.get(t).getTb_name().getAny_name().getName()))
//                                {
//                                   // System.out.println("jjjjj");
//                                    for(int c=0;c<this.createTableList.get(t).getColumn_defs().size();c++)
//                                    {
//                                        for(int y=0;y<this.createTableList.get(t).getColumn_defs().get(c).getType_names().size();y++) {
//                                             System.out.println(this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName());
//                                            if (Main.type_name_list.contains(this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName())) {
//                                             //System.out.println(this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName());
//                                             bufferedWriter.write(s.getName() + ".get(i)" + "."+this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName()+"."+ str[0]);
//                                             bufferedWriter.write(" )" + ";");
//                                             bufferedWriter.newLine();
//
//                                            }
//                                            if (Main.table_name.contains(this.createTableList.get(t).getColumn_defs().get(c).getType_names().get(y).getNames().getName().getName())) {
//                                                //System.out.println(this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName());
//                                                bufferedWriter.write(s.getName() + ".get(i)" + "."+this.createTableList.get(t).getColumn_defs().get(c).getCol_name().getAny_name().getName()+"."+ str[0]);
//                                                bufferedWriter.write(" )" + ";");
//                                                bufferedWriter.newLine();
//
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//
////                            bufferedWriter.write(s.getName() + ".get(i)" + "."+str[1]+"."+ str[0]);
////                            bufferedWriter.write(" )" + ";");
////                            bufferedWriter.newLine();
//                        }

                                bufferedWriter.write("}");
                            }
                        }
                    }

                }
            }
            //}

        }

    public void addClassName(BufferedWriter bufferedWriter,String classname) throws IOException {
        bufferedWriter.write("class " + classname);
        bufferedWriter.write("{");
        bufferedWriter.newLine();
        //add data members and methods here
        addDataMembers(bufferedWriter,classname);
        bufferedWriter.newLine();
        addMethods(bufferedWriter,classname);
        bufferedWriter.write("}");
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    public void addDataMembers(BufferedWriter bufferedWriter,String classname) throws IOException
    {

        if(this.createTypeList.size()>0)
        {
            System.out.println("tyyyyyyy");
            for(int b =0; b<this.createTypeList.size();b++)
            {
                if(this.createTypeList.get(b).getCreate_type_name().getAny_name().getName().equals(classname))
                {
                    for(int j=0;j<createTypeList.get(b).getColumn_def_type().size();j++)
                    {
                        for(int v =0;v<createTypeList.get(b).getColumn_def_type().get(j).getType_names().size();v++)
                        {
                            if(this.createTypeList.get(b).getColumn_def_type().get(j).getType_names().get(v).getNames().getName().getName().equals(Type.NUMBER_CONST))
                            {
                                bufferedWriter.write(double.class.toString() + " " + this.createTypeList.get(b).getColumn_def_type().get(j).getCol_name().getAny_name().getName() + ";");
                                bufferedWriter.newLine();
                            }

                            else if(this.createTypeList.get(b).getColumn_def_type().get(j).getType_names().get(v).getNames().getName().getName().equals(Type.STRING_CONST))
                            {
                                bufferedWriter.write("String" + " " + this.createTypeList.get(b).getColumn_def_type().get(j).getCol_name().getAny_name().getName() + ";");
                                bufferedWriter.newLine();
                            }

                            else if(this.createTypeList.get(b).getColumn_def_type().get(j).getType_names().get(v).getNames().getName().getName().equals(Type.BOOLEAN_CONST))
                            {
                                bufferedWriter.write("boolean" + " " + this.createTypeList.get(b).getColumn_def_type().get(j).getCol_name().getAny_name().getName() + ";");
                                bufferedWriter.newLine();
                            }
                            else
                            {
                                bufferedWriter.write(this.createTypeList.get(b).getColumn_def_type().get(j).getType_names().get(v).getNames().getName().getName() + " " + this.createTypeList.get(b).getColumn_def_type().get(j).getCol_name().getAny_name().getName() + ";");
                                bufferedWriter.newLine();
                            }

                            bufferedWriter.flush();
                        }
                    }
                }
            }
        }

      if(this.createTableList.size()>0) {
          for (int a = 0; a < this.createTableList.size(); a++) {
              if (this.createTableList.get(a).getTb_name().getAny_name().getName().equals(classname)) {
                  for (int i = 0; i < this.createTableList.get(a).getColumn_defs().size(); i++) {

                      for (int j = 0; j < this.createTableList.get(a).getColumn_defs().get(i).getType_names().size(); j++) {
                          if (this.createTableList.get(a).getColumn_defs().get(i).getType_names().get(j).getNames().getName().getName().equals(Type.NUMBER_CONST)) {
                              bufferedWriter.write(double.class.toString() + " " + this.createTableList.get(a).getColumn_defs().get(i).getCol_name().getAny_name().getName() + ";");
                              bufferedWriter.newLine();
                          } else if (this.createTableList.get(a).getColumn_defs().get(i).getType_names().get(j).getNames().getName().getName().equals(Type.STRING_CONST)) {
                              bufferedWriter.write("String" + " " + this.createTableList.get(a).getColumn_defs().get(i).getCol_name().getAny_name().getName() + ";");
                              bufferedWriter.newLine();
                          } else if (this.createTableList.get(a).getColumn_defs().get(i).getType_names().get(j).getNames().getName().getName().equals(Type.BOOLEAN_CONST)) {
                              bufferedWriter.write(boolean.class.toString() + " " + this.createTableList.get(a).getColumn_defs().get(i).getCol_name().getAny_name().getName() + ";");
                              bufferedWriter.newLine();
                          } else if(Main.table_name.contains(this.createTableList.get(a).getColumn_defs().get(i).getType_names().get(j).getNames().getName().getName())){
                              bufferedWriter.write("List<"+this.createTableList.get(a).getColumn_defs().get(i).getType_names().get(j).getNames().getName().getName()+"> "+ this.createTableList.get(a).getColumn_defs().get(i).getCol_name().getAny_name().getName() + ";");
                              bufferedWriter.newLine();
                          }
                          else
                          {
                              bufferedWriter.write(this.createTableList.get(a).getColumn_defs().get(i).getType_names().get(j).getNames().getName().getName()+ " "+this.createTableList.get(a).getColumn_defs().get(i).getCol_name().getAny_name().getName() + ";");
                              bufferedWriter.newLine();
                          }

                          bufferedWriter.flush();

                      }


                  }

                  bufferedWriter.write("private static " + "List" + '<' + this.createTableList.get(a).getTb_name().getAny_name().getName() + '>' + " ");
                  bufferedWriter.write(this.createTableList.get(a).getTb_name().getAny_name().getName() + "list;");
                  //bufferedWriter.write(" new " + "ArrayList()" + ";");
                  bufferedWriter.newLine();


                  bufferedWriter.write("String" + " " + "type = ");
                  bufferedWriter.write(this.createTableList.get(a).getType_create_table() + ";");
                  bufferedWriter.newLine();

                  //add data member path

                  bufferedWriter.write("String" + " " + "path = ");
                  bufferedWriter.write(this.createTableList.get(a).getPath_create_type() + ";");
                  bufferedWriter.newLine();

                  bufferedWriter.flush();
              }
          }
      }

        //add data member type





//        bufferedWriter.flush();

    }

    public void addMethods(BufferedWriter bufferedWriter,String classname) throws IOException
    {
        for(int a =0 ; a<this.createTableList.size();a++) {
            if (this.createTableList.get(a).getTb_name().getAny_name().getName().equals(classname)) {
                //for (int i = 0; i < this.createTableList.get(a).getColumn_defs().size(); i++) {
                 //if(this.createTableList.get(a).getType_create_table().equals("json"))
                 //{
                     //add parsing json to file
                     bufferedWriter.write("private static "+ "List<"+this.createTableList.get(a).getTb_name().getAny_name().getName()+">" +" load_data()");
                     bufferedWriter.write("{");
                     bufferedWriter.newLine();
                     bufferedWriter.write(this.createTableList.get(a).getTb_name().getAny_name().getName() + "list = ");
                     bufferedWriter.write("new ArrayList();");
                     bufferedWriter.newLine();
                      bufferedWriter.write("try(FileReader fileReader = new FileReader"+"("+this.createTableList.get(a).getPath_create_type()+")"+")");
                     bufferedWriter.write("{");
                     bufferedWriter.newLine();
                      bufferedWriter.write("Gson gson = new Gson();\n" +
                              "            JsonReader reader = new JsonReader(fileReader);");
                      bufferedWriter.newLine();

                      bufferedWriter.write(" JsonObject jsonObject  = gson.fromJson(reader,JsonObject.class);\n" +
                              "\n" +
                              "            JsonArray jsonArray =  jsonObject.getAsJsonArray("+'"'+this.createTableList.get(a).getTb_name().getAny_name().getName()+'"'+");");

                      bufferedWriter.newLine();

                     bufferedWriter.write("for(int i =0 ;i<jsonArray.size();i++)\n" +
                             "            {\n" +
                             "                JsonElement jsonElement = jsonArray.get(i);\n" +
                             this.createTableList.get(a).getTb_name().getAny_name().getName()+" "+this.createTableList.get(a).getTb_name().getAny_name().getName()+"_obj"+" = gson.fromJson(jsonElement,"+this.createTableList.get(a).getTb_name().getAny_name().getName()+".class"+");\n" +
                             this.createTableList.get(a).getTb_name().getAny_name().getName() + "list"+".add("+this.createTableList.get(a).getTb_name().getAny_name().getName()+"_obj"+");\n" +
                             "            }");

                     bufferedWriter.write("}");
                     bufferedWriter.newLine();

                bufferedWriter.write("catch (FileNotFoundException e) {\n" +
                        "            e.printStackTrace();\n" +
                        "        } catch (IOException e) {\n" +
                        "            e.printStackTrace();\n" +
                        "        }\n"
                        );
                bufferedWriter.newLine();
                bufferedWriter.write("return "+this.createTableList.get(a).getTb_name().getAny_name().getName() + "list"+";");
                bufferedWriter.newLine();
                     bufferedWriter.write("}");
                     //end load method
                bufferedWriter.newLine();
                bufferedWriter.write("public static "+ "List<"+this.createTableList.get(a).getTb_name().getAny_name().getName()+">" +" getData()");
                bufferedWriter.write("{");
                bufferedWriter.newLine();
                bufferedWriter.write("if("+this.createTableList.get(a).getTb_name().getAny_name().getName() + "list "+"== null){");
                bufferedWriter.newLine();
                bufferedWriter.write(this.createTableList.get(a).getTb_name().getAny_name().getName() + "list = "+"load_data();");
                bufferedWriter.newLine();
                bufferedWriter.write("}");
                bufferedWriter.newLine();
                bufferedWriter.write("return "+this.createTableList.get(a).getTb_name().getAny_name().getName() + "list;");
                bufferedWriter.newLine();
                bufferedWriter.write("}");


                     bufferedWriter.flush();
                 //}
                //}
            }
        }
    }

    public void setSymbolTable(SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
    }

    public void setGeneratefile_path(String generatefile_path) {
        this.generatefile_path = generatefile_path;
    }

    public SymbolTable getSymbolTable() {
        return symbolTable;
    }

    public String getGeneratefile_path() {
        return generatefile_path;
    }


//    public void having()
//    {
//
//    }

}
