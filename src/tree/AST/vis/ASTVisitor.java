package tree.AST.vis;


import tree.AST.Parse;
import tree.AST.QueryStmt.*;
import tree.AST.javaStmt.*;

public interface ASTVisitor {
    public void visit(Parse p);
    public void visit(FunctionDeclaration funcDec);
    public void visit(SQL_Statement Statement);
    public void visit(SelectStmt selectStmt);
    public void visit(Expr expr);
    public void visit(BodyFunction bodyFunction);
    public void visit(HeaderFunction headerFunction);
    public void visit(FunctionName functionName);
    public void visit(Variable variable);
////////////
public void visit(table_or_subquery table_or_subquery);

    public void visit(CreateTable createTable);
    public void visit(create_aggregation_function aggregation_function);
    public void visit(shcemaarray shcemaarray);
    public void visit(column_def column_def);
    public void visit(column_constraint column_constraint);
    public void visit(Name name);
    public void visit(column_default column_default);
    public void visit(signed_number signed_number);
    public void visit(column_default_value column_default_value);
    public void visit(collation_name ccollation_name);
    public void visit(type_name type_name);
    public void visit(table_constraint table_constraint);
    public void visit(createType createType);
    public void visit(create_type_name createTypeName);
    public void visit(columnDefType column_def_type);





///////////s//majd//////////

    public void visit(While whiles);
    public void visit(Header_while header_while);
    public void visit(condition_header condition_header);
    public void visit(Bollean booleane);
    public void visit(Do_While do_while);
    public void visit(switch_case switch_case);
    public void visit(Header_switch header_switch);
    public void visit(Body_switch body_switch);
    public void visit (Case casee);
    public void visit (Default defaulte);
    public void visit (Call_func call_func);
    public void visit(Argument_list argument_list);
    public void visit(Argument_list_higher argument_list_higher);
    public void visit(heigher_order_function heigher_order_function);
    public void visit (Block block);
    public void visit (print printe);
    public void visit (Return_stm return_stm);
    public void visit(BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket);



    ///sql///
    public void visit (alter_table_stmt alter_table_stmt);
    public void visit(Source_table_name source_table_name);
    public void visit(New_table_name new_table_name);
    public void visit (alter_table_add alter_table_add);
    public void visit(alter_table_add_constraint alter_table_add_constraint);

    public void visit (table_constraint_primary_key table_constraint_primary_t);
    public void visit (table_constraint_key table_constraint_keye);
    public void visit(table_constraint_unique table_constraint_unique);
    public void visit(table_constraint_foreign_key table_constraint_foreign_key);
    public void visit(indexed_column indexed_columns);
    public void visit(fk_origin_column_name fk_origin_column_names);
    public void visit (foreign_key_clause foreign_key_clauses);
    public void visit (foreign_table foreign_tables);
    public void visit (fk_target_column_name fk_target_column_names);
    public void visit(column_constraint_foreign_key column_constraint_foreign_keye);


    /////////e///majd/////////


    /////////////sym/////////



    // For
    public void visit(forDecleration for_declaration);

    public void visit(DeclareForVar declare_for_var);
    public void visit(ForCondition for_condition);
    public void visit(forProcessVar for_declaration);

    public void visit(incrDecrWithoutComma inc_dec);
    public void visit(expFor exp_for);
    public void visit(exp_i expi);


    //If
    public void visit(IfDecleration if_declaration);
    public void visit(elseIf else_if);
    public void visit(Else els);


    //If Line
    public void visit(ifLine if_line);
    public void visit(ifLineFirstParameter if_line_f_pa);
    public void visit(ifLineSecondParameter if_line_s_pa);


    //Plus Var , Min Var
    public void visit(plusVar plus_var);
    public void visit(minVar min_var);



    //////////////////////////////// Sql //////////////////////////////////

    //////////////////////////////// Sql //////////////////////////////////

    //Insert
    public void visit(insertSql insert_sql);
    public void visit(db_name dbName);
    public void visit(anyNam any_name);
    public void visit(tb_name table_name);
    public void visit(col_name c_name);
    public void visit(dropTable drop_table);


    //Update
    public void visit(updateSql update_sql);
    public void visit(qualifiedTableName qualified_Table_Name);
    public void visit(indexNam index_name);
    public void visit(UpdateFirstParameter Update_First_Parameter);
    public void visit(UpdateSecondParameter Update_Second_Parameter);


    //Delete Sql
    public void visit(deleteSql delete_sql);


    //kk
    ///added_katren-java
    public void visit(ForEach forEach);
    public void visit(ForEachHeader forEachHeader);
    public void visit(DeclareArray declareArray);
    public void visit(GetPropertyJson getPropertyJson);
    public void visit(DeclareVar declareVar);
    public void visit(AssigneVar assigneVar);
    public void visit(JsonObjectStmt jsonObjectStmt);
    public void visit(JsonBody jsonBody);
    public void visit(PropertyJson propertyJson);
    public void visit(ArrayJson arrayJson);
    public void visit(LvalueJson lvalueJson);
    public void visit(SetPropertyJson setPropertyJson);
    public void visit(Array array);
    public void visit(AssigneArray assigneArray);
    public void visit(ElementArray elementArray);
    public void visit(UnaryExpr unaryExpr);
    public void visit(Order_Tearm order_tearm);
    public void visit(ResultColumn resultColumn);
    public void visit(AssigneSelect assigneSelect) ;
    public void visit(joinclaus joinclaus);
    public void visit(Function_name function_name);


}


