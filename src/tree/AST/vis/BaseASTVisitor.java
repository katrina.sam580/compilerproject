package tree.AST.vis;
import tree.AST.Parse;
import tree.AST.QueryStmt.*;
import tree.AST.javaStmt.*;

public class BaseASTVisitor implements ASTVisitor {
    @Override
    public void visit(Parse p) {
        System.out.println("----AST PARSE----");
    }
    @Override
    public void visit(FunctionDeclaration funcDec) {
        System.out.println("visit_ast FunctionDeclaration ");
    }

    @Override
    public void visit(SQL_Statement stmt) {

        System.out.println("visit_ast Statement ");
    }

    @Override
    public void visit(SelectStmt selectStmt) {
        System.out.println("visit_ast selectStmt ");

    }
    @Override
    public void visit(Expr expr) {
        //System.out.println("visit_ast Expr");
    }
    @Override
    public void visit(BodyFunction bodyFunction)
    {
        System.out.println("visit_ast bodyFunction");
    }
    @Override
    public void visit(HeaderFunction headerFunction)
    {
        System.out.println("visit_ast headerFunction");
    }
    @Override
    public void visit(FunctionName functionName)
    {
        System.out.println("visit_ast functionName");
    }
    @Override
    public void visit(Variable variable)
    {
        System.out.println("visit_ast variable");
    }


    //////////////////////
    @Override
    public void visit(table_or_subquery table_or_subquery)
    {
        System.out.println("visit_ast table_or_subquery");
    }

    @Override
    public void visit(CreateTable createTable)
    {
        System.out.println("visit_ast Create_table");
    }
    @Override
    public void visit(create_aggregation_function aggregation_function){
        System.out.println("visit_ast create_aggregation_function");
    }
    @Override
    public void visit(shcemaarray shcemaarray){
        System.out.println("visit_ast shcema_array");
    }
    @Override
    public void visit(column_def column_def)
    {
        System.out.println("visit_ast column_def");
    }
    @Override
    public void visit(column_constraint column_constraint)
    {
        System.out.println("visit_ast column_constraint");
    }
    @Override
    public void visit(Name name)
    {
        System.out.println("visit_ast Name");
    }
    @Override
    public void visit(column_default column_default)
    {
        System.out.println("visit_ast column_default");
    }
    @Override
    public void visit(signed_number signed_number)
    {
        System.out.println("visit_ast signed_number");
    }
    @Override
    public void visit(column_default_value column_default_value)
    {
        System.out.println("visit_ast column_default_value");
    }
    @Override
    public void visit(createType createType)
    {
        System.out.println("visit_ast Create_type");
    }

    @Override
    public void visit(create_type_name createTypeName)
    {
        System.out.println("visit_ast Create_type_name");
    }

    @Override
    public void visit(columnDefType column_def_type)
    {
        System.out.println("visit_ast column_def_type");
    }
    @Override
    public void visit(collation_name ccollation_name)
    {
        System.out.println("visit_ast collation_name");
    }
    @Override
    public void visit(type_name type_name)
    {
        System.out.println("visit_ast type_name");
    }
    @Override
    public void visit(table_constraint table_constraint)
    {
        System.out.println("visit_ast table_constraint");
    }



    /////////////////

    @Override
    public void visit(While whiles)
    {
        System.out.println("visit_ast While");
    }
    @Override
    public void visit (Header_while header_while)
    {
        System.out.println("visit_ast header_while");
    }

    @Override
    public void visit(condition_header condition_header)
    {

        System.out.println("visit_ast condition_header");
    }
    @Override
    public void visit(Bollean booleane)
    {
        System.out.println("visit_ast  boolean");
    }
    @Override
    public void visit(Do_While do_while)
    {
        System.out.println("visit_ast  do_while");
    }
    @Override
    public void visit (switch_case switch_case)
    {
        System.out.println("visit_ast  switch_case");
    }
    @Override
    public void visit (Header_switch header_switch)
    {
        System.out.println("visit_ast  Header_switch");
    }
    @Override
    public void visit (Body_switch body_switch)
    {
        System.out.println("visit_ast  Body_switch");
    }
    @Override
    public void visit (Case casee)
    {
        System.out.println("visit_ast  Case ");
    }
    @Override
    public void visit (Default defaulte)
    {
        System.out.println("visit_ast  Default");
    }
    @Override
    public void visit (Call_func call_func)
    {
        System.out.println("visit_ast Call_func");
    }
    @Override
    public void visit(Block block) {
        System.out.println("visit_ast Block");
    }
    @Override
    public void visit (print printe)
    {
        System.out.println("visit_ast print");
    }
    @Override
    public void visit (Return_stm return_stm)
    {
        System.out.println("visit_ast Return_stm ");
    }

    @Override
    public void visit(BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket) {

        {
            System.out.println("visit_ast BodyFunctionWilthoutBracket ");
        }
    }


    @Override
    public void visit (Argument_list argument_list)
    {
        System.out.println("visit_ast Argument_list");
    }
    @Override
    public void visit (heigher_order_function heigher_order_function)
    {
        System.out.println("visit_ast heigher_order_function");
    }


    @Override
    public void visit (Argument_list_higher argument_list_higher)
    {
        System.out.println("visit_ast Argument_list_higher");
    }


    ///////sql//////

    @Override
    public void visit (alter_table_stmt alter_table_stmt)
    {
        System.out.println("visit_ast alter_table_stmt");
    }

    @Override
    public void visit (alter_table_add alter_table_add)
    {
        System.out.println("visit_ast alter_table_add");
    }

    @Override
    public void visit (Source_table_name source_table_name){
        System.out.println("visit_ast Source_table_name");
    }

    @Override
    public void visit (New_table_name new_table_name){
        System.out.println("visit_ast New_table_name");
    }

    @Override
    public void visit (alter_table_add_constraint alter_table_add_constraint){
        System.out.println("visit_ast alter_table_add_constraint");
    }
    @Override
    public void visit ( table_constraint_primary_key table_constraint_primary_t)
    {
        System.out.println("visit_ast table_constraint_primary_key");
    }
    @Override
    public void visit ( table_constraint_key table_constraint_keye)
    {
        System.out.println("visit_ast table_constraint_key");
    }
    @Override
    public void visit ( table_constraint_unique table_constraint_unique)
    {
        System.out.println("visit_ast table_constraint_unique");
    }
    @Override
    public void visit ( table_constraint_foreign_key table_constraint_foreign_key)
    {
        System.out.println("visit_ast table_constraint_foreign_key");
    }
    @Override
    public void visit ( indexed_column indexed_columns)
    {
        System.out.println("visit_ast indexed_column");
    }
    @Override
    public void visit ( fk_origin_column_name fk_origin_column_names)
    {
        System.out.println("visit_ast fk_origin_column_name");
    }
    @Override
    public void visit ( foreign_key_clause foreign_key_clauses)
    {
        System.out.println("visit_ast foreign_key_clause");
    }
    @Override
    public void visit ( foreign_table foreign_tables)
    {
        System.out.println("visit_ast foreign_table");
    }
    @Override
    public void visit ( fk_target_column_name fk_target_column_names)
    {
        System.out.println("visit_ast fk_target_column_name");
    }
    @Override
    public void visit ( column_constraint_foreign_key column_constraint_foreign_keye)
    {
        System.out.println("visit_ast column_constraint_foreign_key");
    }


    //For
    @Override
    public void visit(forDecleration for_declaration)
    {
        System.out.println("visit_ast forDecleration");
    }

    @Override
    public void visit(DeclareForVar declare_for_var)
    {
        System.out.println("visit_ast DeclareForVar");
    }

    @Override
    public void visit( ForCondition for_condition)
    {
        System.out.println("visit_ast ForCondition");
    }


    @Override
    public void visit(forProcessVar for_proccess_var)
    {
        System.out.println("visit_ast forProcessVar");
    }


    @Override
    public void visit(incrDecrWithoutComma inc_dec)
    {
        System.out.println("visit_ast incrDecrWithoutComma");
    }

    @Override
    public void visit(expFor exp_for)
    {
        System.out.println("visit_ast expFor");
    }

    @Override
    public void visit(exp_i expi)
    {
        System.out.println("visit_ast exp_i");
    }




    //If
    @Override
    public void visit(IfDecleration if_declaration)
    {
        System.out.println("visit_ast IfDecleration");
    }



    //Else If
    @Override
    public void visit(elseIf else_if)
    {
        System.out.println("visit_ast elseIf");
    }


    //Else
    @Override
    public void visit(Else els)
    {
        System.out.println("visit_ast Else");
    }



    //If Line
    @Override
    public void visit(ifLine if_line)
    {
        System.out.println("visit_ast ifLine");
    }



    //If Line First Parameter
    @Override
    public void visit(ifLineFirstParameter if_line_f_pa)
    {
        System.out.println("visit_ast ifLineFirstParameter");
    }

    //If Line Second Parameter
    @Override
    public void visit(ifLineSecondParameter if_line_s_pa)
    {
        System.out.println("visit_ast ifLineSecondParameter");
    }



    ////////////////////////////////// Sql //////////////////////////


    //Insert
    @Override
    public void visit(insertSql insert_sql )
    {
        System.out.println("visit_ast insertSql");
    }

    @Override
    public void visit(db_name dbName )
    {
        System.out.println("visit_ast db_name");
    }

    @Override
    public void visit(tb_name tbName )
    {
        System.out.println("visit_ast tb_name");
    }


    @Override
    public void visit(anyNam any_name )
    {
        System.out.println("visit_ast anyName");
    }

    @Override
    public void visit(col_name c_name )
    {
        System.out.println("visit_ast col_name");
    }






    //Drop Table
    @Override
    public void visit(dropTable drop_table  )
    {
        System.out.println("visit_ast dropTable");
    }


    //Update
    @Override
    public void visit(updateSql update_sql  )
    {
        System.out.println("visit_ast updateSql");
    }


    @Override
    public void visit(qualifiedTableName qualified_Table_Name)
    {
        System.out.println("visit_ast qualifiedTableName");
    }

    @Override
    public void visit(indexNam index_name  )
    {
        System.out.println("visit_ast indexName");
    }


    @Override
    public void visit(UpdateFirstParameter Update_First_Parameter  )
    {
        System.out.println("visit_ast UpdateFirstParameter");
    }


    @Override
    public void visit(UpdateSecondParameter Update_Second_Parameter  )
    {
        System.out.println("visit_ast UpdateSecondParameter");
    }



    @Override
    public void visit(deleteSql delete_sql  )
    {
        System.out.println("visit_ast deleteSql");
    }



    //Plus Var , Min Var
    @Override
    public void visit(plusVar plus_var)
    {
        System.out.println("visit_ast plusVar");
    }
/*
    @Override
    public void visit(AssigneArray assign_array)
    {
        System.out.println("visit_ast AssigneArray");
    }*/

    @Override
    public void visit(minVar min_var)
    {
        System.out.println("visit_ast minVar");
    }









    //kk
@Override
public void visit(ForEach forEach)
{
    System.out.println("visit_ast ForEach");
}
    @Override
    public void visit(ForEachHeader forEachHeader)
    {
        System.out.println("visit_ast ForEachHeader");
    }
    @Override
    public void visit(DeclareArray declareArray)
    {
        System.out.println("visit_ast DeclareArray");
    }
    @Override
    public void visit(GetPropertyJson getPropertyJson)
    {
        System.out.println("visit_ast GetPropertyJson");
    }
    @Override
    public void visit(DeclareVar declareVar)
    {
        System.out.println("visit_ast DeclareVar");
    }
    @Override
    public void visit(AssigneVar assigneVar)
    {
        System.out.println("visit_ast AssigneVar");
    }
    @Override
    public void visit(JsonObjectStmt jsonObjectStmt)
    {
        System.out.println("visit_ast JsonObjectStmt");
    }
    @Override
    public void visit(JsonBody jsonBody)
    {
        System.out.println("visit_ast JsonBody");
    }
    @Override
    public void visit(PropertyJson propertyJson)
    {
        System.out.println("visit_ast PropertyJson");
    }
    @Override
    public void visit(ArrayJson arrayJson)
    {
        System.out.println("visit_ast ArrayJson");
    }
    @Override
    public void visit(LvalueJson lvalueJson)
    {
        System.out.println("visit_ast LvalueJson");
    }
    @Override
    public void visit(SetPropertyJson setPropertyJson)
    {
        System.out.println("visit_ast SetPropertyJson");
    }
    @Override
    public void visit(Array array)
    {
        System.out.println("visit_ast Array");
    }
    @Override
    public void visit(AssigneArray assigneArray)
    {
        System.out.println("visit_ast AssigneArray");
    }
    @Override
    public void visit(ElementArray elementArray)
    {
        System.out.println("visit_ast ElementArray");
    }


    @Override
    public void visit(UnaryExpr unaryExpr)
    {
        System.out.println("visit_ast UnaryExpr");
    }

    @Override
    public void visit(Order_Tearm order_tearm)
    {
        System.out.println("visit_ast Order_Tearm");
    }
    @Override
    public void visit(ResultColumn resultColumn)
    {
        System.out.println("visit_ast ResultColumn");
    }
    @Override
    public void visit(AssigneSelect assigneSelect)
    {
        System.out.println("visit_ast AssigneSelect");
    }
    @Override
    public void visit(joinclaus joinclaus)
    {
        System.out.println("visit_ast joinclaus");
    }

    @Override
    public void visit(Function_name function_name) {
            System.out.println("visit_ast Function_name");
    }
}
