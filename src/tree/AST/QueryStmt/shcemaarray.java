package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class shcemaarray extends Node {

    List<String> elements = new ArrayList<>();

    public void setElements(List<String> elements) {
        this.elements = elements;
    }

    public List<String> getElements() {
        return elements;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if (this.elements.size()>0){
            for(int i=0;i<this.elements.size();i++){
                System.out.println("element_"+i+" : "+elements.get(i));
            }
        }
    }
}
