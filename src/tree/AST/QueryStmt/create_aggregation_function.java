package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.List;

public class create_aggregation_function extends Node {

    Function_name function_name;
    String JarPath;
    String ClassName;
    String MethodName;
    shcemaarray array;
    String return_type;

    public void setReturn_type(String return_type) {
        this.return_type = return_type;
    }

    public void setFunction_name(Function_name function_name) {
        this.function_name = function_name;
    }

    public void setJarParth(String jarParth) {
        JarPath = jarParth;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public void setMethodName(String methodName) {
        MethodName = methodName;
    }

    public void setArray(shcemaarray array) {
        this.array = array;
    }

    public Function_name getFunction_name() {
        return function_name;
    }

    public String getJarParth() {
        return JarPath;
    }

    public String getClassName() {
        return ClassName;
    }

    public shcemaarray getArray() {
        return array;
    }

    public String getReturn_type() {
        return return_type;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if (this.function_name!=null){
            this.function_name.accept(astVisitor);
        }
        if (this.JarPath!=null){
            System.out.println("jar path : "+this.JarPath);
        }
        if (this.ClassName!=null){
            System.out.println("class name : "+this.ClassName);
        }
        if(this.MethodName!=null){
            System.out.println("method name : "+this.MethodName);
        }
        if(this.return_type!=null){

            System.out.println("return type : "+this.return_type);
        }
        if (this.array!=null){
            this.array.accept(astVisitor);
        }
    }
}
