package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class indexed_column extends Node {
    tree.AST.QueryStmt.col_name col_name;
    tree.AST.QueryStmt.collation_name collation_name;

    public void setCollation_name(tree.AST.QueryStmt.collation_name collation_name) {
        this.collation_name = collation_name;
    }

    public void setCol_name(tree.AST.QueryStmt.col_name col_name) {
        this.col_name = col_name;
    }

    public tree.AST.QueryStmt.col_name getCol_name() {
        return col_name;
    }

    public tree.AST.QueryStmt.collation_name getCollation_name() {
        return collation_name;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getCol_name()!=null)
        {
            this.col_name.accept(astVisitor);

        }
        if(this.getCollation_name()!=null)
        {
            this.collation_name.accept(astVisitor);
        }


    }
}
