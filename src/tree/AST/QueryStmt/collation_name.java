package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class collation_name extends Node {
    anyNam collation_name;

    public void setCollation_name(anyNam collation_name) {
        this.collation_name = collation_name;
    }

    public anyNam getCollation_name() {
        return collation_name;
    }


    @Override
    public void accept(ASTVisitor astVisitor){


        astVisitor.visit(this);
        collation_name.accept(astVisitor);

    }
}
