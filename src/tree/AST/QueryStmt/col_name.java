package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class col_name extends Node {
        anyNam any_name = new anyNam();

    public void setAny_name(anyNam any_name) {
        this.any_name = any_name;
    }

    public anyNam getAny_name() {
        return any_name;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        this.any_name.accept(astVisitor);
    }

}
