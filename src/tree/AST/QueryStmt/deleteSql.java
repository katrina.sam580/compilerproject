package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

public class deleteSql extends Node {

    qualifiedTableName qualified_table_name ;

    Expr exp ;


    public void setQualified_table_name(qualifiedTableName qualified_table_name) {
        this.qualified_table_name = qualified_table_name;
    }

    public qualifiedTableName getQualified_table_name() {
        return qualified_table_name;
    }

    public void setExp(Expr exp) {
        this.exp = exp;
    }

    public Expr getExp() {
        return exp;
    }
@Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);
        this.qualified_table_name.accept(astVisitor);
        this.exp.accept(astVisitor);

    }
}
