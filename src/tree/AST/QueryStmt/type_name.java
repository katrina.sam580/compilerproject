package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class type_name extends Node {
   // anyNam name = new anyNam();
    Name name ;

    public void setNames(Name names) {
        this.name = names;
    }

    public Name getNames() {
        return name;
    }

    // signed_number signed_number = new signed_number();
    List<signed_number> signed_numbers= new ArrayList<>();
    List<anyNam> anyNams= new ArrayList<>();



    public void setAnyNams(List<anyNam> anyNams) {
        this.anyNams = anyNams;
    }

    public void setSigned_numbers(List<signed_number> signed_numbers) {
        this.signed_numbers = signed_numbers;
    }



    public List<anyNam> getAnyNams() {
        return anyNams;
    }

    public List<signed_number> getSigned_numbers() {
        return signed_numbers;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(signed_numbers.size()>0)
        {
            for(int i=0;i<signed_numbers.size();i++)
            {
                this.signed_numbers.get(i).accept(astVisitor);
            }
        }
        if(anyNams.size()>0)
        {
            for(int i=0;i<anyNams.size();i++)
            {
                this.anyNams.get(i).accept(astVisitor);
            }
        }
        if(name.name.getName()!=null)
        {
            this.name.accept(astVisitor);
        }
    }

}
