package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class Name extends Node {
    anyNam name;

    public void setName(anyNam name) {
        this.name = name;
    }

    public anyNam getName() {
        return name;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);
        name.accept(astVisitor);
        //System.out.println("Name : "+name.getName());

    }

}
