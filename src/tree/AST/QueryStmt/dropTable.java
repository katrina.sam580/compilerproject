package tree.AST.QueryStmt;


import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class dropTable extends Node {

    db_name dbName ;
    tb_name tbName ;



    public void setDbName(db_name dbName) {
        this.dbName = dbName;
    }

    public void setTbName(tb_name tbName) {
        this.tbName = tbName;
    }

    public db_name getDbName() {
        return dbName;
    }

    public tb_name getTbName() {
        return tbName;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.dbName != null){
            this.dbName.accept(astVisitor);
        }

        this.tbName.accept(astVisitor);
    }

}
