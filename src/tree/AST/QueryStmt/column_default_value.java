package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class column_default_value extends Node {
    signed_number signed_numbers;
    String literal ;

    public void setSigned_numbers(signed_number signed_numbers) {
        this.signed_numbers = signed_numbers;
    }

    public void setLiteral(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }

    public signed_number getSigned_numbers() {
        return signed_numbers;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(signed_numbers.numeric!=null)
        {
            this.signed_numbers.accept(astVisitor);
        }
        if(literal!=null)
        {
            System.out.println("column_default_value"+literal);
        }
    }
}
