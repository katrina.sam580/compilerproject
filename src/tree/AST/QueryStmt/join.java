package tree.AST.QueryStmt;

import tree.AST.Node;

public class join extends Node {

    private boolean outer = false;
    private boolean left = false;
    private boolean inner = false;
    private String left_item;
    private String right_item;

    public void setOuter(boolean outer){this.outer=true;}
    public void setInner(boolean inner){this.inner=true;}
    public void setLeft(boolean left){this.left=true;}
    public void setLeft_item(String left_item){this.left_item=left_item;}
    public void setRight_item(String right_item){this.right_item=right_item;}

    public boolean getOuter(){return this.outer;}
    public boolean getInner(){return this.inner;}
    public boolean getLeft(){return this.left;}
    public String getLeft_item(){return this.left_item;}
    public String getRight_item(){return this.right_item;}

//    private FromItem rightItem;
//    private Expression onExpression;
}
