package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class table_constraint_primary_key extends Node {
    List<indexed_column> indexed_columnList= new ArrayList<>();

    public void setIndexed_columnList(List<indexed_column> indexed_columnList) {
        this.indexed_columnList = indexed_columnList;
    }

    public List<indexed_column> getIndexed_columnList() {
        return indexed_columnList;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getIndexed_columnList().size()>0)
        {
            for(int i=0;i<this.indexed_columnList.size();i++)
            {
                this.indexed_columnList.get(i).accept(astVisitor);
            }
        }


    }

}
