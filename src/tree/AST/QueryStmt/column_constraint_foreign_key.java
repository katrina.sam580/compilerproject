package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class column_constraint_foreign_key extends Node {
    tree.AST.QueryStmt.foreign_key_clause foreign_key_clause ;

    public void setForeign_key_clause(tree.AST.QueryStmt.foreign_key_clause foreign_key_clause) {
        this.foreign_key_clause = foreign_key_clause;
    }

    public tree.AST.QueryStmt.foreign_key_clause getForeign_key_clause() {
        return foreign_key_clause;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getForeign_key_clause()!=null)
        {
            this.foreign_key_clause.accept(astVisitor);
        }
    }
}
