package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class qualifiedTableName extends Node {
    db_name dbName ;
    tb_name tbName ;
    indexNam in_name ;

    public void setDbName(db_name dbName) {
        this.dbName = dbName;
    }

    public void setTbName(tb_name tbName) {
        this.tbName = tbName;
    }

    public db_name getDbName() {
        return dbName;
    }

    public tb_name getTbName() {
        return tbName;
    }

    public void setIn_name(indexNam in_name) {
        this.in_name = in_name;
    }

    public indexNam getIn_name() {
        return in_name;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.dbName != null){
            this.dbName.accept(astVisitor);
        }

        if(this.tbName != null){
            this.tbName.accept(astVisitor);
        }

        if(this.in_name != null){
            this.in_name.accept(astVisitor);
        }

    }


}
