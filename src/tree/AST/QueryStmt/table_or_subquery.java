package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class table_or_subquery extends Node {

    db_name db_name;
    tb_name tb_name;
    String table_alias_1;
    String table_alias_2;
    String table_alias_3;
    String index_name;
    List<table_or_subquery>table_or_subqueries=new ArrayList<>();
    SelectStmt selectStmt;
    joinclaus joinclaus;

    public void setJoinclaus(tree.AST.QueryStmt.joinclaus joinclaus) {
        this.joinclaus = joinclaus;
    }

    public void setTable_or_subqueries(List<table_or_subquery> table_or_subqueries) {
        this.table_or_subqueries = table_or_subqueries;
    }

    //    public void setSelectStmt(SelectStmt selectStmt) {
//        this.selectStmt = selectStmt;
//    }
//    public SelectStmt getSelectStmt() {
//        return selectStmt;
//    }

    //    public void setTable_or_subqueries(List<table_or_subquery> table_or_subqueries) {
//        this.table_or_subqueries = table_or_subqueries;
//    }
//
//    public List<table_or_subquery> getTable_or_subqueries() {
//        return table_or_subqueries;
//    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    public void setDb_name(tree.AST.QueryStmt.db_name db_name) {
        this.db_name = db_name;
    }

    public void setTb_name(tree.AST.QueryStmt.tb_name tb_name) {
        this.tb_name = tb_name;
    }

    public tree.AST.QueryStmt.db_name getDb_name() {
        return db_name;
    }

    public tree.AST.QueryStmt.tb_name getTb_name() {
        return tb_name;
    }

    public void setTable_alias_1(String table_alias_1) {
        this.table_alias_1 = table_alias_1;
    }

    public void setTable_alias_2(String table_alias_2) {
        this.table_alias_2 = table_alias_2;
    }

    public String getTable_alias_3() {
        return table_alias_3;
    }

    public void setIndex_name(String index_name) {
        this.index_name = index_name;
    }

    public String getIndex_name() {
        return index_name;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);
        if(this.db_name!=null)
        {
            this.db_name.accept(astVisitor);
        }
        if(this.tb_name!=null)
        {
            this.tb_name.accept(astVisitor);
        }
        if(table_or_subqueries.size()>0)
        {
            for(int i=0;i<table_or_subqueries.size();i++)
            {
                this.table_or_subqueries.get(i).accept(astVisitor);
            }
        }
        if(this.index_name!=null)
        {
            System.out.println("index_name : "+this.index_name);
        }

        if(this.table_alias_1!=null)
        {
            System.out.println("table_alias : "+this.table_alias_1);
        }

        if(this.selectStmt!=null)
        {
            this.selectStmt.accept(astVisitor);
        }
        if(this.joinclaus!=null)
        {
            this.joinclaus.accept(astVisitor);
        }

    }

}

