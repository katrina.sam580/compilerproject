package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class alter_table_add_constraint extends Node {
    tree.AST.QueryStmt.anyNam anyNam ;
    tree.AST.QueryStmt.table_constraint table_constraint;

    public void setTable_constraint(tree.AST.QueryStmt.table_constraint table_constraint) {
        this.table_constraint = table_constraint;
    }

    public void setAnyNam(tree.AST.QueryStmt.anyNam anyNam) {
        this.anyNam = anyNam;
    }

    public tree.AST.QueryStmt.table_constraint getTable_constraint() {
        return table_constraint;
    }

    public tree.AST.QueryStmt.anyNam getAnyNam() {
        return anyNam;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getAnyNam()!=null)
        {
            this.anyNam.accept(astVisitor);
        }
        if(this.getTable_constraint()!=null)
        {
            this.table_constraint.accept(astVisitor);
        }
    }
}
