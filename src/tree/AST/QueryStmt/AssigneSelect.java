package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Bollean;
import tree.AST.javaStmt.DeclareArray;
import tree.AST.javaStmt.DeclareVar;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

public class AssigneSelect extends Node {
    String var;
    String value_var;
    Expr exp;
    Bollean boll_expr ;
    Symbol symbol ;
    SelectStmt selectStmt;
    DeclareArray declareArray;
    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }

    public void setBoll_expr(Bollean boll_expr) {
        this.boll_expr = boll_expr;
    }

    public Bollean getBoll_expr() {
        return boll_expr;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setExp(Expr exp) {
        this.exp = exp;
    }

    public void setValue_var(String value_var) {
        this.value_var = value_var;
    }

    public String getVar() {
        return var;
    }

    public Expr getExp() {
        return exp;
    }
    public void setDeclareArray(DeclareArray declareArray) {
        this.declareArray = declareArray;
    }


    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        System.out.println("Varname : "+var);
        if(this.exp!=null)
        {
            this.exp.accept(astVisitor);
        }
        if(this.value_var!=null)
        {
            System.out.println("value_var : "+this.value_var);
        }
        if(this.boll_expr!=null){
            this.boll_expr.accept(astVisitor);
            //   System.out.println("boll_expr : "+this.boll_expr);
        }
        if(this.declareArray!=null)
        {
            this.declareArray.accept(astVisitor);
        }
        if(this.selectStmt!=null)
        {
            this.selectStmt.accept(astVisitor);
        }

    }
}
