package tree.AST.QueryStmt;

import tree.AST.javaStmt.Expr;
import tree.AST.javaStmt.LogicalExpr;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class expr_sql extends Expr {
    Expr left_expr;
    Expr right_expr;
    List<Expr> listexpr ;
    public enum OP_sql
    {
        K_IS ,
        K_NOT,
        K_IN,
        K_NOT_K_IN,
        K_LIKE,
        K_GLOB ,
        K_MATCH,
        K_REGEXP,
        K_EXISTS
    }

    OP_sql op_sql;

    public void setListexpr(List<Expr> listexpr) {
        this.listexpr = listexpr;
    }

    public List<Expr> getListexpr() {
        return listexpr;
    }

    public void setOp_sql(OP_sql op_sql) {
        this.op_sql = op_sql;
    }

    public OP_sql getOp_sql() {
        return op_sql;
    }

    public void setLeft_expr(Expr left_expr) {
        this.left_expr = left_expr;
    }

    public void setRight_expr(Expr right_expr) {
        this.right_expr = right_expr;
    }

    public Expr getLeft_expr() {
        return left_expr;
    }

    public Expr getRight_expr() {
        return right_expr;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.left_expr!=null) {
            left_expr.accept(astVisitor);
        }
        System.out.println("Logic OP : "+this.op_sql);
        if(this.right_expr!=null) {
            right_expr.accept(astVisitor);
        }


    }
}
