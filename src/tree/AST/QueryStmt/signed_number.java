package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class signed_number extends Node {
    String numeric;

    enum OP_signed_number
    {
        PLUS,
        MINUS,
        STAR
    }
    OP_signed_number op_logic;

    public void setNumeric(String numeric) {
        this.numeric = numeric;
    }

    public void setOp_logic(OP_signed_number op_logic) {
        this.op_logic = op_logic;
    }

    public OP_signed_number getOp_logic() {
        return op_logic;
    }

    public String getNumeric() {
        return numeric;
    }


    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        System.out.println("signed_number : "+numeric);
    }
}
