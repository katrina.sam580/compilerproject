package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

public class ResultColumn extends Node {

    tb_name tb_name;
    Expr expr;
    String alias_name;
    String star;

    public void setTb_name(tree.AST.QueryStmt.tb_name tb_name) {
        this.tb_name = tb_name;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public Expr getExpr() {
        return expr;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getStar() {
        return star;
    }

    public void setAlias_name(String alias_name) {
        this.alias_name = alias_name;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.tb_name!=null)
        {
            this.tb_name.accept(astVisitor);
        }
        if(this.expr!=null)
        {
            this.expr.accept(astVisitor);
        }
        if(this.alias_name!=null)
        {
            System.out.println("Alias_name : "+this.alias_name);
        }
        if (this.star!=null)
        {
            System.out.println("All column : "+star);
        }
    }

}
