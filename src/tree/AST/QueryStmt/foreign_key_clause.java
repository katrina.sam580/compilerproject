package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class foreign_key_clause extends Node {

    db_name databaseName ;
    tree.AST.QueryStmt.foreign_table foreign_table ;
    List<fk_target_column_name> fk_target_column_names= new ArrayList<>();
    List<Name> name = new ArrayList<>();

    public void setName(List<Name> name) {
        this.name = name;
    }

    public List<Name> getName() {
        return name;
    }

    public void setDatabaseName(db_name databaseName) {
        this.databaseName = databaseName;
    }

    public void setFk_target_column_names(List<fk_target_column_name> fk_target_column_names) {
        this.fk_target_column_names = fk_target_column_names;
    }

    public void setForeign_table(tree.AST.QueryStmt.foreign_table foreign_table) {
        this.foreign_table = foreign_table;
    }


    public db_name getDatabaseName() {
        return databaseName;
    }

    public tree.AST.QueryStmt.foreign_table getForeign_table() {
        return foreign_table;
    }

    public List<fk_target_column_name> getFk_target_column_names() {
        return fk_target_column_names;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.getDatabaseName()!=null)
        {
            this.databaseName.accept(astVisitor);
        }
        if(this.getForeign_table()!=null)
        {
            this.foreign_table.accept(astVisitor);
        }

        if(this.getFk_target_column_names().size()>0)
        {
            for(int i=0;i<this.fk_target_column_names.size();i++)
            {
                this.fk_target_column_names.get(i).accept(astVisitor);
            }
        }
        if(this.getName().size()>0)
        {
            for(int i=0;i<this.name.size();i++)
            {
                this.name.get(i).accept(astVisitor);
            }
        }


    }
}
