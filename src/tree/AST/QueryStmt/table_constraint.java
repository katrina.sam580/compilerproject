package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

public class table_constraint  extends Node {
    Name Nam;
    Expr expr;
    tree.AST.QueryStmt.table_constraint_primary_key table_constraint_primary_key;
    table_constraint_key table_constraint_key;
    table_constraint_unique table_constraint_unique ;

    table_constraint_foreign_key table_constraint_foreign_key;

    public void setExpr(Expr expr) {
        this.expr = expr;
    }
    public void setNam(Name nam) {
        Nam = nam;
    }
    public void setTable_constraint_foreign_key(tree.AST.QueryStmt.table_constraint_foreign_key table_constraint_foreign_key) {
        this.table_constraint_foreign_key = table_constraint_foreign_key;
    }
    public void setTable_constraint_key(tree.AST.QueryStmt.table_constraint_key table_constraint_key) {
        this.table_constraint_key = table_constraint_key;
    }
    public void setTable_constraint_primary_key(tree.AST.QueryStmt.table_constraint_primary_key table_constraint_primary_key) {
        this.table_constraint_primary_key = table_constraint_primary_key;
    }
    public void setTable_constraint_unique(tree.AST.QueryStmt.table_constraint_unique table_constraint_unique) {
        this.table_constraint_unique = table_constraint_unique;
    }
    public Name getNam() {
        return Nam;
    }
    public Expr getExpr() {
        return expr;
    }
    public tree.AST.QueryStmt.table_constraint_foreign_key getTable_constraint_foreign_key() {
        return table_constraint_foreign_key;
    }
    public tree.AST.QueryStmt.table_constraint_key getTable_constraint_key() {
        return table_constraint_key;
    }
    public tree.AST.QueryStmt.table_constraint_primary_key getTable_constraint_primary_key() {
        return table_constraint_primary_key;
    }
    public tree.AST.QueryStmt.table_constraint_unique getTable_constraint_unique() {
        return table_constraint_unique;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(Nam!=null)
        {
            this.Nam.accept(astVisitor);
        }
        if(this.expr!=null)
        {
            this.expr.accept(astVisitor);
        }
        if(this.getTable_constraint_primary_key()!=null)
        {
            this.table_constraint_primary_key.accept(astVisitor);
        }
        if(this.getTable_constraint_key()!=null)
        {
            this.table_constraint_key.accept(astVisitor);
        }
        if(this.getTable_constraint_unique()!=null)
        {
            this.table_constraint_unique.accept(astVisitor);
        }
        if(this.getTable_constraint_foreign_key()!=null)
        {
            this.table_constraint_foreign_key.accept(astVisitor);
        }
    }

}
