package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class columnDefType extends Node {

    tree.AST.QueryStmt.col_name col_name;

    // type_name type_names= new type_name();
    List<type_name> type_names= new ArrayList<>();



    public tree.AST.QueryStmt.col_name getCol_name() {
        return col_name;
    }

    /*public type_name getType_names() {
        return type_names;
    }

    public void setType_names(type_name type_names) {
        this.type_names = type_names;
    }*/

    public List<type_name> getType_names() {
        return type_names;
    }

    public void setType_names(List<type_name> type_names) {
        this.type_names = type_names;
    }

    public void setCol_name(tree.AST.QueryStmt.col_name col_name) {
        this.col_name = col_name;
    }



    @Override
    public void accept(ASTVisitor astVisitor) {

        astVisitor.visit(this);

        if(this.col_name != null){
            this.col_name.accept(astVisitor);
        }

    /*    if(this.type_names!=null) {

            this.type_names.accept(astVisitor);

        }*/

        if(type_names.size()>0)
        {
            for(int i=0;i<this.type_names.size();i++)
            {
                this.type_names.get(i).accept(astVisitor);
            }
        }

    }
}
