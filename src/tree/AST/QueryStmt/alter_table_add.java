package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class alter_table_add extends Node {
    tree.AST.QueryStmt.table_constraint table_constraint;

    public void setTable_constraint(tree.AST.QueryStmt.table_constraint table_constraint) {
        this.table_constraint = table_constraint;
    }

    public tree.AST.QueryStmt.table_constraint getTable_constraint() {
        return table_constraint;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getTable_constraint()!=null)
        {
            this.table_constraint.accept(astVisitor);
        }

    }
}
