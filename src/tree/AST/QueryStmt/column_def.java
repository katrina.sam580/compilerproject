package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class column_def extends Node {
    tree.AST.QueryStmt.col_name col_name;
    List<column_constraint> column_constraints= new ArrayList<>();
    List<type_name> type_names= new ArrayList<>();

    public void setCol_name(tree.AST.QueryStmt.col_name col_name) {
        this.col_name = col_name;
    }

    public void setColumn_constraints(List<column_constraint> column_constraints) {
        this.column_constraints = column_constraints;
    }


    public void setType_names(List<type_name> type_names) {
        this.type_names = type_names;
    }

    public tree.AST.QueryStmt.col_name getCol_name() {
        return col_name;
    }

    public List<column_constraint> getColumn_constraints() {
        return column_constraints;
    }


    public List<type_name> getType_names() {
        return type_names;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(col_name.any_name.name!=null)
        {
            this.col_name.accept(astVisitor);
        }
        if(column_constraints.size()>0)
        {
            for(int i=0;i<this.column_constraints.size();i++)
            {
                this.column_constraints.get(i).accept(astVisitor);
            }
        }
        if(type_names.size()>0)
        {
            for(int i=0;i<this.type_names.size();i++)
            {
                this.type_names.get(i).accept(astVisitor);
            }
        }
    }
}
