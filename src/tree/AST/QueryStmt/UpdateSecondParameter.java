package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class UpdateSecondParameter extends Node {
    List<col_name> c_name = new ArrayList<>();
    List<Expr> exp = new ArrayList<>();

    public void setC_name(List<col_name> c_name) {
        this.c_name = c_name;
    }

    public void setExp(List<Expr> exp) {
        this.exp = exp;
    }

    public List<col_name> getC_name() {
        return c_name;
    }

    public List<Expr> getExp() {
        return exp;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(c_name.size()>0)
        {
            for(int i=0;i<c_name.size();i++)
            {
                this.c_name.get(i).accept(astVisitor);
            }
        }
        if(exp.size()>0)
        {
            for(int i=0;i<exp.size();i++)
            {
                this.exp.get(i).accept(astVisitor);
            }
        }


    }
}
