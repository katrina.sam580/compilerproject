package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class column_default extends Node {
    column_default_value column_default_values;
    Expr expr;
    List<anyNam> anyNames = new ArrayList<>();

    public void setAnyNams(List<anyNam> anyNams) {
        this.anyNames = anyNams;
    }

    public List<anyNam> getAnyNams() {
        return anyNames;
    }

    public void setColumn_default_values(column_default_value column_default_values) {
        this.column_default_values = column_default_values;
    }

    public column_default_value getColumn_default_values() {
        return column_default_values;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public Expr getExpr() {
        return expr;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(column_default_values.literal!=null||column_default_values.signed_numbers!=null)
        {
            this.column_default_values.accept(astVisitor);
        }
        if(expr.getValue()!=null)
        {
            this.expr.accept(astVisitor);
        }
        if(anyNames.size()>0) {
            for (int i=0;i<anyNames.size();i++)
            {
                this.anyNames.get(i).accept(astVisitor);

            }
        }
    }

}
