package tree.AST.QueryStmt;
import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

public class Order_Tearm extends Node {

    Expr tearm;
    String collation_name;

    public void setTearm(Expr tearm) {
        this.tearm = tearm;
    }

    public Expr getTearm() {
        return tearm;
    }

    public void setCollation_name(String collation_name) {
        this.collation_name = collation_name;
    }

    public String getCollation_name() {
        return collation_name;
    }
    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if(this.tearm!=null)
        {
            this.tearm.accept(astVisitor);
        }
        if(this.collation_name!=null)
        {
            System.out.println("collation_name : "+this.collation_name);
        }
    }
}
