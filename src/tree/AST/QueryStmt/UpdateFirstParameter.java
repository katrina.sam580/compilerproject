package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;


public class UpdateFirstParameter extends Node {
    col_name c_name ;
    Expr exp ;

    public void setC_name(col_name c_name) {
        this.c_name = c_name;
    }

    public void setExp(Expr exp) {
        this.exp = exp;
    }

    public col_name getC_name() {
        return c_name;
    }

    public Expr getExp() {
        return exp;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        this.c_name.accept(astVisitor);
        this.exp.accept(astVisitor);
    }
}
