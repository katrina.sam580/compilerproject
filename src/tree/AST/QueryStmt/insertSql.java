package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class insertSql extends Node {
    db_name dbName ;
    tb_name tbName ;
    List<col_name> c_name = new ArrayList<>();
    List<Expr> exp = new ArrayList<>();

    SelectStmt select_stmt ;



    public void setDbName(db_name dbName) {
        this.dbName = dbName;
    }

    public void setTbName(tb_name tbName) {
        this.tbName = tbName;
    }

    public db_name getDbName() {
        return dbName;
    }

    public tb_name getTbName() {
        return tbName;
    }

   public void setC_name(List<col_name> c_name) {
       this.c_name = c_name;
   }

    public List<col_name> getC_name() {
        return c_name;
    }


    public void setExp(List<Expr> exp) {
        this.exp = exp;
    }

    public List<Expr> getExp() {
        return exp;
    }


    public void setSelect_stmt(SelectStmt select_stmt) {
        this.select_stmt = select_stmt;
    }

    public SelectStmt getSelect_stmt() {
        return select_stmt;
    }

    @Override
   public void accept(ASTVisitor astVisitor){
    astVisitor.visit(this);
    if(this.dbName != null){
        this.dbName.accept(astVisitor);
    }

    if(this.tbName != null){
        this.tbName.accept(astVisitor);
    }


    if(c_name.size()>0)
    {
        // System.out.println("size: "+c_name.size());
        for(int i=0;i<c_name.size();i++)
        {
            this.c_name.get(i).accept(astVisitor);
        }
    }


    if(exp.size()>0)
    {
        for(int i=0;i<exp.size();i++)
        {
            this.exp.get(i).accept(astVisitor);
        }
    }

        if(this.select_stmt != null){
            this.select_stmt.accept(astVisitor);
        }


}



}
