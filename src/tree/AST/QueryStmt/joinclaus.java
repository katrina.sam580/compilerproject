package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class joinclaus extends Node {

    List<table_or_subquery>table_or_subqueries;
    List<Expr> join_constrain=new ArrayList<>();


    public void setJoin_constrain(List<Expr> join_constrain) {
        this.join_constrain = join_constrain;
    }

    public void setTable_or_subqueries(List<table_or_subquery> table_or_subqueries) {
        this.table_or_subqueries = table_or_subqueries;
    }

    public List<Expr> getJoin_constrain() {
        return join_constrain;
    }

    public List<table_or_subquery> getTable_or_subqueries() {
        return table_or_subqueries;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if(this.table_or_subqueries.size()>0)
        {
            for(int i=0;i<this.table_or_subqueries.size();i++)
            {
                this.table_or_subqueries.get(i).accept(astVisitor);
            }
        }
        if(this.join_constrain.size()>0)
        {
            for(int i=0;i<this.join_constrain.size();i++)
            {
                this.join_constrain.get(i).accept(astVisitor);
            }
        }

    }

}
