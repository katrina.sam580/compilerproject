package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

public class column_constraint  extends Node {
   // anyNam nam= new anyNam();
    Name nam ;
    column_constraint_primary_key column_constraint_primary_key;
    tree.AST.QueryStmt.column_constraint_foreign_key column_constraint_foreign_key ;
    tree.AST.QueryStmt.column_constraint_not_null column_constraint_not_null;
    tree.AST.QueryStmt.column_constraint_null column_constraint_null;
    Expr expr;
    column_default column_defaults;
    tree.AST.QueryStmt.collation_name collation_name;

    public void setName(Name name) {
        this.nam = name;
    }

    public Name getName() {
        return nam;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }


    public void setCollation_name(tree.AST.QueryStmt.collation_name collation_name) {
        this.collation_name = collation_name;
    }

    public void setColumn_constraint_foreign_key(tree.AST.QueryStmt.column_constraint_foreign_key column_constraint_foreign_key) {
        this.column_constraint_foreign_key = column_constraint_foreign_key;
    }

    public void setColumn_constraint_not_null(tree.AST.QueryStmt.column_constraint_not_null column_constraint_not_null) {
        this.column_constraint_not_null = column_constraint_not_null;
    }

    public void setColumn_constraint_null(tree.AST.QueryStmt.column_constraint_null column_constraint_null) {
        this.column_constraint_null = column_constraint_null;
    }

    public void setColumn_constraint_primary_key(tree.AST.QueryStmt.column_constraint_primary_key column_constraint_primary_key) {
        this.column_constraint_primary_key = column_constraint_primary_key;
    }

    public void setColumn_defaults(column_default column_defaults) {
        this.column_defaults = column_defaults;
    }

    public Expr getExpr() {
        return expr;
    }



    public tree.AST.QueryStmt.collation_name getCollation_name() {
        return collation_name;
    }

    public tree.AST.QueryStmt.column_constraint_foreign_key getColumn_constraint_foreign_key() {
        return column_constraint_foreign_key;
    }

    public tree.AST.QueryStmt.column_constraint_not_null getColumn_constraint_not_null() {
        return column_constraint_not_null;
    }

    public tree.AST.QueryStmt.column_constraint_null getColumn_constraint_null() {
        return column_constraint_null;
    }

    public tree.AST.QueryStmt.column_constraint_primary_key getColumn_constraint_primary_key() {
        return column_constraint_primary_key;
    }

    public column_default getColumn_defaults() {
        return column_defaults;
    }
    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);
        if(this.getName()!=null)
        {
            this.nam.accept(astVisitor);
        }
        if(this.expr!=null)
        {
            this.expr.accept(astVisitor);
        }
        if(this.getColumn_defaults()!=null)
        {
            this.column_defaults.accept(astVisitor);
        }
        if(this.getCollation_name()!=null)
        {
            this.collation_name.accept(astVisitor);
        }
//        if(this.getColumn_constraint_primary_key()!=null)
//        {
//            this.column_constraint_primary_key.accept(astVisitor);
//        }
        if(this.getColumn_constraint_foreign_key()!=null)
        {
            this.column_constraint_foreign_key.accept(astVisitor);
        }
//        if(this.getColumn_constraint_not_null()!=null)
//        {
//            this.column_constraint_not_null.accept(astVisitor);
//        }
//        if(this.getColumn_constraint_null()!=null)
//        {
//            this.column_constraint_null.accept(astVisitor);
//        }
    }
}




