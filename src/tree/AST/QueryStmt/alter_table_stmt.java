package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class alter_table_stmt extends Node {
    db_name databaseName ;
    Source_table_name source_table_name;
    New_table_name new_table_name;
    alter_table_add alter_table_add;
    alter_table_add_constraint alter_table_add_constraint;
    tree.AST.QueryStmt.column_def column_def;

    public void setColumn_def(tree.AST.QueryStmt.column_def column_def) {
        this.column_def = column_def;
    }
    public void setDatabaseName(db_name databaseName) {
        this.databaseName = databaseName;
    }
    public void setAlter_table_add(tree.AST.QueryStmt.alter_table_add alter_table_add) {
        this.alter_table_add = alter_table_add;
    }
    public void setAlter_table_add_constraint(tree.AST.QueryStmt.alter_table_add_constraint alter_table_add_constraint) {
        this.alter_table_add_constraint = alter_table_add_constraint;
    }
    public void setNew_table_name(New_table_name new_table_name) {
        this.new_table_name = new_table_name;
    }
    public void setSource_table_name(Source_table_name source_table_name) {
        this.source_table_name = source_table_name;
    }
    public tree.AST.QueryStmt.column_def getColumn_def() {
        return column_def;
    }
    public db_name getDatabaseName() {
        return databaseName;
    }
    public tree.AST.QueryStmt.alter_table_add getAlter_table_add() {
        return alter_table_add;
    }
    public tree.AST.QueryStmt.alter_table_add_constraint getAlter_table_add_constraint() {
        return alter_table_add_constraint;
    }
    public New_table_name getNew_table_name() {
        return new_table_name;
    }
    public Source_table_name getSource_table_name() {
        return source_table_name;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
         if(this.getDatabaseName()!=null)
         {
             this.databaseName.accept(astVisitor);
         }
         if(this.getSource_table_name()!=null)
         {
             this.source_table_name.accept(astVisitor);
         }
         if(this.getNew_table_name()!=null)
         {
             this.new_table_name.accept(astVisitor);
         }
         if(this.getAlter_table_add()!=null)
         {
             this.alter_table_add.accept(astVisitor);
         }
         if(this.getAlter_table_add_constraint()!=null)
         {
             this.alter_table_add_constraint.accept(astVisitor);
         }
         if(this.getColumn_def()!=null)
         {
             this.column_def.accept(astVisitor);
         }
    }

}
