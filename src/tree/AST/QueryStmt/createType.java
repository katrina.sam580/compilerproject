package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class createType extends Node {
    tree.AST.QueryStmt.create_type_name create_type_name;

    List<columnDefType> column_def_type=new ArrayList<>();

    public tree.AST.QueryStmt.create_type_name getCreate_type_name() {
        return create_type_name;
    }

    public List<columnDefType> getColumn_def_type() {
        return column_def_type;
    }

    public void setColumn_def_type(List<columnDefType> column_def_type) {
        this.column_def_type = column_def_type;
    }

    public void setCreate_type_name(tree.AST.QueryStmt.create_type_name create_type_name) {
        this.create_type_name = create_type_name;
    }




    @Override
    public void accept(ASTVisitor astVisitor) {

        astVisitor.visit(this);

        if(this.create_type_name !=null){
            this.create_type_name.accept(astVisitor);

        }

        if(this.column_def_type.size() > 0){
            for(int i=0;i<column_def_type.size();i++){
                this.column_def_type.get(i).accept(astVisitor);
            }
        }

    }
}
