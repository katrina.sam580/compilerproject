package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class CreateTable  extends Node {

   tree.AST.QueryStmt.tb_name tb_name;
   tree.AST.QueryStmt.db_name db_name;
   List<column_def>column_defs=new ArrayList<>();
   List<table_constraint>table_constraints=new ArrayList<>();
   SelectStmt selectStmt;
   String type_create_table;
   String path_create_type;

    public void setType_create_table(String type_create_table) {
        this.type_create_table = type_create_table;
    }

    public String getType_create_table() {
        return type_create_table;
    }

    public void setPath_create_type(String path_create_type) {
        this.path_create_type = path_create_type;
    }

    public String getPath_create_type() {
        return path_create_type;
    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }

    public void setTb_name(tree.AST.QueryStmt.tb_name tb_name) {
        this.tb_name = tb_name;
    }

    public void setDb_name(tree.AST.QueryStmt.db_name db_name) {
        this.db_name = db_name;
    }

    public tree.AST.QueryStmt.tb_name getTb_name() {
        return tb_name;
    }

    public tree.AST.QueryStmt.db_name getDb_name() {
        return db_name;
    }

    public void setColumn_defs(List<column_def> column_defs) {
        this.column_defs = column_defs;
    }

    public List<column_def> getColumn_defs() {
        return column_defs;
    }

    public void setTable_constraints(List<table_constraint> table_constraints) {
        this.table_constraints = table_constraints;
    }

    public List<table_constraint> getTable_constraints() {
        return table_constraints;
    }


    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.db_name!=null)
        {
            this.db_name.accept(astVisitor);
        }
        if(this.tb_name!=null)
        {
            this.tb_name.accept(astVisitor);
        }
        if(column_defs.size()>0)
        {
            for(int i=0;i<column_defs.size();i++)
            {
                this.column_defs.get(i).accept(astVisitor);
            }
        }
        if(this.type_create_table!=null)
        {
            System.out.println("Type of Table : "+this.type_create_table);
        }
        if(this.path_create_type!=null)
        {
            System.out.println("Path of File : "+this.path_create_type);
        }
        if(table_constraints.size()>0)
        {
            for(int i=0;i<table_constraints.size();i++)
            {
                this.table_constraints.get(i).accept(astVisitor);
            }
        }
    }
}
