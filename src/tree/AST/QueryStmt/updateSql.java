package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

public class updateSql extends Node {

    qualifiedTableName qualified_table_name;

    UpdateFirstParameter update_f_parameter ;
    UpdateSecondParameter update_s_parameter ;


    Expr exp;



    public void setQualified_table_name(qualifiedTableName qualified_table_name) {
        this.qualified_table_name = qualified_table_name;
    }

    public qualifiedTableName getQualified_table_name() {
        return qualified_table_name;
    }


    public void setUpdate_f_parameter(UpdateFirstParameter update_f_parameter) {
        this.update_f_parameter = update_f_parameter;
    }

    public void setUpdate_s_parameter(UpdateSecondParameter update_s_parameter) {
        this.update_s_parameter = update_s_parameter;
    }

    public UpdateFirstParameter getUpdate_f_parameter() {
        return update_f_parameter;
    }

    public UpdateSecondParameter getUpdate_s_parameter() {
        return update_s_parameter;
    }

    public void setExp(Expr exp) {
        this.exp = exp;
    }

    public Expr getExp() {
        return exp;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.qualified_table_name != null){
            this.qualified_table_name.accept(astVisitor);
        }

        if(this.update_f_parameter != null){
            this.update_f_parameter.accept(astVisitor);
        }


        if(this.update_s_parameter != null){
            this.update_s_parameter.accept(astVisitor);
        }

        if(this.exp != null){
            this.exp.accept(astVisitor);
        }



    }

}
