package tree.AST.QueryStmt;

import tree.AST.javaStmt.DeclareVar;
import tree.AST.javaStmt.Expr;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

//addedkatren
public class SelectStmt extends SQL_Statement {    //factored_select_stmt

    //    private Distinct distinct;
//    private List<SelectItem> selectItems;
//    private List<Table> intoTables;

    private List<ResultColumn> columnItem=new ArrayList<>();
    private Expr whereItem;
    List<Expr>Values_exprs=new ArrayList<>();
    List<Order_Tearm>order_tearms=new ArrayList<>();
    Expr Limits;
    Expr offset;
    List<Expr>groupby=new ArrayList<>();
    Expr Having_expr;
    List<table_or_subquery>table_or_subqueries=new ArrayList<>();
    joinclaus joinclaus;

    //    private List<Join> joins;
//    private Expression where;
//    private GroupByElement groupBy;
//    private List<OrderByElement> orderByElements;
//    private Expression having;
//    private Limit limit;
//    private Offset offset;




    public void setJoinclaus(tree.AST.QueryStmt.joinclaus joinclaus) {
        this.joinclaus = joinclaus;
    }

    public tree.AST.QueryStmt.joinclaus getJoinclaus() {
        return joinclaus;
    }

    public void setHaving_expr(Expr having_expr) {
        Having_expr = having_expr;
    }

    public void setGroupby(List<Expr> groupby) {
        this.groupby = groupby;
    }

    public List<Expr> getGroupby() {
        return groupby;
    }

    public void setOffset(Expr offset) {
        this.offset = offset;
    }

    public Expr getOffset() {
        return offset;
    }

    public void setTable_or_subqueries(List<table_or_subquery> table_or_subqueries) {
        this.table_or_subqueries = table_or_subqueries;
    }

    public List<table_or_subquery> getTable_or_subqueries() {
        return table_or_subqueries;
    }

    public void setLimits(Expr limits) {
        Limits = limits;
    }

    public Expr getLimits() {
        return Limits;
    }

    public void setOrder_tearms(List<Order_Tearm> order_tearms) {
        this.order_tearms = order_tearms;
    }

    public List<Order_Tearm> getOrder_tearms() {
        return order_tearms;
    }

    public SelectStmt()
    {
        columnItem=new ArrayList<>();
    }



    public void setWhereItem(Expr whereItem) {
        this.whereItem = whereItem;
    }

    public void setColumnItem(List<ResultColumn> columnItem) {
        this.columnItem = columnItem;
    }




    public Expr getWhereItem() {
        return whereItem;
    }

    public List<ResultColumn> getColumnItem() {
        return columnItem;
    }

    public void setValues_exprs(List<Expr> values_exprs) {
        Values_exprs = values_exprs;
    }

    public List<Expr> getValues_exprs() {
        return Values_exprs;
    }



    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);


        //System.out.println("FromItem : "+this.getFromItem());
        if(this.getColumnItem().size()>0) {
            for (int i = 0; i < this.getColumnItem().size(); i++) {

                this.columnItem.get(i).accept(astVisitor);
            }
        }

        if(this.table_or_subqueries.size()>0)
        {
            for(int i=0;i<table_or_subqueries.size();i++)
            {
                this.table_or_subqueries.get(i).accept(astVisitor);
            }
        }

        if(this.joinclaus!=null)
        {
            this.joinclaus.accept(astVisitor);
        }

        if(this.whereItem!=null)
        {
            System.out.println("where : ");
            this.whereItem.accept(astVisitor);
        }

        if(this.groupby.size()>0)
        {
          System.out.println("group by :");
          for(int i=0;i<this.groupby.size();i++)
          {
              this.groupby.get(i).accept(astVisitor);
          }
        }
        if(this.Having_expr!=null)
        {
            System.out.println("having : ");
            this.Having_expr.accept(astVisitor);
        }
        if(this.order_tearms.size()>0)
        {
            System.out.println("order : ");
            for(int i=0;i<this.order_tearms.size();i++)
            {
                this.order_tearms.get(i).accept(astVisitor);
            }
        }

        if(this.Limits!=null)
        {
            System.out.println("limit : ");
            this.Limits.accept(astVisitor);
        }
        if(this.offset!=null)
        {
            System.out.println("offset : ");
            this.offset.accept(astVisitor);
        }



    }

}
