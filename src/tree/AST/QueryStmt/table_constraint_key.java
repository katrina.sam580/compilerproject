package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class table_constraint_key extends Node {
    Name name ;

    List<indexed_column> indexed_columns= new ArrayList<>();

    public void setIndexed_columns(List<indexed_column> indexed_columns) {
        this.indexed_columns = indexed_columns;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Name getName() {
        return name;
    }

    public List<indexed_column> getIndexed_columns() {
        return indexed_columns;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getName()!=null)
        {
            this.name.accept(astVisitor);
        }
       if(this.getIndexed_columns().size()>0)
       {
           for (int i=0;i<this.indexed_columns.size();i++)
           {
               this.indexed_columns.get(i).accept(astVisitor);
           }

       }
    }
}
