package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class SQL_Statement extends Node {
    private String name;

     //select
    SelectStmt selectStmt;
    //insert
    insertSql insert ;

    //delete
    deleteSql del_sql;


    //update
    updateSql upd ;

    //drop
    dropTable drop_table;

    //alter
    tree.AST.QueryStmt.alter_table_stmt alter_table_stmt;

    //create
    CreateTable createTable;

     //create agg

    create_aggregation_function aggregation_function;

    //create type
    createType createType;

    public void setAggregation_function(create_aggregation_function aggregation_function) {
        this.aggregation_function = aggregation_function;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }

    public create_aggregation_function getAggregation_function() {
        return aggregation_function;
    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    public void setCreateTable(CreateTable createTable) {
        this.createTable = createTable;
    }

    public CreateTable getCreateTable() {
        return createTable;
    }


    public void setAlter_table_stmt(tree.AST.QueryStmt.alter_table_stmt alter_table_stmt) {
        this.alter_table_stmt = alter_table_stmt;
    }

    public tree.AST.QueryStmt.createType getCreateType() {
        return createType;
    }

    public void setCreateType(tree.AST.QueryStmt.createType createType) {
        this.createType = createType;
    }

    public tree.AST.QueryStmt.alter_table_stmt getAlter_table_stmt() {
        return alter_table_stmt;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setInsert(insertSql insert) {
        this.insert = insert;
    }

    public insertSql getInsert() {
        return insert;
    }

    public void setDel_sql(deleteSql del_sql) {
        this.del_sql = del_sql;
    }

    public deleteSql getDel_sql() {
        return del_sql;
    }



    public void setUpd(updateSql upd) {
        this.upd = upd;
    }

    public updateSql getUpd() {
        return upd;
    }

    public void setDrop_table(dropTable drop_table) {
        this.drop_table = drop_table;
    }

    public dropTable getDrop_table() {
        return drop_table;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.aggregation_function!=null){
            this.aggregation_function.accept(astVisitor);
        }
        if(this.insert != null){
            this.insert.accept(astVisitor);
        }

        if(this.upd != null){
            this.upd.accept(astVisitor);
        }

        if(this.del_sql != null){
            this.del_sql.accept(astVisitor);
        }

        if(this.drop_table != null){
            this.drop_table.accept(astVisitor);
        }
        if(this.alter_table_stmt!=null)
        {
            this.alter_table_stmt.accept(astVisitor);
        }
        if(this.createTable!=null)
        {
            this.createTable.accept(astVisitor);
        }
        if(this.selectStmt!=null)
        {
            this.selectStmt.accept(astVisitor);
        }
        if(this.createType!=null){
            this.createType.accept(astVisitor);
        }


    }


}
