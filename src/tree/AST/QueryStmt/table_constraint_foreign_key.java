package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class table_constraint_foreign_key extends Node {
    List<fk_origin_column_name> fk_origin_column_names= new ArrayList<>();
    tree.AST.QueryStmt.foreign_key_clause foreign_key_clause ;

    public void setFk_origin_column_names(List<fk_origin_column_name> fk_origin_column_names) {
        this.fk_origin_column_names = fk_origin_column_names;
    }

    public void setForeign_key_clause(tree.AST.QueryStmt.foreign_key_clause foreign_key_clause) {
        this.foreign_key_clause = foreign_key_clause;
    }

    public tree.AST.QueryStmt.foreign_key_clause getForeign_key_clause() {
        return foreign_key_clause;
    }

    public List<fk_origin_column_name> getFk_origin_column_names() {
        return fk_origin_column_names;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getFk_origin_column_names().size()>0)
        {
            for(int i=0;i<this.fk_origin_column_names.size();i++)
            {
                this.fk_origin_column_names.get(i).accept(astVisitor);
            }
        }
        if(this.getForeign_key_clause()!=null)
        {
            this.foreign_key_clause.accept(astVisitor);
        }

    }
}
