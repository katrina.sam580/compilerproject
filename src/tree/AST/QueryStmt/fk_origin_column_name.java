package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class fk_origin_column_name extends Node {
    tree.AST.QueryStmt.col_name col_name;

    public void setCol_name(tree.AST.QueryStmt.col_name col_name) {
        this.col_name = col_name;
    }

    public tree.AST.QueryStmt.col_name getCol_name() {
        return col_name;
    }


    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getCol_name()!=null)
        {
            this.col_name.accept(astVisitor);
        }


    }
}
