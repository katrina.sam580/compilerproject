package tree.AST.QueryStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class fk_target_column_name extends Node {
   // anyNam nam= new anyNam();
Name name;

    public void setName(Name name) {
        this.name = name;
    }

    public Name getName() {
        return name;
    }


    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.getName()!=null)
        {
            this.name.accept(astVisitor);
        }



    }
}
