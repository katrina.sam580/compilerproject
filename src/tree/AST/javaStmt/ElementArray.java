package tree.AST.javaStmt;

import tree.AST.vis.ASTVisitor;

public class ElementArray extends JavaStatement {

    Expr element;

    public void setElement(Expr element) {
        this.element = element;
    }

    public Expr getElement() {
        return element;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if (this.element!=null)
        {
            this.element.accept(astVisitor);
        }
    }
}
