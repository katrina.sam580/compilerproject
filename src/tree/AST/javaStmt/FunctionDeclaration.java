package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;


public class FunctionDeclaration extends Node {

    BodyFunction body ;
    HeaderFunction header ;
    FunctionName funcname ;
    Scope scope_function;

    public void setScope_function(Scope scope_function) {
        this.scope_function = scope_function;
    }

    public Scope getScope_function() {
        return scope_function;
    }

    public void setBody(BodyFunction body) {
        this.body = body;
    }

    public void setHeader(HeaderFunction header) {
        this.header = header;
    }
    public void setFuncname(FunctionName funcname) {
        this.funcname = funcname;
    }

    public BodyFunction getBody() {
        return body;
    }

    public HeaderFunction getHeader() {
        return header;
    }
    public FunctionName getFuncname() {
        return funcname;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        this.funcname.accept(astVisitor);
        this.header.accept(astVisitor);
        this.body.accept(astVisitor);
    }

}
