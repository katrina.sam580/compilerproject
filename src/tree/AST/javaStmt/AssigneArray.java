package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class AssigneArray extends Node {

    String arr_name;
    List<ElementArray> elementlist=new ArrayList<>();

    public void setArr(String arr) {
        this.arr_name = arr;
    }

    public void setElementlist(List<ElementArray> elementlist) {
        this.elementlist = elementlist;
    }

    public String getArr() {
        return arr_name;
    }

    public List<ElementArray> getElementlist() {
        return elementlist;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        System.out.println("arr_name : "+this.arr_name);

        if(this.elementlist.size()>0)
        {
            for (int i=0;i<this.elementlist.size();i++)
            {
                this.elementlist.get(i).accept(astVisitor);
            }
        }
    }
}
