package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

//addedkatren
public class PropertyJson extends Node {

    String propertyName;
    JsonBody jsonBody ;
    ArrayJson arrayJson;
    Expr expr;

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public void setJsonBody(JsonBody jsonBody) {
        this.jsonBody = jsonBody;
    }

    public void setArrayJson(ArrayJson arrayJson) {
        this.arrayJson = arrayJson;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public JsonBody getJsonBody() {
        return jsonBody;
    }

    public ArrayJson getArrayJson() {
        return arrayJson;
    }

    public Expr getExpr() {
        return expr;
    }

    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.propertyName!=null)
        {
            System.out.println("propertyName : "+this.propertyName);

        }
        if(this.jsonBody!=null)
        {
            this.jsonBody.accept(astVisitor);
        }
        if(this.arrayJson!=null)
        {
            this.arrayJson.accept(astVisitor);
        }
        if(this.expr!=null)
        {
            this.expr.accept(astVisitor);
        }

    }
}
