package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class Argument_list extends Node {
    List<Expr> expr= new ArrayList<>();
    heigher_order_function heigher;

    public void setExpr(List<Expr> expr) {
        this.expr = expr;
    }

    public void setHeigher(heigher_order_function heigher) {
        this.heigher = heigher;
    }

    public List<Expr> getExpr() {
        return expr;
    }

    public heigher_order_function getHeigher() {
        return heigher;
    }

    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        if(expr.size()>0)
        {
            for(int i=0;i<expr.size();i++)
            {
                this.expr.get(i).accept(astVisitor);
            }
        }
        if(this.heigher!=null)
        {
            this.heigher.accept(astVisitor);
        }

    }
}
