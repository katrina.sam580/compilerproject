package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;


public class Variable extends Node {

    String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
    }
}
