package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.QueryStmt.*;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

import java.util.ArrayList;
import java.util.List;

public class Expr extends Node {
    String valueType;

    String value;
    Expr exprwithbrackets;
    UnaryExpr unaryExpr;
    List<DeclareArray> declareArray=new ArrayList<>();
    GetPropertyJson getPropertyJsons;
    incrDecrWithoutComma incrDecr;
    List<Call_func> call_funcs =new ArrayList<>();
    tb_name tablename;
    db_name databasename;
    Bollean bool_expr;
    SelectStmt selectStmt;
    Symbol symbol ;
    Call_func call_func;
    MathimaticalExpr mathimaticalExpr;
    LogicalExpr logicalExpr;
    Binary_Expr binary_expr;
    col_name col_name;
    expr_sql expr_sql;

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }

    public db_name getDatabasename() {
        return databasename;
    }

    public tb_name getTablename() {
        return tablename;
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    @Override
    public int getCol() {
        return super.getCol();
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public String getValueType() {
        return valueType;
    }

    public void setExpr_sql(tree.AST.QueryStmt.expr_sql expr_sql) {
        this.expr_sql = expr_sql;
    }

    public tree.AST.QueryStmt.expr_sql getExpr_sql() {
        return expr_sql;
    }

    public UnaryExpr getUnaryExpr() {
        return unaryExpr;
    }

    public void setCol_name(tree.AST.QueryStmt.col_name col_name) {
        this.col_name = col_name;
    }

    public Bollean getBool_expr() {
        return bool_expr;
    }

    public tree.AST.QueryStmt.col_name getCol_name() {
        return col_name;
    }

    public void setBinary_expr(Binary_Expr binary_expr) {
        this.binary_expr = binary_expr;
    }

    public Binary_Expr getBinary_expr() {
        return binary_expr;
    }

    public void setMathimaticalExpr(MathimaticalExpr mathimaticalExpr) {
        this.mathimaticalExpr = mathimaticalExpr;
    }

    public MathimaticalExpr getMathimaticalExpr() {
        return mathimaticalExpr;
    }

    public void setLogicalExpr(LogicalExpr logicalExpr) {
        this.logicalExpr = logicalExpr;
    }

    public LogicalExpr getLogicalExpr() {
        return logicalExpr;
    }

    public void setCall_func(Call_func call_func) {
        this.call_func = call_func;
    }

    public Call_func getCall_func() {
        return call_func;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }
    //Function_name function_name;


//    public void setFunction_name(Function_name function_name) {
//        this.function_name = function_name;
//    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

//    public Function_name getFunction_name() {
//        return function_name;
//    }

    public void setTablename(tb_name tablename) {
        this.tablename = tablename;
    }

    public void setDatabasename(db_name databasename) {
        this.databasename = databasename;
    }

    public void setBool_expr(Bollean bool_expr) {
        this.bool_expr = bool_expr;
    }

    public Expr getExprwithbrackets() {
        return exprwithbrackets;
    }

    public void setUnaryExpr(UnaryExpr unaryExpr) {
        this.unaryExpr = unaryExpr;
    }

    public void setExprwithbrackets(Expr exprwithbrackets) {
        this.exprwithbrackets = exprwithbrackets;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public void setGetPropertyJsons(GetPropertyJson getPropertyJsons) {
        this.getPropertyJsons = getPropertyJsons;
    }

    public void setCall_funcs(List<Call_func> call_funcs) {
        this.call_funcs = call_funcs;
    }

    public void setIncrDecr(incrDecrWithoutComma incrDecr) {
        this.incrDecr = incrDecr;
    }

    public void setDeclareArray(List<DeclareArray> declareArray) {
        this.declareArray = declareArray;
    }

    public String getValue() {
        return value;
    }

    public List<DeclareArray> getDeclareArray() {
        return declareArray;
    }

    public GetPropertyJson getGetPropertyJsons() {
        return getPropertyJsons;
    }

    public incrDecrWithoutComma getIncrDecr() {
        return incrDecr;
    }

    public List<Call_func> getCall_funcs() {
        return call_funcs;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        if(this.value!=null) {
            System.out.println("value " + this.value);
        }
        if(this.declareArray.size()>0)
        {
            for(int i=0;i<this.declareArray.size();i++)
            {
                this.declareArray.get(i).accept(astVisitor);
            }
        }
        if(this.getPropertyJsons!=null)
        {
            this.getPropertyJsons.accept(astVisitor);
        }
        if(this.incrDecr!=null)
        {
            this.incrDecr.accept(astVisitor);
        }
        if(this.call_funcs.size()>0)
        {
            // System.out.println("44444444444444444444444444444");
            for(int i=0;i<this.call_funcs.size();i++)
            {
                this.call_funcs.get(i).accept(astVisitor);
            }
        }

        if(this.exprwithbrackets!=null)
        {
            this.exprwithbrackets.accept(astVisitor);
        }

        if(this.unaryExpr!=null)
        {
            this.unaryExpr.accept(astVisitor);
        }
        if(this.bool_expr!= null)
        {
            this.bool_expr.accept(astVisitor);
        }

        if(this.databasename!=null)
        {
            this.databasename.accept(astVisitor);
        }

        if(this.tablename!=null)
        {
            this.tablename.accept(astVisitor);
        }
        if(this.selectStmt!=null)
        {
            this.selectStmt.accept(astVisitor);
        }
//        if(this.function_name!=null)
//        {
//           // System.out.println("function_name : "+this.function_name);
//            this.function_name.accept(astVisitor);
//        }

    }
}
