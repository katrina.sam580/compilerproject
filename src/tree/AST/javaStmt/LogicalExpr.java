package tree.AST.javaStmt;

import tree.AST.vis.ASTVisitor;

public class LogicalExpr extends Expr {

    Expr left_expr;
    Expr right_expr;
    public enum OP_logic
    {
        GT,
        LT,
        GT_EQ,
        LT_EQ,
        EQ,
        NOT_EQ1,
        NOT_EQ2,
        K_AND,
        K_OR,
        PIPE2,
        AMP2

    }
    OP_logic logic;


    public void setLeft_expr(Expr left_expr) {
        this.left_expr = left_expr;
    }

    public void setlogic(OP_logic op_logic) {
        this.logic = op_logic;
    }

    public void setRight_expr(Expr right_expr) {
        this.right_expr = right_expr;
    }

    public Expr getLeft_expr() {
        return left_expr;
    }

    public Expr getRight_expr() {
        return right_expr;
    }

    public OP_logic getlogic() {
        return logic;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.left_expr!=null) {
            left_expr.accept(astVisitor);
        }
        System.out.println("Logic OP : "+this.getlogic());
        if(this.right_expr!=null) {
            right_expr.accept(astVisitor);
        }


    }
}
