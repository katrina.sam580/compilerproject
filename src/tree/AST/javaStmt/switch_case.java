package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

public class switch_case extends Node {
    Header_switch header_switch;
    Body_switch body_switch ;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setBody_switch(Body_switch body_switch) {
        this.body_switch = body_switch;
    }

    public void setHeader_switch(Header_switch header_switch) {
        this.header_switch = header_switch;
    }

    public Body_switch getBody_switch() {
        return body_switch;
    }

    public Header_switch getHeader_switch() {
        return header_switch;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        header_switch.accept(astVisitor);
        body_switch.accept(astVisitor);
    }
}
