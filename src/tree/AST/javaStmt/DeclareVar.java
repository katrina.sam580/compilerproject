package tree.AST.javaStmt;

import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

//addedkatren
public class DeclareVar extends Variable {

    String Varname;
    Symbol symbol;

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }
    public void setVarname(String varname) {
        Varname = varname;
    }

    public String getVarname() {
        return Varname;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        System.out.println("Varname : "+Varname);
    }

}
