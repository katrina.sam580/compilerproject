package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

import java.util.ArrayList;
import java.util.List;

public class minVar extends Node {


    String identif;
    String num_lteral;

    List<DeclareArray> assign_array = new ArrayList<>();

    GetPropertyJson get_prop_json;
    Symbol symbol ;

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setIdentif(String identif) {
        this.identif = identif;
    }

    public void setNum_lteral(String num_lteral) {
        this.num_lteral = num_lteral;
    }

    public void setAssign_array(List<DeclareArray> assign_array) {
        this.assign_array = assign_array;
    }

    public void setGet_prop_json(GetPropertyJson get_prop_json) {
        this.get_prop_json = get_prop_json;
    }


    public String getIdentif() {
        return identif;
    }


    public String getNum_lteral() {
        return num_lteral;
    }


    public List<DeclareArray> getAssign_array() {
        return assign_array;
    }

    public GetPropertyJson getGet_prop_json() {
        return get_prop_json;
    }



    @Override
    public void accept(ASTVisitor astVisitor) {

        astVisitor.visit(this);

        if(this.identif!=null) {
            System.out.println("min_variable : " + this.identif);
        }
        if(this.num_lteral!=null)
        {
            System.out.println("min_variable : " + this.num_lteral);
        }
        if (this.assign_array.size() > 0) {
            for (int i = 0; i < this.assign_array.size(); i++) {
                this.assign_array.get(i).accept(astVisitor);
            }
        }
        if (this.getGet_prop_json() !=null) {
            this.getGet_prop_json().accept(astVisitor);
        }

    }
}
