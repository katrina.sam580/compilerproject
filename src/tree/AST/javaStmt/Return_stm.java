package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class Return_stm extends Node {
    tree.AST.javaStmt.ifLine ifLine;
    Bollean bool_exor;
    Expr expr;

    public void setIfLine(tree.AST.javaStmt.ifLine ifLine) {
        this.ifLine = ifLine;
    }

    public tree.AST.javaStmt.ifLine getIfLine() {
        return ifLine;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public void setBool_exor(Bollean bool_exor) {
        this.bool_exor = bool_exor;
    }

    public Expr getExpr() {
        return expr;
    }

    public Bollean getBool_exor() {
        return bool_exor;
    }


    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        if(this.getExpr()!=null)
        {
            this.expr.accept(astVisitor);
        }

        if(this.getBool_exor()!=null)
        {
            this.bool_exor.accept(astVisitor);
        }

    }

}
