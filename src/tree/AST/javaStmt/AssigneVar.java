package tree.AST.javaStmt;

import tree.AST.QueryStmt.AssigneSelect;
import tree.AST.QueryStmt.SelectStmt;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

//addedkatren
public class AssigneVar extends Variable {

    String var;
    String value_var;
    Expr exp;
    Bollean boll_expr ;
    Symbol symbol ;

    AssigneSelect assigneSelect ;

    public void setAssigneSelect(AssigneSelect assigneSelect) {
        this.assigneSelect = assigneSelect;
    }

    public AssigneSelect getAssigneSelect() {
        return assigneSelect;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setBoll_expr(Bollean boll_expr) {
        this.boll_expr = boll_expr;
    }

    public Bollean getBoll_expr() {
        return boll_expr;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public void setExp(Expr exp) {
        this.exp = exp;
    }

    public void setValue_var(String value_var) {
        this.value_var = value_var;
    }

    public String getVar() {
        return var;
    }

    public Expr getExp() {
        return exp;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        System.out.println("Varname : "+var);
        if(this.exp!=null)
        {
            this.exp.accept(astVisitor);
        }
        if(this.value_var!=null)
        {
            System.out.println("value_var : "+this.value_var);
        }
        if(this.boll_expr!=null){
            this.boll_expr.accept(astVisitor);
         //   System.out.println("boll_expr : "+this.boll_expr);
        }
        if(this.assigneSelect!=null)
        {
            this.assigneSelect.accept(astVisitor);
        }
    }
}
