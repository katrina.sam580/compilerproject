package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class expFor extends Node {

    String Identifier ;


    enum OP
    {
        ASSIGN,
        PLUS_EQ,
        MINUS_EQ,
        STAR_EQ,
        DIV_EQ
    }

    String OP;

    incrDecrWithoutComma inc_dec ;

    String literal_value ;

    exp_i expI ;

    public void setIdentifier(String identifier) {
        Identifier = identifier;
    }

    public String getIdentifier() {
        return Identifier;
    }

    public void setInc_dec(incrDecrWithoutComma inc_dec) {
        this.inc_dec = inc_dec;
    }

    public incrDecrWithoutComma getInc_dec() {
        return inc_dec;
    }

    public void setLiteral_value(String literal_value) {
        this.literal_value = literal_value;
    }

    public String getLiteral_value() {
        return literal_value;
    }

    public void setExpI(exp_i expI) {
        this.expI = expI;
    }

    public exp_i getExpI() {
        return expI;
    }

    public void setOP(String OP) {
        this.OP = OP;
    }

    public String getOP() {
        return OP;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.Identifier != null){
            System.out.println("Identifier : "+this.Identifier);
        }

        System.out.println(" OP : "+this.getOP());

        if(this.inc_dec != null){
            this.inc_dec.accept(astVisitor);
        }


        if(this.literal_value != null){
            System.out.println("Literal Value : "+this.literal_value);
        }


        if(this.expI != null){
            this.expI.accept(astVisitor);
        }

    }

}
