package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class print extends Node {
    Expr expr;
    GetPropertyJson getPropertyJson;


    public void setGetPropertyJson(GetPropertyJson getPropertyJson) {
        this.getPropertyJson = getPropertyJson;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public Expr getExpr() {
        return expr;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        if(this.getExpr()!=null)
        {
            this.expr.accept(astVisitor);
        }
        if(this.getPropertyJson!=null)
        {
            this.getPropertyJson.accept(astVisitor);
        }

    }

}
