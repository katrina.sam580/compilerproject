package tree.AST.javaStmt;


public class headerFor {
    /*  List<Variable> parameterFor = new ArrayList<>();


      public void setParameterFor(List<Variable> parameterFor) {
          this.parameterFor = parameterFor;
      }

      public List<Variable> getParameterFor() {
          return parameterFor;
      }
  */
    DeclareForVar for_var ;
    ForCondition for_con ;
    forProcessVar for_process ;

    public void setFor_var(DeclareForVar for_var) {
        this.for_var = for_var;
    }

    public void setFor_con(ForCondition for_con) {
        this.for_con = for_con;
    }

    public DeclareForVar getFor_var() {
        return for_var;
    }

    public ForCondition getFor_con() {
        return for_con;
    }

    public void setFor_process(forProcessVar for_process) {
        this.for_process = for_process;
    }

    public forProcessVar getFor_process() {
        return for_process;
    }
}
