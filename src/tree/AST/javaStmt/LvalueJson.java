package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

//addedkatren
public class LvalueJson extends Node {

    String value;
    ArrayJson arrayJson;

    public void setArrayJson(ArrayJson arrayJson) {
        this.arrayJson = arrayJson;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ArrayJson getArrayJson() {
        return arrayJson;
    }

    public String getValue() {
        return value;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {

        astVisitor.visit(this);
        if(this.value!=null) {
            System.out.println("valueJson : " + this.value);
        }
        if(this.arrayJson!=null)
        {
            this.arrayJson.accept(astVisitor);
        }

    }

}
