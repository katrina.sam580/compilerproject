package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

public class Default extends Node {
    BodyFunction bodyFunction;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setBodyFunction(BodyFunction bodyFunction) {
        this.bodyFunction = bodyFunction;
    }
    public BodyFunction getBodyFunction() {
        return bodyFunction;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);


        if(this.bodyFunction!=null)
        {
            bodyFunction.accept(astVisitor);
        }
    }


}
