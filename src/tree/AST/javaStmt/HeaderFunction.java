package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

import java.util.ArrayList;
import java.util.List;

public class HeaderFunction extends Node {


    List<Variable> parameters = new ArrayList<>();
    List<Symbol>symbols=new ArrayList<>();

    public void setSymbols(List<Symbol> symbols) {
        this.symbols = symbols;
    }

    public List<Symbol> getSymbols() {
        return symbols;
    }

    public void setParameters(List<Variable> parameters) {
        this.parameters = parameters;
    }


    public List<Variable> getParameters() {
        return parameters;
    }
@Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        if(parameters.size()>0)
        {
            for(int i=0;i<parameters.size();i++)
            {
                this.parameters.get(i).accept(astVisitor);
            }
        }

    }

}
