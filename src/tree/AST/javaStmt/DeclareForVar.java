package tree.AST.javaStmt;

import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

public class DeclareForVar extends Variable {
    String ForVarname;
    Expr valu;
    Symbol symbol ;

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }
    public void setForVarname(String forVarname) {
        ForVarname = forVarname;
    }

    public String getForVarname() {
        return ForVarname;
    }

    public void setValu(Expr valu) {
        this.valu = valu;
    }

    public Expr getValu() {
        return valu;
    }
@Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.ForVarname!=null) {
            System.out.println("ForVarname " + this.ForVarname);
        }

        this.valu.accept(astVisitor);

    }
}
