package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

//addedkatren
public class ForEach extends Node {

    ForEachHeader forEachHeader = new ForEachHeader();
    BodyFunction body ;
    BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setBodyFunctionWilthoutBracket(BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket) {
        this.bodyFunctionWilthoutBracket = bodyFunctionWilthoutBracket;
    }

    public BodyFunctionWilthoutBracket getBodyFunctionWilthoutBracket() {
        return bodyFunctionWilthoutBracket;
    }

    public void setForEachHeader(ForEachHeader forEachHeader) {
        this.forEachHeader = forEachHeader;
    }


    public ForEachHeader getForEachHeader() {
        return forEachHeader;
    }

    public void setBody(BodyFunction body) {
        this.body = body;
    }



    public BodyFunction getBody() {
        return body;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        this.forEachHeader.accept(astVisitor);
        if(this.body!=null)
        {
            this.body.accept(astVisitor);
        }
        if(this.bodyFunctionWilthoutBracket!=null)
        {
            this.bodyFunctionWilthoutBracket.accept(astVisitor);
        }

    }
}
