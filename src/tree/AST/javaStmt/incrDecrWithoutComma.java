package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

import java.util.ArrayList;
import java.util.List;

public class incrDecrWithoutComma extends Node {

    String value;
    List<DeclareArray> declareArray=new ArrayList<>();
    GetPropertyJson getPropertyJson;
    Symbol symbol;
    //PLUS_PLUS : '++';
//MINUS_MINUS : '--';
    public enum OP_logic
    {
        PLUS_PLUSl,
        PLUS_PLUSr,
        MINUS_MINUSl,
        MINUS_MINUSr
    }
    OP_logic logic;

    public void setLogic(OP_logic logic) {
        this.logic = logic;
    }

    public OP_logic getLogic() {
        return logic;
    }

    @Override
    public int getLine() {
        return super.getLine();
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public void setDeclareArray(List<DeclareArray> declareArray) {
        this.declareArray = declareArray;
    }
    public void setGetPropertyJson(GetPropertyJson getPropertyJson) {
        this.getPropertyJson = getPropertyJson;
    }
    public String getValue() {
        return value;
    }
    public GetPropertyJson getGetPropertyJson() {
        return getPropertyJson;
    }
    public List<DeclareArray> getDeclareArray() {
        return declareArray;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.value != null){
            System.out.println("value :" + this.value);
        }

        if(declareArray.size()>0)
        {
            for(int i=0;i<declareArray.size();i++)
            {
                this.declareArray.get(i).accept(astVisitor);
            }
        }

        if(this.getPropertyJson!=null) {
            this.getPropertyJson.accept(astVisitor);
        }
    }
}