package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

public class elseIf extends Node {
    condition_header else_if_header ;

    BodyFunction body ;
    BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setElse_if_header(condition_header else_if_header) {
        this.else_if_header = else_if_header;
    }

    public condition_header getElse_if_header() {
        return else_if_header;
    }

    public void setBody(BodyFunction body) {
        this.body = body;
    }

    public BodyFunction getBody() {
        return body;
    }

    public void setBodyFunctionWilthoutBracket(BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket) {
        this.bodyFunctionWilthoutBracket = bodyFunctionWilthoutBracket;
    }

    public BodyFunctionWilthoutBracket getBodyFunctionWilthoutBracket() {
        return bodyFunctionWilthoutBracket;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        this.else_if_header.accept(astVisitor);
        if(this.body!=null)
        {
            this.body.accept(astVisitor);
        }


        if(this.bodyFunctionWilthoutBracket!=null)
        {
            this.bodyFunctionWilthoutBracket.accept(astVisitor);
        }
    }
}
