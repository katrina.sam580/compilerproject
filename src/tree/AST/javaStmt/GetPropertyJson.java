package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

//addedkatren
public class GetPropertyJson extends Node {

    List<String> vars=new ArrayList<>();

    public void setVars(List<String> vars) {
        this.vars = vars;
    }

    public List<String> getVars() {
        return vars;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        for(int i=0;i<this.vars.size();i++)
        {
            System.out.println("var_"+i+": "+vars.get(i));
        }
    }
}
