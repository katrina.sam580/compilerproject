package tree.AST.javaStmt;


import tree.AST.QueryStmt.AssigneSelect;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class BodyFunctionWilthoutBracket  extends JavaStatement {
    List<Expr> expr = new ArrayList<>();
    List<ForEach> foreach_stmt = new ArrayList<>();
    List<DeclareArray> declareArrays=new ArrayList<>();
    List<DeclareVar>declareVars=new ArrayList<>();
    List<AssigneVar>assigneVars=new ArrayList<>();
    List<AssigneArray>assigneArrays=new ArrayList<>();
    List<JsonObjectStmt>jsonObjectStmts = new ArrayList<>();

    List<forDecleration> for_stmt= new ArrayList<>();
    List<IfDecleration> if_stmt= new ArrayList<>();
    List<print> printt= new ArrayList<>();
    List<ifLine> if_line= new ArrayList<>();
    List<While> whiles =new ArrayList<>();
    List<Do_While> do_whiles = new ArrayList<>();
    List<Call_func> call_funcs =new ArrayList<>();
    List<switch_case> switch_cases= new ArrayList<>();
    List<Block> blocks = new ArrayList<>();
    List<AssigneSelect>assigneSelects=new ArrayList<>();
    Return_stm return_stm ;

    List<plusVar> plus_var = new ArrayList<>();
    List<minVar> min_var = new ArrayList<>();


    public void setMin_var(List<minVar> min_var) {
        this.min_var = min_var;
    }

    public void setPlus_var(List<plusVar> plus_var) {
        this.plus_var = plus_var;
    }

    public void setAssigneSelects(List<AssigneSelect> assigneSelects) {
        this.assigneSelects = assigneSelects;
    }

    public void setJsonObjectStmts(List<JsonObjectStmt> jsonObjectStmts) {
        this.jsonObjectStmts = jsonObjectStmts;
    }

    public void setFor_stmt(List<forDecleration> for_stmt) {
        this.for_stmt = for_stmt;
    }

    public void setForeach_stmt(List<ForEach> foreach_stmt) {
        this.foreach_stmt = foreach_stmt;
    }

    public void setDeclareVars(List<DeclareVar> declareVars) {
        this.declareVars = declareVars;
    }

    public void setAssigneArrays(List<AssigneArray> assigneArrays) {
        this.assigneArrays = assigneArrays;
    }

    public void setAssigneVars(List<AssigneVar> assigneVars) {
        this.assigneVars = assigneVars;
    }

    public void setDeclareArrays(List<DeclareArray> declareArrays) {
        this.declareArrays = declareArrays;
    }

    public void setIf_stmt(List<IfDecleration> if_stmt) {
        this.if_stmt = if_stmt;
    }

    public void setIf_line(List<ifLine> if_line) {
        this.if_line = if_line;
    }

    public void setPrintt(List<print> printt) {
        this.printt = printt;
    }

    public List<JsonObjectStmt> getJsonObjectStmts() {
        return jsonObjectStmts;
    }

    public List<forDecleration> getFor_stmt() {
        return for_stmt;
    }

    public List<DeclareVar> getDeclareVars() {
        return declareVars;
    }

    public List<AssigneArray> getAssigneArrays() {
        return assigneArrays;
    }

    public List<AssigneVar> getAssigneVars() {
        return assigneVars;
    }

    public List<ForEach> getForeach_stmt() {
        return foreach_stmt;
    }

    public List<DeclareArray> getDeclareArrays() {
        return declareArrays;
    }

    public List<IfDecleration> getIf_stmt() {
        return if_stmt;
    }

    public List<print> getPrintt() {
        return printt;
    }

    public List<ifLine> getIf_line() {
        return if_line;
    }



    public void setWhiles(List<While> whiles) {
        this.whiles = whiles;
    }

    public void setExpr(List<Expr> expr) {
        this.expr = expr;
    }

    public void setDo_whiles(List<Do_While> do_whiles) {
        this.do_whiles = do_whiles;
    }

    public void setCall_funcs(List<Call_func> call_funcs) {
        this.call_funcs = call_funcs;
    }

    public void setSwitch_cases(List<switch_case> switch_cases) {
        this.switch_cases = switch_cases;
    }

    public void setReturn_stm(Return_stm return_stm) {
        this.return_stm = return_stm;
    }


    public void setBlocks(List<Block> blocks) {
        this.blocks = blocks;
    }

    public  List<Expr> getExpr() {
        return expr;
    }

    public List<While> getWhiles() {
        return whiles;
    }

    public List<Do_While> getDo_whiles() {
        return do_whiles;
    }

    public List<Call_func> getCall_funcs() {
        return call_funcs;
    }

    public List<switch_case> getSwitch_cases() {
        return switch_cases;
    }


    public List<Block> getBlocks() {
        return blocks;
    }

    public Return_stm getReturn_stm() {
        return return_stm;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);
        if(this.getExpr().size()>0)
        {
            for(int i=0;i<this.expr.size();i++)
            {
                this.expr.get(i).accept(astVisitor);
            }
        }

        if(this.getWhiles().size()>0)
        {
            for(int i=0;i<this.whiles.size();i++)
            {
                this.whiles.get(i).accept(astVisitor);
            }
        }
        if(this.getDo_whiles().size()>0)
        {
            for(int i=0;i<this.do_whiles.size();i++)
            {
                this.do_whiles.get(i).accept(astVisitor);
            }
        }
        if(this.getSwitch_cases().size()>0)
        {
            for(int i=0;i<this.switch_cases.size();i++)
            {
                this.switch_cases.get(i).accept(astVisitor);
            }
        }
        if(this.getCall_funcs().size()>0)
        {
            for(int i=0;i<this.call_funcs.size();i++)
            {
                this.call_funcs.get(i).accept(astVisitor);
            }
        }
        if(this.getBlocks().size()>0)
        {
            for(int i=0;i<this.blocks.size();i++)
            {
                this.blocks.get(i).accept(astVisitor);
            }
        }
        if(this.getPrintt().size()>0)
        {
            for(int i=0;i<this.printt.size();i++)
            {
                this.printt.get(i).accept(astVisitor);
            }
        }
        if(this.getReturn_stm()!=null)
        {
            this.return_stm.accept(astVisitor);
        }

        if(foreach_stmt.size()>0)
        {
            for(int i=0;i<foreach_stmt.size();i++)
            {
                this.foreach_stmt.get(i).accept(astVisitor);
            }
        }
        if(declareVars.size()>0)
        {
            for(int i=0;i<declareVars.size();i++) {

                this.declareVars.get(i).accept(astVisitor);
            }
        }
        if(assigneVars.size()>0)
        {
            for(int i=0;i<assigneVars.size();i++)
            {
                this.assigneVars.get(i).accept(astVisitor);
            }
        }
//        if(this.PropertyJsons.size()>0)
//        {
//            for(int i=0;i<this.PropertyJsons.size();i++)
//            {
//                this.PropertyJsons.get(i).accept(astVisitor);
//            }
//        }
        if(this.jsonObjectStmts.size()>0)
        {
            for(int i=0;i<this.jsonObjectStmts.size();i++)
            {
                this.jsonObjectStmts.get(i).accept(astVisitor);
            }
        }
        if(this.assigneArrays.size()>0)
        {
            for(int i=0;i<this.assigneArrays.size();i++)
            {
                this.assigneArrays.get(i).accept(astVisitor);
            }
        }
        if(this.declareArrays.size()>0)
        {
            for(int i=0;i<this.declareArrays.size();i++)
            {
                this.declareArrays.get(i).accept(astVisitor);
            }
        }
        if(this.for_stmt.size()>0) {
            for (int i = 0; i < this.for_stmt.size(); i++) {
                this.for_stmt.get(i).accept(astVisitor);
            }
        }
        if(this.if_stmt.size()>0) {
            for (int i = 0; i < this.if_stmt.size(); i++) {
                this.if_stmt.get(i).accept(astVisitor);
            }
        }
        if(this.if_line!=null)
        {
            for(int i=0;i<this.if_line.size();i++)
            {
                this.if_line.get(i).accept(astVisitor);
            }

        }

        if(this.assigneSelects.size()>0)
        {
            for(int i=0;i<this.assigneSelects.size();i++)
            {
                this.assigneSelects.get(i).accept(astVisitor);
            }
        }

        //Min , Plus
        if(this.plus_var.size()>0) {
            for (int i = 0; i < this.plus_var.size(); i++) {
                this.plus_var.get(i).accept(astVisitor);
            }
        }

       if(this.min_var.size()>0) {
           for (int i = 0; i < this.min_var.size(); i++) {
               this.min_var.get(i).accept(astVisitor);
           }
       }
    }

}
