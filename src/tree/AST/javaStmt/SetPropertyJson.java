package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

//addedkatren
public class SetPropertyJson extends Node {

    GetPropertyJson getPropertyJson;
    Expr expr;

    public void setGetPropertyJson(GetPropertyJson getPropertyJson) {
        this.getPropertyJson = getPropertyJson;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getPropertyJson!=null)
        {
            this.getPropertyJson.accept(astVisitor);
        }
        if(this.expr!=null)
        {
            this.expr.accept(astVisitor);
        }
    }
}