package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class ifLineSecondParameter extends Node {

    print pr /*= new print()*/;
    Expr expr /*= new Expr()*/;

    ifLine if_line /*= new ifLine()*/;
    ifLine ifLine_brakets;

    public void setIfLine_brakets(ifLine ifLine_brakets) {
        this.ifLine_brakets = ifLine_brakets;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public Expr getExpr() {
        return expr;
    }

    public void setPr(print pr) {
        this.pr = pr;
    }

    public print getPr() {
        return pr;
    }

    public void setIf_line(ifLine if_line) {
        this.if_line = if_line;
    }

    public ifLine getIf_line() {
        return if_line;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);


        if(this.pr != null){
            this.pr.accept(astVisitor);
        }

        if(this.expr != null){
            this.expr.accept(astVisitor);
        }

        if(this.if_line != null){
            this.if_line.accept(astVisitor);
        }

        if(this.ifLine_brakets!=null)
        {
            this.ifLine_brakets.accept(astVisitor);
        }

    }

}
