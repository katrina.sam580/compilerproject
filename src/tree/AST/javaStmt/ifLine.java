package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class ifLine extends Node {
   /* print pr = new print();
    Expr expr = new Expr();
*/
    //ifLine if_line = new ifLine();

    condition_header con_header ;

    ifLineFirstParameter if_lin_f_pa ;
    ifLineSecondParameter if_lin_s_pa;
    DeclareVar var;

    public void setCon_header(condition_header con_header) {
        this.con_header = con_header;
    }



    public condition_header getCon_header() {
        return con_header;
    }



    public void setIf_lin_f_pa(ifLineFirstParameter if_lin_f_pa) {
        this.if_lin_f_pa = if_lin_f_pa;
    }

    public ifLineFirstParameter getIf_lin_f_pa() {
        return if_lin_f_pa;
    }


    public void setIf_lin_s_pa(ifLineSecondParameter if_lin_s_pa) {
        this.if_lin_s_pa = if_lin_s_pa;
    }

    public ifLineSecondParameter getIf_lin_s_pa() {
        return if_lin_s_pa;
    }

    public void setVar(DeclareVar var) {
        this.var = var;
    }

    @Override
    public void accept(ASTVisitor astVisitor){


        astVisitor.visit(this);

        if(this.var!=null)
        {
            this.var.accept(astVisitor);
        }
        if(this.con_header!=null) {
            this.con_header.accept(astVisitor);
        }
        if(this.if_lin_f_pa!=null) {
            this.if_lin_f_pa.accept(astVisitor);
        }
        if(this.if_lin_s_pa!=null) {
            this.if_lin_s_pa.accept(astVisitor);
        }

    }
}
