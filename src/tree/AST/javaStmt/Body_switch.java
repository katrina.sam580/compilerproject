package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class Body_switch extends Node {
    List<Case> cases= new ArrayList<>();
    Default aDefault ;

    public void setCases(List<Case> cases) {
        this.cases = cases;
    }

    public void setaDefault(Default aDefault) {
        this.aDefault = aDefault;
    }

    public List<Case> getCases() {
        return cases;
    }

    public Default getaDefault() {
        return aDefault;
    }
    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        if(cases.size()>0)
        {
            for(int i=0;i<cases.size();i++)
            {
                this.cases.get(i).accept(astVisitor);
            }
        }
        if(this.aDefault!=null)
        {
            aDefault.accept(astVisitor);
        }
    }
}
