package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

import java.util.ArrayList;
import java.util.List;

public class IfDecleration  extends Node {
    condition_header if_header /*= new condition_header()*/;
    condition_header con_header /*= new condition_header()*/;
    BodyFunction body = new BodyFunction();
    BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket ;

    List<elseIf> elseIf = new ArrayList<>();

    Else els /*= new Else()*/;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setBodyFunctionWilthoutBracket(BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket) {
        this.bodyFunctionWilthoutBracket = bodyFunctionWilthoutBracket;
    }

    public BodyFunctionWilthoutBracket getBodyFunctionWilthoutBracket() {
        return bodyFunctionWilthoutBracket;
    }

    public void setIf_header(condition_header if_header) {
        this.if_header = if_header;
    }

    public condition_header getIf_header() {
        return if_header;
    }

    public void setElseIf(List<tree.AST.javaStmt.elseIf> elseIf) {
        this.elseIf = elseIf;
    }

    public List<tree.AST.javaStmt.elseIf> getElseIf() {
        return elseIf;
    }

    public void setBody(BodyFunction body) {
        this.body = body;
    }

    public BodyFunction getBody() {
        return body;
    }

    public void setEls(Else els) {
        this.els = els;
    }

    public Else getEls() {
        return els;
    }




    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);


        if(this.if_header != null){
            this.if_header.accept(astVisitor);
        }


        if(this.con_header != null){
            this.con_header.accept(astVisitor);
        }


        if(elseIf.size()>0) {
            for (int i = 0; i < elseIf.size(); i++) {

                this.elseIf.get(i).accept(astVisitor);
            }
        }

        if(this.els != null){
            this.els.accept(astVisitor);
        }

        if(this.body!=null)
        {
            this.body.accept(astVisitor);
        }


        if(this.bodyFunctionWilthoutBracket!=null)
        {
            this.bodyFunctionWilthoutBracket.accept(astVisitor);
        }



    }


}
