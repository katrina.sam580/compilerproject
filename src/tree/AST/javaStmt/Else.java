package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

public class Else  extends Node {

    BodyFunction body;
    BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setBody(BodyFunction body) {
        this.body = body;
    }

    public BodyFunction getBody() {
        return body;
    }

    public void setBodyFunctionWilthoutBracket(BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket) {
        this.bodyFunctionWilthoutBracket = bodyFunctionWilthoutBracket;
    }

    public BodyFunctionWilthoutBracket getBodyFunctionWilthoutBracket() {
        return bodyFunctionWilthoutBracket;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.body!=null)
        {
            this.body.accept(astVisitor);
        }
        if(this.bodyFunctionWilthoutBracket!=null)
        {
            this.bodyFunctionWilthoutBracket.accept(astVisitor);
        }

    }
}
