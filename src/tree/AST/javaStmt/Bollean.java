package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class Bollean extends Node {
    String Bool;

    Bollean left_expr;
    Bollean right_expr;

    public enum OP_logic {
        AMP,
        PIPE
    }

    OP_logic logic;
    public void setBool(String bool) {
        Bool = bool;
    }

    public String getBool() {
        return Bool;
    }

    public void setLeft_expr(Bollean left_expr) {
        this.left_expr = left_expr;
    }

    public void setRight_expr(Bollean right_expr) {
        this.right_expr = right_expr;
    }

    public Bollean getLeft_expr() {
        return left_expr;
    }

    public Bollean getRight_expr() {
        return right_expr;
    }

    public void setLogic(OP_logic logic) {
        this.logic = logic;
    }

    public OP_logic getLogic() {
        return logic;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.Bool!=null)
        {
            System.out.println("Bool : " + this.Bool);
        }

        if (this.left_expr!=null){
            astVisitor.visit(this);
           System.out.println("Bool_left : " + this.left_expr.getBool());
        }
        if (this.logic!=null)
        {
            System.out.println("bool OP : "+this.logic);
        }
        if (this.right_expr!=null){
            astVisitor.visit(this);
            System.out.println("Bool_right : " + this.right_expr.getBool());
        }

    }
}
