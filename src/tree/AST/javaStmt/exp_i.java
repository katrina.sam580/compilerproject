package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class exp_i extends Node {

    incrDecrWithoutComma inc_dec;

    String literal_value;

    List<exp_i> expI= new ArrayList<>();


    public void setLiteral_value(String literal_value) {
        this.literal_value = literal_value;
    }

    public void setInc_dec(incrDecrWithoutComma inc_dec) {
        this.inc_dec = inc_dec;
    }

    public String getLiteral_value() {
        return literal_value;
    }

    public incrDecrWithoutComma getInc_dec() {
        return inc_dec;
    }


    public void setExpI(List<exp_i> expI) {
        this.expI = expI;
    }

    public List<exp_i> getExpI() {
        return expI;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.inc_dec != null) {

            this.inc_dec.accept(astVisitor);
        }
        if (this.literal_value != null) {
            System.out.println("Literal Value : " + this.literal_value);
        }


        if(this.expI.size()>0)
        {

            for(int i=0;i<this.expI.size();i++)
            {
                this.expI.get(i).accept(astVisitor);
            }

        }

    }
}
