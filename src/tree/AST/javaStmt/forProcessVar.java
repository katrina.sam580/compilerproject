package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class forProcessVar extends Node {

    expFor exp_for ;

    incrDecrWithoutComma inc_dec ;


    public void setExp_for(expFor exp_for) {
        this.exp_for = exp_for;
    }

    public expFor getExp_for() {
        return exp_for;
    }

    public void setInc_dec(incrDecrWithoutComma inc_dec) {
        this.inc_dec = inc_dec;
    }

    public incrDecrWithoutComma getInc_dec() {
        return inc_dec;
    }
@Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.inc_dec != null){
            this.inc_dec.accept(astVisitor);

        }

        if(this.exp_for != null){
            this.exp_for.accept(astVisitor);
        }



    }
}
