package tree.AST.javaStmt;


import tree.AST.vis.ASTVisitor;

public class MathimaticalExpr extends Expr {

    Expr left_expr;
    Expr right_expr;
    public enum OP_math
    {
        PLUS,
        STAR,
        MINUS,
        DIV,
        MOD,
        PLUS_EQ,
        MINUS_EQ,
        STAR_EQ,
        DIV_EQ,
        MOD_EQ,
        ASSIGN
    }
    String op_math;
    OP_math math;


    public void setMath(OP_math math) {
        this.math = math;
    }

    public OP_math getMath() {
        return math;
    }

    public void setOp_math(String op_math) {
        this.op_math = op_math;
    }

    public void setLeft_expr(Expr left_expr) {
        this.left_expr = left_expr;
    }

    public void setRight_expr(Expr right_expr) {
        this.right_expr = right_expr;
    }

    public void setop_math(String op_math) {
        this.op_math = op_math;
    }

    public Expr getLeft_expr() {
        return left_expr;
    }

    public Expr getRight_expr() {
        return right_expr;
    }

    public String getOp_math() {
        return op_math;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.left_expr!=null) {
            left_expr.accept(astVisitor);
        }
        System.out.println("Math OP : "+this.getMath());
        if(this.right_expr!=null) {
            right_expr.accept(astVisitor);
        }
        //this.accept(astVisitor);

    }
}
