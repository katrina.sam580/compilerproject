package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

//addedkatren
public class ForEachHeader extends Node {

    String leftforeachstmt;
    String rightforeachstmt;
    Symbol symbol ;

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }
    public void setLeftforeachstmt(String leftforeachstmt) {
        this.leftforeachstmt = leftforeachstmt;
    }

    public void setRightforeachstmt(String rightforeachstmt) {
        this.rightforeachstmt = rightforeachstmt;
    }

    public String getLeftforeachstmt() {
        return leftforeachstmt;
    }

    public String getRightforeachstmt() {
        return rightforeachstmt;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);
        System.out.println("ForeachHeader : "+leftforeachstmt +" "+rightforeachstmt);
    }
}
