package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

//addedkatren
public class JsonObjectStmt extends Node {

    String ObjectName;
    JsonBody jsonBody;

    public void setObjectName(String objectName) {
        ObjectName = objectName;
    }

    public void setJsonBody(JsonBody jsonBody) {
        this.jsonBody = jsonBody;
    }

    public String getObjectName() {
        return ObjectName;
    }

    public JsonBody getJsonBody() {
        return jsonBody;
    }

    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        System.out.println("Json_Object_name : " + this.ObjectName);
        if(this.jsonBody!=null)
        {
            this.jsonBody.accept(astVisitor);
        }
    }

}
