package tree.AST.javaStmt;

import tree.AST.vis.ASTVisitor;

public class UnaryExpr extends Expr {

    Expr term;
    public enum unary_operator
    {
        PLUS,
        MINUS,
        TILDE,
        K_NOT
    }

    unary_operator Unary;

    public void setUnary(unary_operator unary) {
        Unary = unary;
    }

    public unary_operator getUnary() {
        return Unary;
    }

    public void setTerm(Expr term) {
        this.term = term;
    }

    public Expr getTerm() {
        return term;
    }

    @Override
    public void accept(ASTVisitor astVisitor)
    {
        astVisitor.visit(this);
        System.out.println("Unary OP :"+this.getUnary());
        if(this.term!=null)
        {
            this.term.accept(astVisitor);
        }
    }

}
