package tree.AST.javaStmt;

import tree.AST.vis.ASTVisitor;

public class condition_header extends Expr {
    Expr expr;
    Bollean bool_expr;
    condition_header condition_header_brackets;

    public void setCondition_header_brackets(condition_header condition_header_brackets) {
        this.condition_header_brackets = condition_header_brackets;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public Expr getExpr() {
        return expr;
    }

    public void setBool_expr(Bollean bool_expr) {
        this.bool_expr = bool_expr;
    }

    public Bollean getBool_expr() {
        return bool_expr;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.bool_expr!=null)
        {
            this.bool_expr.accept(astVisitor);
        }
        if(this.expr!=null)
        {
            //System.out.println("+++++++++");
            this.expr.accept(astVisitor);
        }

        if(this.condition_header_brackets!=null)
        {
            this.condition_header_brackets.accept(astVisitor);
        }
    }
}
