package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

//addedkatren
public class DeclareArray extends Node {

    String ArrayName;
    Expr Arrayindex1;
    Expr Arrayindex2;

    public void setArrayindex1(Expr arrayindex1) {
        Arrayindex1 = arrayindex1;
    }

    public void setArrayindex2(Expr arrayindex2) {
        Arrayindex2 = arrayindex2;
    }

    public Expr getArrayindex1() {
        return Arrayindex1;
    }

    public Expr getArrayindex2() {
        return Arrayindex2;
    }
    public void setArrayName(String arrayName) {
        ArrayName = arrayName;
    }

    public String getArrayName() {
        return ArrayName;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        System.out.println("Array_name : "+this.ArrayName);
        if(this.Arrayindex1!=null)
        {

            this.Arrayindex1.accept(astVisitor);
            //System.out.println("index_1 : " + Arrayindex1.getValue());
        }
        if(this.Arrayindex2!=null)
        {
            //System.out.println("index_2");
            this.Arrayindex2.accept(astVisitor);
        }
    }
}
