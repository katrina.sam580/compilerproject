package tree.AST.javaStmt;
import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class Header_while extends Node {

   tree.AST.javaStmt.condition_header condition_header;

    public void setCondition_header(tree.AST.javaStmt.condition_header condition_header) {
        this.condition_header = condition_header;
    }

    public tree.AST.javaStmt.condition_header getCondition_header() {
        return condition_header;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        this.condition_header.accept(astVisitor);


    }

}
