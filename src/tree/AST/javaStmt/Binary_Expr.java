package tree.AST.javaStmt;
import tree.AST.vis.ASTVisitor;

public class Binary_Expr extends Expr {
    Expr left_expr;
    Expr right_expr;

    public enum OP_logic {
        GT2,
        LT2,
        AMP,
        PIPE
    }

    OP_logic logic;

    public void setLogic(OP_logic logic) {
        this.logic = logic;
    }

    public void setRight_expr(Expr right_expr) {
        this.right_expr = right_expr;
    }

    public void setLeft_expr(Expr left_expr) {
        this.left_expr = left_expr;
    }

    public OP_logic getLogic() {
        return logic;
    }

    public Expr getRight_expr() {
        return right_expr;
    }

    public Expr getLeft_expr() {
        return left_expr;
    }

    @Override
    public void accept(ASTVisitor astVisitor) {
        astVisitor.visit(this);

        if (this.left_expr != null) {
            left_expr.accept(astVisitor);
        }
        System.out.println("Logic OP : " + this.getLogic());
        if (this.right_expr != null) {
            right_expr.accept(astVisitor);
        }
    }
}
