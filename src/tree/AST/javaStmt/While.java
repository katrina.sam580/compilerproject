package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

public class While extends Node {
    Header_while header_while;
    BodyFunction bodyFunction;
    BodyFunctionWilthoutBracket BODY;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setHeader_while(Header_while header_while) {
        this.header_while = header_while;
    }

    public void setBODY(BodyFunctionWilthoutBracket BODY) {
        this.BODY = BODY;
    }

    public Header_while getHeader_while() {
        return header_while;
    }

    public void setBodyFunction(BodyFunction bodyFunction) {
        this.bodyFunction = bodyFunction;
    }

    public BodyFunction getBodyFunction() {
        return bodyFunction;
    }

    public BodyFunctionWilthoutBracket getBODY() {
        return BODY;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.header_while!=null)
        {
            header_while.accept(astVisitor);
        }
       if(this.bodyFunction!=null)
       {
           bodyFunction.accept(astVisitor);
       }
        if(this.BODY!=null)
        {
            BODY.accept(astVisitor);
        }

    }

}
