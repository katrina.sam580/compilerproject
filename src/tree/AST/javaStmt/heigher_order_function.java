package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class heigher_order_function extends Node {
    Argument_list_higher argument_list_higher ;
    BodyFunction bodyFunction;


    public void setArgument_list_higher(Argument_list_higher argument_list_higher) {
        this.argument_list_higher = argument_list_higher;
    }

    public void setBodyFunction(BodyFunction bodyFunction) {
        this.bodyFunction = bodyFunction;
    }

    public Argument_list_higher getArgument_list_higher() {
        return argument_list_higher;
    }

    public BodyFunction getBodyFunction() {
        return bodyFunction;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.argument_list_higher!=null)
        {
            argument_list_higher.accept(astVisitor);
        }
        if(this.bodyFunction!=null)
        {
           bodyFunction.accept(astVisitor);
        }

    }
}
