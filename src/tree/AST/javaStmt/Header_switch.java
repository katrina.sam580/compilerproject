package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class Header_switch extends Node {
    Expr expr;
    Call_func call_func ;
    tree.AST.javaStmt.ifLine ifLine ;
    AssigneVar assigneVar;
    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public void setCall_func(Call_func call_func) {
        this.call_func = call_func;
    }

    public void setIfLine(tree.AST.javaStmt.ifLine ifLine) {
        this.ifLine = ifLine;
    }

    public void setAssigneVar(AssigneVar assigneVar) {
        this.assigneVar = assigneVar;
    }

    public Expr getExpr() {
        return expr;
    }

    public Call_func getCall_func() {
        return call_func;
    }

    public tree.AST.javaStmt.ifLine getIfLine() {
        return ifLine;
    }

    public AssigneVar getAssigneVar() {
        return assigneVar;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
         if(this.getCall_func()!=null)
        {
            call_func.accept(astVisitor);
        }
         else if(this.getExpr()!=null)
        {
            expr.accept(astVisitor);
        }

//        else if(this.getIfLine()!=null)
//        {
//            ifLine.accept(astVisitor);
//        }
        else if(this.getAssigneVar()!=null)
        {
            assigneVar.accept(astVisitor);
        }


    }
}
