package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

//addedkatren
public class Array extends Node {

    String ArrayName;

    public void setArrayName(String arrayName) {
        ArrayName = arrayName;
    }

    public String getArrayName() {
        return ArrayName;
    }
    @Override
    public void accept(ASTVisitor astVisitor){
        System.out.println("ArrayName : "+this.ArrayName);
    }
}
