package tree.AST.javaStmt;


import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class Argument_list_higher extends Node {
    List<Expr> expr= new ArrayList<>();
    List<Bollean> bool_exor = new ArrayList<>();

    public void setExpr(List<Expr> expr) {
        this.expr = expr;
    }

    public void setBool_exor(List<Bollean> bool_exor) {
        this.bool_exor = bool_exor;
    }

    public List<Expr> getExpr() {
        return expr;
    }

    public List<Bollean> getBool_exor() {
        return bool_exor;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        if(expr.size()>0)
        {
            for(int i=0;i<expr.size();i++)
            {
                this.expr.get(i).accept(astVisitor);
            }
        }
        if(bool_exor.size()>0)
        {
            for(int i=0;i<bool_exor.size();i++)
            {
                this.bool_exor.get(i).accept(astVisitor);
            }
        }

    }
}
