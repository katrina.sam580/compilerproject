package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class ForCondition extends Node {


    Expr exp;

    public void setExp(Expr exp) {
        this.exp = exp;
    }


    public Expr getExp() {
        return exp;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        this.exp.accept(astVisitor);
        // System.out.println("exp:"+ this.exp.getValue());

    }
}
