package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

public class forDecleration extends Node {
    // headerFor header = new headerFor();


    DeclareForVar for_var ;
    ForCondition for_con ;
    forProcessVar for_process ;

    BodyFunction body ;
    BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setBodyFunctionWilthoutBracket(BodyFunctionWilthoutBracket bodyFunctionWilthoutBracket) {
        this.bodyFunctionWilthoutBracket = bodyFunctionWilthoutBracket;
    }

    public BodyFunctionWilthoutBracket getBodyFunctionWilthoutBracket() {
        return bodyFunctionWilthoutBracket;
    }

    public void setFor_var(DeclareForVar for_var) {
        this.for_var = for_var;
    }

    public void setFor_con(ForCondition for_con) {
        this.for_con = for_con;
    }

    public DeclareForVar getFor_var() {
        return for_var;
    }

    public ForCondition getFor_con() {
        return for_con;
    }


    public void setFor_process(forProcessVar for_process) {
        this.for_process = for_process;
    }

    public forProcessVar getFor_process() {
        return for_process;
    }


    public void setBody(BodyFunction body) {
        this.body = body;
    }


    public BodyFunction getBody() {
        return body;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        this.for_var.accept(astVisitor);
        this.for_con.accept(astVisitor);
        this.for_process.accept(astVisitor);
if(this.body!=null)
{
    this.body.accept(astVisitor);
}
if(this.bodyFunctionWilthoutBracket!=null)
{
    this.bodyFunctionWilthoutBracket.accept(astVisitor);
}



    }



}
