package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class FunctionName extends Node {
    String funcname;

    public void setFuncname(String funcname) {
        this.funcname = funcname;
    }

    public String getFuncname() {
        return funcname;
    }
@Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);
        System.out.println("Function name : "+this.funcname);
    }
}
