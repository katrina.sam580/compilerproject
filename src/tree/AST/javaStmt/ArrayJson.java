package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

//addedkatren
public class ArrayJson extends Node {

    List<JsonBody>  jsonBodies = new ArrayList<>();
    List<LvalueJson> lvalueJsons = new ArrayList<>();

    public void setJsonBodies(List<JsonBody> jsonBodies) {
        this.jsonBodies = jsonBodies;
    }

    public void setLvalueJsons(List<LvalueJson> lvalueJsons) {
        this.lvalueJsons = lvalueJsons;
    }

    public List<JsonBody> getJsonBodies() {
        return jsonBodies;
    }

    public List<LvalueJson> getLvalueJsons() {
        return lvalueJsons;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.jsonBodies.size()>0)
        {
            for(int i=0;i<this.jsonBodies.size();i++)
            {
                this.jsonBodies.get(i).accept(astVisitor);
            }
        }
        if(this.lvalueJsons.size()>0)
        {
            for(int i=0;i<this.lvalueJsons.size();i++)
            {
                this.lvalueJsons.get(i).accept(astVisitor);
            }
        }


    }
}
