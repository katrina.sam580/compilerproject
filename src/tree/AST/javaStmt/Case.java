package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.QueryStmt.anyNam;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Scope;

public class Case extends Node {
    anyNam any_name ;
    String case_of_switch;
    BodyFunction body ;
    Scope scope;

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }
    public void setBody(BodyFunction body) {
        this.body = body;
    }

    public void setAny_name(anyNam any_name) {
        this.any_name = any_name;
    }

    public anyNam getAny_name() {
        return any_name;
    }

    public void setCase_of_switch(String case_of_switch) {
        this.case_of_switch = case_of_switch;
    }

    public BodyFunction getBody() {
        return body;
    }

    public String getCase_of_switch() {
        return case_of_switch;
    }

    @Override
    public void accept(ASTVisitor astVisitor){

        astVisitor.visit(this);

        if(this.any_name!=null)
        {
            any_name.accept(astVisitor);
        }
        if(this.case_of_switch!=null)
        {
            System.out.println("case_of_switch "+ this.case_of_switch);
        }
        if(this.body!=null)
        {
            body.accept(astVisitor);
        }


    }


}
