package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

//addedkatren
public class JsonBody extends Node {

    List<PropertyJson> propertyJsons = new ArrayList<>();

    public void setPropertyJsons(List<PropertyJson> propertyJsons) {
        this.propertyJsons = propertyJsons;
    }

    public List<PropertyJson> getPropertyJsons() {
        return propertyJsons;
    }

    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(propertyJsons.size()>0)
        {
            for(int i=0;i<propertyJsons.size();i++)
            {
                this.propertyJsons.get(i).accept(astVisitor);
            }
        }

    }
}
