package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;
import tree.SymbolTable.Symbol;

public class Call_func extends Node {
    String var_assin;
    FunctionName fun_name ;
    Argument_list argument_list ;
    Symbol symbol ;

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setVar_assin(String var_assin) {
        this.var_assin = var_assin;
    }

    public void setFun_name(FunctionName fun_name) {
        this.fun_name = fun_name;
    }

    public void setArgument_list(Argument_list argument_list) {
        this.argument_list = argument_list;
    }

    public String getVar_assin() {
        return var_assin;
    }

    public FunctionName getFun_name() {
        return fun_name;
    }

    public Argument_list getArgument_list() {
        return argument_list;
    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
        if(this.getVar_assin()!=null)
        {
            System.out.println("var_assin "+ this.var_assin);
        }
        if(this.argument_list!=null)
        {
            fun_name.accept(astVisitor);
        }
        if(this.argument_list!=null)
        {
            argument_list.accept(astVisitor);
        }
    }


}
