package tree.AST.javaStmt;

import tree.AST.Node;
import tree.AST.vis.ASTVisitor;

public class ifLineFirstParameter extends Node {
    print pr;
    Expr expr;

    ifLine if_line /* = new ifLine()*/;
    ifLine ifLine_brackets;

    public void setIfLine_brackets(ifLine ifLine_brackets) {
        this.ifLine_brackets = ifLine_brackets;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public Expr getExpr() {
        return expr;
    }

    public void setPr(print pr) {
        this.pr = pr;
    }

    public print getPr() {
        return pr;
    }

    public void setIf_lin(ifLine if_lin) {
        this.if_line = if_lin;
    }

    public ifLine getIf_lin() {
        return if_line;
    }


    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);

        if(this.if_line != null){
            this.if_line.accept(astVisitor);
        }

        if(this.pr != null){
            this.pr.accept(astVisitor);
        }

        if(this.expr != null){
            this.expr.accept(astVisitor);
        }

        if(this.ifLine_brackets!=null)
        {
         this.ifLine_brackets.accept(astVisitor);
        }

    }
}

