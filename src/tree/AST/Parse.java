package tree.AST;

import tree.AST.QueryStmt.SQL_Statement;
import tree.AST.javaStmt.FunctionDeclaration;
import tree.AST.vis.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class Parse extends Node {

    private List<List<SQL_Statement>> sqlStmts = new ArrayList<>();
    private List<FunctionDeclaration> functions = new ArrayList<FunctionDeclaration>();

//    public void addQuery(SQL_Statement query){
//        this.sqlStmts.add(query);
//    }

    public void setSqlStmts(List<List<SQL_Statement>> sqlStmts) {
        this.sqlStmts = sqlStmts;
    }

    public void setFunctions(List<FunctionDeclaration> functions) {
        this.functions = functions;
    }

    public List<List<SQL_Statement>> getSqlStmts() {
        return sqlStmts;
    }

    public List<FunctionDeclaration> getFunctions() {
        return functions;
    }

//    @Override
//    public String toString(){
//
//        if(this.sqlStmts.size()!=0) {
//            return  "sql stmts = " + getSqlStmts().get(0).getName();
//        }
//       /* if(functions.size()!=0)
//        {
//            return  "function stmts = " + getFunctions().get(0).getFuncname();
//        }*/
//       return " ";
//    }

    @Override
    public void accept(ASTVisitor astVisitor){
        astVisitor.visit(this);
//        this.sqlStmts.forEach( stmt -> stmt.accept(astVisitor));
        if(this.sqlStmts.size()>0) {
            for (int i = 0; i < this.sqlStmts.size(); i++) {
                for(int j=0;j<this.sqlStmts.get(i).size();j++) {
                    this.sqlStmts.get(i).get(j).accept(astVisitor);
                }
            }
        }
        if(this.functions.size()>0) {
            for (int i = 0; i < this.functions.size(); i++) {
                this.functions.get(i).accept(astVisitor);
            }
        }
    }

}
