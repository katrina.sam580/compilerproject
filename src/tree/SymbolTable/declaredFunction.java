package tree.SymbolTable;

import java.util.ArrayList;

public class declaredFunction {
    private String FunctionName;
    private Type returnType;
    private Scope scopefunction;

    public void setFunctionName(String functionName) {
        FunctionName = functionName;
    }


    public void setReturnType(Type returnType) {
        this.returnType = returnType;
    }

    public void setScopefunction(Scope scopefunction) {
        this.scopefunction = scopefunction;
    }


    public Scope getScopefunction() {
        return scopefunction;
    }

    public String getFunctionName() {
        return FunctionName;
    }

    public Type getReturnType() {
        return returnType;
    }
}
