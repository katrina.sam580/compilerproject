package tree.SymbolTable;

import java.util.ArrayList;
import tree.AST.QueryStmt.create_aggregation_function;

public class SymbolTable {


    private ArrayList<Scope> scopes = new ArrayList<Scope>();

    private ArrayList<Type> declaredTypes = new ArrayList<Type>();
    private ArrayList<Type> declaredTypesType = new ArrayList<Type>();// type of types
    private ArrayList<create_aggregation_function> declaredAggregationFunction = new ArrayList<create_aggregation_function>();
    private ArrayList<declaredFunction> declaredFunctions = new ArrayList<declaredFunction>();

    public void setDeclaredFunction(ArrayList<tree.SymbolTable.declaredFunction> declaredFunction) {
        this.declaredFunctions = declaredFunction;
    }

    public ArrayList<tree.SymbolTable.declaredFunction> getDeclaredFunction() {
        return declaredFunctions;
    }
    public void addFunction(declaredFunction declaredFunction){
        this.declaredFunctions.add(declaredFunction);
    }

    public ArrayList<create_aggregation_function> getDeclaredAggregationFunction() {
        return declaredAggregationFunction;
    }

    public void setDeclaredAggregationFunction(ArrayList<create_aggregation_function> declaredAggregationFunction) {
        this.declaredAggregationFunction = declaredAggregationFunction;
    }
    public void setDeclaredTypesType(ArrayList<Type> declaredTypesType) {
        this.declaredTypesType = declaredTypesType;
    }

    public ArrayList<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(ArrayList<Scope> scopes) {
        this.scopes = scopes;
    }

    public ArrayList<Type> getDeclaredTypes() {
        return declaredTypes;
    }
    public ArrayList<Type> getDeclaredTypesType() {
        return declaredTypesType;
    }

    public void setDeclaredTypes(ArrayList<Type> declaredTypes) {
        this.declaredTypes = declaredTypes;
    }


    public void addScope(Scope scope){
        this.scopes.add(scope);
    }
    public void addType(Type type){
        this.declaredTypes.add(type);
    }
    public void addTypeType(Type type){
        this.declaredTypesType.add(type);
    }
}
