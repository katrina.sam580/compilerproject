// Generated from C:/Users/User/Desktop/New folder (4)/Compiler (3)/src\SQL.g4 by ANTLR 4.7.2
package generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link SQLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface SQLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link SQLParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(SQLParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#error}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError(SQLParser.ErrorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt_list(SQLParser.Sql_stmt_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#sql_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt(SQLParser.Sql_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#alter_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlter_table_stmt(SQLParser.Alter_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#alter_table_add_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlter_table_add_constraint(SQLParser.Alter_table_add_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#alter_table_add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlter_table_add(SQLParser.Alter_table_addContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#create_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_table_stmt(SQLParser.Create_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#type_create_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_create_table(SQLParser.Type_create_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#path_create_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPath_create_table(SQLParser.Path_create_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_aggregation_function(SQLParser.Create_aggregation_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#jar_path}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJar_path(SQLParser.Jar_pathContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#class_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_name(SQLParser.Class_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#return_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_type(SQLParser.Return_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#method_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod_name(SQLParser.Method_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#delete_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelete_stmt(SQLParser.Delete_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#drop_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrop_table_stmt(SQLParser.Drop_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactored_select_stmt(SQLParser.Factored_select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#assigne_select}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssigne_select(SQLParser.Assigne_selectContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#insert_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsert_stmt(SQLParser.Insert_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_stmt(SQLParser.Select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#select_or_values}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_or_values(SQLParser.Select_or_valuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#update_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdate_stmt(SQLParser.Update_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#update_first_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdate_first_parameter(SQLParser.Update_first_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#update_second_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdate_second_parameter(SQLParser.Update_second_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_def(SQLParser.Column_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#type_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_name(SQLParser.Type_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint(SQLParser.Column_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_primary_key(SQLParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_foreign_key(SQLParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_not_null(SQLParser.Column_constraint_not_nullContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_constraint_null}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_null(SQLParser.Column_constraint_nullContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_default}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_default(SQLParser.Column_defaultContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_default_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_default_value(SQLParser.Column_default_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(SQLParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#expr_test}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_test(SQLParser.Expr_testContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#fun_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFun_name(SQLParser.Fun_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#parameter_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter_list(SQLParser.Parameter_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#define_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefine_var(SQLParser.Define_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#define_var_assigne}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefine_var_assigne(SQLParser.Define_var_assigneContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(SQLParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(SQLParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(SQLParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#body_without_bracketa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody_without_bracketa(SQLParser.Body_without_bracketaContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#json_object_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJson_object_stmt(SQLParser.Json_object_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#json_body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJson_body(SQLParser.Json_bodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#property_json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProperty_json(SQLParser.Property_jsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#array_json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_json(SQLParser.Array_jsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#get_property_json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGet_property_json(SQLParser.Get_property_jsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#set_prorerty_json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSet_prorerty_json(SQLParser.Set_prorerty_jsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#json_object_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJson_object_name(SQLParser.Json_object_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#return_stm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_stm(SQLParser.Return_stmContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#expr_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_condition(SQLParser.Expr_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#condition_header}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_header(SQLParser.Condition_headerContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#if_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_stmt(SQLParser.If_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#else_if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_if(SQLParser.Else_ifContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#else_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_stmt(SQLParser.Else_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#switch_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_stmt(SQLParser.Switch_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#case_stm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCase_stm(SQLParser.Case_stmContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#default_stm_switch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_stm_switch(SQLParser.Default_stm_switchContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#body_switch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody_switch(SQLParser.Body_switchContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#exp_for}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp_for(SQLParser.Exp_forContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#exp_i}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp_i(SQLParser.Exp_iContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#incr_decr_without_comma}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncr_decr_without_comma(SQLParser.Incr_decr_without_commaContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#for_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_stmt(SQLParser.For_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#declare_var_for}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclare_var_for(SQLParser.Declare_var_forContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#for_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_condition(SQLParser.For_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#for_process_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_process_var(SQLParser.For_process_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#min_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMin_var(SQLParser.Min_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#plus_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlus_var(SQLParser.Plus_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#foreach}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeach(SQLParser.ForeachContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#foreach_header}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeach_header(SQLParser.Foreach_headerContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#if_line_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_line_stmt(SQLParser.If_line_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#if_line_first_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_line_first_parameter(SQLParser.If_line_first_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#if_line_second_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_line_second_parameter(SQLParser.If_line_second_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(SQLParser.PrintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#expr_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_var(SQLParser.Expr_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#declare_variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclare_variable(SQLParser.Declare_variableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#declare_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclare_array(SQLParser.Declare_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#schema_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSchema_array(SQLParser.Schema_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#array_assigne}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_assigne(SQLParser.Array_assigneContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#element_arr_2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement_arr_2(SQLParser.Element_arr_2Context ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#element_arr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement_arr(SQLParser.Element_arrContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#declare_array_vr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclare_array_vr(SQLParser.Declare_array_vrContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#expr_while}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_while(SQLParser.Expr_whileContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#op1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOp1(SQLParser.Op1Context ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#while_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_stmt(SQLParser.While_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#do_while_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_while_stmt(SQLParser.Do_while_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#call_func}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall_func(SQLParser.Call_funcContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#aggregation_func}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregation_func(SQLParser.Aggregation_funcContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#heigher_order_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHeigher_order_function(SQLParser.Heigher_order_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#args_list_higher}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgs_list_higher(SQLParser.Args_list_higherContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#argument_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument_list(SQLParser.Argument_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#aggr_argument_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggr_argument_list(SQLParser.Aggr_argument_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#bool_ex}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool_ex(SQLParser.Bool_exContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_key_clause(SQLParser.Foreign_key_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFk_target_column_name(SQLParser.Fk_target_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#indexed_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexed_column(SQLParser.Indexed_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint(SQLParser.Table_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_primary_key(SQLParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_foreign_key(SQLParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_unique(SQLParser.Table_constraint_uniqueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_constraint_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_key(SQLParser.Table_constraint_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFk_origin_column_name(SQLParser.Fk_origin_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#qualified_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualified_table_name(SQLParser.Qualified_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#ordering_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdering_term(SQLParser.Ordering_termContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#pragma_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPragma_value(SQLParser.Pragma_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#common_table_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCommon_table_expression(SQLParser.Common_table_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#result_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column(SQLParser.Result_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_or_subquery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_subquery(SQLParser.Table_or_subqueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#join_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_clause(SQLParser.Join_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#join_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_operator(SQLParser.Join_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#join_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_constraint(SQLParser.Join_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#select_core}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_core(SQLParser.Select_coreContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#create_type_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_type_stmt(SQLParser.Create_type_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#create_type_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_type_name(SQLParser.Create_type_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_def_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_def_type(SQLParser.Column_def_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#cte_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCte_table_name(SQLParser.Cte_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#signed_number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSigned_number(SQLParser.Signed_numberContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#literal_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral_value(SQLParser.Literal_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#literal_value_json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral_value_json(SQLParser.Literal_value_jsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#unary_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operator(SQLParser.Unary_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#error_message}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError_message(SQLParser.Error_messageContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#module_argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModule_argument(SQLParser.Module_argumentContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_alias(SQLParser.Column_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyword(SQLParser.KeywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#unknown}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnknown(SQLParser.UnknownContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(SQLParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#function_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_name(SQLParser.Function_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#database_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabase_name(SQLParser.Database_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#source_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSource_table_name(SQLParser.Source_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_name(SQLParser.Table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_or_index_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_index_name(SQLParser.Table_or_index_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#new_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNew_table_name(SQLParser.New_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name(SQLParser.Column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#collation_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollation_name(SQLParser.Collation_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#foreign_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_table(SQLParser.Foreign_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#index_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_name(SQLParser.Index_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#trigger_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrigger_name(SQLParser.Trigger_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#view_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitView_name(SQLParser.View_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#module_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModule_name(SQLParser.Module_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#pragma_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPragma_name(SQLParser.Pragma_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#savepoint_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSavepoint_name(SQLParser.Savepoint_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#table_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_alias(SQLParser.Table_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#transaction_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTransaction_name(SQLParser.Transaction_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link SQLParser#any_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny_name(SQLParser.Any_nameContext ctx);
}