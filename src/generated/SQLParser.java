// Generated from C:/Users/User/Desktop/New folder (4)/Compiler (3)/src\SQL.g4 by ANTLR 4.7.2
package generated;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SQLParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, PLUS_PLUSl=9, 
		MINUS_MINUSl=10, PLUS_PLUSr=11, MINUS_MINUSr=12, SCOL=13, DOT=14, OPEN_PAR=15, 
		CLOSE_PAR=16, COMMA=17, ASSIGN=18, STAR=19, PLUS=20, MINUS=21, TILDE=22, 
		PIPE2=23, DIV=24, MOD=25, LT2=26, GT2=27, AMP=28, AMP2=29, PIPE=30, LT=31, 
		LT_EQ=32, GT=33, GT_EQ=34, EQ=35, NOT_EQ1=36, NOT_EQ2=37, PLUS_EQ=38, 
		MINUS_EQ=39, STAR_EQ=40, DIV_EQ=41, MOD_EQ=42, K_ABORT=43, K_ACTION=44, 
		K_ADD=45, K_AGGREGATION_FUNC=46, K_AGGREGATION=47, K_AFTER=48, K_ALL=49, 
		K_ALTER=50, K_ANALYZE=51, K_AND=52, K_AS=53, K_ASC=54, K_ATTACH=55, K_AUTOINCREMENT=56, 
		K_BEFORE=57, K_BEGIN=58, K_BETWEEN=59, K_BY=60, K_CASCADE=61, K_CASE=62, 
		K_CAST=63, K_CHECK=64, K_COLLATE=65, K_COLUMN=66, K_COMMIT=67, K_CONFLICT=68, 
		K_CONSTRAINT=69, K_CREATE=70, K_CROSS=71, K_CURRENT_DATE=72, K_CURRENT_TIME=73, 
		K_CURRENT_TIMESTAMP=74, K_DATABASE=75, K_DEFAULT=76, K_DEFERRABLE=77, 
		K_DEFERRED=78, K_DELETE=79, K_DESC=80, K_DETACH=81, K_DISTINCT=82, K_DROP=83, 
		K_EACH=84, K_ELSE=85, K_END=86, K_ENABLE=87, K_ESCAPE=88, K_EXCEPT=89, 
		K_EXCLUSIVE=90, K_EXISTS=91, K_EXPLAIN=92, K_FAIL=93, K_FOR=94, K_FOREIGN=95, 
		K_FROM=96, K_FULL=97, K_GLOB=98, K_GROUP=99, K_HAVING=100, K_IF=101, K_IGNORE=102, 
		K_IMMEDIATE=103, K_IN=104, K_INDEX=105, K_INDEXED=106, K_INITIALLY=107, 
		K_INNER=108, K_INSERT=109, K_INSTEAD=110, K_INTERSECT=111, K_INTO=112, 
		K_IS=113, K_ISNULL=114, K_JOIN=115, K_KEY=116, K_LEFT=117, K_LIKE=118, 
		K_LIMIT=119, K_MATCH=120, K_NATURAL=121, K_NEXTVAL=122, K_NO=123, K_NOT=124, 
		K_NOTNULL=125, K_NULL=126, K_OF=127, K_OFFSET=128, K_ON=129, K_ONLY=130, 
		K_OR=131, K_ORDER=132, K_OUTER=133, K_PLAN=134, K_PRAGMA=135, K_PRIMARY=136, 
		K_QUERY=137, K_RAISE=138, K_RECURSIVE=139, K_REFERENCES=140, K_REGEXP=141, 
		K_REINDEX=142, K_RELEASE=143, K_RENAME=144, K_REPLACE=145, K_RESTRICT=146, 
		K_RIGHT=147, K_ROLLBACK=148, K_ROW=149, K_SAVEPOINT=150, K_SELECT=151, 
		K_SET=152, K_TABLE=153, K_TYPE=154, K_PATH=155, K_TEMP=156, K_TEMPORARY=157, 
		K_THEN=158, K_TO=159, K_TRANSACTION=160, K_TRIGGER=161, K_UNION=162, K_UNIQUE=163, 
		K_UPDATE=164, K_USING=165, K_VACUUM=166, K_VALUES=167, K_VIEW=168, K_VIRTUAL=169, 
		K_WHEN=170, K_WHERE=171, K_WITH=172, K_WITHOUT=173, K_SWITCH=174, K_RETURN=175, 
		K_VAR=176, K_PRINT=177, K_TRUE=178, K_FALSE=179, K_WHILE=180, K_DO=181, 
		K_BREAK=182, K_FUNCTION=183, IDENTIFIER=184, IDENTIFIER2=185, NUMERIC_LITERAL=186, 
		BIND_PARAMETER=187, STRING_LITERAL=188, BLOB_LITERAL=189, SINGLE_LINE_COMMENT=190, 
		MULTILINE_COMMENT=191, SPACES=192, UNEXPECTED_CHAR=193, DIGIT=194;
	public static final int
		RULE_parse = 0, RULE_error = 1, RULE_sql_stmt_list = 2, RULE_sql_stmt = 3, 
		RULE_alter_table_stmt = 4, RULE_alter_table_add_constraint = 5, RULE_alter_table_add = 6, 
		RULE_create_table_stmt = 7, RULE_type_create_table = 8, RULE_path_create_table = 9, 
		RULE_create_aggregation_function = 10, RULE_jar_path = 11, RULE_class_name = 12, 
		RULE_return_type = 13, RULE_method_name = 14, RULE_delete_stmt = 15, RULE_drop_table_stmt = 16, 
		RULE_factored_select_stmt = 17, RULE_assigne_select = 18, RULE_insert_stmt = 19, 
		RULE_select_stmt = 20, RULE_select_or_values = 21, RULE_update_stmt = 22, 
		RULE_update_first_parameter = 23, RULE_update_second_parameter = 24, RULE_column_def = 25, 
		RULE_type_name = 26, RULE_column_constraint = 27, RULE_column_constraint_primary_key = 28, 
		RULE_column_constraint_foreign_key = 29, RULE_column_constraint_not_null = 30, 
		RULE_column_constraint_null = 31, RULE_column_default = 32, RULE_column_default_value = 33, 
		RULE_expr = 34, RULE_expr_test = 35, RULE_fun_name = 36, RULE_parameter_list = 37, 
		RULE_define_var = 38, RULE_define_var_assigne = 39, RULE_function = 40, 
		RULE_block = 41, RULE_body = 42, RULE_body_without_bracketa = 43, RULE_json_object_stmt = 44, 
		RULE_json_body = 45, RULE_property_json = 46, RULE_array_json = 47, RULE_get_property_json = 48, 
		RULE_set_prorerty_json = 49, RULE_json_object_name = 50, RULE_return_stm = 51, 
		RULE_expr_condition = 52, RULE_condition_header = 53, RULE_if_stmt = 54, 
		RULE_else_if = 55, RULE_else_stmt = 56, RULE_switch_stmt = 57, RULE_case_stm = 58, 
		RULE_default_stm_switch = 59, RULE_body_switch = 60, RULE_exp_for = 61, 
		RULE_exp_i = 62, RULE_incr_decr_without_comma = 63, RULE_for_stmt = 64, 
		RULE_declare_var_for = 65, RULE_for_condition = 66, RULE_for_process_var = 67, 
		RULE_min_var = 68, RULE_plus_var = 69, RULE_foreach = 70, RULE_foreach_header = 71, 
		RULE_if_line_stmt = 72, RULE_if_line_first_parameter = 73, RULE_if_line_second_parameter = 74, 
		RULE_print = 75, RULE_expr_var = 76, RULE_declare_variable = 77, RULE_declare_array = 78, 
		RULE_schema_array = 79, RULE_array_assigne = 80, RULE_element_arr_2 = 81, 
		RULE_element_arr = 82, RULE_declare_array_vr = 83, RULE_expr_while = 84, 
		RULE_op1 = 85, RULE_while_stmt = 86, RULE_do_while_stmt = 87, RULE_call_func = 88, 
		RULE_aggregation_func = 89, RULE_heigher_order_function = 90, RULE_args_list_higher = 91, 
		RULE_argument_list = 92, RULE_aggr_argument_list = 93, RULE_bool_ex = 94, 
		RULE_foreign_key_clause = 95, RULE_fk_target_column_name = 96, RULE_indexed_column = 97, 
		RULE_table_constraint = 98, RULE_table_constraint_primary_key = 99, RULE_table_constraint_foreign_key = 100, 
		RULE_table_constraint_unique = 101, RULE_table_constraint_key = 102, RULE_fk_origin_column_name = 103, 
		RULE_qualified_table_name = 104, RULE_ordering_term = 105, RULE_pragma_value = 106, 
		RULE_common_table_expression = 107, RULE_result_column = 108, RULE_table_or_subquery = 109, 
		RULE_join_clause = 110, RULE_join_operator = 111, RULE_join_constraint = 112, 
		RULE_select_core = 113, RULE_create_type_stmt = 114, RULE_create_type_name = 115, 
		RULE_column_def_type = 116, RULE_cte_table_name = 117, RULE_signed_number = 118, 
		RULE_literal_value = 119, RULE_literal_value_json = 120, RULE_unary_operator = 121, 
		RULE_error_message = 122, RULE_module_argument = 123, RULE_column_alias = 124, 
		RULE_keyword = 125, RULE_unknown = 126, RULE_name = 127, RULE_function_name = 128, 
		RULE_database_name = 129, RULE_source_table_name = 130, RULE_table_name = 131, 
		RULE_table_or_index_name = 132, RULE_new_table_name = 133, RULE_column_name = 134, 
		RULE_collation_name = 135, RULE_foreign_table = 136, RULE_index_name = 137, 
		RULE_trigger_name = 138, RULE_view_name = 139, RULE_module_name = 140, 
		RULE_pragma_name = 141, RULE_savepoint_name = 142, RULE_table_alias = 143, 
		RULE_transaction_name = 144, RULE_any_name = 145;
	private static String[] makeRuleNames() {
		return new String[] {
			"parse", "error", "sql_stmt_list", "sql_stmt", "alter_table_stmt", "alter_table_add_constraint", 
			"alter_table_add", "create_table_stmt", "type_create_table", "path_create_table", 
			"create_aggregation_function", "jar_path", "class_name", "return_type", 
			"method_name", "delete_stmt", "drop_table_stmt", "factored_select_stmt", 
			"assigne_select", "insert_stmt", "select_stmt", "select_or_values", "update_stmt", 
			"update_first_parameter", "update_second_parameter", "column_def", "type_name", 
			"column_constraint", "column_constraint_primary_key", "column_constraint_foreign_key", 
			"column_constraint_not_null", "column_constraint_null", "column_default", 
			"column_default_value", "expr", "expr_test", "fun_name", "parameter_list", 
			"define_var", "define_var_assigne", "function", "block", "body", "body_without_bracketa", 
			"json_object_stmt", "json_body", "property_json", "array_json", "get_property_json", 
			"set_prorerty_json", "json_object_name", "return_stm", "expr_condition", 
			"condition_header", "if_stmt", "else_if", "else_stmt", "switch_stmt", 
			"case_stm", "default_stm_switch", "body_switch", "exp_for", "exp_i", 
			"incr_decr_without_comma", "for_stmt", "declare_var_for", "for_condition", 
			"for_process_var", "min_var", "plus_var", "foreach", "foreach_header", 
			"if_line_stmt", "if_line_first_parameter", "if_line_second_parameter", 
			"print", "expr_var", "declare_variable", "declare_array", "schema_array", 
			"array_assigne", "element_arr_2", "element_arr", "declare_array_vr", 
			"expr_while", "op1", "while_stmt", "do_while_stmt", "call_func", "aggregation_func", 
			"heigher_order_function", "args_list_higher", "argument_list", "aggr_argument_list", 
			"bool_ex", "foreign_key_clause", "fk_target_column_name", "indexed_column", 
			"table_constraint", "table_constraint_primary_key", "table_constraint_foreign_key", 
			"table_constraint_unique", "table_constraint_key", "fk_origin_column_name", 
			"qualified_table_name", "ordering_term", "pragma_value", "common_table_expression", 
			"result_column", "table_or_subquery", "join_clause", "join_operator", 
			"join_constraint", "select_core", "create_type_stmt", "create_type_name", 
			"column_def_type", "cte_table_name", "signed_number", "literal_value", 
			"literal_value_json", "unary_operator", "error_message", "module_argument", 
			"column_alias", "keyword", "unknown", "name", "function_name", "database_name", 
			"source_table_name", "table_name", "table_or_index_name", "new_table_name", 
			"column_name", "collation_name", "foreign_table", "index_name", "trigger_name", 
			"view_name", "module_name", "pragma_name", "savepoint_name", "table_alias", 
			"transaction_name", "any_name"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'::'", "'{'", "'}'", "':'", "'['", "']'", "'?'", "'[]'", null, 
			null, null, null, "';'", "'.'", "'('", "')'", "','", "'='", "'*'", "'+'", 
			"'-'", "'~'", "'||'", "'/'", "'%'", "'<<'", "'>>'", "'&'", "'&&'", "'|'", 
			"'<'", "'<='", "'>'", "'>='", "'=='", "'!='", "'<>'", "'+='", "'-='", 
			"'*='", "'/='", "'%='"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "PLUS_PLUSl", "MINUS_MINUSl", 
			"PLUS_PLUSr", "MINUS_MINUSr", "SCOL", "DOT", "OPEN_PAR", "CLOSE_PAR", 
			"COMMA", "ASSIGN", "STAR", "PLUS", "MINUS", "TILDE", "PIPE2", "DIV", 
			"MOD", "LT2", "GT2", "AMP", "AMP2", "PIPE", "LT", "LT_EQ", "GT", "GT_EQ", 
			"EQ", "NOT_EQ1", "NOT_EQ2", "PLUS_EQ", "MINUS_EQ", "STAR_EQ", "DIV_EQ", 
			"MOD_EQ", "K_ABORT", "K_ACTION", "K_ADD", "K_AGGREGATION_FUNC", "K_AGGREGATION", 
			"K_AFTER", "K_ALL", "K_ALTER", "K_ANALYZE", "K_AND", "K_AS", "K_ASC", 
			"K_ATTACH", "K_AUTOINCREMENT", "K_BEFORE", "K_BEGIN", "K_BETWEEN", "K_BY", 
			"K_CASCADE", "K_CASE", "K_CAST", "K_CHECK", "K_COLLATE", "K_COLUMN", 
			"K_COMMIT", "K_CONFLICT", "K_CONSTRAINT", "K_CREATE", "K_CROSS", "K_CURRENT_DATE", 
			"K_CURRENT_TIME", "K_CURRENT_TIMESTAMP", "K_DATABASE", "K_DEFAULT", "K_DEFERRABLE", 
			"K_DEFERRED", "K_DELETE", "K_DESC", "K_DETACH", "K_DISTINCT", "K_DROP", 
			"K_EACH", "K_ELSE", "K_END", "K_ENABLE", "K_ESCAPE", "K_EXCEPT", "K_EXCLUSIVE", 
			"K_EXISTS", "K_EXPLAIN", "K_FAIL", "K_FOR", "K_FOREIGN", "K_FROM", "K_FULL", 
			"K_GLOB", "K_GROUP", "K_HAVING", "K_IF", "K_IGNORE", "K_IMMEDIATE", "K_IN", 
			"K_INDEX", "K_INDEXED", "K_INITIALLY", "K_INNER", "K_INSERT", "K_INSTEAD", 
			"K_INTERSECT", "K_INTO", "K_IS", "K_ISNULL", "K_JOIN", "K_KEY", "K_LEFT", 
			"K_LIKE", "K_LIMIT", "K_MATCH", "K_NATURAL", "K_NEXTVAL", "K_NO", "K_NOT", 
			"K_NOTNULL", "K_NULL", "K_OF", "K_OFFSET", "K_ON", "K_ONLY", "K_OR", 
			"K_ORDER", "K_OUTER", "K_PLAN", "K_PRAGMA", "K_PRIMARY", "K_QUERY", "K_RAISE", 
			"K_RECURSIVE", "K_REFERENCES", "K_REGEXP", "K_REINDEX", "K_RELEASE", 
			"K_RENAME", "K_REPLACE", "K_RESTRICT", "K_RIGHT", "K_ROLLBACK", "K_ROW", 
			"K_SAVEPOINT", "K_SELECT", "K_SET", "K_TABLE", "K_TYPE", "K_PATH", "K_TEMP", 
			"K_TEMPORARY", "K_THEN", "K_TO", "K_TRANSACTION", "K_TRIGGER", "K_UNION", 
			"K_UNIQUE", "K_UPDATE", "K_USING", "K_VACUUM", "K_VALUES", "K_VIEW", 
			"K_VIRTUAL", "K_WHEN", "K_WHERE", "K_WITH", "K_WITHOUT", "K_SWITCH", 
			"K_RETURN", "K_VAR", "K_PRINT", "K_TRUE", "K_FALSE", "K_WHILE", "K_DO", 
			"K_BREAK", "K_FUNCTION", "IDENTIFIER", "IDENTIFIER2", "NUMERIC_LITERAL", 
			"BIND_PARAMETER", "STRING_LITERAL", "BLOB_LITERAL", "SINGLE_LINE_COMMENT", 
			"MULTILINE_COMMENT", "SPACES", "UNEXPECTED_CHAR", "DIGIT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "SQL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SQLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ParseContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(SQLParser.EOF, 0); }
		public List<FunctionContext> function() {
			return getRuleContexts(FunctionContext.class);
		}
		public FunctionContext function(int i) {
			return getRuleContext(FunctionContext.class,i);
		}
		public List<Sql_stmt_listContext> sql_stmt_list() {
			return getRuleContexts(Sql_stmt_listContext.class);
		}
		public Sql_stmt_listContext sql_stmt_list(int i) {
			return getRuleContext(Sql_stmt_listContext.class,i);
		}
		public List<ErrorContext> error() {
			return getRuleContexts(ErrorContext.class);
		}
		public ErrorContext error(int i) {
			return getRuleContext(ErrorContext.class,i);
		}
		public ParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitParse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitParse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParseContext parse() throws RecognitionException {
		ParseContext _localctx = new ParseContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_parse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(297);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SCOL || _la==K_ALTER || ((((_la - 70)) & ~0x3f) == 0 && ((1L << (_la - 70)) & ((1L << (K_CREATE - 70)) | (1L << (K_DELETE - 70)) | (1L << (K_DROP - 70)) | (1L << (K_INSERT - 70)))) != 0) || ((((_la - 151)) & ~0x3f) == 0 && ((1L << (_la - 151)) & ((1L << (K_SELECT - 151)) | (1L << (K_UPDATE - 151)) | (1L << (K_VALUES - 151)) | (1L << (IDENTIFIER - 151)) | (1L << (UNEXPECTED_CHAR - 151)))) != 0)) {
				{
				setState(295);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case IDENTIFIER:
					{
					setState(292);
					function();
					}
					break;
				case SCOL:
				case K_ALTER:
				case K_CREATE:
				case K_DELETE:
				case K_DROP:
				case K_INSERT:
				case K_SELECT:
				case K_UPDATE:
				case K_VALUES:
					{
					setState(293);
					sql_stmt_list();
					}
					break;
				case UNEXPECTED_CHAR:
					{
					setState(294);
					error();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(299);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(300);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ErrorContext extends ParserRuleContext {
		public Token UNEXPECTED_CHAR;
		public TerminalNode UNEXPECTED_CHAR() { return getToken(SQLParser.UNEXPECTED_CHAR, 0); }
		public ErrorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_error; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterError(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitError(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitError(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ErrorContext error() throws RecognitionException {
		ErrorContext _localctx = new ErrorContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_error);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(302);
			((ErrorContext)_localctx).UNEXPECTED_CHAR = match(UNEXPECTED_CHAR);

			     throw new RuntimeException("UNEXPECTED_CHAR=" + (((ErrorContext)_localctx).UNEXPECTED_CHAR!=null?((ErrorContext)_localctx).UNEXPECTED_CHAR.getText():null));
			   
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmt_listContext extends ParserRuleContext {
		public List<Sql_stmtContext> sql_stmt() {
			return getRuleContexts(Sql_stmtContext.class);
		}
		public Sql_stmtContext sql_stmt(int i) {
			return getRuleContext(Sql_stmtContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SQLParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SQLParser.SCOL, i);
		}
		public Sql_stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSql_stmt_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSql_stmt_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSql_stmt_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmt_listContext sql_stmt_list() throws RecognitionException {
		Sql_stmt_listContext _localctx = new Sql_stmt_listContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_sql_stmt_list);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(308);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SCOL) {
				{
				{
				setState(305);
				match(SCOL);
				}
				}
				setState(310);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(311);
			sql_stmt();
			setState(320);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(313); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(312);
						match(SCOL);
						}
						}
						setState(315); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==SCOL );
					setState(317);
					sql_stmt();
					}
					} 
				}
				setState(322);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			setState(326);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(323);
					match(SCOL);
					}
					} 
				}
				setState(328);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmtContext extends ParserRuleContext {
		public Alter_table_stmtContext alter_table_stmt() {
			return getRuleContext(Alter_table_stmtContext.class,0);
		}
		public Create_table_stmtContext create_table_stmt() {
			return getRuleContext(Create_table_stmtContext.class,0);
		}
		public Delete_stmtContext delete_stmt() {
			return getRuleContext(Delete_stmtContext.class,0);
		}
		public Drop_table_stmtContext drop_table_stmt() {
			return getRuleContext(Drop_table_stmtContext.class,0);
		}
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public Insert_stmtContext insert_stmt() {
			return getRuleContext(Insert_stmtContext.class,0);
		}
		public Update_stmtContext update_stmt() {
			return getRuleContext(Update_stmtContext.class,0);
		}
		public Create_aggregation_functionContext create_aggregation_function() {
			return getRuleContext(Create_aggregation_functionContext.class,0);
		}
		public Create_type_stmtContext create_type_stmt() {
			return getRuleContext(Create_type_stmtContext.class,0);
		}
		public Sql_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSql_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSql_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSql_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmtContext sql_stmt() throws RecognitionException {
		Sql_stmtContext _localctx = new Sql_stmtContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_sql_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(338);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(329);
				alter_table_stmt();
				}
				break;
			case 2:
				{
				setState(330);
				create_table_stmt();
				}
				break;
			case 3:
				{
				setState(331);
				delete_stmt();
				}
				break;
			case 4:
				{
				setState(332);
				drop_table_stmt();
				}
				break;
			case 5:
				{
				setState(333);
				factored_select_stmt();
				}
				break;
			case 6:
				{
				setState(334);
				insert_stmt();
				}
				break;
			case 7:
				{
				setState(335);
				update_stmt();
				}
				break;
			case 8:
				{
				setState(336);
				create_aggregation_function();
				}
				break;
			case 9:
				{
				setState(337);
				create_type_stmt();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alter_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_ALTER() { return getToken(SQLParser.K_ALTER, 0); }
		public TerminalNode K_TABLE() { return getToken(SQLParser.K_TABLE, 0); }
		public Source_table_nameContext source_table_name() {
			return getRuleContext(Source_table_nameContext.class,0);
		}
		public TerminalNode K_RENAME() { return getToken(SQLParser.K_RENAME, 0); }
		public TerminalNode K_TO() { return getToken(SQLParser.K_TO, 0); }
		public New_table_nameContext new_table_name() {
			return getRuleContext(New_table_nameContext.class,0);
		}
		public Alter_table_addContext alter_table_add() {
			return getRuleContext(Alter_table_addContext.class,0);
		}
		public Alter_table_add_constraintContext alter_table_add_constraint() {
			return getRuleContext(Alter_table_add_constraintContext.class,0);
		}
		public TerminalNode K_ADD() { return getToken(SQLParser.K_ADD, 0); }
		public Column_defContext column_def() {
			return getRuleContext(Column_defContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public TerminalNode K_COLUMN() { return getToken(SQLParser.K_COLUMN, 0); }
		public Alter_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alter_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAlter_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAlter_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAlter_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Alter_table_stmtContext alter_table_stmt() throws RecognitionException {
		Alter_table_stmtContext _localctx = new Alter_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_alter_table_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(340);
			match(K_ALTER);
			setState(341);
			match(K_TABLE);
			setState(345);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				{
				setState(342);
				database_name();
				setState(343);
				match(DOT);
				}
				break;
			}
			setState(347);
			source_table_name();
			setState(358);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				{
				setState(348);
				match(K_RENAME);
				setState(349);
				match(K_TO);
				setState(350);
				new_table_name();
				}
				break;
			case 2:
				{
				setState(351);
				alter_table_add();
				}
				break;
			case 3:
				{
				setState(352);
				alter_table_add_constraint();
				}
				break;
			case 4:
				{
				setState(353);
				match(K_ADD);
				setState(355);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_COLUMN) {
					{
					setState(354);
					match(K_COLUMN);
					}
				}

				setState(357);
				column_def();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alter_table_add_constraintContext extends ParserRuleContext {
		public TerminalNode K_ADD() { return getToken(SQLParser.K_ADD, 0); }
		public TerminalNode K_CONSTRAINT() { return getToken(SQLParser.K_CONSTRAINT, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_constraintContext table_constraint() {
			return getRuleContext(Table_constraintContext.class,0);
		}
		public Alter_table_add_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alter_table_add_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAlter_table_add_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAlter_table_add_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAlter_table_add_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Alter_table_add_constraintContext alter_table_add_constraint() throws RecognitionException {
		Alter_table_add_constraintContext _localctx = new Alter_table_add_constraintContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_alter_table_add_constraint);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(360);
			match(K_ADD);
			setState(361);
			match(K_CONSTRAINT);
			setState(362);
			any_name();
			setState(363);
			table_constraint();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alter_table_addContext extends ParserRuleContext {
		public TerminalNode K_ADD() { return getToken(SQLParser.K_ADD, 0); }
		public Table_constraintContext table_constraint() {
			return getRuleContext(Table_constraintContext.class,0);
		}
		public Alter_table_addContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alter_table_add; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAlter_table_add(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAlter_table_add(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAlter_table_add(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Alter_table_addContext alter_table_add() throws RecognitionException {
		Alter_table_addContext _localctx = new Alter_table_addContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_alter_table_add);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(365);
			match(K_ADD);
			setState(366);
			table_constraint();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SQLParser.K_CREATE, 0); }
		public TerminalNode K_TABLE() { return getToken(SQLParser.K_TABLE, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Column_defContext> column_def() {
			return getRuleContexts(Column_defContext.class);
		}
		public Column_defContext column_def(int i) {
			return getRuleContext(Column_defContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_AS() { return getToken(SQLParser.K_AS, 0); }
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public List<Table_constraintContext> table_constraint() {
			return getRuleContexts(Table_constraintContext.class);
		}
		public Table_constraintContext table_constraint(int i) {
			return getRuleContext(Table_constraintContext.class,i);
		}
		public Type_create_tableContext type_create_table() {
			return getRuleContext(Type_create_tableContext.class,0);
		}
		public Path_create_tableContext path_create_table() {
			return getRuleContext(Path_create_tableContext.class,0);
		}
		public Create_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreate_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreate_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCreate_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_table_stmtContext create_table_stmt() throws RecognitionException {
		Create_table_stmtContext _localctx = new Create_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_create_table_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(368);
			match(K_CREATE);
			setState(369);
			match(K_TABLE);
			setState(373);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_IF) {
				{
				setState(370);
				match(K_IF);
				setState(371);
				match(K_NOT);
				setState(372);
				match(K_EXISTS);
				}
			}

			setState(378);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(375);
				database_name();
				setState(376);
				match(DOT);
				}
				break;
			}
			setState(380);
			table_name();
			setState(401);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
				{
				setState(381);
				match(OPEN_PAR);
				setState(382);
				column_def();
				setState(389);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					setState(387);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						setState(383);
						match(COMMA);
						setState(384);
						table_constraint();
						}
						break;
					case 2:
						{
						setState(385);
						match(COMMA);
						setState(386);
						column_def();
						}
						break;
					}
					}
					setState(391);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(392);
				match(CLOSE_PAR);
				setState(394);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_TYPE) {
					{
					setState(393);
					type_create_table();
					}
				}

				setState(397);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_PATH) {
					{
					setState(396);
					path_create_table();
					}
				}

				}
				break;
			case K_AS:
				{
				setState(399);
				match(K_AS);
				setState(400);
				select_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_create_tableContext extends ParserRuleContext {
		public TerminalNode K_TYPE() { return getToken(SQLParser.K_TYPE, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Type_create_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_create_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterType_create_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitType_create_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitType_create_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_create_tableContext type_create_table() throws RecognitionException {
		Type_create_tableContext _localctx = new Type_create_tableContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_type_create_table);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(403);
			match(K_TYPE);
			setState(404);
			match(ASSIGN);
			setState(405);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Path_create_tableContext extends ParserRuleContext {
		public TerminalNode K_PATH() { return getToken(SQLParser.K_PATH, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Path_create_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_path_create_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterPath_create_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitPath_create_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitPath_create_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Path_create_tableContext path_create_table() throws RecognitionException {
		Path_create_tableContext _localctx = new Path_create_tableContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_path_create_table);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(407);
			match(K_PATH);
			setState(408);
			match(ASSIGN);
			setState(409);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_aggregation_functionContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SQLParser.K_CREATE, 0); }
		public TerminalNode K_AGGREGATION_FUNC() { return getToken(SQLParser.K_AGGREGATION_FUNC, 0); }
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Jar_pathContext jar_path() {
			return getRuleContext(Jar_pathContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Class_nameContext class_name() {
			return getRuleContext(Class_nameContext.class,0);
		}
		public Method_nameContext method_name() {
			return getRuleContext(Method_nameContext.class,0);
		}
		public Return_typeContext return_type() {
			return getRuleContext(Return_typeContext.class,0);
		}
		public Schema_arrayContext schema_array() {
			return getRuleContext(Schema_arrayContext.class,0);
		}
		public Create_aggregation_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_aggregation_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreate_aggregation_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreate_aggregation_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCreate_aggregation_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_aggregation_functionContext create_aggregation_function() throws RecognitionException {
		Create_aggregation_functionContext _localctx = new Create_aggregation_functionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_create_aggregation_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(411);
			match(K_CREATE);
			setState(412);
			match(K_AGGREGATION_FUNC);
			setState(413);
			function_name();
			setState(414);
			match(OPEN_PAR);
			setState(425);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				setState(415);
				jar_path();
				setState(416);
				match(COMMA);
				setState(417);
				class_name();
				setState(418);
				match(COMMA);
				setState(419);
				method_name();
				setState(420);
				match(COMMA);
				setState(421);
				return_type();
				setState(422);
				match(COMMA);
				setState(423);
				schema_array();
				}
			}

			setState(427);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Jar_pathContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Jar_pathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jar_path; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJar_path(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJar_path(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJar_path(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Jar_pathContext jar_path() throws RecognitionException {
		Jar_pathContext _localctx = new Jar_pathContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_jar_path);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(429);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Class_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterClass_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitClass_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitClass_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_nameContext class_name() throws RecognitionException {
		Class_nameContext _localctx = new Class_nameContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_class_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(431);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_typeContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Return_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterReturn_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitReturn_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitReturn_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_typeContext return_type() throws RecognitionException {
		Return_typeContext _localctx = new Return_typeContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_return_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(433);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Method_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Method_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterMethod_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitMethod_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitMethod_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Method_nameContext method_name() throws RecognitionException {
		Method_nameContext _localctx = new Method_nameContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_method_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(435);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Delete_stmtContext extends ParserRuleContext {
		public TerminalNode K_DELETE() { return getToken(SQLParser.K_DELETE, 0); }
		public TerminalNode K_FROM() { return getToken(SQLParser.K_FROM, 0); }
		public Qualified_table_nameContext qualified_table_name() {
			return getRuleContext(Qualified_table_nameContext.class,0);
		}
		public TerminalNode K_WHERE() { return getToken(SQLParser.K_WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Delete_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_delete_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDelete_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDelete_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDelete_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Delete_stmtContext delete_stmt() throws RecognitionException {
		Delete_stmtContext _localctx = new Delete_stmtContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_delete_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(437);
			match(K_DELETE);
			setState(438);
			match(K_FROM);
			setState(439);
			qualified_table_name();
			setState(442);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_WHERE) {
				{
				setState(440);
				match(K_WHERE);
				setState(441);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Drop_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_DROP() { return getToken(SQLParser.K_DROP, 0); }
		public TerminalNode K_TABLE() { return getToken(SQLParser.K_TABLE, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public Drop_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_drop_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDrop_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDrop_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDrop_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Drop_table_stmtContext drop_table_stmt() throws RecognitionException {
		Drop_table_stmtContext _localctx = new Drop_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_drop_table_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(444);
			match(K_DROP);
			setState(445);
			match(K_TABLE);
			setState(448);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_IF) {
				{
				setState(446);
				match(K_IF);
				setState(447);
				match(K_EXISTS);
				}
			}

			setState(453);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				setState(450);
				database_name();
				setState(451);
				match(DOT);
				}
				break;
			}
			setState(455);
			table_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Factored_select_stmtContext extends ParserRuleContext {
		public Select_coreContext select_core() {
			return getRuleContext(Select_coreContext.class,0);
		}
		public TerminalNode K_ORDER() { return getToken(SQLParser.K_ORDER, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public List<Ordering_termContext> ordering_term() {
			return getRuleContexts(Ordering_termContext.class);
		}
		public Ordering_termContext ordering_term(int i) {
			return getRuleContext(Ordering_termContext.class,i);
		}
		public TerminalNode K_LIMIT() { return getToken(SQLParser.K_LIMIT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public TerminalNode K_OFFSET() { return getToken(SQLParser.K_OFFSET, 0); }
		public Factored_select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factored_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFactored_select_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFactored_select_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFactored_select_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Factored_select_stmtContext factored_select_stmt() throws RecognitionException {
		Factored_select_stmtContext _localctx = new Factored_select_stmtContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_factored_select_stmt);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(457);
			select_core();
			setState(468);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ORDER) {
				{
				setState(458);
				match(K_ORDER);
				setState(459);
				match(K_BY);
				setState(460);
				ordering_term();
				setState(465);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(461);
						match(COMMA);
						setState(462);
						ordering_term();
						}
						} 
					}
					setState(467);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
				}
				}
			}

			setState(476);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_LIMIT) {
				{
				setState(470);
				match(K_LIMIT);
				setState(471);
				expr(0);
				setState(474);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,23,_ctx) ) {
				case 1:
					{
					setState(472);
					_la = _input.LA(1);
					if ( !(_la==COMMA || _la==K_OFFSET) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(473);
					expr(0);
					}
					break;
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assigne_selectContext extends ParserRuleContext {
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public Declare_arrayContext declare_array() {
			return getRuleContext(Declare_arrayContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public Assigne_selectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assigne_select; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAssigne_select(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAssigne_select(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAssigne_select(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assigne_selectContext assigne_select() throws RecognitionException {
		Assigne_selectContext _localctx = new Assigne_selectContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_assigne_select);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(483);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				{
				{
				setState(479);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_VAR) {
					{
					setState(478);
					match(K_VAR);
					}
				}

				setState(481);
				match(IDENTIFIER);
				}
				}
				break;
			case 2:
				{
				setState(482);
				declare_array();
				}
				break;
			}
			setState(485);
			match(ASSIGN);
			setState(486);
			factored_select_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Insert_stmtContext extends ParserRuleContext {
		public TerminalNode K_INSERT() { return getToken(SQLParser.K_INSERT, 0); }
		public TerminalNode K_INTO() { return getToken(SQLParser.K_INTO, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode K_VALUES() { return getToken(SQLParser.K_VALUES, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(SQLParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SQLParser.OPEN_PAR, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SQLParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SQLParser.CLOSE_PAR, i);
		}
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public TerminalNode K_DEFAULT() { return getToken(SQLParser.K_DEFAULT, 0); }
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Insert_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insert_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterInsert_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitInsert_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitInsert_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Insert_stmtContext insert_stmt() throws RecognitionException {
		Insert_stmtContext _localctx = new Insert_stmtContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_insert_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(488);
			match(K_INSERT);
			setState(489);
			match(K_INTO);
			setState(493);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				{
				setState(490);
				database_name();
				setState(491);
				match(DOT);
				}
				break;
			}
			setState(495);
			table_name();
			setState(507);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPEN_PAR) {
				{
				setState(496);
				match(OPEN_PAR);
				setState(497);
				column_name();
				setState(502);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(498);
					match(COMMA);
					setState(499);
					column_name();
					}
					}
					setState(504);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(505);
				match(CLOSE_PAR);
				}
			}

			setState(540);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				{
				setState(509);
				match(K_VALUES);
				setState(510);
				match(OPEN_PAR);
				setState(511);
				expr(0);
				setState(516);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(512);
					match(COMMA);
					setState(513);
					expr(0);
					}
					}
					setState(518);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(519);
				match(CLOSE_PAR);
				setState(534);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(520);
					match(COMMA);
					setState(521);
					match(OPEN_PAR);
					setState(522);
					expr(0);
					setState(527);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(523);
						match(COMMA);
						setState(524);
						expr(0);
						}
						}
						setState(529);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(530);
					match(CLOSE_PAR);
					}
					}
					setState(536);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case 2:
				{
				setState(537);
				factored_select_stmt();
				}
				break;
			case 3:
				{
				setState(538);
				match(K_DEFAULT);
				setState(539);
				match(K_VALUES);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_stmtContext extends ParserRuleContext {
		public Select_or_valuesContext select_or_values() {
			return getRuleContext(Select_or_valuesContext.class,0);
		}
		public TerminalNode K_ORDER() { return getToken(SQLParser.K_ORDER, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public List<Ordering_termContext> ordering_term() {
			return getRuleContexts(Ordering_termContext.class);
		}
		public Ordering_termContext ordering_term(int i) {
			return getRuleContext(Ordering_termContext.class,i);
		}
		public TerminalNode K_LIMIT() { return getToken(SQLParser.K_LIMIT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public TerminalNode K_OFFSET() { return getToken(SQLParser.K_OFFSET, 0); }
		public Select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSelect_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSelect_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSelect_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_stmtContext select_stmt() throws RecognitionException {
		Select_stmtContext _localctx = new Select_stmtContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_select_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(542);
			select_or_values();
			setState(553);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ORDER) {
				{
				setState(543);
				match(K_ORDER);
				setState(544);
				match(K_BY);
				setState(545);
				ordering_term();
				setState(550);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(546);
					match(COMMA);
					setState(547);
					ordering_term();
					}
					}
					setState(552);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(561);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_LIMIT) {
				{
				setState(555);
				match(K_LIMIT);
				setState(556);
				expr(0);
				setState(559);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COMMA || _la==K_OFFSET) {
					{
					setState(557);
					_la = _input.LA(1);
					if ( !(_la==COMMA || _la==K_OFFSET) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(558);
					expr(0);
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_or_valuesContext extends ParserRuleContext {
		public TerminalNode K_SELECT() { return getToken(SQLParser.K_SELECT, 0); }
		public List<Result_columnContext> result_column() {
			return getRuleContexts(Result_columnContext.class);
		}
		public Result_columnContext result_column(int i) {
			return getRuleContext(Result_columnContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public TerminalNode K_FROM() { return getToken(SQLParser.K_FROM, 0); }
		public TerminalNode K_WHERE() { return getToken(SQLParser.K_WHERE, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode K_GROUP() { return getToken(SQLParser.K_GROUP, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public TerminalNode K_ALL() { return getToken(SQLParser.K_ALL, 0); }
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public Join_clauseContext join_clause() {
			return getRuleContext(Join_clauseContext.class,0);
		}
		public TerminalNode K_HAVING() { return getToken(SQLParser.K_HAVING, 0); }
		public TerminalNode K_VALUES() { return getToken(SQLParser.K_VALUES, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(SQLParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SQLParser.OPEN_PAR, i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SQLParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SQLParser.CLOSE_PAR, i);
		}
		public Select_or_valuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_or_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSelect_or_values(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSelect_or_values(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSelect_or_values(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_or_valuesContext select_or_values() throws RecognitionException {
		Select_or_valuesContext _localctx = new Select_or_valuesContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_select_or_values);
		int _la;
		try {
			setState(637);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_SELECT:
				enterOuterAlt(_localctx, 1);
				{
				setState(563);
				match(K_SELECT);
				setState(565);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_ALL || _la==K_DISTINCT) {
					{
					setState(564);
					_la = _input.LA(1);
					if ( !(_la==K_ALL || _la==K_DISTINCT) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(567);
				result_column();
				setState(572);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(568);
					match(COMMA);
					setState(569);
					result_column();
					}
					}
					setState(574);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(587);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_FROM) {
					{
					setState(575);
					match(K_FROM);
					setState(585);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
					case 1:
						{
						setState(576);
						table_or_subquery();
						setState(581);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(577);
							match(COMMA);
							setState(578);
							table_or_subquery();
							}
							}
							setState(583);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
						break;
					case 2:
						{
						setState(584);
						join_clause();
						}
						break;
					}
					}
				}

				setState(591);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_WHERE) {
					{
					setState(589);
					match(K_WHERE);
					setState(590);
					expr(0);
					}
				}

				setState(607);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_GROUP) {
					{
					setState(593);
					match(K_GROUP);
					setState(594);
					match(K_BY);
					setState(595);
					expr(0);
					setState(600);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(596);
						match(COMMA);
						setState(597);
						expr(0);
						}
						}
						setState(602);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(605);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_HAVING) {
						{
						setState(603);
						match(K_HAVING);
						setState(604);
						expr(0);
						}
					}

					}
				}

				}
				break;
			case K_VALUES:
				enterOuterAlt(_localctx, 2);
				{
				setState(609);
				match(K_VALUES);
				setState(610);
				match(OPEN_PAR);
				setState(611);
				expr(0);
				setState(616);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(612);
					match(COMMA);
					setState(613);
					expr(0);
					}
					}
					setState(618);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(619);
				match(CLOSE_PAR);
				setState(634);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(620);
					match(COMMA);
					setState(621);
					match(OPEN_PAR);
					setState(622);
					expr(0);
					setState(627);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(623);
						match(COMMA);
						setState(624);
						expr(0);
						}
						}
						setState(629);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(630);
					match(CLOSE_PAR);
					}
					}
					setState(636);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Update_stmtContext extends ParserRuleContext {
		public TerminalNode K_UPDATE() { return getToken(SQLParser.K_UPDATE, 0); }
		public Qualified_table_nameContext qualified_table_name() {
			return getRuleContext(Qualified_table_nameContext.class,0);
		}
		public TerminalNode K_SET() { return getToken(SQLParser.K_SET, 0); }
		public Update_first_parameterContext update_first_parameter() {
			return getRuleContext(Update_first_parameterContext.class,0);
		}
		public Update_second_parameterContext update_second_parameter() {
			return getRuleContext(Update_second_parameterContext.class,0);
		}
		public TerminalNode K_WHERE() { return getToken(SQLParser.K_WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Update_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_update_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUpdate_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUpdate_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUpdate_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Update_stmtContext update_stmt() throws RecognitionException {
		Update_stmtContext _localctx = new Update_stmtContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_update_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(639);
			match(K_UPDATE);
			setState(640);
			qualified_table_name();
			setState(641);
			match(K_SET);
			setState(642);
			update_first_parameter();
			setState(643);
			update_second_parameter();
			setState(646);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_WHERE) {
				{
				setState(644);
				match(K_WHERE);
				setState(645);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Update_first_parameterContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Update_first_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_update_first_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUpdate_first_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUpdate_first_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUpdate_first_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Update_first_parameterContext update_first_parameter() throws RecognitionException {
		Update_first_parameterContext _localctx = new Update_first_parameterContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_update_first_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(648);
			column_name();
			setState(649);
			match(ASSIGN);
			setState(650);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Update_second_parameterContext extends ParserRuleContext {
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public List<TerminalNode> ASSIGN() { return getTokens(SQLParser.ASSIGN); }
		public TerminalNode ASSIGN(int i) {
			return getToken(SQLParser.ASSIGN, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Update_second_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_update_second_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUpdate_second_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUpdate_second_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUpdate_second_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Update_second_parameterContext update_second_parameter() throws RecognitionException {
		Update_second_parameterContext _localctx = new Update_second_parameterContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_update_second_parameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(659);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(652);
				match(COMMA);
				setState(653);
				column_name();
				setState(654);
				match(ASSIGN);
				setState(655);
				expr(0);
				}
				}
				setState(661);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_defContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public List<Column_constraintContext> column_constraint() {
			return getRuleContexts(Column_constraintContext.class);
		}
		public Column_constraintContext column_constraint(int i) {
			return getRuleContext(Column_constraintContext.class,i);
		}
		public List<Type_nameContext> type_name() {
			return getRuleContexts(Type_nameContext.class);
		}
		public Type_nameContext type_name(int i) {
			return getRuleContext(Type_nameContext.class,i);
		}
		public Column_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_defContext column_def() throws RecognitionException {
		Column_defContext _localctx = new Column_defContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_column_def);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(662);
			column_name();
			setState(667);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(665);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case K_CHECK:
					case K_COLLATE:
					case K_CONSTRAINT:
					case K_DEFAULT:
					case K_NOT:
					case K_NULL:
					case K_PRIMARY:
					case K_REFERENCES:
					case K_UNIQUE:
						{
						setState(663);
						column_constraint();
						}
						break;
					case OPEN_PAR:
					case IDENTIFIER:
					case STRING_LITERAL:
						{
						setState(664);
						type_name();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(669);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Signed_numberContext> signed_number() {
			return getRuleContexts(Signed_numberContext.class);
		}
		public Signed_numberContext signed_number(int i) {
			return getRuleContext(Signed_numberContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode COMMA() { return getToken(SQLParser.COMMA, 0); }
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterType_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitType_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitType_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_nameContext type_name() throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_type_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(670);
			name();
			setState(690);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,58,_ctx) ) {
			case 1:
				{
				setState(671);
				match(OPEN_PAR);
				setState(672);
				signed_number();
				setState(674);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==OPEN_PAR || _la==IDENTIFIER || _la==STRING_LITERAL) {
					{
					setState(673);
					any_name();
					}
				}

				setState(676);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(678);
				match(OPEN_PAR);
				setState(679);
				signed_number();
				setState(681);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==OPEN_PAR || _la==IDENTIFIER || _la==STRING_LITERAL) {
					{
					setState(680);
					any_name();
					}
				}

				setState(683);
				match(COMMA);
				setState(684);
				signed_number();
				setState(686);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==OPEN_PAR || _la==IDENTIFIER || _la==STRING_LITERAL) {
					{
					setState(685);
					any_name();
					}
				}

				setState(688);
				match(CLOSE_PAR);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraintContext extends ParserRuleContext {
		public Column_constraint_primary_keyContext column_constraint_primary_key() {
			return getRuleContext(Column_constraint_primary_keyContext.class,0);
		}
		public Column_constraint_foreign_keyContext column_constraint_foreign_key() {
			return getRuleContext(Column_constraint_foreign_keyContext.class,0);
		}
		public Column_constraint_not_nullContext column_constraint_not_null() {
			return getRuleContext(Column_constraint_not_nullContext.class,0);
		}
		public Column_constraint_nullContext column_constraint_null() {
			return getRuleContext(Column_constraint_nullContext.class,0);
		}
		public TerminalNode K_UNIQUE() { return getToken(SQLParser.K_UNIQUE, 0); }
		public TerminalNode K_CHECK() { return getToken(SQLParser.K_CHECK, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Column_defaultContext column_default() {
			return getRuleContext(Column_defaultContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(SQLParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_CONSTRAINT() { return getToken(SQLParser.K_CONSTRAINT, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Column_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraintContext column_constraint() throws RecognitionException {
		Column_constraintContext _localctx = new Column_constraintContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_column_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(694);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_CONSTRAINT) {
				{
				setState(692);
				match(K_CONSTRAINT);
				setState(693);
				name();
				}
			}

			setState(709);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_PRIMARY:
				{
				setState(696);
				column_constraint_primary_key();
				}
				break;
			case K_REFERENCES:
				{
				setState(697);
				column_constraint_foreign_key();
				}
				break;
			case K_NOT:
				{
				setState(698);
				column_constraint_not_null();
				}
				break;
			case K_NULL:
				{
				setState(699);
				column_constraint_null();
				}
				break;
			case K_UNIQUE:
				{
				setState(700);
				match(K_UNIQUE);
				}
				break;
			case K_CHECK:
				{
				setState(701);
				match(K_CHECK);
				setState(702);
				match(OPEN_PAR);
				setState(703);
				expr(0);
				setState(704);
				match(CLOSE_PAR);
				}
				break;
			case K_DEFAULT:
				{
				setState(706);
				column_default();
				}
				break;
			case K_COLLATE:
				{
				setState(707);
				match(K_COLLATE);
				setState(708);
				collation_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_primary_keyContext extends ParserRuleContext {
		public TerminalNode K_PRIMARY() { return getToken(SQLParser.K_PRIMARY, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode K_AUTOINCREMENT() { return getToken(SQLParser.K_AUTOINCREMENT, 0); }
		public TerminalNode K_ASC() { return getToken(SQLParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(SQLParser.K_DESC, 0); }
		public Column_constraint_primary_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_primary_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_constraint_primary_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_constraint_primary_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_constraint_primary_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_primary_keyContext column_constraint_primary_key() throws RecognitionException {
		Column_constraint_primary_keyContext _localctx = new Column_constraint_primary_keyContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_column_constraint_primary_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(711);
			match(K_PRIMARY);
			setState(712);
			match(K_KEY);
			setState(714);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(713);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(717);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_AUTOINCREMENT) {
				{
				setState(716);
				match(K_AUTOINCREMENT);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_foreign_keyContext extends ParserRuleContext {
		public Foreign_key_clauseContext foreign_key_clause() {
			return getRuleContext(Foreign_key_clauseContext.class,0);
		}
		public Column_constraint_foreign_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_foreign_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_constraint_foreign_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_constraint_foreign_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_constraint_foreign_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_foreign_keyContext column_constraint_foreign_key() throws RecognitionException {
		Column_constraint_foreign_keyContext _localctx = new Column_constraint_foreign_keyContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_column_constraint_foreign_key);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(719);
			foreign_key_clause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_not_nullContext extends ParserRuleContext {
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public Column_constraint_not_nullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_not_null; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_constraint_not_null(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_constraint_not_null(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_constraint_not_null(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_not_nullContext column_constraint_not_null() throws RecognitionException {
		Column_constraint_not_nullContext _localctx = new Column_constraint_not_nullContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_column_constraint_not_null);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(721);
			match(K_NOT);
			setState(722);
			match(K_NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_nullContext extends ParserRuleContext {
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public Column_constraint_nullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_null; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_constraint_null(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_constraint_null(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_constraint_null(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_nullContext column_constraint_null() throws RecognitionException {
		Column_constraint_nullContext _localctx = new Column_constraint_nullContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_column_constraint_null);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(724);
			match(K_NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_defaultContext extends ParserRuleContext {
		public TerminalNode K_DEFAULT() { return getToken(SQLParser.K_DEFAULT, 0); }
		public Column_default_valueContext column_default_value() {
			return getRuleContext(Column_default_valueContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_NEXTVAL() { return getToken(SQLParser.K_NEXTVAL, 0); }
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public Column_defaultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_default; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_default(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_default(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_default(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_defaultContext column_default() throws RecognitionException {
		Column_defaultContext _localctx = new Column_defaultContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_column_default);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(726);
			match(K_DEFAULT);
			setState(738);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,63,_ctx) ) {
			case 1:
				{
				setState(727);
				column_default_value();
				}
				break;
			case 2:
				{
				setState(728);
				match(OPEN_PAR);
				setState(729);
				expr(0);
				setState(730);
				match(CLOSE_PAR);
				}
				break;
			case 3:
				{
				setState(732);
				match(K_NEXTVAL);
				setState(733);
				match(OPEN_PAR);
				setState(734);
				expr(0);
				setState(735);
				match(CLOSE_PAR);
				}
				break;
			case 4:
				{
				setState(737);
				any_name();
				}
				break;
			}
			setState(746);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(740);
				match(T__0);
				setState(742); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(741);
						any_name();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(744); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,64,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_default_valueContext extends ParserRuleContext {
		public Signed_numberContext signed_number() {
			return getRuleContext(Signed_numberContext.class,0);
		}
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Column_default_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_default_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_default_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_default_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_default_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_default_valueContext column_default_value() throws RecognitionException {
		Column_default_valueContext _localctx = new Column_default_valueContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_column_default_value);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(750);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,66,_ctx) ) {
			case 1:
				{
				setState(748);
				signed_number();
				}
				break;
			case 2:
				{
				setState(749);
				literal_value();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Array_assigneContext array_assigne() {
			return getRuleContext(Array_assigneContext.class,0);
		}
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public List<TerminalNode> DOT() { return getTokens(SQLParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(SQLParser.DOT, i);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Call_funcContext call_func() {
			return getRuleContext(Call_funcContext.class,0);
		}
		public Bool_exContext bool_ex() {
			return getRuleContext(Bool_exContext.class,0);
		}
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Incr_decr_without_commaContext incr_decr_without_comma() {
			return getRuleContext(Incr_decr_without_commaContext.class,0);
		}
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode PIPE2() { return getToken(SQLParser.PIPE2, 0); }
		public TerminalNode PLUS_EQ() { return getToken(SQLParser.PLUS_EQ, 0); }
		public TerminalNode MINUS_EQ() { return getToken(SQLParser.MINUS_EQ, 0); }
		public TerminalNode STAR_EQ() { return getToken(SQLParser.STAR_EQ, 0); }
		public TerminalNode DIV_EQ() { return getToken(SQLParser.DIV_EQ, 0); }
		public TerminalNode MOD_EQ() { return getToken(SQLParser.MOD_EQ, 0); }
		public TerminalNode DIV() { return getToken(SQLParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SQLParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public TerminalNode LT2() { return getToken(SQLParser.LT2, 0); }
		public TerminalNode GT2() { return getToken(SQLParser.GT2, 0); }
		public TerminalNode AMP() { return getToken(SQLParser.AMP, 0); }
		public TerminalNode PIPE() { return getToken(SQLParser.PIPE, 0); }
		public TerminalNode LT() { return getToken(SQLParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SQLParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SQLParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SQLParser.GT_EQ, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public TerminalNode EQ() { return getToken(SQLParser.EQ, 0); }
		public TerminalNode NOT_EQ1() { return getToken(SQLParser.NOT_EQ1, 0); }
		public TerminalNode NOT_EQ2() { return getToken(SQLParser.NOT_EQ2, 0); }
		public TerminalNode K_IS() { return getToken(SQLParser.K_IS, 0); }
		public TerminalNode K_LIKE() { return getToken(SQLParser.K_LIKE, 0); }
		public TerminalNode K_GLOB() { return getToken(SQLParser.K_GLOB, 0); }
		public TerminalNode K_MATCH() { return getToken(SQLParser.K_MATCH, 0); }
		public TerminalNode K_REGEXP() { return getToken(SQLParser.K_REGEXP, 0); }
		public TerminalNode K_AND() { return getToken(SQLParser.K_AND, 0); }
		public TerminalNode K_OR() { return getToken(SQLParser.K_OR, 0); }
		public TerminalNode K_IN() { return getToken(SQLParser.K_IN, 0); }
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 68;
		enterRecursionRule(_localctx, 68, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(804);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,74,_ctx) ) {
			case 1:
				{
				setState(753);
				literal_value();
				}
				break;
			case 2:
				{
				setState(754);
				array_assigne();
				}
				break;
			case 3:
				{
				setState(763);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,68,_ctx) ) {
				case 1:
					{
					setState(758);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,67,_ctx) ) {
					case 1:
						{
						setState(755);
						database_name();
						setState(756);
						match(DOT);
						}
						break;
					}
					setState(760);
					table_name();
					setState(761);
					match(DOT);
					}
					break;
				}
				setState(765);
				column_name();
				}
				break;
			case 4:
				{
				setState(766);
				unary_operator();
				setState(767);
				expr(17);
				}
				break;
			case 5:
				{
				setState(769);
				call_func();
				}
				break;
			case 6:
				{
				setState(770);
				bool_ex(0);
				}
				break;
			case 7:
				{
				setState(771);
				function_name();
				setState(772);
				match(OPEN_PAR);
				setState(785);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case PLUS_PLUSr:
				case MINUS_MINUSr:
				case OPEN_PAR:
				case PLUS:
				case MINUS:
				case TILDE:
				case K_CURRENT_DATE:
				case K_CURRENT_TIME:
				case K_CURRENT_TIMESTAMP:
				case K_DISTINCT:
				case K_EXISTS:
				case K_NOT:
				case K_NULL:
				case K_VAR:
				case K_TRUE:
				case K_FALSE:
				case IDENTIFIER:
				case NUMERIC_LITERAL:
				case STRING_LITERAL:
				case BLOB_LITERAL:
					{
					setState(774);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_DISTINCT) {
						{
						setState(773);
						match(K_DISTINCT);
						}
					}

					setState(776);
					expr(0);
					setState(781);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(777);
						match(COMMA);
						setState(778);
						expr(0);
						}
						}
						setState(783);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				case STAR:
					{
					setState(784);
					match(STAR);
					}
					break;
				case CLOSE_PAR:
					break;
				default:
					break;
				}
				setState(787);
				match(CLOSE_PAR);
				}
				break;
			case 8:
				{
				setState(789);
				match(OPEN_PAR);
				setState(790);
				expr(0);
				setState(791);
				match(CLOSE_PAR);
				}
				break;
			case 9:
				{
				setState(793);
				incr_decr_without_comma();
				}
				break;
			case 10:
				{
				setState(798);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_EXISTS || _la==K_NOT) {
					{
					setState(795);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_NOT) {
						{
						setState(794);
						match(K_NOT);
						}
					}

					setState(797);
					match(K_EXISTS);
					}
				}

				setState(800);
				match(OPEN_PAR);
				setState(801);
				factored_select_stmt();
				setState(802);
				match(CLOSE_PAR);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(873);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,82,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(871);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,81,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(806);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(807);
						match(PIPE2);
						setState(808);
						expr(15);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(809);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(810);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS_EQ) | (1L << MINUS_EQ) | (1L << STAR_EQ) | (1L << DIV_EQ) | (1L << MOD_EQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(811);
						expr(14);
						}
						break;
					case 3:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(812);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(813);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(814);
						expr(13);
						}
						break;
					case 4:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(815);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(816);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(817);
						expr(12);
						}
						break;
					case 5:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(818);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(819);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT2) | (1L << GT2) | (1L << AMP) | (1L << PIPE))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(820);
						expr(11);
						}
						break;
					case 6:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(821);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(822);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(823);
						expr(10);
						}
						break;
					case 7:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(824);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(836);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,75,_ctx) ) {
						case 1:
							{
							setState(825);
							match(ASSIGN);
							}
							break;
						case 2:
							{
							setState(826);
							match(EQ);
							}
							break;
						case 3:
							{
							setState(827);
							match(NOT_EQ1);
							}
							break;
						case 4:
							{
							setState(828);
							match(NOT_EQ2);
							}
							break;
						case 5:
							{
							setState(829);
							match(K_IS);
							}
							break;
						case 6:
							{
							setState(830);
							match(K_IS);
							setState(831);
							match(K_NOT);
							}
							break;
						case 7:
							{
							setState(832);
							match(K_LIKE);
							}
							break;
						case 8:
							{
							setState(833);
							match(K_GLOB);
							}
							break;
						case 9:
							{
							setState(834);
							match(K_MATCH);
							}
							break;
						case 10:
							{
							setState(835);
							match(K_REGEXP);
							}
							break;
						}
						setState(838);
						expr(9);
						}
						break;
					case 8:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(839);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(840);
						match(K_AND);
						setState(841);
						expr(8);
						}
						break;
					case 9:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(842);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(843);
						match(K_OR);
						setState(844);
						expr(7);
						}
						break;
					case 10:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(845);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(847);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==K_NOT) {
							{
							setState(846);
							match(K_NOT);
							}
						}

						setState(849);
						match(K_IN);
						setState(869);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,80,_ctx) ) {
						case 1:
							{
							setState(850);
							match(OPEN_PAR);
							setState(860);
							_errHandler.sync(this);
							switch (_input.LA(1)) {
							case K_SELECT:
							case K_VALUES:
								{
								setState(851);
								factored_select_stmt();
								}
								break;
							case PLUS_PLUSr:
							case MINUS_MINUSr:
							case OPEN_PAR:
							case PLUS:
							case MINUS:
							case TILDE:
							case K_CURRENT_DATE:
							case K_CURRENT_TIME:
							case K_CURRENT_TIMESTAMP:
							case K_EXISTS:
							case K_NOT:
							case K_NULL:
							case K_VAR:
							case K_TRUE:
							case K_FALSE:
							case IDENTIFIER:
							case NUMERIC_LITERAL:
							case STRING_LITERAL:
							case BLOB_LITERAL:
								{
								setState(852);
								expr(0);
								setState(857);
								_errHandler.sync(this);
								_la = _input.LA(1);
								while (_la==COMMA) {
									{
									{
									setState(853);
									match(COMMA);
									setState(854);
									expr(0);
									}
									}
									setState(859);
									_errHandler.sync(this);
									_la = _input.LA(1);
								}
								}
								break;
							case CLOSE_PAR:
								break;
							default:
								break;
							}
							setState(862);
							match(CLOSE_PAR);
							}
							break;
						case 2:
							{
							setState(866);
							_errHandler.sync(this);
							switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
							case 1:
								{
								setState(863);
								database_name();
								setState(864);
								match(DOT);
								}
								break;
							}
							setState(868);
							table_name();
							}
							break;
						}
						}
						break;
					}
					} 
				}
				setState(875);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,82,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Expr_testContext extends ParserRuleContext {
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Array_assigneContext array_assigne() {
			return getRuleContext(Array_assigneContext.class,0);
		}
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Call_funcContext call_func() {
			return getRuleContext(Call_funcContext.class,0);
		}
		public Bool_exContext bool_ex() {
			return getRuleContext(Bool_exContext.class,0);
		}
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public List<Expr_testContext> expr_test() {
			return getRuleContexts(Expr_testContext.class);
		}
		public Expr_testContext expr_test(int i) {
			return getRuleContext(Expr_testContext.class,i);
		}
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Incr_decr_without_commaContext incr_decr_without_comma() {
			return getRuleContext(Incr_decr_without_commaContext.class,0);
		}
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode PIPE2() { return getToken(SQLParser.PIPE2, 0); }
		public TerminalNode PLUS_EQ() { return getToken(SQLParser.PLUS_EQ, 0); }
		public TerminalNode MINUS_EQ() { return getToken(SQLParser.MINUS_EQ, 0); }
		public TerminalNode STAR_EQ() { return getToken(SQLParser.STAR_EQ, 0); }
		public TerminalNode DIV_EQ() { return getToken(SQLParser.DIV_EQ, 0); }
		public TerminalNode MOD_EQ() { return getToken(SQLParser.MOD_EQ, 0); }
		public TerminalNode DIV() { return getToken(SQLParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SQLParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public TerminalNode LT2() { return getToken(SQLParser.LT2, 0); }
		public TerminalNode GT2() { return getToken(SQLParser.GT2, 0); }
		public TerminalNode AMP() { return getToken(SQLParser.AMP, 0); }
		public TerminalNode PIPE() { return getToken(SQLParser.PIPE, 0); }
		public TerminalNode LT() { return getToken(SQLParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SQLParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SQLParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SQLParser.GT_EQ, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public TerminalNode EQ() { return getToken(SQLParser.EQ, 0); }
		public TerminalNode NOT_EQ1() { return getToken(SQLParser.NOT_EQ1, 0); }
		public TerminalNode NOT_EQ2() { return getToken(SQLParser.NOT_EQ2, 0); }
		public TerminalNode K_IS() { return getToken(SQLParser.K_IS, 0); }
		public TerminalNode K_IN() { return getToken(SQLParser.K_IN, 0); }
		public TerminalNode K_LIKE() { return getToken(SQLParser.K_LIKE, 0); }
		public TerminalNode K_GLOB() { return getToken(SQLParser.K_GLOB, 0); }
		public TerminalNode K_MATCH() { return getToken(SQLParser.K_MATCH, 0); }
		public TerminalNode K_REGEXP() { return getToken(SQLParser.K_REGEXP, 0); }
		public TerminalNode K_AND() { return getToken(SQLParser.K_AND, 0); }
		public TerminalNode K_OR() { return getToken(SQLParser.K_OR, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public Expr_testContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_test; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExpr_test(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExpr_test(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExpr_test(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_testContext expr_test() throws RecognitionException {
		return expr_test(0);
	}

	private Expr_testContext expr_test(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Expr_testContext _localctx = new Expr_testContext(_ctx, _parentState);
		Expr_testContext _prevctx = _localctx;
		int _startState = 70;
		enterRecursionRule(_localctx, 70, RULE_expr_test, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(919);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,88,_ctx) ) {
			case 1:
				{
				setState(877);
				literal_value();
				}
				break;
			case 2:
				{
				setState(878);
				match(IDENTIFIER);
				}
				break;
			case 3:
				{
				setState(879);
				array_assigne();
				}
				break;
			case 4:
				{
				setState(880);
				get_property_json();
				}
				break;
			case 5:
				{
				setState(881);
				unary_operator();
				setState(882);
				expr(0);
				}
				break;
			case 6:
				{
				setState(884);
				call_func();
				}
				break;
			case 7:
				{
				setState(885);
				bool_ex(0);
				}
				break;
			case 8:
				{
				setState(886);
				function_name();
				setState(887);
				match(OPEN_PAR);
				setState(900);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case PLUS_PLUSr:
				case MINUS_MINUSr:
				case OPEN_PAR:
				case PLUS:
				case MINUS:
				case TILDE:
				case K_CURRENT_DATE:
				case K_CURRENT_TIME:
				case K_CURRENT_TIMESTAMP:
				case K_DISTINCT:
				case K_EXISTS:
				case K_NOT:
				case K_NULL:
				case K_VAR:
				case K_TRUE:
				case K_FALSE:
				case IDENTIFIER:
				case NUMERIC_LITERAL:
				case STRING_LITERAL:
				case BLOB_LITERAL:
					{
					setState(889);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_DISTINCT) {
						{
						setState(888);
						match(K_DISTINCT);
						}
					}

					setState(891);
					expr_test(0);
					setState(896);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(892);
						match(COMMA);
						setState(893);
						expr_test(0);
						}
						}
						setState(898);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				case STAR:
					{
					setState(899);
					match(STAR);
					}
					break;
				case CLOSE_PAR:
					break;
				default:
					break;
				}
				setState(902);
				match(CLOSE_PAR);
				}
				break;
			case 9:
				{
				setState(904);
				match(OPEN_PAR);
				setState(905);
				expr_test(0);
				setState(906);
				match(CLOSE_PAR);
				}
				break;
			case 10:
				{
				setState(908);
				incr_decr_without_comma();
				}
				break;
			case 11:
				{
				setState(913);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_EXISTS || _la==K_NOT) {
					{
					setState(910);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_NOT) {
						{
						setState(909);
						match(K_NOT);
						}
					}

					setState(912);
					match(K_EXISTS);
					}
				}

				setState(915);
				match(OPEN_PAR);
				setState(916);
				factored_select_stmt();
				setState(917);
				match(CLOSE_PAR);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(989);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,96,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(987);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,95,_ctx) ) {
					case 1:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(921);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(922);
						match(PIPE2);
						setState(923);
						expr_test(15);
						}
						break;
					case 2:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(924);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(925);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS_EQ) | (1L << MINUS_EQ) | (1L << STAR_EQ) | (1L << DIV_EQ) | (1L << MOD_EQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(926);
						expr_test(14);
						}
						break;
					case 3:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(927);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(928);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(929);
						expr_test(13);
						}
						break;
					case 4:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(930);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(931);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(932);
						expr_test(12);
						}
						break;
					case 5:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(933);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(934);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT2) | (1L << GT2) | (1L << AMP) | (1L << PIPE))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(935);
						expr_test(11);
						}
						break;
					case 6:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(936);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(937);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(938);
						expr_test(10);
						}
						break;
					case 7:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(939);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(952);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,89,_ctx) ) {
						case 1:
							{
							setState(940);
							match(ASSIGN);
							}
							break;
						case 2:
							{
							setState(941);
							match(EQ);
							}
							break;
						case 3:
							{
							setState(942);
							match(NOT_EQ1);
							}
							break;
						case 4:
							{
							setState(943);
							match(NOT_EQ2);
							}
							break;
						case 5:
							{
							setState(944);
							match(K_IS);
							}
							break;
						case 6:
							{
							setState(945);
							match(K_IS);
							setState(946);
							match(K_NOT);
							}
							break;
						case 7:
							{
							setState(947);
							match(K_IN);
							}
							break;
						case 8:
							{
							setState(948);
							match(K_LIKE);
							}
							break;
						case 9:
							{
							setState(949);
							match(K_GLOB);
							}
							break;
						case 10:
							{
							setState(950);
							match(K_MATCH);
							}
							break;
						case 11:
							{
							setState(951);
							match(K_REGEXP);
							}
							break;
						}
						setState(954);
						expr_test(9);
						}
						break;
					case 8:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(955);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(956);
						match(K_AND);
						setState(957);
						expr_test(8);
						}
						break;
					case 9:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(958);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(959);
						match(K_OR);
						setState(960);
						expr_test(7);
						}
						break;
					case 10:
						{
						_localctx = new Expr_testContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_test);
						setState(961);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(963);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==K_NOT) {
							{
							setState(962);
							match(K_NOT);
							}
						}

						setState(965);
						match(K_IN);
						setState(985);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,94,_ctx) ) {
						case 1:
							{
							setState(966);
							match(OPEN_PAR);
							setState(976);
							_errHandler.sync(this);
							switch (_input.LA(1)) {
							case K_SELECT:
							case K_VALUES:
								{
								setState(967);
								factored_select_stmt();
								}
								break;
							case PLUS_PLUSr:
							case MINUS_MINUSr:
							case OPEN_PAR:
							case PLUS:
							case MINUS:
							case TILDE:
							case K_CURRENT_DATE:
							case K_CURRENT_TIME:
							case K_CURRENT_TIMESTAMP:
							case K_EXISTS:
							case K_NOT:
							case K_NULL:
							case K_VAR:
							case K_TRUE:
							case K_FALSE:
							case IDENTIFIER:
							case NUMERIC_LITERAL:
							case STRING_LITERAL:
							case BLOB_LITERAL:
								{
								setState(968);
								expr_test(0);
								setState(973);
								_errHandler.sync(this);
								_la = _input.LA(1);
								while (_la==COMMA) {
									{
									{
									setState(969);
									match(COMMA);
									setState(970);
									expr_test(0);
									}
									}
									setState(975);
									_errHandler.sync(this);
									_la = _input.LA(1);
								}
								}
								break;
							case CLOSE_PAR:
								break;
							default:
								break;
							}
							setState(978);
							match(CLOSE_PAR);
							}
							break;
						case 2:
							{
							setState(982);
							_errHandler.sync(this);
							switch ( getInterpreter().adaptivePredict(_input,93,_ctx) ) {
							case 1:
								{
								setState(979);
								database_name();
								setState(980);
								match(DOT);
								}
								break;
							}
							setState(984);
							table_name();
							}
							break;
						}
						}
						break;
					}
					} 
				}
				setState(991);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,96,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Fun_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Fun_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fun_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFun_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFun_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFun_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fun_nameContext fun_name() throws RecognitionException {
		Fun_nameContext _localctx = new Fun_nameContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_fun_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(992);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_listContext extends ParserRuleContext {
		public List<Define_varContext> define_var() {
			return getRuleContexts(Define_varContext.class);
		}
		public Define_varContext define_var(int i) {
			return getRuleContext(Define_varContext.class,i);
		}
		public List<Define_var_assigneContext> define_var_assigne() {
			return getRuleContexts(Define_var_assigneContext.class);
		}
		public Define_var_assigneContext define_var_assigne(int i) {
			return getRuleContext(Define_var_assigneContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterParameter_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitParameter_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitParameter_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Parameter_listContext parameter_list() throws RecognitionException {
		Parameter_listContext _localctx = new Parameter_listContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_parameter_list);
		int _la;
		try {
			int _alt;
			setState(1024);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,103,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1005);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,99,_ctx) ) {
				case 1:
					{
					setState(1002);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_VAR || _la==IDENTIFIER) {
						{
						setState(994);
						define_var_assigne();
						setState(999);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(995);
							match(COMMA);
							setState(996);
							define_var_assigne();
							}
							}
							setState(1001);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						}
					}

					}
					break;
				case 2:
					{
					setState(1004);
					define_var();
					}
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1022);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_VAR) {
					{
					setState(1007);
					define_var();
					setState(1012);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(1008);
							match(COMMA);
							setState(1009);
							define_var();
							}
							} 
						}
						setState(1014);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
					}
					setState(1019);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(1015);
						match(COMMA);
						setState(1016);
						define_var_assigne();
						}
						}
						setState(1021);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Define_varContext extends ParserRuleContext {
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Define_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_define_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDefine_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDefine_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDefine_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Define_varContext define_var() throws RecognitionException {
		Define_varContext _localctx = new Define_varContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_define_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1026);
			match(K_VAR);
			setState(1027);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Define_var_assigneContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(SQLParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(SQLParser.IDENTIFIER, i);
		}
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Assigne_selectContext assigne_select() {
			return getRuleContext(Assigne_selectContext.class,0);
		}
		public Bool_exContext bool_ex() {
			return getRuleContext(Bool_exContext.class,0);
		}
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public Define_var_assigneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_define_var_assigne; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDefine_var_assigne(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDefine_var_assigne(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDefine_var_assigne(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Define_var_assigneContext define_var_assigne() throws RecognitionException {
		Define_var_assigneContext _localctx = new Define_var_assigneContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_define_var_assigne);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1040);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,106,_ctx) ) {
			case 1:
				{
				setState(1030);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_VAR) {
					{
					setState(1029);
					match(K_VAR);
					}
				}

				setState(1032);
				match(IDENTIFIER);
				setState(1033);
				match(ASSIGN);
				setState(1037);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,105,_ctx) ) {
				case 1:
					{
					setState(1034);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1035);
					bool_ex(0);
					}
					break;
				case 3:
					{
					setState(1036);
					expr_var(0);
					}
					break;
				}
				}
				break;
			case 2:
				{
				setState(1039);
				assigne_select();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public Fun_nameContext fun_name() {
			return getRuleContext(Fun_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1042);
			fun_name();
			setState(1043);
			match(OPEN_PAR);
			setState(1044);
			parameter_list();
			setState(1045);
			match(CLOSE_PAR);
			setState(1046);
			match(T__1);
			setState(1047);
			body();
			setState(1048);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1050);
			match(T__1);
			setState(1052);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,107,_ctx) ) {
			case 1:
				{
				setState(1051);
				body();
				}
				break;
			}
			setState(1054);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public List<If_stmtContext> if_stmt() {
			return getRuleContexts(If_stmtContext.class);
		}
		public If_stmtContext if_stmt(int i) {
			return getRuleContext(If_stmtContext.class,i);
		}
		public List<If_line_stmtContext> if_line_stmt() {
			return getRuleContexts(If_line_stmtContext.class);
		}
		public If_line_stmtContext if_line_stmt(int i) {
			return getRuleContext(If_line_stmtContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(SQLParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SQLParser.SCOL, i);
		}
		public List<Json_object_stmtContext> json_object_stmt() {
			return getRuleContexts(Json_object_stmtContext.class);
		}
		public Json_object_stmtContext json_object_stmt(int i) {
			return getRuleContext(Json_object_stmtContext.class,i);
		}
		public List<Set_prorerty_jsonContext> set_prorerty_json() {
			return getRuleContexts(Set_prorerty_jsonContext.class);
		}
		public Set_prorerty_jsonContext set_prorerty_json(int i) {
			return getRuleContext(Set_prorerty_jsonContext.class,i);
		}
		public List<Switch_stmtContext> switch_stmt() {
			return getRuleContexts(Switch_stmtContext.class);
		}
		public Switch_stmtContext switch_stmt(int i) {
			return getRuleContext(Switch_stmtContext.class,i);
		}
		public List<Min_varContext> min_var() {
			return getRuleContexts(Min_varContext.class);
		}
		public Min_varContext min_var(int i) {
			return getRuleContext(Min_varContext.class,i);
		}
		public List<Plus_varContext> plus_var() {
			return getRuleContexts(Plus_varContext.class);
		}
		public Plus_varContext plus_var(int i) {
			return getRuleContext(Plus_varContext.class,i);
		}
		public List<For_stmtContext> for_stmt() {
			return getRuleContexts(For_stmtContext.class);
		}
		public For_stmtContext for_stmt(int i) {
			return getRuleContext(For_stmtContext.class,i);
		}
		public List<ForeachContext> foreach() {
			return getRuleContexts(ForeachContext.class);
		}
		public ForeachContext foreach(int i) {
			return getRuleContext(ForeachContext.class,i);
		}
		public List<PrintContext> print() {
			return getRuleContexts(PrintContext.class);
		}
		public PrintContext print(int i) {
			return getRuleContext(PrintContext.class,i);
		}
		public List<Define_varContext> define_var() {
			return getRuleContexts(Define_varContext.class);
		}
		public Define_varContext define_var(int i) {
			return getRuleContext(Define_varContext.class,i);
		}
		public List<Call_funcContext> call_func() {
			return getRuleContexts(Call_funcContext.class);
		}
		public Call_funcContext call_func(int i) {
			return getRuleContext(Call_funcContext.class,i);
		}
		public List<Define_var_assigneContext> define_var_assigne() {
			return getRuleContexts(Define_var_assigneContext.class);
		}
		public Define_var_assigneContext define_var_assigne(int i) {
			return getRuleContext(Define_var_assigneContext.class,i);
		}
		public List<While_stmtContext> while_stmt() {
			return getRuleContexts(While_stmtContext.class);
		}
		public While_stmtContext while_stmt(int i) {
			return getRuleContext(While_stmtContext.class,i);
		}
		public List<Do_while_stmtContext> do_while_stmt() {
			return getRuleContexts(Do_while_stmtContext.class);
		}
		public Do_while_stmtContext do_while_stmt(int i) {
			return getRuleContext(Do_while_stmtContext.class,i);
		}
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<Declare_arrayContext> declare_array() {
			return getRuleContexts(Declare_arrayContext.class);
		}
		public Declare_arrayContext declare_array(int i) {
			return getRuleContext(Declare_arrayContext.class,i);
		}
		public List<Declare_array_vrContext> declare_array_vr() {
			return getRuleContexts(Declare_array_vrContext.class);
		}
		public Declare_array_vrContext declare_array_vr(int i) {
			return getRuleContext(Declare_array_vrContext.class,i);
		}
		public List<Sql_stmt_listContext> sql_stmt_list() {
			return getRuleContexts(Sql_stmt_listContext.class);
		}
		public Sql_stmt_listContext sql_stmt_list(int i) {
			return getRuleContext(Sql_stmt_listContext.class,i);
		}
		public List<Assigne_selectContext> assigne_select() {
			return getRuleContexts(Assigne_selectContext.class);
		}
		public Assigne_selectContext assigne_select(int i) {
			return getRuleContext(Assigne_selectContext.class,i);
		}
		public Return_stmContext return_stm() {
			return getRuleContext(Return_stmContext.class,0);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << PLUS_PLUSr) | (1L << MINUS_MINUSr) | (1L << SCOL) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << K_ALTER))) != 0) || ((((_la - 70)) & ~0x3f) == 0 && ((1L << (_la - 70)) & ((1L << (K_CREATE - 70)) | (1L << (K_CURRENT_DATE - 70)) | (1L << (K_CURRENT_TIME - 70)) | (1L << (K_CURRENT_TIMESTAMP - 70)) | (1L << (K_DELETE - 70)) | (1L << (K_DROP - 70)) | (1L << (K_EXISTS - 70)) | (1L << (K_FOR - 70)) | (1L << (K_IF - 70)) | (1L << (K_INSERT - 70)) | (1L << (K_NOT - 70)) | (1L << (K_NULL - 70)))) != 0) || ((((_la - 151)) & ~0x3f) == 0 && ((1L << (_la - 151)) & ((1L << (K_SELECT - 151)) | (1L << (K_UPDATE - 151)) | (1L << (K_VALUES - 151)) | (1L << (K_SWITCH - 151)) | (1L << (K_VAR - 151)) | (1L << (K_PRINT - 151)) | (1L << (K_TRUE - 151)) | (1L << (K_FALSE - 151)) | (1L << (K_WHILE - 151)) | (1L << (K_DO - 151)) | (1L << (IDENTIFIER - 151)) | (1L << (NUMERIC_LITERAL - 151)) | (1L << (STRING_LITERAL - 151)) | (1L << (BLOB_LITERAL - 151)))) != 0)) {
				{
				setState(1099);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,108,_ctx) ) {
				case 1:
					{
					setState(1056);
					if_stmt();
					}
					break;
				case 2:
					{
					setState(1057);
					if_line_stmt();
					setState(1058);
					match(SCOL);
					}
					break;
				case 3:
					{
					setState(1060);
					json_object_stmt();
					setState(1061);
					match(SCOL);
					}
					break;
				case 4:
					{
					setState(1063);
					set_prorerty_json();
					setState(1064);
					match(SCOL);
					}
					break;
				case 5:
					{
					setState(1066);
					switch_stmt();
					}
					break;
				case 6:
					{
					setState(1067);
					min_var();
					}
					break;
				case 7:
					{
					setState(1068);
					plus_var();
					}
					break;
				case 8:
					{
					setState(1069);
					for_stmt();
					}
					break;
				case 9:
					{
					setState(1070);
					foreach();
					}
					break;
				case 10:
					{
					setState(1071);
					print();
					setState(1072);
					match(SCOL);
					}
					break;
				case 11:
					{
					setState(1074);
					define_var();
					setState(1075);
					match(SCOL);
					}
					break;
				case 12:
					{
					setState(1077);
					call_func();
					setState(1078);
					match(SCOL);
					}
					break;
				case 13:
					{
					setState(1080);
					define_var_assigne();
					setState(1081);
					match(SCOL);
					}
					break;
				case 14:
					{
					setState(1083);
					while_stmt();
					}
					break;
				case 15:
					{
					setState(1084);
					do_while_stmt();
					}
					break;
				case 16:
					{
					setState(1085);
					block();
					}
					break;
				case 17:
					{
					setState(1086);
					expr(0);
					setState(1087);
					match(SCOL);
					}
					break;
				case 18:
					{
					setState(1089);
					declare_array();
					setState(1090);
					match(SCOL);
					}
					break;
				case 19:
					{
					setState(1092);
					declare_array_vr();
					setState(1093);
					match(SCOL);
					}
					break;
				case 20:
					{
					setState(1095);
					sql_stmt_list();
					}
					break;
				case 21:
					{
					setState(1096);
					assigne_select();
					setState(1097);
					match(SCOL);
					}
					break;
				}
				}
				setState(1103);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1105);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_RETURN) {
				{
				setState(1104);
				return_stm();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Body_without_bracketaContext extends ParserRuleContext {
		public If_stmtContext if_stmt() {
			return getRuleContext(If_stmtContext.class,0);
		}
		public If_line_stmtContext if_line_stmt() {
			return getRuleContext(If_line_stmtContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Json_object_stmtContext json_object_stmt() {
			return getRuleContext(Json_object_stmtContext.class,0);
		}
		public Set_prorerty_jsonContext set_prorerty_json() {
			return getRuleContext(Set_prorerty_jsonContext.class,0);
		}
		public Switch_stmtContext switch_stmt() {
			return getRuleContext(Switch_stmtContext.class,0);
		}
		public Min_varContext min_var() {
			return getRuleContext(Min_varContext.class,0);
		}
		public Plus_varContext plus_var() {
			return getRuleContext(Plus_varContext.class,0);
		}
		public For_stmtContext for_stmt() {
			return getRuleContext(For_stmtContext.class,0);
		}
		public ForeachContext foreach() {
			return getRuleContext(ForeachContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public Define_varContext define_var() {
			return getRuleContext(Define_varContext.class,0);
		}
		public Call_funcContext call_func() {
			return getRuleContext(Call_funcContext.class,0);
		}
		public Define_var_assigneContext define_var_assigne() {
			return getRuleContext(Define_var_assigneContext.class,0);
		}
		public While_stmtContext while_stmt() {
			return getRuleContext(While_stmtContext.class,0);
		}
		public Do_while_stmtContext do_while_stmt() {
			return getRuleContext(Do_while_stmtContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Declare_arrayContext declare_array() {
			return getRuleContext(Declare_arrayContext.class,0);
		}
		public Declare_array_vrContext declare_array_vr() {
			return getRuleContext(Declare_array_vrContext.class,0);
		}
		public Return_stmContext return_stm() {
			return getRuleContext(Return_stmContext.class,0);
		}
		public Sql_stmt_listContext sql_stmt_list() {
			return getRuleContext(Sql_stmt_listContext.class,0);
		}
		public Assigne_selectContext assigne_select() {
			return getRuleContext(Assigne_selectContext.class,0);
		}
		public Body_without_bracketaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body_without_bracketa; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBody_without_bracketa(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBody_without_bracketa(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBody_without_bracketa(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Body_without_bracketaContext body_without_bracketa() throws RecognitionException {
		Body_without_bracketaContext _localctx = new Body_without_bracketaContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_body_without_bracketa);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1150);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,111,_ctx) ) {
			case 1:
				{
				setState(1107);
				if_stmt();
				}
				break;
			case 2:
				{
				setState(1108);
				if_line_stmt();
				setState(1109);
				match(SCOL);
				}
				break;
			case 3:
				{
				setState(1111);
				json_object_stmt();
				setState(1112);
				match(SCOL);
				}
				break;
			case 4:
				{
				setState(1114);
				set_prorerty_json();
				setState(1115);
				match(SCOL);
				}
				break;
			case 5:
				{
				setState(1117);
				switch_stmt();
				}
				break;
			case 6:
				{
				setState(1118);
				min_var();
				}
				break;
			case 7:
				{
				setState(1119);
				plus_var();
				}
				break;
			case 8:
				{
				setState(1120);
				for_stmt();
				}
				break;
			case 9:
				{
				setState(1121);
				foreach();
				}
				break;
			case 10:
				{
				setState(1122);
				print();
				setState(1123);
				match(SCOL);
				}
				break;
			case 11:
				{
				setState(1125);
				define_var();
				setState(1126);
				match(SCOL);
				}
				break;
			case 12:
				{
				setState(1128);
				call_func();
				setState(1129);
				match(SCOL);
				}
				break;
			case 13:
				{
				setState(1131);
				define_var_assigne();
				setState(1132);
				match(SCOL);
				}
				break;
			case 14:
				{
				setState(1134);
				while_stmt();
				}
				break;
			case 15:
				{
				setState(1135);
				do_while_stmt();
				}
				break;
			case 16:
				{
				setState(1136);
				expr(0);
				setState(1137);
				match(SCOL);
				}
				break;
			case 17:
				{
				setState(1139);
				declare_array();
				setState(1140);
				match(SCOL);
				}
				break;
			case 18:
				{
				setState(1142);
				declare_array_vr();
				setState(1143);
				match(SCOL);
				}
				break;
			case 19:
				{
				setState(1145);
				return_stm();
				}
				break;
			case 20:
				{
				setState(1146);
				sql_stmt_list();
				}
				break;
			case 21:
				{
				setState(1147);
				assigne_select();
				setState(1148);
				match(SCOL);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Json_object_stmtContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Json_bodyContext json_body() {
			return getRuleContext(Json_bodyContext.class,0);
		}
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public Json_object_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_json_object_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJson_object_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJson_object_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJson_object_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Json_object_stmtContext json_object_stmt() throws RecognitionException {
		Json_object_stmtContext _localctx = new Json_object_stmtContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_json_object_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1153);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(1152);
				match(K_VAR);
				}
			}

			setState(1155);
			match(IDENTIFIER);
			setState(1156);
			match(ASSIGN);
			setState(1157);
			json_body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Json_bodyContext extends ParserRuleContext {
		public List<Property_jsonContext> property_json() {
			return getRuleContexts(Property_jsonContext.class);
		}
		public Property_jsonContext property_json(int i) {
			return getRuleContext(Property_jsonContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Json_bodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_json_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJson_body(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJson_body(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJson_body(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Json_bodyContext json_body() throws RecognitionException {
		Json_bodyContext _localctx = new Json_bodyContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_json_body);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1159);
			match(T__1);
			setState(1168);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IDENTIFIER) {
				{
				{
				setState(1160);
				property_json();
				}
				setState(1165);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1161);
					match(COMMA);
					setState(1162);
					property_json();
					}
					}
					setState(1167);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(1170);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Property_jsonContext extends ParserRuleContext {
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Array_jsonContext array_json() {
			return getRuleContext(Array_jsonContext.class,0);
		}
		public Json_bodyContext json_body() {
			return getRuleContext(Json_bodyContext.class,0);
		}
		public Property_jsonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_property_json; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterProperty_json(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitProperty_json(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitProperty_json(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Property_jsonContext property_json() throws RecognitionException {
		Property_jsonContext _localctx = new Property_jsonContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_property_json);
		try {
			setState(1181);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,115,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(1172);
				match(IDENTIFIER);
				}
				setState(1173);
				match(T__3);
				setState(1174);
				expr_var(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(1175);
				match(IDENTIFIER);
				}
				setState(1176);
				match(T__3);
				setState(1177);
				array_json();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				{
				setState(1178);
				match(IDENTIFIER);
				}
				setState(1179);
				match(T__3);
				setState(1180);
				json_body();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_jsonContext extends ParserRuleContext {
		public List<Literal_value_jsonContext> literal_value_json() {
			return getRuleContexts(Literal_value_jsonContext.class);
		}
		public Literal_value_jsonContext literal_value_json(int i) {
			return getRuleContext(Literal_value_jsonContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public List<Json_bodyContext> json_body() {
			return getRuleContexts(Json_bodyContext.class);
		}
		public Json_bodyContext json_body(int i) {
			return getRuleContext(Json_bodyContext.class,i);
		}
		public Array_jsonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_json; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterArray_json(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitArray_json(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitArray_json(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_jsonContext array_json() throws RecognitionException {
		Array_jsonContext _localctx = new Array_jsonContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_array_json);
		int _la;
		try {
			setState(1207);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,120,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(1183);
				match(T__4);
				setState(1192);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__4 || ((((_la - 126)) & ~0x3f) == 0 && ((1L << (_la - 126)) & ((1L << (K_NULL - 126)) | (1L << (K_TRUE - 126)) | (1L << (K_FALSE - 126)) | (1L << (IDENTIFIER - 126)) | (1L << (NUMERIC_LITERAL - 126)) | (1L << (STRING_LITERAL - 126)) | (1L << (BLOB_LITERAL - 126)))) != 0)) {
					{
					{
					setState(1184);
					literal_value_json();
					}
					setState(1189);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(1185);
						match(COMMA);
						setState(1186);
						literal_value_json();
						}
						}
						setState(1191);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(1194);
				match(T__5);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(1195);
				match(T__4);
				setState(1204);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__1) {
					{
					{
					setState(1196);
					json_body();
					}
					setState(1201);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(1197);
						match(COMMA);
						setState(1198);
						json_body();
						}
						}
						setState(1203);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(1206);
				match(T__5);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Get_property_jsonContext extends ParserRuleContext {
		public List<Json_object_nameContext> json_object_name() {
			return getRuleContexts(Json_object_nameContext.class);
		}
		public Json_object_nameContext json_object_name(int i) {
			return getRuleContext(Json_object_nameContext.class,i);
		}
		public List<TerminalNode> DOT() { return getTokens(SQLParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(SQLParser.DOT, i);
		}
		public Get_property_jsonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_get_property_json; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterGet_property_json(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitGet_property_json(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitGet_property_json(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Get_property_jsonContext get_property_json() throws RecognitionException {
		Get_property_jsonContext _localctx = new Get_property_jsonContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_get_property_json);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1209);
			json_object_name();
			setState(1210);
			match(DOT);
			setState(1211);
			json_object_name();
			setState(1216);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,121,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1212);
					match(DOT);
					setState(1213);
					json_object_name();
					}
					} 
				}
				setState(1218);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,121,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Set_prorerty_jsonContext extends ParserRuleContext {
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public Set_prorerty_jsonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_set_prorerty_json; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSet_prorerty_json(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSet_prorerty_json(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSet_prorerty_json(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Set_prorerty_jsonContext set_prorerty_json() throws RecognitionException {
		Set_prorerty_jsonContext _localctx = new Set_prorerty_jsonContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_set_prorerty_json);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1219);
			get_property_json();
			setState(1220);
			match(ASSIGN);
			setState(1221);
			expr_var(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Json_object_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Json_object_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_json_object_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJson_object_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJson_object_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJson_object_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Json_object_nameContext json_object_name() throws RecognitionException {
		Json_object_nameContext _localctx = new Json_object_nameContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_json_object_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1223);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_stmContext extends ParserRuleContext {
		public TerminalNode K_RETURN() { return getToken(SQLParser.K_RETURN, 0); }
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public If_line_stmtContext if_line_stmt() {
			return getRuleContext(If_line_stmtContext.class,0);
		}
		public Bool_exContext bool_ex() {
			return getRuleContext(Bool_exContext.class,0);
		}
		public Expr_testContext expr_test() {
			return getRuleContext(Expr_testContext.class,0);
		}
		public Return_stmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterReturn_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitReturn_stm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitReturn_stm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_stmContext return_stm() throws RecognitionException {
		Return_stmContext _localctx = new Return_stmContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_return_stm);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1225);
			match(K_RETURN);
			setState(1229);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,122,_ctx) ) {
			case 1:
				{
				setState(1226);
				if_line_stmt();
				}
				break;
			case 2:
				{
				setState(1227);
				bool_ex(0);
				}
				break;
			case 3:
				{
				setState(1228);
				expr_test(0);
				}
				break;
			}
			setState(1231);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_conditionContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Array_assigneContext array_assigne() {
			return getRuleContext(Array_assigneContext.class,0);
		}
		public TerminalNode DIGIT() { return getToken(SQLParser.DIGIT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Expr_conditionContext> expr_condition() {
			return getRuleContexts(Expr_conditionContext.class);
		}
		public Expr_conditionContext expr_condition(int i) {
			return getRuleContext(Expr_conditionContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public Incr_decr_without_commaContext incr_decr_without_comma() {
			return getRuleContext(Incr_decr_without_commaContext.class,0);
		}
		public Call_funcContext call_func() {
			return getRuleContext(Call_funcContext.class,0);
		}
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(SQLParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SQLParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public Expr_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExpr_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExpr_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExpr_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_conditionContext expr_condition() throws RecognitionException {
		return expr_condition(0);
	}

	private Expr_conditionContext expr_condition(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Expr_conditionContext _localctx = new Expr_conditionContext(_ctx, _parentState);
		Expr_conditionContext _prevctx = _localctx;
		int _startState = 104;
		enterRecursionRule(_localctx, 104, RULE_expr_condition, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1249);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,123,_ctx) ) {
			case 1:
				{
				setState(1234);
				match(NUMERIC_LITERAL);
				}
				break;
			case 2:
				{
				setState(1235);
				literal_value();
				}
				break;
			case 3:
				{
				setState(1236);
				get_property_json();
				}
				break;
			case 4:
				{
				setState(1237);
				match(IDENTIFIER);
				}
				break;
			case 5:
				{
				setState(1238);
				array_assigne();
				}
				break;
			case 6:
				{
				setState(1239);
				match(DIGIT);
				}
				break;
			case 7:
				{
				setState(1240);
				match(OPEN_PAR);
				setState(1241);
				expr_condition(0);
				setState(1242);
				match(CLOSE_PAR);
				}
				break;
			case 8:
				{
				setState(1244);
				unary_operator();
				setState(1245);
				match(NUMERIC_LITERAL);
				}
				break;
			case 9:
				{
				setState(1247);
				incr_decr_without_comma();
				}
				break;
			case 10:
				{
				setState(1248);
				call_func();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(1259);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,125,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1257);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,124,_ctx) ) {
					case 1:
						{
						_localctx = new Expr_conditionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_condition);
						setState(1251);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(1252);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1253);
						expr_condition(5);
						}
						break;
					case 2:
						{
						_localctx = new Expr_conditionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_condition);
						setState(1254);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(1255);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1256);
						expr_condition(4);
						}
						break;
					}
					} 
				}
				setState(1261);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,125,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Condition_headerContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Condition_headerContext> condition_header() {
			return getRuleContexts(Condition_headerContext.class);
		}
		public Condition_headerContext condition_header(int i) {
			return getRuleContext(Condition_headerContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public List<Expr_varContext> expr_var() {
			return getRuleContexts(Expr_varContext.class);
		}
		public Expr_varContext expr_var(int i) {
			return getRuleContext(Expr_varContext.class,i);
		}
		public TerminalNode LT() { return getToken(SQLParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SQLParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SQLParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SQLParser.GT_EQ, 0); }
		public TerminalNode EQ() { return getToken(SQLParser.EQ, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public TerminalNode NOT_EQ1() { return getToken(SQLParser.NOT_EQ1, 0); }
		public Bool_exContext bool_ex() {
			return getRuleContext(Bool_exContext.class,0);
		}
		public TerminalNode K_AND() { return getToken(SQLParser.K_AND, 0); }
		public TerminalNode K_OR() { return getToken(SQLParser.K_OR, 0); }
		public TerminalNode AMP2() { return getToken(SQLParser.AMP2, 0); }
		public TerminalNode PIPE2() { return getToken(SQLParser.PIPE2, 0); }
		public Condition_headerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition_header; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCondition_header(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCondition_header(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCondition_header(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Condition_headerContext condition_header() throws RecognitionException {
		return condition_header(0);
	}

	private Condition_headerContext condition_header(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Condition_headerContext _localctx = new Condition_headerContext(_ctx, _parentState);
		Condition_headerContext _prevctx = _localctx;
		int _startState = 106;
		enterRecursionRule(_localctx, 106, RULE_condition_header, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1273);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,126,_ctx) ) {
			case 1:
				{
				setState(1263);
				match(OPEN_PAR);
				setState(1264);
				condition_header(0);
				setState(1265);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(1267);
				match(IDENTIFIER);
				}
				break;
			case 3:
				{
				setState(1268);
				expr_var(0);
				setState(1269);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASSIGN) | (1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ) | (1L << EQ) | (1L << NOT_EQ1))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1270);
				expr_var(0);
				}
				break;
			case 4:
				{
				setState(1272);
				bool_ex(0);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(1283);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,128,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1281);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,127,_ctx) ) {
					case 1:
						{
						_localctx = new Condition_headerContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condition_header);
						setState(1275);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(1276);
						_la = _input.LA(1);
						if ( !(_la==K_AND || _la==K_OR) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1277);
						condition_header(4);
						}
						break;
					case 2:
						{
						_localctx = new Condition_headerContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_condition_header);
						setState(1278);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(1279);
						_la = _input.LA(1);
						if ( !(_la==PIPE2 || _la==AMP2) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1280);
						condition_header(3);
						}
						break;
					}
					} 
				}
				setState(1285);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,128,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class If_stmtContext extends ParserRuleContext {
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Condition_headerContext condition_header() {
			return getRuleContext(Condition_headerContext.class,0);
		}
		public List<Else_ifContext> else_if() {
			return getRuleContexts(Else_ifContext.class);
		}
		public Else_ifContext else_if(int i) {
			return getRuleContext(Else_ifContext.class,i);
		}
		public Else_stmtContext else_stmt() {
			return getRuleContext(Else_stmtContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public Body_without_bracketaContext body_without_bracketa() {
			return getRuleContext(Body_without_bracketaContext.class,0);
		}
		public If_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIf_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIf_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIf_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_stmtContext if_stmt() throws RecognitionException {
		If_stmtContext _localctx = new If_stmtContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_if_stmt);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1286);
			match(K_IF);
			setState(1287);
			match(OPEN_PAR);
			{
			setState(1288);
			condition_header(0);
			}
			setState(1289);
			match(CLOSE_PAR);
			setState(1295);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(1290);
				match(T__1);
				{
				setState(1291);
				body();
				}
				setState(1292);
				match(T__2);
				}
				break;
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_ALTER:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DELETE:
			case K_DROP:
			case K_EXISTS:
			case K_FOR:
			case K_IF:
			case K_INSERT:
			case K_NOT:
			case K_NULL:
			case K_SELECT:
			case K_UPDATE:
			case K_VALUES:
			case K_SWITCH:
			case K_RETURN:
			case K_VAR:
			case K_PRINT:
			case K_TRUE:
			case K_FALSE:
			case K_WHILE:
			case K_DO:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				{
				setState(1294);
				body_without_bracketa();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(1300);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,130,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1297);
					else_if();
					}
					} 
				}
				setState(1302);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,130,_ctx);
			}
			setState(1304);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,131,_ctx) ) {
			case 1:
				{
				setState(1303);
				else_stmt();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_ifContext extends ParserRuleContext {
		public TerminalNode K_ELSE() { return getToken(SQLParser.K_ELSE, 0); }
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Condition_headerContext condition_header() {
			return getRuleContext(Condition_headerContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public Body_without_bracketaContext body_without_bracketa() {
			return getRuleContext(Body_without_bracketaContext.class,0);
		}
		public Else_ifContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_if; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterElse_if(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitElse_if(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitElse_if(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_ifContext else_if() throws RecognitionException {
		Else_ifContext _localctx = new Else_ifContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_else_if);
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(1306);
			match(K_ELSE);
			setState(1307);
			match(K_IF);
			setState(1308);
			match(OPEN_PAR);
			{
			setState(1309);
			condition_header(0);
			}
			setState(1310);
			match(CLOSE_PAR);
			setState(1316);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(1311);
				match(T__1);
				{
				setState(1312);
				body();
				}
				setState(1313);
				match(T__2);
				}
				break;
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_ALTER:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DELETE:
			case K_DROP:
			case K_EXISTS:
			case K_FOR:
			case K_IF:
			case K_INSERT:
			case K_NOT:
			case K_NULL:
			case K_SELECT:
			case K_UPDATE:
			case K_VALUES:
			case K_SWITCH:
			case K_RETURN:
			case K_VAR:
			case K_PRINT:
			case K_TRUE:
			case K_FALSE:
			case K_WHILE:
			case K_DO:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				{
				setState(1315);
				body_without_bracketa();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_stmtContext extends ParserRuleContext {
		public TerminalNode K_ELSE() { return getToken(SQLParser.K_ELSE, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public Body_without_bracketaContext body_without_bracketa() {
			return getRuleContext(Body_without_bracketaContext.class,0);
		}
		public Else_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterElse_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitElse_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitElse_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_stmtContext else_stmt() throws RecognitionException {
		Else_stmtContext _localctx = new Else_stmtContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_else_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(1318);
			match(K_ELSE);
			setState(1324);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(1319);
				match(T__1);
				{
				setState(1320);
				body();
				}
				setState(1321);
				match(T__2);
				}
				break;
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_ALTER:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DELETE:
			case K_DROP:
			case K_EXISTS:
			case K_FOR:
			case K_IF:
			case K_INSERT:
			case K_NOT:
			case K_NULL:
			case K_SELECT:
			case K_UPDATE:
			case K_VALUES:
			case K_SWITCH:
			case K_RETURN:
			case K_VAR:
			case K_PRINT:
			case K_TRUE:
			case K_FALSE:
			case K_WHILE:
			case K_DO:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				{
				setState(1323);
				body_without_bracketa();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_stmtContext extends ParserRuleContext {
		public TerminalNode K_SWITCH() { return getToken(SQLParser.K_SWITCH, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Body_switchContext body_switch() {
			return getRuleContext(Body_switchContext.class,0);
		}
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public Call_funcContext call_func() {
			return getRuleContext(Call_funcContext.class,0);
		}
		public If_line_stmtContext if_line_stmt() {
			return getRuleContext(If_line_stmtContext.class,0);
		}
		public Define_var_assigneContext define_var_assigne() {
			return getRuleContext(Define_var_assigneContext.class,0);
		}
		public Switch_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSwitch_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSwitch_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSwitch_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Switch_stmtContext switch_stmt() throws RecognitionException {
		Switch_stmtContext _localctx = new Switch_stmtContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_switch_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1326);
			match(K_SWITCH);
			setState(1327);
			match(OPEN_PAR);
			setState(1332);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,134,_ctx) ) {
			case 1:
				{
				setState(1328);
				expr_var(0);
				}
				break;
			case 2:
				{
				setState(1329);
				call_func();
				}
				break;
			case 3:
				{
				setState(1330);
				if_line_stmt();
				}
				break;
			case 4:
				{
				setState(1331);
				define_var_assigne();
				}
				break;
			}
			setState(1334);
			match(CLOSE_PAR);
			setState(1335);
			match(T__1);
			setState(1336);
			body_switch();
			setState(1337);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Case_stmContext extends ParserRuleContext {
		public TerminalNode K_CASE() { return getToken(SQLParser.K_CASE, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public TerminalNode K_BREAK() { return getToken(SQLParser.K_BREAK, 0); }
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Case_stmContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_case_stm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCase_stm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCase_stm(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCase_stm(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Case_stmContext case_stm() throws RecognitionException {
		Case_stmContext _localctx = new Case_stmContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_case_stm);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1339);
			match(K_CASE);
			setState(1342);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,135,_ctx) ) {
			case 1:
				{
				setState(1340);
				literal_value();
				}
				break;
			case 2:
				{
				setState(1341);
				any_name();
				}
				break;
			}
			setState(1344);
			match(T__3);
			setState(1345);
			body();
			{
			setState(1348);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_BREAK) {
				{
				setState(1346);
				match(K_BREAK);
				setState(1347);
				match(SCOL);
				}
			}

			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Default_stm_switchContext extends ParserRuleContext {
		public TerminalNode K_DEFAULT() { return getToken(SQLParser.K_DEFAULT, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public TerminalNode K_BREAK() { return getToken(SQLParser.K_BREAK, 0); }
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Default_stm_switchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default_stm_switch; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDefault_stm_switch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDefault_stm_switch(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDefault_stm_switch(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Default_stm_switchContext default_stm_switch() throws RecognitionException {
		Default_stm_switchContext _localctx = new Default_stm_switchContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_default_stm_switch);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1350);
			match(K_DEFAULT);
			setState(1351);
			match(T__3);
			setState(1352);
			body();
			{
			setState(1355);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_BREAK) {
				{
				setState(1353);
				match(K_BREAK);
				setState(1354);
				match(SCOL);
				}
			}

			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Body_switchContext extends ParserRuleContext {
		public List<Case_stmContext> case_stm() {
			return getRuleContexts(Case_stmContext.class);
		}
		public Case_stmContext case_stm(int i) {
			return getRuleContext(Case_stmContext.class,i);
		}
		public Default_stm_switchContext default_stm_switch() {
			return getRuleContext(Default_stm_switchContext.class,0);
		}
		public Body_switchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body_switch; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBody_switch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBody_switch(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBody_switch(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Body_switchContext body_switch() throws RecognitionException {
		Body_switchContext _localctx = new Body_switchContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_body_switch);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1360);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_CASE) {
				{
				{
				setState(1357);
				case_stm();
				}
				}
				setState(1362);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1364);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_DEFAULT) {
				{
				setState(1363);
				default_stm_switch();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Exp_forContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public TerminalNode DIGIT() { return getToken(SQLParser.DIGIT, 0); }
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Incr_decr_without_commaContext incr_decr_without_comma() {
			return getRuleContext(Incr_decr_without_commaContext.class,0);
		}
		public Exp_iContext exp_i() {
			return getRuleContext(Exp_iContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode PLUS_EQ() { return getToken(SQLParser.PLUS_EQ, 0); }
		public TerminalNode MINUS_EQ() { return getToken(SQLParser.MINUS_EQ, 0); }
		public TerminalNode STAR_EQ() { return getToken(SQLParser.STAR_EQ, 0); }
		public TerminalNode DIV_EQ() { return getToken(SQLParser.DIV_EQ, 0); }
		public Exp_forContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp_for; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExp_for(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExp_for(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExp_for(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Exp_forContext exp_for() throws RecognitionException {
		Exp_forContext _localctx = new Exp_forContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_exp_for);
		try {
			setState(1466);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,145,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1366);
				match(IDENTIFIER);
				setState(1367);
				match(ASSIGN);
				setState(1384);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,140,_ctx) ) {
				case 1:
					{
					setState(1368);
					match(DIGIT);
					}
					break;
				case 2:
					{
					setState(1369);
					literal_value();
					}
					break;
				case 3:
					{
					setState(1370);
					incr_decr_without_comma();
					}
					break;
				case 4:
					{
					setState(1371);
					exp_i();
					}
					break;
				case 5:
					{
					setState(1372);
					match(OPEN_PAR);
					setState(1373);
					exp_i();
					setState(1374);
					match(CLOSE_PAR);
					}
					break;
				case 6:
					{
					setState(1376);
					match(OPEN_PAR);
					setState(1377);
					incr_decr_without_comma();
					setState(1378);
					match(CLOSE_PAR);
					}
					break;
				case 7:
					{
					setState(1380);
					match(OPEN_PAR);
					setState(1381);
					literal_value();
					setState(1382);
					match(CLOSE_PAR);
					}
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1386);
				match(IDENTIFIER);
				setState(1387);
				match(PLUS_EQ);
				setState(1404);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,141,_ctx) ) {
				case 1:
					{
					setState(1388);
					match(DIGIT);
					}
					break;
				case 2:
					{
					setState(1389);
					literal_value();
					}
					break;
				case 3:
					{
					setState(1390);
					incr_decr_without_comma();
					}
					break;
				case 4:
					{
					setState(1391);
					exp_i();
					}
					break;
				case 5:
					{
					setState(1392);
					match(OPEN_PAR);
					setState(1393);
					exp_i();
					setState(1394);
					match(CLOSE_PAR);
					}
					break;
				case 6:
					{
					setState(1396);
					match(OPEN_PAR);
					setState(1397);
					incr_decr_without_comma();
					setState(1398);
					match(CLOSE_PAR);
					}
					break;
				case 7:
					{
					setState(1400);
					match(OPEN_PAR);
					setState(1401);
					literal_value();
					setState(1402);
					match(CLOSE_PAR);
					}
					break;
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1406);
				match(IDENTIFIER);
				setState(1407);
				match(MINUS_EQ);
				setState(1424);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,142,_ctx) ) {
				case 1:
					{
					setState(1408);
					match(DIGIT);
					}
					break;
				case 2:
					{
					setState(1409);
					literal_value();
					}
					break;
				case 3:
					{
					setState(1410);
					incr_decr_without_comma();
					}
					break;
				case 4:
					{
					setState(1411);
					exp_i();
					}
					break;
				case 5:
					{
					setState(1412);
					match(OPEN_PAR);
					setState(1413);
					exp_i();
					setState(1414);
					match(CLOSE_PAR);
					}
					break;
				case 6:
					{
					setState(1416);
					match(OPEN_PAR);
					setState(1417);
					incr_decr_without_comma();
					setState(1418);
					match(CLOSE_PAR);
					}
					break;
				case 7:
					{
					setState(1420);
					match(OPEN_PAR);
					setState(1421);
					literal_value();
					setState(1422);
					match(CLOSE_PAR);
					}
					break;
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1426);
				match(IDENTIFIER);
				setState(1427);
				match(STAR_EQ);
				setState(1444);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,143,_ctx) ) {
				case 1:
					{
					setState(1428);
					match(DIGIT);
					}
					break;
				case 2:
					{
					setState(1429);
					literal_value();
					}
					break;
				case 3:
					{
					setState(1430);
					incr_decr_without_comma();
					}
					break;
				case 4:
					{
					setState(1431);
					exp_i();
					}
					break;
				case 5:
					{
					setState(1432);
					match(OPEN_PAR);
					setState(1433);
					exp_i();
					setState(1434);
					match(CLOSE_PAR);
					}
					break;
				case 6:
					{
					setState(1436);
					match(OPEN_PAR);
					setState(1437);
					incr_decr_without_comma();
					setState(1438);
					match(CLOSE_PAR);
					}
					break;
				case 7:
					{
					setState(1440);
					match(OPEN_PAR);
					setState(1441);
					literal_value();
					setState(1442);
					match(CLOSE_PAR);
					}
					break;
				}
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1446);
				match(IDENTIFIER);
				setState(1447);
				match(DIV_EQ);
				setState(1464);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,144,_ctx) ) {
				case 1:
					{
					setState(1448);
					match(DIGIT);
					}
					break;
				case 2:
					{
					setState(1449);
					literal_value();
					}
					break;
				case 3:
					{
					setState(1450);
					incr_decr_without_comma();
					}
					break;
				case 4:
					{
					setState(1451);
					exp_i();
					}
					break;
				case 5:
					{
					setState(1452);
					match(OPEN_PAR);
					setState(1453);
					exp_i();
					setState(1454);
					match(CLOSE_PAR);
					}
					break;
				case 6:
					{
					setState(1456);
					match(OPEN_PAR);
					setState(1457);
					incr_decr_without_comma();
					setState(1458);
					match(CLOSE_PAR);
					}
					break;
				case 7:
					{
					setState(1460);
					match(OPEN_PAR);
					setState(1461);
					literal_value();
					setState(1462);
					match(CLOSE_PAR);
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Exp_iContext extends ParserRuleContext {
		public List<TerminalNode> PLUS() { return getTokens(SQLParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(SQLParser.PLUS, i);
		}
		public List<TerminalNode> MINUS() { return getTokens(SQLParser.MINUS); }
		public TerminalNode MINUS(int i) {
			return getToken(SQLParser.MINUS, i);
		}
		public List<TerminalNode> DIV() { return getTokens(SQLParser.DIV); }
		public TerminalNode DIV(int i) {
			return getToken(SQLParser.DIV, i);
		}
		public List<TerminalNode> STAR() { return getTokens(SQLParser.STAR); }
		public TerminalNode STAR(int i) {
			return getToken(SQLParser.STAR, i);
		}
		public List<Literal_valueContext> literal_value() {
			return getRuleContexts(Literal_valueContext.class);
		}
		public Literal_valueContext literal_value(int i) {
			return getRuleContext(Literal_valueContext.class,i);
		}
		public List<Incr_decr_without_commaContext> incr_decr_without_comma() {
			return getRuleContexts(Incr_decr_without_commaContext.class);
		}
		public Incr_decr_without_commaContext incr_decr_without_comma(int i) {
			return getRuleContext(Incr_decr_without_commaContext.class,i);
		}
		public List<TerminalNode> OPEN_PAR() { return getTokens(SQLParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SQLParser.OPEN_PAR, i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SQLParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SQLParser.CLOSE_PAR, i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(SQLParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(SQLParser.IDENTIFIER, i);
		}
		public List<Exp_iContext> exp_i() {
			return getRuleContexts(Exp_iContext.class);
		}
		public Exp_iContext exp_i(int i) {
			return getRuleContext(Exp_iContext.class,i);
		}
		public Exp_iContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exp_i; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExp_i(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExp_i(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExp_i(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Exp_iContext exp_i() throws RecognitionException {
		Exp_iContext _localctx = new Exp_iContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_exp_i);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1484);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,147,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1478);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,146,_ctx) ) {
					case 1:
						{
						setState(1468);
						literal_value();
						}
						break;
					case 2:
						{
						setState(1469);
						incr_decr_without_comma();
						}
						break;
					case 3:
						{
						setState(1470);
						match(OPEN_PAR);
						setState(1471);
						literal_value();
						setState(1472);
						match(CLOSE_PAR);
						}
						break;
					case 4:
						{
						setState(1474);
						match(OPEN_PAR);
						setState(1475);
						incr_decr_without_comma();
						setState(1476);
						match(CLOSE_PAR);
						}
						break;
					}
					setState(1480);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << DIV))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					} 
				}
				setState(1486);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,147,_ctx);
			}
			setState(1528); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(1505);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,148,_ctx) ) {
				case 1:
					{
					setState(1487);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1488);
					literal_value();
					}
					break;
				case 3:
					{
					setState(1489);
					incr_decr_without_comma();
					}
					break;
				case 4:
					{
					setState(1490);
					match(OPEN_PAR);
					setState(1491);
					match(IDENTIFIER);
					setState(1492);
					match(CLOSE_PAR);
					}
					break;
				case 5:
					{
					setState(1493);
					match(OPEN_PAR);
					setState(1494);
					literal_value();
					setState(1495);
					match(CLOSE_PAR);
					}
					break;
				case 6:
					{
					setState(1497);
					match(OPEN_PAR);
					setState(1498);
					incr_decr_without_comma();
					setState(1499);
					match(CLOSE_PAR);
					}
					break;
				case 7:
					{
					setState(1501);
					match(OPEN_PAR);
					setState(1502);
					exp_i();
					setState(1503);
					match(CLOSE_PAR);
					}
					break;
				}
				setState(1507);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << DIV))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1526);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,149,_ctx) ) {
				case 1:
					{
					setState(1508);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1509);
					literal_value();
					}
					break;
				case 3:
					{
					setState(1510);
					incr_decr_without_comma();
					}
					break;
				case 4:
					{
					setState(1511);
					match(OPEN_PAR);
					setState(1512);
					match(IDENTIFIER);
					setState(1513);
					match(CLOSE_PAR);
					}
					break;
				case 5:
					{
					setState(1514);
					match(OPEN_PAR);
					setState(1515);
					literal_value();
					setState(1516);
					match(CLOSE_PAR);
					}
					break;
				case 6:
					{
					setState(1518);
					match(OPEN_PAR);
					setState(1519);
					incr_decr_without_comma();
					setState(1520);
					match(CLOSE_PAR);
					}
					break;
				case 7:
					{
					setState(1522);
					match(OPEN_PAR);
					setState(1523);
					exp_i();
					setState(1524);
					match(CLOSE_PAR);
					}
					break;
				}
				}
				}
				setState(1530); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( ((((_la - 11)) & ~0x3f) == 0 && ((1L << (_la - 11)) & ((1L << (PLUS_PLUSr - 11)) | (1L << (MINUS_MINUSr - 11)) | (1L << (OPEN_PAR - 11)) | (1L << (K_CURRENT_DATE - 11)) | (1L << (K_CURRENT_TIME - 11)) | (1L << (K_CURRENT_TIMESTAMP - 11)))) != 0) || ((((_la - 126)) & ~0x3f) == 0 && ((1L << (_la - 126)) & ((1L << (K_NULL - 126)) | (1L << (IDENTIFIER - 126)) | (1L << (NUMERIC_LITERAL - 126)) | (1L << (STRING_LITERAL - 126)) | (1L << (BLOB_LITERAL - 126)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Incr_decr_without_commaContext extends ParserRuleContext {
		public TerminalNode MINUS_MINUSl() { return getToken(SQLParser.MINUS_MINUSl, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Array_assigneContext array_assigne() {
			return getRuleContext(Array_assigneContext.class,0);
		}
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public TerminalNode PLUS_PLUSl() { return getToken(SQLParser.PLUS_PLUSl, 0); }
		public TerminalNode MINUS_MINUSr() { return getToken(SQLParser.MINUS_MINUSr, 0); }
		public TerminalNode PLUS_PLUSr() { return getToken(SQLParser.PLUS_PLUSr, 0); }
		public Incr_decr_without_commaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_incr_decr_without_comma; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIncr_decr_without_comma(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIncr_decr_without_comma(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIncr_decr_without_comma(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Incr_decr_without_commaContext incr_decr_without_comma() throws RecognitionException {
		Incr_decr_without_commaContext _localctx = new Incr_decr_without_commaContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_incr_decr_without_comma);
		try {
			setState(1556);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,155,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1535);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,151,_ctx) ) {
				case 1:
					{
					setState(1532);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1533);
					array_assigne();
					}
					break;
				case 3:
					{
					setState(1534);
					get_property_json();
					}
					break;
				}
				setState(1537);
				match(MINUS_MINUSl);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1541);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,152,_ctx) ) {
				case 1:
					{
					setState(1538);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1539);
					array_assigne();
					}
					break;
				case 3:
					{
					setState(1540);
					get_property_json();
					}
					break;
				}
				setState(1543);
				match(PLUS_PLUSl);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1544);
				match(MINUS_MINUSr);
				setState(1548);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,153,_ctx) ) {
				case 1:
					{
					setState(1545);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1546);
					array_assigne();
					}
					break;
				case 3:
					{
					setState(1547);
					get_property_json();
					}
					break;
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1550);
				match(PLUS_PLUSr);
				setState(1554);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,154,_ctx) ) {
				case 1:
					{
					setState(1551);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1552);
					array_assigne();
					}
					break;
				case 3:
					{
					setState(1553);
					get_property_json();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_stmtContext extends ParserRuleContext {
		public TerminalNode K_FOR() { return getToken(SQLParser.K_FOR, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Declare_var_forContext declare_var_for() {
			return getRuleContext(Declare_var_forContext.class,0);
		}
		public List<TerminalNode> SCOL() { return getTokens(SQLParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(SQLParser.SCOL, i);
		}
		public For_conditionContext for_condition() {
			return getRuleContext(For_conditionContext.class,0);
		}
		public For_process_varContext for_process_var() {
			return getRuleContext(For_process_varContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public Body_without_bracketaContext body_without_bracketa() {
			return getRuleContext(Body_without_bracketaContext.class,0);
		}
		public For_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFor_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFor_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFor_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_stmtContext for_stmt() throws RecognitionException {
		For_stmtContext _localctx = new For_stmtContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_for_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1558);
			match(K_FOR);
			setState(1559);
			match(OPEN_PAR);
			{
			setState(1560);
			declare_var_for();
			setState(1561);
			match(SCOL);
			setState(1562);
			for_condition();
			setState(1563);
			match(SCOL);
			setState(1564);
			for_process_var();
			}
			setState(1566);
			match(CLOSE_PAR);
			setState(1572);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(1567);
				match(T__1);
				setState(1568);
				body();
				setState(1569);
				match(T__2);
				}
				break;
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_ALTER:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DELETE:
			case K_DROP:
			case K_EXISTS:
			case K_FOR:
			case K_IF:
			case K_INSERT:
			case K_NOT:
			case K_NULL:
			case K_SELECT:
			case K_UPDATE:
			case K_VALUES:
			case K_SWITCH:
			case K_RETURN:
			case K_VAR:
			case K_PRINT:
			case K_TRUE:
			case K_FALSE:
			case K_WHILE:
			case K_DO:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(1571);
				body_without_bracketa();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Declare_var_forContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public Declare_var_forContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declare_var_for; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDeclare_var_for(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDeclare_var_for(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDeclare_var_for(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declare_var_forContext declare_var_for() throws RecognitionException {
		Declare_var_forContext _localctx = new Declare_var_forContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_declare_var_for);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1580);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR || _la==IDENTIFIER) {
				{
				setState(1575);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_VAR) {
					{
					setState(1574);
					match(K_VAR);
					}
				}

				setState(1577);
				match(IDENTIFIER);
				{
				setState(1578);
				match(ASSIGN);
				}
				{
				setState(1579);
				expr_var(0);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_conditionContext extends ParserRuleContext {
		public TerminalNode LT() { return getToken(SQLParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SQLParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SQLParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SQLParser.GT_EQ, 0); }
		public TerminalNode EQ() { return getToken(SQLParser.EQ, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public TerminalNode NOT_EQ1() { return getToken(SQLParser.NOT_EQ1, 0); }
		public List<Expr_varContext> expr_var() {
			return getRuleContexts(Expr_varContext.class);
		}
		public Expr_varContext expr_var(int i) {
			return getRuleContext(Expr_varContext.class,i);
		}
		public For_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFor_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFor_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFor_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_conditionContext for_condition() throws RecognitionException {
		For_conditionContext _localctx = new For_conditionContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_for_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1586);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS_PLUSr) | (1L << MINUS_MINUSr) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE))) != 0) || ((((_la - 72)) & ~0x3f) == 0 && ((1L << (_la - 72)) & ((1L << (K_CURRENT_DATE - 72)) | (1L << (K_CURRENT_TIME - 72)) | (1L << (K_CURRENT_TIMESTAMP - 72)) | (1L << (K_NOT - 72)) | (1L << (K_NULL - 72)))) != 0) || ((((_la - 176)) & ~0x3f) == 0 && ((1L << (_la - 176)) & ((1L << (K_VAR - 176)) | (1L << (IDENTIFIER - 176)) | (1L << (NUMERIC_LITERAL - 176)) | (1L << (STRING_LITERAL - 176)) | (1L << (BLOB_LITERAL - 176)))) != 0)) {
				{
				{
				setState(1582);
				expr_var(0);
				}
				setState(1583);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ASSIGN) | (1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ) | (1L << EQ) | (1L << NOT_EQ1))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				{
				setState(1584);
				expr_var(0);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_process_varContext extends ParserRuleContext {
		public Incr_decr_without_commaContext incr_decr_without_comma() {
			return getRuleContext(Incr_decr_without_commaContext.class,0);
		}
		public Exp_forContext exp_for() {
			return getRuleContext(Exp_forContext.class,0);
		}
		public For_process_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_process_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFor_process_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFor_process_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFor_process_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_process_varContext for_process_var() throws RecognitionException {
		For_process_varContext _localctx = new For_process_varContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_for_process_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1590);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,160,_ctx) ) {
			case 1:
				{
				setState(1588);
				incr_decr_without_comma();
				}
				break;
			case 2:
				{
				setState(1589);
				exp_for();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Min_varContext extends ParserRuleContext {
		public TerminalNode MINUS_MINUSl() { return getToken(SQLParser.MINUS_MINUSl, 0); }
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public TerminalNode MINUS_MINUSr() { return getToken(SQLParser.MINUS_MINUSr, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public Array_assigneContext array_assigne() {
			return getRuleContext(Array_assigneContext.class,0);
		}
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public Min_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_min_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterMin_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitMin_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitMin_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Min_varContext min_var() throws RecognitionException {
		Min_varContext _localctx = new Min_varContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_min_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1617);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,165,_ctx) ) {
			case 1:
				{
				setState(1596);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,161,_ctx) ) {
				case 1:
					{
					setState(1592);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1593);
					match(NUMERIC_LITERAL);
					}
					break;
				case 3:
					{
					setState(1594);
					array_assigne();
					}
					break;
				case 4:
					{
					setState(1595);
					get_property_json();
					}
					break;
				}
				setState(1598);
				match(MINUS_MINUSl);
				setState(1599);
				match(SCOL);
				}
				break;
			case 2:
				{
				setState(1604);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,162,_ctx) ) {
				case 1:
					{
					setState(1600);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1601);
					match(NUMERIC_LITERAL);
					}
					break;
				case 3:
					{
					setState(1602);
					array_assigne();
					}
					break;
				case 4:
					{
					setState(1603);
					get_property_json();
					}
					break;
				}
				setState(1606);
				match(MINUS_MINUSl);
				}
				break;
			case 3:
				{
				setState(1607);
				match(MINUS_MINUSr);
				setState(1612);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,163,_ctx) ) {
				case 1:
					{
					setState(1608);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1609);
					match(NUMERIC_LITERAL);
					}
					break;
				case 3:
					{
					setState(1610);
					array_assigne();
					}
					break;
				case 4:
					{
					setState(1611);
					get_property_json();
					}
					break;
				}
				setState(1615);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,164,_ctx) ) {
				case 1:
					{
					setState(1614);
					match(SCOL);
					}
					break;
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Plus_varContext extends ParserRuleContext {
		public TerminalNode PLUS_PLUSl() { return getToken(SQLParser.PLUS_PLUSl, 0); }
		public TerminalNode PLUS_PLUSr() { return getToken(SQLParser.PLUS_PLUSr, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public Array_assigneContext array_assigne() {
			return getRuleContext(Array_assigneContext.class,0);
		}
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Plus_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plus_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterPlus_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitPlus_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitPlus_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Plus_varContext plus_var() throws RecognitionException {
		Plus_varContext _localctx = new Plus_varContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_plus_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1639);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
			case NUMERIC_LITERAL:
				{
				setState(1623);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,166,_ctx) ) {
				case 1:
					{
					setState(1619);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1620);
					match(NUMERIC_LITERAL);
					}
					break;
				case 3:
					{
					setState(1621);
					array_assigne();
					}
					break;
				case 4:
					{
					setState(1622);
					get_property_json();
					}
					break;
				}
				setState(1625);
				match(PLUS_PLUSl);
				setState(1627);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,167,_ctx) ) {
				case 1:
					{
					setState(1626);
					match(SCOL);
					}
					break;
				}
				}
				break;
			case PLUS_PLUSr:
				{
				setState(1629);
				match(PLUS_PLUSr);
				setState(1634);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,168,_ctx) ) {
				case 1:
					{
					setState(1630);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1631);
					match(NUMERIC_LITERAL);
					}
					break;
				case 3:
					{
					setState(1632);
					array_assigne();
					}
					break;
				case 4:
					{
					setState(1633);
					get_property_json();
					}
					break;
				}
				setState(1637);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,169,_ctx) ) {
				case 1:
					{
					setState(1636);
					match(SCOL);
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForeachContext extends ParserRuleContext {
		public TerminalNode K_FOR() { return getToken(SQLParser.K_FOR, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Foreach_headerContext foreach_header() {
			return getRuleContext(Foreach_headerContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public Body_without_bracketaContext body_without_bracketa() {
			return getRuleContext(Body_without_bracketaContext.class,0);
		}
		public ForeachContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreach; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterForeach(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitForeach(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitForeach(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForeachContext foreach() throws RecognitionException {
		ForeachContext _localctx = new ForeachContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_foreach);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1641);
			match(K_FOR);
			setState(1642);
			match(OPEN_PAR);
			setState(1643);
			foreach_header();
			setState(1644);
			match(CLOSE_PAR);
			setState(1650);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(1645);
				match(T__1);
				setState(1646);
				body();
				setState(1647);
				match(T__2);
				}
				break;
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_ALTER:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DELETE:
			case K_DROP:
			case K_EXISTS:
			case K_FOR:
			case K_IF:
			case K_INSERT:
			case K_NOT:
			case K_NULL:
			case K_SELECT:
			case K_UPDATE:
			case K_VALUES:
			case K_SWITCH:
			case K_RETURN:
			case K_VAR:
			case K_PRINT:
			case K_TRUE:
			case K_FALSE:
			case K_WHILE:
			case K_DO:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(1649);
				body_without_bracketa();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreach_headerContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(SQLParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(SQLParser.IDENTIFIER, i);
		}
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public Foreach_headerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreach_header; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterForeach_header(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitForeach_header(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitForeach_header(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreach_headerContext foreach_header() throws RecognitionException {
		Foreach_headerContext _localctx = new Foreach_headerContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_foreach_header);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1653);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(1652);
				match(K_VAR);
				}
			}

			setState(1655);
			match(IDENTIFIER);
			setState(1656);
			match(T__3);
			setState(1657);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_line_stmtContext extends ParserRuleContext {
		public Condition_headerContext condition_header() {
			return getRuleContext(Condition_headerContext.class,0);
		}
		public If_line_first_parameterContext if_line_first_parameter() {
			return getRuleContext(If_line_first_parameterContext.class,0);
		}
		public If_line_second_parameterContext if_line_second_parameter() {
			return getRuleContext(If_line_second_parameterContext.class,0);
		}
		public Define_varContext define_var() {
			return getRuleContext(Define_varContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public If_line_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_line_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIf_line_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIf_line_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIf_line_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_line_stmtContext if_line_stmt() throws RecognitionException {
		If_line_stmtContext _localctx = new If_line_stmtContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_if_line_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1662);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,173,_ctx) ) {
			case 1:
				{
				setState(1659);
				define_var();
				setState(1660);
				match(ASSIGN);
				}
				break;
			}
			setState(1664);
			condition_header(0);
			setState(1665);
			match(T__6);
			setState(1666);
			if_line_first_parameter();
			setState(1667);
			match(T__3);
			setState(1668);
			if_line_second_parameter();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_line_first_parameterContext extends ParserRuleContext {
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public If_line_stmtContext if_line_stmt() {
			return getRuleContext(If_line_stmtContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public If_line_first_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_line_first_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIf_line_first_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIf_line_first_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIf_line_first_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_line_first_parameterContext if_line_first_parameter() throws RecognitionException {
		If_line_first_parameterContext _localctx = new If_line_first_parameterContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_if_line_first_parameter);
		try {
			setState(1679);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,175,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1673);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,174,_ctx) ) {
				case 1:
					{
					setState(1670);
					expr_var(0);
					}
					break;
				case 2:
					{
					setState(1671);
					print();
					}
					break;
				case 3:
					{
					setState(1672);
					if_line_stmt();
					}
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1675);
				match(OPEN_PAR);
				setState(1676);
				if_line_stmt();
				setState(1677);
				match(CLOSE_PAR);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_line_second_parameterContext extends ParserRuleContext {
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public If_line_stmtContext if_line_stmt() {
			return getRuleContext(If_line_stmtContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public If_line_second_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_line_second_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIf_line_second_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIf_line_second_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIf_line_second_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_line_second_parameterContext if_line_second_parameter() throws RecognitionException {
		If_line_second_parameterContext _localctx = new If_line_second_parameterContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_if_line_second_parameter);
		try {
			setState(1690);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,177,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1684);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,176,_ctx) ) {
				case 1:
					{
					setState(1681);
					expr_var(0);
					}
					break;
				case 2:
					{
					setState(1682);
					print();
					}
					break;
				case 3:
					{
					setState(1683);
					if_line_stmt();
					}
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1686);
				match(OPEN_PAR);
				setState(1687);
				if_line_stmt();
				setState(1688);
				match(CLOSE_PAR);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public TerminalNode K_PRINT() { return getToken(SQLParser.K_PRINT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public Expr_testContext expr_test() {
			return getRuleContext(Expr_testContext.class,0);
		}
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterPrint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitPrint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_print);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1692);
			match(K_PRINT);
			setState(1693);
			match(OPEN_PAR);
			setState(1696);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,178,_ctx) ) {
			case 1:
				{
				setState(1694);
				get_property_json();
				}
				break;
			case 2:
				{
				setState(1695);
				expr_test(0);
				}
				break;
			}
			setState(1698);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_varContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Array_assigneContext array_assigne() {
			return getRuleContext(Array_assigneContext.class,0);
		}
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Call_funcContext call_func() {
			return getRuleContext(Call_funcContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Expr_varContext> expr_var() {
			return getRuleContexts(Expr_varContext.class);
		}
		public Expr_varContext expr_var(int i) {
			return getRuleContext(Expr_varContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public Incr_decr_without_commaContext incr_decr_without_comma() {
			return getRuleContext(Incr_decr_without_commaContext.class,0);
		}
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(SQLParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(SQLParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public Expr_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExpr_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExpr_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExpr_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_varContext expr_var() throws RecognitionException {
		return expr_var(0);
	}

	private Expr_varContext expr_var(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Expr_varContext _localctx = new Expr_varContext(_ctx, _parentState);
		Expr_varContext _prevctx = _localctx;
		int _startState = 152;
		enterRecursionRule(_localctx, 152, RULE_expr_var, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1714);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,179,_ctx) ) {
			case 1:
				{
				setState(1701);
				match(IDENTIFIER);
				}
				break;
			case 2:
				{
				setState(1702);
				array_assigne();
				}
				break;
			case 3:
				{
				setState(1703);
				get_property_json();
				}
				break;
			case 4:
				{
				setState(1704);
				literal_value();
				}
				break;
			case 5:
				{
				setState(1705);
				call_func();
				}
				break;
			case 6:
				{
				setState(1706);
				match(OPEN_PAR);
				setState(1707);
				expr_var(0);
				setState(1708);
				match(CLOSE_PAR);
				}
				break;
			case 7:
				{
				setState(1710);
				unary_operator();
				setState(1711);
				expr_var(4);
				}
				break;
			case 8:
				{
				setState(1713);
				incr_decr_without_comma();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(1724);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,181,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1722);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,180,_ctx) ) {
					case 1:
						{
						_localctx = new Expr_varContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_var);
						setState(1716);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(1717);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1718);
						expr_var(4);
						}
						break;
					case 2:
						{
						_localctx = new Expr_varContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr_var);
						setState(1719);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(1720);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1721);
						expr_var(3);
						}
						break;
					}
					} 
				}
				setState(1726);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,181,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Declare_variableContext extends ParserRuleContext {
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public Sql_stmtContext sql_stmt() {
			return getRuleContext(Sql_stmtContext.class,0);
		}
		public Bool_exContext bool_ex() {
			return getRuleContext(Bool_exContext.class,0);
		}
		public Declare_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declare_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDeclare_variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDeclare_variable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDeclare_variable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declare_variableContext declare_variable() throws RecognitionException {
		Declare_variableContext _localctx = new Declare_variableContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_declare_variable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1727);
			match(K_VAR);
			setState(1728);
			match(IDENTIFIER);
			setState(1735);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(1729);
				match(ASSIGN);
				setState(1733);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case PLUS_PLUSr:
				case MINUS_MINUSr:
				case OPEN_PAR:
				case PLUS:
				case MINUS:
				case TILDE:
				case K_CURRENT_DATE:
				case K_CURRENT_TIME:
				case K_CURRENT_TIMESTAMP:
				case K_NOT:
				case K_NULL:
				case K_VAR:
				case IDENTIFIER:
				case NUMERIC_LITERAL:
				case STRING_LITERAL:
				case BLOB_LITERAL:
					{
					setState(1730);
					expr_var(0);
					}
					break;
				case K_ALTER:
				case K_CREATE:
				case K_DELETE:
				case K_DROP:
				case K_INSERT:
				case K_SELECT:
				case K_UPDATE:
				case K_VALUES:
					{
					setState(1731);
					sql_stmt();
					}
					break;
				case K_TRUE:
				case K_FALSE:
					{
					setState(1732);
					bool_ex(0);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Declare_arrayContext extends ParserRuleContext {
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public List<Expr_varContext> expr_var() {
			return getRuleContexts(Expr_varContext.class);
		}
		public Expr_varContext expr_var(int i) {
			return getRuleContext(Expr_varContext.class,i);
		}
		public Declare_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declare_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDeclare_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDeclare_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDeclare_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declare_arrayContext declare_array() throws RecognitionException {
		Declare_arrayContext _localctx = new Declare_arrayContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_declare_array);
		try {
			setState(1759);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,186,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1737);
				match(K_VAR);
				setState(1738);
				match(IDENTIFIER);
				setState(1744);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__4:
					{
					{
					setState(1739);
					match(T__4);
					{
					setState(1740);
					expr_var(0);
					}
					setState(1741);
					match(T__5);
					}
					}
					break;
				case T__7:
					{
					{
					setState(1743);
					match(T__7);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1746);
				match(K_VAR);
				setState(1747);
				match(IDENTIFIER);
				setState(1757);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__4:
					{
					{
					setState(1748);
					match(T__4);
					{
					setState(1749);
					expr_var(0);
					}
					setState(1750);
					match(T__5);
					setState(1751);
					match(T__4);
					{
					setState(1752);
					expr_var(0);
					}
					setState(1753);
					match(T__5);
					}
					}
					break;
				case T__7:
					{
					{
					setState(1755);
					match(T__7);
					setState(1756);
					match(T__7);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Schema_arrayContext extends ParserRuleContext {
		public List<TerminalNode> IDENTIFIER() { return getTokens(SQLParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(SQLParser.IDENTIFIER, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Schema_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_schema_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSchema_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSchema_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSchema_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Schema_arrayContext schema_array() throws RecognitionException {
		Schema_arrayContext _localctx = new Schema_arrayContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_schema_array);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1761);
			match(T__4);
			setState(1766);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,187,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1762);
					match(IDENTIFIER);
					setState(1763);
					match(COMMA);
					}
					} 
				}
				setState(1768);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,187,_ctx);
			}
			setState(1769);
			match(IDENTIFIER);
			setState(1770);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_assigneContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public List<Expr_varContext> expr_var() {
			return getRuleContexts(Expr_varContext.class);
		}
		public Expr_varContext expr_var(int i) {
			return getRuleContext(Expr_varContext.class,i);
		}
		public Array_assigneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_assigne; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterArray_assigne(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitArray_assigne(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitArray_assigne(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_assigneContext array_assigne() throws RecognitionException {
		Array_assigneContext _localctx = new Array_assigneContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_array_assigne);
		try {
			setState(1788);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,189,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1772);
				match(IDENTIFIER);
				setState(1778);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__4:
					{
					{
					setState(1773);
					match(T__4);
					{
					setState(1774);
					expr_var(0);
					}
					setState(1775);
					match(T__5);
					}
					}
					break;
				case T__7:
					{
					{
					setState(1777);
					match(T__7);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1780);
				match(IDENTIFIER);
				{
				{
				setState(1781);
				match(T__4);
				{
				setState(1782);
				expr_var(0);
				}
				setState(1783);
				match(T__5);
				setState(1784);
				match(T__4);
				{
				setState(1785);
				expr_var(0);
				}
				setState(1786);
				match(T__5);
				}
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Element_arr_2Context extends ParserRuleContext {
		public List<Element_arrContext> element_arr() {
			return getRuleContexts(Element_arrContext.class);
		}
		public Element_arrContext element_arr(int i) {
			return getRuleContext(Element_arrContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Element_arr_2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_element_arr_2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterElement_arr_2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitElement_arr_2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitElement_arr_2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Element_arr_2Context element_arr_2() throws RecognitionException {
		Element_arr_2Context _localctx = new Element_arr_2Context(_ctx, getState());
		enterRule(_localctx, 162, RULE_element_arr_2);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1798);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS_PLUSr) | (1L << MINUS_MINUSr) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE))) != 0) || ((((_la - 72)) & ~0x3f) == 0 && ((1L << (_la - 72)) & ((1L << (K_CURRENT_DATE - 72)) | (1L << (K_CURRENT_TIME - 72)) | (1L << (K_CURRENT_TIMESTAMP - 72)) | (1L << (K_NOT - 72)) | (1L << (K_NULL - 72)))) != 0) || ((((_la - 176)) & ~0x3f) == 0 && ((1L << (_la - 176)) & ((1L << (K_VAR - 176)) | (1L << (K_TRUE - 176)) | (1L << (K_FALSE - 176)) | (1L << (IDENTIFIER - 176)) | (1L << (NUMERIC_LITERAL - 176)) | (1L << (STRING_LITERAL - 176)) | (1L << (BLOB_LITERAL - 176)))) != 0)) {
				{
				setState(1790);
				element_arr();
				setState(1795);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1791);
					match(COMMA);
					setState(1792);
					element_arr();
					}
					}
					setState(1797);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Element_arrContext extends ParserRuleContext {
		public Bool_exContext bool_ex() {
			return getRuleContext(Bool_exContext.class,0);
		}
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public Element_arrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_element_arr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterElement_arr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitElement_arr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitElement_arr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Element_arrContext element_arr() throws RecognitionException {
		Element_arrContext _localctx = new Element_arrContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_element_arr);
		try {
			setState(1802);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_TRUE:
			case K_FALSE:
				enterOuterAlt(_localctx, 1);
				{
				setState(1800);
				bool_ex(0);
				}
				break;
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_NOT:
			case K_NULL:
			case K_VAR:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(1801);
				expr_var(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Declare_array_vrContext extends ParserRuleContext {
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public List<Element_arrContext> element_arr() {
			return getRuleContexts(Element_arrContext.class);
		}
		public Element_arrContext element_arr(int i) {
			return getRuleContext(Element_arrContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public List<Element_arr_2Context> element_arr_2() {
			return getRuleContexts(Element_arr_2Context.class);
		}
		public Element_arr_2Context element_arr_2(int i) {
			return getRuleContext(Element_arr_2Context.class,i);
		}
		public Declare_array_vrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declare_array_vr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDeclare_array_vr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDeclare_array_vr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDeclare_array_vr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Declare_array_vrContext declare_array_vr() throws RecognitionException {
		Declare_array_vrContext _localctx = new Declare_array_vrContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_declare_array_vr);
		int _la;
		try {
			setState(1842);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,196,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1804);
				match(K_VAR);
				setState(1805);
				match(IDENTIFIER);
				setState(1806);
				match(T__7);
				{
				setState(1807);
				match(ASSIGN);
				setState(1808);
				match(T__1);
				setState(1817);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS_PLUSr) | (1L << MINUS_MINUSr) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE))) != 0) || ((((_la - 72)) & ~0x3f) == 0 && ((1L << (_la - 72)) & ((1L << (K_CURRENT_DATE - 72)) | (1L << (K_CURRENT_TIME - 72)) | (1L << (K_CURRENT_TIMESTAMP - 72)) | (1L << (K_NOT - 72)) | (1L << (K_NULL - 72)))) != 0) || ((((_la - 176)) & ~0x3f) == 0 && ((1L << (_la - 176)) & ((1L << (K_VAR - 176)) | (1L << (K_TRUE - 176)) | (1L << (K_FALSE - 176)) | (1L << (IDENTIFIER - 176)) | (1L << (NUMERIC_LITERAL - 176)) | (1L << (STRING_LITERAL - 176)) | (1L << (BLOB_LITERAL - 176)))) != 0)) {
					{
					setState(1809);
					element_arr();
					setState(1814);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(1810);
						match(COMMA);
						setState(1811);
						element_arr();
						}
						}
						setState(1816);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(1819);
				match(T__2);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1820);
				match(K_VAR);
				setState(1821);
				match(IDENTIFIER);
				setState(1822);
				match(T__7);
				setState(1823);
				match(T__7);
				{
				setState(1824);
				match(ASSIGN);
				setState(1825);
				match(T__1);
				{
				setState(1826);
				match(T__1);
				setState(1827);
				element_arr_2();
				setState(1828);
				match(T__2);
				}
				setState(1837);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1830);
					match(COMMA);
					setState(1831);
					match(T__1);
					setState(1832);
					element_arr_2();
					setState(1833);
					match(T__2);
					}
					}
					setState(1839);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1840);
				match(T__2);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_whileContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public TerminalNode DIGIT() { return getToken(SQLParser.DIGIT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Expr_whileContext expr_while() {
			return getRuleContext(Expr_whileContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode LT() { return getToken(SQLParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(SQLParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(SQLParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(SQLParser.GT_EQ, 0); }
		public TerminalNode EQ() { return getToken(SQLParser.EQ, 0); }
		public TerminalNode NOT_EQ1() { return getToken(SQLParser.NOT_EQ1, 0); }
		public List<Op1Context> op1() {
			return getRuleContexts(Op1Context.class);
		}
		public Op1Context op1(int i) {
			return getRuleContext(Op1Context.class,i);
		}
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public Get_property_jsonContext get_property_json() {
			return getRuleContext(Get_property_jsonContext.class,0);
		}
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Expr_varContext expr_var() {
			return getRuleContext(Expr_varContext.class,0);
		}
		public Expr_whileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_while; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterExpr_while(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitExpr_while(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitExpr_while(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_whileContext expr_while() throws RecognitionException {
		Expr_whileContext _localctx = new Expr_whileContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_expr_while);
		int _la;
		try {
			setState(1864);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,198,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1844);
				match(NUMERIC_LITERAL);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1845);
				match(DIGIT);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1846);
				match(OPEN_PAR);
				setState(1847);
				expr_while();
				setState(1848);
				match(CLOSE_PAR);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				{
				setState(1850);
				op1();
				}
				setState(1851);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ) | (1L << EQ) | (1L << NOT_EQ1))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1862);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,197,_ctx) ) {
				case 1:
					{
					setState(1852);
					op1();
					}
					break;
				case 2:
					{
					setState(1853);
					match(IDENTIFIER);
					}
					break;
				case 3:
					{
					setState(1854);
					match(DIGIT);
					}
					break;
				case 4:
					{
					setState(1855);
					match(IDENTIFIER);
					setState(1856);
					match(T__4);
					{
					setState(1857);
					expr_var(0);
					}
					setState(1858);
					match(T__5);
					}
					break;
				case 5:
					{
					setState(1860);
					get_property_json();
					}
					break;
				case 6:
					{
					setState(1861);
					literal_value();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Op1Context extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode PLUS_EQ() { return getToken(SQLParser.PLUS_EQ, 0); }
		public TerminalNode MINUS_EQ() { return getToken(SQLParser.MINUS_EQ, 0); }
		public TerminalNode STAR_EQ() { return getToken(SQLParser.STAR_EQ, 0); }
		public TerminalNode DIV_EQ() { return getToken(SQLParser.DIV_EQ, 0); }
		public TerminalNode MOD_EQ() { return getToken(SQLParser.MOD_EQ, 0); }
		public List<TerminalNode> IDENTIFIER() { return getTokens(SQLParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(SQLParser.IDENTIFIER, i);
		}
		public List<TerminalNode> DIGIT() { return getTokens(SQLParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(SQLParser.DIGIT, i);
		}
		public List<Literal_valueContext> literal_value() {
			return getRuleContexts(Literal_valueContext.class);
		}
		public Literal_valueContext literal_value(int i) {
			return getRuleContext(Literal_valueContext.class,i);
		}
		public List<Get_property_jsonContext> get_property_json() {
			return getRuleContexts(Get_property_jsonContext.class);
		}
		public Get_property_jsonContext get_property_json(int i) {
			return getRuleContext(Get_property_jsonContext.class,i);
		}
		public Op1Context op1() {
			return getRuleContext(Op1Context.class,0);
		}
		public List<Expr_varContext> expr_var() {
			return getRuleContexts(Expr_varContext.class);
		}
		public Expr_varContext expr_var(int i) {
			return getRuleContext(Expr_varContext.class,i);
		}
		public List<TerminalNode> PLUS() { return getTokens(SQLParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(SQLParser.PLUS, i);
		}
		public List<TerminalNode> MINUS() { return getTokens(SQLParser.MINUS); }
		public TerminalNode MINUS(int i) {
			return getToken(SQLParser.MINUS, i);
		}
		public List<TerminalNode> STAR() { return getTokens(SQLParser.STAR); }
		public TerminalNode STAR(int i) {
			return getToken(SQLParser.STAR, i);
		}
		public List<TerminalNode> DIV() { return getTokens(SQLParser.DIV); }
		public TerminalNode DIV(int i) {
			return getToken(SQLParser.DIV, i);
		}
		public List<TerminalNode> MOD() { return getTokens(SQLParser.MOD); }
		public TerminalNode MOD(int i) {
			return getToken(SQLParser.MOD, i);
		}
		public List<Incr_decr_without_commaContext> incr_decr_without_comma() {
			return getRuleContexts(Incr_decr_without_commaContext.class);
		}
		public Incr_decr_without_commaContext incr_decr_without_comma(int i) {
			return getRuleContext(Incr_decr_without_commaContext.class,i);
		}
		public Op1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_op1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOp1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOp1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitOp1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Op1Context op1() throws RecognitionException {
		Op1Context _localctx = new Op1Context(_ctx, getState());
		enterRule(_localctx, 170, RULE_op1);
		int _la;
		try {
			int _alt;
			setState(1945);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,206,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1866);
				match(OPEN_PAR);
				setState(1876);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,199,_ctx) ) {
				case 1:
					{
					setState(1867);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1868);
					match(DIGIT);
					}
					break;
				case 3:
					{
					setState(1869);
					literal_value();
					}
					break;
				case 4:
					{
					setState(1870);
					match(IDENTIFIER);
					setState(1871);
					match(T__4);
					{
					setState(1872);
					expr_var(0);
					}
					setState(1873);
					match(T__5);
					}
					break;
				case 5:
					{
					setState(1875);
					get_property_json();
					}
					break;
				}
				setState(1878);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS_EQ) | (1L << MINUS_EQ) | (1L << STAR_EQ) | (1L << DIV_EQ) | (1L << MOD_EQ))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				{
				setState(1879);
				op1();
				}
				setState(1880);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1897);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,201,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1892);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,200,_ctx) ) {
						case 1:
							{
							setState(1882);
							match(IDENTIFIER);
							}
							break;
						case 2:
							{
							setState(1883);
							match(DIGIT);
							}
							break;
						case 3:
							{
							setState(1884);
							literal_value();
							}
							break;
						case 4:
							{
							setState(1885);
							match(IDENTIFIER);
							setState(1886);
							match(T__4);
							{
							setState(1887);
							expr_var(0);
							}
							setState(1888);
							match(T__5);
							}
							break;
						case 5:
							{
							setState(1890);
							get_property_json();
							}
							break;
						case 6:
							{
							setState(1891);
							incr_decr_without_comma();
							}
							break;
						}
						setState(1894);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						} 
					}
					setState(1899);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,201,_ctx);
				}
				setState(1925); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(1910);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,202,_ctx) ) {
					case 1:
						{
						setState(1900);
						match(IDENTIFIER);
						}
						break;
					case 2:
						{
						setState(1901);
						match(DIGIT);
						}
						break;
					case 3:
						{
						setState(1902);
						literal_value();
						}
						break;
					case 4:
						{
						setState(1903);
						match(IDENTIFIER);
						setState(1904);
						match(T__4);
						{
						setState(1905);
						expr_var(0);
						}
						setState(1906);
						match(T__5);
						}
						break;
					case 5:
						{
						setState(1908);
						get_property_json();
						}
						break;
					case 6:
						{
						setState(1909);
						incr_decr_without_comma();
						}
						break;
					}
					setState(1912);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << DIV) | (1L << MOD))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(1923);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,203,_ctx) ) {
					case 1:
						{
						setState(1913);
						match(IDENTIFIER);
						}
						break;
					case 2:
						{
						setState(1914);
						match(DIGIT);
						}
						break;
					case 3:
						{
						setState(1915);
						literal_value();
						}
						break;
					case 4:
						{
						setState(1916);
						match(IDENTIFIER);
						setState(1917);
						match(T__4);
						{
						setState(1918);
						expr_var(0);
						}
						setState(1919);
						match(T__5);
						}
						break;
					case 5:
						{
						setState(1921);
						get_property_json();
						}
						break;
					case 6:
						{
						setState(1922);
						incr_decr_without_comma();
						}
						break;
					}
					}
					}
					setState(1927); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==PLUS_PLUSr || _la==MINUS_MINUSr || ((((_la - 72)) & ~0x3f) == 0 && ((1L << (_la - 72)) & ((1L << (K_CURRENT_DATE - 72)) | (1L << (K_CURRENT_TIME - 72)) | (1L << (K_CURRENT_TIMESTAMP - 72)) | (1L << (K_NULL - 72)))) != 0) || ((((_la - 184)) & ~0x3f) == 0 && ((1L << (_la - 184)) & ((1L << (IDENTIFIER - 184)) | (1L << (NUMERIC_LITERAL - 184)) | (1L << (STRING_LITERAL - 184)) | (1L << (BLOB_LITERAL - 184)) | (1L << (DIGIT - 184)))) != 0) );
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1939);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,205,_ctx) ) {
				case 1:
					{
					setState(1929);
					match(IDENTIFIER);
					}
					break;
				case 2:
					{
					setState(1930);
					match(DIGIT);
					}
					break;
				case 3:
					{
					setState(1931);
					literal_value();
					}
					break;
				case 4:
					{
					setState(1932);
					match(IDENTIFIER);
					setState(1933);
					match(T__4);
					{
					setState(1934);
					expr_var(0);
					}
					setState(1935);
					match(T__5);
					}
					break;
				case 5:
					{
					setState(1937);
					get_property_json();
					}
					break;
				case 6:
					{
					setState(1938);
					incr_decr_without_comma();
					}
					break;
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1941);
				match(OPEN_PAR);
				setState(1942);
				op1();
				setState(1943);
				match(CLOSE_PAR);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_stmtContext extends ParserRuleContext {
		public TerminalNode K_WHILE() { return getToken(SQLParser.K_WHILE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Condition_headerContext condition_header() {
			return getRuleContext(Condition_headerContext.class,0);
		}
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public Body_without_bracketaContext body_without_bracketa() {
			return getRuleContext(Body_without_bracketaContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public While_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterWhile_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitWhile_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitWhile_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_stmtContext while_stmt() throws RecognitionException {
		While_stmtContext _localctx = new While_stmtContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_while_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1947);
			match(K_WHILE);
			setState(1948);
			match(OPEN_PAR);
			{
			setState(1949);
			condition_header(0);
			}
			setState(1950);
			match(CLOSE_PAR);
			setState(1957);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,207,_ctx) ) {
			case 1:
				{
				setState(1951);
				match(T__1);
				setState(1952);
				body();
				setState(1953);
				match(T__2);
				}
				break;
			case 2:
				{
				setState(1955);
				body_without_bracketa();
				}
				break;
			case 3:
				{
				setState(1956);
				match(SCOL);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Do_while_stmtContext extends ParserRuleContext {
		public TerminalNode K_DO() { return getToken(SQLParser.K_DO, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public TerminalNode K_WHILE() { return getToken(SQLParser.K_WHILE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode SCOL() { return getToken(SQLParser.SCOL, 0); }
		public Body_without_bracketaContext body_without_bracketa() {
			return getRuleContext(Body_without_bracketaContext.class,0);
		}
		public Condition_headerContext condition_header() {
			return getRuleContext(Condition_headerContext.class,0);
		}
		public Do_while_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_do_while_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDo_while_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDo_while_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDo_while_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Do_while_stmtContext do_while_stmt() throws RecognitionException {
		Do_while_stmtContext _localctx = new Do_while_stmtContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_do_while_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1959);
			match(K_DO);
			setState(1976);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__1:
				{
				setState(1960);
				match(T__1);
				setState(1961);
				body();
				setState(1962);
				match(T__2);
				setState(1963);
				match(K_WHILE);
				setState(1964);
				match(OPEN_PAR);
				{
				setState(1965);
				condition_header(0);
				}
				setState(1966);
				match(CLOSE_PAR);
				setState(1967);
				match(SCOL);
				}
				break;
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case SCOL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_ALTER:
			case K_CREATE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DELETE:
			case K_DROP:
			case K_EXISTS:
			case K_FOR:
			case K_IF:
			case K_INSERT:
			case K_NOT:
			case K_NULL:
			case K_SELECT:
			case K_UPDATE:
			case K_VALUES:
			case K_SWITCH:
			case K_RETURN:
			case K_VAR:
			case K_PRINT:
			case K_TRUE:
			case K_FALSE:
			case K_WHILE:
			case K_DO:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(1969);
				body_without_bracketa();
				setState(1970);
				match(K_WHILE);
				setState(1971);
				match(OPEN_PAR);
				{
				setState(1972);
				condition_header(0);
				}
				setState(1973);
				match(CLOSE_PAR);
				setState(1974);
				match(SCOL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Call_funcContext extends ParserRuleContext {
		public Fun_nameContext fun_name() {
			return getRuleContext(Fun_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Argument_listContext argument_list() {
			return getRuleContext(Argument_listContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Call_funcContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call_func; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCall_func(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCall_func(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCall_func(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Call_funcContext call_func() throws RecognitionException {
		Call_funcContext _localctx = new Call_funcContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_call_func);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1981);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(1978);
				match(K_VAR);
				setState(1979);
				match(IDENTIFIER);
				setState(1980);
				match(ASSIGN);
				}
			}

			setState(1983);
			fun_name();
			setState(1984);
			match(OPEN_PAR);
			setState(1985);
			argument_list();
			setState(1986);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Aggregation_funcContext extends ParserRuleContext {
		public Fun_nameContext fun_name() {
			return getRuleContext(Fun_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Aggr_argument_listContext aggr_argument_list() {
			return getRuleContext(Aggr_argument_listContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode ASSIGN() { return getToken(SQLParser.ASSIGN, 0); }
		public Aggregation_funcContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregation_func; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAggregation_func(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAggregation_func(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAggregation_func(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Aggregation_funcContext aggregation_func() throws RecognitionException {
		Aggregation_funcContext _localctx = new Aggregation_funcContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_aggregation_func);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1991);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(1988);
				match(K_VAR);
				setState(1989);
				match(IDENTIFIER);
				setState(1990);
				match(ASSIGN);
				}
			}

			setState(1993);
			fun_name();
			setState(1994);
			match(OPEN_PAR);
			setState(1995);
			aggr_argument_list();
			setState(1996);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Heigher_order_functionContext extends ParserRuleContext {
		public TerminalNode K_FUNCTION() { return getToken(SQLParser.K_FUNCTION, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Args_list_higherContext args_list_higher() {
			return getRuleContext(Args_list_higherContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public Heigher_order_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_heigher_order_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterHeigher_order_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitHeigher_order_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitHeigher_order_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Heigher_order_functionContext heigher_order_function() throws RecognitionException {
		Heigher_order_functionContext _localctx = new Heigher_order_functionContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_heigher_order_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1998);
			match(K_FUNCTION);
			setState(1999);
			match(OPEN_PAR);
			setState(2000);
			args_list_higher();
			setState(2001);
			match(CLOSE_PAR);
			setState(2002);
			body();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Args_list_higherContext extends ParserRuleContext {
		public List<Bool_exContext> bool_ex() {
			return getRuleContexts(Bool_exContext.class);
		}
		public Bool_exContext bool_ex(int i) {
			return getRuleContext(Bool_exContext.class,i);
		}
		public List<Expr_varContext> expr_var() {
			return getRuleContexts(Expr_varContext.class);
		}
		public Expr_varContext expr_var(int i) {
			return getRuleContext(Expr_varContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Args_list_higherContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_args_list_higher; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterArgs_list_higher(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitArgs_list_higher(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitArgs_list_higher(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Args_list_higherContext args_list_higher() throws RecognitionException {
		Args_list_higherContext _localctx = new Args_list_higherContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_args_list_higher);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2018);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS_PLUSr) | (1L << MINUS_MINUSr) | (1L << OPEN_PAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE))) != 0) || ((((_la - 72)) & ~0x3f) == 0 && ((1L << (_la - 72)) & ((1L << (K_CURRENT_DATE - 72)) | (1L << (K_CURRENT_TIME - 72)) | (1L << (K_CURRENT_TIMESTAMP - 72)) | (1L << (K_NOT - 72)) | (1L << (K_NULL - 72)))) != 0) || ((((_la - 176)) & ~0x3f) == 0 && ((1L << (_la - 176)) & ((1L << (K_VAR - 176)) | (1L << (K_TRUE - 176)) | (1L << (K_FALSE - 176)) | (1L << (IDENTIFIER - 176)) | (1L << (NUMERIC_LITERAL - 176)) | (1L << (STRING_LITERAL - 176)) | (1L << (BLOB_LITERAL - 176)))) != 0)) {
				{
				setState(2006);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_TRUE:
				case K_FALSE:
					{
					setState(2004);
					bool_ex(0);
					}
					break;
				case PLUS_PLUSr:
				case MINUS_MINUSr:
				case OPEN_PAR:
				case PLUS:
				case MINUS:
				case TILDE:
				case K_CURRENT_DATE:
				case K_CURRENT_TIME:
				case K_CURRENT_TIMESTAMP:
				case K_NOT:
				case K_NULL:
				case K_VAR:
				case IDENTIFIER:
				case NUMERIC_LITERAL:
				case STRING_LITERAL:
				case BLOB_LITERAL:
					{
					setState(2005);
					expr_var(0);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(2015);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(2008);
					match(COMMA);
					setState(2011);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case K_TRUE:
					case K_FALSE:
						{
						setState(2009);
						bool_ex(0);
						}
						break;
					case PLUS_PLUSr:
					case MINUS_MINUSr:
					case OPEN_PAR:
					case PLUS:
					case MINUS:
					case TILDE:
					case K_CURRENT_DATE:
					case K_CURRENT_TIME:
					case K_CURRENT_TIMESTAMP:
					case K_NOT:
					case K_NULL:
					case K_VAR:
					case IDENTIFIER:
					case NUMERIC_LITERAL:
					case STRING_LITERAL:
					case BLOB_LITERAL:
						{
						setState(2010);
						expr_var(0);
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					}
					setState(2017);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Argument_listContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public List<Expr_varContext> expr_var() {
			return getRuleContexts(Expr_varContext.class);
		}
		public Expr_varContext expr_var(int i) {
			return getRuleContext(Expr_varContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Heigher_order_functionContext heigher_order_function() {
			return getRuleContext(Heigher_order_functionContext.class,0);
		}
		public Argument_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argument_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterArgument_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitArgument_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitArgument_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Argument_listContext argument_list() throws RecognitionException {
		Argument_listContext _localctx = new Argument_listContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_argument_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2033);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_NOT:
			case K_NULL:
			case K_VAR:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				{
				setState(2020);
				expr_var(0);
				}
				setState(2025);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(2021);
					match(COMMA);
					{
					setState(2022);
					expr_var(0);
					}
					}
					}
					setState(2027);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case STAR:
				{
				setState(2028);
				match(STAR);
				setState(2031);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COMMA) {
					{
					setState(2029);
					match(COMMA);
					setState(2030);
					heigher_order_function();
					}
				}

				}
				break;
			case CLOSE_PAR:
				break;
			default:
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Aggr_argument_listContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Aggr_argument_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggr_argument_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAggr_argument_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAggr_argument_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAggr_argument_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Aggr_argument_listContext aggr_argument_list() throws RecognitionException {
		Aggr_argument_listContext _localctx = new Aggr_argument_listContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_aggr_argument_list);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2047);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS_PLUSr:
			case MINUS_MINUSr:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DISTINCT:
			case K_EXISTS:
			case K_NOT:
			case K_NULL:
			case K_VAR:
			case K_TRUE:
			case K_FALSE:
			case IDENTIFIER:
			case NUMERIC_LITERAL:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(2036);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_DISTINCT) {
					{
					setState(2035);
					match(K_DISTINCT);
					}
				}

				{
				setState(2038);
				expr(0);
				}
				setState(2043);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(2039);
					match(COMMA);
					{
					setState(2040);
					expr(0);
					}
					}
					}
					setState(2045);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case STAR:
				{
				setState(2046);
				match(STAR);
				}
				break;
			case CLOSE_PAR:
				break;
			default:
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bool_exContext extends ParserRuleContext {
		public TerminalNode K_TRUE() { return getToken(SQLParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(SQLParser.K_FALSE, 0); }
		public List<Bool_exContext> bool_ex() {
			return getRuleContexts(Bool_exContext.class);
		}
		public Bool_exContext bool_ex(int i) {
			return getRuleContext(Bool_exContext.class,i);
		}
		public TerminalNode AMP() { return getToken(SQLParser.AMP, 0); }
		public TerminalNode PIPE() { return getToken(SQLParser.PIPE, 0); }
		public Bool_exContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool_ex; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterBool_ex(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitBool_ex(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitBool_ex(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bool_exContext bool_ex() throws RecognitionException {
		return bool_ex(0);
	}

	private Bool_exContext bool_ex(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Bool_exContext _localctx = new Bool_exContext(_ctx, _parentState);
		Bool_exContext _prevctx = _localctx;
		int _startState = 188;
		enterRecursionRule(_localctx, 188, RULE_bool_ex, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2052);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_TRUE:
				{
				setState(2050);
				match(K_TRUE);
				}
				break;
			case K_FALSE:
				{
				setState(2051);
				match(K_FALSE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(2059);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,222,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Bool_exContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_bool_ex);
					setState(2054);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(2055);
					_la = _input.LA(1);
					if ( !(_la==AMP || _la==PIPE) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(2056);
					bool_ex(2);
					}
					} 
				}
				setState(2061);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,222,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Foreign_key_clauseContext extends ParserRuleContext {
		public TerminalNode K_REFERENCES() { return getToken(SQLParser.K_REFERENCES, 0); }
		public Foreign_tableContext foreign_table() {
			return getRuleContext(Foreign_tableContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Fk_target_column_nameContext> fk_target_column_name() {
			return getRuleContexts(Fk_target_column_nameContext.class);
		}
		public Fk_target_column_nameContext fk_target_column_name(int i) {
			return getRuleContext(Fk_target_column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_DEFERRABLE() { return getToken(SQLParser.K_DEFERRABLE, 0); }
		public List<TerminalNode> K_ON() { return getTokens(SQLParser.K_ON); }
		public TerminalNode K_ON(int i) {
			return getToken(SQLParser.K_ON, i);
		}
		public List<TerminalNode> K_MATCH() { return getTokens(SQLParser.K_MATCH); }
		public TerminalNode K_MATCH(int i) {
			return getToken(SQLParser.K_MATCH, i);
		}
		public List<NameContext> name() {
			return getRuleContexts(NameContext.class);
		}
		public NameContext name(int i) {
			return getRuleContext(NameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public List<TerminalNode> K_DELETE() { return getTokens(SQLParser.K_DELETE); }
		public TerminalNode K_DELETE(int i) {
			return getToken(SQLParser.K_DELETE, i);
		}
		public List<TerminalNode> K_UPDATE() { return getTokens(SQLParser.K_UPDATE); }
		public TerminalNode K_UPDATE(int i) {
			return getToken(SQLParser.K_UPDATE, i);
		}
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_INITIALLY() { return getToken(SQLParser.K_INITIALLY, 0); }
		public TerminalNode K_DEFERRED() { return getToken(SQLParser.K_DEFERRED, 0); }
		public TerminalNode K_IMMEDIATE() { return getToken(SQLParser.K_IMMEDIATE, 0); }
		public TerminalNode K_ENABLE() { return getToken(SQLParser.K_ENABLE, 0); }
		public List<TerminalNode> K_SET() { return getTokens(SQLParser.K_SET); }
		public TerminalNode K_SET(int i) {
			return getToken(SQLParser.K_SET, i);
		}
		public List<TerminalNode> K_NULL() { return getTokens(SQLParser.K_NULL); }
		public TerminalNode K_NULL(int i) {
			return getToken(SQLParser.K_NULL, i);
		}
		public List<TerminalNode> K_DEFAULT() { return getTokens(SQLParser.K_DEFAULT); }
		public TerminalNode K_DEFAULT(int i) {
			return getToken(SQLParser.K_DEFAULT, i);
		}
		public List<TerminalNode> K_CASCADE() { return getTokens(SQLParser.K_CASCADE); }
		public TerminalNode K_CASCADE(int i) {
			return getToken(SQLParser.K_CASCADE, i);
		}
		public List<TerminalNode> K_RESTRICT() { return getTokens(SQLParser.K_RESTRICT); }
		public TerminalNode K_RESTRICT(int i) {
			return getToken(SQLParser.K_RESTRICT, i);
		}
		public List<TerminalNode> K_NO() { return getTokens(SQLParser.K_NO); }
		public TerminalNode K_NO(int i) {
			return getToken(SQLParser.K_NO, i);
		}
		public List<TerminalNode> K_ACTION() { return getTokens(SQLParser.K_ACTION); }
		public TerminalNode K_ACTION(int i) {
			return getToken(SQLParser.K_ACTION, i);
		}
		public Foreign_key_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_key_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterForeign_key_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitForeign_key_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitForeign_key_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_key_clauseContext foreign_key_clause() throws RecognitionException {
		Foreign_key_clauseContext _localctx = new Foreign_key_clauseContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_foreign_key_clause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2062);
			match(K_REFERENCES);
			setState(2066);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,223,_ctx) ) {
			case 1:
				{
				setState(2063);
				database_name();
				setState(2064);
				match(DOT);
				}
				break;
			}
			setState(2068);
			foreign_table();
			setState(2080);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,225,_ctx) ) {
			case 1:
				{
				setState(2069);
				match(OPEN_PAR);
				setState(2070);
				fk_target_column_name();
				setState(2075);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(2071);
					match(COMMA);
					setState(2072);
					fk_target_column_name();
					}
					}
					setState(2077);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(2078);
				match(CLOSE_PAR);
				}
				break;
			}
			setState(2100);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_MATCH || _la==K_ON) {
				{
				{
				setState(2096);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_ON:
					{
					setState(2082);
					match(K_ON);
					setState(2083);
					_la = _input.LA(1);
					if ( !(_la==K_DELETE || _la==K_UPDATE) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(2092);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,226,_ctx) ) {
					case 1:
						{
						setState(2084);
						match(K_SET);
						setState(2085);
						match(K_NULL);
						}
						break;
					case 2:
						{
						setState(2086);
						match(K_SET);
						setState(2087);
						match(K_DEFAULT);
						}
						break;
					case 3:
						{
						setState(2088);
						match(K_CASCADE);
						}
						break;
					case 4:
						{
						setState(2089);
						match(K_RESTRICT);
						}
						break;
					case 5:
						{
						setState(2090);
						match(K_NO);
						setState(2091);
						match(K_ACTION);
						}
						break;
					}
					}
					break;
				case K_MATCH:
					{
					setState(2094);
					match(K_MATCH);
					setState(2095);
					name();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				setState(2102);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2116);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,232,_ctx) ) {
			case 1:
				{
				setState(2104);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_NOT) {
					{
					setState(2103);
					match(K_NOT);
					}
				}

				setState(2106);
				match(K_DEFERRABLE);
				setState(2111);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,230,_ctx) ) {
				case 1:
					{
					setState(2107);
					match(K_INITIALLY);
					setState(2108);
					match(K_DEFERRED);
					}
					break;
				case 2:
					{
					setState(2109);
					match(K_INITIALLY);
					setState(2110);
					match(K_IMMEDIATE);
					}
					break;
				}
				setState(2114);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_ENABLE) {
					{
					setState(2113);
					match(K_ENABLE);
					}
				}

				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fk_target_column_nameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Fk_target_column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fk_target_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFk_target_column_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFk_target_column_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFk_target_column_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fk_target_column_nameContext fk_target_column_name() throws RecognitionException {
		Fk_target_column_nameContext _localctx = new Fk_target_column_nameContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_fk_target_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2118);
			name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Indexed_columnContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(SQLParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(SQLParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(SQLParser.K_DESC, 0); }
		public Indexed_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexed_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIndexed_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIndexed_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIndexed_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Indexed_columnContext indexed_column() throws RecognitionException {
		Indexed_columnContext _localctx = new Indexed_columnContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_indexed_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2120);
			column_name();
			setState(2123);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_COLLATE) {
				{
				setState(2121);
				match(K_COLLATE);
				setState(2122);
				collation_name();
				}
			}

			setState(2126);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(2125);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraintContext extends ParserRuleContext {
		public Table_constraint_primary_keyContext table_constraint_primary_key() {
			return getRuleContext(Table_constraint_primary_keyContext.class,0);
		}
		public Table_constraint_keyContext table_constraint_key() {
			return getRuleContext(Table_constraint_keyContext.class,0);
		}
		public Table_constraint_uniqueContext table_constraint_unique() {
			return getRuleContext(Table_constraint_uniqueContext.class,0);
		}
		public TerminalNode K_CHECK() { return getToken(SQLParser.K_CHECK, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Table_constraint_foreign_keyContext table_constraint_foreign_key() {
			return getRuleContext(Table_constraint_foreign_keyContext.class,0);
		}
		public TerminalNode K_CONSTRAINT() { return getToken(SQLParser.K_CONSTRAINT, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Table_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraintContext table_constraint() throws RecognitionException {
		Table_constraintContext _localctx = new Table_constraintContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_table_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2130);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_CONSTRAINT) {
				{
				setState(2128);
				match(K_CONSTRAINT);
				setState(2129);
				name();
				}
			}

			setState(2141);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_PRIMARY:
				{
				setState(2132);
				table_constraint_primary_key();
				}
				break;
			case K_KEY:
				{
				setState(2133);
				table_constraint_key();
				}
				break;
			case K_UNIQUE:
				{
				setState(2134);
				table_constraint_unique();
				}
				break;
			case K_CHECK:
				{
				setState(2135);
				match(K_CHECK);
				setState(2136);
				match(OPEN_PAR);
				setState(2137);
				expr(0);
				setState(2138);
				match(CLOSE_PAR);
				}
				break;
			case K_FOREIGN:
				{
				setState(2140);
				table_constraint_foreign_key();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_primary_keyContext extends ParserRuleContext {
		public TerminalNode K_PRIMARY() { return getToken(SQLParser.K_PRIMARY, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Table_constraint_primary_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_primary_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint_primary_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint_primary_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint_primary_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_primary_keyContext table_constraint_primary_key() throws RecognitionException {
		Table_constraint_primary_keyContext _localctx = new Table_constraint_primary_keyContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_table_constraint_primary_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2143);
			match(K_PRIMARY);
			setState(2144);
			match(K_KEY);
			setState(2145);
			match(OPEN_PAR);
			setState(2146);
			indexed_column();
			setState(2151);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(2147);
				match(COMMA);
				setState(2148);
				indexed_column();
				}
				}
				setState(2153);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2154);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_foreign_keyContext extends ParserRuleContext {
		public TerminalNode K_FOREIGN() { return getToken(SQLParser.K_FOREIGN, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Fk_origin_column_nameContext> fk_origin_column_name() {
			return getRuleContexts(Fk_origin_column_nameContext.class);
		}
		public Fk_origin_column_nameContext fk_origin_column_name(int i) {
			return getRuleContext(Fk_origin_column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Foreign_key_clauseContext foreign_key_clause() {
			return getRuleContext(Foreign_key_clauseContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Table_constraint_foreign_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_foreign_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint_foreign_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint_foreign_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint_foreign_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_foreign_keyContext table_constraint_foreign_key() throws RecognitionException {
		Table_constraint_foreign_keyContext _localctx = new Table_constraint_foreign_keyContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_table_constraint_foreign_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2156);
			match(K_FOREIGN);
			setState(2157);
			match(K_KEY);
			setState(2158);
			match(OPEN_PAR);
			setState(2159);
			fk_origin_column_name();
			setState(2164);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(2160);
				match(COMMA);
				setState(2161);
				fk_origin_column_name();
				}
				}
				setState(2166);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2167);
			match(CLOSE_PAR);
			setState(2168);
			foreign_key_clause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_uniqueContext extends ParserRuleContext {
		public TerminalNode K_UNIQUE() { return getToken(SQLParser.K_UNIQUE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Table_constraint_uniqueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_unique; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint_unique(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint_unique(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint_unique(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_uniqueContext table_constraint_unique() throws RecognitionException {
		Table_constraint_uniqueContext _localctx = new Table_constraint_uniqueContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_table_constraint_unique);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2170);
			match(K_UNIQUE);
			setState(2172);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_KEY) {
				{
				setState(2171);
				match(K_KEY);
				}
			}

			setState(2175);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,240,_ctx) ) {
			case 1:
				{
				setState(2174);
				name();
				}
				break;
			}
			setState(2177);
			match(OPEN_PAR);
			setState(2178);
			indexed_column();
			setState(2183);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(2179);
				match(COMMA);
				setState(2180);
				indexed_column();
				}
				}
				setState(2185);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2186);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_keyContext extends ParserRuleContext {
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Table_constraint_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_constraint_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_constraint_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_constraint_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_keyContext table_constraint_key() throws RecognitionException {
		Table_constraint_keyContext _localctx = new Table_constraint_keyContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_table_constraint_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2188);
			match(K_KEY);
			setState(2190);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,242,_ctx) ) {
			case 1:
				{
				setState(2189);
				name();
				}
				break;
			}
			setState(2192);
			match(OPEN_PAR);
			setState(2193);
			indexed_column();
			setState(2198);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(2194);
				match(COMMA);
				setState(2195);
				indexed_column();
				}
				}
				setState(2200);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2201);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fk_origin_column_nameContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Fk_origin_column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fk_origin_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFk_origin_column_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFk_origin_column_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFk_origin_column_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fk_origin_column_nameContext fk_origin_column_name() throws RecognitionException {
		Fk_origin_column_nameContext _localctx = new Fk_origin_column_nameContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_fk_origin_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2203);
			column_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Qualified_table_nameContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public TerminalNode K_INDEXED() { return getToken(SQLParser.K_INDEXED, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public Qualified_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualified_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterQualified_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitQualified_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitQualified_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Qualified_table_nameContext qualified_table_name() throws RecognitionException {
		Qualified_table_nameContext _localctx = new Qualified_table_nameContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_qualified_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2208);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,244,_ctx) ) {
			case 1:
				{
				setState(2205);
				database_name();
				setState(2206);
				match(DOT);
				}
				break;
			}
			setState(2210);
			table_name();
			setState(2216);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,245,_ctx) ) {
			case 1:
				{
				setState(2211);
				match(K_INDEXED);
				setState(2212);
				match(K_BY);
				setState(2213);
				index_name();
				}
				break;
			case 2:
				{
				setState(2214);
				match(K_NOT);
				setState(2215);
				match(K_INDEXED);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ordering_termContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(SQLParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(SQLParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(SQLParser.K_DESC, 0); }
		public Ordering_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordering_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterOrdering_term(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitOrdering_term(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitOrdering_term(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ordering_termContext ordering_term() throws RecognitionException {
		Ordering_termContext _localctx = new Ordering_termContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_ordering_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2218);
			expr(0);
			setState(2221);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_COLLATE) {
				{
				setState(2219);
				match(K_COLLATE);
				setState(2220);
				collation_name();
				}
			}

			setState(2224);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(2223);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pragma_valueContext extends ParserRuleContext {
		public Signed_numberContext signed_number() {
			return getRuleContext(Signed_numberContext.class,0);
		}
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public Pragma_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pragma_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterPragma_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitPragma_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitPragma_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Pragma_valueContext pragma_value() throws RecognitionException {
		Pragma_valueContext _localctx = new Pragma_valueContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_pragma_value);
		try {
			setState(2229);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,248,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2226);
				signed_number();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2227);
				name();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2228);
				match(STRING_LITERAL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Common_table_expressionContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(SQLParser.K_AS, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(SQLParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SQLParser.OPEN_PAR, i);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SQLParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SQLParser.CLOSE_PAR, i);
		}
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Common_table_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_common_table_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCommon_table_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCommon_table_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCommon_table_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Common_table_expressionContext common_table_expression() throws RecognitionException {
		Common_table_expressionContext _localctx = new Common_table_expressionContext(_ctx, getState());
		enterRule(_localctx, 214, RULE_common_table_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2231);
			table_name();
			setState(2243);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPEN_PAR) {
				{
				setState(2232);
				match(OPEN_PAR);
				setState(2233);
				column_name();
				setState(2238);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(2234);
					match(COMMA);
					setState(2235);
					column_name();
					}
					}
					setState(2240);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(2241);
				match(CLOSE_PAR);
				}
			}

			setState(2245);
			match(K_AS);
			setState(2246);
			match(OPEN_PAR);
			setState(2247);
			select_stmt();
			setState(2248);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Result_columnContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Column_aliasContext column_alias() {
			return getRuleContext(Column_aliasContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(SQLParser.K_AS, 0); }
		public Result_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_result_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterResult_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitResult_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitResult_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Result_columnContext result_column() throws RecognitionException {
		Result_columnContext _localctx = new Result_columnContext(_ctx, getState());
		enterRule(_localctx, 216, RULE_result_column);
		int _la;
		try {
			setState(2262);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,253,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2250);
				match(STAR);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2251);
				table_name();
				setState(2252);
				match(DOT);
				setState(2253);
				match(STAR);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2255);
				expr(0);
				setState(2260);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,252,_ctx) ) {
				case 1:
					{
					setState(2257);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(2256);
						match(K_AS);
						}
					}

					setState(2259);
					column_alias();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_or_subqueryContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(SQLParser.DOT, 0); }
		public Table_aliasContext table_alias() {
			return getRuleContext(Table_aliasContext.class,0);
		}
		public TerminalNode K_INDEXED() { return getToken(SQLParser.K_INDEXED, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_AS() { return getToken(SQLParser.K_AS, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public Join_clauseContext join_clause() {
			return getRuleContext(Join_clauseContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public Table_or_subqueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_or_subquery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_or_subquery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_or_subquery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_or_subquery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_or_subqueryContext table_or_subquery() throws RecognitionException {
		Table_or_subqueryContext _localctx = new Table_or_subqueryContext(_ctx, getState());
		enterRule(_localctx, 218, RULE_table_or_subquery);
		int _la;
		try {
			setState(2311);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,264,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2267);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,254,_ctx) ) {
				case 1:
					{
					setState(2264);
					database_name();
					setState(2265);
					match(DOT);
					}
					break;
				}
				setState(2269);
				table_name();
				setState(2274);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,256,_ctx) ) {
				case 1:
					{
					setState(2271);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(2270);
						match(K_AS);
						}
					}

					setState(2273);
					table_alias();
					}
					break;
				}
				setState(2281);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,257,_ctx) ) {
				case 1:
					{
					setState(2276);
					match(K_INDEXED);
					setState(2277);
					match(K_BY);
					setState(2278);
					index_name();
					}
					break;
				case 2:
					{
					setState(2279);
					match(K_NOT);
					setState(2280);
					match(K_INDEXED);
					}
					break;
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2283);
				match(OPEN_PAR);
				setState(2293);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,259,_ctx) ) {
				case 1:
					{
					setState(2284);
					table_or_subquery();
					setState(2289);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==COMMA) {
						{
						{
						setState(2285);
						match(COMMA);
						setState(2286);
						table_or_subquery();
						}
						}
						setState(2291);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				case 2:
					{
					setState(2292);
					join_clause();
					}
					break;
				}
				setState(2295);
				match(CLOSE_PAR);
				setState(2300);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,261,_ctx) ) {
				case 1:
					{
					setState(2297);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(2296);
						match(K_AS);
						}
					}

					setState(2299);
					table_alias();
					}
					break;
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2302);
				match(OPEN_PAR);
				setState(2303);
				factored_select_stmt();
				setState(2304);
				match(CLOSE_PAR);
				setState(2309);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,263,_ctx) ) {
				case 1:
					{
					setState(2306);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_AS) {
						{
						setState(2305);
						match(K_AS);
						}
					}

					setState(2308);
					table_alias();
					}
					break;
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_clauseContext extends ParserRuleContext {
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public List<Join_operatorContext> join_operator() {
			return getRuleContexts(Join_operatorContext.class);
		}
		public Join_operatorContext join_operator(int i) {
			return getRuleContext(Join_operatorContext.class,i);
		}
		public List<Join_constraintContext> join_constraint() {
			return getRuleContexts(Join_constraintContext.class);
		}
		public Join_constraintContext join_constraint(int i) {
			return getRuleContext(Join_constraintContext.class,i);
		}
		public Join_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJoin_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJoin_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJoin_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_clauseContext join_clause() throws RecognitionException {
		Join_clauseContext _localctx = new Join_clauseContext(_ctx, getState());
		enterRule(_localctx, 220, RULE_join_clause);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2313);
			table_or_subquery();
			setState(2320);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,265,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(2314);
					join_operator();
					setState(2315);
					table_or_subquery();
					setState(2316);
					join_constraint();
					}
					} 
				}
				setState(2322);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,265,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_operatorContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(SQLParser.COMMA, 0); }
		public TerminalNode K_JOIN() { return getToken(SQLParser.K_JOIN, 0); }
		public TerminalNode K_LEFT() { return getToken(SQLParser.K_LEFT, 0); }
		public TerminalNode K_INNER() { return getToken(SQLParser.K_INNER, 0); }
		public TerminalNode K_OUTER() { return getToken(SQLParser.K_OUTER, 0); }
		public Join_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJoin_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJoin_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJoin_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_operatorContext join_operator() throws RecognitionException {
		Join_operatorContext _localctx = new Join_operatorContext(_ctx, getState());
		enterRule(_localctx, 222, RULE_join_operator);
		int _la;
		try {
			setState(2332);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COMMA:
				enterOuterAlt(_localctx, 1);
				{
				setState(2323);
				match(COMMA);
				}
				break;
			case K_INNER:
			case K_JOIN:
			case K_LEFT:
				enterOuterAlt(_localctx, 2);
				{
				setState(2329);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_LEFT:
					{
					setState(2324);
					match(K_LEFT);
					setState(2326);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_OUTER) {
						{
						setState(2325);
						match(K_OUTER);
						}
					}

					}
					break;
				case K_INNER:
					{
					setState(2328);
					match(K_INNER);
					}
					break;
				case K_JOIN:
					break;
				default:
					break;
				}
				setState(2331);
				match(K_JOIN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_constraintContext extends ParserRuleContext {
		public TerminalNode K_ON() { return getToken(SQLParser.K_ON, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Join_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterJoin_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitJoin_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitJoin_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_constraintContext join_constraint() throws RecognitionException {
		Join_constraintContext _localctx = new Join_constraintContext(_ctx, getState());
		enterRule(_localctx, 224, RULE_join_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2336);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ON) {
				{
				setState(2334);
				match(K_ON);
				setState(2335);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_coreContext extends ParserRuleContext {
		public TerminalNode K_SELECT() { return getToken(SQLParser.K_SELECT, 0); }
		public List<Result_columnContext> result_column() {
			return getRuleContexts(Result_columnContext.class);
		}
		public Result_columnContext result_column(int i) {
			return getRuleContext(Result_columnContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public TerminalNode K_FROM() { return getToken(SQLParser.K_FROM, 0); }
		public TerminalNode K_WHERE() { return getToken(SQLParser.K_WHERE, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode K_GROUP() { return getToken(SQLParser.K_GROUP, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public TerminalNode K_ALL() { return getToken(SQLParser.K_ALL, 0); }
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public Join_clauseContext join_clause() {
			return getRuleContext(Join_clauseContext.class,0);
		}
		public TerminalNode K_HAVING() { return getToken(SQLParser.K_HAVING, 0); }
		public TerminalNode K_VALUES() { return getToken(SQLParser.K_VALUES, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(SQLParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(SQLParser.OPEN_PAR, i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(SQLParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(SQLParser.CLOSE_PAR, i);
		}
		public Select_coreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_core; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSelect_core(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSelect_core(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSelect_core(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_coreContext select_core() throws RecognitionException {
		Select_coreContext _localctx = new Select_coreContext(_ctx, getState());
		enterRule(_localctx, 226, RULE_select_core);
		int _la;
		try {
			int _alt;
			setState(2412);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_SELECT:
				enterOuterAlt(_localctx, 1);
				{
				setState(2338);
				match(K_SELECT);
				setState(2340);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_ALL || _la==K_DISTINCT) {
					{
					setState(2339);
					_la = _input.LA(1);
					if ( !(_la==K_ALL || _la==K_DISTINCT) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(2342);
				result_column();
				setState(2347);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,271,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(2343);
						match(COMMA);
						setState(2344);
						result_column();
						}
						} 
					}
					setState(2349);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,271,_ctx);
				}
				setState(2362);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_FROM) {
					{
					setState(2350);
					match(K_FROM);
					setState(2360);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,273,_ctx) ) {
					case 1:
						{
						setState(2351);
						table_or_subquery();
						setState(2356);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,272,_ctx);
						while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
							if ( _alt==1 ) {
								{
								{
								setState(2352);
								match(COMMA);
								setState(2353);
								table_or_subquery();
								}
								} 
							}
							setState(2358);
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,272,_ctx);
						}
						}
						break;
					case 2:
						{
						setState(2359);
						join_clause();
						}
						break;
					}
					}
				}

				setState(2366);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_WHERE) {
					{
					setState(2364);
					match(K_WHERE);
					setState(2365);
					expr(0);
					}
				}

				setState(2382);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_GROUP) {
					{
					setState(2368);
					match(K_GROUP);
					setState(2369);
					match(K_BY);
					setState(2370);
					expr(0);
					setState(2375);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,276,_ctx);
					while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
						if ( _alt==1 ) {
							{
							{
							setState(2371);
							match(COMMA);
							setState(2372);
							expr(0);
							}
							} 
						}
						setState(2377);
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,276,_ctx);
					}
					setState(2380);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_HAVING) {
						{
						setState(2378);
						match(K_HAVING);
						setState(2379);
						expr(0);
						}
					}

					}
				}

				}
				break;
			case K_VALUES:
				enterOuterAlt(_localctx, 2);
				{
				setState(2384);
				match(K_VALUES);
				setState(2385);
				match(OPEN_PAR);
				setState(2386);
				expr(0);
				setState(2391);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(2387);
					match(COMMA);
					setState(2388);
					expr(0);
					}
					}
					setState(2393);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(2394);
				match(CLOSE_PAR);
				setState(2409);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,281,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(2395);
						match(COMMA);
						setState(2396);
						match(OPEN_PAR);
						setState(2397);
						expr(0);
						setState(2402);
						_errHandler.sync(this);
						_la = _input.LA(1);
						while (_la==COMMA) {
							{
							{
							setState(2398);
							match(COMMA);
							setState(2399);
							expr(0);
							}
							}
							setState(2404);
							_errHandler.sync(this);
							_la = _input.LA(1);
						}
						setState(2405);
						match(CLOSE_PAR);
						}
						} 
					}
					setState(2411);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,281,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_type_stmtContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(SQLParser.K_CREATE, 0); }
		public TerminalNode K_TYPE() { return getToken(SQLParser.K_TYPE, 0); }
		public Create_type_nameContext create_type_name() {
			return getRuleContext(Create_type_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Column_def_typeContext> column_def_type() {
			return getRuleContexts(Column_def_typeContext.class);
		}
		public Column_def_typeContext column_def_type(int i) {
			return getRuleContext(Column_def_typeContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Create_type_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_type_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreate_type_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreate_type_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCreate_type_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_type_stmtContext create_type_stmt() throws RecognitionException {
		Create_type_stmtContext _localctx = new Create_type_stmtContext(_ctx, getState());
		enterRule(_localctx, 228, RULE_create_type_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2414);
			match(K_CREATE);
			setState(2415);
			match(K_TYPE);
			setState(2419);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_IF) {
				{
				setState(2416);
				match(K_IF);
				setState(2417);
				match(K_NOT);
				setState(2418);
				match(K_EXISTS);
				}
			}

			setState(2421);
			create_type_name();
			{
			setState(2422);
			match(OPEN_PAR);
			setState(2423);
			column_def_type();
			setState(2428);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(2424);
				match(COMMA);
				setState(2425);
				column_def_type();
				}
				}
				setState(2430);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2431);
			match(CLOSE_PAR);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_type_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Create_type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCreate_type_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCreate_type_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCreate_type_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_type_nameContext create_type_name() throws RecognitionException {
		Create_type_nameContext _localctx = new Create_type_nameContext(_ctx, getState());
		enterRule(_localctx, 230, RULE_create_type_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2433);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_def_typeContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public List<Type_nameContext> type_name() {
			return getRuleContexts(Type_nameContext.class);
		}
		public Type_nameContext type_name(int i) {
			return getRuleContext(Type_nameContext.class,i);
		}
		public Column_def_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_def_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_def_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_def_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_def_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_def_typeContext column_def_type() throws RecognitionException {
		Column_def_typeContext _localctx = new Column_def_typeContext(_ctx, getState());
		enterRule(_localctx, 232, RULE_column_def_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2435);
			column_name();
			setState(2439);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OPEN_PAR || _la==IDENTIFIER || _la==STRING_LITERAL) {
				{
				{
				setState(2436);
				type_name();
				}
				}
				setState(2441);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cte_table_nameContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public List<TerminalNode> COMMA() { return getTokens(SQLParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(SQLParser.COMMA, i);
		}
		public Cte_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cte_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCte_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCte_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCte_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Cte_table_nameContext cte_table_name() throws RecognitionException {
		Cte_table_nameContext _localctx = new Cte_table_nameContext(_ctx, getState());
		enterRule(_localctx, 234, RULE_cte_table_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2442);
			table_name();
			setState(2454);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPEN_PAR) {
				{
				setState(2443);
				match(OPEN_PAR);
				setState(2444);
				column_name();
				setState(2449);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(2445);
					match(COMMA);
					setState(2446);
					column_name();
					}
					}
					setState(2451);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(2452);
				match(CLOSE_PAR);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Signed_numberContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STAR() { return getToken(SQLParser.STAR, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public Signed_numberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signed_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSigned_number(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSigned_number(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSigned_number(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Signed_numberContext signed_number() throws RecognitionException {
		Signed_numberContext _localctx = new Signed_numberContext(_ctx, getState());
		enterRule(_localctx, 236, RULE_signed_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2461);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PLUS:
			case MINUS:
			case NUMERIC_LITERAL:
				{
				setState(2457);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==PLUS || _la==MINUS) {
					{
					setState(2456);
					_la = _input.LA(1);
					if ( !(_la==PLUS || _la==MINUS) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(2459);
				match(NUMERIC_LITERAL);
				}
				break;
			case STAR:
				{
				setState(2460);
				match(STAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Literal_valueContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public TerminalNode BLOB_LITERAL() { return getToken(SQLParser.BLOB_LITERAL, 0); }
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public TerminalNode K_CURRENT_TIME() { return getToken(SQLParser.K_CURRENT_TIME, 0); }
		public TerminalNode K_CURRENT_DATE() { return getToken(SQLParser.K_CURRENT_DATE, 0); }
		public TerminalNode K_CURRENT_TIMESTAMP() { return getToken(SQLParser.K_CURRENT_TIMESTAMP, 0); }
		public Literal_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterLiteral_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitLiteral_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitLiteral_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Literal_valueContext literal_value() throws RecognitionException {
		Literal_valueContext _localctx = new Literal_valueContext(_ctx, getState());
		enterRule(_localctx, 238, RULE_literal_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2463);
			_la = _input.LA(1);
			if ( !(((((_la - 72)) & ~0x3f) == 0 && ((1L << (_la - 72)) & ((1L << (K_CURRENT_DATE - 72)) | (1L << (K_CURRENT_TIME - 72)) | (1L << (K_CURRENT_TIMESTAMP - 72)) | (1L << (K_NULL - 72)))) != 0) || ((((_la - 186)) & ~0x3f) == 0 && ((1L << (_la - 186)) & ((1L << (NUMERIC_LITERAL - 186)) | (1L << (STRING_LITERAL - 186)) | (1L << (BLOB_LITERAL - 186)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Literal_value_jsonContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(SQLParser.NUMERIC_LITERAL, 0); }
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public TerminalNode BLOB_LITERAL() { return getToken(SQLParser.BLOB_LITERAL, 0); }
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public TerminalNode K_TRUE() { return getToken(SQLParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(SQLParser.K_FALSE, 0); }
		public Array_jsonContext array_json() {
			return getRuleContext(Array_jsonContext.class,0);
		}
		public Literal_value_jsonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal_value_json; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterLiteral_value_json(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitLiteral_value_json(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitLiteral_value_json(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Literal_value_jsonContext literal_value_json() throws RecognitionException {
		Literal_value_jsonContext _localctx = new Literal_value_jsonContext(_ctx, getState());
		enterRule(_localctx, 240, RULE_literal_value_json);
		try {
			setState(2473);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMERIC_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(2465);
				match(NUMERIC_LITERAL);
				}
				break;
			case IDENTIFIER:
				enterOuterAlt(_localctx, 2);
				{
				setState(2466);
				match(IDENTIFIER);
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 3);
				{
				setState(2467);
				match(STRING_LITERAL);
				}
				break;
			case BLOB_LITERAL:
				enterOuterAlt(_localctx, 4);
				{
				setState(2468);
				match(BLOB_LITERAL);
				}
				break;
			case K_NULL:
				enterOuterAlt(_localctx, 5);
				{
				setState(2469);
				match(K_NULL);
				}
				break;
			case K_TRUE:
				enterOuterAlt(_localctx, 6);
				{
				setState(2470);
				match(K_TRUE);
				}
				break;
			case K_FALSE:
				enterOuterAlt(_localctx, 7);
				{
				setState(2471);
				match(K_FALSE);
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 8);
				{
				setState(2472);
				array_json();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_operatorContext extends ParserRuleContext {
		public TerminalNode MINUS() { return getToken(SQLParser.MINUS, 0); }
		public TerminalNode PLUS() { return getToken(SQLParser.PLUS, 0); }
		public TerminalNode TILDE() { return getToken(SQLParser.TILDE, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public Unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUnary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUnary_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUnary_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unary_operatorContext unary_operator() throws RecognitionException {
		Unary_operatorContext _localctx = new Unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 242, RULE_unary_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2475);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << TILDE))) != 0) || _la==K_NOT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Error_messageContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public Error_messageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_error_message; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterError_message(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitError_message(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitError_message(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Error_messageContext error_message() throws RecognitionException {
		Error_messageContext _localctx = new Error_messageContext(_ctx, getState());
		enterRule(_localctx, 244, RULE_error_message);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2477);
			match(STRING_LITERAL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Module_argumentContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Column_defContext column_def() {
			return getRuleContext(Column_defContext.class,0);
		}
		public Module_argumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_module_argument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterModule_argument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitModule_argument(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitModule_argument(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Module_argumentContext module_argument() throws RecognitionException {
		Module_argumentContext _localctx = new Module_argumentContext(_ctx, getState());
		enterRule(_localctx, 246, RULE_module_argument);
		try {
			setState(2481);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,291,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2479);
				expr(0);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2480);
				column_def();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_aliasContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public Column_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_aliasContext column_alias() throws RecognitionException {
		Column_aliasContext _localctx = new Column_aliasContext(_ctx, getState());
		enterRule(_localctx, 248, RULE_column_alias);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2483);
			_la = _input.LA(1);
			if ( !(_la==IDENTIFIER || _la==STRING_LITERAL) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public TerminalNode K_ABORT() { return getToken(SQLParser.K_ABORT, 0); }
		public TerminalNode K_ACTION() { return getToken(SQLParser.K_ACTION, 0); }
		public TerminalNode K_AGGREGATION() { return getToken(SQLParser.K_AGGREGATION, 0); }
		public TerminalNode K_AGGREGATION_FUNC() { return getToken(SQLParser.K_AGGREGATION_FUNC, 0); }
		public TerminalNode K_ADD() { return getToken(SQLParser.K_ADD, 0); }
		public TerminalNode K_AFTER() { return getToken(SQLParser.K_AFTER, 0); }
		public TerminalNode K_ALL() { return getToken(SQLParser.K_ALL, 0); }
		public TerminalNode K_ALTER() { return getToken(SQLParser.K_ALTER, 0); }
		public TerminalNode K_ANALYZE() { return getToken(SQLParser.K_ANALYZE, 0); }
		public TerminalNode K_AND() { return getToken(SQLParser.K_AND, 0); }
		public TerminalNode K_AS() { return getToken(SQLParser.K_AS, 0); }
		public TerminalNode K_ASC() { return getToken(SQLParser.K_ASC, 0); }
		public TerminalNode K_ATTACH() { return getToken(SQLParser.K_ATTACH, 0); }
		public TerminalNode K_AUTOINCREMENT() { return getToken(SQLParser.K_AUTOINCREMENT, 0); }
		public TerminalNode K_BEFORE() { return getToken(SQLParser.K_BEFORE, 0); }
		public TerminalNode K_BEGIN() { return getToken(SQLParser.K_BEGIN, 0); }
		public TerminalNode K_BETWEEN() { return getToken(SQLParser.K_BETWEEN, 0); }
		public TerminalNode K_BY() { return getToken(SQLParser.K_BY, 0); }
		public TerminalNode K_CASCADE() { return getToken(SQLParser.K_CASCADE, 0); }
		public TerminalNode K_CASE() { return getToken(SQLParser.K_CASE, 0); }
		public TerminalNode K_CAST() { return getToken(SQLParser.K_CAST, 0); }
		public TerminalNode K_CHECK() { return getToken(SQLParser.K_CHECK, 0); }
		public TerminalNode K_COLLATE() { return getToken(SQLParser.K_COLLATE, 0); }
		public TerminalNode K_COLUMN() { return getToken(SQLParser.K_COLUMN, 0); }
		public TerminalNode K_COMMIT() { return getToken(SQLParser.K_COMMIT, 0); }
		public TerminalNode K_CONFLICT() { return getToken(SQLParser.K_CONFLICT, 0); }
		public TerminalNode K_CONSTRAINT() { return getToken(SQLParser.K_CONSTRAINT, 0); }
		public TerminalNode K_CREATE() { return getToken(SQLParser.K_CREATE, 0); }
		public TerminalNode K_CROSS() { return getToken(SQLParser.K_CROSS, 0); }
		public TerminalNode K_CURRENT_DATE() { return getToken(SQLParser.K_CURRENT_DATE, 0); }
		public TerminalNode K_CURRENT_TIME() { return getToken(SQLParser.K_CURRENT_TIME, 0); }
		public TerminalNode K_CURRENT_TIMESTAMP() { return getToken(SQLParser.K_CURRENT_TIMESTAMP, 0); }
		public TerminalNode K_DATABASE() { return getToken(SQLParser.K_DATABASE, 0); }
		public TerminalNode K_DEFAULT() { return getToken(SQLParser.K_DEFAULT, 0); }
		public TerminalNode K_DEFERRABLE() { return getToken(SQLParser.K_DEFERRABLE, 0); }
		public TerminalNode K_DEFERRED() { return getToken(SQLParser.K_DEFERRED, 0); }
		public TerminalNode K_DELETE() { return getToken(SQLParser.K_DELETE, 0); }
		public TerminalNode K_DESC() { return getToken(SQLParser.K_DESC, 0); }
		public TerminalNode K_DETACH() { return getToken(SQLParser.K_DETACH, 0); }
		public TerminalNode K_DISTINCT() { return getToken(SQLParser.K_DISTINCT, 0); }
		public TerminalNode K_DROP() { return getToken(SQLParser.K_DROP, 0); }
		public TerminalNode K_EACH() { return getToken(SQLParser.K_EACH, 0); }
		public TerminalNode K_ELSE() { return getToken(SQLParser.K_ELSE, 0); }
		public TerminalNode K_END() { return getToken(SQLParser.K_END, 0); }
		public TerminalNode K_ENABLE() { return getToken(SQLParser.K_ENABLE, 0); }
		public TerminalNode K_ESCAPE() { return getToken(SQLParser.K_ESCAPE, 0); }
		public TerminalNode K_EXCEPT() { return getToken(SQLParser.K_EXCEPT, 0); }
		public TerminalNode K_EXCLUSIVE() { return getToken(SQLParser.K_EXCLUSIVE, 0); }
		public TerminalNode K_EXISTS() { return getToken(SQLParser.K_EXISTS, 0); }
		public TerminalNode K_EXPLAIN() { return getToken(SQLParser.K_EXPLAIN, 0); }
		public TerminalNode K_FAIL() { return getToken(SQLParser.K_FAIL, 0); }
		public TerminalNode K_FOR() { return getToken(SQLParser.K_FOR, 0); }
		public TerminalNode K_FOREIGN() { return getToken(SQLParser.K_FOREIGN, 0); }
		public TerminalNode K_FROM() { return getToken(SQLParser.K_FROM, 0); }
		public TerminalNode K_FULL() { return getToken(SQLParser.K_FULL, 0); }
		public TerminalNode K_GLOB() { return getToken(SQLParser.K_GLOB, 0); }
		public TerminalNode K_GROUP() { return getToken(SQLParser.K_GROUP, 0); }
		public TerminalNode K_HAVING() { return getToken(SQLParser.K_HAVING, 0); }
		public TerminalNode K_IF() { return getToken(SQLParser.K_IF, 0); }
		public TerminalNode K_IGNORE() { return getToken(SQLParser.K_IGNORE, 0); }
		public TerminalNode K_IMMEDIATE() { return getToken(SQLParser.K_IMMEDIATE, 0); }
		public TerminalNode K_IN() { return getToken(SQLParser.K_IN, 0); }
		public TerminalNode K_INDEX() { return getToken(SQLParser.K_INDEX, 0); }
		public TerminalNode K_INDEXED() { return getToken(SQLParser.K_INDEXED, 0); }
		public TerminalNode K_INITIALLY() { return getToken(SQLParser.K_INITIALLY, 0); }
		public TerminalNode K_INNER() { return getToken(SQLParser.K_INNER, 0); }
		public TerminalNode K_INSERT() { return getToken(SQLParser.K_INSERT, 0); }
		public TerminalNode K_INSTEAD() { return getToken(SQLParser.K_INSTEAD, 0); }
		public TerminalNode K_INTERSECT() { return getToken(SQLParser.K_INTERSECT, 0); }
		public TerminalNode K_INTO() { return getToken(SQLParser.K_INTO, 0); }
		public TerminalNode K_IS() { return getToken(SQLParser.K_IS, 0); }
		public TerminalNode K_ISNULL() { return getToken(SQLParser.K_ISNULL, 0); }
		public TerminalNode K_JOIN() { return getToken(SQLParser.K_JOIN, 0); }
		public TerminalNode K_KEY() { return getToken(SQLParser.K_KEY, 0); }
		public TerminalNode K_LEFT() { return getToken(SQLParser.K_LEFT, 0); }
		public TerminalNode K_LIKE() { return getToken(SQLParser.K_LIKE, 0); }
		public TerminalNode K_LIMIT() { return getToken(SQLParser.K_LIMIT, 0); }
		public TerminalNode K_MATCH() { return getToken(SQLParser.K_MATCH, 0); }
		public TerminalNode K_NATURAL() { return getToken(SQLParser.K_NATURAL, 0); }
		public TerminalNode K_NO() { return getToken(SQLParser.K_NO, 0); }
		public TerminalNode K_NOT() { return getToken(SQLParser.K_NOT, 0); }
		public TerminalNode K_NOTNULL() { return getToken(SQLParser.K_NOTNULL, 0); }
		public TerminalNode K_NULL() { return getToken(SQLParser.K_NULL, 0); }
		public TerminalNode K_OF() { return getToken(SQLParser.K_OF, 0); }
		public TerminalNode K_OFFSET() { return getToken(SQLParser.K_OFFSET, 0); }
		public TerminalNode K_ON() { return getToken(SQLParser.K_ON, 0); }
		public TerminalNode K_OR() { return getToken(SQLParser.K_OR, 0); }
		public TerminalNode K_ORDER() { return getToken(SQLParser.K_ORDER, 0); }
		public TerminalNode K_OUTER() { return getToken(SQLParser.K_OUTER, 0); }
		public TerminalNode K_PLAN() { return getToken(SQLParser.K_PLAN, 0); }
		public TerminalNode K_PRAGMA() { return getToken(SQLParser.K_PRAGMA, 0); }
		public TerminalNode K_PRIMARY() { return getToken(SQLParser.K_PRIMARY, 0); }
		public TerminalNode K_QUERY() { return getToken(SQLParser.K_QUERY, 0); }
		public TerminalNode K_RAISE() { return getToken(SQLParser.K_RAISE, 0); }
		public TerminalNode K_RECURSIVE() { return getToken(SQLParser.K_RECURSIVE, 0); }
		public TerminalNode K_REFERENCES() { return getToken(SQLParser.K_REFERENCES, 0); }
		public TerminalNode K_REGEXP() { return getToken(SQLParser.K_REGEXP, 0); }
		public TerminalNode K_REINDEX() { return getToken(SQLParser.K_REINDEX, 0); }
		public TerminalNode K_RELEASE() { return getToken(SQLParser.K_RELEASE, 0); }
		public TerminalNode K_RENAME() { return getToken(SQLParser.K_RENAME, 0); }
		public TerminalNode K_REPLACE() { return getToken(SQLParser.K_REPLACE, 0); }
		public TerminalNode K_RESTRICT() { return getToken(SQLParser.K_RESTRICT, 0); }
		public TerminalNode K_RIGHT() { return getToken(SQLParser.K_RIGHT, 0); }
		public TerminalNode K_ROLLBACK() { return getToken(SQLParser.K_ROLLBACK, 0); }
		public TerminalNode K_ROW() { return getToken(SQLParser.K_ROW, 0); }
		public TerminalNode K_SAVEPOINT() { return getToken(SQLParser.K_SAVEPOINT, 0); }
		public TerminalNode K_SELECT() { return getToken(SQLParser.K_SELECT, 0); }
		public TerminalNode K_SET() { return getToken(SQLParser.K_SET, 0); }
		public TerminalNode K_TABLE() { return getToken(SQLParser.K_TABLE, 0); }
		public TerminalNode K_TYPE() { return getToken(SQLParser.K_TYPE, 0); }
		public TerminalNode K_PATH() { return getToken(SQLParser.K_PATH, 0); }
		public TerminalNode K_TEMP() { return getToken(SQLParser.K_TEMP, 0); }
		public TerminalNode K_TEMPORARY() { return getToken(SQLParser.K_TEMPORARY, 0); }
		public TerminalNode K_THEN() { return getToken(SQLParser.K_THEN, 0); }
		public TerminalNode K_TO() { return getToken(SQLParser.K_TO, 0); }
		public TerminalNode K_TRANSACTION() { return getToken(SQLParser.K_TRANSACTION, 0); }
		public TerminalNode K_TRIGGER() { return getToken(SQLParser.K_TRIGGER, 0); }
		public TerminalNode K_UNION() { return getToken(SQLParser.K_UNION, 0); }
		public TerminalNode K_UNIQUE() { return getToken(SQLParser.K_UNIQUE, 0); }
		public TerminalNode K_UPDATE() { return getToken(SQLParser.K_UPDATE, 0); }
		public TerminalNode K_USING() { return getToken(SQLParser.K_USING, 0); }
		public TerminalNode K_VACUUM() { return getToken(SQLParser.K_VACUUM, 0); }
		public TerminalNode K_VALUES() { return getToken(SQLParser.K_VALUES, 0); }
		public TerminalNode K_VIEW() { return getToken(SQLParser.K_VIEW, 0); }
		public TerminalNode K_VIRTUAL() { return getToken(SQLParser.K_VIRTUAL, 0); }
		public TerminalNode K_WHEN() { return getToken(SQLParser.K_WHEN, 0); }
		public TerminalNode K_WHERE() { return getToken(SQLParser.K_WHERE, 0); }
		public TerminalNode K_WITH() { return getToken(SQLParser.K_WITH, 0); }
		public TerminalNode K_WITHOUT() { return getToken(SQLParser.K_WITHOUT, 0); }
		public TerminalNode K_NEXTVAL() { return getToken(SQLParser.K_NEXTVAL, 0); }
		public TerminalNode K_SWITCH() { return getToken(SQLParser.K_SWITCH, 0); }
		public TerminalNode K_RETURN() { return getToken(SQLParser.K_RETURN, 0); }
		public TerminalNode K_VAR() { return getToken(SQLParser.K_VAR, 0); }
		public TerminalNode K_PRINT() { return getToken(SQLParser.K_PRINT, 0); }
		public TerminalNode K_TRUE() { return getToken(SQLParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(SQLParser.K_FALSE, 0); }
		public TerminalNode K_WHILE() { return getToken(SQLParser.K_WHILE, 0); }
		public TerminalNode K_DO() { return getToken(SQLParser.K_DO, 0); }
		public TerminalNode K_BREAK() { return getToken(SQLParser.K_BREAK, 0); }
		public TerminalNode K_FUNCTION() { return getToken(SQLParser.K_FUNCTION, 0); }
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterKeyword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitKeyword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitKeyword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 250, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2485);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << K_ABORT) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_AGGREGATION_FUNC) | (1L << K_AGGREGATION) | (1L << K_AFTER) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_ANALYZE) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_ATTACH) | (1L << K_AUTOINCREMENT) | (1L << K_BEFORE) | (1L << K_BEGIN) | (1L << K_BETWEEN) | (1L << K_BY) | (1L << K_CASCADE) | (1L << K_CASE) | (1L << K_CAST))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CHECK - 64)) | (1L << (K_COLLATE - 64)) | (1L << (K_COLUMN - 64)) | (1L << (K_COMMIT - 64)) | (1L << (K_CONFLICT - 64)) | (1L << (K_CONSTRAINT - 64)) | (1L << (K_CREATE - 64)) | (1L << (K_CROSS - 64)) | (1L << (K_CURRENT_DATE - 64)) | (1L << (K_CURRENT_TIME - 64)) | (1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DATABASE - 64)) | (1L << (K_DEFAULT - 64)) | (1L << (K_DEFERRABLE - 64)) | (1L << (K_DEFERRED - 64)) | (1L << (K_DELETE - 64)) | (1L << (K_DESC - 64)) | (1L << (K_DETACH - 64)) | (1L << (K_DISTINCT - 64)) | (1L << (K_DROP - 64)) | (1L << (K_EACH - 64)) | (1L << (K_ELSE - 64)) | (1L << (K_END - 64)) | (1L << (K_ENABLE - 64)) | (1L << (K_ESCAPE - 64)) | (1L << (K_EXCEPT - 64)) | (1L << (K_EXCLUSIVE - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_EXPLAIN - 64)) | (1L << (K_FAIL - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_FULL - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IGNORE - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEX - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INSTEAD - 64)) | (1L << (K_INTERSECT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)) | (1L << (K_LIMIT - 64)) | (1L << (K_MATCH - 64)) | (1L << (K_NATURAL - 64)) | (1L << (K_NEXTVAL - 64)) | (1L << (K_NO - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NOTNULL - 64)) | (1L << (K_NULL - 64)) | (1L << (K_OF - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_OFFSET - 128)) | (1L << (K_ON - 128)) | (1L << (K_OR - 128)) | (1L << (K_ORDER - 128)) | (1L << (K_OUTER - 128)) | (1L << (K_PLAN - 128)) | (1L << (K_PRAGMA - 128)) | (1L << (K_PRIMARY - 128)) | (1L << (K_QUERY - 128)) | (1L << (K_RAISE - 128)) | (1L << (K_RECURSIVE - 128)) | (1L << (K_REFERENCES - 128)) | (1L << (K_REGEXP - 128)) | (1L << (K_REINDEX - 128)) | (1L << (K_RELEASE - 128)) | (1L << (K_RENAME - 128)) | (1L << (K_REPLACE - 128)) | (1L << (K_RESTRICT - 128)) | (1L << (K_RIGHT - 128)) | (1L << (K_ROLLBACK - 128)) | (1L << (K_ROW - 128)) | (1L << (K_SAVEPOINT - 128)) | (1L << (K_SELECT - 128)) | (1L << (K_SET - 128)) | (1L << (K_TABLE - 128)) | (1L << (K_TYPE - 128)) | (1L << (K_PATH - 128)) | (1L << (K_TEMP - 128)) | (1L << (K_TEMPORARY - 128)) | (1L << (K_THEN - 128)) | (1L << (K_TO - 128)) | (1L << (K_TRANSACTION - 128)) | (1L << (K_TRIGGER - 128)) | (1L << (K_UNION - 128)) | (1L << (K_UNIQUE - 128)) | (1L << (K_UPDATE - 128)) | (1L << (K_USING - 128)) | (1L << (K_VACUUM - 128)) | (1L << (K_VALUES - 128)) | (1L << (K_VIEW - 128)) | (1L << (K_VIRTUAL - 128)) | (1L << (K_WHEN - 128)) | (1L << (K_WHERE - 128)) | (1L << (K_WITH - 128)) | (1L << (K_WITHOUT - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_RETURN - 128)) | (1L << (K_VAR - 128)) | (1L << (K_PRINT - 128)) | (1L << (K_TRUE - 128)) | (1L << (K_FALSE - 128)) | (1L << (K_WHILE - 128)) | (1L << (K_DO - 128)) | (1L << (K_BREAK - 128)) | (1L << (K_FUNCTION - 128)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnknownContext extends ParserRuleContext {
		public UnknownContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unknown; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterUnknown(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitUnknown(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitUnknown(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnknownContext unknown() throws RecognitionException {
		UnknownContext _localctx = new UnknownContext(_ctx, getState());
		enterRule(_localctx, 252, RULE_unknown);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2488); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(2487);
				matchWildcard();
				}
				}
				setState(2490); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << T__5) | (1L << T__6) | (1L << T__7) | (1L << PLUS_PLUSl) | (1L << MINUS_MINUSl) | (1L << PLUS_PLUSr) | (1L << MINUS_MINUSr) | (1L << SCOL) | (1L << DOT) | (1L << OPEN_PAR) | (1L << CLOSE_PAR) | (1L << COMMA) | (1L << ASSIGN) | (1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << TILDE) | (1L << PIPE2) | (1L << DIV) | (1L << MOD) | (1L << LT2) | (1L << GT2) | (1L << AMP) | (1L << AMP2) | (1L << PIPE) | (1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ) | (1L << EQ) | (1L << NOT_EQ1) | (1L << NOT_EQ2) | (1L << PLUS_EQ) | (1L << MINUS_EQ) | (1L << STAR_EQ) | (1L << DIV_EQ) | (1L << MOD_EQ) | (1L << K_ABORT) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_AGGREGATION_FUNC) | (1L << K_AGGREGATION) | (1L << K_AFTER) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_ANALYZE) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_ATTACH) | (1L << K_AUTOINCREMENT) | (1L << K_BEFORE) | (1L << K_BEGIN) | (1L << K_BETWEEN) | (1L << K_BY) | (1L << K_CASCADE) | (1L << K_CASE) | (1L << K_CAST))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CHECK - 64)) | (1L << (K_COLLATE - 64)) | (1L << (K_COLUMN - 64)) | (1L << (K_COMMIT - 64)) | (1L << (K_CONFLICT - 64)) | (1L << (K_CONSTRAINT - 64)) | (1L << (K_CREATE - 64)) | (1L << (K_CROSS - 64)) | (1L << (K_CURRENT_DATE - 64)) | (1L << (K_CURRENT_TIME - 64)) | (1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DATABASE - 64)) | (1L << (K_DEFAULT - 64)) | (1L << (K_DEFERRABLE - 64)) | (1L << (K_DEFERRED - 64)) | (1L << (K_DELETE - 64)) | (1L << (K_DESC - 64)) | (1L << (K_DETACH - 64)) | (1L << (K_DISTINCT - 64)) | (1L << (K_DROP - 64)) | (1L << (K_EACH - 64)) | (1L << (K_ELSE - 64)) | (1L << (K_END - 64)) | (1L << (K_ENABLE - 64)) | (1L << (K_ESCAPE - 64)) | (1L << (K_EXCEPT - 64)) | (1L << (K_EXCLUSIVE - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_EXPLAIN - 64)) | (1L << (K_FAIL - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_FULL - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IGNORE - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEX - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INSTEAD - 64)) | (1L << (K_INTERSECT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)) | (1L << (K_LIMIT - 64)) | (1L << (K_MATCH - 64)) | (1L << (K_NATURAL - 64)) | (1L << (K_NEXTVAL - 64)) | (1L << (K_NO - 64)) | (1L << (K_NOT - 64)) | (1L << (K_NOTNULL - 64)) | (1L << (K_NULL - 64)) | (1L << (K_OF - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_OFFSET - 128)) | (1L << (K_ON - 128)) | (1L << (K_ONLY - 128)) | (1L << (K_OR - 128)) | (1L << (K_ORDER - 128)) | (1L << (K_OUTER - 128)) | (1L << (K_PLAN - 128)) | (1L << (K_PRAGMA - 128)) | (1L << (K_PRIMARY - 128)) | (1L << (K_QUERY - 128)) | (1L << (K_RAISE - 128)) | (1L << (K_RECURSIVE - 128)) | (1L << (K_REFERENCES - 128)) | (1L << (K_REGEXP - 128)) | (1L << (K_REINDEX - 128)) | (1L << (K_RELEASE - 128)) | (1L << (K_RENAME - 128)) | (1L << (K_REPLACE - 128)) | (1L << (K_RESTRICT - 128)) | (1L << (K_RIGHT - 128)) | (1L << (K_ROLLBACK - 128)) | (1L << (K_ROW - 128)) | (1L << (K_SAVEPOINT - 128)) | (1L << (K_SELECT - 128)) | (1L << (K_SET - 128)) | (1L << (K_TABLE - 128)) | (1L << (K_TYPE - 128)) | (1L << (K_PATH - 128)) | (1L << (K_TEMP - 128)) | (1L << (K_TEMPORARY - 128)) | (1L << (K_THEN - 128)) | (1L << (K_TO - 128)) | (1L << (K_TRANSACTION - 128)) | (1L << (K_TRIGGER - 128)) | (1L << (K_UNION - 128)) | (1L << (K_UNIQUE - 128)) | (1L << (K_UPDATE - 128)) | (1L << (K_USING - 128)) | (1L << (K_VACUUM - 128)) | (1L << (K_VALUES - 128)) | (1L << (K_VIEW - 128)) | (1L << (K_VIRTUAL - 128)) | (1L << (K_WHEN - 128)) | (1L << (K_WHERE - 128)) | (1L << (K_WITH - 128)) | (1L << (K_WITHOUT - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_RETURN - 128)) | (1L << (K_VAR - 128)) | (1L << (K_PRINT - 128)) | (1L << (K_TRUE - 128)) | (1L << (K_FALSE - 128)) | (1L << (K_WHILE - 128)) | (1L << (K_DO - 128)) | (1L << (K_BREAK - 128)) | (1L << (K_FUNCTION - 128)) | (1L << (IDENTIFIER - 128)) | (1L << (IDENTIFIER2 - 128)) | (1L << (NUMERIC_LITERAL - 128)) | (1L << (BIND_PARAMETER - 128)) | (1L << (STRING_LITERAL - 128)) | (1L << (BLOB_LITERAL - 128)) | (1L << (SINGLE_LINE_COMMENT - 128)) | (1L << (MULTILINE_COMMENT - 128)))) != 0) || ((((_la - 192)) & ~0x3f) == 0 && ((1L << (_la - 192)) & ((1L << (SPACES - 192)) | (1L << (UNEXPECTED_CHAR - 192)) | (1L << (DIGIT - 192)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 254, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2492);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Function_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterFunction_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitFunction_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitFunction_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_nameContext function_name() throws RecognitionException {
		Function_nameContext _localctx = new Function_nameContext(_ctx, getState());
		enterRule(_localctx, 256, RULE_function_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2494);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Database_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Database_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_database_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterDatabase_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitDatabase_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitDatabase_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Database_nameContext database_name() throws RecognitionException {
		Database_nameContext _localctx = new Database_nameContext(_ctx, getState());
		enterRule(_localctx, 258, RULE_database_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2496);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Source_table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Source_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSource_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSource_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSource_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Source_table_nameContext source_table_name() throws RecognitionException {
		Source_table_nameContext _localctx = new Source_table_nameContext(_ctx, getState());
		enterRule(_localctx, 260, RULE_source_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2498);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_nameContext table_name() throws RecognitionException {
		Table_nameContext _localctx = new Table_nameContext(_ctx, getState());
		enterRule(_localctx, 262, RULE_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2500);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_or_index_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_or_index_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_or_index_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_or_index_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_or_index_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_or_index_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_or_index_nameContext table_or_index_name() throws RecognitionException {
		Table_or_index_nameContext _localctx = new Table_or_index_nameContext(_ctx, getState());
		enterRule(_localctx, 264, RULE_table_or_index_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2502);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class New_table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public New_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_new_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterNew_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitNew_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitNew_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final New_table_nameContext new_table_name() throws RecognitionException {
		New_table_nameContext _localctx = new New_table_nameContext(_ctx, getState());
		enterRule(_localctx, 266, RULE_new_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2504);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterColumn_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitColumn_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitColumn_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_nameContext column_name() throws RecognitionException {
		Column_nameContext _localctx = new Column_nameContext(_ctx, getState());
		enterRule(_localctx, 268, RULE_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2506);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Collation_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Collation_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collation_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterCollation_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitCollation_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitCollation_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Collation_nameContext collation_name() throws RecognitionException {
		Collation_nameContext _localctx = new Collation_nameContext(_ctx, getState());
		enterRule(_localctx, 270, RULE_collation_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2508);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_tableContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Foreign_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterForeign_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitForeign_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitForeign_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_tableContext foreign_table() throws RecognitionException {
		Foreign_tableContext _localctx = new Foreign_tableContext(_ctx, getState());
		enterRule(_localctx, 272, RULE_foreign_table);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2510);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Index_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Index_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterIndex_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitIndex_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitIndex_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_nameContext index_name() throws RecognitionException {
		Index_nameContext _localctx = new Index_nameContext(_ctx, getState());
		enterRule(_localctx, 274, RULE_index_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2512);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Trigger_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Trigger_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_trigger_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTrigger_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTrigger_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTrigger_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Trigger_nameContext trigger_name() throws RecognitionException {
		Trigger_nameContext _localctx = new Trigger_nameContext(_ctx, getState());
		enterRule(_localctx, 276, RULE_trigger_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2514);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class View_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public View_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_view_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterView_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitView_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitView_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final View_nameContext view_name() throws RecognitionException {
		View_nameContext _localctx = new View_nameContext(_ctx, getState());
		enterRule(_localctx, 278, RULE_view_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2516);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Module_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Module_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_module_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterModule_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitModule_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitModule_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Module_nameContext module_name() throws RecognitionException {
		Module_nameContext _localctx = new Module_nameContext(_ctx, getState());
		enterRule(_localctx, 280, RULE_module_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2518);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pragma_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Pragma_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pragma_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterPragma_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitPragma_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitPragma_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Pragma_nameContext pragma_name() throws RecognitionException {
		Pragma_nameContext _localctx = new Pragma_nameContext(_ctx, getState());
		enterRule(_localctx, 282, RULE_pragma_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2520);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Savepoint_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Savepoint_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_savepoint_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterSavepoint_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitSavepoint_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitSavepoint_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Savepoint_nameContext savepoint_name() throws RecognitionException {
		Savepoint_nameContext _localctx = new Savepoint_nameContext(_ctx, getState());
		enterRule(_localctx, 284, RULE_savepoint_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2522);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_aliasContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTable_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTable_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTable_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_aliasContext table_alias() throws RecognitionException {
		Table_aliasContext _localctx = new Table_aliasContext(_ctx, getState());
		enterRule(_localctx, 286, RULE_table_alias);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2524);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Transaction_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Transaction_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transaction_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterTransaction_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitTransaction_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitTransaction_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Transaction_nameContext transaction_name() throws RecognitionException {
		Transaction_nameContext _localctx = new Transaction_nameContext(_ctx, getState());
		enterRule(_localctx, 288, RULE_transaction_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2526);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Any_nameContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(SQLParser.IDENTIFIER, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(SQLParser.STRING_LITERAL, 0); }
		public TerminalNode OPEN_PAR() { return getToken(SQLParser.OPEN_PAR, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(SQLParser.CLOSE_PAR, 0); }
		public Any_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_any_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).enterAny_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SQLListener ) ((SQLListener)listener).exitAny_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof SQLVisitor ) return ((SQLVisitor<? extends T>)visitor).visitAny_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Any_nameContext any_name() throws RecognitionException {
		Any_nameContext _localctx = new Any_nameContext(_ctx, getState());
		enterRule(_localctx, 290, RULE_any_name);
		try {
			setState(2534);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(2528);
				match(IDENTIFIER);
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(2529);
				match(STRING_LITERAL);
				}
				break;
			case OPEN_PAR:
				enterOuterAlt(_localctx, 3);
				{
				setState(2530);
				match(OPEN_PAR);
				setState(2531);
				any_name();
				setState(2532);
				match(CLOSE_PAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 34:
			return expr_sempred((ExprContext)_localctx, predIndex);
		case 35:
			return expr_test_sempred((Expr_testContext)_localctx, predIndex);
		case 52:
			return expr_condition_sempred((Expr_conditionContext)_localctx, predIndex);
		case 53:
			return condition_header_sempred((Condition_headerContext)_localctx, predIndex);
		case 76:
			return expr_var_sempred((Expr_varContext)_localctx, predIndex);
		case 94:
			return bool_ex_sempred((Bool_exContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 14);
		case 1:
			return precpred(_ctx, 13);
		case 2:
			return precpred(_ctx, 12);
		case 3:
			return precpred(_ctx, 11);
		case 4:
			return precpred(_ctx, 10);
		case 5:
			return precpred(_ctx, 9);
		case 6:
			return precpred(_ctx, 8);
		case 7:
			return precpred(_ctx, 7);
		case 8:
			return precpred(_ctx, 6);
		case 9:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean expr_test_sempred(Expr_testContext _localctx, int predIndex) {
		switch (predIndex) {
		case 10:
			return precpred(_ctx, 14);
		case 11:
			return precpred(_ctx, 13);
		case 12:
			return precpred(_ctx, 12);
		case 13:
			return precpred(_ctx, 11);
		case 14:
			return precpred(_ctx, 10);
		case 15:
			return precpred(_ctx, 9);
		case 16:
			return precpred(_ctx, 8);
		case 17:
			return precpred(_ctx, 7);
		case 18:
			return precpred(_ctx, 6);
		case 19:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean expr_condition_sempred(Expr_conditionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 20:
			return precpred(_ctx, 4);
		case 21:
			return precpred(_ctx, 3);
		}
		return true;
	}
	private boolean condition_header_sempred(Condition_headerContext _localctx, int predIndex) {
		switch (predIndex) {
		case 22:
			return precpred(_ctx, 3);
		case 23:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean expr_var_sempred(Expr_varContext _localctx, int predIndex) {
		switch (predIndex) {
		case 24:
			return precpred(_ctx, 3);
		case 25:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean bool_ex_sempred(Bool_exContext _localctx, int predIndex) {
		switch (predIndex) {
		case 26:
			return precpred(_ctx, 1);
		}
		return true;
	}

	private static final int _serializedATNSegments = 2;
	private static final String _serializedATNSegment0 =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u00c4\u09eb\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\4m\tm\4n\tn\4o\to\4p\tp\4q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4"+
		"w\tw\4x\tx\4y\ty\4z\tz\4{\t{\4|\t|\4}\t}\4~\t~\4\177\t\177\4\u0080\t\u0080"+
		"\4\u0081\t\u0081\4\u0082\t\u0082\4\u0083\t\u0083\4\u0084\t\u0084\4\u0085"+
		"\t\u0085\4\u0086\t\u0086\4\u0087\t\u0087\4\u0088\t\u0088\4\u0089\t\u0089"+
		"\4\u008a\t\u008a\4\u008b\t\u008b\4\u008c\t\u008c\4\u008d\t\u008d\4\u008e"+
		"\t\u008e\4\u008f\t\u008f\4\u0090\t\u0090\4\u0091\t\u0091\4\u0092\t\u0092"+
		"\4\u0093\t\u0093\3\2\3\2\3\2\7\2\u012a\n\2\f\2\16\2\u012d\13\2\3\2\3\2"+
		"\3\3\3\3\3\3\3\4\7\4\u0135\n\4\f\4\16\4\u0138\13\4\3\4\3\4\6\4\u013c\n"+
		"\4\r\4\16\4\u013d\3\4\7\4\u0141\n\4\f\4\16\4\u0144\13\4\3\4\7\4\u0147"+
		"\n\4\f\4\16\4\u014a\13\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u0155"+
		"\n\5\3\6\3\6\3\6\3\6\3\6\5\6\u015c\n\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\5\6\u0166\n\6\3\6\5\6\u0169\n\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3"+
		"\t\3\t\3\t\3\t\5\t\u0178\n\t\3\t\3\t\3\t\5\t\u017d\n\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\7\t\u0186\n\t\f\t\16\t\u0189\13\t\3\t\3\t\5\t\u018d\n\t\3"+
		"\t\5\t\u0190\n\t\3\t\3\t\5\t\u0194\n\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13"+
		"\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u01ac"+
		"\n\f\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3\21"+
		"\3\21\5\21\u01bd\n\21\3\22\3\22\3\22\3\22\5\22\u01c3\n\22\3\22\3\22\3"+
		"\22\5\22\u01c8\n\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\7\23\u01d2"+
		"\n\23\f\23\16\23\u01d5\13\23\5\23\u01d7\n\23\3\23\3\23\3\23\3\23\5\23"+
		"\u01dd\n\23\5\23\u01df\n\23\3\24\5\24\u01e2\n\24\3\24\3\24\5\24\u01e6"+
		"\n\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\5\25\u01f0\n\25\3\25\3\25"+
		"\3\25\3\25\3\25\7\25\u01f7\n\25\f\25\16\25\u01fa\13\25\3\25\3\25\5\25"+
		"\u01fe\n\25\3\25\3\25\3\25\3\25\3\25\7\25\u0205\n\25\f\25\16\25\u0208"+
		"\13\25\3\25\3\25\3\25\3\25\3\25\3\25\7\25\u0210\n\25\f\25\16\25\u0213"+
		"\13\25\3\25\3\25\7\25\u0217\n\25\f\25\16\25\u021a\13\25\3\25\3\25\3\25"+
		"\5\25\u021f\n\25\3\26\3\26\3\26\3\26\3\26\3\26\7\26\u0227\n\26\f\26\16"+
		"\26\u022a\13\26\5\26\u022c\n\26\3\26\3\26\3\26\3\26\5\26\u0232\n\26\5"+
		"\26\u0234\n\26\3\27\3\27\5\27\u0238\n\27\3\27\3\27\3\27\7\27\u023d\n\27"+
		"\f\27\16\27\u0240\13\27\3\27\3\27\3\27\3\27\7\27\u0246\n\27\f\27\16\27"+
		"\u0249\13\27\3\27\5\27\u024c\n\27\5\27\u024e\n\27\3\27\3\27\5\27\u0252"+
		"\n\27\3\27\3\27\3\27\3\27\3\27\7\27\u0259\n\27\f\27\16\27\u025c\13\27"+
		"\3\27\3\27\5\27\u0260\n\27\5\27\u0262\n\27\3\27\3\27\3\27\3\27\3\27\7"+
		"\27\u0269\n\27\f\27\16\27\u026c\13\27\3\27\3\27\3\27\3\27\3\27\3\27\7"+
		"\27\u0274\n\27\f\27\16\27\u0277\13\27\3\27\3\27\7\27\u027b\n\27\f\27\16"+
		"\27\u027e\13\27\5\27\u0280\n\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\5\30"+
		"\u0289\n\30\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\7\32\u0294\n"+
		"\32\f\32\16\32\u0297\13\32\3\33\3\33\3\33\7\33\u029c\n\33\f\33\16\33\u029f"+
		"\13\33\3\34\3\34\3\34\3\34\5\34\u02a5\n\34\3\34\3\34\3\34\3\34\3\34\5"+
		"\34\u02ac\n\34\3\34\3\34\3\34\5\34\u02b1\n\34\3\34\3\34\5\34\u02b5\n\34"+
		"\3\35\3\35\5\35\u02b9\n\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\5\35\u02c8\n\35\3\36\3\36\3\36\5\36\u02cd\n\36\3"+
		"\36\5\36\u02d0\n\36\3\37\3\37\3 \3 \3 \3!\3!\3\"\3\"\3\"\3\"\3\"\3\"\3"+
		"\"\3\"\3\"\3\"\3\"\3\"\5\"\u02e5\n\"\3\"\3\"\6\"\u02e9\n\"\r\"\16\"\u02ea"+
		"\5\"\u02ed\n\"\3#\3#\5#\u02f1\n#\3$\3$\3$\3$\3$\3$\5$\u02f9\n$\3$\3$\3"+
		"$\5$\u02fe\n$\3$\3$\3$\3$\3$\3$\3$\3$\3$\5$\u0309\n$\3$\3$\3$\7$\u030e"+
		"\n$\f$\16$\u0311\13$\3$\5$\u0314\n$\3$\3$\3$\3$\3$\3$\3$\3$\5$\u031e\n"+
		"$\3$\5$\u0321\n$\3$\3$\3$\3$\5$\u0327\n$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3"+
		"$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\3$\5$\u0347"+
		"\n$\3$\3$\3$\3$\3$\3$\3$\3$\3$\5$\u0352\n$\3$\3$\3$\3$\3$\3$\7$\u035a"+
		"\n$\f$\16$\u035d\13$\5$\u035f\n$\3$\3$\3$\3$\5$\u0365\n$\3$\5$\u0368\n"+
		"$\7$\u036a\n$\f$\16$\u036d\13$\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%"+
		"\5%\u037c\n%\3%\3%\3%\7%\u0381\n%\f%\16%\u0384\13%\3%\5%\u0387\n%\3%\3"+
		"%\3%\3%\3%\3%\3%\3%\5%\u0391\n%\3%\5%\u0394\n%\3%\3%\3%\3%\5%\u039a\n"+
		"%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3%\3"+
		"%\3%\3%\3%\3%\3%\3%\3%\3%\5%\u03bb\n%\3%\3%\3%\3%\3%\3%\3%\3%\3%\5%\u03c6"+
		"\n%\3%\3%\3%\3%\3%\3%\7%\u03ce\n%\f%\16%\u03d1\13%\5%\u03d3\n%\3%\3%\3"+
		"%\3%\5%\u03d9\n%\3%\5%\u03dc\n%\7%\u03de\n%\f%\16%\u03e1\13%\3&\3&\3\'"+
		"\3\'\3\'\7\'\u03e8\n\'\f\'\16\'\u03eb\13\'\5\'\u03ed\n\'\3\'\5\'\u03f0"+
		"\n\'\3\'\3\'\3\'\7\'\u03f5\n\'\f\'\16\'\u03f8\13\'\3\'\3\'\7\'\u03fc\n"+
		"\'\f\'\16\'\u03ff\13\'\5\'\u0401\n\'\5\'\u0403\n\'\3(\3(\3(\3)\5)\u0409"+
		"\n)\3)\3)\3)\3)\3)\5)\u0410\n)\3)\5)\u0413\n)\3*\3*\3*\3*\3*\3*\3*\3*"+
		"\3+\3+\5+\u041f\n+\3+\3+\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,"+
		"\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,\3,"+
		"\3,\3,\3,\3,\3,\7,\u044e\n,\f,\16,\u0451\13,\3,\5,\u0454\n,\3-\3-\3-\3"+
		"-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3"+
		"-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\5-\u0481\n-\3.\5.\u0484"+
		"\n.\3.\3.\3.\3.\3/\3/\3/\3/\7/\u048e\n/\f/\16/\u0491\13/\5/\u0493\n/\3"+
		"/\3/\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\5\60\u04a0\n\60\3\61"+
		"\3\61\3\61\3\61\7\61\u04a6\n\61\f\61\16\61\u04a9\13\61\5\61\u04ab\n\61"+
		"\3\61\3\61\3\61\3\61\3\61\7\61\u04b2\n\61\f\61\16\61\u04b5\13\61\5\61"+
		"\u04b7\n\61\3\61\5\61\u04ba\n\61\3\62\3\62\3\62\3\62\3\62\7\62\u04c1\n"+
		"\62\f\62\16\62\u04c4\13\62\3\63\3\63\3\63\3\63\3\64\3\64\3\65\3\65\3\65"+
		"\3\65\5\65\u04d0\n\65\3\65\3\65\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66"+
		"\3\66\3\66\3\66\3\66\3\66\3\66\3\66\3\66\5\66\u04e4\n\66\3\66\3\66\3\66"+
		"\3\66\3\66\3\66\7\66\u04ec\n\66\f\66\16\66\u04ef\13\66\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\3\67\3\67\3\67\3\67\3\67\5\67\u04fc\n\67\3\67\3\67\3\67"+
		"\3\67\3\67\3\67\7\67\u0504\n\67\f\67\16\67\u0507\13\67\38\38\38\38\38"+
		"\38\38\38\38\58\u0512\n8\38\78\u0515\n8\f8\168\u0518\138\38\58\u051b\n"+
		"8\39\39\39\39\39\39\39\39\39\39\59\u0527\n9\3:\3:\3:\3:\3:\3:\5:\u052f"+
		"\n:\3;\3;\3;\3;\3;\3;\5;\u0537\n;\3;\3;\3;\3;\3;\3<\3<\3<\5<\u0541\n<"+
		"\3<\3<\3<\3<\5<\u0547\n<\3=\3=\3=\3=\3=\5=\u054e\n=\3>\7>\u0551\n>\f>"+
		"\16>\u0554\13>\3>\5>\u0557\n>\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3"+
		"?\3?\3?\3?\3?\5?\u056b\n?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3"+
		"?\3?\3?\3?\5?\u057f\n?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3"+
		"?\3?\3?\5?\u0593\n?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3"+
		"?\3?\5?\u05a7\n?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3?\3"+
		"?\5?\u05bb\n?\5?\u05bd\n?\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\5@\u05c9\n@\3"+
		"@\3@\7@\u05cd\n@\f@\16@\u05d0\13@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@"+
		"\3@\3@\3@\3@\3@\3@\5@\u05e4\n@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@\3@"+
		"\3@\3@\3@\3@\3@\3@\5@\u05f9\n@\6@\u05fb\n@\r@\16@\u05fc\3A\3A\3A\5A\u0602"+
		"\nA\3A\3A\3A\3A\5A\u0608\nA\3A\3A\3A\3A\3A\5A\u060f\nA\3A\3A\3A\3A\5A"+
		"\u0615\nA\5A\u0617\nA\3B\3B\3B\3B\3B\3B\3B\3B\3B\3B\3B\3B\3B\3B\5B\u0627"+
		"\nB\3C\5C\u062a\nC\3C\3C\3C\5C\u062f\nC\3D\3D\3D\3D\5D\u0635\nD\3E\3E"+
		"\5E\u0639\nE\3F\3F\3F\3F\5F\u063f\nF\3F\3F\3F\3F\3F\3F\5F\u0647\nF\3F"+
		"\3F\3F\3F\3F\3F\5F\u064f\nF\3F\5F\u0652\nF\5F\u0654\nF\3G\3G\3G\3G\5G"+
		"\u065a\nG\3G\3G\5G\u065e\nG\3G\3G\3G\3G\3G\5G\u0665\nG\3G\5G\u0668\nG"+
		"\5G\u066a\nG\3H\3H\3H\3H\3H\3H\3H\3H\3H\5H\u0675\nH\3I\5I\u0678\nI\3I"+
		"\3I\3I\3I\3J\3J\3J\5J\u0681\nJ\3J\3J\3J\3J\3J\3J\3K\3K\3K\5K\u068c\nK"+
		"\3K\3K\3K\3K\5K\u0692\nK\3L\3L\3L\5L\u0697\nL\3L\3L\3L\3L\5L\u069d\nL"+
		"\3M\3M\3M\3M\5M\u06a3\nM\3M\3M\3N\3N\3N\3N\3N\3N\3N\3N\3N\3N\3N\3N\3N"+
		"\3N\5N\u06b5\nN\3N\3N\3N\3N\3N\3N\7N\u06bd\nN\fN\16N\u06c0\13N\3O\3O\3"+
		"O\3O\3O\3O\5O\u06c8\nO\5O\u06ca\nO\3P\3P\3P\3P\3P\3P\3P\5P\u06d3\nP\3"+
		"P\3P\3P\3P\3P\3P\3P\3P\3P\3P\3P\5P\u06e0\nP\5P\u06e2\nP\3Q\3Q\3Q\7Q\u06e7"+
		"\nQ\fQ\16Q\u06ea\13Q\3Q\3Q\3Q\3R\3R\3R\3R\3R\3R\5R\u06f5\nR\3R\3R\3R\3"+
		"R\3R\3R\3R\3R\5R\u06ff\nR\3S\3S\3S\7S\u0704\nS\fS\16S\u0707\13S\5S\u0709"+
		"\nS\3T\3T\5T\u070d\nT\3U\3U\3U\3U\3U\3U\3U\3U\7U\u0717\nU\fU\16U\u071a"+
		"\13U\5U\u071c\nU\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\3U\7U\u072e"+
		"\nU\fU\16U\u0731\13U\3U\3U\5U\u0735\nU\3V\3V\3V\3V\3V\3V\3V\3V\3V\3V\3"+
		"V\3V\3V\3V\3V\3V\3V\3V\5V\u0749\nV\5V\u074b\nV\3W\3W\3W\3W\3W\3W\3W\3"+
		"W\3W\3W\5W\u0757\nW\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\5W\u0767"+
		"\nW\3W\7W\u076a\nW\fW\16W\u076d\13W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\5W\u0779"+
		"\nW\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\5W\u0786\nW\6W\u0788\nW\rW\16W\u0789"+
		"\3W\3W\3W\3W\3W\3W\3W\3W\3W\3W\5W\u0796\nW\3W\3W\3W\3W\5W\u079c\nW\3X"+
		"\3X\3X\3X\3X\3X\3X\3X\3X\3X\5X\u07a8\nX\3Y\3Y\3Y\3Y\3Y\3Y\3Y\3Y\3Y\3Y"+
		"\3Y\3Y\3Y\3Y\3Y\3Y\3Y\5Y\u07bb\nY\3Z\3Z\3Z\5Z\u07c0\nZ\3Z\3Z\3Z\3Z\3Z"+
		"\3[\3[\3[\5[\u07ca\n[\3[\3[\3[\3[\3[\3\\\3\\\3\\\3\\\3\\\3\\\3]\3]\5]"+
		"\u07d9\n]\3]\3]\3]\5]\u07de\n]\7]\u07e0\n]\f]\16]\u07e3\13]\5]\u07e5\n"+
		"]\3^\3^\3^\7^\u07ea\n^\f^\16^\u07ed\13^\3^\3^\3^\5^\u07f2\n^\5^\u07f4"+
		"\n^\3_\5_\u07f7\n_\3_\3_\3_\7_\u07fc\n_\f_\16_\u07ff\13_\3_\5_\u0802\n"+
		"_\3`\3`\3`\5`\u0807\n`\3`\3`\3`\7`\u080c\n`\f`\16`\u080f\13`\3a\3a\3a"+
		"\3a\5a\u0815\na\3a\3a\3a\3a\3a\7a\u081c\na\fa\16a\u081f\13a\3a\3a\5a\u0823"+
		"\na\3a\3a\3a\3a\3a\3a\3a\3a\3a\3a\5a\u082f\na\3a\3a\5a\u0833\na\7a\u0835"+
		"\na\fa\16a\u0838\13a\3a\5a\u083b\na\3a\3a\3a\3a\3a\5a\u0842\na\3a\5a\u0845"+
		"\na\5a\u0847\na\3b\3b\3c\3c\3c\5c\u084e\nc\3c\5c\u0851\nc\3d\3d\5d\u0855"+
		"\nd\3d\3d\3d\3d\3d\3d\3d\3d\3d\5d\u0860\nd\3e\3e\3e\3e\3e\3e\7e\u0868"+
		"\ne\fe\16e\u086b\13e\3e\3e\3f\3f\3f\3f\3f\3f\7f\u0875\nf\ff\16f\u0878"+
		"\13f\3f\3f\3f\3g\3g\5g\u087f\ng\3g\5g\u0882\ng\3g\3g\3g\3g\7g\u0888\n"+
		"g\fg\16g\u088b\13g\3g\3g\3h\3h\5h\u0891\nh\3h\3h\3h\3h\7h\u0897\nh\fh"+
		"\16h\u089a\13h\3h\3h\3i\3i\3j\3j\3j\5j\u08a3\nj\3j\3j\3j\3j\3j\3j\5j\u08ab"+
		"\nj\3k\3k\3k\5k\u08b0\nk\3k\5k\u08b3\nk\3l\3l\3l\5l\u08b8\nl\3m\3m\3m"+
		"\3m\3m\7m\u08bf\nm\fm\16m\u08c2\13m\3m\3m\5m\u08c6\nm\3m\3m\3m\3m\3m\3"+
		"n\3n\3n\3n\3n\3n\3n\5n\u08d4\nn\3n\5n\u08d7\nn\5n\u08d9\nn\3o\3o\3o\5"+
		"o\u08de\no\3o\3o\5o\u08e2\no\3o\5o\u08e5\no\3o\3o\3o\3o\3o\5o\u08ec\n"+
		"o\3o\3o\3o\3o\7o\u08f2\no\fo\16o\u08f5\13o\3o\5o\u08f8\no\3o\3o\5o\u08fc"+
		"\no\3o\5o\u08ff\no\3o\3o\3o\3o\5o\u0905\no\3o\5o\u0908\no\5o\u090a\no"+
		"\3p\3p\3p\3p\3p\7p\u0911\np\fp\16p\u0914\13p\3q\3q\3q\5q\u0919\nq\3q\5"+
		"q\u091c\nq\3q\5q\u091f\nq\3r\3r\5r\u0923\nr\3s\3s\5s\u0927\ns\3s\3s\3"+
		"s\7s\u092c\ns\fs\16s\u092f\13s\3s\3s\3s\3s\7s\u0935\ns\fs\16s\u0938\13"+
		"s\3s\5s\u093b\ns\5s\u093d\ns\3s\3s\5s\u0941\ns\3s\3s\3s\3s\3s\7s\u0948"+
		"\ns\fs\16s\u094b\13s\3s\3s\5s\u094f\ns\5s\u0951\ns\3s\3s\3s\3s\3s\7s\u0958"+
		"\ns\fs\16s\u095b\13s\3s\3s\3s\3s\3s\3s\7s\u0963\ns\fs\16s\u0966\13s\3"+
		"s\3s\7s\u096a\ns\fs\16s\u096d\13s\5s\u096f\ns\3t\3t\3t\3t\3t\5t\u0976"+
		"\nt\3t\3t\3t\3t\3t\7t\u097d\nt\ft\16t\u0980\13t\3t\3t\3u\3u\3v\3v\7v\u0988"+
		"\nv\fv\16v\u098b\13v\3w\3w\3w\3w\3w\7w\u0992\nw\fw\16w\u0995\13w\3w\3"+
		"w\5w\u0999\nw\3x\5x\u099c\nx\3x\3x\5x\u09a0\nx\3y\3y\3z\3z\3z\3z\3z\3"+
		"z\3z\3z\5z\u09ac\nz\3{\3{\3|\3|\3}\3}\5}\u09b4\n}\3~\3~\3\177\3\177\3"+
		"\u0080\6\u0080\u09bb\n\u0080\r\u0080\16\u0080\u09bc\3\u0081\3\u0081\3"+
		"\u0082\3\u0082\3\u0083\3\u0083\3\u0084\3\u0084\3\u0085\3\u0085\3\u0086"+
		"\3\u0086\3\u0087\3\u0087\3\u0088\3\u0088\3\u0089\3\u0089\3\u008a\3\u008a"+
		"\3\u008b\3\u008b\3\u008c\3\u008c\3\u008d\3\u008d\3\u008e\3\u008e\3\u008f"+
		"\3\u008f\3\u0090\3\u0090\3\u0091\3\u0091\3\u0092\3\u0092\3\u0093\3\u0093"+
		"\3\u0093\3\u0093\3\u0093\3\u0093\5\u0093\u09e9\n\u0093\3\u0093\2\bFHj"+
		"l\u009a\u00be\u0094\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60"+
		"\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086"+
		"\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e"+
		"\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6"+
		"\u00b8\u00ba\u00bc\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca\u00cc\u00ce"+
		"\u00d0\u00d2\u00d4\u00d6\u00d8\u00da\u00dc\u00de\u00e0\u00e2\u00e4\u00e6"+
		"\u00e8\u00ea\u00ec\u00ee\u00f0\u00f2\u00f4\u00f6\u00f8\u00fa\u00fc\u00fe"+
		"\u0100\u0102\u0104\u0106\u0108\u010a\u010c\u010e\u0110\u0112\u0114\u0116"+
		"\u0118\u011a\u011c\u011e\u0120\u0122\u0124\2\26\4\2\23\23\u0082\u0082"+
		"\4\2\63\63TT\4\288RR\3\2(,\4\2\25\25\32\33\3\2\26\27\4\2\34\36  \3\2!"+
		"$\4\2\24\24!&\4\2\66\66\u0085\u0085\4\2\31\31\37\37\4\2\25\27\32\32\3"+
		"\2!&\4\2\25\27\32\33\4\2\36\36  \4\2QQ\u00a6\u00a6\6\2JL\u0080\u0080\u00bc"+
		"\u00bc\u00be\u00bf\4\2\26\30~~\4\2\u00ba\u00ba\u00be\u00be\4\2-\u0083"+
		"\u0085\u00b9\2\u0b74\2\u012b\3\2\2\2\4\u0130\3\2\2\2\6\u0136\3\2\2\2\b"+
		"\u0154\3\2\2\2\n\u0156\3\2\2\2\f\u016a\3\2\2\2\16\u016f\3\2\2\2\20\u0172"+
		"\3\2\2\2\22\u0195\3\2\2\2\24\u0199\3\2\2\2\26\u019d\3\2\2\2\30\u01af\3"+
		"\2\2\2\32\u01b1\3\2\2\2\34\u01b3\3\2\2\2\36\u01b5\3\2\2\2 \u01b7\3\2\2"+
		"\2\"\u01be\3\2\2\2$\u01cb\3\2\2\2&\u01e5\3\2\2\2(\u01ea\3\2\2\2*\u0220"+
		"\3\2\2\2,\u027f\3\2\2\2.\u0281\3\2\2\2\60\u028a\3\2\2\2\62\u0295\3\2\2"+
		"\2\64\u0298\3\2\2\2\66\u02a0\3\2\2\28\u02b8\3\2\2\2:\u02c9\3\2\2\2<\u02d1"+
		"\3\2\2\2>\u02d3\3\2\2\2@\u02d6\3\2\2\2B\u02d8\3\2\2\2D\u02f0\3\2\2\2F"+
		"\u0326\3\2\2\2H\u0399\3\2\2\2J\u03e2\3\2\2\2L\u0402\3\2\2\2N\u0404\3\2"+
		"\2\2P\u0412\3\2\2\2R\u0414\3\2\2\2T\u041c\3\2\2\2V\u044f\3\2\2\2X\u0480"+
		"\3\2\2\2Z\u0483\3\2\2\2\\\u0489\3\2\2\2^\u049f\3\2\2\2`\u04b9\3\2\2\2"+
		"b\u04bb\3\2\2\2d\u04c5\3\2\2\2f\u04c9\3\2\2\2h\u04cb\3\2\2\2j\u04e3\3"+
		"\2\2\2l\u04fb\3\2\2\2n\u0508\3\2\2\2p\u051c\3\2\2\2r\u0528\3\2\2\2t\u0530"+
		"\3\2\2\2v\u053d\3\2\2\2x\u0548\3\2\2\2z\u0552\3\2\2\2|\u05bc\3\2\2\2~"+
		"\u05ce\3\2\2\2\u0080\u0616\3\2\2\2\u0082\u0618\3\2\2\2\u0084\u062e\3\2"+
		"\2\2\u0086\u0634\3\2\2\2\u0088\u0638\3\2\2\2\u008a\u0653\3\2\2\2\u008c"+
		"\u0669\3\2\2\2\u008e\u066b\3\2\2\2\u0090\u0677\3\2\2\2\u0092\u0680\3\2"+
		"\2\2\u0094\u0691\3\2\2\2\u0096\u069c\3\2\2\2\u0098\u069e\3\2\2\2\u009a"+
		"\u06b4\3\2\2\2\u009c\u06c1\3\2\2\2\u009e\u06e1\3\2\2\2\u00a0\u06e3\3\2"+
		"\2\2\u00a2\u06fe\3\2\2\2\u00a4\u0708\3\2\2\2\u00a6\u070c\3\2\2\2\u00a8"+
		"\u0734\3\2\2\2\u00aa\u074a\3\2\2\2\u00ac\u079b\3\2\2\2\u00ae\u079d\3\2"+
		"\2\2\u00b0\u07a9\3\2\2\2\u00b2\u07bf\3\2\2\2\u00b4\u07c9\3\2\2\2\u00b6"+
		"\u07d0\3\2\2\2\u00b8\u07e4\3\2\2\2\u00ba\u07f3\3\2\2\2\u00bc\u0801\3\2"+
		"\2\2\u00be\u0806\3\2\2\2\u00c0\u0810\3\2\2\2\u00c2\u0848\3\2\2\2\u00c4"+
		"\u084a\3\2\2\2\u00c6\u0854\3\2\2\2\u00c8\u0861\3\2\2\2\u00ca\u086e\3\2"+
		"\2\2\u00cc\u087c\3\2\2\2\u00ce\u088e\3\2\2\2\u00d0\u089d\3\2\2\2\u00d2"+
		"\u08a2\3\2\2\2\u00d4\u08ac\3\2\2\2\u00d6\u08b7\3\2\2\2\u00d8\u08b9\3\2"+
		"\2\2\u00da\u08d8\3\2\2\2\u00dc\u0909\3\2\2\2\u00de\u090b\3\2\2\2\u00e0"+
		"\u091e\3\2\2\2\u00e2\u0922\3\2\2\2\u00e4\u096e\3\2\2\2\u00e6\u0970\3\2"+
		"\2\2\u00e8\u0983\3\2\2\2\u00ea\u0985\3\2\2\2\u00ec\u098c\3\2\2\2\u00ee"+
		"\u099f\3\2\2\2\u00f0\u09a1\3\2\2\2\u00f2\u09ab\3\2\2\2\u00f4\u09ad\3\2"+
		"\2\2\u00f6\u09af\3\2\2\2\u00f8\u09b3\3\2\2\2\u00fa\u09b5\3\2\2\2\u00fc"+
		"\u09b7\3\2\2\2\u00fe\u09ba\3\2\2\2\u0100\u09be\3\2\2\2\u0102\u09c0\3\2"+
		"\2\2\u0104\u09c2\3\2\2\2\u0106\u09c4\3\2\2\2\u0108\u09c6\3\2\2\2\u010a"+
		"\u09c8\3\2\2\2\u010c\u09ca\3\2\2\2\u010e\u09cc\3\2\2\2\u0110\u09ce\3\2"+
		"\2\2\u0112\u09d0\3\2\2\2\u0114\u09d2\3\2\2\2\u0116\u09d4\3\2\2\2\u0118"+
		"\u09d6\3\2\2\2\u011a\u09d8\3\2\2\2\u011c\u09da\3\2\2\2\u011e\u09dc\3\2"+
		"\2\2\u0120\u09de\3\2\2\2\u0122\u09e0\3\2\2\2\u0124\u09e8\3\2\2\2\u0126"+
		"\u012a\5R*\2\u0127\u012a\5\6\4\2\u0128\u012a\5\4\3\2\u0129\u0126\3\2\2"+
		"\2\u0129\u0127\3\2\2\2\u0129\u0128\3\2\2\2\u012a\u012d\3\2\2\2\u012b\u0129"+
		"\3\2\2\2\u012b\u012c\3\2\2\2\u012c\u012e\3\2\2\2\u012d\u012b\3\2\2\2\u012e"+
		"\u012f\7\2\2\3\u012f\3\3\2\2\2\u0130\u0131\7\u00c3\2\2\u0131\u0132\b\3"+
		"\1\2\u0132\5\3\2\2\2\u0133\u0135\7\17\2\2\u0134\u0133\3\2\2\2\u0135\u0138"+
		"\3\2\2\2\u0136\u0134\3\2\2\2\u0136\u0137\3\2\2\2\u0137\u0139\3\2\2\2\u0138"+
		"\u0136\3\2\2\2\u0139\u0142\5\b\5\2\u013a\u013c\7\17\2\2\u013b\u013a\3"+
		"\2\2\2\u013c\u013d\3\2\2\2\u013d\u013b\3\2\2\2\u013d\u013e\3\2\2\2\u013e"+
		"\u013f\3\2\2\2\u013f\u0141\5\b\5\2\u0140\u013b\3\2\2\2\u0141\u0144\3\2"+
		"\2\2\u0142\u0140\3\2\2\2\u0142\u0143\3\2\2\2\u0143\u0148\3\2\2\2\u0144"+
		"\u0142\3\2\2\2\u0145\u0147\7\17\2\2\u0146\u0145\3\2\2\2\u0147\u014a\3"+
		"\2\2\2\u0148\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149\7\3\2\2\2\u014a\u0148"+
		"\3\2\2\2\u014b\u0155\5\n\6\2\u014c\u0155\5\20\t\2\u014d\u0155\5 \21\2"+
		"\u014e\u0155\5\"\22\2\u014f\u0155\5$\23\2\u0150\u0155\5(\25\2\u0151\u0155"+
		"\5.\30\2\u0152\u0155\5\26\f\2\u0153\u0155\5\u00e6t\2\u0154\u014b\3\2\2"+
		"\2\u0154\u014c\3\2\2\2\u0154\u014d\3\2\2\2\u0154\u014e\3\2\2\2\u0154\u014f"+
		"\3\2\2\2\u0154\u0150\3\2\2\2\u0154\u0151\3\2\2\2\u0154\u0152\3\2\2\2\u0154"+
		"\u0153\3\2\2\2\u0155\t\3\2\2\2\u0156\u0157\7\64\2\2\u0157\u015b\7\u009b"+
		"\2\2\u0158\u0159\5\u0104\u0083\2\u0159\u015a\7\20\2\2\u015a\u015c\3\2"+
		"\2\2\u015b\u0158\3\2\2\2\u015b\u015c\3\2\2\2\u015c\u015d\3\2\2\2\u015d"+
		"\u0168\5\u0106\u0084\2\u015e\u015f\7\u0092\2\2\u015f\u0160\7\u00a1\2\2"+
		"\u0160\u0169\5\u010c\u0087\2\u0161\u0169\5\16\b\2\u0162\u0169\5\f\7\2"+
		"\u0163\u0165\7/\2\2\u0164\u0166\7D\2\2\u0165\u0164\3\2\2\2\u0165\u0166"+
		"\3\2\2\2\u0166\u0167\3\2\2\2\u0167\u0169\5\64\33\2\u0168\u015e\3\2\2\2"+
		"\u0168\u0161\3\2\2\2\u0168\u0162\3\2\2\2\u0168\u0163\3\2\2\2\u0169\13"+
		"\3\2\2\2\u016a\u016b\7/\2\2\u016b\u016c\7G\2\2\u016c\u016d\5\u0124\u0093"+
		"\2\u016d\u016e\5\u00c6d\2\u016e\r\3\2\2\2\u016f\u0170\7/\2\2\u0170\u0171"+
		"\5\u00c6d\2\u0171\17\3\2\2\2\u0172\u0173\7H\2\2\u0173\u0177\7\u009b\2"+
		"\2\u0174\u0175\7g\2\2\u0175\u0176\7~\2\2\u0176\u0178\7]\2\2\u0177\u0174"+
		"\3\2\2\2\u0177\u0178\3\2\2\2\u0178\u017c\3\2\2\2\u0179\u017a\5\u0104\u0083"+
		"\2\u017a\u017b\7\20\2\2\u017b\u017d\3\2\2\2\u017c\u0179\3\2\2\2\u017c"+
		"\u017d\3\2\2\2\u017d\u017e\3\2\2\2\u017e\u0193\5\u0108\u0085\2\u017f\u0180"+
		"\7\21\2\2\u0180\u0187\5\64\33\2\u0181\u0182\7\23\2\2\u0182\u0186\5\u00c6"+
		"d\2\u0183\u0184\7\23\2\2\u0184\u0186\5\64\33\2\u0185\u0181\3\2\2\2\u0185"+
		"\u0183\3\2\2\2\u0186\u0189\3\2\2\2\u0187\u0185\3\2\2\2\u0187\u0188\3\2"+
		"\2\2\u0188\u018a\3\2\2\2\u0189\u0187\3\2\2\2\u018a\u018c\7\22\2\2\u018b"+
		"\u018d\5\22\n\2\u018c\u018b\3\2\2\2\u018c\u018d\3\2\2\2\u018d\u018f\3"+
		"\2\2\2\u018e\u0190\5\24\13\2\u018f\u018e\3\2\2\2\u018f\u0190\3\2\2\2\u0190"+
		"\u0194\3\2\2\2\u0191\u0192\7\67\2\2\u0192\u0194\5*\26\2\u0193\u017f\3"+
		"\2\2\2\u0193\u0191\3\2\2\2\u0194\21\3\2\2\2\u0195\u0196\7\u009c\2\2\u0196"+
		"\u0197\7\24\2\2\u0197\u0198\7\u00ba\2\2\u0198\23\3\2\2\2\u0199\u019a\7"+
		"\u009d\2\2\u019a\u019b\7\24\2\2\u019b\u019c\7\u00ba\2\2\u019c\25\3\2\2"+
		"\2\u019d\u019e\7H\2\2\u019e\u019f\7\60\2\2\u019f\u01a0\5\u0102\u0082\2"+
		"\u01a0\u01ab\7\21\2\2\u01a1\u01a2\5\30\r\2\u01a2\u01a3\7\23\2\2\u01a3"+
		"\u01a4\5\32\16\2\u01a4\u01a5\7\23\2\2\u01a5\u01a6\5\36\20\2\u01a6\u01a7"+
		"\7\23\2\2\u01a7\u01a8\5\34\17\2\u01a8\u01a9\7\23\2\2\u01a9\u01aa\5\u00a0"+
		"Q\2\u01aa\u01ac\3\2\2\2\u01ab\u01a1\3\2\2\2\u01ab\u01ac\3\2\2\2\u01ac"+
		"\u01ad\3\2\2\2\u01ad\u01ae\7\22\2\2\u01ae\27\3\2\2\2\u01af\u01b0\7\u00ba"+
		"\2\2\u01b0\31\3\2\2\2\u01b1\u01b2\7\u00ba\2\2\u01b2\33\3\2\2\2\u01b3\u01b4"+
		"\7\u00ba\2\2\u01b4\35\3\2\2\2\u01b5\u01b6\7\u00ba\2\2\u01b6\37\3\2\2\2"+
		"\u01b7\u01b8\7Q\2\2\u01b8\u01b9\7b\2\2\u01b9\u01bc\5\u00d2j\2\u01ba\u01bb"+
		"\7\u00ad\2\2\u01bb\u01bd\5F$\2\u01bc\u01ba\3\2\2\2\u01bc\u01bd\3\2\2\2"+
		"\u01bd!\3\2\2\2\u01be\u01bf\7U\2\2\u01bf\u01c2\7\u009b\2\2\u01c0\u01c1"+
		"\7g\2\2\u01c1\u01c3\7]\2\2\u01c2\u01c0\3\2\2\2\u01c2\u01c3\3\2\2\2\u01c3"+
		"\u01c7\3\2\2\2\u01c4\u01c5\5\u0104\u0083\2\u01c5\u01c6\7\20\2\2\u01c6"+
		"\u01c8\3\2\2\2\u01c7\u01c4\3\2\2\2\u01c7\u01c8\3\2\2\2\u01c8\u01c9\3\2"+
		"\2\2\u01c9\u01ca\5\u0108\u0085\2\u01ca#\3\2\2\2\u01cb\u01d6\5\u00e4s\2"+
		"\u01cc\u01cd\7\u0086\2\2\u01cd\u01ce\7>\2\2\u01ce\u01d3\5\u00d4k\2\u01cf"+
		"\u01d0\7\23\2\2\u01d0\u01d2\5\u00d4k\2\u01d1\u01cf\3\2\2\2\u01d2\u01d5"+
		"\3\2\2\2\u01d3\u01d1\3\2\2\2\u01d3\u01d4\3\2\2\2\u01d4\u01d7\3\2\2\2\u01d5"+
		"\u01d3\3\2\2\2\u01d6\u01cc\3\2\2\2\u01d6\u01d7\3\2\2\2\u01d7\u01de\3\2"+
		"\2\2\u01d8\u01d9\7y\2\2\u01d9\u01dc\5F$\2\u01da\u01db\t\2\2\2\u01db\u01dd"+
		"\5F$\2\u01dc\u01da\3\2\2\2\u01dc\u01dd\3\2\2\2\u01dd\u01df\3\2\2\2\u01de"+
		"\u01d8\3\2\2\2\u01de\u01df\3\2\2\2\u01df%\3\2\2\2\u01e0\u01e2\7\u00b2"+
		"\2\2\u01e1\u01e0\3\2\2\2\u01e1\u01e2\3\2\2\2\u01e2\u01e3\3\2\2\2\u01e3"+
		"\u01e6\7\u00ba\2\2\u01e4\u01e6\5\u009eP\2\u01e5\u01e1\3\2\2\2\u01e5\u01e4"+
		"\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7\u01e8\7\24\2\2\u01e8\u01e9\5$\23\2"+
		"\u01e9\'\3\2\2\2\u01ea\u01eb\7o\2\2\u01eb\u01ef\7r\2\2\u01ec\u01ed\5\u0104"+
		"\u0083\2\u01ed\u01ee\7\20\2\2\u01ee\u01f0\3\2\2\2\u01ef\u01ec\3\2\2\2"+
		"\u01ef\u01f0\3\2\2\2\u01f0\u01f1\3\2\2\2\u01f1\u01fd\5\u0108\u0085\2\u01f2"+
		"\u01f3\7\21\2\2\u01f3\u01f8\5\u010e\u0088\2\u01f4\u01f5\7\23\2\2\u01f5"+
		"\u01f7\5\u010e\u0088\2\u01f6\u01f4\3\2\2\2\u01f7\u01fa\3\2\2\2\u01f8\u01f6"+
		"\3\2\2\2\u01f8\u01f9\3\2\2\2\u01f9\u01fb\3\2\2\2\u01fa\u01f8\3\2\2\2\u01fb"+
		"\u01fc\7\22\2\2\u01fc\u01fe\3\2\2\2\u01fd\u01f2\3\2\2\2\u01fd\u01fe\3"+
		"\2\2\2\u01fe\u021e\3\2\2\2\u01ff\u0200\7\u00a9\2\2\u0200\u0201\7\21\2"+
		"\2\u0201\u0206\5F$\2\u0202\u0203\7\23\2\2\u0203\u0205\5F$\2\u0204\u0202"+
		"\3\2\2\2\u0205\u0208\3\2\2\2\u0206\u0204\3\2\2\2\u0206\u0207\3\2\2\2\u0207"+
		"\u0209\3\2\2\2\u0208\u0206\3\2\2\2\u0209\u0218\7\22\2\2\u020a\u020b\7"+
		"\23\2\2\u020b\u020c\7\21\2\2\u020c\u0211\5F$\2\u020d\u020e\7\23\2\2\u020e"+
		"\u0210\5F$\2\u020f\u020d\3\2\2\2\u0210\u0213\3\2\2\2\u0211\u020f\3\2\2"+
		"\2\u0211\u0212\3\2\2\2\u0212\u0214\3\2\2\2\u0213\u0211\3\2\2\2\u0214\u0215"+
		"\7\22\2\2\u0215\u0217\3\2\2\2\u0216\u020a\3\2\2\2\u0217\u021a\3\2\2\2"+
		"\u0218\u0216\3\2\2\2\u0218\u0219\3\2\2\2\u0219\u021f\3\2\2\2\u021a\u0218"+
		"\3\2\2\2\u021b\u021f\5$\23\2\u021c\u021d\7N\2\2\u021d\u021f\7\u00a9\2"+
		"\2\u021e\u01ff\3\2\2\2\u021e\u021b\3\2\2\2\u021e\u021c\3\2\2\2\u021f)"+
		"\3\2\2\2\u0220\u022b\5,\27\2\u0221\u0222\7\u0086\2\2\u0222\u0223\7>\2"+
		"\2\u0223\u0228\5\u00d4k\2\u0224\u0225\7\23\2\2\u0225\u0227\5\u00d4k\2"+
		"\u0226\u0224\3\2\2\2\u0227\u022a\3\2\2\2\u0228\u0226\3\2\2\2\u0228\u0229"+
		"\3\2\2\2\u0229\u022c\3\2\2\2\u022a\u0228\3\2\2\2\u022b\u0221\3\2\2\2\u022b"+
		"\u022c\3\2\2\2\u022c\u0233\3\2\2\2\u022d\u022e\7y\2\2\u022e\u0231\5F$"+
		"\2\u022f\u0230\t\2\2\2\u0230\u0232\5F$\2\u0231\u022f\3\2\2\2\u0231\u0232"+
		"\3\2\2\2\u0232\u0234\3\2\2\2\u0233\u022d\3\2\2\2\u0233\u0234\3\2\2\2\u0234"+
		"+\3\2\2\2\u0235\u0237\7\u0099\2\2\u0236\u0238\t\3\2\2\u0237\u0236\3\2"+
		"\2\2\u0237\u0238\3\2\2\2\u0238\u0239\3\2\2\2\u0239\u023e\5\u00dan\2\u023a"+
		"\u023b\7\23\2\2\u023b\u023d\5\u00dan\2\u023c\u023a\3\2\2\2\u023d\u0240"+
		"\3\2\2\2\u023e\u023c\3\2\2\2\u023e\u023f\3\2\2\2\u023f\u024d\3\2\2\2\u0240"+
		"\u023e\3\2\2\2\u0241\u024b\7b\2\2\u0242\u0247\5\u00dco\2\u0243\u0244\7"+
		"\23\2\2\u0244\u0246\5\u00dco\2\u0245\u0243\3\2\2\2\u0246\u0249\3\2\2\2"+
		"\u0247\u0245\3\2\2\2\u0247\u0248\3\2\2\2\u0248\u024c\3\2\2\2\u0249\u0247"+
		"\3\2\2\2\u024a\u024c\5\u00dep\2\u024b\u0242\3\2\2\2\u024b\u024a\3\2\2"+
		"\2\u024c\u024e\3\2\2\2\u024d\u0241\3\2\2\2\u024d\u024e\3\2\2\2\u024e\u0251"+
		"\3\2\2\2\u024f\u0250\7\u00ad\2\2\u0250\u0252\5F$\2\u0251\u024f\3\2\2\2"+
		"\u0251\u0252\3\2\2\2\u0252\u0261\3\2\2\2\u0253\u0254\7e\2\2\u0254\u0255"+
		"\7>\2\2\u0255\u025a\5F$\2\u0256\u0257\7\23\2\2\u0257\u0259\5F$\2\u0258"+
		"\u0256\3\2\2\2\u0259\u025c\3\2\2\2\u025a\u0258\3\2\2\2\u025a\u025b\3\2"+
		"\2\2\u025b\u025f\3\2\2\2\u025c\u025a\3\2\2\2\u025d\u025e\7f\2\2\u025e"+
		"\u0260\5F$\2\u025f\u025d\3\2\2\2\u025f\u0260\3\2\2\2\u0260\u0262\3\2\2"+
		"\2\u0261\u0253\3\2\2\2\u0261\u0262\3\2\2\2\u0262\u0280\3\2\2\2\u0263\u0264"+
		"\7\u00a9\2\2\u0264\u0265\7\21\2\2\u0265\u026a\5F$\2\u0266\u0267\7\23\2"+
		"\2\u0267\u0269\5F$\2\u0268\u0266\3\2\2\2\u0269\u026c\3\2\2\2\u026a\u0268"+
		"\3\2\2\2\u026a\u026b\3\2\2\2\u026b\u026d\3\2\2\2\u026c\u026a\3\2\2\2\u026d"+
		"\u027c\7\22\2\2\u026e\u026f\7\23\2\2\u026f\u0270\7\21\2\2\u0270\u0275"+
		"\5F$\2\u0271\u0272\7\23\2\2\u0272\u0274\5F$\2\u0273\u0271\3\2\2\2\u0274"+
		"\u0277\3\2\2\2\u0275\u0273\3\2\2\2\u0275\u0276\3\2\2\2\u0276\u0278\3\2"+
		"\2\2\u0277\u0275\3\2\2\2\u0278\u0279\7\22\2\2\u0279\u027b\3\2\2\2\u027a"+
		"\u026e\3\2\2\2\u027b\u027e\3\2\2\2\u027c\u027a\3\2\2\2\u027c\u027d\3\2"+
		"\2\2\u027d\u0280\3\2\2\2\u027e\u027c\3\2\2\2\u027f\u0235\3\2\2\2\u027f"+
		"\u0263\3\2\2\2\u0280-\3\2\2\2\u0281\u0282\7\u00a6\2\2\u0282\u0283\5\u00d2"+
		"j\2\u0283\u0284\7\u009a\2\2\u0284\u0285\5\60\31\2\u0285\u0288\5\62\32"+
		"\2\u0286\u0287\7\u00ad\2\2\u0287\u0289\5F$\2\u0288\u0286\3\2\2\2\u0288"+
		"\u0289\3\2\2\2\u0289/\3\2\2\2\u028a\u028b\5\u010e\u0088\2\u028b\u028c"+
		"\7\24\2\2\u028c\u028d\5F$\2\u028d\61\3\2\2\2\u028e\u028f\7\23\2\2\u028f"+
		"\u0290\5\u010e\u0088\2\u0290\u0291\7\24\2\2\u0291\u0292\5F$\2\u0292\u0294"+
		"\3\2\2\2\u0293\u028e\3\2\2\2\u0294\u0297\3\2\2\2\u0295\u0293\3\2\2\2\u0295"+
		"\u0296\3\2\2\2\u0296\63\3\2\2\2\u0297\u0295\3\2\2\2\u0298\u029d\5\u010e"+
		"\u0088\2\u0299\u029c\58\35\2\u029a\u029c\5\66\34\2\u029b\u0299\3\2\2\2"+
		"\u029b\u029a\3\2\2\2\u029c\u029f\3\2\2\2\u029d\u029b\3\2\2\2\u029d\u029e"+
		"\3\2\2\2\u029e\65\3\2\2\2\u029f\u029d\3\2\2\2\u02a0\u02b4\5\u0100\u0081"+
		"\2\u02a1\u02a2\7\21\2\2\u02a2\u02a4\5\u00eex\2\u02a3\u02a5\5\u0124\u0093"+
		"\2\u02a4\u02a3\3\2\2\2\u02a4\u02a5\3\2\2\2\u02a5\u02a6\3\2\2\2\u02a6\u02a7"+
		"\7\22\2\2\u02a7\u02b5\3\2\2\2\u02a8\u02a9\7\21\2\2\u02a9\u02ab\5\u00ee"+
		"x\2\u02aa\u02ac\5\u0124\u0093\2\u02ab\u02aa\3\2\2\2\u02ab\u02ac\3\2\2"+
		"\2\u02ac\u02ad\3\2\2\2\u02ad\u02ae\7\23\2\2\u02ae\u02b0\5\u00eex\2\u02af"+
		"\u02b1\5\u0124\u0093\2\u02b0\u02af\3\2\2\2\u02b0\u02b1\3\2\2\2\u02b1\u02b2"+
		"\3\2\2\2\u02b2\u02b3\7\22\2\2\u02b3\u02b5\3\2\2\2\u02b4\u02a1\3\2\2\2"+
		"\u02b4\u02a8\3\2\2\2\u02b4\u02b5\3\2\2\2\u02b5\67\3\2\2\2\u02b6\u02b7"+
		"\7G\2\2\u02b7\u02b9\5\u0100\u0081\2\u02b8\u02b6\3\2\2\2\u02b8\u02b9\3"+
		"\2\2\2\u02b9\u02c7\3\2\2\2\u02ba\u02c8\5:\36\2\u02bb\u02c8\5<\37\2\u02bc"+
		"\u02c8\5> \2\u02bd\u02c8\5@!\2\u02be\u02c8\7\u00a5\2\2\u02bf\u02c0\7B"+
		"\2\2\u02c0\u02c1\7\21\2\2\u02c1\u02c2\5F$\2\u02c2\u02c3\7\22\2\2\u02c3"+
		"\u02c8\3\2\2\2\u02c4\u02c8\5B\"\2\u02c5\u02c6\7C\2\2\u02c6\u02c8\5\u0110"+
		"\u0089\2\u02c7\u02ba\3\2\2\2\u02c7\u02bb\3\2\2\2\u02c7\u02bc\3\2\2\2\u02c7"+
		"\u02bd\3\2\2\2\u02c7\u02be\3\2\2\2\u02c7\u02bf\3\2\2\2\u02c7\u02c4\3\2"+
		"\2\2\u02c7\u02c5\3\2\2\2\u02c89\3\2\2\2\u02c9\u02ca\7\u008a\2\2\u02ca"+
		"\u02cc\7v\2\2\u02cb\u02cd\t\4\2\2\u02cc\u02cb\3\2\2\2\u02cc\u02cd\3\2"+
		"\2\2\u02cd\u02cf\3\2\2\2\u02ce\u02d0\7:\2\2\u02cf\u02ce\3\2\2\2\u02cf"+
		"\u02d0\3\2\2\2\u02d0;\3\2\2\2\u02d1\u02d2\5\u00c0a\2\u02d2=\3\2\2\2\u02d3"+
		"\u02d4\7~\2\2\u02d4\u02d5\7\u0080\2\2\u02d5?\3\2\2\2\u02d6\u02d7\7\u0080"+
		"\2\2\u02d7A\3\2\2\2\u02d8\u02e4\7N\2\2\u02d9\u02e5\5D#\2\u02da\u02db\7"+
		"\21\2\2\u02db\u02dc\5F$\2\u02dc\u02dd\7\22\2\2\u02dd\u02e5\3\2\2\2\u02de"+
		"\u02df\7|\2\2\u02df\u02e0\7\21\2\2\u02e0\u02e1\5F$\2\u02e1\u02e2\7\22"+
		"\2\2\u02e2\u02e5\3\2\2\2\u02e3\u02e5\5\u0124\u0093\2\u02e4\u02d9\3\2\2"+
		"\2\u02e4\u02da\3\2\2\2\u02e4\u02de\3\2\2\2\u02e4\u02e3\3\2\2\2\u02e5\u02ec"+
		"\3\2\2\2\u02e6\u02e8\7\3\2\2\u02e7\u02e9\5\u0124\u0093\2\u02e8\u02e7\3"+
		"\2\2\2\u02e9\u02ea\3\2\2\2\u02ea\u02e8\3\2\2\2\u02ea\u02eb\3\2\2\2\u02eb"+
		"\u02ed\3\2\2\2\u02ec\u02e6\3\2\2\2\u02ec\u02ed\3\2\2\2\u02edC\3\2\2\2"+
		"\u02ee\u02f1\5\u00eex\2\u02ef\u02f1\5\u00f0y\2\u02f0\u02ee\3\2\2\2\u02f0"+
		"\u02ef\3\2\2\2\u02f1E\3\2\2\2\u02f2\u02f3\b$\1\2\u02f3\u0327\5\u00f0y"+
		"\2\u02f4\u0327\5\u00a2R\2\u02f5\u02f6\5\u0104\u0083\2\u02f6\u02f7\7\20"+
		"\2\2\u02f7\u02f9\3\2\2\2\u02f8\u02f5\3\2\2\2\u02f8\u02f9\3\2\2\2\u02f9"+
		"\u02fa\3\2\2\2\u02fa\u02fb\5\u0108\u0085\2\u02fb\u02fc\7\20\2\2\u02fc"+
		"\u02fe\3\2\2\2\u02fd\u02f8\3\2\2\2\u02fd\u02fe\3\2\2\2\u02fe\u02ff\3\2"+
		"\2\2\u02ff\u0327\5\u010e\u0088\2\u0300\u0301\5\u00f4{\2\u0301\u0302\5"+
		"F$\23\u0302\u0327\3\2\2\2\u0303\u0327\5\u00b2Z\2\u0304\u0327\5\u00be`"+
		"\2\u0305\u0306\5\u0102\u0082\2\u0306\u0313\7\21\2\2\u0307\u0309\7T\2\2"+
		"\u0308\u0307\3\2\2\2\u0308\u0309\3\2\2\2\u0309\u030a\3\2\2\2\u030a\u030f"+
		"\5F$\2\u030b\u030c\7\23\2\2\u030c\u030e\5F$\2\u030d\u030b\3\2\2\2\u030e"+
		"\u0311\3\2\2\2\u030f\u030d\3\2\2\2\u030f\u0310\3\2\2\2\u0310\u0314\3\2"+
		"\2\2\u0311\u030f\3\2\2\2\u0312\u0314\7\25\2\2\u0313\u0308\3\2\2\2\u0313"+
		"\u0312\3\2\2\2\u0313\u0314\3\2\2\2\u0314\u0315\3\2\2\2\u0315\u0316\7\22"+
		"\2\2\u0316\u0327\3\2\2\2\u0317\u0318\7\21\2\2\u0318\u0319\5F$\2\u0319"+
		"\u031a\7\22\2\2\u031a\u0327\3\2\2\2\u031b\u0327\5\u0080A\2\u031c\u031e"+
		"\7~\2\2\u031d\u031c\3\2\2\2\u031d\u031e\3\2\2\2\u031e\u031f\3\2\2\2\u031f"+
		"\u0321\7]\2\2\u0320\u031d\3\2\2\2\u0320\u0321\3\2\2\2\u0321\u0322\3\2"+
		"\2\2\u0322\u0323\7\21\2\2\u0323\u0324\5$\23\2\u0324\u0325\7\22\2\2\u0325"+
		"\u0327\3\2\2\2\u0326\u02f2\3\2\2\2\u0326\u02f4\3\2\2\2\u0326\u02fd\3\2"+
		"\2\2\u0326\u0300\3\2\2\2\u0326\u0303\3\2\2\2\u0326\u0304\3\2\2\2\u0326"+
		"\u0305\3\2\2\2\u0326\u0317\3\2\2\2\u0326\u031b\3\2\2\2\u0326\u0320\3\2"+
		"\2\2\u0327\u036b\3\2\2\2\u0328\u0329\f\20\2\2\u0329\u032a\7\31\2\2\u032a"+
		"\u036a\5F$\21\u032b\u032c\f\17\2\2\u032c\u032d\t\5\2\2\u032d\u036a\5F"+
		"$\20\u032e\u032f\f\16\2\2\u032f\u0330\t\6\2\2\u0330\u036a\5F$\17\u0331"+
		"\u0332\f\r\2\2\u0332\u0333\t\7\2\2\u0333\u036a\5F$\16\u0334\u0335\f\f"+
		"\2\2\u0335\u0336\t\b\2\2\u0336\u036a\5F$\r\u0337\u0338\f\13\2\2\u0338"+
		"\u0339\t\t\2\2\u0339\u036a\5F$\f\u033a\u0346\f\n\2\2\u033b\u0347\7\24"+
		"\2\2\u033c\u0347\7%\2\2\u033d\u0347\7&\2\2\u033e\u0347\7\'\2\2\u033f\u0347"+
		"\7s\2\2\u0340\u0341\7s\2\2\u0341\u0347\7~\2\2\u0342\u0347\7x\2\2\u0343"+
		"\u0347\7d\2\2\u0344\u0347\7z\2\2\u0345\u0347\7\u008f\2\2\u0346\u033b\3"+
		"\2\2\2\u0346\u033c\3\2\2\2\u0346\u033d\3\2\2\2\u0346\u033e\3\2\2\2\u0346"+
		"\u033f\3\2\2\2\u0346\u0340\3\2\2\2\u0346\u0342\3\2\2\2\u0346\u0343\3\2"+
		"\2\2\u0346\u0344\3\2\2\2\u0346\u0345\3\2\2\2\u0347\u0348\3\2\2\2\u0348"+
		"\u036a\5F$\13\u0349\u034a\f\t\2\2\u034a\u034b\7\66\2\2\u034b\u036a\5F"+
		"$\n\u034c\u034d\f\b\2\2\u034d\u034e\7\u0085\2\2\u034e\u036a\5F$\t\u034f"+
		"\u0351\f\4\2\2\u0350\u0352\7~\2\2\u0351\u0350\3\2\2\2\u0351\u0352\3\2"+
		"\2\2\u0352\u0353\3\2\2\2\u0353\u0367\7j\2\2\u0354\u035e\7\21\2\2\u0355"+
		"\u035f\5$\23\2\u0356\u035b\5F$\2\u0357\u0358\7\23\2\2\u0358\u035a\5F$"+
		"\2\u0359\u0357\3\2\2\2\u035a\u035d\3\2\2\2\u035b\u0359\3\2\2\2\u035b\u035c"+
		"\3\2\2\2\u035c\u035f\3\2\2\2\u035d\u035b\3\2\2\2\u035e\u0355\3\2\2\2\u035e"+
		"\u0356\3\2\2\2\u035e\u035f\3\2\2\2\u035f\u0360\3\2\2\2\u0360\u0368\7\22"+
		"\2\2\u0361\u0362\5\u0104\u0083\2\u0362\u0363\7\20\2\2\u0363\u0365\3\2"+
		"\2\2\u0364\u0361\3\2\2\2\u0364\u0365\3\2\2\2\u0365\u0366\3\2\2\2\u0366"+
		"\u0368\5\u0108\u0085\2\u0367\u0354\3\2\2\2\u0367\u0364\3\2\2\2\u0368\u036a"+
		"\3\2\2\2\u0369\u0328\3\2\2\2\u0369\u032b\3\2\2\2\u0369\u032e\3\2\2\2\u0369"+
		"\u0331\3\2\2\2\u0369\u0334\3\2\2\2\u0369\u0337\3\2\2\2\u0369\u033a\3\2"+
		"\2\2\u0369\u0349\3\2\2\2\u0369\u034c\3\2\2\2\u0369\u034f\3\2\2\2\u036a"+
		"\u036d\3\2\2\2\u036b\u0369\3\2\2\2\u036b\u036c\3\2\2\2\u036cG\3\2\2\2"+
		"\u036d\u036b\3\2\2\2\u036e\u036f\b%\1\2\u036f\u039a\5\u00f0y\2\u0370\u039a"+
		"\7\u00ba\2\2\u0371\u039a\5\u00a2R\2\u0372\u039a\5b\62\2\u0373\u0374\5"+
		"\u00f4{\2\u0374\u0375\5F$\2\u0375\u039a\3\2\2\2\u0376\u039a\5\u00b2Z\2"+
		"\u0377\u039a\5\u00be`\2\u0378\u0379\5\u0102\u0082\2\u0379\u0386\7\21\2"+
		"\2\u037a\u037c\7T\2\2\u037b\u037a\3\2\2\2\u037b\u037c\3\2\2\2\u037c\u037d"+
		"\3\2\2\2\u037d\u0382\5H%\2\u037e\u037f\7\23\2\2\u037f\u0381\5H%\2\u0380"+
		"\u037e\3\2\2\2\u0381\u0384\3\2\2\2\u0382\u0380\3\2\2\2\u0382\u0383\3\2"+
		"\2\2\u0383\u0387\3\2\2\2\u0384\u0382\3\2\2\2\u0385\u0387\7\25\2\2\u0386"+
		"\u037b\3\2\2\2\u0386\u0385\3\2\2\2\u0386\u0387\3\2\2\2\u0387\u0388\3\2"+
		"\2\2\u0388\u0389\7\22\2\2\u0389\u039a\3\2\2\2\u038a\u038b\7\21\2\2\u038b"+
		"\u038c\5H%\2\u038c\u038d\7\22\2\2\u038d\u039a\3\2\2\2\u038e\u039a\5\u0080"+
		"A\2\u038f\u0391\7~\2\2\u0390\u038f\3\2\2\2\u0390\u0391\3\2\2\2\u0391\u0392"+
		"\3\2\2\2\u0392\u0394\7]\2\2\u0393\u0390\3\2\2\2\u0393\u0394\3\2\2\2\u0394"+
		"\u0395\3\2\2\2\u0395\u0396\7\21\2\2\u0396\u0397\5$\23\2\u0397\u0398\7"+
		"\22\2\2\u0398\u039a\3\2\2\2\u0399\u036e\3\2\2\2\u0399\u0370\3\2\2\2\u0399"+
		"\u0371\3\2\2\2\u0399\u0372\3\2\2\2\u0399\u0373\3\2\2\2\u0399\u0376\3\2"+
		"\2\2\u0399\u0377\3\2\2\2\u0399\u0378\3\2\2\2\u0399\u038a\3\2\2\2\u0399"+
		"\u038e\3\2\2\2\u0399\u0393\3\2\2\2\u039a\u03df\3\2\2\2\u039b\u039c\f\20"+
		"\2\2\u039c\u039d\7\31\2\2\u039d\u03de\5H%\21\u039e\u039f\f\17\2\2\u039f"+
		"\u03a0\t\5\2\2\u03a0\u03de\5H%\20\u03a1\u03a2\f\16\2\2\u03a2\u03a3\t\6"+
		"\2\2\u03a3\u03de\5H%\17\u03a4\u03a5\f\r\2\2\u03a5\u03a6\t\7\2\2\u03a6"+
		"\u03de\5H%\16\u03a7\u03a8\f\f\2\2\u03a8\u03a9\t\b\2\2\u03a9\u03de\5H%"+
		"\r\u03aa\u03ab\f\13\2\2\u03ab\u03ac\t\t\2\2\u03ac\u03de\5H%\f\u03ad\u03ba"+
		"\f\n\2\2\u03ae\u03bb\7\24\2\2\u03af\u03bb\7%\2\2\u03b0\u03bb\7&\2\2\u03b1"+
		"\u03bb\7\'\2\2\u03b2\u03bb\7s\2\2\u03b3\u03b4\7s\2\2\u03b4\u03bb\7~\2"+
		"\2\u03b5\u03bb\7j\2\2\u03b6\u03bb\7x\2\2\u03b7\u03bb\7d\2\2\u03b8\u03bb"+
		"\7z\2\2\u03b9\u03bb\7\u008f\2\2\u03ba\u03ae\3\2\2\2\u03ba\u03af\3\2\2"+
		"\2\u03ba\u03b0\3\2\2\2\u03ba\u03b1\3\2\2\2\u03ba\u03b2\3\2\2\2\u03ba\u03b3"+
		"\3\2\2\2\u03ba\u03b5\3\2\2\2\u03ba\u03b6\3\2\2\2\u03ba\u03b7\3\2\2\2\u03ba"+
		"\u03b8\3\2\2\2\u03ba\u03b9\3\2\2\2\u03bb\u03bc\3\2\2\2\u03bc\u03de\5H"+
		"%\13\u03bd\u03be\f\t\2\2\u03be\u03bf\7\66\2\2\u03bf\u03de\5H%\n\u03c0"+
		"\u03c1\f\b\2\2\u03c1\u03c2\7\u0085\2\2\u03c2\u03de\5H%\t\u03c3\u03c5\f"+
		"\4\2\2\u03c4\u03c6\7~\2\2\u03c5\u03c4\3\2\2\2\u03c5\u03c6\3\2\2\2\u03c6"+
		"\u03c7\3\2\2\2\u03c7\u03db\7j\2\2\u03c8\u03d2\7\21\2\2\u03c9\u03d3\5$"+
		"\23\2\u03ca\u03cf\5H%\2\u03cb\u03cc\7\23\2\2\u03cc\u03ce\5H%\2\u03cd\u03cb"+
		"\3\2\2\2\u03ce\u03d1\3\2\2\2\u03cf\u03cd\3\2\2\2\u03cf\u03d0\3\2\2\2\u03d0"+
		"\u03d3\3\2\2\2\u03d1\u03cf\3\2\2\2\u03d2\u03c9\3\2\2\2\u03d2\u03ca\3\2"+
		"\2\2\u03d2\u03d3\3\2\2\2\u03d3\u03d4\3\2\2\2\u03d4\u03dc\7\22\2\2\u03d5"+
		"\u03d6\5\u0104\u0083\2\u03d6\u03d7\7\20\2\2\u03d7\u03d9\3\2\2\2\u03d8"+
		"\u03d5\3\2\2\2\u03d8\u03d9\3\2\2\2\u03d9\u03da\3\2\2\2\u03da\u03dc\5\u0108"+
		"\u0085\2\u03db\u03c8\3\2\2\2\u03db\u03d8\3\2\2\2\u03dc\u03de\3\2\2\2\u03dd"+
		"\u039b\3\2\2\2\u03dd\u039e\3\2\2\2\u03dd\u03a1\3\2\2\2\u03dd\u03a4\3\2"+
		"\2\2\u03dd\u03a7\3\2\2\2\u03dd\u03aa\3\2\2\2\u03dd\u03ad\3\2\2\2\u03dd"+
		"\u03bd\3\2\2\2\u03dd\u03c0\3\2\2\2\u03dd\u03c3\3\2\2\2\u03de\u03e1\3\2"+
		"\2\2\u03df\u03dd\3\2\2\2\u03df\u03e0\3\2\2\2\u03e0I\3\2\2\2\u03e1\u03df"+
		"\3\2\2\2\u03e2\u03e3\7\u00ba\2\2\u03e3K\3\2\2\2\u03e4\u03e9\5P)\2\u03e5"+
		"\u03e6\7\23\2\2\u03e6\u03e8\5P)\2\u03e7\u03e5\3\2\2\2\u03e8\u03eb\3\2"+
		"\2\2\u03e9\u03e7\3\2\2\2\u03e9\u03ea\3\2\2\2\u03ea\u03ed\3\2\2\2\u03eb"+
		"\u03e9\3\2\2\2\u03ec\u03e4\3\2\2\2\u03ec\u03ed\3\2\2\2\u03ed\u03f0\3\2"+
		"\2\2\u03ee\u03f0\5N(\2\u03ef\u03ec\3\2\2\2\u03ef\u03ee\3\2\2\2\u03ef\u03f0"+
		"\3\2\2\2\u03f0\u0403\3\2\2\2\u03f1\u03f6\5N(\2\u03f2\u03f3\7\23\2\2\u03f3"+
		"\u03f5\5N(\2\u03f4\u03f2\3\2\2\2\u03f5\u03f8\3\2\2\2\u03f6\u03f4\3\2\2"+
		"\2\u03f6\u03f7\3\2\2\2\u03f7\u03fd\3\2\2\2\u03f8\u03f6\3\2\2\2\u03f9\u03fa"+
		"\7\23\2\2\u03fa\u03fc\5P)\2\u03fb\u03f9\3\2\2\2\u03fc\u03ff\3\2\2\2\u03fd"+
		"\u03fb\3\2\2\2\u03fd\u03fe\3\2\2\2\u03fe\u0401\3\2\2\2\u03ff\u03fd\3\2"+
		"\2\2\u0400\u03f1\3\2\2\2\u0400\u0401\3\2\2\2\u0401\u0403\3\2\2\2\u0402"+
		"\u03ef\3\2\2\2\u0402\u0400\3\2\2\2\u0403M\3\2\2\2\u0404\u0405\7\u00b2"+
		"\2\2\u0405\u0406\7\u00ba\2\2\u0406O\3\2\2\2\u0407\u0409\7\u00b2\2\2\u0408"+
		"\u0407\3\2\2\2\u0408\u0409\3\2\2\2\u0409\u040a\3\2\2\2\u040a\u040b\7\u00ba"+
		"\2\2\u040b\u040f\7\24\2\2\u040c\u0410\7\u00ba\2\2\u040d\u0410\5\u00be"+
		"`\2\u040e\u0410\5\u009aN\2\u040f\u040c\3\2\2\2\u040f\u040d\3\2\2\2\u040f"+
		"\u040e\3\2\2\2\u0410\u0413\3\2\2\2\u0411\u0413\5&\24\2\u0412\u0408\3\2"+
		"\2\2\u0412\u0411\3\2\2\2\u0413Q\3\2\2\2\u0414\u0415\5J&\2\u0415\u0416"+
		"\7\21\2\2\u0416\u0417\5L\'\2\u0417\u0418\7\22\2\2\u0418\u0419\7\4\2\2"+
		"\u0419\u041a\5V,\2\u041a\u041b\7\5\2\2\u041bS\3\2\2\2\u041c\u041e\7\4"+
		"\2\2\u041d\u041f\5V,\2\u041e\u041d\3\2\2\2\u041e\u041f\3\2\2\2\u041f\u0420"+
		"\3\2\2\2\u0420\u0421\7\5\2\2\u0421U\3\2\2\2\u0422\u044e\5n8\2\u0423\u0424"+
		"\5\u0092J\2\u0424\u0425\7\17\2\2\u0425\u044e\3\2\2\2\u0426\u0427\5Z.\2"+
		"\u0427\u0428\7\17\2\2\u0428\u044e\3\2\2\2\u0429\u042a\5d\63\2\u042a\u042b"+
		"\7\17\2\2\u042b\u044e\3\2\2\2\u042c\u044e\5t;\2\u042d\u044e\5\u008aF\2"+
		"\u042e\u044e\5\u008cG\2\u042f\u044e\5\u0082B\2\u0430\u044e\5\u008eH\2"+
		"\u0431\u0432\5\u0098M\2\u0432\u0433\7\17\2\2\u0433\u044e\3\2\2\2\u0434"+
		"\u0435\5N(\2\u0435\u0436\7\17\2\2\u0436\u044e\3\2\2\2\u0437\u0438\5\u00b2"+
		"Z\2\u0438\u0439\7\17\2\2\u0439\u044e\3\2\2\2\u043a\u043b\5P)\2\u043b\u043c"+
		"\7\17\2\2\u043c\u044e\3\2\2\2\u043d\u044e\5\u00aeX\2\u043e\u044e\5\u00b0"+
		"Y\2\u043f\u044e\5T+\2\u0440\u0441\5F$\2\u0441\u0442\7\17\2\2\u0442\u044e"+
		"\3\2\2\2\u0443\u0444\5\u009eP\2\u0444\u0445\7\17\2\2\u0445\u044e\3\2\2"+
		"\2\u0446\u0447\5\u00a8U\2\u0447\u0448\7\17\2\2\u0448\u044e\3\2\2\2\u0449"+
		"\u044e\5\6\4\2\u044a\u044b\5&\24\2\u044b\u044c\7\17\2\2\u044c\u044e\3"+
		"\2\2\2\u044d\u0422\3\2\2\2\u044d\u0423\3\2\2\2\u044d\u0426\3\2\2\2\u044d"+
		"\u0429\3\2\2\2\u044d\u042c\3\2\2\2\u044d\u042d\3\2\2\2\u044d\u042e\3\2"+
		"\2\2\u044d\u042f\3\2\2\2\u044d\u0430\3\2\2\2\u044d\u0431\3\2\2\2\u044d"+
		"\u0434\3\2\2\2\u044d\u0437\3\2\2\2\u044d\u043a\3\2\2\2\u044d\u043d\3\2"+
		"\2\2\u044d\u043e\3\2\2\2\u044d\u043f\3\2\2\2\u044d\u0440\3\2\2\2\u044d"+
		"\u0443\3\2\2\2\u044d\u0446\3\2\2\2\u044d\u0449\3\2\2\2\u044d\u044a\3\2"+
		"\2\2\u044e\u0451\3\2\2\2\u044f\u044d\3\2\2\2\u044f\u0450\3\2\2\2\u0450"+
		"\u0453\3\2\2\2\u0451\u044f\3\2\2\2\u0452\u0454\5h\65\2\u0453\u0452\3\2"+
		"\2\2\u0453\u0454\3\2\2\2\u0454W\3\2\2\2\u0455\u0481\5n8\2\u0456\u0457"+
		"\5\u0092J\2\u0457\u0458\7\17\2\2\u0458\u0481\3\2\2\2\u0459\u045a\5Z.\2"+
		"\u045a\u045b\7\17\2\2\u045b\u0481\3\2\2\2\u045c\u045d\5d\63\2\u045d\u045e"+
		"\7\17\2\2\u045e\u0481\3\2\2\2\u045f\u0481\5t;\2\u0460\u0481\5\u008aF\2"+
		"\u0461\u0481\5\u008cG\2\u0462\u0481\5\u0082B\2\u0463\u0481\5\u008eH\2"+
		"\u0464\u0465\5\u0098M\2\u0465\u0466\7\17\2\2\u0466\u0481\3\2\2\2\u0467"+
		"\u0468\5N(\2\u0468\u0469\7\17\2\2\u0469\u0481\3\2\2\2\u046a\u046b\5\u00b2"+
		"Z\2\u046b\u046c\7\17\2\2\u046c\u0481\3\2\2\2\u046d\u046e\5P)\2\u046e\u046f"+
		"\7\17\2\2\u046f\u0481\3\2\2\2\u0470\u0481\5\u00aeX\2\u0471\u0481\5\u00b0"+
		"Y\2\u0472\u0473\5F$\2\u0473\u0474\7\17\2\2\u0474\u0481\3\2\2\2\u0475\u0476"+
		"\5\u009eP\2\u0476\u0477\7\17\2\2\u0477\u0481\3\2\2\2\u0478\u0479\5\u00a8"+
		"U\2\u0479\u047a\7\17\2\2\u047a\u0481\3\2\2\2\u047b\u0481\5h\65\2\u047c"+
		"\u0481\5\6\4\2\u047d\u047e\5&\24\2\u047e\u047f\7\17\2\2\u047f\u0481\3"+
		"\2\2\2\u0480\u0455\3\2\2\2\u0480\u0456\3\2\2\2\u0480\u0459\3\2\2\2\u0480"+
		"\u045c\3\2\2\2\u0480\u045f\3\2\2\2\u0480\u0460\3\2\2\2\u0480\u0461\3\2"+
		"\2\2\u0480\u0462\3\2\2\2\u0480\u0463\3\2\2\2\u0480\u0464\3\2\2\2\u0480"+
		"\u0467\3\2\2\2\u0480\u046a\3\2\2\2\u0480\u046d\3\2\2\2\u0480\u0470\3\2"+
		"\2\2\u0480\u0471\3\2\2\2\u0480\u0472\3\2\2\2\u0480\u0475\3\2\2\2\u0480"+
		"\u0478\3\2\2\2\u0480\u047b\3\2\2\2\u0480\u047c\3\2\2\2\u0480\u047d\3\2"+
		"\2\2\u0481Y\3\2\2\2\u0482\u0484\7\u00b2\2\2\u0483\u0482\3\2\2\2\u0483"+
		"\u0484\3\2\2\2\u0484\u0485\3\2\2\2\u0485\u0486\7\u00ba\2\2\u0486\u0487"+
		"\7\24\2\2\u0487\u0488\5\\/\2\u0488[\3\2\2\2\u0489\u0492\7\4\2\2\u048a"+
		"\u048f\5^\60\2\u048b\u048c\7\23\2\2\u048c\u048e\5^\60\2\u048d\u048b\3"+
		"\2\2\2\u048e\u0491\3\2\2\2\u048f\u048d\3\2\2\2\u048f\u0490\3\2\2\2\u0490"+
		"\u0493\3\2\2\2\u0491\u048f\3\2\2\2\u0492\u048a\3\2\2\2\u0492\u0493\3\2"+
		"\2\2\u0493\u0494\3\2\2\2\u0494\u0495\7\5\2\2\u0495]\3\2\2\2\u0496\u0497"+
		"\7\u00ba\2\2\u0497\u0498\7\6\2\2\u0498\u04a0\5\u009aN\2\u0499\u049a\7"+
		"\u00ba\2\2\u049a\u049b\7\6\2\2\u049b\u04a0\5`\61\2\u049c\u049d\7\u00ba"+
		"\2\2\u049d\u049e\7\6\2\2\u049e\u04a0\5\\/\2\u049f\u0496\3\2\2\2\u049f"+
		"\u0499\3\2\2\2\u049f\u049c\3\2\2\2\u04a0_\3\2\2\2\u04a1\u04aa\7\7\2\2"+
		"\u04a2\u04a7\5\u00f2z\2\u04a3\u04a4\7\23\2\2\u04a4\u04a6\5\u00f2z\2\u04a5"+
		"\u04a3\3\2\2\2\u04a6\u04a9\3\2\2\2\u04a7\u04a5\3\2\2\2\u04a7\u04a8\3\2"+
		"\2\2\u04a8\u04ab\3\2\2\2\u04a9\u04a7\3\2\2\2\u04aa\u04a2\3\2\2\2\u04aa"+
		"\u04ab\3\2\2\2\u04ab\u04ac\3\2\2\2\u04ac\u04ba\7\b\2\2\u04ad\u04b6\7\7"+
		"\2\2\u04ae\u04b3\5\\/\2\u04af\u04b0\7\23\2\2\u04b0\u04b2\5\\/\2\u04b1"+
		"\u04af\3\2\2\2\u04b2\u04b5\3\2\2\2\u04b3\u04b1\3\2\2\2\u04b3\u04b4\3\2"+
		"\2\2\u04b4\u04b7\3\2\2\2\u04b5\u04b3\3\2\2\2\u04b6\u04ae\3\2\2\2\u04b6"+
		"\u04b7\3\2\2\2\u04b7\u04b8\3\2\2\2\u04b8\u04ba\7\b\2\2\u04b9\u04a1\3\2"+
		"\2\2\u04b9\u04ad\3\2\2\2\u04baa\3\2\2\2\u04bb\u04bc\5f\64\2\u04bc\u04bd"+
		"\7\20\2\2\u04bd\u04c2\5f\64\2\u04be\u04bf\7\20\2\2\u04bf\u04c1\5f\64\2"+
		"\u04c0\u04be\3\2\2\2\u04c1\u04c4\3\2\2\2\u04c2\u04c0\3\2\2\2\u04c2\u04c3"+
		"\3\2\2\2\u04c3c\3\2\2\2\u04c4\u04c2\3\2\2\2\u04c5\u04c6\5b\62\2\u04c6"+
		"\u04c7\7\24\2\2\u04c7\u04c8\5\u009aN\2\u04c8e\3\2\2\2\u04c9\u04ca\7\u00ba"+
		"\2\2\u04cag\3\2\2\2\u04cb\u04cf\7\u00b1\2\2\u04cc\u04d0\5\u0092J\2\u04cd"+
		"\u04d0\5\u00be`\2\u04ce\u04d0\5H%\2\u04cf\u04cc\3\2\2\2\u04cf\u04cd\3"+
		"\2\2\2\u04cf\u04ce\3\2\2\2\u04cf\u04d0\3\2\2\2\u04d0\u04d1\3\2\2\2\u04d1"+
		"\u04d2\7\17\2\2\u04d2i\3\2\2\2\u04d3\u04d4\b\66\1\2\u04d4\u04e4\7\u00bc"+
		"\2\2\u04d5\u04e4\5\u00f0y\2\u04d6\u04e4\5b\62\2\u04d7\u04e4\7\u00ba\2"+
		"\2\u04d8\u04e4\5\u00a2R\2\u04d9\u04e4\7\u00c4\2\2\u04da\u04db\7\21\2\2"+
		"\u04db\u04dc\5j\66\2\u04dc\u04dd\7\22\2\2\u04dd\u04e4\3\2\2\2\u04de\u04df"+
		"\5\u00f4{\2\u04df\u04e0\7\u00bc\2\2\u04e0\u04e4\3\2\2\2\u04e1\u04e4\5"+
		"\u0080A\2\u04e2\u04e4\5\u00b2Z\2\u04e3\u04d3\3\2\2\2\u04e3\u04d5\3\2\2"+
		"\2\u04e3\u04d6\3\2\2\2\u04e3\u04d7\3\2\2\2\u04e3\u04d8\3\2\2\2\u04e3\u04d9"+
		"\3\2\2\2\u04e3\u04da\3\2\2\2\u04e3\u04de\3\2\2\2\u04e3\u04e1\3\2\2\2\u04e3"+
		"\u04e2\3\2\2\2\u04e4\u04ed\3\2\2\2\u04e5\u04e6\f\6\2\2\u04e6\u04e7\t\6"+
		"\2\2\u04e7\u04ec\5j\66\7\u04e8\u04e9\f\5\2\2\u04e9\u04ea\t\7\2\2\u04ea"+
		"\u04ec\5j\66\6\u04eb\u04e5\3\2\2\2\u04eb\u04e8\3\2\2\2\u04ec\u04ef\3\2"+
		"\2\2\u04ed\u04eb\3\2\2\2\u04ed\u04ee\3\2\2\2\u04eek\3\2\2\2\u04ef\u04ed"+
		"\3\2\2\2\u04f0\u04f1\b\67\1\2\u04f1\u04f2\7\21\2\2\u04f2\u04f3\5l\67\2"+
		"\u04f3\u04f4\7\22\2\2\u04f4\u04fc\3\2\2\2\u04f5\u04fc\7\u00ba\2\2\u04f6"+
		"\u04f7\5\u009aN\2\u04f7\u04f8\t\n\2\2\u04f8\u04f9\5\u009aN\2\u04f9\u04fc"+
		"\3\2\2\2\u04fa\u04fc\5\u00be`\2\u04fb\u04f0\3\2\2\2\u04fb\u04f5\3\2\2"+
		"\2\u04fb\u04f6\3\2\2\2\u04fb\u04fa\3\2\2\2\u04fc\u0505\3\2\2\2\u04fd\u04fe"+
		"\f\5\2\2\u04fe\u04ff\t\13\2\2\u04ff\u0504\5l\67\6\u0500\u0501\f\4\2\2"+
		"\u0501\u0502\t\f\2\2\u0502\u0504\5l\67\5\u0503\u04fd\3\2\2\2\u0503\u0500"+
		"\3\2\2\2\u0504\u0507\3\2\2\2\u0505\u0503\3\2\2\2\u0505\u0506\3\2\2\2\u0506"+
		"m\3\2\2\2\u0507\u0505\3\2\2\2\u0508\u0509\7g\2\2\u0509\u050a\7\21\2\2"+
		"\u050a\u050b\5l\67\2\u050b\u0511\7\22\2\2\u050c\u050d\7\4\2\2\u050d\u050e"+
		"\5V,\2\u050e\u050f\7\5\2\2\u050f\u0512\3\2\2\2\u0510\u0512\5X-\2\u0511"+
		"\u050c\3\2\2\2\u0511\u0510\3\2\2\2\u0512\u0516\3\2\2\2\u0513\u0515\5p"+
		"9\2\u0514\u0513\3\2\2\2\u0515\u0518\3\2\2\2\u0516\u0514\3\2\2\2\u0516"+
		"\u0517\3\2\2\2\u0517\u051a\3\2\2\2\u0518\u0516\3\2\2\2\u0519\u051b\5r"+
		":\2\u051a\u0519\3\2\2\2\u051a\u051b\3\2\2\2\u051bo\3\2\2\2\u051c\u051d"+
		"\7W\2\2\u051d\u051e\7g\2\2\u051e\u051f\7\21\2\2\u051f\u0520\5l\67\2\u0520"+
		"\u0526\7\22\2\2\u0521\u0522\7\4\2\2\u0522\u0523\5V,\2\u0523\u0524\7\5"+
		"\2\2\u0524\u0527\3\2\2\2\u0525\u0527\5X-\2\u0526\u0521\3\2\2\2\u0526\u0525"+
		"\3\2\2\2\u0527q\3\2\2\2\u0528\u052e\7W\2\2\u0529\u052a\7\4\2\2\u052a\u052b"+
		"\5V,\2\u052b\u052c\7\5\2\2\u052c\u052f\3\2\2\2\u052d\u052f\5X-\2\u052e"+
		"\u0529\3\2\2\2\u052e\u052d\3\2\2\2\u052fs\3\2\2\2\u0530\u0531\7\u00b0"+
		"\2\2\u0531\u0536\7\21\2\2\u0532\u0537\5\u009aN\2\u0533\u0537\5\u00b2Z"+
		"\2\u0534\u0537\5\u0092J\2\u0535\u0537\5P)\2\u0536\u0532\3\2\2\2\u0536"+
		"\u0533\3\2\2\2\u0536\u0534\3\2\2\2\u0536\u0535\3\2\2\2\u0537\u0538\3\2"+
		"\2\2\u0538\u0539\7\22\2\2\u0539\u053a\7\4\2\2\u053a\u053b\5z>\2\u053b"+
		"\u053c\7\5\2\2\u053cu\3\2\2\2\u053d\u0540\7@\2\2\u053e\u0541\5\u00f0y"+
		"\2\u053f\u0541\5\u0124\u0093\2\u0540\u053e\3\2\2\2\u0540\u053f\3\2\2\2"+
		"\u0541\u0542\3\2\2\2\u0542\u0543\7\6\2\2\u0543\u0546\5V,\2\u0544\u0545"+
		"\7\u00b8\2\2\u0545\u0547\7\17\2\2\u0546\u0544\3\2\2\2\u0546\u0547\3\2"+
		"\2\2\u0547w\3\2\2\2\u0548\u0549\7N\2\2\u0549\u054a\7\6\2\2\u054a\u054d"+
		"\5V,\2\u054b\u054c\7\u00b8\2\2\u054c\u054e\7\17\2\2\u054d\u054b\3\2\2"+
		"\2\u054d\u054e\3\2\2\2\u054ey\3\2\2\2\u054f\u0551\5v<\2\u0550\u054f\3"+
		"\2\2\2\u0551\u0554\3\2\2\2\u0552\u0550\3\2\2\2\u0552\u0553\3\2\2\2\u0553"+
		"\u0556\3\2\2\2\u0554\u0552\3\2\2\2\u0555\u0557\5x=\2\u0556\u0555\3\2\2"+
		"\2\u0556\u0557\3\2\2\2\u0557{\3\2\2\2\u0558\u0559\7\u00ba\2\2\u0559\u056a"+
		"\7\24\2\2\u055a\u056b\7\u00c4\2\2\u055b\u056b\5\u00f0y\2\u055c\u056b\5"+
		"\u0080A\2\u055d\u056b\5~@\2\u055e\u055f\7\21\2\2\u055f\u0560\5~@\2\u0560"+
		"\u0561\7\22\2\2\u0561\u056b\3\2\2\2\u0562\u0563\7\21\2\2\u0563\u0564\5"+
		"\u0080A\2\u0564\u0565\7\22\2\2\u0565\u056b\3\2\2\2\u0566\u0567\7\21\2"+
		"\2\u0567\u0568\5\u00f0y\2\u0568\u0569\7\22\2\2\u0569\u056b\3\2\2\2\u056a"+
		"\u055a\3\2\2\2\u056a\u055b\3\2\2\2\u056a\u055c\3\2\2\2\u056a\u055d\3\2"+
		"\2\2\u056a\u055e\3\2\2\2\u056a\u0562\3\2\2\2\u056a\u0566\3\2\2\2\u056b"+
		"\u05bd\3\2\2\2\u056c\u056d\7\u00ba\2\2\u056d\u057e\7(\2\2\u056e\u057f"+
		"\7\u00c4\2\2\u056f\u057f\5\u00f0y\2\u0570\u057f\5\u0080A\2\u0571\u057f"+
		"\5~@\2\u0572\u0573\7\21\2\2\u0573\u0574\5~@\2\u0574\u0575\7\22\2\2\u0575"+
		"\u057f\3\2\2\2\u0576\u0577\7\21\2\2\u0577\u0578\5\u0080A\2\u0578\u0579"+
		"\7\22\2\2\u0579\u057f\3\2\2\2\u057a\u057b\7\21\2\2\u057b\u057c\5\u00f0"+
		"y\2\u057c\u057d\7\22\2\2\u057d\u057f\3\2\2\2\u057e\u056e\3\2\2\2\u057e"+
		"\u056f\3\2\2\2\u057e\u0570\3\2\2\2\u057e\u0571\3\2\2\2\u057e\u0572\3\2"+
		"\2\2\u057e\u0576\3\2\2\2\u057e\u057a\3\2\2\2\u057f\u05bd\3\2\2\2\u0580"+
		"\u0581\7\u00ba\2\2\u0581\u0592\7)\2\2\u0582\u0593\7\u00c4\2\2\u0583\u0593"+
		"\5\u00f0y\2\u0584\u0593\5\u0080A\2\u0585\u0593\5~@\2\u0586\u0587\7\21"+
		"\2\2\u0587\u0588\5~@\2\u0588\u0589\7\22\2\2\u0589\u0593\3\2\2\2\u058a"+
		"\u058b\7\21\2\2\u058b\u058c\5\u0080A\2\u058c\u058d\7\22\2\2\u058d\u0593"+
		"\3\2\2\2\u058e\u058f\7\21\2\2\u058f\u0590\5\u00f0y\2\u0590\u0591\7\22"+
		"\2\2\u0591\u0593\3\2\2\2\u0592\u0582\3\2\2\2\u0592\u0583\3\2\2\2\u0592"+
		"\u0584\3\2\2\2\u0592\u0585\3\2\2\2\u0592\u0586\3\2\2\2\u0592\u058a\3\2"+
		"\2\2\u0592\u058e\3\2\2\2\u0593\u05bd\3\2\2\2\u0594\u0595\7\u00ba\2\2\u0595"+
		"\u05a6\7*\2\2\u0596\u05a7\7\u00c4\2\2\u0597\u05a7\5\u00f0y\2\u0598\u05a7"+
		"\5\u0080A\2\u0599\u05a7\5~@\2\u059a\u059b\7\21\2\2\u059b\u059c\5~@\2\u059c"+
		"\u059d\7\22\2\2\u059d\u05a7\3\2\2\2\u059e\u059f\7\21\2\2\u059f\u05a0\5"+
		"\u0080A\2\u05a0\u05a1\7\22\2\2\u05a1\u05a7\3\2\2\2\u05a2\u05a3\7\21\2"+
		"\2\u05a3\u05a4\5\u00f0y\2\u05a4\u05a5\7\22\2\2\u05a5\u05a7\3\2\2\2\u05a6"+
		"\u0596\3\2\2\2\u05a6\u0597\3\2\2\2\u05a6\u0598\3\2\2\2\u05a6\u0599\3\2"+
		"\2\2\u05a6\u059a\3\2\2\2\u05a6\u059e\3\2\2\2\u05a6\u05a2\3\2\2\2\u05a7"+
		"\u05bd\3\2\2\2\u05a8\u05a9\7\u00ba\2\2\u05a9\u05ba\7+\2\2\u05aa\u05bb"+
		"\7\u00c4\2\2\u05ab\u05bb\5\u00f0y\2\u05ac\u05bb\5\u0080A\2\u05ad\u05bb"+
		"\5~@\2\u05ae\u05af\7\21\2\2\u05af\u05b0\5~@\2\u05b0\u05b1\7\22\2\2\u05b1"+
		"\u05bb\3\2\2\2\u05b2\u05b3\7\21\2\2\u05b3\u05b4\5\u0080A\2\u05b4\u05b5"+
		"\7\22\2\2\u05b5\u05bb\3\2\2\2\u05b6\u05b7\7\21\2\2\u05b7\u05b8\5\u00f0"+
		"y\2\u05b8\u05b9\7\22\2\2\u05b9\u05bb\3\2\2\2\u05ba\u05aa\3\2\2\2\u05ba"+
		"\u05ab\3\2\2\2\u05ba\u05ac\3\2\2\2\u05ba\u05ad\3\2\2\2\u05ba\u05ae\3\2"+
		"\2\2\u05ba\u05b2\3\2\2\2\u05ba\u05b6\3\2\2\2\u05bb\u05bd\3\2\2\2\u05bc"+
		"\u0558\3\2\2\2\u05bc\u056c\3\2\2\2\u05bc\u0580\3\2\2\2\u05bc\u0594\3\2"+
		"\2\2\u05bc\u05a8\3\2\2\2\u05bd}\3\2\2\2\u05be\u05c9\5\u00f0y\2\u05bf\u05c9"+
		"\5\u0080A\2\u05c0\u05c1\7\21\2\2\u05c1\u05c2\5\u00f0y\2\u05c2\u05c3\7"+
		"\22\2\2\u05c3\u05c9\3\2\2\2\u05c4\u05c5\7\21\2\2\u05c5\u05c6\5\u0080A"+
		"\2\u05c6\u05c7\7\22\2\2\u05c7\u05c9\3\2\2\2\u05c8\u05be\3\2\2\2\u05c8"+
		"\u05bf\3\2\2\2\u05c8\u05c0\3\2\2\2\u05c8\u05c4\3\2\2\2\u05c9\u05ca\3\2"+
		"\2\2\u05ca\u05cb\t\r\2\2\u05cb\u05cd\3\2\2\2\u05cc\u05c8\3\2\2\2\u05cd"+
		"\u05d0\3\2\2\2\u05ce\u05cc\3\2\2\2\u05ce\u05cf\3\2\2\2\u05cf\u05fa\3\2"+
		"\2\2\u05d0\u05ce\3\2\2\2\u05d1\u05e4\7\u00ba\2\2\u05d2\u05e4\5\u00f0y"+
		"\2\u05d3\u05e4\5\u0080A\2\u05d4\u05d5\7\21\2\2\u05d5\u05d6\7\u00ba\2\2"+
		"\u05d6\u05e4\7\22\2\2\u05d7\u05d8\7\21\2\2\u05d8\u05d9\5\u00f0y\2\u05d9"+
		"\u05da\7\22\2\2\u05da\u05e4\3\2\2\2\u05db\u05dc\7\21\2\2\u05dc\u05dd\5"+
		"\u0080A\2\u05dd\u05de\7\22\2\2\u05de\u05e4\3\2\2\2\u05df\u05e0\7\21\2"+
		"\2\u05e0\u05e1\5~@\2\u05e1\u05e2\7\22\2\2\u05e2\u05e4\3\2\2\2\u05e3\u05d1"+
		"\3\2\2\2\u05e3\u05d2\3\2\2\2\u05e3\u05d3\3\2\2\2\u05e3\u05d4\3\2\2\2\u05e3"+
		"\u05d7\3\2\2\2\u05e3\u05db\3\2\2\2\u05e3\u05df\3\2\2\2\u05e4\u05e5\3\2"+
		"\2\2\u05e5\u05f8\t\r\2\2\u05e6\u05f9\7\u00ba\2\2\u05e7\u05f9\5\u00f0y"+
		"\2\u05e8\u05f9\5\u0080A\2\u05e9\u05ea\7\21\2\2\u05ea\u05eb\7\u00ba\2\2"+
		"\u05eb\u05f9\7\22\2\2\u05ec\u05ed\7\21\2\2\u05ed\u05ee\5\u00f0y\2\u05ee"+
		"\u05ef\7\22\2\2\u05ef\u05f9\3\2\2\2\u05f0\u05f1\7\21\2\2\u05f1\u05f2\5"+
		"\u0080A\2\u05f2\u05f3\7\22\2\2\u05f3\u05f9\3\2\2\2\u05f4\u05f5\7\21\2"+
		"\2\u05f5\u05f6\5~@\2\u05f6\u05f7\7\22\2\2\u05f7\u05f9\3\2\2\2\u05f8\u05e6"+
		"\3\2\2\2\u05f8\u05e7\3\2\2\2\u05f8\u05e8\3\2\2\2\u05f8\u05e9\3\2\2\2\u05f8"+
		"\u05ec\3\2\2\2\u05f8\u05f0\3\2\2\2\u05f8\u05f4\3\2\2\2\u05f9\u05fb\3\2"+
		"\2\2\u05fa\u05e3\3\2\2\2\u05fb\u05fc\3\2\2\2\u05fc\u05fa\3\2\2\2\u05fc"+
		"\u05fd\3\2\2\2\u05fd\177\3\2\2\2\u05fe\u0602\7\u00ba\2\2\u05ff\u0602\5"+
		"\u00a2R\2\u0600\u0602\5b\62\2\u0601\u05fe\3\2\2\2\u0601\u05ff\3\2\2\2"+
		"\u0601\u0600\3\2\2\2\u0602\u0603\3\2\2\2\u0603\u0617\7\f\2\2\u0604\u0608"+
		"\7\u00ba\2\2\u0605\u0608\5\u00a2R\2\u0606\u0608\5b\62\2\u0607\u0604\3"+
		"\2\2\2\u0607\u0605\3\2\2\2\u0607\u0606\3\2\2\2\u0608\u0609\3\2\2\2\u0609"+
		"\u0617\7\13\2\2\u060a\u060e\7\16\2\2\u060b\u060f\7\u00ba\2\2\u060c\u060f"+
		"\5\u00a2R\2\u060d\u060f\5b\62\2\u060e\u060b\3\2\2\2\u060e\u060c\3\2\2"+
		"\2\u060e\u060d\3\2\2\2\u060f\u0617\3\2\2\2\u0610\u0614\7\r\2\2\u0611\u0615"+
		"\7\u00ba\2\2\u0612\u0615\5\u00a2R\2\u0613\u0615\5b\62\2\u0614\u0611\3"+
		"\2\2\2\u0614\u0612\3\2\2\2\u0614\u0613\3\2\2\2\u0615\u0617\3\2\2\2\u0616"+
		"\u0601\3\2\2\2\u0616\u0607\3\2\2\2\u0616\u060a\3\2\2\2\u0616\u0610\3\2"+
		"\2\2\u0617\u0081\3\2\2\2\u0618\u0619\7`\2\2\u0619\u061a\7\21\2\2\u061a"+
		"\u061b\5\u0084C\2\u061b\u061c\7\17\2\2\u061c\u061d\5\u0086D\2\u061d\u061e"+
		"\7\17\2\2\u061e\u061f\5\u0088E\2\u061f\u0620\3\2\2\2\u0620\u0626\7\22"+
		"\2\2\u0621\u0622\7\4\2\2\u0622\u0623\5V,\2\u0623\u0624\7\5\2\2\u0624\u0627"+
		"\3\2\2\2\u0625\u0627\5X-\2\u0626\u0621\3\2\2\2\u0626\u0625\3\2\2\2\u0627"+
		"\u0083\3\2\2\2\u0628\u062a\7\u00b2\2\2\u0629\u0628\3\2\2\2\u0629\u062a"+
		"\3\2\2\2\u062a\u062b\3\2\2\2\u062b\u062c\7\u00ba\2\2\u062c\u062d\7\24"+
		"\2\2\u062d\u062f\5\u009aN\2\u062e\u0629\3\2\2\2\u062e\u062f\3\2\2\2\u062f"+
		"\u0085\3\2\2\2\u0630\u0631\5\u009aN\2\u0631\u0632\t\n\2\2\u0632\u0633"+
		"\5\u009aN\2\u0633\u0635\3\2\2\2\u0634\u0630\3\2\2\2\u0634\u0635\3\2\2"+
		"\2\u0635\u0087\3\2\2\2\u0636\u0639\5\u0080A\2\u0637\u0639\5|?\2\u0638"+
		"\u0636\3\2\2\2\u0638\u0637\3\2\2\2\u0638\u0639\3\2\2\2\u0639\u0089\3\2"+
		"\2\2\u063a\u063f\7\u00ba\2\2\u063b\u063f\7\u00bc\2\2\u063c\u063f\5\u00a2"+
		"R\2\u063d\u063f\5b\62\2\u063e\u063a\3\2\2\2\u063e\u063b\3\2\2\2\u063e"+
		"\u063c\3\2\2\2\u063e\u063d\3\2\2\2\u063f\u0640\3\2\2\2\u0640\u0641\7\f"+
		"\2\2\u0641\u0654\7\17\2\2\u0642\u0647\7\u00ba\2\2\u0643\u0647\7\u00bc"+
		"\2\2\u0644\u0647\5\u00a2R\2\u0645\u0647\5b\62\2\u0646\u0642\3\2\2\2\u0646"+
		"\u0643\3\2\2\2\u0646\u0644\3\2\2\2\u0646\u0645\3\2\2\2\u0647\u0648\3\2"+
		"\2\2\u0648\u0654\7\f\2\2\u0649\u064e\7\16\2\2\u064a\u064f\7\u00ba\2\2"+
		"\u064b\u064f\7\u00bc\2\2\u064c\u064f\5\u00a2R\2\u064d\u064f\5b\62\2\u064e"+
		"\u064a\3\2\2\2\u064e\u064b\3\2\2\2\u064e\u064c\3\2\2\2\u064e\u064d\3\2"+
		"\2\2\u064f\u0651\3\2\2\2\u0650\u0652\7\17\2\2\u0651\u0650\3\2\2\2\u0651"+
		"\u0652\3\2\2\2\u0652\u0654\3\2\2\2\u0653\u063e\3\2\2\2\u0653\u0646\3\2"+
		"\2\2\u0653\u0649\3\2\2\2\u0654\u008b\3\2\2\2\u0655\u065a\7\u00ba\2\2\u0656"+
		"\u065a\7\u00bc\2\2\u0657\u065a\5\u00a2R\2\u0658\u065a\5b\62\2\u0659\u0655"+
		"\3\2\2\2\u0659\u0656\3\2\2\2\u0659\u0657\3\2\2\2\u0659\u0658\3\2\2\2\u065a"+
		"\u065b\3\2\2\2\u065b\u065d\7\13\2\2\u065c\u065e\7\17\2\2\u065d\u065c\3"+
		"\2\2\2\u065d\u065e\3\2\2\2\u065e\u066a\3\2\2\2\u065f\u0664\7\r\2\2\u0660"+
		"\u0665\7\u00ba\2\2\u0661\u0665\7\u00bc\2\2\u0662\u0665\5\u00a2R\2\u0663"+
		"\u0665\5b\62\2\u0664\u0660\3\2\2\2\u0664\u0661\3\2\2\2\u0664\u0662\3\2"+
		"\2\2\u0664\u0663\3\2\2\2\u0665\u0667\3\2\2\2\u0666\u0668\7\17\2\2\u0667"+
		"\u0666\3\2\2\2\u0667\u0668\3\2\2\2\u0668\u066a\3\2\2\2\u0669\u0659\3\2"+
		"\2\2\u0669\u065f\3\2\2\2\u066a\u008d\3\2\2\2\u066b\u066c\7`\2\2\u066c"+
		"\u066d\7\21\2\2\u066d\u066e\5\u0090I\2\u066e\u0674\7\22\2\2\u066f\u0670"+
		"\7\4\2\2\u0670\u0671\5V,\2\u0671\u0672\7\5\2\2\u0672\u0675\3\2\2\2\u0673"+
		"\u0675\5X-\2\u0674\u066f\3\2\2\2\u0674\u0673\3\2\2\2\u0675\u008f\3\2\2"+
		"\2\u0676\u0678\7\u00b2\2\2\u0677\u0676\3\2\2\2\u0677\u0678\3\2\2\2\u0678"+
		"\u0679\3\2\2\2\u0679\u067a\7\u00ba\2\2\u067a\u067b\7\6\2\2\u067b\u067c"+
		"\7\u00ba\2\2\u067c\u0091\3\2\2\2\u067d\u067e\5N(\2\u067e\u067f\7\24\2"+
		"\2\u067f\u0681\3\2\2\2\u0680\u067d\3\2\2\2\u0680\u0681\3\2\2\2\u0681\u0682"+
		"\3\2\2\2\u0682\u0683\5l\67\2\u0683\u0684\7\t\2\2\u0684\u0685\5\u0094K"+
		"\2\u0685\u0686\7\6\2\2\u0686\u0687\5\u0096L\2\u0687\u0093\3\2\2\2\u0688"+
		"\u068c\5\u009aN\2\u0689\u068c\5\u0098M\2\u068a\u068c\5\u0092J\2\u068b"+
		"\u0688\3\2\2\2\u068b\u0689\3\2\2\2\u068b\u068a\3\2\2\2\u068c\u0692\3\2"+
		"\2\2\u068d\u068e\7\21\2\2\u068e\u068f\5\u0092J\2\u068f\u0690\7\22\2\2"+
		"\u0690\u0692\3\2\2\2\u0691\u068b\3\2\2\2\u0691\u068d\3\2\2\2\u0692\u0095"+
		"\3\2\2\2\u0693\u0697\5\u009aN\2\u0694\u0697\5\u0098M\2\u0695\u0697\5\u0092"+
		"J\2\u0696\u0693\3\2\2\2\u0696\u0694\3\2\2\2\u0696\u0695\3\2\2\2\u0697"+
		"\u069d\3\2\2\2\u0698\u0699\7\21\2\2\u0699\u069a\5\u0092J\2\u069a\u069b"+
		"\7\22\2\2\u069b\u069d\3\2\2\2\u069c\u0696\3\2\2\2\u069c\u0698\3\2\2\2"+
		"\u069d\u0097\3\2\2\2\u069e\u069f\7\u00b3\2\2\u069f\u06a2\7\21\2\2\u06a0"+
		"\u06a3\5b\62\2\u06a1\u06a3\5H%\2\u06a2\u06a0\3\2\2\2\u06a2\u06a1\3\2\2"+
		"\2\u06a3\u06a4\3\2\2\2\u06a4\u06a5\7\22\2\2\u06a5\u0099\3\2\2\2\u06a6"+
		"\u06a7\bN\1\2\u06a7\u06b5\7\u00ba\2\2\u06a8\u06b5\5\u00a2R\2\u06a9\u06b5"+
		"\5b\62\2\u06aa\u06b5\5\u00f0y\2\u06ab\u06b5\5\u00b2Z\2\u06ac\u06ad\7\21"+
		"\2\2\u06ad\u06ae\5\u009aN\2\u06ae\u06af\7\22\2\2\u06af\u06b5\3\2\2\2\u06b0"+
		"\u06b1\5\u00f4{\2\u06b1\u06b2\5\u009aN\6\u06b2\u06b5\3\2\2\2\u06b3\u06b5"+
		"\5\u0080A\2\u06b4\u06a6\3\2\2\2\u06b4\u06a8\3\2\2\2\u06b4\u06a9\3\2\2"+
		"\2\u06b4\u06aa\3\2\2\2\u06b4\u06ab\3\2\2\2\u06b4\u06ac\3\2\2\2\u06b4\u06b0"+
		"\3\2\2\2\u06b4\u06b3\3\2\2\2\u06b5\u06be\3\2\2\2\u06b6\u06b7\f\5\2\2\u06b7"+
		"\u06b8\t\6\2\2\u06b8\u06bd\5\u009aN\6\u06b9\u06ba\f\4\2\2\u06ba\u06bb"+
		"\t\7\2\2\u06bb\u06bd\5\u009aN\5\u06bc\u06b6\3\2\2\2\u06bc\u06b9\3\2\2"+
		"\2\u06bd\u06c0\3\2\2\2\u06be\u06bc\3\2\2\2\u06be\u06bf\3\2\2\2\u06bf\u009b"+
		"\3\2\2\2\u06c0\u06be\3\2\2\2\u06c1\u06c2\7\u00b2\2\2\u06c2\u06c9\7\u00ba"+
		"\2\2\u06c3\u06c7\7\24\2\2\u06c4\u06c8\5\u009aN\2\u06c5\u06c8\5\b\5\2\u06c6"+
		"\u06c8\5\u00be`\2\u06c7\u06c4\3\2\2\2\u06c7\u06c5\3\2\2\2\u06c7\u06c6"+
		"\3\2\2\2\u06c8\u06ca\3\2\2\2\u06c9\u06c3\3\2\2\2\u06c9\u06ca\3\2\2\2\u06ca"+
		"\u009d\3\2\2\2\u06cb\u06cc\7\u00b2\2\2\u06cc\u06d2\7\u00ba\2\2\u06cd\u06ce"+
		"\7\7\2\2\u06ce\u06cf\5\u009aN\2\u06cf\u06d0\7\b\2\2\u06d0\u06d3\3\2\2"+
		"\2\u06d1\u06d3\7\n\2\2\u06d2\u06cd\3\2\2\2\u06d2\u06d1\3\2\2\2\u06d3\u06e2"+
		"\3\2\2\2\u06d4\u06d5\7\u00b2\2\2\u06d5\u06df\7\u00ba\2\2\u06d6\u06d7\7"+
		"\7\2\2\u06d7\u06d8\5\u009aN\2\u06d8\u06d9\7\b\2\2\u06d9\u06da\7\7\2\2"+
		"\u06da\u06db\5\u009aN\2\u06db\u06dc\7\b\2\2\u06dc\u06e0\3\2\2\2\u06dd"+
		"\u06de\7\n\2\2\u06de\u06e0\7\n\2\2\u06df\u06d6\3\2\2\2\u06df\u06dd\3\2"+
		"\2\2\u06e0\u06e2\3\2\2\2\u06e1\u06cb\3\2\2\2\u06e1\u06d4\3\2\2\2\u06e2"+
		"\u009f\3\2\2\2\u06e3\u06e8\7\7\2\2\u06e4\u06e5\7\u00ba\2\2\u06e5\u06e7"+
		"\7\23\2\2\u06e6\u06e4\3\2\2\2\u06e7\u06ea\3\2\2\2\u06e8\u06e6\3\2\2\2"+
		"\u06e8\u06e9\3\2\2\2\u06e9\u06eb\3\2\2\2\u06ea\u06e8\3\2\2\2\u06eb\u06ec"+
		"\7\u00ba\2\2\u06ec\u06ed\7\b\2\2\u06ed\u00a1\3\2\2\2\u06ee\u06f4\7\u00ba"+
		"\2\2\u06ef\u06f0\7\7\2\2\u06f0\u06f1\5\u009aN\2\u06f1\u06f2\7\b\2\2\u06f2"+
		"\u06f5\3\2\2\2\u06f3\u06f5\7\n\2\2\u06f4\u06ef\3\2\2\2\u06f4\u06f3\3\2"+
		"\2\2\u06f5\u06ff\3\2\2\2\u06f6\u06f7\7\u00ba\2\2\u06f7\u06f8\7\7\2\2\u06f8"+
		"\u06f9\5\u009aN\2\u06f9\u06fa\7\b\2\2\u06fa\u06fb\7\7\2\2\u06fb\u06fc"+
		"\5\u009aN\2\u06fc\u06fd\7\b\2\2\u06fd\u06ff\3\2\2\2\u06fe\u06ee\3\2\2"+
		"\2\u06fe\u06f6\3\2\2\2\u06ff\u00a3\3\2\2\2\u0700\u0705\5\u00a6T\2\u0701"+
		"\u0702\7\23\2\2\u0702\u0704\5\u00a6T\2\u0703\u0701\3\2\2\2\u0704\u0707"+
		"\3\2\2\2\u0705\u0703\3\2\2\2\u0705\u0706\3\2\2\2\u0706\u0709\3\2\2\2\u0707"+
		"\u0705\3\2\2\2\u0708\u0700\3\2\2\2\u0708\u0709\3\2\2\2\u0709\u00a5\3\2"+
		"\2\2\u070a\u070d\5\u00be`\2\u070b\u070d\5\u009aN\2\u070c\u070a\3\2\2\2"+
		"\u070c\u070b\3\2\2\2\u070d\u00a7\3\2\2\2\u070e\u070f\7\u00b2\2\2\u070f"+
		"\u0710\7\u00ba\2\2\u0710\u0711\7\n\2\2\u0711\u0712\7\24\2\2\u0712\u071b"+
		"\7\4\2\2\u0713\u0718\5\u00a6T\2\u0714\u0715\7\23\2\2\u0715\u0717\5\u00a6"+
		"T\2\u0716\u0714\3\2\2\2\u0717\u071a\3\2\2\2\u0718\u0716\3\2\2\2\u0718"+
		"\u0719\3\2\2\2\u0719\u071c\3\2\2\2\u071a\u0718\3\2\2\2\u071b\u0713\3\2"+
		"\2\2\u071b\u071c\3\2\2\2\u071c\u071d\3\2\2\2\u071d\u0735\7\5\2\2\u071e"+
		"\u071f\7\u00b2\2\2\u071f\u0720\7\u00ba\2\2\u0720\u0721\7\n\2\2\u0721\u0722"+
		"\7\n\2\2\u0722\u0723\7\24\2\2\u0723\u0724\7\4\2\2\u0724\u0725\7\4\2\2"+
		"\u0725\u0726\5\u00a4S\2\u0726\u0727\7\5\2\2\u0727\u072f\3\2\2\2\u0728"+
		"\u0729\7\23\2\2\u0729\u072a\7\4\2\2\u072a\u072b\5\u00a4S\2\u072b\u072c"+
		"\7\5\2\2\u072c\u072e\3\2\2\2\u072d\u0728\3\2\2\2\u072e\u0731\3\2\2\2\u072f"+
		"\u072d\3\2\2\2\u072f\u0730\3\2\2\2\u0730\u0732\3\2\2\2\u0731\u072f\3\2"+
		"\2\2\u0732\u0733\7\5\2\2\u0733\u0735\3\2\2\2\u0734\u070e\3\2\2\2\u0734"+
		"\u071e\3\2\2\2\u0735\u00a9\3\2\2\2\u0736\u074b\7\u00bc\2\2\u0737\u074b"+
		"\7\u00c4\2\2\u0738\u0739\7\21\2\2\u0739\u073a\5\u00aaV\2\u073a\u073b\7"+
		"\22\2\2\u073b\u074b\3\2\2\2\u073c\u073d\5\u00acW\2\u073d\u0748\t\16\2"+
		"\2\u073e\u0749\5\u00acW\2\u073f\u0749\7\u00ba\2\2\u0740\u0749\7\u00c4"+
		"\2\2\u0741\u0742\7\u00ba\2\2\u0742\u0743\7\7\2\2\u0743\u0744\5\u009aN"+
		"\2\u0744\u0745\7\b\2\2\u0745\u0749\3\2\2\2\u0746\u0749\5b\62\2\u0747\u0749"+
		"\5\u00f0y\2\u0748\u073e\3\2\2\2\u0748\u073f\3\2\2\2\u0748\u0740\3\2\2"+
		"\2\u0748\u0741\3\2\2\2\u0748\u0746\3\2\2\2\u0748\u0747\3\2\2\2\u0749\u074b"+
		"\3\2\2\2\u074a\u0736\3\2\2\2\u074a\u0737\3\2\2\2\u074a\u0738\3\2\2\2\u074a"+
		"\u073c\3\2\2\2\u074b\u00ab\3\2\2\2\u074c\u0756\7\21\2\2\u074d\u0757\7"+
		"\u00ba\2\2\u074e\u0757\7\u00c4\2\2\u074f\u0757\5\u00f0y\2\u0750\u0751"+
		"\7\u00ba\2\2\u0751\u0752\7\7\2\2\u0752\u0753\5\u009aN\2\u0753\u0754\7"+
		"\b\2\2\u0754\u0757\3\2\2\2\u0755\u0757\5b\62\2\u0756\u074d\3\2\2\2\u0756"+
		"\u074e\3\2\2\2\u0756\u074f\3\2\2\2\u0756\u0750\3\2\2\2\u0756\u0755\3\2"+
		"\2\2\u0757\u0758\3\2\2\2\u0758\u0759\t\5\2\2\u0759\u075a\5\u00acW\2\u075a"+
		"\u075b\7\22\2\2\u075b\u079c\3\2\2\2\u075c\u0767\7\u00ba\2\2\u075d\u0767"+
		"\7\u00c4\2\2\u075e\u0767\5\u00f0y\2\u075f\u0760\7\u00ba\2\2\u0760\u0761"+
		"\7\7\2\2\u0761\u0762\5\u009aN\2\u0762\u0763\7\b\2\2\u0763\u0767\3\2\2"+
		"\2\u0764\u0767\5b\62\2\u0765\u0767\5\u0080A\2\u0766\u075c\3\2\2\2\u0766"+
		"\u075d\3\2\2\2\u0766\u075e\3\2\2\2\u0766\u075f\3\2\2\2\u0766\u0764\3\2"+
		"\2\2\u0766\u0765\3\2\2\2\u0767\u0768\3\2\2\2\u0768\u076a\t\17\2\2\u0769"+
		"\u0766\3\2\2\2\u076a\u076d\3\2\2\2\u076b\u0769\3\2\2\2\u076b\u076c\3\2"+
		"\2\2\u076c\u0787\3\2\2\2\u076d\u076b\3\2\2\2\u076e\u0779\7\u00ba\2\2\u076f"+
		"\u0779\7\u00c4\2\2\u0770\u0779\5\u00f0y\2\u0771\u0772\7\u00ba\2\2\u0772"+
		"\u0773\7\7\2\2\u0773\u0774\5\u009aN\2\u0774\u0775\7\b\2\2\u0775\u0779"+
		"\3\2\2\2\u0776\u0779\5b\62\2\u0777\u0779\5\u0080A\2\u0778\u076e\3\2\2"+
		"\2\u0778\u076f\3\2\2\2\u0778\u0770\3\2\2\2\u0778\u0771\3\2\2\2\u0778\u0776"+
		"\3\2\2\2\u0778\u0777\3\2\2\2\u0779\u077a\3\2\2\2\u077a\u0785\t\17\2\2"+
		"\u077b\u0786\7\u00ba\2\2\u077c\u0786\7\u00c4\2\2\u077d\u0786\5\u00f0y"+
		"\2\u077e\u077f\7\u00ba\2\2\u077f\u0780\7\7\2\2\u0780\u0781\5\u009aN\2"+
		"\u0781\u0782\7\b\2\2\u0782\u0786\3\2\2\2\u0783\u0786\5b\62\2\u0784\u0786"+
		"\5\u0080A\2\u0785\u077b\3\2\2\2\u0785\u077c\3\2\2\2\u0785\u077d\3\2\2"+
		"\2\u0785\u077e\3\2\2\2\u0785\u0783\3\2\2\2\u0785\u0784\3\2\2\2\u0786\u0788"+
		"\3\2\2\2\u0787\u0778\3\2\2\2\u0788\u0789\3\2\2\2\u0789\u0787\3\2\2\2\u0789"+
		"\u078a\3\2\2\2\u078a\u079c\3\2\2\2\u078b\u0796\7\u00ba\2\2\u078c\u0796"+
		"\7\u00c4\2\2\u078d\u0796\5\u00f0y\2\u078e\u078f\7\u00ba\2\2\u078f\u0790"+
		"\7\7\2\2\u0790\u0791\5\u009aN\2\u0791\u0792\7\b\2\2\u0792\u0796\3\2\2"+
		"\2\u0793\u0796\5b\62\2\u0794\u0796\5\u0080A\2\u0795\u078b\3\2\2\2\u0795"+
		"\u078c\3\2\2\2\u0795\u078d\3\2\2\2\u0795\u078e\3\2\2\2\u0795\u0793\3\2"+
		"\2\2\u0795\u0794\3\2\2\2\u0796\u079c\3\2\2\2\u0797\u0798\7\21\2\2\u0798"+
		"\u0799\5\u00acW\2\u0799\u079a\7\22\2\2\u079a\u079c\3\2\2\2\u079b\u074c"+
		"\3\2\2\2\u079b\u076b\3\2\2\2\u079b\u0795\3\2\2\2\u079b\u0797\3\2\2\2\u079c"+
		"\u00ad\3\2\2\2\u079d\u079e\7\u00b6\2\2\u079e\u079f\7\21\2\2\u079f\u07a0"+
		"\5l\67\2\u07a0\u07a7\7\22\2\2\u07a1\u07a2\7\4\2\2\u07a2\u07a3\5V,\2\u07a3"+
		"\u07a4\7\5\2\2\u07a4\u07a8\3\2\2\2\u07a5\u07a8\5X-\2\u07a6\u07a8\7\17"+
		"\2\2\u07a7\u07a1\3\2\2\2\u07a7\u07a5\3\2\2\2\u07a7\u07a6\3\2\2\2\u07a8"+
		"\u00af\3\2\2\2\u07a9\u07ba\7\u00b7\2\2\u07aa\u07ab\7\4\2\2\u07ab\u07ac"+
		"\5V,\2\u07ac\u07ad\7\5\2\2\u07ad\u07ae\7\u00b6\2\2\u07ae\u07af\7\21\2"+
		"\2\u07af\u07b0\5l\67\2\u07b0\u07b1\7\22\2\2\u07b1\u07b2\7\17\2\2\u07b2"+
		"\u07bb\3\2\2\2\u07b3\u07b4\5X-\2\u07b4\u07b5\7\u00b6\2\2\u07b5\u07b6\7"+
		"\21\2\2\u07b6\u07b7\5l\67\2\u07b7\u07b8\7\22\2\2\u07b8\u07b9\7\17\2\2"+
		"\u07b9\u07bb\3\2\2\2\u07ba\u07aa\3\2\2\2\u07ba\u07b3\3\2\2\2\u07bb\u00b1"+
		"\3\2\2\2\u07bc\u07bd\7\u00b2\2\2\u07bd\u07be\7\u00ba\2\2\u07be\u07c0\7"+
		"\24\2\2\u07bf\u07bc\3\2\2\2\u07bf\u07c0\3\2\2\2\u07c0\u07c1\3\2\2\2\u07c1"+
		"\u07c2\5J&\2\u07c2\u07c3\7\21\2\2\u07c3\u07c4\5\u00ba^\2\u07c4\u07c5\7"+
		"\22\2\2\u07c5\u00b3\3\2\2\2\u07c6\u07c7\7\u00b2\2\2\u07c7\u07c8\7\u00ba"+
		"\2\2\u07c8\u07ca\7\24\2\2\u07c9\u07c6\3\2\2\2\u07c9\u07ca\3\2\2\2\u07ca"+
		"\u07cb\3\2\2\2\u07cb\u07cc\5J&\2\u07cc\u07cd\7\21\2\2\u07cd\u07ce\5\u00bc"+
		"_\2\u07ce\u07cf\7\22\2\2\u07cf\u00b5\3\2\2\2\u07d0\u07d1\7\u00b9\2\2\u07d1"+
		"\u07d2\7\21\2\2\u07d2\u07d3\5\u00b8]\2\u07d3\u07d4\7\22\2\2\u07d4\u07d5"+
		"\5V,\2\u07d5\u00b7\3\2\2\2\u07d6\u07d9\5\u00be`\2\u07d7\u07d9\5\u009a"+
		"N\2\u07d8\u07d6\3\2\2\2\u07d8\u07d7\3\2\2\2\u07d9\u07e1\3\2\2\2\u07da"+
		"\u07dd\7\23\2\2\u07db\u07de\5\u00be`\2\u07dc\u07de\5\u009aN\2\u07dd\u07db"+
		"\3\2\2\2\u07dd\u07dc\3\2\2\2\u07de\u07e0\3\2\2\2\u07df\u07da\3\2\2\2\u07e0"+
		"\u07e3\3\2\2\2\u07e1\u07df\3\2\2\2\u07e1\u07e2\3\2\2\2\u07e2\u07e5\3\2"+
		"\2\2\u07e3\u07e1\3\2\2\2\u07e4\u07d8\3\2\2\2\u07e4\u07e5\3\2\2\2\u07e5"+
		"\u00b9\3\2\2\2\u07e6\u07eb\5\u009aN\2\u07e7\u07e8\7\23\2\2\u07e8\u07ea"+
		"\5\u009aN\2\u07e9\u07e7\3\2\2\2\u07ea\u07ed\3\2\2\2\u07eb\u07e9\3\2\2"+
		"\2\u07eb\u07ec\3\2\2\2\u07ec\u07f4\3\2\2\2\u07ed\u07eb\3\2\2\2\u07ee\u07f1"+
		"\7\25\2\2\u07ef\u07f0\7\23\2\2\u07f0\u07f2\5\u00b6\\\2\u07f1\u07ef\3\2"+
		"\2\2\u07f1\u07f2\3\2\2\2\u07f2\u07f4\3\2\2\2\u07f3\u07e6\3\2\2\2\u07f3"+
		"\u07ee\3\2\2\2\u07f3\u07f4\3\2\2\2\u07f4\u00bb\3\2\2\2\u07f5\u07f7\7T"+
		"\2\2\u07f6\u07f5\3\2\2\2\u07f6\u07f7\3\2\2\2\u07f7\u07f8\3\2\2\2\u07f8"+
		"\u07fd\5F$\2\u07f9\u07fa\7\23\2\2\u07fa\u07fc\5F$\2\u07fb\u07f9\3\2\2"+
		"\2\u07fc\u07ff\3\2\2\2\u07fd\u07fb\3\2\2\2\u07fd\u07fe\3\2\2\2\u07fe\u0802"+
		"\3\2\2\2\u07ff\u07fd\3\2\2\2\u0800\u0802\7\25\2\2\u0801\u07f6\3\2\2\2"+
		"\u0801\u0800\3\2\2\2\u0801\u0802\3\2\2\2\u0802\u00bd\3\2\2\2\u0803\u0804"+
		"\b`\1\2\u0804\u0807\7\u00b4\2\2\u0805\u0807\7\u00b5\2\2\u0806\u0803\3"+
		"\2\2\2\u0806\u0805\3\2\2\2\u0807\u080d\3\2\2\2\u0808\u0809\f\3\2\2\u0809"+
		"\u080a\t\20\2\2\u080a\u080c\5\u00be`\4\u080b\u0808\3\2\2\2\u080c\u080f"+
		"\3\2\2\2\u080d\u080b\3\2\2\2\u080d\u080e\3\2\2\2\u080e\u00bf\3\2\2\2\u080f"+
		"\u080d\3\2\2\2\u0810\u0814\7\u008e\2\2\u0811\u0812\5\u0104\u0083\2\u0812"+
		"\u0813\7\20\2\2\u0813\u0815\3\2\2\2\u0814\u0811\3\2\2\2\u0814\u0815\3"+
		"\2\2\2\u0815\u0816\3\2\2\2\u0816\u0822\5\u0112\u008a\2\u0817\u0818\7\21"+
		"\2\2\u0818\u081d\5\u00c2b\2\u0819\u081a\7\23\2\2\u081a\u081c\5\u00c2b"+
		"\2\u081b\u0819\3\2\2\2\u081c\u081f\3\2\2\2\u081d\u081b\3\2\2\2\u081d\u081e"+
		"\3\2\2\2\u081e\u0820\3\2\2\2\u081f\u081d\3\2\2\2\u0820\u0821\7\22\2\2"+
		"\u0821\u0823\3\2\2\2\u0822\u0817\3\2\2\2\u0822\u0823\3\2\2\2\u0823\u0836"+
		"\3\2\2\2\u0824\u0825\7\u0083\2\2\u0825\u082e\t\21\2\2\u0826\u0827\7\u009a"+
		"\2\2\u0827\u082f\7\u0080\2\2\u0828\u0829\7\u009a\2\2\u0829\u082f\7N\2"+
		"\2\u082a\u082f\7?\2\2\u082b\u082f\7\u0094\2\2\u082c\u082d\7}\2\2\u082d"+
		"\u082f\7.\2\2\u082e\u0826\3\2\2\2\u082e\u0828\3\2\2\2\u082e\u082a\3\2"+
		"\2\2\u082e\u082b\3\2\2\2\u082e\u082c\3\2\2\2\u082f\u0833\3\2\2\2\u0830"+
		"\u0831\7z\2\2\u0831\u0833\5\u0100\u0081\2\u0832\u0824\3\2\2\2\u0832\u0830"+
		"\3\2\2\2\u0833\u0835\3\2\2\2\u0834\u0832\3\2\2\2\u0835\u0838\3\2\2\2\u0836"+
		"\u0834\3\2\2\2\u0836\u0837\3\2\2\2\u0837\u0846\3\2\2\2\u0838\u0836\3\2"+
		"\2\2\u0839\u083b\7~\2\2\u083a\u0839\3\2\2\2\u083a\u083b\3\2\2\2\u083b"+
		"\u083c\3\2\2\2\u083c\u0841\7O\2\2\u083d\u083e\7m\2\2\u083e\u0842\7P\2"+
		"\2\u083f\u0840\7m\2\2\u0840\u0842\7i\2\2\u0841\u083d\3\2\2\2\u0841\u083f"+
		"\3\2\2\2\u0841\u0842\3\2\2\2\u0842\u0844\3\2\2\2\u0843\u0845\7Y\2\2\u0844"+
		"\u0843\3\2\2\2\u0844\u0845\3\2\2\2\u0845\u0847\3\2\2\2\u0846\u083a\3\2"+
		"\2\2\u0846\u0847\3\2\2\2\u0847\u00c1\3\2\2\2\u0848\u0849\5\u0100\u0081"+
		"\2\u0849\u00c3\3\2\2\2\u084a\u084d\5\u010e\u0088\2\u084b\u084c\7C\2\2"+
		"\u084c\u084e\5\u0110\u0089\2\u084d\u084b\3\2\2\2\u084d\u084e\3\2\2\2\u084e"+
		"\u0850\3\2\2\2\u084f\u0851\t\4\2\2\u0850\u084f\3\2\2\2\u0850\u0851\3\2"+
		"\2\2\u0851\u00c5\3\2\2\2\u0852\u0853\7G\2\2\u0853\u0855\5\u0100\u0081"+
		"\2\u0854\u0852\3\2\2\2\u0854\u0855\3\2\2\2\u0855\u085f\3\2\2\2\u0856\u0860"+
		"\5\u00c8e\2\u0857\u0860\5\u00ceh\2\u0858\u0860\5\u00ccg\2\u0859\u085a"+
		"\7B\2\2\u085a\u085b\7\21\2\2\u085b\u085c\5F$\2\u085c\u085d\7\22\2\2\u085d"+
		"\u0860\3\2\2\2\u085e\u0860\5\u00caf\2\u085f\u0856\3\2\2\2\u085f\u0857"+
		"\3\2\2\2\u085f\u0858\3\2\2\2\u085f\u0859\3\2\2\2\u085f\u085e\3\2\2\2\u0860"+
		"\u00c7\3\2\2\2\u0861\u0862\7\u008a\2\2\u0862\u0863\7v\2\2\u0863\u0864"+
		"\7\21\2\2\u0864\u0869\5\u00c4c\2\u0865\u0866\7\23\2\2\u0866\u0868\5\u00c4"+
		"c\2\u0867\u0865\3\2\2\2\u0868\u086b\3\2\2\2\u0869\u0867\3\2\2\2\u0869"+
		"\u086a\3\2\2\2\u086a\u086c\3\2\2\2\u086b\u0869\3\2\2\2\u086c\u086d\7\22"+
		"\2\2\u086d\u00c9\3\2\2\2\u086e\u086f\7a\2\2\u086f\u0870\7v\2\2\u0870\u0871"+
		"\7\21\2\2\u0871\u0876\5\u00d0i\2\u0872\u0873\7\23\2\2\u0873\u0875\5\u00d0"+
		"i\2\u0874\u0872\3\2\2\2\u0875\u0878\3\2\2\2\u0876\u0874\3\2\2\2\u0876"+
		"\u0877\3\2\2\2\u0877\u0879\3\2\2\2\u0878\u0876\3\2\2\2\u0879\u087a\7\22"+
		"\2\2\u087a\u087b\5\u00c0a\2\u087b\u00cb\3\2\2\2\u087c\u087e\7\u00a5\2"+
		"\2\u087d\u087f\7v\2\2\u087e\u087d\3\2\2\2\u087e\u087f\3\2\2\2\u087f\u0881"+
		"\3\2\2\2\u0880\u0882\5\u0100\u0081\2\u0881\u0880\3\2\2\2\u0881\u0882\3"+
		"\2\2\2\u0882\u0883\3\2\2\2\u0883\u0884\7\21\2\2\u0884\u0889\5\u00c4c\2"+
		"\u0885\u0886\7\23\2\2\u0886\u0888\5\u00c4c\2\u0887\u0885\3\2\2\2\u0888"+
		"\u088b\3\2\2\2\u0889\u0887\3\2\2\2\u0889\u088a\3\2\2\2\u088a\u088c\3\2"+
		"\2\2\u088b\u0889\3\2\2\2\u088c\u088d\7\22\2\2\u088d\u00cd\3\2\2\2\u088e"+
		"\u0890\7v\2\2\u088f\u0891\5\u0100\u0081\2\u0890\u088f\3\2\2\2\u0890\u0891"+
		"\3\2\2\2\u0891\u0892\3\2\2\2\u0892\u0893\7\21\2\2\u0893\u0898\5\u00c4"+
		"c\2\u0894\u0895\7\23\2\2\u0895\u0897\5\u00c4c\2\u0896\u0894\3\2\2\2\u0897"+
		"\u089a\3\2\2\2\u0898\u0896\3\2\2\2\u0898\u0899\3\2\2\2\u0899\u089b\3\2"+
		"\2\2\u089a\u0898\3\2\2\2\u089b\u089c\7\22\2\2\u089c\u00cf\3\2\2\2\u089d"+
		"\u089e\5\u010e\u0088\2\u089e\u00d1\3\2\2\2\u089f\u08a0\5\u0104\u0083\2"+
		"\u08a0\u08a1\7\20\2\2\u08a1\u08a3\3\2\2\2\u08a2\u089f\3\2\2\2\u08a2\u08a3"+
		"\3\2\2\2\u08a3\u08a4\3\2\2\2\u08a4\u08aa\5\u0108\u0085\2\u08a5\u08a6\7"+
		"l\2\2\u08a6\u08a7\7>\2\2\u08a7\u08ab\5\u0114\u008b\2\u08a8\u08a9\7~\2"+
		"\2\u08a9\u08ab\7l\2\2\u08aa\u08a5\3\2\2\2\u08aa\u08a8\3\2\2\2\u08aa\u08ab"+
		"\3\2\2\2\u08ab\u00d3\3\2\2\2\u08ac\u08af\5F$\2\u08ad\u08ae\7C\2\2\u08ae"+
		"\u08b0\5\u0110\u0089\2\u08af\u08ad\3\2\2\2\u08af\u08b0\3\2\2\2\u08b0\u08b2"+
		"\3\2\2\2\u08b1\u08b3\t\4\2\2\u08b2\u08b1\3\2\2\2\u08b2\u08b3\3\2\2\2\u08b3"+
		"\u00d5\3\2\2\2\u08b4\u08b8\5\u00eex\2\u08b5\u08b8\5\u0100\u0081\2\u08b6"+
		"\u08b8\7\u00be\2\2\u08b7\u08b4\3\2\2\2\u08b7\u08b5\3\2\2\2\u08b7\u08b6"+
		"\3\2\2\2\u08b8\u00d7\3\2\2\2\u08b9\u08c5\5\u0108\u0085\2\u08ba\u08bb\7"+
		"\21\2\2\u08bb\u08c0\5\u010e\u0088\2\u08bc\u08bd\7\23\2\2\u08bd\u08bf\5"+
		"\u010e\u0088\2\u08be\u08bc\3\2\2\2\u08bf\u08c2\3\2\2\2\u08c0\u08be\3\2"+
		"\2\2\u08c0\u08c1\3\2\2\2\u08c1\u08c3\3\2\2\2\u08c2\u08c0\3\2\2\2\u08c3"+
		"\u08c4\7\22\2\2\u08c4\u08c6\3\2\2\2\u08c5\u08ba\3\2\2\2\u08c5\u08c6\3"+
		"\2\2\2\u08c6\u08c7\3\2\2\2\u08c7\u08c8\7\67\2\2\u08c8\u08c9\7\21\2\2\u08c9"+
		"\u08ca\5*\26\2\u08ca\u08cb\7\22\2\2\u08cb\u00d9\3\2\2\2\u08cc\u08d9\7"+
		"\25\2\2\u08cd\u08ce\5\u0108\u0085\2\u08ce\u08cf\7\20\2\2\u08cf\u08d0\7"+
		"\25\2\2\u08d0\u08d9\3\2\2\2\u08d1\u08d6\5F$\2\u08d2\u08d4\7\67\2\2\u08d3"+
		"\u08d2\3\2\2\2\u08d3\u08d4\3\2\2\2\u08d4\u08d5\3\2\2\2\u08d5\u08d7\5\u00fa"+
		"~\2\u08d6\u08d3\3\2\2\2\u08d6\u08d7\3\2\2\2\u08d7\u08d9\3\2\2\2\u08d8"+
		"\u08cc\3\2\2\2\u08d8\u08cd\3\2\2\2\u08d8\u08d1\3\2\2\2\u08d9\u00db\3\2"+
		"\2\2\u08da\u08db\5\u0104\u0083\2\u08db\u08dc\7\20\2\2\u08dc\u08de\3\2"+
		"\2\2\u08dd\u08da\3\2\2\2\u08dd\u08de\3\2\2\2\u08de\u08df\3\2\2\2\u08df"+
		"\u08e4\5\u0108\u0085\2\u08e0\u08e2\7\67\2\2\u08e1\u08e0\3\2\2\2\u08e1"+
		"\u08e2\3\2\2\2\u08e2\u08e3\3\2\2\2\u08e3\u08e5\5\u0120\u0091\2\u08e4\u08e1"+
		"\3\2\2\2\u08e4\u08e5\3\2\2\2\u08e5\u08eb\3\2\2\2\u08e6\u08e7\7l\2\2\u08e7"+
		"\u08e8\7>\2\2\u08e8\u08ec\5\u0114\u008b\2\u08e9\u08ea\7~\2\2\u08ea\u08ec"+
		"\7l\2\2\u08eb\u08e6\3\2\2\2\u08eb\u08e9\3\2\2\2\u08eb\u08ec\3\2\2\2\u08ec"+
		"\u090a\3\2\2\2\u08ed\u08f7\7\21\2\2\u08ee\u08f3\5\u00dco\2\u08ef\u08f0"+
		"\7\23\2\2\u08f0\u08f2\5\u00dco\2\u08f1\u08ef\3\2\2\2\u08f2\u08f5\3\2\2"+
		"\2\u08f3\u08f1\3\2\2\2\u08f3\u08f4\3\2\2\2\u08f4\u08f8\3\2\2\2\u08f5\u08f3"+
		"\3\2\2\2\u08f6\u08f8\5\u00dep\2\u08f7\u08ee\3\2\2\2\u08f7\u08f6\3\2\2"+
		"\2\u08f8\u08f9\3\2\2\2\u08f9\u08fe\7\22\2\2\u08fa\u08fc\7\67\2\2\u08fb"+
		"\u08fa\3\2\2\2\u08fb\u08fc\3\2\2\2\u08fc\u08fd\3\2\2\2\u08fd\u08ff\5\u0120"+
		"\u0091\2\u08fe\u08fb\3\2\2\2\u08fe\u08ff\3\2\2\2\u08ff\u090a\3\2\2\2\u0900"+
		"\u0901\7\21\2\2\u0901\u0902\5$\23\2\u0902\u0907\7\22\2\2\u0903\u0905\7"+
		"\67\2\2\u0904\u0903\3\2\2\2\u0904\u0905\3\2\2\2\u0905\u0906\3\2\2\2\u0906"+
		"\u0908\5\u0120\u0091\2\u0907\u0904\3\2\2\2\u0907\u0908\3\2\2\2\u0908\u090a"+
		"\3\2\2\2\u0909\u08dd\3\2\2\2\u0909\u08ed\3\2\2\2\u0909\u0900\3\2\2\2\u090a"+
		"\u00dd\3\2\2\2\u090b\u0912\5\u00dco\2\u090c\u090d\5\u00e0q\2\u090d\u090e"+
		"\5\u00dco\2\u090e\u090f\5\u00e2r\2\u090f\u0911\3\2\2\2\u0910\u090c\3\2"+
		"\2\2\u0911\u0914\3\2\2\2\u0912\u0910\3\2\2\2\u0912\u0913\3\2\2\2\u0913"+
		"\u00df\3\2\2\2\u0914\u0912\3\2\2\2\u0915\u091f\7\23\2\2\u0916\u0918\7"+
		"w\2\2\u0917\u0919\7\u0087\2\2\u0918\u0917\3\2\2\2\u0918\u0919\3\2\2\2"+
		"\u0919\u091c\3\2\2\2\u091a\u091c\7n\2\2";
	private static final String _serializedATNSegment1 =
		"\u091b\u0916\3\2\2\2\u091b\u091a\3\2\2\2\u091b\u091c\3\2\2\2\u091c\u091d"+
		"\3\2\2\2\u091d\u091f\7u\2\2\u091e\u0915\3\2\2\2\u091e\u091b\3\2\2\2\u091f"+
		"\u00e1\3\2\2\2\u0920\u0921\7\u0083\2\2\u0921\u0923\5F$\2\u0922\u0920\3"+
		"\2\2\2\u0922\u0923\3\2\2\2\u0923\u00e3\3\2\2\2\u0924\u0926\7\u0099\2\2"+
		"\u0925\u0927\t\3\2\2\u0926\u0925\3\2\2\2\u0926\u0927\3\2\2\2\u0927\u0928"+
		"\3\2\2\2\u0928\u092d\5\u00dan\2\u0929\u092a\7\23\2\2\u092a\u092c\5\u00da"+
		"n\2\u092b\u0929\3\2\2\2\u092c\u092f\3\2\2\2\u092d\u092b\3\2\2\2\u092d"+
		"\u092e\3\2\2\2\u092e\u093c\3\2\2\2\u092f\u092d\3\2\2\2\u0930\u093a\7b"+
		"\2\2\u0931\u0936\5\u00dco\2\u0932\u0933\7\23\2\2\u0933\u0935\5\u00dco"+
		"\2\u0934\u0932\3\2\2\2\u0935\u0938\3\2\2\2\u0936\u0934\3\2\2\2\u0936\u0937"+
		"\3\2\2\2\u0937\u093b\3\2\2\2\u0938\u0936\3\2\2\2\u0939\u093b\5\u00dep"+
		"\2\u093a\u0931\3\2\2\2\u093a\u0939\3\2\2\2\u093b\u093d\3\2\2\2\u093c\u0930"+
		"\3\2\2\2\u093c\u093d\3\2\2\2\u093d\u0940\3\2\2\2\u093e\u093f\7\u00ad\2"+
		"\2\u093f\u0941\5F$\2\u0940\u093e\3\2\2\2\u0940\u0941\3\2\2\2\u0941\u0950"+
		"\3\2\2\2\u0942\u0943\7e\2\2\u0943\u0944\7>\2\2\u0944\u0949\5F$\2\u0945"+
		"\u0946\7\23\2\2\u0946\u0948\5F$\2\u0947\u0945\3\2\2\2\u0948\u094b\3\2"+
		"\2\2\u0949\u0947\3\2\2\2\u0949\u094a\3\2\2\2\u094a\u094e\3\2\2\2\u094b"+
		"\u0949\3\2\2\2\u094c\u094d\7f\2\2\u094d\u094f\5F$\2\u094e\u094c\3\2\2"+
		"\2\u094e\u094f\3\2\2\2\u094f\u0951\3\2\2\2\u0950\u0942\3\2\2\2\u0950\u0951"+
		"\3\2\2\2\u0951\u096f\3\2\2\2\u0952\u0953\7\u00a9\2\2\u0953\u0954\7\21"+
		"\2\2\u0954\u0959\5F$\2\u0955\u0956\7\23\2\2\u0956\u0958\5F$\2\u0957\u0955"+
		"\3\2\2\2\u0958\u095b\3\2\2\2\u0959\u0957\3\2\2\2\u0959\u095a\3\2\2\2\u095a"+
		"\u095c\3\2\2\2\u095b\u0959\3\2\2\2\u095c\u096b\7\22\2\2\u095d\u095e\7"+
		"\23\2\2\u095e\u095f\7\21\2\2\u095f\u0964\5F$\2\u0960\u0961\7\23\2\2\u0961"+
		"\u0963\5F$\2\u0962\u0960\3\2\2\2\u0963\u0966\3\2\2\2\u0964\u0962\3\2\2"+
		"\2\u0964\u0965\3\2\2\2\u0965\u0967\3\2\2\2\u0966\u0964\3\2\2\2\u0967\u0968"+
		"\7\22\2\2\u0968\u096a\3\2\2\2\u0969\u095d\3\2\2\2\u096a\u096d\3\2\2\2"+
		"\u096b\u0969\3\2\2\2\u096b\u096c\3\2\2\2\u096c\u096f\3\2\2\2\u096d\u096b"+
		"\3\2\2\2\u096e\u0924\3\2\2\2\u096e\u0952\3\2\2\2\u096f\u00e5\3\2\2\2\u0970"+
		"\u0971\7H\2\2\u0971\u0975\7\u009c\2\2\u0972\u0973\7g\2\2\u0973\u0974\7"+
		"~\2\2\u0974\u0976\7]\2\2\u0975\u0972\3\2\2\2\u0975\u0976\3\2\2\2\u0976"+
		"\u0977\3\2\2\2\u0977\u0978\5\u00e8u\2\u0978\u0979\7\21\2\2\u0979\u097e"+
		"\5\u00eav\2\u097a\u097b\7\23\2\2\u097b\u097d\5\u00eav\2\u097c\u097a\3"+
		"\2\2\2\u097d\u0980\3\2\2\2\u097e\u097c\3\2\2\2\u097e\u097f\3\2\2\2\u097f"+
		"\u0981\3\2\2\2\u0980\u097e\3\2\2\2\u0981\u0982\7\22\2\2\u0982\u00e7\3"+
		"\2\2\2\u0983\u0984\5\u0124\u0093\2\u0984\u00e9\3\2\2\2\u0985\u0989\5\u010e"+
		"\u0088\2\u0986\u0988\5\66\34\2\u0987\u0986\3\2\2\2\u0988\u098b\3\2\2\2"+
		"\u0989\u0987\3\2\2\2\u0989\u098a\3\2\2\2\u098a\u00eb\3\2\2\2\u098b\u0989"+
		"\3\2\2\2\u098c\u0998\5\u0108\u0085\2\u098d\u098e\7\21\2\2\u098e\u0993"+
		"\5\u010e\u0088\2\u098f\u0990\7\23\2\2\u0990\u0992\5\u010e\u0088\2\u0991"+
		"\u098f\3\2\2\2\u0992\u0995\3\2\2\2\u0993\u0991\3\2\2\2\u0993\u0994\3\2"+
		"\2\2\u0994\u0996\3\2\2\2\u0995\u0993\3\2\2\2\u0996\u0997\7\22\2\2\u0997"+
		"\u0999\3\2\2\2\u0998\u098d\3\2\2\2\u0998\u0999\3\2\2\2\u0999\u00ed\3\2"+
		"\2\2\u099a\u099c\t\7\2\2\u099b\u099a\3\2\2\2\u099b\u099c\3\2\2\2\u099c"+
		"\u099d\3\2\2\2\u099d\u09a0\7\u00bc\2\2\u099e\u09a0\7\25\2\2\u099f\u099b"+
		"\3\2\2\2\u099f\u099e\3\2\2\2\u09a0\u00ef\3\2\2\2\u09a1\u09a2\t\22\2\2"+
		"\u09a2\u00f1\3\2\2\2\u09a3\u09ac\7\u00bc\2\2\u09a4\u09ac\7\u00ba\2\2\u09a5"+
		"\u09ac\7\u00be\2\2\u09a6\u09ac\7\u00bf\2\2\u09a7\u09ac\7\u0080\2\2\u09a8"+
		"\u09ac\7\u00b4\2\2\u09a9\u09ac\7\u00b5\2\2\u09aa\u09ac\5`\61\2\u09ab\u09a3"+
		"\3\2\2\2\u09ab\u09a4\3\2\2\2\u09ab\u09a5\3\2\2\2\u09ab\u09a6\3\2\2\2\u09ab"+
		"\u09a7\3\2\2\2\u09ab\u09a8\3\2\2\2\u09ab\u09a9\3\2\2\2\u09ab\u09aa\3\2"+
		"\2\2\u09ac\u00f3\3\2\2\2\u09ad\u09ae\t\23\2\2\u09ae\u00f5\3\2\2\2\u09af"+
		"\u09b0\7\u00be\2\2\u09b0\u00f7\3\2\2\2\u09b1\u09b4\5F$\2\u09b2\u09b4\5"+
		"\64\33\2\u09b3\u09b1\3\2\2\2\u09b3\u09b2\3\2\2\2\u09b4\u00f9\3\2\2\2\u09b5"+
		"\u09b6\t\24\2\2\u09b6\u00fb\3\2\2\2\u09b7\u09b8\t\25\2\2\u09b8\u00fd\3"+
		"\2\2\2\u09b9\u09bb\13\2\2\2\u09ba\u09b9\3\2\2\2\u09bb\u09bc\3\2\2\2\u09bc"+
		"\u09ba\3\2\2\2\u09bc\u09bd\3\2\2\2\u09bd\u00ff\3\2\2\2\u09be\u09bf\5\u0124"+
		"\u0093\2\u09bf\u0101\3\2\2\2\u09c0\u09c1\5\u0124\u0093\2\u09c1\u0103\3"+
		"\2\2\2\u09c2\u09c3\5\u0124\u0093\2\u09c3\u0105\3\2\2\2\u09c4\u09c5\5\u0124"+
		"\u0093\2\u09c5\u0107\3\2\2\2\u09c6\u09c7\5\u0124\u0093\2\u09c7\u0109\3"+
		"\2\2\2\u09c8\u09c9\5\u0124\u0093\2\u09c9\u010b\3\2\2\2\u09ca\u09cb\5\u0124"+
		"\u0093\2\u09cb\u010d\3\2\2\2\u09cc\u09cd\5\u0124\u0093\2\u09cd\u010f\3"+
		"\2\2\2\u09ce\u09cf\5\u0124\u0093\2\u09cf\u0111\3\2\2\2\u09d0\u09d1\5\u0124"+
		"\u0093\2\u09d1\u0113\3\2\2\2\u09d2\u09d3\5\u0124\u0093\2\u09d3\u0115\3"+
		"\2\2\2\u09d4\u09d5\5\u0124\u0093\2\u09d5\u0117\3\2\2\2\u09d6\u09d7\5\u0124"+
		"\u0093\2\u09d7\u0119\3\2\2\2\u09d8\u09d9\5\u0124\u0093\2\u09d9\u011b\3"+
		"\2\2\2\u09da\u09db\5\u0124\u0093\2\u09db\u011d\3\2\2\2\u09dc\u09dd\5\u0124"+
		"\u0093\2\u09dd\u011f\3\2\2\2\u09de\u09df\5\u0124\u0093\2\u09df\u0121\3"+
		"\2\2\2\u09e0\u09e1\5\u0124\u0093\2\u09e1\u0123\3\2\2\2\u09e2\u09e9\7\u00ba"+
		"\2\2\u09e3\u09e9\7\u00be\2\2\u09e4\u09e5\7\21\2\2\u09e5\u09e6\5\u0124"+
		"\u0093\2\u09e6\u09e7\7\22\2\2\u09e7\u09e9\3\2\2\2\u09e8\u09e2\3\2\2\2"+
		"\u09e8\u09e3\3\2\2\2\u09e8\u09e4\3\2\2\2\u09e9\u0125\3\2\2\2\u0128\u0129"+
		"\u012b\u0136\u013d\u0142\u0148\u0154\u015b\u0165\u0168\u0177\u017c\u0185"+
		"\u0187\u018c\u018f\u0193\u01ab\u01bc\u01c2\u01c7\u01d3\u01d6\u01dc\u01de"+
		"\u01e1\u01e5\u01ef\u01f8\u01fd\u0206\u0211\u0218\u021e\u0228\u022b\u0231"+
		"\u0233\u0237\u023e\u0247\u024b\u024d\u0251\u025a\u025f\u0261\u026a\u0275"+
		"\u027c\u027f\u0288\u0295\u029b\u029d\u02a4\u02ab\u02b0\u02b4\u02b8\u02c7"+
		"\u02cc\u02cf\u02e4\u02ea\u02ec\u02f0\u02f8\u02fd\u0308\u030f\u0313\u031d"+
		"\u0320\u0326\u0346\u0351\u035b\u035e\u0364\u0367\u0369\u036b\u037b\u0382"+
		"\u0386\u0390\u0393\u0399\u03ba\u03c5\u03cf\u03d2\u03d8\u03db\u03dd\u03df"+
		"\u03e9\u03ec\u03ef\u03f6\u03fd\u0400\u0402\u0408\u040f\u0412\u041e\u044d"+
		"\u044f\u0453\u0480\u0483\u048f\u0492\u049f\u04a7\u04aa\u04b3\u04b6\u04b9"+
		"\u04c2\u04cf\u04e3\u04eb\u04ed\u04fb\u0503\u0505\u0511\u0516\u051a\u0526"+
		"\u052e\u0536\u0540\u0546\u054d\u0552\u0556\u056a\u057e\u0592\u05a6\u05ba"+
		"\u05bc\u05c8\u05ce\u05e3\u05f8\u05fc\u0601\u0607\u060e\u0614\u0616\u0626"+
		"\u0629\u062e\u0634\u0638\u063e\u0646\u064e\u0651\u0653\u0659\u065d\u0664"+
		"\u0667\u0669\u0674\u0677\u0680\u068b\u0691\u0696\u069c\u06a2\u06b4\u06bc"+
		"\u06be\u06c7\u06c9\u06d2\u06df\u06e1\u06e8\u06f4\u06fe\u0705\u0708\u070c"+
		"\u0718\u071b\u072f\u0734\u0748\u074a\u0756\u0766\u076b\u0778\u0785\u0789"+
		"\u0795\u079b\u07a7\u07ba\u07bf\u07c9\u07d8\u07dd\u07e1\u07e4\u07eb\u07f1"+
		"\u07f3\u07f6\u07fd\u0801\u0806\u080d\u0814\u081d\u0822\u082e\u0832\u0836"+
		"\u083a\u0841\u0844\u0846\u084d\u0850\u0854\u085f\u0869\u0876\u087e\u0881"+
		"\u0889\u0890\u0898\u08a2\u08aa\u08af\u08b2\u08b7\u08c0\u08c5\u08d3\u08d6"+
		"\u08d8\u08dd\u08e1\u08e4\u08eb\u08f3\u08f7\u08fb\u08fe\u0904\u0907\u0909"+
		"\u0912\u0918\u091b\u091e\u0922\u0926\u092d\u0936\u093a\u093c\u0940\u0949"+
		"\u094e\u0950\u0959\u0964\u096b\u096e\u0975\u097e\u0989\u0993\u0998\u099b"+
		"\u099f\u09ab\u09b3\u09bc\u09e8";
	public static final String _serializedATN = Utils.join(
		new String[] {
			_serializedATNSegment0,
			_serializedATNSegment1
		},
		""
	);
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}